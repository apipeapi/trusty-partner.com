var api_fade_speed = 500;

$(document).ready(function() {
    $(window).on("popstate", function (e) {
        location.reload();
    });
});


function api_qty_box_more(id,type) {    
    if (type == 'product_view')
        temp = '<table width="100%" border="0"><tr><td valign="middle"><span class="input-group-addon pointer btn-minus" onclick="api_qty_input_change(' + id + ',\'' + type + '\',\'sub\');" style="width: 35px; height:35px; border-left: 1px solid #ccc !important;"><span class="fa fa-minus"></span></span></td><td valign="middle"><input type="text" name="quantity" id="' + 'api_quantity_input_' + type + '_' + id + '" class="form-control text-center quantity-input" value="10" style="width: 100%; height:36px;" required="required" /></td><td valign="middle"><span class="input-group-addon pointer btn-plus" onclick="api_qty_input_change(' + id + ',\'' + type + '\',\'add\');" style="width: 35px; height:35px; border-right: 1px solid #ccc !important;"><span class="fa fa-plus"></span></span></td></tr></table>';    
    else {
        temp = '<table width="100%" border="0"><tr><td valign="middle"><span class="input-group-addon pointer btn-minus" onclick="api_qty_input_change(' + id + ',\'' + type + '\',\'sub\');" style="width: 30px; height:30px; border-left: 1px solid #ccc !important;"><span class="fa fa-minus"></span></span></td><td valign="middle"><input type="text" name="quantity" id="' + 'api_quantity_input_' + type + '_' + id + '" class="form-control text-center quantity-input" value="10" style="width: 100%; height:31px;" required="required" /></td><td valign="middle"><span class="input-group-addon pointer btn-plus" onclick="api_qty_input_change(' + id + ',\'' + type + '\',\'add\');" style="width: 30px; height:30px; border-right: 1px solid #ccc !important;"><span class="fa fa-plus"></span></span></td></tr></table>';
        $('#api_qty_box_' + type + '_' + id).css("width","144px");
    }

    if ($('#api_quantity_' + type + '_' + id).val() == 'more')    
        $('#api_qty_box_' + type + '_' + id).html(temp);
}
function api_qty_input_change(id,type,add_or_sub) {
    temp = document.getElementById('api_quantity_input_' + type + '_' + id).value;
    if (add_or_sub == 'add')
        temp = parseInt(temp) + 1;
    if (add_or_sub == 'sub') {
        if (parseInt(temp) <= 1)
            temp = 1;
        else
            temp = parseInt(temp) - 1;    
    }
    document.getElementById('api_quantity_input_' + type + '_' + id).value = temp;
}
function api_round_to(n, digits) {
    if (digits === undefined) {
        digits = 0;
    }
    
    var multiplicator = Math.pow(10, digits);
    n = parseFloat((n * multiplicator).toFixed(11));
    var test =(Math.round(n) / multiplicator);
    return +(test.toFixed(digits));
}  

function api_select_check_all(obj,me){
	var temp = obj.form.elements;
	for (var i=0;i<temp.length;i++){
		if (temp[i].name && temp[i].name == me) 
            temp[i].checked = obj.checked;
	}
}
function api_select_check_remove_cart(frm,chkname,url,url_redirect){
    var postData = [];
    var nocheck = true;
    var selected_id = 0;
	eles=document.forms[frm].elements;
    var id_list = '';
	for (var i=0;i<eles.length;i++){
		if (eles[i].name&&eles[i].name == chkname){
			if(eles[i].checked==true)
               id_list = id_list + '-' + eles[i].value;
		} 
	}
                
	if (id_list == '')
        alert("Please check a record");
    else {
        postData['api_id_list'] = id_list;
        var result = $.ajax
        (
        	{
        		url:url + '?api_id_list=' + id_list,
        		type: 'GET',
        		secureuri:false,
        		dataType: 'html',
        		data:postData,
        		async: false,
        		error: function (response, status, e)
        		{
        			alert(e);
        		}
        	}
        ).responseText;    
    	var array_data = String(result).split("api-ajax-request-multiple-result-split");
        if (array_data[1]) {
            //sa_alert('',array_data[1]);
            window.location = url_redirect;
        }
    }
}

function api_validate_email(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}


function api_remove_cart(frm,value,url,url_redirect){
    var postData = [];
    var nocheck = true;
    var selected_id = 0;

    postData['api_id_list'] = value;
    var result = $.ajax
    (
    	{
    		url:url + '?api_id_list=' + value,
    		type: 'GET',
    		secureuri:false,
    		dataType: 'html',
    		data:postData,
    		async: false,
    		error: function (response, status, e)
    		{
    			alert(e);
    		}
    	}
    ).responseText;    
	var array_data = String(result).split("api-ajax-request-multiple-result-split");
    if (array_data[1]) {
        //sa_alert('',array_data[1]);
        window.location = url_redirect;
    }
}

$(document).ready(function() {
    $(".api_numberic_input").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
             // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
             // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
});

if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    
    var header = document.getElementById("api_header");
    var header_sticky = document.getElementById("api_header_sticky");    

    var url = location.pathname + '';
    if (url.indexOf('/admin/') >= 0) {

    }
    else {
        window.onscroll = function() {api_header_sticky()};
        var sticky = header.offsetTop;
    }
    
    function api_header_sticky() {
        if (window.pageYOffset >= sticky) {
            header.classList.add("sticky");
            header_sticky.classList.add("api_header_sticky");
        } 
        else 
        {
            header.classList.remove("sticky");
            header_sticky.classList.remove("api_header_sticky");
        }
    }
}

function api_setValueCheckbox(check_name,set_id,api_form_product,sep){
    var temp = '';
	eles = document.forms[api_form_product].elements;
	for (var i=0;i<eles.length;i++){
		if (eles[i].name == check_name){
			if(eles[i].checked==true){
				temp = temp + sep + eles[i].value;
			}
		} 
	}		
	document.getElementById(set_id).value = temp;
}

function api_confirm(id) {
    if (confirm("You will order this item for the first time. Are you sure ?")) {
        $('#' + id).click();
    } else {

    }
}

//$('#large-title').scrollView();
$.fn.scrollView = function () {
    return this.each(function () {
        $('html, body').animate({
            scrollTop: $(this).offset().top
        }, 500);
    });
}

function api_bulk_actions_v2(form_action, action){
    $("#action-form").attr('action', form_action);
    $('#form_action').val(action);
    $("#action-form").submit();
}       
function api_generate_password(postData){
    var result = $.ajax
    (
    	{
    		url: postData['website_address'] + 'main/api_generate_password',
    		type: 'GET',
    		secureuri:false,
    		dataType: 'html',
    		data:postData,
    		async: false,
    		error: function (response, status, e)
    		{
    			alert(e);
    		}
    	}
    ).responseText;  
    var array_data = String(result).split("api-ajax-request-multiple-result-split");
    $('#api_modal_title').html(postData['generated_password']);
    $('#api_modal_body').html(array_data[1]);
    $('#api_modal_footer').html('<button type="button" class="btn btn-info" data-dismiss="modal" onclick="api_use_this_password()">'+ postData['use_this_password']+'</button> <button type="button" class="btn btn-danger" data-dismiss="modal">'+postData['close']+'</button>');
    $('#api_modal_trigger').click();
}

function api_ajax_update_display(postData){
    var result = $.ajax
    (
        {
            url: postData['ajax_file'],
            type: 'GET',
            secureuri:false,
            dataType: 'html',
            data:postData,
            async: false,
            error: function (response, status, e)
            {
                alert(e);
            }
        }
    ).responseText;
//var myWindow = window.open("", "MsgWindow", "width=700, height=400");
//myWindow.document.write(result);     
    var array_data = String(result).split("api-ajax-request-multiple-result-split");
    var b = array_data[0];
    var result_text = array_data[1];
    $("#" + postData['element_id']).html(array_data[2]);
}

function calculate_delivery_fee(distance) {
  var fee = 0;
  if (distance.indexOf('km') >= 0) {
    distance = parseFloat(distance);
    if (distance <= 1)
      fee = 1;
    if (distance > 1 && distance <= 1.5)
      fee = 1.5;
    if (distance > 1.5 && distance <= 2)
      fee = 2;
    if (distance > 2) {
      fee = 2;
      var temp = distance - 2;
      temp = temp / 0.5;
      temp_2 = parseInt(temp);
      if (temp_2 >= 1)
        fee = fee + (temp_2 * 0.5);
    }
  }
  else
    fee = 1;

  return fee;
}
function api_formatMoney(t,e){
    if(e||(e=site.settings.symbol),1==site.settings.sac)return(1==site.settings.display_symbol?e:"")+""+formatSA(parseFloat(t).toFixed(site.settings.decimals))+(2==site.settings.display_symbol?e:"");var s=accounting.formatMoney(t,e,site.settings.decimals,0==site.settings.thousands_sep?" ":site.settings.thousands_sep,site.settings.decimals_sep,"%s%v");return(1==site.settings.display_symbol?e:"")+s+(2==site.settings.display_symbol?e:"")
}

function round_up_second_decimal(value){
    //1-4 = 5
    //5-9 = 10 
    var temp_decimal = value - Math.floor(value);
    if (temp_decimal > 0) {
        temp_decimal = temp_decimal + "";
        
        var temp2 = temp_decimal.split(".");        
        var temp_decimal_array = temp2[1].split('');
        
        if (temp_decimal_array[1] >= 6) {
            value = parseFloat(value);
            value = value.toFixed(1);
        }
        else {            
            if (temp_decimal_array[1] > 0) {
                temp_decimal_array[1] = "5";                
                value = Math.floor(value) + parseFloat('0.' + temp_decimal_array[0] + temp_decimal_array[1]);
            }
        }
    }
    return value;
}

function api_ajax_update_ordering(postData){
    var result = $.ajax
    (
        {
            url: site.url + 'admin/welcome/api_ajax_update_ordering',
            type: 'GET',
            secureuri:false,
            dataType: 'html',
            data:postData,
            async: false,
            error: function (response, status, e)
            {
                alert(e);
            }
        }
    ).responseText;
// var myWindow = window.open("", "MsgWindow", "width=700, height=400");
// myWindow.document.write(result);       
    var array_data = String(result).split("api-ajax-request-multiple-result-split");
    var b = array_data[0];
    var result_text = array_data[1];
    window.location = postData['redirect'];
}
