function api_cart_express_ajax_load_cart_count(postData){
    $.ajax({
        type: 'GET',
        url:postData['ajax_url'],
        dataType: "json",
        data:postData,
        error: function (jqXHR, textStatus, errorThrown) {
            var message = errorThrown;
            if (jqXHR.responseText !== null && jqXHR.responseText !== 'undefined' && jqXHR.responseText !== '') {
                message = jqXHR.responseText;
            }
            console.log(message);
        },        
        success: function (result) {
            $('#api_cart_count_text').html(result.total_cart);
            $('.cart-total-items').html(result.total_cart_qty + ' ' + lang.item_v2);
        }
    });
}

function api_cart_express_ajax_update_cart_qty(postData){    
    url_redirect = site.base_url + 'cart';
    var b = 1;
    postData['qty'] = $('.api_component_cart_express .api_temp_qty_' + postData['cart_id']).html();
    postData['qty'] = parseFloat(postData['qty']);
    if (postData['type'] == 'add')
        postData['qty'] = postData['qty'] + 1;
    if (postData['type'] == 'sub') {
        if (postData['qty'] <= postData['min_qty']) {
            postData['qty'] = postData['min_qty'];
            b = 0;
        }
        else
            postData['qty'] = postData['qty'] - 1;
        if (postData['qty'] <= postData['min_qty']) {
            $('.api_component_cart_express .api_temp_button_sub_' + postData['cart_id']).addClass('api_temp_button_calculation');
            $('.api_component_cart_express .api_temp_button_sub_' + postData['cart_id']).removeClass('api_temp_button_calculation_disable');

            $('.api_component_cart_express .api_temp_button_sub_mobile_' + postData['cart_id']).removeClass('api_temp_disabled');
        }
        else {
            $('.api_component_cart_express .api_temp_button_sub_' + postData['cart_id']).removeClass('api_temp_button_calculation_disable');
            $('.api_component_cart_express .api_temp_button_sub_' + postData['cart_id']).addClass('api_temp_button_calculation');            

            $('.api_component_cart_express .api_temp_button_sub_mobile_' + postData['cart_id']).removeClass('api_temp_disabled');
        }
    }
    $('.api_component_cart_express .api_temp_qty_' + postData['cart_id']).html(postData['qty']);
    if (b == 1) {
        $.ajax({
            type: 'GET',
            url:postData['ajax_url'],
            dataType: "json",
            data:postData,
            error: function (jqXHR, textStatus, errorThrown) {
                var message = errorThrown;
                if (jqXHR.responseText !== null && jqXHR.responseText !== 'undefined' && jqXHR.responseText !== '') {
                    message = jqXHR.responseText;
                }
                console.log(message);
            },
            success: function (result) {
                //console.log(result.test);
                if (result.error == '') {
                    if (result.status == 'success') {        
                        $('.api_panel_right').html(result.cart_total_display);
                        $('.api_component_cart_express_total_cart').html(result.total_cart);
                        $('.api_component_cart_express_cart_total_cart_qty').html(result.total_cart_qty);
                        $('#api_cart_count_text').html(result.total_cart);
                        $('.cart-total-items').html(result.total_cart_qty + ' ' + lang.item_v2);
        
                        if (result.btn_sub_status == 'sub_enable') {
                            $('.api_component_cart_express .api_temp_button_sub_' + postData['cart_id']).addClass('api_temp_button_calculation');
                            $('.api_component_cart_express .api_temp_button_sub_' + postData['cart_id']).removeClass('api_temp_button_calculation_disable');

                            $('.api_component_cart_express .api_temp_button_sub_mobile_' + postData['cart_id']).removeClass('api_temp_disabled');
                        }
                        if (result.btn_sub_status == 'sub_disable') {
                            $('.api_component_cart_express .api_temp_button_sub_' + postData['cart_id']).addClass('api_temp_button_calculation_disable');
                            $('.api_component_cart_express .api_temp_button_sub_' + postData['cart_id']).removeClass('api_temp_button_calculation');

                            $('.api_component_cart_express .api_temp_button_sub_mobile_' + postData['cart_id']).addClass('api_temp_disabled');
                        }
                    }
                    else if (result.status == 'out_of_stock') {
                        // $('#api_swal3-question').hide();
                        // $('#api_swal3-warning').show();
                        // $('#api_swal3-info').hide();
                        // $('#api_swal3-success').hide();
                        // $('#api_modal_2_title').html('Out of stock');
                        // $('#api_modal_2_body').html(result.status);
            
                        // $('#api_modal_2_btn_ok').hide();
                        // $('#api_modal_2_btn_close').show();
            
                        // $('#api_modal_2_btn_close').html('<button id="" type="button" role="button" tabindex="0" class="api_swal3-cancel api_swal3-styled" data-dismiss="modal" onclick="window.location = \'' + url_redirect + '\'" style="background-color: rgb(170, 170, 170);">Ok</button>');    
            
                        // $('#api_modal_2_trigger').click();
                    }    
                    else if (result.status == 'minimum_quantity') {
                        // $('#api_swal3-question').hide();
                        // $('#api_swal3-warning').show();
                        // $('#api_swal3-info').hide();
                        // $('#api_swal3-success').hide();
                        // $('#api_modal_2_title').html('Minimum Quantity is ' + result.status);
                        // $('#api_modal_2_body').html('For this product');
            
                        // $('#api_modal_2_btn_ok').hide();
                        // $('#api_modal_2_btn_close').show();
            
                        // $('#api_modal_2_btn_close').html('<button id="" type="button" role="button" tabindex="0" class="api_swal3-cancel api_swal3-styled" data-dismiss="modal" onclick="window.location = \'' + url_redirect + '\'" style="background-color: rgb(170, 170, 170);">Ok</button>');    
            
                        // $('#api_modal_2_trigger').click();
                    }
                }
                else if (result.error == 'not_logged_in')
                    window.location = site.base_url + 'login';                
            }
        });
    }

}

function api_cart_express_ajax_update_cart_qty_restaurant(postData){    
    var b = 1;
    
    postData['qty'] = $('.api_component_cart_express .api_temp_qty_' + postData['cart_id']).html();
    postData['qty'] = parseFloat(postData['qty']);    

    if (postData['type'] == 'add') {
        postData['qty'] = postData['qty'] + 1;
        if (postData['qty'] > postData['min_qty']) {
            $('.api_component_cart_express .api_temp_button_sub_' + postData['cart_id']).removeClass('api_temp_button_calculation_disable');
            $('.api_component_cart_express .api_temp_button_sub_' + postData['cart_id']).addClass('api_temp_button_calculation');            
        }
    }
    if (postData['type'] == 'sub') {
        if (postData['qty'] <= postData['min_qty']) {
            postData['qty'] = postData['min_qty'];
            b = 0;
        }
        else
            postData['qty'] = postData['qty'] - 1;
        if (postData['qty'] <= postData['min_qty']) {
            $('.api_component_cart_express .api_temp_button_sub_' + postData['cart_id']).removeClass('api_temp_button_calculation');
            $('.api_component_cart_express .api_temp_button_sub_' + postData['cart_id']).addClass('api_temp_button_calculation_disable');
            $('.api_component_cart_express .api_temp_button_sub_mobile_' + postData['cart_id']).removeClass('api_temp_disabled');
        }
        else {
            $('.api_component_cart_express .api_temp_button_sub_' + postData['cart_id']).removeClass('api_temp_button_calculation_disable');
            $('.api_component_cart_express .api_temp_button_sub_' + postData['cart_id']).addClass('api_temp_button_calculation');            

            $('.api_component_cart_express .api_temp_button_sub_mobile_' + postData['cart_id']).addClass('api_temp_disabled');
        }
    }
    $('.api_component_cart_express .api_temp_qty_' + postData['cart_id']).html(postData['qty']);

    if (b == 1) {
        $.ajax({
            type: 'GET',
            url:postData['ajax_url'],
            dataType: "json",
            data:postData,
            error: function (jqXHR, textStatus, errorThrown) {
                var message = errorThrown;
                if (jqXHR.responseText !== null && jqXHR.responseText !== 'undefined' && jqXHR.responseText !== '') {
                    message = jqXHR.responseText;
                }
                console.log(message);
            },
            success: function (result) {
                console.log(result.test);
                if (result.error == '') {
                    if (result.status == 'success') { 

                        // $('.api_component_cart_express_total_cart').html(result.total_cart);
                        // $('.api_component_cart_express_cart_total_cart_qty').html(result.total_cart_qty);
                        // $('#api_cart_count_text').html(result.total_cart);
                        // $('.cart-total-items').html(result.total_cart_qty + ' ' + lang.item_v2);
        
                        // if (result.btn_sub_status == 'sub_enable') {
                        //     $('.api_component_cart_express .api_temp_button_sub_' + postData['cart_id']).addClass('api_temp_button_calculation');
                        //     $('.api_component_cart_express .api_temp_button_sub_' + postData['cart_id']).removeClass('api_temp_button_calculation_disable');

                        //     $('.api_component_cart_express .api_temp_button_sub_mobile_' + postData['cart_id']).removeClass('api_temp_disabled');
                        // }
                        // if (result.btn_sub_status == 'sub_disable') {
                        //     $('.api_component_cart_express .api_temp_button_sub_' + postData['cart_id']).addClass('api_temp_button_calculation_disable');
                        //     $('.api_component_cart_express .api_temp_button_sub_' + postData['cart_id']).removeClass('api_temp_button_calculation');

                        //     $('.api_component_cart_express .api_temp_button_sub_mobile_' + postData['cart_id']).addClass('api_temp_disabled');
                        // }
                    }
                }
                else if (result.error == 'not_logged_in')
                    window.location = site.base_url + 'login';                
            }
        });
    }

}

function api_cart_express_remove_selected(postData){    
	var eles = document.forms[postData['form_name']].elements;
    var id_list = '';
	for (var i=0;i<eles.length;i++){
		if (eles[i].name == postData['checkbox_name']){
			if(eles[i].checked==true)
               id_list = id_list + '-' + eles[i].value;
		} 
	}

    if (id_list != '') {
        postData['cart_id'] = id_list;
        $.ajax({
            type: 'GET',
            url:postData['ajax_url'],
            dataType: "json",
            data:postData,
            success: function (result) {
                // var myWindow = window.open("", "MsgWindow", "width=700, height=400");
                // myWindow.document.write(result);   
                //console.log(result);
                if (result.error == '') {
                    for (var i=0;i<eles.length;i++){
                        if (eles[i].name == postData['checkbox_name']){
                            if(eles[i].checked==true) {
                            id_list = id_list + '-' + eles[i].value;
                            $('#api_temp_box_' + eles[i].value).addClass('api_display_none');
                            }
                        } 
                    }            
                    $('.api_panel_right').html(result.cart_total_display);
                    $('.api_component_cart_express_total_cart').html(result.total_cart);
                    $('.api_component_cart_express_cart_total_cart_qty').html(result.total_cart_qty);
                    $('#api_cart_count_text').html(result.total_cart);
                    $('.cart-total-items').html(result.total_cart_qty + ' ' + lang.item_v2);
                }
                else if (result.error == 'not_logged_in')
                    window.location = site.base_url + 'login';                
            }
        });    
    }
}

function api_cart_express_ajax_company_change(postData){    
    $.ajax({
        type: 'GET',
        url:postData['ajax_url'],
        dataType: "json",
        data:postData,
        success: function (result) {
            // var myWindow = window.open("", "MsgWindow", "width=700, height=400");
            // myWindow.document.write(result);
            //console.log($result.test); 
            if (result.error == '') {
                $('.api_component_cart_express .api_temp_address').html(result.display);
            }
            else if (result.error == 'not_logged_in')
                window.location = site.base_url + 'login';
        }
    });
}

function api_cart_express_ajax_update_cart(postData){    
    $.ajax({
        type: 'GET',
        url:postData['ajax_url'],
        dataType: "json",
        data:postData,
        success: function (result) {
            //console.log($result.test); 
            if (result.error == '') {      
                
            }
            else if (result.error == 'not_logged_in')
                window.location = site.base_url + 'login';
        }
    });
}

function api_cart_express_ajax_company_change(postData){    
    $.ajax({
        type: 'GET',
        url:site.base_url + 'cart/api_cart_express_ajax_company_change',
        dataType: "json",
        data:postData,
        success: function (result) {
            // var myWindow = window.open("", "MsgWindow", "width=700, height=400");
            // myWindow.document.write(result);   
            if (result.error == '') {
                $('.api_component_cart_express .api_temp_address').html(result.display);
            }
            else if (result.error == 'not_logged_in')
                window.location = site.base_url + 'login';
        }
    });
}

function api_cart_express_ajax_update_payment_method(postData){   
    $.ajax({
        type: 'GET',
        url:postData['ajax_url'],
        dataType: "json",
        data:postData,
        success: function (result) {
            //console.log(result);
            if (result.error == '') {      
                $('.api_component_cart_express .api_temp_wrapper_payment_method').html(result.display);
            }
            else if (result.error == 'not_logged_in')
                window.location = site.base_url + 'login';
        }
    });
}


function api_cart_express_remove(postData){
    $.ajax({
        type: 'GET',
        url:postData['ajax_url'],
        dataType: "json",
        data:postData,
        success: function (result) {
            // var myWindow = window.open("", "MsgWindow", "width=700, height=400");
            // myWindow.document.write(result);   
            //console.log(result.test);
            if (result.error == '') {
                $('#api_temp_box_' + postData['cart_id']).addClass('api_display_none');
                $('.api_panel_right').html(result.cart_total_display);
                $('.api_component_cart_express_total_cart').html(result.total_cart);
                $('.api_component_cart_express_cart_total_cart_qty').html(result.total_cart_qty);
                $('#api_cart_count_text').html(result.total_cart);
                $('.cart-total-items').html(result.total_cart_qty + ' ' + lang.item_v2);
            }
            else if (result.error == 'not_logged_in')
                window.location = site.base_url + 'login';            
        }
    });
}

function api_cart_express_ajax_add_cart(postData){
    $.ajax({
        type: 'GET',
        url:postData['ajax_url'],
        dataType: "json",
        data:postData,
        success: function (result) {
            // var myWindow = window.open("", "MsgWindow", "width=700, height=400");
            // myWindow.document.write(result);
            //console.log(result);
            if (result.error == '') {

                var i = $(postData['image_class']);
                i.clone().offset({top:i.offset().top,left:i.offset().left}).css({opacity:"0.5",position:"absolute",height:"150px",width:"150px","z-index":"1000"}).appendTo($("body")).animate({top:$(".api_cart_count_text").offset().top,left:$(".api_cart_count_text").offset().left,width:"50px",height:"50px"},400).animate({width:0,height:0});

                $('#api_cart_count_text').html(result.total_cart);
                $('.cart-total-items').html(result.total_cart_qty + ' ' + lang.item_v2);
            }
            else if (result.error == 'not_logged_in')
                window.location = site.base_url + 'login';
            else
                sa_alert('Error', result.error, 'error', 1);
        }
    });
}

function api_cart_express_ajax_order_insert(postData){
    $.ajax({
        type: 'GET',
        url:postData['ajax_url'],
        dataType: "json",
        data:postData,
        beforeSend: function() {
            $('.api_section_wrapper').fadeOut(api_fade_speed, function() {
                $('.api_section_wrapper').html(api_loading_html);
                $(".api_section_wrapper").fadeIn(api_fade_speed);
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            var message = errorThrown;
            if (jqXHR.responseText !== null && jqXHR.responseText !== 'undefined' && jqXHR.responseText !== '') {
                message = jqXHR.responseText;
            }
            console.log(message);
        },
        success: function (result) {
            //console.log(result.test);
            if (result.error == '') {
                if (result.post_data != '') {
                    api_get_data = {};
                    api_get_data = result.post_data['get_data'];
                    var postData = result.post_data;
                    postData['script'] = result.script;
                    api_ajax_load_page(postData);
                }
                // $('.api_body').scrollView();
                // $.getScript(result.script);                
                // //console.log(result.script_2);
                // $('.api_section_wrapper').html(api_loading_html);
                // $('.api_section_wrapper').fadeOut(api_fade_speed, function() {
                //     $('.api_section_wrapper').addClass(result.wrapper_class);
                //     $('.api_section_wrapper').html('<div class="api_section">'+result.display+'</div>');
                //     $.getScript(result.script_2);
                //     if (postData['page_url'] != '')
                //         history.pushState('','',result.page_url);
                //     $(".api_section_wrapper").fadeIn(api_fade_speed);
                // });
            }
            else if (result.error == 'not_logged_in')
                window.location = site.base_url + 'login';    
        }
    });
}
