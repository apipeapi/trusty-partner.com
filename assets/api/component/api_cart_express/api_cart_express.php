<?php

class api_cart_express {

public function __construct()
{
    
}
public function __get($var)
{
    return get_instance()->$var;
}

public function config($config_data_function) {
    $config_data = array(
        'cart_type' => $config_data_function['cart_type'],
        'step' => $config_data_function['step'],        
        'btn_back_to_cart' => $config_data_function['btn_back_to_cart'],
        'table_id' => $config_data_function['table_id'],
        'category_id' => $config_data_function['category_id'],
        'product_id' => $config_data_function['product_id'],
    );  
    $select_data = $this->initial_data($config_data);    
    $config_data = array(
        'select_data' => $select_data
    );
    $temp_cart_item = $this->cart_item($config_data);    
    $config_data = array(
        'cart_list_title' => lang('shopping_cart'),
        'image_wrapper_class' => 'api_box_120_120',
        'title_class' => 'api_text_dot_1_5',
        'cart_item' => $temp_cart_item['cart_item'],
        'continue_shopping_class' => '',
        'continue_shopping_link' => base_url().'products',
        'duration_class' => '',
        'link_disable' => 0,
        'delete_class' => '',
    );
    $return = array_merge($select_data, $config_data);
    return $return;
}
public function config_restaurant($config_data_function) {
    $config_data = array(
        'table_name' => 'sma_restaurant',
        'select_table' => 'sma_restaurant',
        'translate' => '',
        'description' => '',
        'select_condition' => "id = ".$config_data_function['table_id'],
    );
    $temp = $this->api_helper->api_select_data_v2($config_data);
    $select_data = json_decode($temp[0]['json_data'],true);

    $config_data = array(
        'select_data' => $select_data
    );
    $temp_cart_item = $this->cart_item($config_data);
    $config_data = array(
        'cart_list_title' => lang('shopping_cart'),
        'image_wrapper_class' => 'api_box_120_120',
        'title_class' => 'api_text_dot_1_5',
        'cart_item' => $temp_cart_item['cart_item'],
        'continue_shopping_class' => '',
        'continue_shopping_link' => base_url().'products',
        'duration_class' => '',
        'link_disable' => 0,
        'delete_class' => '',
    );
    $return = array_merge($select_data, $config_data);
    //$this->api_helper->print_array($return);
    
    return $return;    
}

public function get_data($config_data_function) {
    $return = array();
    $id = get_cookie('api_user_cart_id_'.$config_data_function['cart_type'], TRUE);
    if ($id == '') {
        $api_user_cart_id = md5($this->session->userdata('user_id').microtime());
        set_cookie('api_user_cart_id_'.$config_data_function['cart_type'], $api_user_cart_id, 2592000);
        $data['id'] = $api_user_cart_id;
        $temp = array(
            'id' => $api_user_cart_id,
            'cart_type' => $config_data_function['cart_type'],
            'user_id' => $this->session->userdata('user_id'),
            'json_data' => json_encode($data),
        );
        $this->db->insert('sma_user_cart', $temp);
    }
    $config_data = array(
        'table_name' => 'sma_user_cart',
        'select_table' => 'sma_user_cart',
        'translate' => '',
        'description' => '',
        'select_condition' => "id = '".$id."' and cart_type = '".$config_data_function['cart_type']."'",
    );
    $select_data = $this->api_helper->api_select_data_v2($config_data);
    $return = json_decode($select_data[0]['json_data'],true);

    return $return;
}

public function initial_data($config_data_function) {
    $l = $this->api_shop_setting[0]['api_lang_key'];            
    $return = array();
    if ($this->loggedIn)
        $temp_customer = $this->site->getCompanyByID_v2($this->session->userdata('company_id'));

    $temp = $this->get_data($config_data_function);
    $select_data = array_merge($temp, $config_data_function);
    //$this->sma->print_arrays($select_data);

    if ($config_data_function['step'] == '' || $config_data_function['step'] == 'cart') {
        $select_data['customer_id'] = $this->session->userdata('company_id');
        //-addition_price_rate--------------------------------
            $config_data = array(
                'customer_id' => $this->session->userdata('company_id'),
            );
            $temp = $this->site->api_calculate_customer_group_price($config_data);
            if ($temp['discount_rate'] > 0)
                $temp_order_discount = $temp_customer->discount_product - $temp['discount_rate'];
            else
                $temp_order_discount = abs($temp['discount_rate']) + $temp_customer->discount_product;
            if ($temp_order_discount < 0)
                $temp_addition_price_rate = abs($temp_order_discount);
        //-addition_price_rate--------------------------------

        $select_data['total_food'] = 0;
        $select_data['total_alcohol'] = 0;
        $select_data['total'] = 0;
        $select_data['subtotal'] = 0;
        $select_data['total_cart_qty'] = 0;
        $select_data['discount'] = 0;
        $select_data['discount_rate'] = 0;
        $select_data['total_vat'] = 0;
        $select_data['total_plt'] = 0;
        $select_data['grand_total'] = 0;
        for ($i=0;$i<count($select_data['cart_item']);$i++) {
            $config_data = array(
                'table_name' => 'sma_products',
                'select_table' => 'sma_products',
                'translate' => 'yes',
                'select_condition' => "id = ".$select_data['cart_item'][$i]['id'],
            );
            $temp = $this->site->api_select_data_v2($config_data);

            $select_data['cart_item'][$i]['name'] = $temp[0]['title_'.$l];
            $select_data['cart_item'][$i]['image'] = $temp[0]['image'];
            $select_data['cart_item'][$i]['slug'] = $temp[0]['slug'];
            $select_data['cart_item'][$i]['quantity'] = $temp[0]['quantity'];

            $config_data_2 = array(
                'id' => $select_data['cart_item'][$i]['id'],
                'customer_id' => $this->session->userdata('company_id'),
                'addition_price_rate' => $temp_addition_price_rate,
            );
            $temp = $this->site->api_calculate_product_price_v2($config_data_2);
            $select_data['cart_item'][$i]['price'] = $this->sma->formatDecimal($temp['price']);
            
            $select_data['cart_item'][$i]['price_original'] = $this->sma->formatDecimal($temp['temp_price']);
            
            $select_data['cart_item'][$i]['discount_rate'] = 0;
            $select_data['cart_item'][$i]['discount_price'] = 0;
            if ($temp['promotion']['price'] > 0) {
                $select_data['cart_item'][$i]['is_promotion'] = 1;
                $select_data['cart_item'][$i]['discount_rate'] = $temp['promotion']['rate'];
                $select_data['cart_item'][$i]['min_qty'] = $temp['promotion']['min_qty'];
                $select_data['cart_item'][$i]['duration_left'] = $temp['promotion']['duration_left'];
                $select_data['cart_item'][$i]['discount_price'] = $this->sma->formatDecimal($temp['promotion']['discount_price']);
            }
            else
                $select_data['cart_item'][$i]['is_promotion'] = 0;

            $temp_ordered = $this->site->api_is_ordered($select_data['cart_item'][$i]['id']);
            $select_data['cart_item'][$i]['ordered'] = $temp_ordered;
            $select_data['cart_item'][$i]['total'] = $this->sma->formatDecimal($select_data['cart_item'][$i]['price'] * $select_data['cart_item'][$i]['qty']);

            $config_data = array(
                'product_id' => $select_data['cart_item'][$i]['id'],
                'customer_id' => $temp_customer->id,
                'amount' => $select_data['cart_item'][$i]['price'],
            );
            $temp = $this->site->api_calculate_product_tax($config_data);
            $select_data['cart_item'][$i]['plt'] = 0;
            if ($temp['result'] > 0) {
                $select_data['total_alcohol'] += $select_data['cart_item'][$i]['total'];
                $select_data['cart_item'][$i]['plt'] = $this->sma->formatDecimal($temp['result']);
            }
            else {
                $select_data['total_food'] += $select_data['cart_item'][$i]['total'];
            }

            $select_data['total_cart_qty'] += $select_data['cart_item'][$i]['qty'];
            $select_data['total'] += $select_data['cart_item'][$i]['total'];
        }
        $select_data['total_food'] = $this->sma->formatDecimal($select_data['total_food']);
        $select_data['total_alcohol'] = $this->sma->formatDecimal($select_data['total_alcohol']);
        $select_data['total'] = $this->sma->formatDecimal($select_data['total']);
        $select_data['subtotal'] = $select_data['total'];
        $temp_total_alcohol = $select_data['total_alcohol'];
        
        //-order_discount--------------------------------
            if ($temp_order_discount > 0) {
                $select_data['discount_rate'] = $temp_order_discount;
                $select_data['discount'] = ($select_data['total'] * $select_data['discount_rate']) / 100;
                $select_data['subtotal'] = $select_data['total'] - $select_data['discount'];
                $temp_total_alcohol = $select_data['total_alcohol'] - ($select_data['total_alcohol'] * $select_data['discount_rate']) / 100;
            }
            $select_data['service_fee'] = 0;
            $select_data['service_fee_rate'] = 0;
            if ($temp_order_discount < 0) {         
                // $temp = abs($temp_order_discount);
                // $select_data['service_fee'] = ($select_data['total'] * abs($temp_order_discount)) / 100;
                // $select_data['service_fee_rate'] = abs($temp_order_discount);
                // $select_data['subtotal'] = $select_data['total'] + $select_data['service_fee'];
                // $temp_total_alcohol = $select_data['total_alcohol'] + ($select_data['total_alcohol'] * $select_data['service_fee_rate']) / 100;
            }
            $select_data['discount'] = $this->sma->formatDecimal($select_data['discount']);
            $select_data['subtotal'] = $this->sma->formatDecimal($select_data['subtotal']);
            $select_data['service_fee'] = $this->sma->formatDecimal($select_data['service_fee']);
        //-order_discount--------------------------------

        //-plt--------------------------------     
            if ($select_data['total_alcohol'] > 0) {
                $select_data['total_plt'] = $this->site->api_calulate_plt_amount($temp_total_alcohol);
                if ($temp_customer->vat_no != '' && $temp_customer->vat_invoice_type != 'do') {
                }
                else
                    $select_data['total_plt'] = 0;
                $b = 0;
                if ($this->api_shop_setting[0]['customer_plt'] == 'yes' && $temp_customer->plt != 'yes')
                    $b = 1;
                if ($b == 1) 
                    $select_data['total_plt'] = 0;
            }
            $select_data['total_plt'] = $this->sma->formatDecimal($select_data['total_plt']);
        //-plt--------------------------------            

        //-vat--------------------------------            
            if ($temp_customer->vat_no != '' && $temp_customer->vat_invoice_type != 'do')     
                $select_data['total_vat'] = ($select_data['subtotal'] * 10) / 100;
            $select_data['total_vat'] = $this->sma->formatDecimal($select_data['total_vat']);
        //-vat--------------------------------            

        $select_data['shipping'] = $this->api_shop_setting[0]['shipping'];

        //-grand_total--------------------------------            
            $select_data['grand_total'] = $select_data['subtotal'] + $select_data['total_vat'] + $select_data['total_plt'] + $select_data['shipping'];
            $select_data['grand_total'] = $this->sma->formatDecimal($select_data['grand_total']);
        //-grand_total--------------------------------            

        if ($select_data['total_cart_qty'] <= 0) $select_data['total_cart_qty'] = 0;
        $temp = array(
            'json_data' => json_encode($select_data)
        );        
        $this->db->update('sma_user_cart', $temp,"id = '".$select_data['id']."'");
    }

    //$this->api_helper->print_array($select_data);
    return $select_data;
}

public function initial_data_restaurant($config_data_function) {

    $config_data = array(
        'table_name' => 'sma_restaurant',
        'select_table' => 'sma_restaurant',
        'translate' => '',
        'description' => '',
        'select_condition' => "id = ".$config_data_function['table_id'],
    );
    $select_table = $this->api_helper->api_select_data_v2($config_data);

    //$temp_customer = $this->site->getCompanyByID_v2($this->session->userdata('company_id'));
    //$select_data['customer_id'] = $this->session->userdata('company_id');


    //-addition_price_rate--------------------------------
        // $config_data = array(
        //     'customer_id' => $this->session->userdata('company_id'),
        // );
        // $temp = $this->site->api_calculate_customer_group_price($config_data);
        // if ($temp['discount_rate'] > 0)
        //     $temp_order_discount = $temp_customer->discount_product - $temp['discount_rate'];
        // else
        //     $temp_order_discount = abs($temp['discount_rate']) + $temp_customer->discount_product;
        // if ($temp_order_discount < 0)
        //     $temp_addition_price_rate = abs($temp_order_discount);
    //-addition_price_rate--------------------------------
    
    if ($select_table[0]['json_data'] != '') {
        $select_data = json_decode($select_table[0]['json_data'],true);
        $select_data['total_food'] = 0;
        $select_data['total_alcohol'] = 0;
        $select_data['total'] = 0;
        $select_data['subtotal'] = 0;
        $select_data['total_cart_qty'] = 0;
        $select_data['discount'] = 0;
        $select_data['discount_rate'] = 0;
        $select_data['total_vat'] = 0;
        $select_data['total_plt'] = 0;
        $select_data['grand_total'] = 0;


        $select_data['total_food'] = $this->sma->formatDecimal($select_data['total_food']);
        $select_data['total_alcohol'] = $this->sma->formatDecimal($select_data['total_alcohol']);
        $select_data['total'] = $this->sma->formatDecimal($select_data['total']);
        $select_data['subtotal'] = $select_data['total'];
        $temp_total_alcohol = $select_data['total_alcohol'];
        
        //-order_discount--------------------------------
            if ($temp_order_discount > 0) {
                $select_data['discount_rate'] = $temp_order_discount;
                $select_data['discount'] = ($select_data['total'] * $select_data['discount_rate']) / 100;
                $select_data['subtotal'] = $select_data['total'] - $select_data['discount'];
                $temp_total_alcohol = $select_data['total_alcohol'] - ($select_data['total_alcohol'] * $select_data['discount_rate']) / 100;
            }
            $select_data['service_fee'] = 0;
            $select_data['service_fee_rate'] = 0;
            if ($temp_order_discount < 0) {         
                // $temp = abs($temp_order_discount);
                // $select_data['service_fee'] = ($select_data['total'] * abs($temp_order_discount)) / 100;
                // $select_data['service_fee_rate'] = abs($temp_order_discount);
                // $select_data['subtotal'] = $select_data['total'] + $select_data['service_fee'];
                // $temp_total_alcohol = $select_data['total_alcohol'] + ($select_data['total_alcohol'] * $select_data['service_fee_rate']) / 100;
            }
            $select_data['discount'] = $this->sma->formatDecimal($select_data['discount']);
            $select_data['subtotal'] = $this->sma->formatDecimal($select_data['subtotal']);
            $select_data['service_fee'] = $this->sma->formatDecimal($select_data['service_fee']);
        //-order_discount--------------------------------

        //-plt--------------------------------     
            if ($select_data['total_alcohol'] > 0) {
                $select_data['total_plt'] = $this->site->api_calulate_plt_amount($temp_total_alcohol);
                if ($temp_customer->vat_no != '' && $temp_customer->vat_invoice_type != 'do') {
                }
                else
                    $select_data['total_plt'] = 0;
                $b = 0;
                if ($this->api_shop_setting[0]['customer_plt'] == 'yes' && $temp_customer->plt != 'yes')
                    $b = 1;
                if ($b == 1) 
                    $select_data['total_plt'] = 0;
            }
            $select_data['total_plt'] = $this->sma->formatDecimal($select_data['total_plt']);
        //-plt--------------------------------            

        //-vat--------------------------------            
            if ($temp_customer->vat_no != '' && $temp_customer->vat_invoice_type != 'do')     
                $select_data['total_vat'] = ($select_data['subtotal'] * 10) / 100;
            $select_data['total_vat'] = $this->sma->formatDecimal($select_data['total_vat']);
        //-vat--------------------------------            

        $select_data['shipping'] = $this->api_shop_setting[0]['shipping'];

        //-grand_total--------------------------------            
            $select_data['grand_total'] = $select_data['subtotal'] + $select_data['total_vat'] + $select_data['total_plt'] + $select_data['shipping'];
            $select_data['grand_total'] = $this->sma->formatDecimal($select_data['grand_total']);
        //-grand_total--------------------------------            

        if ($select_data['total_cart_qty'] <= 0) $select_data['total_cart_qty'] = 0;
        $temp = array(
            'json_data' => json_encode($select_data)
        );        
        $this->db->update('sma_user_cart', $temp,"id = '".$select_data['id']."'");
    }
    else {
        $select_data['total_food'] = 0;
        $select_data['total_alcohol'] = 0;
        $select_data['total'] = 0;
        $select_data['subtotal'] = 0;
        $select_data['total_cart_qty'] = 0;
        $select_data['discount'] = 0;
        $select_data['discount_rate'] = 0;
        $select_data['total_vat'] = 0;
        $select_data['total_plt'] = 0;
        $select_data['grand_total'] = 0;
        $temp_json = json_encode($select_data);
        $temp = array(
            'json_data' => $temp_json,
        );
        $this->db->update('sma_restaurant', $temp,"id = ".$config_data_function['table_id']);
    }
    return $select_data;
}

public function get_data_by_cart_id($config_data_function) {
    $return = array();
    
    if ($id = get_cookie('api_user_cart_id_'.$config_data_function['cart_type'], TRUE)) {
        $config_data = array(
            'table_name' => 'sma_user_cart',
            'select_table' => 'sma_user_cart',
            'translate' => '',
            'description' => '',
            'select_condition' => "id = '".$id."' and cart_type = '".$config_data_function['cart_type']."'",
        );
        $select_data = $this->api_helper->api_select_data_v2($config_data);
        $temp = json_decode($select_data[0]['json_data'],true);
        $temp_2 = $temp['cart_item'];
        for ($i=0;$i<count($temp_2);$i++) {
            if ($temp_2[$i]['cart_id'] == $config_data_function['cart_id']) {
                $return = $temp_2[$i];
                break;
            }             
        }
        
    }
    return $return;
}

public function cart_item($config_data_function) {
    $return = array();
    $k = 0;
    //$this->sma->print_arrays($select_data);
    $select_data = $config_data_function['select_data'];
    for ($i=0;$i<count($select_data['cart_item']);$i++) {
        if ($select_data['cart_item'][$i]['id'] > 0) {
            $initial_data[$k] = $select_data['cart_item'][$i];
            $initial_data[$k]['id'] = $select_data['cart_item'][$i]['id'];
            $initial_data[$k]['name'] = $select_data['cart_item'][$i]['name'];
            if (is_file('assets/uploads/thumbs/'.$select_data['cart_item'][$i]['image']))
                $initial_data[$k]['image'] = $select_data['cart_item'][$i]['image'];
            else
                $initial_data[$k]['image'] = 'no_image.jpg';
            $initial_data[$k]['link'] = base_url().'product-detail/'.$select_data['cart_item'][$i]['slug'];
    
            $initial_data[$k]['price'] = $select_data['cart_item'][$i]['price'];
            $initial_data[$k]['price_original'] = $select_data['cart_item'][$i]['price_original'];
            $initial_data[$k]['price_label'] = $this->sma->formatMoney($select_data['cart_item'][$i]['price']);
            
            $initial_data[$k]['is_promotion'] = $select_data['cart_item'][$i]['is_promotion'];
            $initial_data[$k]['discount_rate'] = $select_data['cart_item'][$i]['discount_rate'];
            $initial_data[$k]['min_qty'] = 1;
            if ($initial_data[$k]['is_promotion'] == 1) {
                $initial_data[$k]['promotion_label'] = $this->sma->formatMoney($initial_data[$k]['price_original']);
                $initial_data[$k]['promotion_rate_label'] = $initial_data[$k]['discount_rate'].'%';      
                $initial_data[$k]['promotion_duration_left'] = $select_data['cart_item'][$i]['duration_left'];                    
                $initial_data[$k]['min_qty'] = $select_data['cart_item'][$i]['min_qty'];                    
            }
            
            $initial_data[$k]['qty'] = $select_data['cart_item'][$i]['qty'];
            $initial_data[$k]['title'] = $select_data['cart_item'][$i]['name'];
            $initial_data[$k]['cart_id'] = $select_data['cart_item'][$i]['cart_id'];
            $initial_data[$k]['stock_qty'] = $select_data['cart_item'][$i]['quantity'];
            $initial_data[$k]['ordered'] = $select_data['cart_item'][$i]['ordered'];
            $k++;
        }
    }
    $return['cart_item'] = $initial_data;
    return $return;
}

public function css($config_data_function) {
    $return = array();
    $return['display'] .= '
        <link href="'.base_url().'assets/api/component/api_cart_express/css/style.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
        <link href="'.base_url().'assets/api/component/api_cart_express/css/style_1024.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
        <link href="'.base_url().'assets/api/component/api_cart_express/css/style_768.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">                
        <link href="'.base_url().'assets/api/component/api_cart_express/css/style_600.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
    ';
    return $return;
}

public function script($config_data_function) {
    $return = array();
    $return['script_url'][0] = base_url().'assets/api/component/api_cart_express/js/script.js?v='.$this->api_shop_setting[0]['version'];
    
    for ($i=0;$i<count($return['script_url']);$i++) {
        $return['script'] .= '
            <script src="'.$return['script_url'][$i].'"></script>
        ';
    }
    return $return;
}

public function view_cart_list($config_data_function) {
    $return['display'] .= '
        <div class="api_component_cart_express">
            <form name="api_component_cart_express_form_cart" id="api_component_cart_express_form_cart" action="" method="post">        
    ';
    $return['display'] .= '
        <div class="api_temp_box">
            <table width="100%" border="0">
            <tr>
            <td valign="middle">
                <span class="api_large">
                    <i class="fa fa-shopping-cart"></i> 
                    '.$config_data_function['cart_list_title'].' 
                    (<span class="api_component_cart_express_total_cart">'.count($config_data_function['cart_item']).'</span>)
                </span>
                <span class="api_component_cart_express_cart_total_cart_qty">
                    '.$config_data_function['total_cart_qty'].' 
                </span>
                <span>
                    '.lang('item_v2').'
                </span>

                <div class="api_height_15 hidden-xs"></div>
                <table border="0" class="hidden-xs">
                <tr>
                <td valign="middle">
                    <input class="api_temp_checkbox api_display_block_im" type="checkbox" name="api_check_all" id="api_check_all" onclick="api_select_check_all(this,\'api_check[]\');" />
                </td>
                <td valign="middle">
                    <div class="api_height_3"></div>
                    '.lang('Select all').'
                </td>
                </tr>
                </table>

            </td>
            <td valign="middle" align="right" class="hidden-xs '.$config_data_function['continue_shopping_class'].'">
                <a class="api_bold api_theme_link_1" href="javascript:void(0);" onclick="
                    api_get_data = {
                        \'slug_0\' : \'product\',
                    };
                    var postData = {
                        \'page\' : \'product\',
                        \'get_data\' : api_get_data,
                    };
                    api_ajax_load_page(postData);            
                ">
                    <i class="fa fa-share"></i>
                    &nbsp;'.lang('continue_shopping').'
                </a>                
            </td>
            </tr>
            </table>            
        </div>
    ';

    $select_data = $config_data_function['cart_item'];    
    for ($i=0;$i<count($select_data);$i++) {
        $return['display'] .= '
            <div class="api_temp_box" id="api_temp_box_'.$select_data[$i]['cart_id'].'">
                <table width="100%" border="0">
                <tr>
                <td valign="middle" class="api_td_width_auto hidden-xs">
                    <input class="api_temp_checkbox api_display_block_im" type="checkbox" name="api_check[]" onclick="$(\'#api_check_all\').prop(\'checked\', false);" value="'.$select_data[$i]['cart_id'].'"> 
                </td>
                <td valign="top" class="api_td_width_auto api_temp_td_1">
        ';
                    if ($config_data_function['link_disable'] != 1)
                        $return['display'] .= '
                            <a href="'.$select_data[$i]['link'].'">
                        ';
                    $return['display'] .= '
                            <div class="'.$config_data_function['image_wrapper_class'].'">
                                <img class="api_temp_image" src="'.base_url().'assets/uploads/thumbs/'.$select_data[$i]['image'].'" />
                            </div>
                    ';
                    if ($config_data_function['link_disable'] != 1)
                        $return['display'] .= '
                            </a>
                        ';
            $return['display'] .= '
                </td>
                <td valign="top" class="api_temp_td_2">
                <div class="api_position_relative">
                    <div class="'.$config_data_function['title_class'].'">
            ';
                        if ($config_data_function['link_disable'] != 1)
                        $return['display'] .= '
                            <a class="api_temp_a_2" href="'.$select_data[$i]['link'].'">
                        ';
                        $return['display'] .= '
                            '.$select_data[$i]['title'].'
                        ';
                        if ($config_data_function['link_disable'] != 1)
                        $return['display'] .= '
                            </a>
                        ';
                $return['display'] .= '
                    </div>
                    <div class="api_temp_price">
                        '.$select_data[$i]['price_label'].'
                    </div>                
                ';

                if ($select_data[$i]['is_promotion'] == 1)
                    $return['display'] .= '
                        <div class="api_temp_promotion">
                            <span class="api_temp_promotion_label">
                                '.$select_data[$i]['promotion_label'].'
                            </span>
                            &nbsp;
                            <span class="api_temp_promotion_rate_label">
                                '.$select_data[$i]['promotion_rate_label'].' <small>Off</small>
                            </span>
                            &nbsp;              
                            <span class="'.$config_data_function['duration_class'].' label label-info api_border_r10">
                                '.$select_data[$i]['promotion_duration_left'].'
                            </span>                    
                        </div>                
                    ';

                if ($select_data[$i]['ordered'] == 0)
                    $return['display'] .= '
                        <div class="api_height_10"></div>
                        
                    ';

                $return['display'] .= '                            
                    <div class="api_temp_btn_delete visible-xs">
                        <table border="0">
                        <tr>
                        <td class="api_temp_td_4" valign="middle" align="center">
                            <a href="javascript:void(0);" class="api_temp_a_2 '.$config_data_function['delete_class'].'" onclick="
                            var postData = {
                                \'cart_type\' : \''.$config_data_function['cart_type'].'\',
                                \'cart_type\' : \''.$config_data_function['cart_type'].'\',
                                \'cart_id\' : \''.$select_data[$i]['cart_id'].'\',
                                \'ajax_url\' : \''.base_url().'cart/api_cart_express_ajax_remove_cart\',
                            };
                            api_cart_express_remove(postData);              
                            ">
                                <li class="fa fa-trash-o api_font_size_18 api_margin_top_2"></li>
                            </a>                        
                        </td>
                        </tr>
                        </table>                    
                    </div>
                ';

                if ($select_data[$i]['qty'] <= $select_data[$i]['min_qty'])
                    $temp = 'api_temp_disabled';
                else
                    $temp = '';

                $return['display'] .= '                            
                    <div class="api_temp_btn_qty visible-xs">
                        <table width="100%" border="0">
                        <tr>
                        <td valign="middle" align="center" height="25">
                            <a href="javascript:void(0);" class="api_temp_button_sub_mobile_'.$select_data[$i]['cart_id'].' api_temp_a_3 '.$temp.'" onclick="
                            var postData = {
                                \'cart_type\' : \''.$config_data_function['cart_type'].'\',
                                \'type\' : \'sub\',
                                \'min_qty\' : \''.$select_data[$i]['min_qty'].'\',
                                \'cart_id\' : \''.$select_data[$i]['cart_id'].'\',
                                \'ajax_url\' : \''.base_url().'cart/api_cart_express_ajax_update_cart_qty\',                                  
                            };
                            api_cart_express_ajax_update_cart_qty(postData);                        
                            ">
                                <li class="fa fa-minus api_margin_top_7"></li>
                            </a>
                        </td>
                        <td class="api_padding_left_5 api_padding_right_5" valign="middle" align="center" height="25">
                            <div class="api_temp_qty_'.$select_data[$i]['cart_id'].' api_margin_top_2">
                                '.$select_data[$i]['qty'].'
                            </div>
                        </td>
                        <td valign="middle" align="center" height="25">
                            <a href="javascript:void(0);" class="api_temp_button_add_mobile_'.$select_data[$i]['cart_id'].' api_temp_a_3" onclick="
                            var postData = {
                                \'cart_type\' : \''.$config_data_function['cart_type'].'\',
                                \'type\' : \'add\',
                                \'min_qty\' : \''.$select_data[$i]['min_qty'].'\',
                                \'cart_id\' : \''.$select_data[$i]['cart_id'].'\',
                                \'ajax_url\' : \''.base_url().'cart/api_cart_express_ajax_update_cart_qty\',                                    
                            };
                            api_cart_express_ajax_update_cart_qty(postData);                        
                            ">
                                <li class="fa fa-plus api_margin_top_5"></li>
                            </a>
                        </td>
                        </tr>
                        </table>                           
                    </div>
                ';

            $return['display'] .= '   
                </div>
                </td>
                <td valign="top" align="right" class="api_temp_td_3 hidden-xs">
                    <a href="javascript:void(0);" class="api_temp_a_2 '.$config_data_function['delete_class'].'" onclick="
                    var postData = {
                        \'cart_type\' : \''.$config_data_function['cart_type'].'\',
                        \'cart_id\' : \''.$select_data[$i]['cart_id'].'\',
                        \'ajax_url\' : \''.base_url().'cart/api_cart_express_ajax_remove_cart\',
                    };
                    api_cart_express_remove(postData);              
                    ">
                        <li class="fa fa-trash-o api_font_size_24 api_padding_right_3"></li>
                    </a>
                    <div class="api_height_25"></div>
                    <table border="0">
                    <tr>
            ';
                        if ($select_data[$i]['qty'] <= $select_data[$i]['min_qty'])
                            $temp = 'api_temp_button_calculation_disable';
                        else
                            $temp = 'api_temp_button_calculation';
                $return['display'] .= '
                    <td valign="middle" align="center" width="24" height="24">
                        <div class="api_temp_button_sub_'.$select_data[$i]['cart_id'].' '.$temp.'" onclick="
                        var postData = {
                            \'cart_type\' : \''.$config_data_function['cart_type'].'\',
                            \'type\' : \'sub\',
                            \'min_qty\' : \''.$select_data[$i]['min_qty'].'\',
                            \'cart_id\' : \''.$select_data[$i]['cart_id'].'\',
                            \'ajax_url\' : \''.base_url().'cart/api_cart_express_ajax_update_cart_qty\',                                  
                        };
                        api_cart_express_ajax_update_cart_qty(postData);                        
                    ">                            
                            <i class="fa fa-minus api_font_size_12"></i>
                        </div>
                    </td>
                    <td class="api_padding_left_10 api_padding_right_10" valign="middle" align="center" height="24">
                        <span class="api_temp_qty_'.$select_data[$i]['cart_id'].'" class="api_temp_title_2">'.$select_data[$i]['qty'].'</span>
                    </td>
                ';
                $return['display'] .= '
                    <td valign="middle" align="center" width="24" height="24">
                        <div id="api_temp_button_add_'.$select_data[$i]['cart_id'].'" class="api_temp_button_calculation" onclick="
                            var postData = {
                                \'cart_type\' : \''.$config_data_function['cart_type'].'\',
                                \'type\' : \'add\',
                                \'min_qty\' : \''.$select_data[$i]['min_qty'].'\',
                                \'cart_id\' : \''.$select_data[$i]['cart_id'].'\',
                                \'ajax_url\' : \''.base_url().'cart/api_cart_express_ajax_update_cart_qty\',                                    
                            };
                            api_cart_express_ajax_update_cart_qty(postData);                        
                        ">
                            <i class="fa fa-plus api_font_size_12"></i>
                        </div>
                    </td>
                    </tr>
                    </table>
                    <div class="api_height_10"></div>
                ';

                if ($select_data[$i]['min_qty'] > 1)
                $return['display'] .= '
                    <div class="api_temp_title_1">
                        '.lang('Minimum order quantity').' '.$select_data[$i]['min_qty'].'
                    </div>
                ';

        $return['display'] .= '                            
                </td>
                </tr>
                </table>
            </div>
        ';
    }
    $return['display'] .= ' 
        <div class="api_temp_box hidden-xs">
            <a href="javascript:void(0);" onclick="
            var postData = {
                \'cart_type\' : \''.$config_data_function['cart_type'].'\',
                \'form_name\' : \'api_component_cart_express_form_cart\',
                \'checkbox_name\' : \'api_check[]\',
                \'ajax_url\' : \''.base_url().'cart/api_cart_express_ajax_remove_cart\',                    
            };
            api_cart_express_remove_selected(postData);
            " class="btn btn-danger btn-sm">
                <li class="fa fa-trash-o api_padding_right_3"></li> '.lang('Remove').'
            </a>
        </div>
    ';
    $return['display'] .= '
            </form>
        </div>
    ';
    return $return;
}

function api_cart_express_ajax_update_cart_qty() {
    $return['error'] = '';
    if ($this->session->userdata('user_id') <= 0)
        $return['error'] = 'not_logged_in';
    else {
        foreach ($_GET as $name => $value) {
            ${$name} = $value;
        }

        $qty = floatval($qty);
        if ($qty <= 0) $qty = 1;
        
        $return['status'] = 'success';
        $config_data = array(
            'cart_type' => $cart_type,
            'cart_id' => $cart_id,
        );
        $select_cart_item = $this->get_data_by_cart_id($config_data);
        

        if ($this->api_shop_setting[0]['out_of_stock'] == 1) {
            $config_data = array(
                'product_id' => $select_cart_item['id'],
                'warehouse_id' => $this->shop_settings->warehouse,
                'compare_quantity' => $qty,
            );
            $temp = $this->site->api_check_product_stock($config_data);
            if ($temp['result'] <= 0)
                $return['status'] = 'out_of_stock';
        }

        $config_data_2 = array(
            'id' => $select_cart_item['id'],
            'customer_id' => $this->session->userdata('company_id'),
        );    
        $temp5 = $this->site->api_calculate_product_price_v2($config_data_2);
        $temp_min_qty = 1; 
        if ($temp5['promotion']['price'] > 0) {
            $temp_min_qty = $temp5['promotion']['min_qty'];
            if ($qty < $temp_min_qty) {
                $qty = $temp_min_qty;
                $return['status'] = 'minimum_quantity';
            }
        }

        if ($return['status'] == 'success') {
            $select_cart_item['qty'] = $qty;
            $config_data = array(
                'cart_type' => $cart_type,
                'cart_item' => $select_cart_item,                                
            );
            $this->update_cart_item($config_data);
            $config_data = array(
                'cart_type' => $cart_type,
                'btn_back_to_cart' => 0,
                'step' => 'cart',
            );
            $temp_data = $this->config($config_data);
            $temp_cart_total = $this->view_cart_total($temp_data);
        }


        if ($qty <= $temp_min_qty)
            $btn_sub_status = 'sub_disable';
        else
            $btn_sub_status = 'sub_enable';

        $return['cart_total_display'] = $temp_cart_total['display'];
        $return['total_cart'] = count($temp_data['cart_item']);
        $return['total_cart_qty'] = $temp_data['total_cart_qty'];
        $return['btn_sub_status'] = $btn_sub_status;

    }
    
    echo json_encode($return);
}

function remove($config_data_function) {
    $config_data = array(
        'cart_type' => $config_data_function['cart_type'],
    );
    $temp_data = $this->initial_data($config_data);
        
    $k = 0;
    $temp = array();
    for ($i=0;$i<count($temp_data['cart_item']);$i++) {
        if ($temp_data['cart_item'][$i]['cart_id'] != $config_data_function['cart_id']) {
            $temp[$k] = $temp_data['cart_item'][$i];
            $k++;
        }
    }
    $temp_data['cart_item'] = $temp;
    $return = $temp;
    $temp = array(
        'json_data' => json_encode($temp_data)
    );                
    $this->db->update('sma_user_cart', $temp,"id = '".$temp_data['id']."'");

    return $return;
}

function update_cart_item($config_data_function) {
    $config_data = array(
        'cart_type' => $config_data_function['cart_type'],
    );
    $select_data = $this->initial_data($config_data);

    $k = 0;
    $temp = array();
    for ($i=0;$i<count($select_data['cart_item']);$i++) {
        if ($select_data['cart_item'][$i]['cart_id'] == $config_data_function['cart_item']['cart_id']) {
            $select_data['cart_item'][$i] = $config_data_function['cart_item'];
            break;
        }
    }
    $return = $temp;
    $temp = array(
        'json_data' => json_encode($select_data)
    );                
    $this->db->update('sma_user_cart', $temp,"id = '".$select_data['id']."'");

    return $return;
}

function update_key($config_data_function) {
    $config_data = array(
        'cart_type' => $config_data_function['cart_type'],
    );
    $select_data = $this->initial_data($config_data);

    $select_data[$config_data_function['key']] = $config_data_function['value'];
    $temp = array(
        'json_data' => json_encode($select_data)
    );                
    $this->db->update('sma_user_cart', $temp,"id = '".$select_data['id']."'");

    return $return;
}

function api_cart_express_ajax_remove_cart() {
    $return['error'] = '';
    if ($this->session->userdata('user_id') <= 0)
        $return['error'] = 'not_logged_in';
    else {
        foreach ($_GET as $name => $value) {
            ${$name} = $value;
        }
    
        $temp = explode('-',$cart_id);
        for ($i=0;$i<count($temp);$i++) {
            if ($temp[$i] != '') {
                $config_data = array(
                    'cart_type' => $cart_type,
                    'cart_id' => $temp[$i],
                );                
                $temp_2 = $this->remove($config_data);
            }   
        }
        
        $config_data = array(
            'cart_type' => $cart_type,
            'btn_back_to_cart' => 0,
            'step' => 'cart',
        );
        $temp_data = $this->config($config_data);        
        $temp_cart_total = $this->view_cart_total($temp_data);
        $return['cart_total_display'] = $temp_cart_total['display'];
        $return['total_cart'] = count($temp_data['cart_item']);
        $return['total_cart_qty'] = $temp_data['total_cart_qty'];
    }
    echo json_encode($return);
}

function view_cart_total($config_data_function) {
    $return['display'] .= '    
        <div class="api_large">
            <i class="fa fa-calculator" aria-hidden="true"></i> '.lang('Order Summary').'
        </div>

        <div class="api_height_25"></div>

        <table width="100%" border="0">
    ';
    if ($config_data_function['discount'] > 0 || $config_data_function['service_fee'] > 0)
        $temp_label = lang('total');
    else
        $temp_label = lang('Subtotal');
    $return['display'] .= '
        <tr>
        <td valign="middle">
            '.$temp_label.'
        </td>
        <td valign="middle" align="right">
            '.$this->sma->formatMoney($config_data_function['total']).'
        </td>
        </tr>
    ';

    if ($config_data_function['total_plt'] > 0) {
        $return['display'] .= '
            <tr><td colspan="2" height="15"></td></tr>
            <tr>
                <td>'.lang('Food_Total').'</td>
                <td class="text-right">'.$this->sma->formatMoney($config_data_function['total_food']).'</td>
            </tr>
        ';
        $return['display'] .= '
            <tr><td colspan="2" height="15"></td></tr>
            <tr>
                <td>'.lang('Alcohol_Total').'</td>
                <td class="text-right">'.$this->sma->formatMoney($config_data_function['total_alcohol']).'</td>
            </tr>
        '; 
    }


    if ($config_data_function['discount'] > 0) {
        $return['display'] .= '
            <tr><td colspan="2" height="15"></td></tr>
            <tr class="">
                <td>'.lang('discount').' <small>('.$config_data_function['discount_rate'].'%)</small></td>
                <td class="text-right">'.$this->sma->formatMoney($config_data_function['discount']).'</td>
            </tr>
        ';
        $return['display'] .= '
            <tr><td colspan="2" height="15"></td></tr>
            <tr class="">
                <td>'.lang('Subtotal').'</td>
                <td class="text-right">'.$this->sma->formatMoney($config_data_function['subtotal']).'</td>
            </tr>
        ';
    }

    if ($config_data_function['service_fee'] > 0) {
        $return['display'] .= '
            <tr><td colspan="2" height="15"></td></tr>
            <tr class="">
                <td>'.lang('service_charge').' <small>('.$config_data_function['service_fee_rate'].'%)</small></td>
                <td class="text-right">'.$this->sma->formatMoney($config_data_function['service_fee']).'</td>
            </tr>
        ';
        $return['display'] .= '
            <tr><td colspan="2" height="15"></td></tr>
            <tr class="">
                <td>'.lang('Subtotal').'</td>
                <td class="text-right">'.$this->sma->formatMoney($config_data_function['subtotal']).'</td>
            </tr>
        ';
    }    

    if ($config_data_function['total_vat'] > 0) {
        $return['display'] .= '
            <tr><td colspan="2" height="15"></td></tr>
            <tr class="">
                <td>'.lang('Vat').' <small>(10%)</small></td>
                <td class="text-right">'.$this->sma->formatMoney($config_data_function['total_vat']).'</td>
            </tr>
        ';
    }
    
    if ($config_data_function['total_plt'] > 0) {       
        $return['display'] .= '
            <tr><td colspan="2" height="15"></td></tr>
            <tr>
                <td>'.lang('Public Lighting Tax (PLT)').'</td>
                <td class="text-right">'.$this->sma->formatMoney($config_data_function['total_plt']).'</td>
            </tr>
        ';
    }

    $return['display'] .= '
        <tr><td colspan="2" height="15"></td></tr>
        <tr>
            <td>'.lang('Shipping').'</td>
            <td class="text-right">'.$this->sma->formatMoney($config_data_function['shipping']).'</td>
        </tr>
    ';

    $return['display'] .= '
        </table>
    ';
    $return['display'] .= '
        <div class="api_temp_total">
            <table width="100%" border="0">
                <tr class="active text-bold">
                    <td class="api_padding_top_5" valign="middle">
                        '.lang('Grand_Total').'
                    </td>
                    <td valign="middle" class="text-right">
                        <div class="api_large">
                            '.$this->sma->formatMoney($config_data_function['grand_total']).'
                        </div>
                    </td>
                </tr>
            </table>
        </div>    
    ';

    if ($config_data_function['btn_back_to_cart'] == 1) {
        $temp_1 = 'api_display_none';
        $temp_2 = '';
    }
    else {
        $temp_1 = '';
        $temp_2 = 'api_display_none';
    }

    $return['display'] .= '
        <a href="javascript:void(0);" class="btn btn-primary btn-lg btn-block btn-checkout api_temp_btn_checkout '.$temp_1.'" onclick="
            api_get_data = {
                \'slug_0\' : \'cart\',
                \'slug_1\' : \'checkout\',
            };
            var postData = {
                \'page\' : \'checkout\',
                \'get_data\' : api_get_data,
                \'cart_type\' : \''.$config_data_function['cart_type'].'\',
            };
            api_ajax_load_page(postData);      
        ">
            <i class="fa fa-credit-card api_padding_right_5"></i>
            '.lang('checkout').'
        </a>
    ';    
    
    $return['display'] .= '
        <a href="javascript:void(0);" class="btn btn-primary btn-lg btn-block btn-checkout api_temp_btn_cart '.$temp_2.'" onclick="
            api_get_data = {
                \'slug_0\' : \'cart\',
            };
            var postData = {
                \'page\' : \'cart\',
                \'get_data\' : api_get_data,
                \'cart_type\' : \''.$config_data_function['cart_type'].'\',
            };
            api_ajax_load_page(postData);    
        ">
            <i class="fa fa-reply api_padding_right_5"></i>
            '.lang('shopping_cart').'
        </a>
    ';    

    $return['display'] = '
        <div class="api_component_cart_express">
            <div class="api_temp_box api_absolute_center_wrapper">
                '.$return['display'].'
            </div>
        </div>
    ';
    return $return;
}


function api_cart_express_ajax_update_cart() {
    $return['error'] = '';
    if ($this->session->userdata('user_id') <= 0)
        $return['error'] = 'not_logged_in';
    else {
        foreach ($_GET as $name => $value) {
            ${$name} = $value;            
        }
        $config_data = array(
            'cart_type' => $cart_type,
            'key' => $field_name,
            'value' => $field_value,
        );
        $this->update_key($config_data);
    }
    echo json_encode($return);
}

function api_cart_express_ajax_company_change() {
    $return['error'] = '';
    if ($this->session->userdata('user_id') <= 0)
        $return['error'] = 'not_logged_in';
    else {
        foreach ($_GET as $name => $value) {
            ${$name} = $value;
        }
    
        if ($field_value > 0) {
            $config_data = array(
                'id' => $field_value,
                'display_name' => 0,
                'display_address' => 1,
                'display_parent' => 0,
                'display_city' => 1,
                'display_postal_code' => 1,
            );
            $temp_address = $this->site->api_display_customer_address($config_data);

            $config_data = array(
                'cart_type' => $cart_type,
                'key' => $field_name,
                'value' => $field_value,
            );
            $this->update_key($config_data); 
        }
        $return['display'] = $temp_address['display'];
    }
    echo json_encode($return);
}

function api_cart_express_ajax_update_payment_method() {
    $return['error'] = '';
    if ($this->session->userdata('user_id') <= 0)
        $return['error'] = 'not_logged_in';
    else {
        foreach ($_GET as $name => $value) {
            ${$name} = $value;
        }

        if ($field_value != '') {
            $config_data = array(
                'cart_type' => $cart_type,
                'payment_method' => $field_value,
            );
            $temp = $this->view_payment_method($config_data);
            $return['display'] .= $temp['display'];

            $config_data = array(
                'cart_type' => $cart_type,
                'key' => 'payment_method',
                'value' => $field_value,
            );
            $this->update_key($config_data);
        }
    }
    echo json_encode($return);
}

function view_payment_method($config_data_function) {
    $l = $this->api_shop_setting[0]['api_lang_key'];

    if ($this->session->userdata('company_id') > 0) {
        $config_data = array(
            'table_name' => 'sma_companies',
            'select_table' => 'sma_companies',
            'translate' => '',
            'description' => '',
            'select_condition' => "id = ".$this->session->userdata('company_id')."",
        );
        $select_customer = $this->api_helper->api_select_data_v2($config_data);
    }

    if ($config_data_function['payment_method'] == '') {
        $temp_selected_payment_method = 'cod';
        if ($select_customer[0]['payment_category'] != '' && $select_customer[0]['payment_category'] != 'cash on delivery')
            $temp_selected_payment_method = 'account_receivable';
    }
    else 
        $temp_selected_payment_method = $config_data_function['payment_method'];
        
    $return['display'] .= '
        <table width="100%" border="0">
        <tr>
        <td align="left">   
    ';
            $config_data = array(
                'table_name' => 'sma_payment_method',
                'select_table' => 'sma_payment_method',
                'translate' => 'yes',
                'description' => 'yes',
                'select_condition' => "id > 0 and status = 'enabled' order by ordering asc",
            );
            $temp_method = $this->api_helper->api_select_data_v2($config_data);
            for ($i=0;$i<count($temp_method);$i++) {
                if ($temp_selected_payment_method == $temp_method[$i]['name']) {
                    $temp_selected = 'api_temp_payment_box_selected';
                }
                else {
                    $temp_selected = '';
                }

                $return['display'] .= '    
                    <div class="api_temp_payment_box '.$temp_selected.' '.$temp.'" title="'.$temp_method[$i]['title_'.$l].'" onclick="
                    var postData = {
                        \'cart_type\' : \''.$config_data_function['cart_type'].'\',
                        \'field_name\' : \'payment_method\',
                        \'field_value\' : \''.$temp_method[$i]['name'].'\',
                        \'ajax_url\' : \''.base_url().'cart/api_cart_express_ajax_update_payment_method\',                                    
                    };
                    api_cart_express_ajax_update_payment_method(postData);
                    ">
                        <table width="100%" border="0">
                        <tr>
                        <td valign="middle" align="center">
                            <img src="'.base_url().'assets/api/image/'.$temp_method[$i]['image'].'" style="'.$temp_method[$i]['image_css'].'"/>
                            '.$temp_method[$i]['description_'.$l].'
                        </td>
                        </tr>
                        </table>
                    </div>
                ';

            }            
    $return['display'] .= '
        <div class="api_height_5 api_clear_both"></div>
        </td>
        </tr>
        </table>
    ';                    
    $return['selected_payment_method'] = $temp_selected_payment_method;

    return $return;
}

public function view_checkout($config_data_function) {
    //-get_cart_data---------------------------
        $select_data = $config_data_function;
    //-get_cart_data---------------------------
    
    //-header-----------------------------
        $temp_header .= '
            <div class="api_temp_box">
                <table width="100%" border="0">
                <tr>
                <td valign="middle">
                    <div class="api_large">
                        <i class="fa fa-credit-card" aria-hidden="true"></i> '.lang('checkout').'
                    </div>
                </td>
                <td valign="middle" align="right">
                    <a class="api_bold api_theme_link_1" href="javascript:void(0);" onclick="
                        api_get_data = {
                            \'slug_0\' : \'cart\',
                        };
                        var postData = {
                            \'page\' : \'cart\',
                            \'get_data\' : api_get_data,
                            \'cart_type\' : \''.$config_data_function['cart_type'].'\',
                        };
                        api_ajax_load_page(postData);
                    ">
                        <i class="fa fa-reply"></i>
                        &nbsp;'.lang('shopping_cart').'
                    </a>                
                </td>
                </tr>
                </table>
            </div>
        ';
    //-header-----------------------------
    
    //-select_data_company-----------------------------
        if ($this->session->userdata('company_id') > 0) {
            $config_data = array(
                'table_name' => 'sma_companies',
                'select_table' => 'sma_companies',
                'translate' => '',
                'description' => '',
                'select_condition' => "id = ".$this->session->userdata('company_id')."",
            );
            $select_customer = $this->api_helper->api_select_data_v2($config_data);

            if ($select_customer[0]['parent_id'] == 0) {
                $select_company = array();
                $select_company[0] = $select_customer[0];
                $temp = $this->site->api_select_some_fields_with_where("
                    *     
                    "
                    ,"sma_companies"
                    ,"parent_id = ".$select_customer[0]['id']." order by company asc"
                    ,"arr"
                );
                $j = 1;
                for ($i=0;$i<count($temp);$i++) {
                    $select_company[$j] = $temp[$i];
                    $j++;
                }
            }
            else {
                $select_company = array();
                $temp = $this->site->api_select_some_fields_with_where("
                    *     
                    "
                    ,"sma_companies"
                    ,"id = ".$select_customer[0]['parent_id']
                    ,"arr"
                );
                if (count($temp) > 0) {
                    $select_company[0] = $temp[0];
                    $select_company[1] = $select_customer[0];
                }
                else
                    $select_company[0] = $select_customer[0];
            }
        }
    //-select_data_company-----------------------------

    //-company----------------
        if ($this->session->userdata('company_id') > 0) {
            for ($i=0;$i<count($select_company);$i++) {
                $temp_name = $select_company[$i]['company'];
                if ($select_company[$i]['company'] == '')
                    $temp_name = $select_company[$i]['name'];
                if ($temp_name == '')
                    $temp_name = $select_company[$i]['email'];
                break;
            }
            if ($this->api_shop_setting[0]['display_company'] != 1) {
                $select_data['customer_id'] = $this->session->userdata('company_id');
                $return['display'] .= '
                    <label class="control-label" for="delivery_date">
                        <strong>'.lang('Name').'</strong>
                    </label>
                    <div class="col-md-12 api_padding_0">
                        '.$temp_name.'
                    </div>
                    <div class="api_height_20"></div>
                ';
            }
            else {
                $return['display'] .= '
                    <div class="col-md-12 api_padding_0">
                        <label class="control-label">
                            <strong class="">'.lang('Company').'</strong>
                        </label>
                    </div>
                ';
                if (count($select_company) > 0) {
                    $return['display'] .= '
                        <div class="col-md-6 api_padding_0">
                            <div class="form-group">
                                <div class="controls">                     
                    ';
                        $temp_branch_name = '';
                        $temp_address = $select_company[0]['address'];
                        for ($i=0;$i<count($select_company);$i++) {
                            if ($select_company[$i]['company'] == '' || $select_company[$i]['company'] == '-') {
                                if ($select_company[$i]['name'] != '')
                                    $select_company[$i]['company'] = $select_company[$i]['name'];
                                else
                                    $select_company[$i]['company'] = $select_company[$i]['email'];
                            }
                            $tr2[$select_company[$i]['id']] = $select_company[$i]['company'];
                        }              
                        if ($select_data['customer_id'] <= 0)
                            $temp_selected_customer = $this->session->userdata('company_id');
                        else
                            $temp_selected_customer = $select_data['customer_id'];
                
                        $return['display'] .= form_dropdown('customer_id', $tr2, $temp_selected_customer, 'class="form-control" onchange="
                            var postData = {
                                \'cart_type\' : \''.$config_data_function['cart_type'].'\',
                                \'field_name\' : \'customer_id\',
                                \'field_value\' : this.value,
                                \'ajax_url\' : \''.base_url().'cart/api_cart_express_ajax_company_change\',
                            };
                            api_cart_express_ajax_company_change(postData);      
                        "
                        ');

                    $return['display'] .= '
                                </div>
                            </div>
                        </div>
                    ';
                }
                else {
                    $return['display'] .= '
                        <div class="col-md-12 api_padding_0">
                            '.$temp_name.'
                        </div>
                    ';
                }
            }
        }
    //-company----------------

    //-address----------------
        if ($this->session->userdata('company_id') > 0) {
            $config_data = array(
                'id' => $temp_selected_customer,
                'display_name' => 0,
                'display_address' => 1,
                'display_parent' => 0,
                'display_city' => 1,
                'display_postal_code' => 1,
            );
            $temp = $this->site->api_display_customer_address($config_data);
            $return['display'] .= '
                <div class="col-md-12 api_padding_0">
                    <div class="api_height_25"></div>
                    <label class="control-label" for="delivery_date">
                        <strong>'.lang('Address').'</strong>
                    </label>
                    <div class="api_temp_address">
                        '.$temp['display'].'
                    </div>
                </div>
            ';
        }
    //-address----------------

    $return['display'] .= '
        <div class="api_clear_both api_height_25"></div>
    ';

    //-delivery_date----------------
        $return['display'] .= '
            <div class="col-md-6 api_padding_0">
                <div class="form-group">
                    <label class="control-label" for="delivery_date">
                    <strong>'.lang("delivery_date").'</strong>
                    </label>
                    <div class="controls">
        ';
                    if (date('w') != 0) {
                        if (date('w') == 6)
                            $temp = lang('Next_Monday');
                        else
                            $temp = lang('Tomorrow');                        
                        if (date('H') < 18) {
                            $tr['1'] = lang('Today').' 12:00 PM to 7:00 PM';
                            $tr['2'] = $temp.' 9:00 AM to 12:00 PM';
                            $tr['3'] = $temp.' 12:00 PM to 7:00 PM';
                            if ($select_data['delivery_date'] != '')
                                $temp_selected_delivery_date = $select_data['delivery_date'];
                            else                                            
                                $temp_selected_delivery_date = 1;
                        }
                        else {
                            $tr['2'] = $temp.' 9:00 AM to 12:00 PM';
                            $tr['3'] = $temp.' 12:00 PM to 7:00 PM';
                            if ($select_data['delivery_date'] != '' && $select_data['delivery_date'] != 1)
                                $temp_selected_delivery_date = $select_data['delivery_date'];
                            else                                            
                                $temp_selected_delivery_date = 2;
                        }
                    }
                    else {
                        $temp = lang('Tomorrow');                                 
                        $tr['2'] = $temp.' 9:00 AM to 12:00 PM';
                        $tr['3'] = $temp.' 12:00 PM to 7:00 PM';
                        if ($select_data['delivery_date'] != '' && $select_data['delivery_date'] != 1)
                            $temp_selected_delivery_date = $select_data['delivery_date'];
                        else                                            
                            $temp_selected_delivery_date = 2;                        
                    }
                    $return['display'] .= form_dropdown('delivery_date', $tr, $temp_selected_delivery_date, 'data-placeholder="'.lang("Available_delivery_date").'" onchange="
                        var postData = {
                            \'cart_type\' : \''.$config_data_function['cart_type'].'\',
                            \'field_name\' : \'delivery_date\',
                            \'field_value\' : $(this).val(),
                            \'ajax_url\' : \''.base_url().'cart/api_cart_express_ajax_update_cart\',                             
                        };
                        api_cart_express_ajax_update_cart(postData);                    
                    " class="form-control"');
        $return['display'] .= '
                    </div>
                </div>
            </div>
        ';
    //-delivery_date----------------

    $return['display'] .= '
        <div class="api_clear_both api_height_25"></div>
    ';
    
    //-payment----------------
        $config_data = array(
            'cart_type' => $config_data_function['cart_type'],
            'payment_method' => $select_data['payment_method'],
        );
        $temp = $this->view_payment_method($config_data);
        $temp_selected_payment_method = $temp['selected_payment_method'];
        $return['display'] .= '
            <strong>'.lang('payment_method').'</strong>
            <div class="api_height_10"></div>   
            <div class="api_temp_wrapper_payment_method">
                '.$temp['display'].'
            </div>     
        ';
    //-payment----------------

    $return['display'] .= '
        <div class="api_clear_both api_height_25"></div>
    ';

    //-comment----------------
        $return['display'] .= '
            <strong>'.lang('comment').'</strong>
            <div class="api_height_10"></div>   
            <div class="form-group">
                '.form_textarea('comment', $select_data['comment'], 'class="form-control" onchange="
                    var postData = {
                        \'cart_type\' : \''.$config_data_function['cart_type'].'\',
                        \'field_name\' : \'comment\',
                        \'field_value\' : $(this).val(),
                        \'ajax_url\' : \''.base_url().'cart/api_cart_express_ajax_update_cart\',   
                    };
                    api_cart_express_ajax_update_cart(postData);                
                "').'
            </div>

            <div class="api_height_20"></div>
            <div class="col-md-12 api_padding_0 text-center">
                <label class="api_border_r5 label-warning  api_padding_5 api_color_white">
                    '.lang('Please click Submit Order button to complete your order').'
                </label>
                <div>
                    <li class="fa fa-arrow-down" style="color:#ff9800;"></li>
                </div>
            </div>

        ';
    //-comment----------------

    //-button_checkout----------------
        if (!$this->Staff) {
            $temp_button_checkout .= '
                <div class="btn btn-primary btn-lg btn-block btn-checkout" onclick="
                    var postData = {
                        \'cart_type\' : \''.$config_data_function['cart_type'].'\',
                        \'ajax_url\' : \''.base_url().'cart/api_cart_express_ajax_order_insert\',
                    };
                    api_cart_express_ajax_order_insert(postData);
                ">
                    <i class="fa fa-money api_padding_right_10" aria-hidden="true"></i>'.lang('submit_order').'
                </div>
            ';                
        } 
        else {
            $temp_button_checkout .= '
                <div class="alert alert-warning margin-bottom-no">
                    '.lang('staff_not_allowed').'
                </div>
            ';
        }

        $temp_button_checkout = '
            <div class="api_temp_box">
                '.$temp_button_checkout.'
            </div>
        ';
    //-button_checkout----------------

    //-layout----------------
        $temp_open_form = shop_form_open('','class="validate" name="api_component_cart_express_form_checkout" id="api_component_cart_express_form_checkout"');

        $return['display'] = '
            <div class="api_component_cart_express">
                '.$temp_open_form.'
                '.$temp_header.'
                <div class="api_temp_box">
                    '.$return['display'].'
                    <div class="api_clear_both"></div>
                </div>
                '.$temp_button_checkout.'
                </form>
            </div>
        ';
    //-layout----------------

    //-update_cart_data---------------------------
        if ($temp = get_cookie('cart_id', TRUE)) {
            $select_data['customer_id'] = $temp_selected_customer;
            $select_data['delivery_date'] = $temp_selected_delivery_date;
            $select_data['payment_method'] = $temp_selected_payment_method;
            $temp = array(
                'json_data' => json_encode($select_data)
            );                
            $this->db->update('sma_user_cart', $temp,"id = '".$select_data['id']."'");
        }
    //-update_cart_data---------------------------

    return $return;
}

function api_cart_express_ajax_add_cart() {
    $return['error'] = '';
    if ($this->session->userdata('user_id') <= 0)
        $return['error'] = 'not_logged_in';
    else {
        foreach ($_GET as $name => $value) {
            ${$name} = $value;
        } 

        $config_data['cart_type'] = $cart_type;
        $select_data = $this->get_data($config_data);

        if (count($select_data) > 0) {
            $b = 0;
            for ($i=0;$i<count($select_data['cart_item']);$i++) {
                if ($select_data['cart_item'][$i]['id'] == $product_id) {
                    $select_data['cart_item'][$i]['qty'] = floatval($select_data['cart_item'][$i]['qty']) + floatval($qty);
                    $b = 1;
                    break;
                }
            }
            if ($b == 0) {
                $select_data['cart_item'][count($select_data['cart_item'])] = array(
                    'cart_id' => md5($product_id.microtime()),
                    'id' => $product_id,
                    'qty' => $qty,
                );
            }            
            
            $temp = array(
                'json_data' => json_encode($select_data)
            );                
            $this->db->update('sma_user_cart', $temp,"id = '".$select_data['id']."'");
        }
        else {
            $api_user_cart_id = md5($this->session->userdata('user_id').microtime());
            set_cookie('api_user_cart_id_'.$cart_type, $api_user_cart_id, 2592000);
            $data['id'] = $api_user_cart_id;
            $data['cart_item'][0] = array(
                'cart_id' => md5($product_id.microtime()),
                'id' => $product_id,
                'qty' => $qty,
            );
            $temp = array(
                'id' => $api_user_cart_id,
                'cart_type' => $cart_type,
                'user_id' => $this->session->userdata('user_id'),
                'json_data' => json_encode($data),
            );
            $this->db->insert('sma_user_cart', $temp);
        }

        $config_data = array(
            'cart_type' => $cart_type,
        );
        $temp_data = $this->initial_data($config_data);

        $return['total_cart'] = count($temp_data['cart_item']);
        $return['total_cart_qty'] = $temp_data['total_cart_qty'];
    }
    echo json_encode($return);
}

function api_cart_express_ajax_load_cart_count() {
    $return['error'] = '';
    if ($this->session->userdata('user_id') <= 0)
        $return['error'] = 'not_logged_in';
    else {
        foreach ($_GET as $name => $value) {
            ${$name} = $value;
        } 
        $config_data = array(
            'cart_type' => $cart_type,
        );
        $temp_data = $this->initial_data($config_data);

        $return['total_cart'] = count($temp_data['cart_item']);
        $return['total_cart_qty'] = $temp_data['total_cart_qty'];
    }
    echo json_encode($return);
}

function api_cart_express_ajax_order_insert() {
    $l = $this->api_shop_setting[0]['api_lang_key'];
    $return['error'] = '';
    if ($this->session->userdata('user_id') <= 0 || $this->session->userdata('group_id') != 3) {
        $return['error'] = 'not_logged_in';
        if ($this->session->userdata('user_id') > 0 && $this->session->userdata('group_id') != 3)
            $return['error'] = lang('staff_not_allowed');
    }
    else {
        foreach ($_GET as $name => $value) {
            ${$name} = $value;
        }
        if ($this->session->userdata('sale_consignment_auto_insert') != 'yes') {
            $insert_table = 'sma_sales';
            $insert_table_item = 'sma_sale_items';
            $insert_slug_0 = 'order-detail';
            $insert_page = 'order_detail';
        }
        else {
            $insert_table = 'sma_consignment';
            $insert_table_item = 'sma_consignment_items'; 
            $insert_slug_0 = 'consignment-detail';           
            $insert_page = 'consignment_detail';
        }
        
        $config_data = array(
            'cart_type' => $cart_type,
            'btn_back_to_cart' => 0,
            'step' => 'checkout',
        );
        $select_data = $this->config($config_data);

        $insert_data = $select_data;

        //$insert_data['total_items'] = 0;
        if ($insert_data['sale_status'] == '')
            $insert_data['sale_status'] = 'pending';
        if ($insert_data['payment_status'] == '')
            $insert_data['payment_status'] = 'pending';

        //-delivery_date-----------------
            $insert_data['date'] = date('Y-m-d H:i:s');
            if ($insert_data['delivery_date'] != '') {
                $temp_2 = date('w');
                if ($temp_2 == 6) {
                    $temp = date('Y-m-d');
                    $temp = date('Y-m-d', strtotime($temp. ' + 2 days'));
                }
                else {
                    $temp = new DateTime('tomorrow');
                    $temp = $temp->format('Y-m-d');
                }                                                
                if ($insert_data['delivery_date'] == 1)
                    $insert_data['delivery_date'] = date('Y-m-d').' 12:00:00';
                if ($insert_data['delivery_date'] == 2) {
                    $insert_data['delivery_date'] = $temp.' 9:00:00';
                    $insert_data['date'] =  $insert_data['delivery_date'];
                }
                if ($insert_data['delivery_date'] == 3) {
                    $insert_data['delivery_date'] = $temp.' 12:00:00';
                    $insert_data['date'] = $insert_data['delivery_date'];
                }            
            }
            $temp_add_ons['add_ons_delivery_date'] = $insert_data['delivery_date'];
        //-delivery_date-----------------

        //-customer-----------------
            if ($insert_data['customer_id'] > 0) {
                $config_data = array(
                    'table_name' => 'sma_companies',
                    'select_table' => 'sma_companies',
                    'translate' => '',
                    'description' => '',
                    'select_condition' => "id = ".$insert_data['customer_id'],
                );
                $temp = $this->api_helper->api_select_data_v2($config_data);
                if ($temp[0]['company'] != '' && $temp[0]['company'] != '-')
                    $insert_data['customer'] = $temp[0]['company'];
                else if ($temp[0]['name'] != '')
                    $insert_data['customer'] = $temp[0]['name'];
                else
                    $insert_data['customer'] = $temp[0]['email'];
            }
            $biller = $this->site->getCompanyByID($this->shop_settings->biller);
            $config_data = array(
                'id' => $insert_data['customer_id'],
                'default_warehouse' => $this->shop_settings->warehouse,
            );
            $temp_customer_warehouse = $this->site->api_get_customer_warehouse($config_data);
            if ($temp[0]['vat_no'] != '' && $temp[0]['vat_invoice_type'] != 'do') 
                $insert_data['order_tax_id'] = 2; 
            else
                $insert_data['order_tax_id'] = 1;   
        //-customer-----------------
        
        //-payment_method-----------------
            if ($insert_data['payment_method'] == 'payway_credit' || $insert_data['payment_method'] == 'payway_aba_pay')
                $insert_data['payment_method'] = 'payway'; 
        //-payment_method-----------------

        //-add_ons-----------------
            $temp_add_ons['add_ons_service_fee'] = $insert_data['service_fee'];
            $temp_add_ons['add_ons_service_fee_rate'] = $insert_data['service_fee_rate'];
            $config_data_2 = array(
                'add_ons_table_name' => 'sma_add_ons',
                'table_name' => 'sma_sales',
                'post' => $temp_add_ons,
            );
            $temp = $this->api_helper->api_add_ons_table_insert($config_data_2);
            $add_ons_sale = $temp['add_ons'];
        //-add_ons-----------------

        //-sale_items_data-----------------
            for ($i=0;$i<count($insert_data['cart_item']);$i++) {
                $config_data = array(
                    'table_name' => 'sma_products',
                    'select_table' => 'sma_products',
                    'translate' => '',
                    'description' => '',
                    'select_condition' => "id = ".$insert_data['cart_item'][$i]['id'],
                );
                $temp = $this->api_helper->api_select_data_v2($config_data);
                $unit = $this->site->getUnitByID($temp[0]['unit']);

                $temp_discount_rate = $insert_data['cart_item'][$i]['discount_rate'];
                if ($temp_discount_rate > 0)
                    $temp_discount_rate = $temp_discount_rate.'%';
                $temp_data = [
                    'product_id' => $insert_data['cart_item'][$i]['id'],
                    'product_code' => $temp[0]['code'],
                    'product_name' => $temp[0]['name'],
                    'product_type' => $temp[0]['type'],
                    'option_id' => 0,
                    'net_unit_price' => $insert_data['cart_item'][$i]['price'],
                    'unit_price' => $insert_data['cart_item'][$i]['price'],
                    'real_unit_price' => $insert_data['cart_item'][$i]['price_original'],
                    'quantity' => $insert_data['cart_item'][$i]['qty'],
                    'discount' => $temp_discount_rate,
                    'item_discount' => $insert_data['cart_item'][$i]['discount_price'],
                    'product_unit_id' => $unit ? $unit->id : NULL,
                    'product_unit_code' => $unit ? $unit->code : NULL,
                    'unit_quantity' => $insert_data['cart_item'][$i]['qty'],
                    'warehouse_id' => $temp_customer_warehouse['warehouse_id'],
                    'item_tax' => $insert_data['cart_item'][$i]['plt'],
                    'tax_rate_id' => 0,
                    'subtotal' => $insert_data['cart_item'][$i]['price'] * $insert_data['cart_item'][$i]['qty'],
                    'serial_no' => NULL,
                ];
                $sale_item_data[] = $temp_data;
            }
        //-sale_items_data-----------------

        //-sale_data-----------------
            $temp_order_discount_id = $insert_data['discount_rate'];
            if ($temp_order_discount_id > 0)
                $temp_order_discount_id = $temp_order_discount_id.'%';
            $sale_data = [
                'date' => $insert_data['date'],
                'customer_id' => $insert_data['customer_id'],
                'customer' => $insert_data['customer'],
                'biller_id' => $biller->id,
                'biller' => ($biller->company && $biller->company != '-' ? $biller->company : $biller->name),
                'warehouse_id' => $temp_customer_warehouse['warehouse_id'],
                'note' => $this->db->escape_str($insert_data['comment']),
                'total' => $insert_data['total'],
                'product_tax' => $insert_data['total_plt'],
                'order_tax_id' => $insert_data['order_tax_id'],
                'order_tax' => $insert_data['total_vat'],
                'total_tax' => $insert_data['total_vat'],
                'shipping' => $insert_data['shipping'],
                'order_discount_id' => $temp_order_discount_id,
                'total_discount' => $insert_data['discount'],
                'order_discount' => $insert_data['discount'],
                'grand_total' => $insert_data['grand_total'],
                'total_items' => $insert_data['total_cart_qty'],
                'sale_status' => $insert_data['sale_status'],
                'payment_status' => $insert_data['payment_status'],
                'created_by' => $this->session->userdata('user_id') ? $this->session->userdata('user_id') : NULL,
                'shop' => 1,
                'hash' => hash('sha256', microtime() . mt_rand()),
                'payment_method' => $insert_data['payment_method'],
                'add_ons' => $add_ons_sale,
            ];
        //-sale_data-----------------

        //-insert_sale-----------------
            $this->db->insert($insert_table, $sale_data);
            $sale_id = $this->db->insert_id();

            if ($insert_table == 'sma_sales') {
                $config_data = array(
                    'id' => $sale_id,
                );            
                $this->site->api_calculate_sale_supplier($config_data);
            }
            //$this->cart->destroy();
        //-insert_sale-----------------

        //-insert_item-----------------
            for ($i=0;$i<count($sale_item_data);$i++) {
                $sale_item_data[$i]['sale_id'] = $sale_id;
                $this->db->insert($insert_table_item, $sale_item_data[$i]);
            }
        //-insert_sale-----------------

        //-send_mail----------------------
            if ($insert_table == 'sma_sales') {
                $this->order_received($sale_id);
                //$this->api_order_line_notification($sale_id);
            }
            //$this->session->set_userdata('api_order_send_mail','send');
                
            if ($this->session->userdata('api_order_send_mail') != '') {
                if ($this->session->userdata('api_order_send_mail') == 'send') {
                    $return['script'] = base_url().'assets/api/page/order_detail/js/script_order_mail_success.js?v='.$this->api_shop_setting[0]['version'];
                }
                else {
                    $return['script'] = base_url().'assets/api/page/order_detail/js/script_order_mail_fail.js?v='.$this->api_shop_setting[0]['version'];
                }
            }
        //-send_mail----------------------

        $temp = array(
            'slug_0' => $insert_slug_0,
            'slug_1' => $sale_id,
        );
        $return['post_data'] = array(
            'page' => $insert_page,
            'get_data' => $temp,
        );
    }
    echo json_encode($return);
}

public function getOrder($clause) {
    if ($this->loggedIn) {
        $this->db->order_by('id desc');
        $sale = $this->db->get_where('sales', ['id' => $clause['id']], 1)->row();
        return ($sale->customer_id == $this->session->userdata('company_id')) ? $sale : FALSE;
    } elseif(!empty($clause['hash'])) {
        return $this->db->get_where('sales', $clause, 1)->row();
    }
    return FALSE;
}

public function order_received($id = null, $hash = null) {
    if ($inv = $this->getOrder(['id' => $id, 'hash' => $hash])) {
        $user = $inv->created_by ? $this->site->getUser($inv->created_by) : NULL;
        $customer = $this->site->getCompanyByID_v2($inv->customer_id);
        $biller = $this->site->getCompanyByID($inv->biller_id);
        $this->load->library('parser');

        $sale_id = $id;
        $config_data = array(
            'table_name' => 'sma_sales',
            'select_table' => 'sma_sales',
            'select_condition' => "id = ".$id,
        );
        $select_data = $this->site->api_select_data_v2($config_data);
        $temp = '';
        $temp_branch_post = $select_data[0]['company_branch'];

        $temp_2 = $this->site->api_select_some_fields_with_where("
            *
            "
            ,"sma_companies"
            ,"id = ".$select_data[0]['customer_id']
            ,"arr"
        );
        $temp_3 = array();
        if ($temp_2[0]['parent_id'] > 0) {
            $temp_3 = $this->site->api_select_some_fields_with_where("
                *
                "
                ,"sma_companies"
                ,"id = ".$temp_2[0]['parent_id']
                ,"arr"
            );
        }
        $temp = '<br>Company: '.$select_data[0]['customer'];
        if (count($temp_3) > 0)
            $temp .= '<br>Parent Company: '.$temp_3[0]['company'];

        $temp2 = '';
        if ($select_data[0]['delivery_date'] != '') {
            $temp_3 = explode(' ', $select_data[0]['delivery_date']);
            if ($temp_3[1] == '9:00:00')
                $temp_4 = '9:00 AM to 12:00 AM';
            else
                $temp_4 = '12:00 PM to 7:00 PM';

            $temp2 .= '<br>Delivery Date: '.$temp_3[0].' '.$temp_4;
            
        }
        $temp_item = $this->site->api_select_some_fields_with_where("*
            "
            ,"sma_sale_items"
            ,"sale_id = ".$sale_id." order by id asc"
            ,"arr"
        );
        if ($data['payment_method'] == 'account_receivable')
            $temp_method = 'account_payable';
        elseif ($data['payment_method'] == 'cod')
            $temp_method = 'cash on delivery';
        else
            $temp_method = $select_data[0]['payment_method'];

        $temp_method = str_replace('_',' ',$temp_method);
        $temp_method = ucfirst($temp_method);

        $temp_method = preg_replace('/[^a-zA-Z0-9_. ()+]/', '-', $temp_method);
        if ($temp_method == 'Cash on delivery' || $temp_method == 'Cod') $temp_method = 'COD';

        $temp3 = '<br>Payment Method: '.$temp_method;

        if ($this->api_shop_setting[0]['product_import_date'] == 1 && base_url() == 'https://air.phsarjapan.com/')
            $temp3 .= '<br>Import Date: '.$select_data[0]['product_import_date'];

        $customer->company = preg_replace('/[^a-zA-Z0-9_. ()+]/', '-', $customer->company);
        $temp_display_2 = 'Order ID: '.$sale_id.$temp.$temp2.$temp3.'<br><br>';                   
        $temp_display = '';
        $j = 1;
        $temp_total_alcohol = 0;
        $temp_total_food = 0;
        for ($i=0;$i<count($temp_item);$i++) {
            if ($temp_item[$i]['item_tax'] > 0)
                $temp_total_alcohol += $temp_item[$i]['subtotal'];
            else
                $temp_total_food += $temp_item[$i]['subtotal'];                        
            $temp_item[$i]['product_name'] = preg_replace('/[^a-zA-Z0-9_. ()+]/', '-', $temp_item[$i]['product_name']);
            $temp_display .= $j.'. '.$temp_item[$i]['product_name'].', Qty: '.number_format($temp_item[$i]['quantity'],2).'<br>';
            $j++;
        }                    
        $temp_display_2 = $temp_display_2.$temp_display;
        if ($select_data[0]['order_tax'] > 0) {
            $temp_display_2 .= '<br>Total: '.$this->sma->formatMoney($select_data[0]['total']).'<br>';
            if ($select_data[0]['product_tax'] > 0) {
                $temp_display_2 .= 'Foot Total: '.$this->sma->formatMoney($temp_total_food).'<br>';
                $temp_display_2 .= 'Alcohol Total: '.$this->sma->formatMoney($temp_total_alcohol).'<br>';                                                        
                $temp_display_2 .= 'Public Lighting Tax (PLT): '.$this->sma->formatMoney($select_data[0]['product_tax']).'<br>';
                $temp_display_2 .= 'Subtotal: '.$this->sma->formatMoney($select_data[0]['total'] + $select_data[0]['product_tax']).'<br>';                            
            }                        
            $temp_display_2 .= 'Vat 10: '.$this->sma->formatMoney($select_data[0]['order_tax']);
        }
        $temp_display_2 .= '<br>Grand Total: '.$this->sma->formatMoney($select_data[0]['grand_total']);

        if ($select_data[0]['note'] != '') {
            $temp_note = preg_replace("/[\/\&%#\$]/", "", $select_data[0]['note']);
            $temp_note = preg_replace("/[\"\']/", "", $temp_note);
            $temp_note = str_replace('\r', '', $temp_note);                        
            $temp_note = str_replace('\n', '<br>', $temp_note);                        
            $temp_display_2 .= '<br><br><br>Note:<br>'.$temp_note;
        }
        
        $this->load->model('pay_model');
        $paypal = $this->pay_model->getPaypalSettings();
        $skrill = $this->pay_model->getSkrillSettings();
        $btn_code = '<div id="payment_buttons" class="text-center margin010">';
        if (!empty($this->shop_settings->bank_details)) {
            echo '<div style="width:100%;">'.$this->shop_settings->bank_details.'</div><hr class="divider or">';
        }

        if (base_url() == 'https://order.daishintc.com/')
            $to = 'daishinorder@gmail.com';
        elseif (base_url() == 'https://phsarjapan.com/' || base_url() == 'https://air.phsarjapan.com/')
            $to = 'terashida1515@gmail.com';
        else
            $to = 'apipeapi@gmail.com';

        if (base_url() == 'https://order.daishintc.com/')
            $subject = 'Daishintc Order #'.$id;
        if (base_url() == 'https://dev-invoice.camboinfo.com/' || base_url() == 'https://demo.phsarjapan.com/')
            $subject = 'Daishintc Order #'.$id;             
        if (base_url() == 'https://phsarjapan.com/' || base_url() == 'https://air.phsarjapan.com/')
            $subject = 'Japan Shop Order #'.$id;

        $message = $temp_display_2;                
        $message = str_replace('&', 'and', $message);

        $attachment = '';
        $cc = '';
        $bcc = '';
        if (!is_int(strpos($_SERVER["HTTP_HOST"],"localhost")))
            if ($this->sma->send_email($to, $subject, $message, $this->Settings->default_email, null, $attachment, $cc, $bcc)) {
            }

        $parse_data = array(
            'order_id' => $inv->id,
            'contact_person' => $customer->name,
            'company' => $customer->company && $customer->company != '-' ? '('.$customer->company.')' : '',
            'order_link' => shop_url('orders/'.$id.'/'.($this->loggedIn ? '' : $data['hash'])),
            'site_link' => base_url(),
            'site_name' => $this->Settings->site_name,
            'logo' => '<img src="' . base_url() . 'assets/uploads/logos/' . $this->shop_settings->logo . '" />',
        );
        $msg = file_get_contents('themes/' . $this->Settings->theme . '/admin/views/email_templates/sale.html');
        $message = $msg;
        //$message = $this->parser->parse_string($msg, $parse_data);

        //$attachment = $this->api_order_attachment($id);
        $sent = $error = $cc = $bcc = false;
        $cc = '';
        $bcc = '';
        try {                
            if (!is_int(strpos($_SERVER["HTTP_HOST"],"localhost"))) {
                if ($this->sma->send_email(($user ? $user->email : $customer->email), $subject, $message, $this->Settings->default_email, null, $attachment, $cc, $bcc)) {
                    delete_files($attachment);
                    $this->session->set_userdata('api_order_send_mail','send');
                }
            }
        } catch (Exception $e) {
            $error = $e->getMessage();
            $this->session->set_userdata('api_order_send_mail', $error);
        }                
    }
}

public function api_order_line_notification($id) {
    if ($id > 0) {
        $config_data = array(
            'table_name' => 'sma_sales',
            'select_table' => 'sma_sales',
            'translate' => '',
            'select_condition' => "id = ".$id." and created_by = ".$this->session->userdata('user_id'),
        );
        $select_data = $this->site->api_select_data_v2($config_data);
        if (count($select_data) > 0) {

            $temp_2 = $this->site->api_select_some_fields_with_where("
                *
                "
                ,"sma_companies"
                ,"id = ".$select_data[0]['customer_id']
                ,"arr"
            );
            $temp_3 = array();
            if ($temp_2[0]['parent_id'] > 0) {
                $temp_3 = $this->site->api_select_some_fields_with_where("
                    *
                    "
                    ,"sma_companies"
                    ,"id = ".$temp_2[0]['parent_id']
                    ,"arr"
                );
            }

            $temp = '\nCompany: '.$select_data[0]['customer'];
            if (count($temp_3) > 0)
                $temp .= '\nParent Company: '.$temp_3[0]['company'];

            $temp2 = '';
            if ($select_data[0]['delivery_date'] != '') {
                $temp_3 = explode(' ', $select_data[0]['delivery_date']);
                if ($temp_3[1] == '9:00:00')
                    $temp_4 = '9:00 AM to 12:00 AM';
                else
                    $temp_4 = '12:00 PM to 7:00 PM';

                $temp2 .= '\nDelivery Date: '.$temp_3[0].' '.$temp_4;
                
            }
            $temp_item = $this->site->api_select_some_fields_with_where("*
                "
                ,"sma_sale_items"
                ,"sale_id = ".$id." order by id asc"
                ,"arr"
            );                

            if ($select_data[0]['payment_method'] == 'account_receivable')
                $temp_method = 'account_payable';
            elseif ($select_data[0]['payment_method'] == 'cod')
                $temp_method = 'cash on delivery';
            else
                $temp_method = $select_data[0]['payment_method'];

            $temp_method = str_replace('_',' ',$temp_method);
            $temp_method = ucfirst($temp_method);
            if ($temp_method == 'Cash on delivery') $temp_method = 'COD';

            $temp3 = '\nPayment Method: '.$temp_method;

            $config_data = array(
                'table_name' => 'sma_shop_settings',
                'select_table' => 'sma_shop_settings',
                'translate' => '',
                'select_condition' => "shop_id = 1",
            );
            $this->api_shop_setting = $this->site->api_select_data_v2($config_data);                 
            if ($this->api_shop_setting[0]['product_import_date'] == 1 && base_url() == 'https://air.phsarjapan.com/')
                $temp3 .= '\nImport Date: '.$select_data[0]['product_import_date'];

            $temp_display_2 = 'Order ID: '.$id.$temp.$temp2.$temp3.'\n\n';                   
            $temp_display = '';
            $j = 1;

            $temp_total_alcohol = 0;
            $temp_total_food = 0;                    
            for ($i=0;$i<count($temp_item);$i++) {
                if ($temp_item[$i]['item_tax'] > 0)
                    $temp_total_alcohol += $temp_item[$i]['subtotal'];
                else
                    $temp_total_food += $temp_item[$i]['subtotal'];
                $temp_display .= $j.'. '.$temp_item[$i]['product_name'].', Qty: '.number_format($temp_item[$i]['quantity'],2).'\n';
                $j++;
            }                    
            $temp_display_2 = $temp_display_2.$temp_display;
            if ($select_data[0]['order_tax'] > 0) {
                $temp_display_2 .= '\nTotal: '.$this->sma->formatMoney($select_data[0]['total']).'\n';
                if ($data['product_tax'] > 0) {
                    $temp_display_2 .= 'Food Total: '.$this->sma->formatMoney($temp_total_food).'\n';            
                    $temp_display_2 .= 'Alcohol Total: '.$this->sma->formatMoney($temp_total_alcohol).'\n';                                               
                    $temp_display_2 .= 'Public Lighting Tax (PLT): '.$this->sma->formatMoney($select_data[0]['product_tax']).'\n';
                    $temp_display_2 .= 'Subtotal: '.$this->sma->formatMoney($select_data[0]['total'] + $select_data[0]['product_tax']).'\n';                            
                }
                $temp_display_2 .= 'Vat 10%: '.$this->sma->formatMoney($select_data[0]['order_tax']).'';
            }
            $temp_display_2 .= '\nGrand Total: '.$this->sma->formatMoney($select_data[0]['grand_total']);

            if ($select_data[0]['note'] != '') {
                $temp_note = preg_replace("/[\/\&%#\$]/", "", $select_data[0]['note']);
                $temp_note = preg_replace("/[\"\']/", "", $temp_note);                        
                $temp_display_2 .= '\n\n\nNote:\n'.$temp_note;
            }

            if (base_url() == 'https://order.daishintc.com/') {
                $line_message = '
                    curl -v -X POST https://api.line.me/v2/bot/message/broadcast \
                    -H \'Content-Type:application/json\' \
                    -H \'Authorization: Bearer {FNtBe9JBGzmXbvr1dp5ODw1xdofgqtOC7KHfVosqKnRqA+9tal1O8tl3YjYZNJcamhDtZgvA0kJuHcf0LpA8q0vwKAfwzH8KVvVn5podwiWijuCl9NLeoUCp2Cyh6bDHpcuDDswGQml6UO5ICg1MRQdB04t89/1O/w1cDnyilFU=}\' \
                    -d \'{
                    "messages":[
                        {
                            "type":"text",
                            "text":"'.$temp_display_2.'"
                        }
                    ]
                    }\'
                ';
            }
            if (base_url() == 'https://dev-invoice.camboinfo.com/' || base_url() == 'https://demo.phsarjapan.com/') {
                $line_message = '
                    curl -v -X POST https://api.line.me/v2/bot/message/broadcast \
                    -H \'Content-Type:application/json\' \
                    -H \'Authorization: Bearer {UNTVh6YcE/XXoEOJ1lSrGjuP/ftpl1QsXvAggeidgD014fTA5KooVZ5FU9UFY7eZ0oS/ajspc+pBxzSEnBfJcnAmK+yE6nsgru8g58sjg7BzWV+mZawRtaQ7b1skK7TgefvFtxUCgD0etJD+jcFdLwdB04t89/1O/w1cDnyilFU=}\' \
                    -d \'{
                    "messages":[
                        {
                            "type":"text",
                            "text":"'.$temp_display_2.'"
                        }
                    ]
                    }\'
                ';
            }
            if (base_url() == 'https://phsarjapan.com/' || base_url() == 'https://air.phsarjapan.com/') {
                $line_message = '
                    curl -v -X POST https://api.line.me/v2/bot/message/broadcast \
                    -H \'Content-Type:application/json\' \
                    -H \'Authorization: Bearer {8gM7JhLa/RbLgM1xgY5dwCslVKkzARkRHEE6xIw+VQ2dYlg/1oGazuHkuJpR1hc/Xl1IbFRkx76kP3dE3aA1hKDsuc5w+SrKPtDvkW21CZPkw0IqBHuLSsZCwAFWvZ0QaIXoy96Ip8ivO+oWBTw6lAdB04t89/1O/w1cDnyilFU=}\' \
                    -d \'{
                        "messages":[
                            {
                                "type":"text",
                                "text":"'.$temp_display_2.'"
                            }
                        ]
                    }\'
                ';
            }
            shell_exec($line_message);         

        }
    }        
}


function view_product_list($config_data_function){
    $return = array();

    //-temp_display_product_list----------------
        $config_data = array(
            'table_name' => 'sma_categories',
            'select_table' => 'sma_categories',
            'translate' => '',
            'description' => '',
            'select_condition' => 'id order by CAST(order_number AS UNSIGNED) asc',
        );
        $temp_select_data = $this->api_helper->api_select_data_v2($config_data);
        for ($j=0;$j<count($temp_select_data);$j++) {
            $b = 0;
            if ($config_data_function['category_id'] == '')
                $b = 1;

            if ($config_data_function['category_id'] != '' && $config_data_function['category_id'] != $temp_select_data[$j]['id'])
                $b = 0;
            else
                $b = 1;
            
            if ($b == 1) {
                $temp_display_product_list .= '<div class="api_temp_title_category">'.$temp_select_data[$j]['name'].'</div>';
                
                $config_data = array(
                    'table_name' => 'sma_products',
                    'select_table' => 'sma_products',
                    'translate' => 'yes',
                    'description' => '',
                    'select_condition' => 'category_id = '.$temp_select_data[$j]['id'],
                );
                $select_data = $this->api_helper->api_select_data_v2($config_data);
                $config_data = array(
                    'select_data' => $select_data,
                );
                $initial_data = $this->api_initial_data->restaurant_menu_list($config_data);
                
                $config_data = array(
                    'select_data' => $initial_data ,
                    'table_id' => $config_data_function['table_id'],
                );
                $temp = $this->api_view_as->restaurant_menu_list($config_data);

                $temp_display_product_list .= $temp['display'];
                
            }

        }
    //-temp_display_product_list----------------

    //-temp_select_category----------------
        $temp_select_category .= '    
            <div class="form-group api_temp_main_category hidden-xs"> 
                <select name="" id="api_select_category" class="form-control api_temp_height_v2" href="javascript:void(0);" onchange="
                    var postData = {
                        \' category_id\' : $(\'#api_select_category\').val(),
                        \' table_id\' : \''.$config_data_function['table_id'].'\',
                        \'ajax_url\' : \''.base_url().'admin/restaurant/api_ajax_load_menu\',
                    };
                    api_ajax_load_menu(postData);
                ">
                    <option value="">All Categories</option>
        ';
                    $config_data = array(
                        'table_name' => 'sma_categories',
                        'select_table' => 'sma_categories',
                        'translate' => '',
                        'description' => '',
                        'select_condition' => 'id order by CAST(order_number AS UNSIGNED) asc',
                    );
                    $temp_select_data = $this->api_helper->api_select_data_v2($config_data);
                    for ($j=0;$j<count($temp_select_data);$j++) {
                        if ($config_data_function['category_id'] == $temp_select_data[$j]['id'])
                            $temp = 'selected';
                        else
                            $temp = '';   
                        $temp_select_category .= '                    
                            <option value="'.$temp_select_data[$j]['id'].'" '.$temp.'>'.$temp_select_data[$j]['name'].'</option>                   
                        ';   
                    }
        $temp_select_category .= '                
                </select>   
            </div>
                    
        ';
    //-temp_select_category----------------

    //-temp_display_search_box----------------
        $temp_display_search_box .= '
            <div class="api_temp_select_search hidden-xs">
                <div class="col-md-6">
                    <div class="form-group">
                        <input name="search" type="text" class="form-control api_temp_height" id="product-search" value="" aria-label="Search..." onkeydown="" placeholder="Search..." style="height: 40px; margin-top:15px">
                    </div>
                </div>
                <div class="col-md-6 api_bg_color">
                    '.$temp_select_category.'
                </div>
            </div>    
        ';
    //-temp_display_search_box----------------
    
    //-temp_display_main_category----------------

        $temp_display_main_category .= '
            <div class="api_temp_category">
                <table border="0">
                <tr>
        ';
        

        if ($config_data_function['category_id'] == '')
            $temp = 'selected';
        else
            $temp = '';    
        $temp_display_main_category .= '       
            <td class="api_padding_10" style="white-space: nowrap;">
                <a class="api_temp_category '.$temp.'" href="javascript:void(0);" onclick="
                    var postData = {
                        \' category_id\' : \'\',
                        \' table_id\' : \''.$config_data_function['table_id'].'\',
                        \'ajax_url\' : \''.base_url().'admin/restaurant/api_ajax_load_menu_mobile\',
                    };
                    api_ajax_load_menu_mobile(postData);
                " >
                    All Categories 
                </a>    
            </td>    
        ';
            $config_data = array(
                'table_name' => 'sma_categories',
                'select_table' => 'sma_categories',
                'translate' => '',
                'description' => '',
                'select_condition' => 'id order by CAST(order_number AS UNSIGNED) asc',
            );
            $temp_select_data_2 = $this->api_helper->api_select_data_v2($config_data);
            
            for ($k=0;$k<count($temp_select_data_2);$k++) {
                if ($config_data_function['category_id'] == $temp_select_data_2[$k]['id'])
                    $temp = 'selected';
                else
                    $temp = '';    
                     
                $temp_display_main_category .= '
               
                    <td class="api_padding_10" style="white-space: nowrap;" >
                        <a class="api_temp_category '.$temp.'" href="javascript:void(0);" onclick="
                            var postData = {
                                \' category_id\' : \''.$temp_select_data_2[$k]['id'].'\',
                                \' table_id\' : \''.$config_data_function['table_id'].'\',
                                \'ajax_url\' : \''.base_url().'admin/restaurant/api_ajax_load_menu_mobile\',
                            };
                            api_ajax_load_menu_mobile(postData);
                        " >
                            '.$temp_select_data_2[$k]['name'].'
                        </a>
                    </td>
                ';
            }
        $temp_display_main_category .= '
                </tr>
                </table>
            </div>
        ';  
     
    //-temp_display_main_category----------------

    //-layout-----------------------
        $return['display'] = '
            <div class="api_temp_menu">
                <div class="api_component_cart_express">

        ';
        
        $config_data = array(
            'table_name' => 'sma_restaurant',
            'select_table' => 'sma_restaurant',
            'translate' => '',
            'select_condition' => "id = ".$config_data_function['table_id'],
        );
        $temp_display_table = $this->site->api_select_data_v2($config_data);
        $return['display'] .= '       
                    <div class="api_temp_text_menu" align="center">
                        Table  '.$temp_display_table[0]['table_name'].'
                    </div> 
                    '.$temp_display_search_box.'
                    '.$temp_display_main_category.'

                    <div class="api_padding_left_10 api_padding_right_10">     
                        '.$temp_display_product_list.' 
                    </div>                      
                </div>
            </div>
        ';
    //-layout-----------------------
                                 
    return $return;
}

function get_quantity_by_product_id($config_data_function){
    $select_data = $config_data_function['select_data'];
    $temp_qty = 0;
    for ($i=0;$i<count($select_data['cart_item']);$i++) {
        if ($config_data_function['product_id'] == $select_data['cart_item'][$i]['id']) {
            $temp_qty = $select_data['cart_item'][$i]['qty'];
            break;
        }
    }    
    $return = $temp_qty;
    return $return;
}

}


