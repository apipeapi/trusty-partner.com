function click_table(){
    $('#api_temp_menu').hide();
    $('#api_temp_table_list').show();
}
function click_menu(){
    $('#api_temp_menu').show();
    $('#api_temp_table_list').hide();
}


function api_ajax_load_menu(postData){
    $.ajax({
        type: 'GET',
        url:postData['ajax_url'],
        dataType: "json",
        data:postData, 
        success: function (result) {
            // var myWindow = window.open("", "MsgWindow", "width=700, height=400");
            // myWindow.document.write(result);   
            // console.log(result.test);
            if (result.error == '') {
                $('.api_page_restaurant .api_temp_wrapper_menu').fadeOut(api_fade_speed, function() {
                    $('.api_temp_table_1').removeClass('selected');
                    $('.api_temp_table_id_' + postData['table_id']).addClass('selected');

                    $('.api_page_restaurant .api_temp_wrapper_menu').html(result.display);
                    $('.api_page_restaurant .api_temp_wrapper_total').html(result.display_total);

                    $('.api_restaurant_top_menu_icons').html(result.display_top_menu_icons);

                    if (result.page_url != '')
                        history.pushState('','',result.page_url);
                    $(".api_page_restaurant .api_temp_wrapper_menu").fadeIn(api_fade_speed);
                });
            }
            else if (result.error == 'not_logged_in')
                window.location = site.base_url + 'login';    

        }
    });
}
function api_ajax_load_menu_mobile(postData){
    $.ajax({
        type: 'GET',
        url:postData['ajax_url'],
        dataType: "json",
        data:postData,
        error: function (jqXHR, textStatus, errorThrown) {
            var message = errorThrown;
            if (jqXHR.responseText !== null && jqXHR.responseText !== 'undefined' && jqXHR.responseText !== '') {
                message = jqXHR.responseText;
            }
            console.log(message);
        },        
        success: function (result) {
            // var myWindow = window.open("", "MsgWindow", "width=700, height=400");
            // myWindow.document.write(result);   
            // console.log(result.test);
            if (result.error == '') {                
                $('.api_page_restaurant .api_temp_wrapper_mobile').fadeOut(api_fade_speed, function() {
                    $('.api_temp_table_1').removeClass('selected');
                    $('.api_temp_table_id_' + postData['table_id']).addClass('selected');
                    
                    $('.api_page_restaurant .api_temp_wrapper_mobile').html(result.display);
                    $('.api_page_restaurant .api_temp_footer_total_mobile').html(result.display_total_mobile);

                    $('.top_header_menu_icon').html(result.display_top_header_menu_icon_mobile);

                    if (result.page_url != '')
                        history.pushState('','',result.page_url);
                    $(".api_page_restaurant .api_temp_wrapper_mobile").fadeIn(api_fade_speed);
                });
            }
            else if (result.error == 'not_logged_in')
                window.location = site.base_url + 'login';    

        }
    });
}

function api_ajax_load_view_detail(postData){
    $.ajax({
        type: 'GET',
        url:postData['ajax_url'],
        dataType: "json",
        data:postData, 
        success: function (result) {
            // var myWindow = window.open("", "MsgWindow", "width=700, height=400");
            // myWindow.document.write(result);   
            // console.log(result);
            $('#api_temp_table').remove('.api_bg_ready_status_selected');
            if (result.error == '') {
                $('.api_page_restaurant .api_temp_wrapper_menu').fadeOut(api_fade_speed, function() {
                    $('.api_page_restaurant .api_temp_wrapper_menu').html(result.display); 
                    $('.api_page_restaurant .api_temp_wrapper_total').html(result.display_total);                   
                    if (result.page_url != '')
                        history.pushState('','',result.page_url);
                    $(".api_page_restaurant .api_temp_wrapper_menu").fadeIn(api_fade_speed);
                });
            }
            else if (result.error == 'not_logged_in')
                window.location = site.base_url + 'login';    

        }
    });
}
function api_ajax_load_view_detail_mobile(postData){
    $.ajax({
        type: 'GET',
        url:postData['ajax_url'],
        dataType: "json",
        data:postData, 
        success: function (result) {
            // var myWindow = window.open("", "MsgWindow", "width=700, height=400");
            // myWindow.document.write(result);   
            // console.log(result);
            $('#api_temp_table').remove('.api_bg_ready_status_selected');
            if (result.error == '') {
                $('.api_page_restaurant .api_temp_wrapper_mobile').fadeOut(api_fade_speed, function() {
                    $('.api_page_restaurant .api_temp_wrapper_mobile').html(result.display); 
                    $('.api_page_restaurant .api_temp_footer_total_mobile').html(result.display_total_mobile);
                    
                    $('.top_header_menu_icon').html(result.display_top_header_menu_icon_mobile);
                    
                    if (result.page_url != '')
                        history.pushState('','',result.page_url);
                    $(".api_page_restaurant .api_temp_wrapper_mobile").fadeIn(api_fade_speed);
                });
            }
            else if (result.error == 'not_logged_in')
                window.location = site.base_url + 'login';    

        }
    });
}

function api_ajax_load_payment_detail(postData){
    $.ajax({
        type: 'GET',
        url:postData['ajax_url'],
        dataType: "json",
        data:postData, 
        success: function (result) {
            // var myWindow = window.open("", "MsgWindow", "width=700, height=400");
            // myWindow.document.write(result);   
            // console.log(result);
            if (result.error == '') {
                $('.api_page_restaurant .api_temp_wrapper_table').fadeOut(api_fade_speed, function() {
                    $('.api_page_restaurant .api_temp_wrapper_table').html(result.display);                  
                    $('.api_page_restaurant .api_temp_wrapper_status').html(result.display_status);                  
                    if (result.page_url != '')
                        history.pushState('','',result.page_url);
                    $(".api_page_restaurant .api_temp_wrapper_table").fadeIn(api_fade_speed);
                });
            }
            else if (result.error == 'not_logged_in')
                window.location = site.base_url + 'login';    

        }
    });
}
function api_ajax_load_payment_detail_mobile(postData){
    $.ajax({
        type: 'GET',
        url:postData['ajax_url'],
        dataType: "json",
        data:postData, 
        success: function (result) {
            // var myWindow = window.open("", "MsgWindow", "width=700, height=400");
            // myWindow.document.write(result);   
            // console.log(result);
            if (result.error == '') {
                $('.api_page_restaurant .api_temp_wrapper_mobile').fadeOut(api_fade_speed, function() {
                    $('.api_page_restaurant .api_temp_wrapper_mobile').html(result.display);                  
                    $('.api_page_restaurant .api_temp_wrapper_status').html(result.display_status);   
                    
                    $('.top_header_menu_icon').html(result.display_top_header_menu_icon_mobile);

                    $('.api_page_restaurant .api_temp_footer_total_mobile').html(result.display_payment_submit);
                    
                    
                    if (result.page_url != '')
                        history.pushState('','',result.page_url);
                    $(".api_page_restaurant .api_temp_wrapper_mobile").fadeIn(api_fade_speed);
                });
            }
            else if (result.error == 'not_logged_in')
                window.location = site.base_url + 'login';    

        }
    });
}

function api_ajax_load_table(postData){
    $.ajax({
        type: 'GET',
        url:postData['ajax_url'],
        dataType: "json",
        data:postData, 
        success: function (result) {
            // var myWindow = window.open("", "MsgWindow", "width=700, height=400");
            // myWindow.document.write(result);   
            // console.log(result);
            if (result.error == '') {
                $('.api_page_restaurant .api_temp_wrapper_table').fadeOut(api_fade_speed, function() {
                    $('.api_page_restaurant .api_temp_wrapper_table').html(result.display);                  
                    $('.api_page_restaurant .api_temp_wrapper_status').html(result.display_status);                  
                    if (result.page_url != '')
                        history.pushState('','',result.page_url);
                    $(".api_page_restaurant .api_temp_wrapper_table").fadeIn(api_fade_speed);
                });
            }
            else if (result.error == 'not_logged_in')
                window.location = site.base_url + 'login';    

        }
    });
}
function api_ajax_load_table_mobile(postData){
    $.ajax({
        type: 'GET',
        url:postData['ajax_url'],
        dataType: "json",
        data:postData, 
        success: function (result) {
            // var myWindow = window.open("", "MsgWindow", "width=700, height=400");
            // myWindow.document.write(result);   
            // console.log(result);
            if (result.error == '') {
                $('.api_page_restaurant .api_temp_wrapper_mobile').fadeOut(api_fade_speed, function() {
                    $('.api_page_restaurant .api_temp_wrapper_mobile').html(result.display);                  
                    $('.api_page_restaurant .api_temp_wrapper_status').html(result.display_status); 
                    
                    $('.top_header_menu_icon').html(result.display_top_header_menu_icon_mobile);
                    
                    $('.api_page_restaurant .api_temp_footer_total_mobile').html(result.display_total);

                    if (result.page_url != '')
                        history.pushState('','',result.page_url);
                    $(".api_page_restaurant .api_temp_wrapper_mobile").fadeIn(api_fade_speed);
                });
            }
            else if (result.error == 'not_logged_in')
                window.location = site.base_url + 'login';    

        }
    });
}

function api_ajax_load_category(postData){
    $.ajax({
        type: 'GET',
        url:postData['ajax_url'],
        dataType: "json",
        data:postData, 
        success: function (result) {
            // var myWindow = window.open("", "MsgWindow", "width=700, height=400");
            // myWindow.document.write(result);   
            //console.log(result);
            if (result.error == '') {
                $('.api_page_restaurant .api_temp_wrapper_menu').fadeOut(api_fade_speed, function() {

                    $('.api_page_restaurant .api_temp_wrapper_menu').html(result.display);
                    $('.api_page_restaurant .api_temp_wrapper_total').html(result.display_total);
                    
                    if (result.page_url != '')
                        history.pushState('','',result.page_url);
                    $(".api_page_restaurant .api_temp_wrapper_menu").fadeIn(api_fade_speed);
                });
            }
            else if (result.error == 'not_logged_in')
                window.location = site.base_url + 'login';    

        }
    });
}
function api_ajax_load_category_mobile(postData){
    $.ajax({
        type: 'GET',
        url:postData['ajax_url'],
        dataType: "json",
        data:postData, 
        success: function (result) {
            // var myWindow = window.open("", "MsgWindow", "width=700, height=400");
            // myWindow.document.write(result);   
            //console.log(result);
            if (result.error == '') {
                $('.api_page_restaurant .api_temp_wrapper_menu_mobile').fadeOut(api_fade_speed, function() {

                    $('.api_page_restaurant .api_temp_wrapper_menu_mobile').html(result.display);
                    $('.api_page_restaurant .api_temp_wrapper_total').html(result.display_total);

                    if (result.page_url != '')
                        history.pushState('','',result.page_url);
                    $(".api_page_restaurant .api_temp_wrapper_menu_mobile").fadeIn(api_fade_speed);
                });
            }
            else if (result.error == 'not_logged_in')
                window.location = site.base_url + 'login';    

        }
    });

}



    

