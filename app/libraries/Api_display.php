<?php

class Api_display {

public function __construct()
{

}
public function __get($var)
{
    return get_instance()->$var;
}


// $config_data = array();
// $config_data['lang_key'] = $this->api_shop_setting[0]['api_lang_key'];
// $temp_config = $this->api_display->blog_config($config_data);
// $config_data = $temp_config['config_data'];
public function blog_config($config_data_function) {
    $return = array();
    $config_data = array(
        'border_bottom' => '
            <div class="api_height_15"></div>
            <div class="api_border_bottom"></div>
            <div class="api_height_20"></div>
        ',
        'col_class' => 'col_class',
        'date_format' => 'Y/m/d',
        'display_border_bottom' => 1,
        'display_date' => 1,
        'lang_key' => 'en',
        'image_class' => 'img-responsive api_link_box_none',
        'select_data' => $temp,
        'view_all' => '
            <div class="api_height_30"></div>
            <a href="'.base_url().'Movie">
                <div class="api_button_readmore">
                    MORE <li class="fa fa-arrow-right"></li>
                </div>
            </a>
            <div class="api_height_15"></div>    
        ',
    );
    $return['config_data'] = $config_data;
    return $return;
} 

// $config_data = array(
//     'select_data' => $temp,
//     'lang_key' => $this->api_shop_setting[0]['api_lang_key'],
//     'display_date' => 1,
//     'date_format' => 'Y/m/d',
//     'display_border_bottom' => 1,
//     'border_bottom' => '
//         <div class="api_height_15"></div>
//         <div class="api_border_bottom"></div>
//         <div class="api_height_20"></div>
//     ',
//     'view_all' => '
//         <div class="api_height_30"></div>
//         <a href="'.base_url().'Movie">
//             <div class="api_button_readmore">
//                 MORE <li class="fa fa-arrow-right"></li>
//             </div>
//         </a>
//         <div class="api_height_15"></div>    
//     ',
// );
public function blog($config_data_function) {    
    $return = array();
    $l = $config_data_function['lang_key'];
    $temp_border_bottom = '';
    if ($config_data_function['display_border_bottom'] == 1)
        $temp_border_bottom = '
            <tr>
            <td colspan="2">
                '.$config_data_function['border_bottom'].'
            </td>
            </tr>        
        ';
    $select_data = $config_data_function['select_data'];
    for ($i=0;$i<count($select_data);$i++) {
        if ($i < count($select_data) - 1)
            $temp_border_bottom_2 = $temp_border_bottom;
        else
            $temp_border_bottom_2 = '';

        $temp_date = date_create($select_data[$i]['date']);

        $temp_index_number = '';
        if ($config_data_function['display_index_number'] == 'yes')
            $temp_index_number = '
                <div class="temp_index_number temp_index_number_bg_'.($i + 1).'">
                    '.($i + 1).'
                </div>
            ';
        $temp_display .= '
            <tr>
            <td valign="top" class="api_temp_image_td ">
                <div style="position:relative">
                    '.$temp_index_number.'
                    <div class="api_temp_image">
                        '.$select_data[$i]['image'].'                    
                    </div>                    
                </div>
            </td>
            <td class="api_admin api_admin_wrapper_blog_'.$select_data[$i]['id'].'" valign="top">
                <div class="api_intro_wrapper">
        ';
            if ($config_data_function['title_class'] != 'api_display_none')
                $temp_display .= '
                    <a href="'.$select_data[$i]['link'].'">
                        <div class="'.$config_data_function['title_class'].' api_temp_title">
                            '.$select_data[$i]['title_'.$l].'
                        </div>
                    </a>
                ';

            if ($config_data_function['date_class'] != 'api_display_none')
                $temp_display .= '
                    <div class="'.$config_data_function['date_class'].' api_color_gray">
                        '.date_format($temp_date, $config_data_function['date_format']).'
                    </div>    
                    <div class="api_height_10"></div>
                ';
            
            if ($config_data_function['intro_class'] != 'api_display_none')
                $temp_display .= '
                    <div class="'.$config_data_function['intro_class'].' api_intro api_intro_'.$l.'">
                        '.$select_data[$i]['intro_'.$l].'
                    </div>
                ';
            $temp_display .= '
                </div>
            ';
            $field_data = array();
            $field_data[0] = array(
                'type' => 'translate',
                'col_class' => 'col-md-6',
                'field_class' => '',
                'field_name' => 'translate',
                'translate' => 'yes',
                'field_label_name' => 'Title',                      
                'required' => 'yes',
            );    
            $field_data[1] = array(
                'type' => 'translate',
                'col_class' => 'col-md-6',
                'field_class' => '',
                'field_name' => 'translate_2',
                'translate_2' => 'yes',
                'field_label_name' => 'Title Over Image',                      
            );             
            $field_data[2] = array(
                'type' => 'date',
                'col_class' => 'col-md-4',
                'field_class' => '',
                'field_name' => 'created_date',
                'translate' => '',
                'field_label_name' => 'Post Date',                      
            );    
            $field_data[3] = array(
                'type' => 'select',
                'col_class' => 'col-md-4',
                'field_class' => '',
                'field_name' => 'c_id',
                'field_label_name' => 'Category',
                'field_value' => $select_data[$i]['c_id'],
                'select_field' => 'blog_category',
                'select_field_condition' => "order by title_en asc",
                'required' => 'yes',
            );              
            $field_data[4] = array(
                'type' => 'add_ons_checkbox_yes_no',
                'col_class' => 'col-md-4',
                'field_class' => '',
                'field_name' => 'featured',
                'field_label_name' => 'Display in front page',  
                'field_value' => $select_data[$i]['featured'],                   
            );                  
            $field_data[5] = array(
                'type' => 'translate_textarea',
                'col_class' => 'col-md-12',
                'field_class' => '',
                'field_name' => 'description',
                'description' => 'yes',
                'field_label_name' => 'Intro',                      
            );             
            $field_data[6] = array(
                'type' => 'checkbox_list',
                'col_class' => 'col-md-6',
                'field_class' => '',
                'field_name' => 'tag_id',
                'field_label_name' => 'Tags',
                'field_value' => $select_data[$i]['tag_id'],
                'select_field' => 'blog_tag',
                'select_field_condition' => "order by title_en asc",            
            );         
            $field_data[7] = array(
                'type' => 'file',
                'col_class' => 'col-md-3',
                'field_class' => '',
                'field_name' => 'image',    
                'field_label_name' => 'Image',
                'recommended_width' => 700,
                'recommended_height' => 400,             
            );
            $config_data = array(
                'l' => $l,
                'wrapper_class' => 'api_admin_wrapper_blog_'.$select_data[$i]['id'],
                'modal_class' => 'modal-lg',
                'title' => 'Edit', 
                'field_data' => $field_data,
                'selected_id' => $select_data[$i]['id'],
                'table_name' => 'sma_blog',
                'table_id' => 'id',
                'upload_path' => $config_data_function['file_path'],
                'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
                'datepicker' => 'yes',
                'add' => 'yes',
                'title_add' => 'Add New Blog',
                'delete' => 'yes',
                'title_delete' => '<div>Your will remove</div>'.$select_data[$i]['title_'.$l],
            );
         
            $temp_admin = $this->api_admin->front_end_edit($config_data);
            $temp_display .= $temp_admin['display'];
            $temp_display .= '
                <script>
                    '.$temp_admin['script'].'
                </script>
            ';    

            $temp_display .= '
            </td>    
            </tr>
            '.$temp_border_bottom_2.'
        ';
    }

    if ($config_data_function['view_all'] != '')
        $temp_view_all = '
            <tr>
            <td colspan="2" align="center">
                '.$config_data_function['view_all'].'
            </td>
            </tr>
        ';


    $return['display'] .= '
        <div class="col-md-12 api_padding_0 api_display_blog '.$config_data_function['wrapper_class'].'">
            <table width="100%" border="0">
                '.$temp_display.'
                '.$temp_view_all.'
            </table>
        </div>
    ';

    return $return;
}


// $config_data = array();
// $config_data['lang_key'] = $this->api_shop_setting[0]['api_lang_key'];
// $temp_config = $this->api_display->large_config($config_data);
// $config_data = $temp_config['config_data'];
public function large_icon_config($config_data_function) {
    $return = array();
    $config_data = array(
        'border_bottom' => '
            <div class="api_height_15"></div>
            <div class="api_border_bottom"></div>
            <div class="api_height_20"></div>
        ',
        'col_class' => 'col_class',
        'date_format' => 'Y/m/d',
        'display_border_bottom' => 1,
        'display_date' => 1,
        'lang_key' => 'en',
        'image_class' => 'img-responsive api_link_box_none',
        'select_data' => $temp,
        'view_all' => '
            <div class="api_height_30"></div>
            <a href="'.base_url().'Movie">
                <div class="api_button_readmore">
                    MORE <li class="fa fa-arrow-right"></li>
                </div>
            </a>
            <div class="api_height_15"></div>    
        ',
    );
    $return['config_data'] = $config_data;
    return $return;
} 

public function large_icon($config_data_function) {
    $return = array();
    $l = $config_data_function['lang_key'];
    $select_data = $config_data_function['select_data'];
    
    for ($i=0;$i<count($select_data);$i++) {
        $temp_display .= '
            <div class="'.$config_data_function['col_class'].'">
            <div class="api_admin api_admin_wrapper_large_icon_'.$select_data[$i]['id'].'">
                <div class="api_height_15"></div>

                <div class="api_temp_image">
                    <a href="'.$select_data[$i]['link'].'">
                        '.$select_data[$i]['image'].'
                    </a>
                </div>

                <div class="api_height_15"></div>
                <div class="'.$config_data_function['title_class'].'">
                    <a class="'.$config_data_function['title_a_class'].'" href="'.$select_data[$i]['link'].'">
                        '.$select_data[$i]['title_'.$l].'
                    </a>
                </div>

                <div class="api_height_10"></div>

                <div class="api_intro api_intro_'.$l.'">
                    '.$select_data[$i]['intro_'.$l].'
                </div>               

                <div class="api_height_15"></div>

                <div class="api_readmore_wrapper">
                    <a href="'.$select_data[$i]['link'].'">
                        '.$config_data_function['read_more'].' 
                    </a>
                </div>

                <div class="api_height_15"></div>

        ';

        $field_data = array();
        $field_data[0] = array(
            'type' => 'translate_textarea',
            'col_class' => 'col-md-12',
            'field_class' => '',
            'field_name' => 'translate',
            'translate' => 'yes',
            'field_label_name' => 'Title',                      
        );    
        $field_data[1] = array(
            'type' => 'translate_textarea',
            'col_class' => 'col-md-12',
            'field_class' => '',
            'field_name' => 'description',
            'description' => 'yes',
            'field_label_name' => 'Intro',                      
        );             
        $field_data[2] = array(
            'type' => 'file',
            'col_class' => 'col-md-6',
            'field_class' => '',
            'field_name' => 'image',    
            'field_label_name' => 'Image',
            'recommended_width' => $config_data_function['recommended_width'],
            'recommended_height' => $config_data_function['recommended_height'],
        );
        $config_data = array(
            'l' => $l,
            'wrapper_class' => 'api_admin_wrapper_large_icon_'.$select_data[$i]['id'],
            'modal_class' => '',
            'title' => 'Edit', 
            'field_data' => $field_data,
            'selected_id' => $select_data[$i]['id'],
            'table_name' => 'sma_service',
            'table_id' => 'id',
            'upload_path' => $config_data_function['file_path'],
            'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
        );
        $temp_admin = $this->api_admin->front_end_edit($config_data);
        $temp_display .= $temp_admin['display'];
        $temp_display .= '
            <script>
                '.$temp_admin['script'].'
            </script>
        ';    

        $temp_display .= '
            </div>
            </div>
        ';
    }


    $return['display'] .= '
        <div class="api_display_large_icon">
            '.$temp_display.'
        </div>
    ';

    return $return;
}

public function large_icon_staff($config_data_function) {
    $return = array();
    $l = $config_data_function['lang_key'];

    $select_data = $config_data_function['select_data'];
    for ($i=0;$i<count($select_data);$i++) {
        if ($select_data[$i]['facebook'] != '')
        $select_data[$i]['social_link'] = '
            <div class="api_float_left api_padding_right_10">
                <a class="api_link_box_none" href="'.$select_data[$i]['facebook'].'" target="_blank">
                    <img width="30" src="'.base_url().'assets/images/facebook_icon.png" />
                </a>
            </div>
        ';
    
        if ($select_data[$i]['twitter'] != '')
        $select_data[$i]['social_link'] .= '
            <div class="api_float_left api_padding_right_10">        
                <a class="api_link_box_none" href="'.$select_data[$i]['twitter'].'" target="_blank">
                    <img width="30" src="'.base_url().'assets/images/twitter_icon.png" />
                </a>
            </div>
        ';
    
        if ($select_data[$i]['linkedin'] != '')
        $select_data[$i]['social_link'] .= '
            <div class="api_float_left api_padding_right_10">        
                <a class="api_link_box_none" href="'.$select_data[$i]['linkedin'].'" target="_blank">
                    <img width="30" src="'.base_url().'assets/images/linkedIn.png" />
                </a>
            </div>
        ';    
        $select_data[$i]['social_link'] .= '<div class="api_clear_both"></div>';
    
        if ($select_data[$i]['link'] != '')
            $temp_image = '
                <a href="'.$select_data[$i]['link'].'">
                    <div class="api_temp_image">
                        '.$select_data[$i]['image'].'
                    </div>
                </a>            
            ';
        else 
            $temp_image = '
                <div class="api_temp_image">
                    '.$select_data[$i]['image'].'
                </div>       
            ';        
        $temp_display .= '
            <div class="'.$config_data_function['col_class'].' api_height_ipad_pro api_admin api_admin_wrapper_large_icon_staff_'.$select_data[$i]['id'].'">
                <table width="100%" border="0">
                    <tr> 
                    <td valign="top" class="api_width_50">
                    </td>                                  
                    <td valign="top" class="api_temp_image_td">
                        '.$temp_image.'
                    </td>
                    <td valign="top" class="api_temp_information_td">
                        <div class="api_temp_social_link">
                            '.$select_data[$i]['social_link'].'
                        </div>
                        <div class="api_height_10"></div>
                        <div class="api_temp_position">
                            '.$select_data[$i]['position'].'
                        </div>
                        <div class="api_temp_type">
                            '.$select_data[$i]['type'].'
                        </div>
                        <div class="api_big api_temp_title">
                            '.$select_data[$i]['title_'.$l].'
                        </div>             
                        <div class="api_temp_intro">
                            '.$select_data[$i]['intro_'.$l].'
                        </div>                        
                    </td>    
                    </tr>
                </table>
        ';
        $field_data = array();
        $field_data[0] = array(
            'type' => 'translate_textarea',
            'col_class' => 'col-md-12',
            'field_class' => '',
            'field_name' => 'translate',
            'translate' => 'yes',
            'field_label_name' => 'Title',                      
        );  
        $field_data[1] = array(
            'type' => 'translate_textarea',
            'col_class' => 'col-md-12',
            'field_class' => '',
            'field_name' => 'description',
            'description' => 'yes',
            'field_label_name' => 'Description',                      
        );           
        $field_data[2] = array(
            'type' => 'add_ons',
            'col_class' => 'col-md-4',
            'field_class' => '',
            'field_name' => 'position',
            'field_label_name' => 'Position',                      
        );             
        $field_data[3] = array(
            'type' => 'add_ons',
            'col_class' => 'col-md-4',
            'field_class' => '',
            'field_name' => 'type',
            'field_label_name' => 'Second Position',                      
        );    
        $field_data[4] = array(
            'type' => 'text',
            'col_class' => 'col-md-4',
            'field_class' => 'api_numberic_input',
            'field_name' => 'ordering',
            'field_label_name' => 'Ordering',                      
        );           
        $field_data[5] = array(
            'type' => 'add_ons',
            'col_class' => 'col-md-12',
            'field_class' => '',
            'field_name' => 'facebook',
            'field_label_name' => 'Facebook Link',                      
        );           
        $field_data[6] = array(
            'type' => 'add_ons',
            'col_class' => 'col-md-12',
            'field_class' => '',
            'field_name' => 'twitter',
            'field_label_name' => 'Twitter Link',                      
        );                     
        $field_data[7] = array(
            'type' => 'add_ons',
            'col_class' => 'col-md-12',
            'field_class' => '',
            'field_name' => 'linkedin',
            'field_label_name' => 'LinkedIn Link',                      
        );                  
        $field_data[8] = array(
            'type' => 'file',
            'col_class' => 'col-md-4',
            'field_class' => '',
            'field_name' => 'image',    
            'field_label_name' => 'Photo',
            'recommended_width' => $config_data_function['recommended_width'],
            'recommended_height' => $config_data_function['recommended_height'],
        );
        $config_data = array(
            'l' => $l,
            'wrapper_class' => 'api_admin_wrapper_large_icon_staff_'.$select_data[$i]['id'],
            'modal_class' => '',
            'title' => 'Edit', 
            'field_data' => $field_data,
            'selected_id' => $select_data[$i]['id'],
            'table_name' => 'sma_staff',
            'table_id' => 'id',
            'upload_path' => $config_data_function['file_path'],
            'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
            'add' => 'yes',
            'title_add' => 'And New Member',
            'delete' => 'yes',
            'title_delete' => '<div>Your will remove</div>'.$select_data[$i]['title_'.$l],             
        );
        $temp_admin = $this->api_admin->front_end_edit($config_data);
        $temp_display .= $temp_admin['display'];
        $temp_display .= '
            <script>
                '.$temp_admin['script'].'
            </script>
        ';    

        $temp_display .= '
            </div>
        ';
    }

    $return['display'] .= '
        <div class="col-md-12 api_padding_0 api_display_large_icon_staff">
            '.$temp_display.'
        </div>
    ';
    return $return;
}

public function large_icon_staff_mobile($config_data_function) {
    $return = array();
    $l = $config_data_function['lang_key'];

    $select_data = $config_data_function['select_data'];
    for ($i=0;$i<count($select_data);$i++) {
        if ($select_data[$i]['facebook'] != '')
        $select_data[$i]['social_link'] = '
            <div class="api_float_left api_padding_right_10">
                <a class="api_link_box_none" href="'.$select_data[$i]['facebook'].'" target="_blank">
                    <img width="30" src="'.base_url().'assets/images/facebook_icon.png" />
                </a>
            </div>
        ';
    
        if ($select_data[$i]['twitter'] != '')
        $select_data[$i]['social_link'] .= '
            <div class="api_float_left api_padding_right_10">        
                <a class="api_link_box_none" href="'.$select_data[$i]['twitter'].'" target="_blank">
                    <img width="30" src="'.base_url().'assets/images/twitter_icon.png" />
                </a>
            </div>
        ';
    
        if ($select_data[$i]['linkedin'] != '')
        $select_data[$i]['social_link'] .= '
            <div class="api_float_left api_padding_right_10">        
                <a class="api_link_box_none" href="'.$select_data[$i]['linkedin'].'" target="_blank">
                    <img width="30" src="'.base_url().'assets/images/linkedIn.png" />
                </a>
            </div>
        ';    
        $select_data[$i]['social_link'] .= '<div class="api_clear_both"></div>';
    
        if ($select_data[$i]['link'] != '')
            $temp_image = '
                <a href="'.$select_data[$i]['link'].'">
                    <div class="api_temp_image">
                        '.$select_data[$i]['image'].'
                    </div>
                </a>            
            ';
        else 
            $temp_image = '
                <div class="api_temp_image">
                    '.$select_data[$i]['image'].'
                </div>       
            ';        
        $temp_display .= '
            <div class="'.$config_data_function['col_class'].' text-center">
                <div class="api_temp_image_td">
                    '.$temp_image.'
                </div>
                <div class="api_temp_information_td">
                    <div class="api_temp_social_link">
                        '.$select_data[$i]['social_link'].'
                    </div>
                    <div class="api_height_10"></div>
                    <div class="api_temp_position">
                        '.$select_data[$i]['position'].'
                    </div>
                    <div class="api_temp_type">
                        '.$select_data[$i]['type'].'
                    </div>
                    <div class="api_big api_temp_title">
                        '.$select_data[$i]['title_'.$l].'
                    </div>             
                    <div class="api_temp_intro">
                        '.$select_data[$i]['intro_'.$l].'
                    </div>                        
                </div>    
            </div>
        ';
    }

    $return['display'] .= '
        <div class="large_icon_staff_mobile">
            '.$temp_display.'
        </div>
    ';
    return $return;
}

public function large_icon_news($config_data_function) {
    $return = array();
    $l = $config_data_function['lang_key'];
    $select_data = $config_data_function['select_data'];
    
    for ($i=0;$i<count($select_data);$i++) {
        $temp_date = date_create($select_data[$i]['date']);
        $temp_display_2 = '';
        $temp_display_2 .= '
            <div class="'.$config_data_function['col_class'].' api_admin api_admin_wrapper_blog_'.$select_data[$i]['id'].'">
                <div class="api_height_15"></div>

                <div class="'.$config_data_function['image_class'].' api_temp_image api_absolute_center_wrapper">
                    <a href="'.$select_data[$i]['link'].'">
                        '.$select_data[$i]['image'].'
                    </a>
                    <div class="api_temp_image_title_center api_absolute_center_45">
                        <a class="api_color_white" href="'.$select_data[$i]['link'].'">
                            '.$select_data[$i]['image_title_center'].'
                        </a>
                    </div>                    
                    <div class="api_temp_image_title_bg">
                        <a class="api_color_white" href="'.$select_data[$i]['link'].'">
                            '.$select_data[$i]['image_title_bg'].'
                        </a>
                    </div>
                </div>

                <div class="api_height_15"></div>
                <div class="'.$config_data_function['title_class'].' api_temp_title api_temp_title_'.$l.'">
                    <a class="'.$config_data_function['title_a_class'].'" href="'.$select_data[$i]['link'].'">
                        '.$select_data[$i]['title_'.$l].'
                    </a>
                </div>

                <div class="api_height_10"></div>
        ';

        if ($config_data_function['intro_class'] != 'api_display_none')
            $temp_display_2 .= '
                <div class="'.$config_data_function['intro_class'].' api_intro api_intro_'.$l.'">
                    '.$select_data[$i]['intro_'.$l].'
                </div>               
            ';

        if ($config_data_function['date_class'] != 'api_display_none')
            $temp_display_2 .= '
                <div class="'.$config_data_function['date_class'].'">
                    '.date_format($temp_date, $config_data_function['date_format']).'
                </div>
            ';

        if ($config_data_function['readmore_class'] != 'api_display_none')
        $temp_display_2 .= '
                <div class="api_height_15"></div>
                <div class="api_readmore_wrapper">
                    <a href="'.$select_data[$i]['link'].'">
                        '.$config_data_function['read_more'].' 
                    </a>
                </div>
                <div class="api_height_15"></div>
        ';

        $field_data = array();
        $field_data[0] = array(
            'type' => 'translate',
            'col_class' => 'col-md-6',
            'field_class' => '',
            'field_name' => 'translate',
            'translate' => 'yes',
            'field_label_name' => 'Title',                      
            'required' => 'yes',
        );    
        $field_data[1] = array(
            'type' => 'translate',
            'col_class' => 'col-md-6',
            'field_class' => '',
            'field_name' => 'translate_2',
            'translate_2' => 'yes',
            'field_label_name' => 'Title Over Image',                      
        );             
        $field_data[2] = array(
            'type' => 'date',
            'col_class' => 'col-md-4',
            'field_class' => '',
            'field_name' => 'created_date',
            'translate' => '',
            'field_label_name' => 'Post Date',                      
        );    
        $field_data[3] = array(
            'type' => 'select',
            'col_class' => 'col-md-4',
            'field_class' => '',
            'field_name' => 'c_id',
            'field_label_name' => 'Category',
            'field_value' => $select_data[$i]['c_id'],
            'select_field' => 'blog_category',
            'select_field_condition' => "order by title_en asc",
            'required' => 'yes',
        );              
        $field_data[4] = array(
            'type' => 'add_ons_checkbox_yes_no',
            'col_class' => 'col-md-4',
            'field_class' => '',
            'field_name' => 'featured',
            'field_label_name' => 'Display in front page',  
            'field_value' => $select_data[$i]['featured'],                   
        );              
        $field_data[5] = array(
            'type' => 'translate_textarea',
            'col_class' => 'col-md-12',
            'field_class' => '',
            'field_name' => 'description',
            'description' => 'yes',
            'field_label_name' => 'Intro',                      
        );             
        $field_data[6] = array(
            'type' => 'checkbox_list',
            'col_class' => 'col-md-6',
            'field_class' => '',
            'field_name' => 'tag_id',
            'field_label_name' => 'Tags',
            'field_value' => $select_data[$i]['tag_id'],
            'select_field' => 'blog_tag',
            'select_field_condition' => "order by title_en asc",            
        );         
        $field_data[7] = array(
            'type' => 'file',
            'col_class' => 'col-md-3',
            'field_class' => '',
            'field_name' => 'image',    
            'field_label_name' => 'Image',
            'recommended_width' => 700,
            'recommended_height' => 400,
        );
        $config_data = array(
            'l' => $l,
            'wrapper_class' => 'api_admin_wrapper_blog_'.$select_data[$i]['id'],
            'modal_class' => 'modal-lg',
            'title' => 'Edit', 
            'field_data' => $field_data,
            'selected_id' => $select_data[$i]['id'],
            'table_name' => 'sma_blog',
            'table_id' => 'id',
            'upload_path' => $config_data_function['file_path'],
            'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
            'datepicker' => 'yes',
            'add' => 'yes',
            'title_add' => 'Add New Blog',
            'delete' => 'yes',
            'title_delete' => '<div>Your will remove</div>'.$select_data[$i]['title_'.$l],
        );
     
        $temp_admin = $this->api_admin->front_end_edit($config_data);
        $temp_display_2 .= $temp_admin['display'];
        $temp_display_2 .= '
            <script>
                '.$temp_admin['script'].'
            </script>
        ';    

        $temp_display_2 .= '
            </div>
        ';
        $temp_display .= $temp_display_2;
        $return['array'][$i] = '
            <div class="col-md-12 api_padding_0 api_display_large_icon_news">
                '.$temp_display_2.'
            </div>
        ';
    }

    if ($config_data_function['view_all_class'] != 'api_display_none')
        $temp_display .= '
            <div class="col-md-12 api_padding_0 api_clear_both"></div>
            <div class="col-md-12 api_padding_0 api_temp_view_all_wrapper">
                <div class="api_temp_view_all">           
                    '.$config_data_function['view_all'].'
                    <div class="col-md-12 api_padding_0 api_clear_both"></div>
                </div>
                <div class="col-md-12 api_padding_0 api_clear_both"></div>
            </div>
        ';

    $return['display'] .= '
        <div class="col-md-12 api_padding_0 api_display_large_icon_news">
            '.$temp_display.'
            <div class="col-md-12 api_padding_0 api_clear_both"></div>
        </div>
    ';

    return $return;
}

public function blog_news($config_data_function) {
    $return = array();
    $l = $config_data_function['lang_key'];
    $temp_border_bottom = '';
    if ($config_data_function['display_border_bottom'] == 1)
        $temp_border_bottom = '
            <tr>
            <td colspan="2">
                '.$config_data_function['border_bottom'].'
            </td>
            </tr>        
        ';
    $select_data = $config_data_function['select_data'];
    for ($i=0;$i<count($select_data);$i++) {
        if ($i < count($select_data) - 1)
            $temp_border_bottom_2 = $temp_border_bottom;
        else
            $temp_border_bottom_2 = '';

        $temp_date = date_create($select_data[$i]['date']);
        $temp_display .= '
            <tr class="api_border api_link_box_none api_temp_tr api_admin api_admin_wrapper_blog_news_'.$select_data[$i]['id'].'" >
            <td valign="top" class="api_temp_image_td">              
                    <div class="'.$config_data_function['image_class'].' api_temp_image api_absolute_center_wrapper" onclick="window.location=\''.$select_data[$i]['link'].'\'">
                        <a href="'.$select_data[$i]['link'].'">
                            '.$select_data[$i]['image'].'
                        </a>
                        <div class="api_temp_image_title_center api_absolute_center">
                            <a class="api_color_white" href="'.$select_data[$i]['link'].'">
                                '.$select_data[$i]['image_title_center'].'
                            </a>
                        </div>
                        <div class="api_temp_image_title_bg">
                            <a class="api_color_white" href="'.$select_data[$i]['link'].'">
                                '.$select_data[$i]['image_title_bg'].'
                            </a>
                        </div>
                    </div>
        ';
        $field_data = array();
        $field_data[0] = array(
            'type' => 'translate',
            'col_class' => 'col-md-6',
            'field_class' => '',
            'field_name' => 'translate',
            'translate' => 'yes',
            'field_label_name' => 'Title',                      
            'required' => 'yes',
        );    
        $field_data[1] = array(
            'type' => 'translate',
            'col_class' => 'col-md-6',
            'field_class' => '',
            'field_name' => 'translate_2',
            'translate_2' => 'yes',
            'field_label_name' => 'Title Over Image',                      
        );             
        $field_data[2] = array(
            'type' => 'date',
            'col_class' => 'col-md-4',
            'field_class' => '',
            'field_name' => 'created_date',
            'translate' => '',
            'field_label_name' => 'Post Date',                      
        );    
        $field_data[3] = array(
            'type' => 'select',
            'col_class' => 'col-md-4',
            'field_class' => '',
            'field_name' => 'c_id',
            'field_label_name' => 'Category',
            'field_value' => $select_data[$i]['c_id'],
            'select_field' => 'blog_category',
            'select_field_condition' => "order by title_en asc",
            'required' => 'yes',            
        );              
        $field_data[4] = array(
            'type' => 'add_ons_checkbox_yes_no',
            'col_class' => 'col-md-4',
            'field_class' => '',
            'field_name' => 'featured',
            'field_label_name' => 'Display in front page',  
            'field_value' => $select_data[$i]['featured'],                   
        );               
        $field_data[5] = array(
            'type' => 'translate_textarea',
            'col_class' => 'col-md-12',
            'field_class' => '',
            'field_name' => 'description',
            'description' => 'yes',
            'field_label_name' => 'Intro',                      
        );             
        $field_data[6] = array(
            'type' => 'checkbox_list',
            'col_class' => 'col-md-6',
            'field_class' => '',
            'field_name' => 'tag_id',
            'field_label_name' => 'Tags',
            'field_value' => $select_data[$i]['tag_id'],
            'select_field' => 'blog_tag',
            'select_field_condition' => "order by title_en asc",            
        );         
        $field_data[7] = array(
            'type' => 'file',
            'col_class' => 'col-md-3',
            'field_class' => '',
            'field_name' => 'image',    
            'field_label_name' => 'Image',
            'recommended_width' => 700,
            'recommended_height' => 400,
        );
        $config_data = array(
            'l' => $l,
            'wrapper_class' => 'api_admin_wrapper_blog_news_'.$select_data[$i]['id'],
            'modal_class' => 'modal-lg',
            'title' => 'Edit', 
            'field_data' => $field_data,
            'selected_id' => $select_data[$i]['id'],
            'table_name' => 'sma_blog',
            'table_id' => 'id',
            'upload_path' => $config_data_function['file_path'],
            'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
            'datepicker' => 'yes',
            'add' => 'yes',
            'title_add' => 'Add New Blog',
            'delete' => 'yes',
            'title_delete' => '<div>Your will remove</div>'.$select_data[$i]['title_'.$l],
        );
     
        $temp_admin = $this->api_admin->front_end_edit($config_data);
        $temp_display .= $temp_admin['display'];
        $temp_display .= '
            <script>
                '.$temp_admin['script'].'
            </script>
        ';    

        $temp_display .= '
            </td>
            <td class="" valign="top">
                <div class="api_intro_wrapper" onclick="window.location=\''.$select_data[$i]['link'].'\'">
                    <div class="api_height_10"></div>
                    <div class="'.$config_data_function['title_class'].' api_temp_title api_temp_title_'.$l.'">
                        <a href="'.$select_data[$i]['link'].'">
                            '.$select_data[$i]['title_'.$l].'
                        </a>
                    </div>
                    <div class="api_height_10"></div>
                    <div class="'.$config_data_function['intro_class'].' api_temp_intro api_temp_intro_'.$l.'">
                        '.$select_data[$i]['intro_'.$l].'
                    </div>
                    <div class="api_height_10"></div>
                    <div class="api_temp_date api_color_gray">
                        '.date_format($temp_date, $config_data_function['date_format']).'
                    </div>    
                </div>
            </td>    
            </tr>
        ';

        $temp_display .= '
            <tr>
                <td class="" colspan="2" valign="top">
                    <div class="api_height_30"></div>
                </td>
            <tr>
        ';

        $temp_display_2 .= '
            <tr class="api_border" >
            <td valign="top" class="api_temp_image_td">                
                <div class="'.$config_data_function['image_class'].' api_temp_image api_absolute_center_wrapper">
                    <a href="'.$select_data[$i]['link'].'">
                        '.$select_data[$i]['image'].'
                    </a>
                    <div class="api_temp_image_title_center api_absolute_center">
                        <a class="api_color_white" href="'.$select_data[$i]['link'].'">
                            '.$select_data[$i]['image_title_center'].'
                        </a>
                    </div>                    
                    <div class="api_temp_image_title_bg">
                        <a class="api_color_white" href="'.$select_data[$i]['link'].'">
                            '.$select_data[$i]['image_title_bg'].'
                        </a>
                    </div>
                </div>                   
            </td>
            </tr>
            <tr>
            <td class="" valign="top">
                <div class="api_intro_wrapper">
                    <div class="api_height_10"></div>
                    <div class="'.$config_data_function['title_class'].' api_temp_title api_temp_title_'.$l.'">
                        <a href="'.$select_data[$i]['link'].'">
                            '.$select_data[$i]['title_'.$l].'
                        </a>
                    </div>
                    <div class="api_height_10"></div>
                    <div class="'.$config_data_function['intro_class'].' api_temp_intro api_temp_intro_'.$l.'">
                        '.$select_data[$i]['intro_'.$l].'
                    </div>
                    <div class="api_height_10"></div>
                    <div class="api_temp_date api_color_gray">
                        '.date_format($temp_date, $config_data_function['date_format']).'
                    </div>    
                </div>
            </td>    
            </tr>
        ';

        $temp_display_2 .= '
            <tr>
                <td class="" valign="top">
                    <div class="api_height_30"></div>
                </td>
            <tr>
        ';

    }

    if ($config_data_function['view_all'] != '') {
        $temp_view_all = '
            <tr>
            <td colspan="2" align="center">
                '.$config_data_function['view_all'].'
            </td>
            </tr>
        ';
        $temp_view_all_2 = '
            <tr>
            <td align="center">
                '.$config_data_function['view_all'].'
            </td>
            </tr>
        ';        
    }


    $return['display'] .= '
        <div class="col-md-12 api_padding_0 api_display_blog '.$config_data_function['wrapper_class'].'">
            <table width="100%" border="0" class="hidden-xs">
                '.$temp_display.'
                '.$temp_view_all.'
            </table>
            <table width="100%" border="0" class="visible-xs">
                '.$temp_display_2.'
                '.$temp_view_all_2.'
            </table>            
        </div>
    ';

    return $return;

}

public function pagination($config_data_function){
    // $config_data = array(
    //     'pagination_info' => $pagination_info,
    // );
    // $temp = $this->api_display->pagination($config_data);
    // $temp_display .= $temp['display'];  

    $return = array();    
    $return['display'] = '
        <div class="clearfix"></div>
        <div class="'.$config_data_function['number_class'].'">
            <span class="page-info2 line-height-xl hidden-xs hidden-sm">
                '.str_replace(['_page_', '_total_'], [$config_data_function['pagination_info']['page'], $config_data_function['pagination_info']['total']], lang('page_info')).'
            </span>        
        </div>
        <div class="'.$config_data_function['box_class'].'">
            <div id="pagination2">
                '.$this->pagination->create_links().'
            </div>
        </div>
        <div class="clearfix"></div>
    ';
    return $return;
}

    
public function template_display($config_data_function){
    // $config_data = array(
    //     'wrapper_class' => 'api_page_ayum_blog',
    //     'custom_html' => '',
    //     'display' => $temp_display,
    //     'display_class' => 'col-md-9 api_padding_0',
    //     'panel' => $temp_display_panel,
    //     'panel_class' => 'col-md-3 api_padding_0',
    //     'type' => '',
    // );    
    // $temp = $this->api_display->template_display($config_data);
    // echo $temp['display'];
    $return = array();
    if ($config_data_function['type'] == '') {
        $return['display'] = '   
            <div class="api_section_wrapper api_normal '.$config_data_function['wrapper_class'].'">
                <div class="api_section">
                    '.$config_data_function['custom_html'].'
        ';
                    if ($config_data_function['panel'] == '')
                        $return['display'] .= '
                            <div class="'.$config_data_function['display_class'].'">
                                '.$config_data_function['page_header'].'
                                '.$config_data_function['display'].'
                            </div>
                        ';
                    else {
                        $return['display'] .= '
                            <div class="'.$config_data_function['display_class'].'">
                                '.$config_data_function['page_header'].'
                                '.$config_data_function['display'].'
                            </div>
                            <div class="'.$config_data_function['panel_class'].'">
                                '.$config_data_function['panel'].'
                            </div>
                        ';
                    }
        $return['display'] .= '
                    <div class="api_clear_both"></div>
                </div>
                <div class="api_clear_both"></div>
            </div>        
        ';
    }
    elseif ($config_data_function['type'] == 'full') {
        $return['display'] = '
        <div class="api_section_wrapper '.$config_data_function['wrapper_class'].'">        
            <div class="api_section_full">            
                '.$config_data_function['custom_html'].'
                '.$config_data_function['display'].'
                <div class="api_clear_both"></div>
            </div>
            <div class="api_clear_both"></div>
        </div>
        ';
    }

    return $return;
}

public function col_border_title_description_year($config_data_function){
    $return = array();
    // $config_data_function = array(
    //     'wrapper_class' => 'col_border_title_description_year',
    //     'col_class' => 'col-md-6',
    //     'title' => lang('career_change'),
    //     'description' => lang('ayum_for_jobseeker_6'),
    //     'year_text' => lang('work_at_japanese'),
    // );
    if($config_data_function['title_2_key'] != '') {
        $temp = '
            <div class="api_big api_temp_title_2">
        ';
        $temp_admin = $this->api_helper->api_lang_v2($config_data_function['title_2_key']);
        $temp .= $temp_admin['display'];
        $temp .= '
            <script>
                '.$temp_admin['script'].'
            </script>
        ';
        $temp .= '
            </div>
        ';
    }
    $return['display'] .= '
        <div class="col_border_title_description_year">
            <div class="api_temp_border">
                '.$temp.'
                <div class="api_temp_div">
                    <div class="api_big api_temp_title">
    ';
    $temp_admin = $this->api_helper->api_lang_v2($config_data_function['title_key']);
    $return['display'] .= $temp_admin['display'];
    $return['display'] .= '
        <script>
            '.$temp_admin['script'].'
        </script>
    ';
    $return['display'] .= '
                    </div>
                    <div class="api_temp_description">
    ';
    $temp_admin = $this->api_helper->api_lang_v2($config_data_function['description_key']);
    $return['display'] .= $temp_admin['display'];
    $return['display'] .= '
        <script>
            '.$temp_admin['script'].'
        </script>
    ';
    $return['display'] .= '
                    </div>      
                    <div class="api_temp_year">
    ';
    $temp_admin = $this->api_helper->api_lang_v2($config_data_function['year_text_key']);
    $return['display'] .= $temp_admin['display'];
    $return['display'] .= '
        <script>
            '.$temp_admin['script'].'
        </script>
    ';
    $return['display'] .= '
                    </div> 
                </div>                                                 
            </div>
        </div>
    ';



    return $return;
}


function api_temp_function_1($config_data) {
    // $config_data = array(
    //     'title_1' => 'STEP1',
    //     'title_2' => lang('Preparation before selection'),
    //     'title_3' => lang('Carefully match the requirements and the personality'),
    //     'icon_1' => '<img class="img-responsive" src="'.base_url().'assets/api/page/ayum_hr/image/Picture1.png" />',
    //     'icon_1_title' => lang('Recruitment Plan Requirements Hiring'),
    // );    
if ($config_data['icon_3'] == '') {
    $return['display'] .= '
        <table class="api_temp_table_1 hidden-xs" width="100%" border="0">
        <tr>
        <td valign="middle" class="api_temp_td_1">
            <div class="api_big api_temp_div_1">
                <table width="100%" border="0">
                    <tr>
                        <td valign="middle" class="api_temp_div_1">
                            '.$config_data['title_1'].'
                        </td> 
                    </tr>
                </table>
            </div>
            <div class="api_temp_div_2">
                
                <table width="100%" border="0">
                    <tr>
                        <td valign="middle" class="api_temp_div_2">
    ';
    $temp_admin = $this->api_helper->api_lang_v2($config_data['title_2']);
    $return['display'] .= $temp_admin['display'];
    $return['script'] .= $temp_admin['script'];

    $return['display'] .= '    
                        </td> 
                    </tr>
                </table>
            </div>
        </td>
        <td valign="middle" class="api_temp_td_2" align="center">
            <div class="api_temp_div_3">
                <table width="100%" border="0">
                    <tr>
                    <td valign="middle" class="api_temp_icon_1" >
    ';
    $temp_admin = $this->api_helper->api_lang_v2($config_data['title_3']);
    $return['display'] .= $temp_admin['display'];
    $return['script'] .= $temp_admin['script'];

    $return['display'] .= '
                    </td>
                    </tr>
                </table> 
            </div>
            <div class="api_temp_div_4">
                <table border="0">
                <tr>
                <td valign="middle" class="arrow_box">
                    <table width="100%" border="0">
                    <tr>
                    <td valign="middle" class="api_temp_icon_1" >
                        '.$config_data['icon_1'].'
                    </td>
                    <td valign="middle" class="api_temp_title_1">
    ';
    $temp_admin = $this->api_helper->api_lang_v2($config_data['icon_1_title']);
    $return['display'] .= $temp_admin['display'];
    $return['script'] .= $temp_admin['script'];

    $return['display'] .= '
                    </td>
                    </tr>
                    </table>         
                </td>
                <td valign="middle" width="40">
                    &nbsp;
                </td>
                <td valign="middle" width="15">
                    &nbsp;
                </td>
                <td valign="middle" class="arrow_box">
                    <table width="100%" border="0">
                    <tr>
                    <td valign="middle" class="api_temp_icon_1" >
                        '.$config_data['icon_2'].'
                    </td>
                    <td valign="middle" class="api_temp_title_1">
    ';
    $temp_admin = $this->api_helper->api_lang_v2($config_data['icon_2_title']);
    $return['display'] .= $temp_admin['display'];
    $return['script'] .= $temp_admin['script'];

    $return['display'] .= '
                    </td>
                    </tr>
                    </table>         
                </td>                
                </tr>
                </table>
            </div>
        </td>
        </tr>
        </table>
    ';
}
else {
$return['display'] .= '
        <table class="api_temp_table_1 hidden-xs" width="100%" border="0">
        <tr>
        <td valign="middle" class="api_temp_td_1">
            <div class="api_big api_temp_div_1">
                <table width="100%" border="0">
                    <tr>
                        <td valign="middle" class="api_temp_div_1">
                            '.$config_data['title_1'].'
                        </td> 
                    </tr>
                </table>
            </div>
            <div class="api_temp_div_2">
                
                <table width="100%" border="0">
                    <tr>
                        <td valign="middle" class="api_temp_div_2">
';
$temp_admin = $this->api_helper->api_lang_v2($config_data['title_2']);
$return['display'] .= $temp_admin['display'];
$return['script'] .= $temp_admin['script'];

$return['display'] .= '
                        </td> 
                    </tr>
                </table>
            </div>
        </td>
        <td valign="top" class="api_temp_td_2">
            <div class="api_temp_div_3">
                <table width="100%" border="0">
                    <tr>
                    <td valign="middle" class="api_temp_icon_1" >
';
$temp_admin = $this->api_helper->api_lang_v2($config_data['title_3']);
$return['display'] .= $temp_admin['display'];
$return['script'] .= $temp_admin['script'];

$return['display'] .= '
                    </td>
                    </tr>
                </table> 
            </div>
            <div class="api_temp_div_4_v2">
                <table border="0">
                <tr>
                <td valign="middle" class="arrow_box_2">
                    <table width="100%" border="0">
                    <tr>
                    <td valign="middle" class="api_temp_icon_1">
                        '.$config_data['icon_1'].'
                    </td>
                    <td valign="middle" class="api_temp_title_1">
';
$temp_admin = $this->api_helper->api_lang_v2($config_data['icon_1_title']);
$return['display'] .= $temp_admin['display'];
$return['script'] .= $temp_admin['script'];

$return['display'] .= '
                    </td>
                    </tr>
                    </table>         
                </td>
                <td valign="middle" width="50">
                    &nbsp;
                </td>
                <td valign="middle" class="arrow_box_2">
                    <table width="100%" border="0">
                    <tr>
                    <td valign="middle" class="api_temp_icon_1" >
                        '.$config_data['icon_2'].'
                    </td>
                    <td valign="middle" class="api_temp_title_1">
';
$temp_admin = $this->api_helper->api_lang_v2($config_data['icon_2_title']);
$return['display'] .= $temp_admin['display'];
$return['script'] .= $temp_admin['script'];

$return['display'] .= '
                    </td>
                    </tr>
                    </table>         
                </td>     
                <td valign="middle" width="50">
                    &nbsp;
                </td>
                <td valign="middle" class="arrow_box_2">
                    <table width="100%" border="0">
                    <tr>
                    <td valign="middle" class="api_temp_icon_1_v2" >
                        '.$config_data['icon_3'].'
                    </td>
                    <td valign="middle" class="api_temp_title_1">
';
$temp_admin = $this->api_helper->api_lang_v2($config_data['icon_3_title']);
$return['display'] .= $temp_admin['display'];
$return['script'] .= $temp_admin['script'];

$return['display'] .= '
                    </td>
                    </tr>
                    </table>         
                </td>

                </tr>
                </table>
                
            </div>
        </td>
        </tr>
        </table>
    ';
}
//mobile
if ($config_data['icon_3'] == '') {
    $return['display'] .= '
        <table class="api_temp_table_1 visible-xs" width="100%" border="0">
        <tr>
        <td valign="middle" class="api_temp_td_1">
            <div class="api_big api_temp_div_1">
                <table width="100%" border="0">
                    <tr>
                        <td valign="middle" class="api_temp_div_1">
                            '.$config_data['title_1'].'
                        </td> 
                    </tr>
                </table>
            </div>
            <div class="api_temp_div_2">
                
                <table width="100%" border="0">
                    <tr>
                        <td valign="middle" class="api_temp_div_2">
                            '.lang($config_data['title_2']).'
                        </td> 
                    </tr>
                </table>
            </div>
        </td>
        </tr>
        <tr>
        <td valign="top" align="center" class="api_temp_td_2">
            <div class="api_temp_div_3">
                '.lang($config_data['title_3']).'
            </div>

            <table border="0">
            <tr>
            <td valign="middle" class="arrow_box_3">
                <table width="100%" border="0">
                <tr>
                <td valign="middle" class="api_temp_icon_1" >
                    '.$config_data['icon_1'].'
                </td>
                <td valign="middle" class="api_temp_title_1">
                    '.lang($config_data['icon_1_title']).'
                </td>
                </tr>
                </table>         
            </td>
            </tr>
            <tr>
            <td valign="middle" width="40">
            &nbsp;
            </td>
            </tr>
            <tr>
            <td valign="middle" class="arrow_box_3">
                <table width="100%" border="0">
                <tr>
                <td valign="middle" class="api_temp_icon_1" >
                    '.$config_data['icon_2'].'
                </td>
                <td valign="middle" class="api_temp_title_1">
                    '.lang($config_data['icon_2_title']).'
                </td>
                </tr>
                </table>           
            </td>
            </tr>

            </table>

        </td>        
        </tr>
        </table>
    ';
}else{
    $return['display'] .= '
        <table class="api_temp_table_1 visible-xs" width="100%" border="0">
        <tr>
        <td valign="middle" class="api_temp_td_1">
            <div class="api_big api_temp_div_1">
                <table width="100%" border="0">
                    <tr>
                        <td valign="middle" class="api_temp_div_1">
                            '.$config_data['title_1'].'
                        </td> 
                    </tr>
                </table>
            </div>
            <div class="api_temp_div_2">
                
                <table width="100%" border="0">
                    <tr>
                        <td valign="middle" class="api_temp_div_2">
                            '.lang($config_data['title_2']).'
                        </td> 
                    </tr>
                </table>
            </div>
        </td>
        </tr>
        <tr>
        <td valign="top" align="center" class="api_temp_td_2">
            <div class="api_temp_div_3">
                '.lang($config_data['title_3']).'
            </div>

            <table border="0">
            <tr>
            <td valign="middle" class="arrow_box_3">
                <table width="100%" border="0">
                <tr>
                <td valign="middle" class="api_temp_icon_1" >
                    '.$config_data['icon_1'].'
                </td>
                <td valign="middle" class="api_temp_title_1">
                    '.lang($config_data['icon_1_title']).'
                </td>
                </tr>
                </table>         
            </td>
            </tr>
            <tr>
            <td valign="middle" width="40">
            &nbsp;
            </td>
            </tr>
            <tr>
            <td valign="middle" class="arrow_box_3">
                <table width="100%" border="0">
                <tr>
                <td valign="middle" class="api_temp_icon_1" >
                    '.$config_data['icon_2'].'
                </td>
                <td valign="middle" class="api_temp_title_1">
                    '.lang($config_data['icon_2_title']).'
                </td>
                </tr>
                </table>           
            </td>
            </tr>
            <tr>
            <td valign="middle" width="40">
            &nbsp;
            </td>
            </tr>
            <tr>
            <td valign="middle" class="arrow_box_3">
                <table width="100%" border="0">
                <tr>
                <td valign="middle" class="api_temp_icon_1" >
                    '.$config_data['icon_3'].'
                </td>
                <td valign="middle" class="api_temp_title_1">
                    '.lang($config_data['icon_3_title']).'
                </td>
                </tr>
                </table>           
            </td>
            </tr>

            </table>

        </td>        
        </tr>
        </table>
    ';
}
    return $return;
}




}
