<?php 

class api_cache {
	function __construct()
	{

	}
public function __get($var)
{
    return get_instance()->$var;
}

public function company($config_data){
    //select_company_drop----------------------------------------
    // if (!is_file("assets/api/data/stock_manager/json/people/select_company_drop.json") || $config_data['type'] == 'insert') {
    //     $config_data = array(
    //         'none_label' => lang("All_Companies"),
    //         'table_name' => 'sma_companies',
    //         'space' => ':....',
    //         'strip_id' => '',
    //         'field_name' => 'company',
    //         'condition' => " and group_id = 3 order by company asc",
    //     );                    
    //     $this->site->api_get_option_category($config_data);
    //     for ($i=0;$i<count($_SESSION['api_temp']);$i++) {                        
    //         $temp = explode(':{api}:',$_SESSION['api_temp'][$i]);
    //         if ($temp[0] > 0) {
    //             $temp_2 = $this->site->api_select_some_fields_with_where("
    //                 name
    //                 "
    //                 ,"sma_companies"
    //                 ,"id = ".$temp[0]
    //                 ,"arr"
    //             );                            
    //             $tr_2[$temp[0]] = $temp[1].' [Name: '.$temp_2[0]['name'].']';
    //         }
    //         else
    //             $tr_2[$temp[0]] = $temp[1];
    //     }
    //     $json = json_encode($tr_2);
    //     $bytes = file_put_contents("assets/api/data/stock_manager/json/people/select_company_drop.json", $json); 
    // }

    // $temp = file_get_contents("assets/api/data/stock_manager/json/people/select_company_drop.json");
    // $tr_2 = json_decode($temp, true);    

    // if ($config_data['id'] > 0) {
    //     if ($config_data['type'] == 'update') {
    //         foreach ($tr_2 as $key => $value) {
    //             if ($key == $config_data['id']) {
    //                 $config_data_2 = array(
    //                     'table_name' => 'sma_companies',
    //                     'select_table' => 'sma_companies',
    //                     'translate' => '',
    //                     'select_condition' => "id = ".$config_data['id'],
    //                 );
    //                 $temp = $this->site->api_select_data_v2($config_data_2);                    
    //                 $temp_2 = $temp[0]['company'].' [Name: '.$temp[0]['name'].']';
    //                 $tr_2[$key] = $temp_2;
    //                 break;
    //             }
    //         }
    //         $json = json_encode($tr_2);
    //         $bytes = file_put_contents("assets/api/data/stock_manager/json/people/select_company_drop.json", $json);             
    //     }
    //     if ($config_data['type'] == 'delete') {
    //         $tr = array();
    //         foreach ($tr_2 as $key => $value) {
    //             if ($key != $config_data['id']) {
    //                 $tr[$key] = $tr_2[$key];
    //             }
    //         }
    //         $json = json_encode($tr);
    //         $bytes = file_put_contents("assets/api/data/stock_manager/json/people/select_company_drop.json", $json);              
    //     }
    // }
    //select_company_drop----------------------------------------



    //select_company_arrow----------------------------------------
    if (!is_file("assets/api/data/stock_manager/json/people/select_company_arrow.json") || $config_data['type'] == 'delete' || $config_data['type'] == 'insert') {
            
        $config_data = array(
            'none_label' => lang("All_Companies"),
            'table_name' => 'sma_companies',
            'space' => ' &rarr; ',
            'strip_id' => '',        
            'field_name' => 'company',
            'condition' => ' and group_id = 3 order by company asc',
            'translate' => '',
            'no_space' => 1,
            'type' => 'arrow',
            'only_parent' => '',
        );                        
        $this->site->api_get_option_category($config_data);
        $temp_option = $_SESSION['api_temp'];
        for ($i=0;$i<count($temp_option);$i++) {                        
            $temp = explode(':{api}:',$temp_option[$i]);
            $temp_10 = '';
            if ($temp[0] != '') {
                $config_data_2 = array(
                    'id' => $temp[0],
                    'table_name' => 'sma_companies',
                    'field_name' => 'company',
                    'translate' => '',
                );
                $_SESSION['api_temp'] = array();
                $this->site->api_get_category_arrow($config_data_2);          
                for ($i2 = count($_SESSION['api_temp']) - 1; ; $i2--) {
                    if ($i2 == 0) {
                        break;
                    }
                    $temp_arrow = '';
                    if ($i2 > 1)
                        $temp_arrow = ' &rarr; ';
                    $temp_10 .= $_SESSION['api_temp'][$i2].$temp_arrow;
                }   
            }
            $tr_2[$temp[0]] = $temp_10.$temp[1];
        }

        $json = json_encode($tr_2);
        $bytes = file_put_contents("assets/api/data/stock_manager/json/people/select_company_arrow.json", $json); 
    }

    $temp = file_get_contents("assets/api/data/stock_manager/json/people/select_company_arrow.json");
    $tr_2 = json_decode($temp, true);    
    if ($config_data['id'] > 0) {
        if ($config_data['type'] == 'update') {
            $temp = $this->site->api_select_some_fields_with_where("
                *     
                "
                ,"sma_companies"
                ,"id = ".$config_data['id']
                ,"arr"
            );
            if ($temp[0]['parent_id'] == 0)
                $temp_id = $temp[0]['id'];
            else
                $temp_id = $temp[0]['parent_id'];

            $temp = $this->site->api_select_some_fields_with_where("
                *     
                "
                ,"sma_companies"
                ,"id = ".$temp_id
                ,"arr"
            );                
            foreach ($tr_2 as $key => $value) {
                if ($key == $temp_id) {                   
                    $tr_2[$key] = $temp[0]['company'];
                    break;
                }
            }
            $temp_2 = $this->site->api_select_some_fields_with_where("
                *     
                "
                ,"sma_companies"
                ,"parent_id = ".$temp_id." order by company asc"
                ,"arr"
            );
            for ($i=0;$i<count($temp_2);$i++) {
                foreach ($tr_2 as $key => $value) {
                    if ($key == $temp_2[$i]['id']) {                   
                        $config_data_2 = array(
                            'id' => $temp_2[$i]['id'],
                            'table_name' => 'sma_companies',
                            'field_name' => 'company',
                            'translate' => '',
                        );
                        $temp_3 = $this->api_helper->api_display_category_arrow($config_data_2);
                        $tr_2[$key] = $temp_3['display'];
                    }
                }
            }
        }
    }
    $temp = array();
    $tr = array();
    $k = 0;
    foreach ($tr_2 as $key => $value) {
        $temp[$k]['value'] = $value;
        $temp[$k]['key'] = $key;
        $k++;
    }
    sort($temp);

    $tr[''] = lang("All_Companies");
    for ($i=0;$i<count($temp);$i++) {
        if ($temp[$i]['key'] != '')
            $tr[$temp[$i]['key']] = $temp[$i]['value'];
    }
  
    $json = json_encode($tr);
    $bytes = file_put_contents("assets/api/data/stock_manager/json/people/select_company_arrow.json", $json);         
    //select_company_arrow----------------------------------------

}

public function city($config_data) {
    //cambodia_arrow----------------------------------------
    $language_array = unserialize(multi_language);
    for ($i2=0;$i2<count($language_array);$i2++) {       
        $file_path = 'assets/api/data/stock_manager/json/select_field/city_arrow_cambodia_'.$language_array[$i2][0].'.json';
        if (!is_file($file_path) || $config_data['type'] == 'delete' || $config_data['type'] == 'insert' || $config_data['type'] == 'update') {
                
            $config_data = array(
                'none_label' => '',
                'table_name' => 'sma_city',
                'space' => ' &rarr; ',
                'strip_id' => '',        
                'field_name' => 'title_'.$language_array[$i2][0],
                'condition' => 'order by title_en asc',
                'translate' => 'yes',
                'no_space' => 1,
                'parent_id' => 268,
            );                        
            $this->api_helper->api_get_option_category($config_data);
            $temp_option = $_SESSION['api_temp'];
            for ($i=0;$i<count($temp_option);$i++) {                        
                $temp = explode(':{api}:',$temp_option[$i]);
                $temp_10 = '';
                if ($temp[0] != '') {
                    $_SESSION['api_temp'] = array();
                    $config_data_2 = array(
                        'id' => $temp[0],
                        'table_name' => 'sma_city',
                        'field_name' => 'title_'.$language_array[$i2][0],
                        'translate' => 'yes',
                        'show_parent_name' => 0,
                    );                    
                    $this->api_helper->api_get_category_arrow($config_data_2);          
                    for ($i2 = count($_SESSION['api_temp']) - 1; ; $i2--) {
                        if ($i2 == 0) {
                            break;
                        }
                        $temp_arrow = '';
                        if ($i2 > 1)
                            $temp_arrow = ' &rarr; ';
                        $temp_10 .= $_SESSION['api_temp'][$i2].$temp_arrow;
                    }   
                }
                $tr_2[$temp[0]] = $temp_10.$temp[1];
            }

            $json = json_encode($tr_2);
            $bytes = file_put_contents($file_path, $json); 
        }
    } 
    //cambodia_arrow----------------------------------------

}

public function customer_group($config_data) {
    //drop----------------------------------------
    $file_path = 'assets/api/data/stock_manager/json/select_field/customer_group_drop.json';
    if (!is_file($file_path) || $config_data['type'] == 'delete' || $config_data['type'] == 'insert' || $config_data['type'] == 'update') {
        $config_data = array(
            'none_label' => lang("All_Customer_Groups"),
            'table_name' => 'sma_customer_groups',
            'space' => ':....',
            'strip_id' => '',
            'field_name' => 'name',
            'condition' => 'order by name asc',
        );                        
        $this->site->api_get_option_category($config_data);
        for ($i=0;$i<count($_SESSION['api_temp']);$i++) {                        
            $temp = explode(':{api}:',$_SESSION['api_temp'][$i]);
            $tr_2[$temp[0]] = $temp[1];
        }
        $json = json_encode($tr_2);
        $bytes = file_put_contents($file_path, $json); 
    }   
    //drop----------------------------------------
}

public function api_front_main_category($config_data) {
    if ($this->api_shop_setting[0]['air_base_url'] == base_url())
        $condition .= " and add_ons like '%:air_display:{yes}:%' ";
    $config_data_2 = array(
        'table_name' => 'sma_categories',
        'select_table' => 'sma_categories',
        'translate' => 'yes',
        'select_condition' => "parent_id = 0 ".$condition." ".$config_data['condition']." order by CAST(order_number AS SIGNED) asc",
    );
    $temp = $this->site->api_select_data_v2($config_data_2);
    $this->data['main_categories'] = $temp;        
    for ($i=0;$i<count($temp);$i++) {
        if (is_file("assets/api/data/stock_manager/json/front_main_category/parent_category_".$temp[$i]['id'].".json")) {
            unlink("assets/api/data/stock_manager/json/front_main_category/parent_category_".$temp[$i]['id'].".json");
        }
    }
}


}
?>