<?php 

class api_menu_mobile extends CI_Model{
	function __construct()
	{
		parent::__construct();
	}
    

public function view($l,$api_web,$api_template,$api_page,$config_data){
    $api_web = $this->site->api_select_some_fields_with_where("
        facebook     
        "
        ,"sma_shop_settings"
        ,"shop_id = 1"
        ,"arr"
    );
            
    $return = '';
    $return .= '
<div id="api_menu_mobile" class="api_menu_mobile">
<nav class="mm-menu mm-horizontal mm-offcanvas mm-hasfooter mm-hassearch mm-current mm-opened">
    <div class="mm-search">
        <input placeholder="Search" type="text" autocomplete="off" >
    </div>
    <div class="mm-footer">
        <ul class="follow-us api_margin_left_0 api_padding_left_0">
            <li><a target="_blank" href="'.$api_web[0]['facebook'].'"><i class="fa fa-facebook"></i></a></li>
        </ul>
    </div>
        
    <ul class="mm-list mm-panel mm-opened mm-current" id="mm-0">
        <div class="mm-search-2 api_margin_bottom_10">
                <input id="af_header_menu_yamaha_search" placeholder="Search" type="text" onkeydown="if (event.keyCode == 13) { $(\'#product-search\').val(this.value); $(\'#api_menu_mobile_blocker\').click(); document.getElementById(\'product-search-form\').submit();}">
        </div>
    ';
    
    if ($this->session->userdata('group_id') == 1 || $this->session->userdata('group_id') == 2)
        $return .= '        
            <li id="api_menu_mobile_0"><a href="'.site_url('admin').'"><span>'.lang('dashboard').'</span></a></li>        
        ';
    $return .= '                    
        <li id="api_menu_mobile_1">                    
            <a href="'.base_url().'">        
				<span><div onclick="window.location = \''.base_url().'\'" class="api_display_inline_block">'.lang('home').' </div></span>
            </a>
    ';    
    $return .= '
        </li>
        <li id="api_menu_mobile_2">                    
            <a href="'.shop_url('products').'">        
				<span><div onclick="window.location = \''.shop_url('products').'\'" class="api_display_inline_block">'.lang('products').' </div></span>
            </a>                
        </li>
        <li id="api_menu_mobile_3">                    
			<span><div class="api_display_inline_block">'.lang('categories').' <span class="caret"></span></div></span>
        </li>
    ';

    $condition = '';
    if ($this->api_shop_setting[0]['air_base_url'] == base_url())
        $condition .= " and add_ons like '%:air_display:{yes}:%' ";

    $config_data = array(
        'table_name' => 'sma_categories',
        'select_table' => 'sma_categories',
        'translate' => 'yes',
        'select_condition' => "parent_id = 0 ".$condition." and add_ons LIKE '%:display:{yes}:%' order by name asc",
    );
    $temp = $this->site->api_select_data_v2($config_data);

    if (is_array($temp))
    if (count($temp) > 0){
        $return .= '
    <div class="api_menu_mobile_3_sub menu_panel" style="display: none;">
        ';
        for ($i=0;$i<count($temp);$i++) {

            $config_data = array(
                'table_name' => 'sma_categories',
                'select_table' => 'sma_categories',
                'translate' => 'yes',
                'select_condition' => "parent_id = ".$temp[$i]['id']." ".$condition." and add_ons LIKE '%:display:{yes}:%' order by name asc",
            );
            $temp3 = $this->site->api_select_data_v2($config_data);
        
            if (is_array($temp3))
            if (count($temp3) > 0) {
                $temp2 = '<span class="caret"></span>'; 
                $return .= '<a class="api_menu_mobile_3_sub_'.$temp[$i]['id'].' menu_panel">'.$temp[$i]['title_'.$this->api_shop_setting[0]['api_lang_key']].' '.$temp2.'</a>';
                $return .= '
                <div class="api_menu_mobile_3_sub_'.$temp[$i]['id'].'_drop menu_panel_sub">
                ';                
                    for ($j=0;$j<count($temp3);$j++) {
                        $return .= '
                        <a href="'.site_url('category/'.$temp[$i]['slug'].'/'.$temp3[$j]['slug']).'">
                            '.$temp3[$j]['title_'.$this->api_shop_setting[0]['api_lang_key']].'
                        </a>
                        ';
                    }
                    $return .= '
                    <a href="'.site_url('category/'.$temp[$i]['slug']).'">
                        '.lang('all_products').'
                    </a>
                    ';
                $return .= '
                </div>
                ';
                $temp_script .= '
                    $(".api_menu_mobile_3_sub_'.$temp[$i]['id'].'").click(function(){$(".api_menu_mobile_3_sub_'.$temp[$i]['id'].'_drop").toggle("fast")});
                    $(".api_menu_mobile_3_sub_'.$temp[$i]['id'].'_drop").toggle("fast");
                ';

            }
            else { 
                $temp2 = '';
                $return .= '
                <a href="'.site_url('category/'.$temp[$i]['slug']).'">
                    '.$temp[$i]['title_'.$this->api_shop_setting[0]['api_lang_key']].' '.$temp2.'
                </a>
                ';
            }
        }
        $return .= '
    </div>
        ';
$temp_script .= '
    $("#api_menu_mobile_3").click(function(){$(".api_menu_mobile_3_sub").toggle("fast")});
';        
    }
/*
    $return .= '            
        <li id="api_menu_mobile_4">                    
            <span><div class="api_display_inline_block">'.lang('brands').' <span class="caret"></span></div></span>
    ';
    $temp = $this->site->api_select_some_fields_with_where("
        *
        "
        ,"sma_brands"
        ,"id > 0 and slug != '' order by name asc"
        ,"arr"
    );
        
    if (is_array($temp))
    if (count($temp) > 0){
        $return .= '
            <div class="api_menu_mobile_4_sub menu_panel" style="display: none;">
        ';
        for ($i=0;$i<count($temp);$i++) {         
            $return .= '
                <a href="'.site_url('brand/'.$temp[$i]['slug']).'">
                    '.$temp[$i]['name'].'
                </a>
            ';
        }
        $return .= '
            </div">
        ';        
        $temp_script .= '
            $("#api_menu_mobile_4").click(function(){$(".api_menu_mobile_4_sub").toggle("fast")});
        ';        
    }
    $return .= '            
        </li>
    ';
*/

    $return .= '            
        <li id="api_menu_mobile_5">                    
            <a href="'.site_url('cart').'">        
				<span><div onclick="window.location = \''.site_url('cart').'\'" class="api_display_inline_block">'.lang('cart').' </div></span>
            </a>                
        </li>
        <li id="api_menu_mobile_6">                    
            <a href="'.site_url('cart/checkout').'">        
				<span><div onclick="window.location = \''.site_url('cart/checkout').'\'" class="api_display_inline_block">'.lang('checkout').' </div></span>
            </a>                
        </li>        
    </ul>
</nav>
</div>
<div id="api_menu_mobile_blocker" class="mm-slideout"></div>
<script>            
    $(document).ready(function(){
        $("#api_menu_mobile_button").click(function(){
            $("#api_menu_mobile").toggle("fast");
            $("#api_menu_mobile_blocker").toggle();
        });
        $("#api_menu_mobile_blocker").click(function(){
            $("#api_menu_mobile").toggle("fast");
            $("#api_menu_mobile_blocker").toggle();
        });
        '.$temp_script.'        
    });    
</script>
    ';
    return $return;    	
}


}
?>