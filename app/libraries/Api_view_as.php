<?php

class Api_view_as {

public function __construct()
{

}
public function __get($var)
{
    return get_instance()->$var;
}

public function blog_step($config_data_function) {
    $return = array();    
    $l = $config_data_function['l'];
    $select_data = $config_data_function['select_data'];
    for ($i=0;$i<count($select_data);$i++) {
        $temp_display_1 .= '
        <div class="api_admin api_admin_wrapper_blog_step_'.$select_data[$i]['id'].'">
            <table border="0">
            <tr>
            <td class="api_temp_td_1" width="50%" valign="top">
                <div class="api_big api_temp_title_1">
                    '.$select_data[$i]['title_2'].'
                </div>
                <div class="api_height_5"></div>
                <div class="api_big api_temp_title_2">
                    '.$select_data[$i]['title'].'
                </div>             
                <div class="api_height_10"></div>
                <div class="api_temp_title_3">
                    '.$select_data[$i]['description'].'
                </div>                           
            </td>
            <td class="api_temp_td_2" width="50%" valign="bottom" style="background-image:url('.$config_data_function['image_path'].'/'.$select_data[$i]['image'].');">
                <div class="api_temp_number">
                    '.str_pad('0',2,$i + 1).'
                </div>
            </td>
            </tr>
            </table>
        ';
        
        $field_data = array();
        $field_data_add = array();
        $field_data[0] = array(
            'type' => 'translate',
            'col_class' => 'col-md-6',
            'field_class' => '',
            'field_name' => 'translate',
            'translate' => 'yes',
            'field_label_name' => 'Title Top',                      
        );   
        $field_data[1] = array(
            'type' => 'translate',
            'col_class' => 'col-md-6',
            'field_class' => '',
            'field_name' => 'translate_2',
            'translate_2' => 'yes',
            'field_label_name' => 'Title Bottom',                      
        );   
        $field_data[2] = array(
            'type' => 'translate_textarea',
            'col_class' => 'col-md-12',
            'field_class' => '',
            'field_name' => 'description',
            'description' => 'yes',
            'field_label_name' => 'Description',                      
        );          
        $field_data[3] = array(
            'type' => 'file',
            'col_class' => 'col-md-4',
            'field_class' => '',
            'field_name' => 'image',    
            'field_label_name' => 'Image',
            'recommended_width' => 700,
            'recommended_height' => 450,
        );             
        $field_data[4] = array(
            'type' => 'text',
            'col_class' => 'col-md-4',
            'field_class' => 'api_numberic_input',
            'field_name' => 'ordering',
            'field_label_name' => 'Ordering',                      
        );
        $field_data_add[0] = array(
            'type' => 'hidden',
            'col_class' => 'col-md-12',
            'field_class' => '',
            'field_name' => 'c_id',
            'translate' => '',
            'field_label_name' => 'Category',   
            'field_value' => $config_data_function['category_id'],                   
        );   
        $config_data = array(
            'l' => $l,
            'wrapper_class' => 'api_admin_wrapper_blog_step_'.$select_data[$i]['id'],
            'modal_class' => 'modal-lg',
            'title' => 'Edit Feature', 
            'field_data' => $field_data,
            'selected_id' => $select_data[$i]['id'],
            'table_name' => 'sma_blog_step',
            'table_id' => 'id',
            'upload_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/blog_step',
            'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
            'field_data_add' => $field_data_add,
            'add' => 'yes',
            'title_add' => 'Add Feature',
            'delete' => 'yes',
            'title_delete' => '<div>Your will remove</div>'.$select_data[$i]['title'],
            'condition_ordering' => "and c_id = ".$config_data_function['category_id'],        
        );
        $temp_admin = $this->api_admin->front_end_edit($config_data);
        $temp_display_1 .= $temp_admin['display'];

        $temp_display_1 .= '
        </div>
        ';
        $temp_display_1 .= '
            <script>
                '.$temp_admin['script'].'
            </script>
        ';

        if ($i < count($select_data) - 1)
            $temp_display_1 .= $config_data_function['seperator'];
        
        $temp_display_2 .= '
            <table border="0">
            <tr>            
            <td class="api_temp_td_2" valign="bottom" style="background-image:url('.$config_data_function['image_path'].'/'.$select_data[$i]['image'].');">
                <div class="api_temp_number">
                    '.str_pad('0',2,$i + 1).'
                </div>
            </td>
            </tr>
            <tr>
            <td class="api_temp_td_1" width="50%" valign="top">
                <div class="api_big api_temp_title_1">
                    '.$select_data[$i]['title_2'].'
                </div>
                <div class="api_height_5"></div>
                <div class="api_big api_temp_title_2">
                    '.$select_data[$i]['title'].'
                </div>             
                <div class="api_height_10"></div>
                <div class="api_temp_title_3">
                    '.$select_data[$i]['description'].'
                </div>                           
            </td>         
            </tr>
            </table>   
        ';

        if ($i < count($select_data) - 1)
            $temp_display_2 .= $config_data_function['seperator'];

    }

    $return['display'] = '
        <div class="col-md-12 api_padding_0 api_view_as_blog_step">
            <div class="col-md-12 hidden-sm hidden-xs">
                '.$temp_display_1.'
            </div>
            <div class="col-md-12 visible-sm visible-xs api_kh_padding">
                '.$temp_display_2.'
            </div>
        </div>
    ';
    return $return;
} 

public function blog_step_v2($config_data_function) {
    $return = array();    
    $l = $config_data_function['l'];
    $select_data = $config_data_function['select_data'];
    for ($i=0;$i<count($select_data);$i++) {      
        $data_view = array (
            'wrapper_class' => '',
            'file_path' => $config_data_function['image_path'].'/'.$select_data[$i]['image'],
            'max_width' => 0,
            'max_height' => 0,
            'image_class' => $config_data_function['image_class'],
            'image_id' => '',   
            'resize_type' => 'fixed',       
        );
        $temp_image = $this->api_helper->api_get_image($data_view);

        $temp_display_1 .= '      
        <div class="api_admin api_admin_wrapper_blog_step_'.$select_data[$i]['id'].'">              
            <table border="0">
            <tr>
            <td class="api_temp_td_2" width="45%" valign="top">
                '.$temp_image['image_table'].'
            </td>            
            <td class="api_temp_td_1" width="55%" valign="top"  >
                <div class="api_big api_temp_title_1">
                    <span class="'.$config_data_function['step_class'].'">Step </span>'.str_pad('0',2,$i + 1).'
                </div>
                <div class="api_height_5"></div>
                <div class="api_big api_temp_title_2">
                    '.$select_data[$i]['title'].'
                </div>                             <div class="api_height_10"></div>
                <div class="api_temp_title_3">
                    '.$select_data[$i]['description'].'
                </div>                           
            </td>
            </tr>
            </table>   
        ';

        $field_data = array();
        $field_data_add = array();
        $field_data[0] = array(
            'type' => 'translate',
            'col_class' => 'col-md-12',
            'field_class' => '',
            'field_name' => 'translate',
            'translate' => 'yes',
            'field_label_name' => 'Title Top',                      
        );    
        $field_data[1] = array(
            'type' => 'translate_textarea',
            'col_class' => 'col-md-12',
            'field_class' => '',
            'field_name' => 'description',
            'description' => 'yes',
            'field_label_name' => 'Description',                      
        );          
        $field_data[2] = array(
            'type' => 'file',
            'col_class' => 'col-md-4',
            'field_class' => '',
            'field_name' => 'image',    
            'field_label_name' => 'Image',
            'recommended_width' => 700,
            'recommended_height' => 450,
        );             
        $field_data[3] = array(
            'type' => 'text',
            'col_class' => 'col-md-4',
            'field_class' => 'api_numberic_input',
            'field_name' => 'ordering',
            'field_label_name' => 'Ordering',                      
        );
        $field_data_add[0] = array(
            'type' => 'hidden',
            'col_class' => 'col-md-12',
            'field_class' => '',
            'field_name' => 'c_id',
            'translate' => '',
            'field_label_name' => 'Category',   
            'field_value' => $config_data_function['category_id'],                   
        );   
        $config_data = array(
            'l' => $l,
            'wrapper_class' => 'api_admin_wrapper_blog_step_'.$select_data[$i]['id'],
            'modal_class' => 'modal-lg',
            'title' => 'Edit Feature', 
            'field_data' => $field_data,
            'selected_id' => $select_data[$i]['id'],
            'table_name' => 'sma_blog_step',
            'table_id' => 'id',
            'upload_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/blog_step',
            'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
            'field_data_add' => $field_data_add,
            'add' => 'yes',
            'title_add' => 'Add Feature',
            'delete' => 'yes',
            'title_delete' => '<div>Your will remove</div>'.$select_data[$i]['title'],
            'condition_ordering' => "and c_id = ".$config_data_function['category_id'],        
        );
        $temp_admin = $this->api_admin->front_end_edit($config_data);
        $temp_display_1 .= $temp_admin['display'];
        $temp_display_1 .= '
            </div>
        ';
        $temp_display_1 .= '
            <script>
                '.$temp_admin['script'].'
            </script>
        ';
        
        if ($i < count($select_data) - 1)
            $temp_display_1 .= $config_data_function['seperator'];

        $temp_display_2 .= '                    
            <table border="0">
            <tr>
            <td class="api_temp_td_2" width="100%" valign="bottom">
                <img class="img-responsive" src="'.$config_data_function['image_path'].'/'.$select_data[$i]['image'].'" />            
            </td>       
            </tr>
            </table>   
            <table border="0">
            <tr>                            
            <td class="api_temp_td_1" width="100%" valign="top">
                <div class="api_big api_temp_title_1">
                    Step '.str_pad('0',2,$i + 1).'
                </div>
                <div class="api_height_5"></div>
                <div class="api_big api_temp_title_2">
                    '.$select_data[$i]['title'].'
                </div>             
                <div class="api_height_10"></div>
                <div class="api_temp_title_3">
                    '.$select_data[$i]['description'].'
                </div>                           
            </td>
            </tr>
            </table>               
        ';

        if ($i < count($select_data) - 1)
            $temp_display_2 .= $config_data_function['seperator'];        
    }

    $return['display'] = '
        <div class="col-md-12 api_padding_0 api_view_as_blog_step_v2">
            <div class="col-md-12 hidden-xs">
                '.$temp_display_1.'
            </div>
            <div class="col-md-12 visible-xs api_kh_padding">
                '.$temp_display_2.'
            </div>        
        </div>
    ';

    return $return;
} 

public function blog_step_v3($config_data_function) {
    $return = array();    
    $l = $config_data_function['l'];
    $select_data = $config_data_function['select_data'];
    for ($i=0;$i<count($select_data);$i++) {
        $temp_display_1 .= '
            <div class="api_admin api_admin_wrapper_blog_step_'.$select_data[$i]['id'].'">        
                <table border="0">
                <tr>
                <td class="api_temp_td_1" valign="top">
                    <div class="api_big api_temp_title_1">
                        '.$select_data[$i]['title'].'
                    </div>
                    <div class="api_big api_temp_title_1_underline">
                    </div>                
                    <div class="api_height_5"></div>         
                    <div class="api_height_10"></div>
                    <div class="api_temp_title_3">
                        '.$select_data[$i]['description'].'
                    </div>                           
                </td>
                <td class="api_temp_td_3">&nbsp;</td>
                <td class="api_temp_td_2" align="center" valign="bottom" style="background-image:url('.$config_data_function['image_path'].'/'.$select_data[$i]['image'].');">
                    <div class="api_temp_title_2_wrapper">
                        <div class="api_temp_title_2">
                            '.$select_data[$i]['title_2'].'
                        </div>
                    </div>
                </td>
                </tr>
                </table>
        ';
        
        $field_data = array();
        $field_data_add = array();
        $field_data[0] = array(
            'type' => 'translate',
            'col_class' => 'col-md-12',
            'field_class' => '',
            'field_name' => 'translate',
            'translate' => 'yes',
            'field_label_name' => 'Title',                      
        );    
        $field_data[1] = array(
            'type' => 'translate',
            'col_class' => 'col-md-12',
            'field_class' => '',
            'field_name' => 'translate_2',
            'translate_2' => 'yes',
            'field_label_name' => 'Title Over Image',                      
        );    
        $field_data[2] = array(
            'type' => 'translate_textarea',
            'col_class' => 'col-md-12',
            'field_class' => '',
            'field_name' => 'description',
            'description' => 'yes',
            'field_label_name' => 'Description',                      
        );          
        $field_data[3] = array(
            'type' => 'file',
            'col_class' => 'col-md-4',
            'field_class' => '',
            'field_name' => 'image',    
            'field_label_name' => 'Image',
            'recommended_width' => 700,
            'recommended_height' => 450,
        );             
        $field_data[4] = array(
            'type' => 'text',
            'col_class' => 'col-md-4',
            'field_class' => 'api_numberic_input',
            'field_name' => 'ordering',
            'field_label_name' => 'Ordering',                      
        );
        $field_data_add[0] = array(
            'type' => 'hidden',
            'col_class' => 'col-md-12',
            'field_class' => '',
            'field_name' => 'c_id',
            'translate' => '',
            'field_label_name' => 'Category',   
            'field_value' => $config_data_function['category_id'],                   
        );   
        $config_data = array(
            'l' => $l,
            'wrapper_class' => 'api_admin_wrapper_blog_step_'.$select_data[$i]['id'],
            'modal_class' => 'modal-lg',
            'title' => 'Edit Feature', 
            'field_data' => $field_data,
            'selected_id' => $select_data[$i]['id'],
            'table_name' => 'sma_blog_step',
            'table_id' => 'id',
            'upload_path' => 'assets/uploads/web/'.$this->api_web[0]['id'].'/blog_step',
            'redirect' => $this->api_shop_setting[0]['api_temp_current_url'],
            'field_data_add' => $field_data_add,
            'add' => 'yes',
            'title_add' => 'Add',
            'delete' => 'yes',
            'title_delete' => '<div>Your will remove</div>'.$select_data[$i]['title'],
            'condition_ordering' => "and c_id = ".$config_data_function['category_id'],        
        );
        $temp_admin = $this->api_admin->front_end_edit($config_data);
        $temp_display_1 .= $temp_admin['display'];
        $temp_display_1 .= '
            </div>
        ';
        $temp_display_1 .= '
            <script>
                '.$temp_admin['script'].'
            </script>
        ';

          
        if ($i < count($select_data) - 1)
            $temp_display_1 .= $config_data_function['seperator'];
        
        $temp_display_2 .= '
            <table border="0">
            <tr>
            <td class="api_temp_td_2" align="center" valign="bottom" style="background-image:url('.$config_data_function['image_path'].'/'.$select_data[$i]['image'].');">
                <div class="api_temp_title_2_wrapper">
                    <div class="api_temp_title_2">
                        '.$select_data[$i]['title_2'].'
                    </div>
                </div>
            </td>
            </tr>
            <tr>
            <td class="api_temp_td_1" width="50%" valign="top">
                <div class="api_big api_temp_title_1">
                    '.$select_data[$i]['title'].'
                </div>
                <div class="api_big api_temp_title_1_underline">
                </div>                
                <div class="api_height_5"></div>         
                <div class="api_height_10"></div>
                <div class="api_temp_title_3">
                    '.$select_data[$i]['description'].'
                </div>                           
            </td>
            </tr>
            </table>
        ';

        if ($i < count($select_data) - 1)
            $temp_display_2 .= $config_data_function['seperator'];

    }

    $return['display'] = '
        <div class="col-md-12 api_padding_0 api_view_as_blog_step_v3">
            <div class="col-md-12 hidden-xs">
                '.$temp_display_1.'
            </div>
            <div class="col-md-12 visible-xs api_kh_padding">
                '.$temp_display_2.'
            </div>
        </div>
    ';

    return $return;
} 

public function large_icon_v2($config_data_function) {
    $return = array();    
    $l = $config_data_function['l'];
    $select_data = $config_data_function['select_data'];
    for ($i=0;$i<count($select_data);$i++) {
        $data_view = array (
            'wrapper_class' => 'hidden-sm hidden-xs',
            'file_path' => $config_data_function['image_path'].'/'.$select_data[$i]['image'],
            'max_width' => 600,
            'max_height' => 350,
            'product_link' => '',
            'image_class' => '',
            'image_id' => '',   
            'resize_type' => 'full',       
        );
        $temp_image = $this->api_helper->api_get_image($data_view);
        $data_view = array (
            'wrapper_class' => 'visible-sm visible-xs',
            'file_path' => $config_data_function['image_path'].'/'.$select_data[$i]['image'],
            'max_width' => 700,
            'max_height' => 300,
            'product_link' => '',
            'image_class' => 'img-responsive',
            'image_id' => '',   
            'resize_type' => 'full',       
        );
        $temp_image_mobile = $this->api_helper->api_get_image($data_view);
    
        $temp_display .= '
            <div class="col-md-6">
                <div class="api_big api_temp_title">
                    '.$select_data[$i]['title'].'
                </div>
                <div class="api_temp_image">
                    '.$temp_image['image_full_src'].'
                    '.$temp_image_mobile['image_table'].'
                </div>                
                <div class="api_temp_description" id="api_temp_id_'.$i.'">
                    '.$select_data[$i]['description'].'
                </div>
            </div>
        ';
    }

    $return['display'] = '
        <div class="col-md-12 api_padding_0 api_view_as_large_icon_v2">
            <div class="col-md-12 api_temp_wrapper_1">
                '.$temp_display.'
            </div>
        </div>
    ';

    return $return;
}

public function large_icon_cart($config_data_function) {
    $return = array();    
    $l = $this->api_shop_setting[0]['api_lang_key'];
    $select_data = $config_data_function['select_data'];
    for ($i=0;$i<count($select_data);$i++) {    

        $temp_path = str_replace(base_url(),'',$config_data_function['image_path']);
        $temp_image = $select_data[$i]['image'];
        if (!is_file($temp_path.'/'.$select_data[$i]['image']))
            $temp_image = 'no_image.png';

        $config_data_3 = array(
            'id' => $select_data[$i]['id'],
            'duration_left_align' => 'right',
            'duration_left_break_line' => 0,
        );
        $temp_price_discount = $this->api_helper->price_discount($config_data_3);
        
        $temp_cart_qty = '
            <table width="100%" border="0">
            <tr>
            <td valign="middle" class="api_td_width_auto">
                <span class="input-group-addon pointer btn-minus api_temp_span_left" onclick="
                var postData = {
                    \'id\' : \''.$select_data[$i]['id'].'\',
                    \'min_qty\' : \''.$temp_price_discount['min_qty'].'\',
                }
                api_temp_default_cart_minus_click(postData);
            ">
                    <span class="fa fa-minus"></span>
                </span>
            </td>
            <td valign="middle">
                <input type="tel" name="quantity" id="api_product_list_quantity_'.$select_data[$i]['id'].'" onmouseout="
                    sma_input_qty_mouse_out(\'api_product_list_quantity_'.$select_data[$i]['id'].'\','.$temp_qty.');
                " class="form-control text-center api_numberic_input quantity-input api_temp_input" value="'.$temp_price_discount['min_qty'].'" min_qty="'.$temp_price_discount['min_qty'].'">            
            </td>
            <td valign="middle" class="api_td_width_auto">
                <span class="input-group-addon pointer btn-plus api_temp_span_right" onclick="
                    var postData = {
                        \'id\' : \''.$select_data[$i]['id'].'\',
                    }
                    api_temp_default_cart_plus_click(postData);
                ">
                    <span class="fa fa-plus"></span>
                </span>            
            </td>            
            </tr>
            </table>        
        ';      

        $temp_is_favorite = $this->api_helper->is_favorite($select_data[$i]['id']);
        if ($temp_is_favorite == 1) {
            $temp_favorite = 'fa-heart';
            $temp_title = lang('remove_favorite');
        }
        else {
            $temp_favorite = 'fa-heart-o';
            $temp_title = lang('add_favorite');
        }
        $temp_btn_cart = '
            <table width="100%" border="0">
            <tr>
            <td valign="middle" class="api_td_width_auto">
                <button class="btn btn-info add-to-wishlist api_temp_btn_favorite" data-id="'.$select_data[$i]['id'].'" title="'.$temp_title.'">
                    <i class="fa '.$temp_favorite.' category_wishlist_heart_'.$select_data[$i]['id'].'"></i>
                </button>
            </td>
            <td valign="middle">
                <button class="btn btn-danger api_temp_btn_cart '.$temp_display_1.'" '.$temp_sold_out[2].' data-id="'.$select_data[$i]['id'].'" onclick="
                    var postData = {
                        \'image_class\' : \'.api_view_as_large_icon_cart .api_temp_image_'.$select_data[$i]['id'].'\',
                        \'product_id\' : \''.$select_data[$i]['id'].'\',
                        \'qty\' : $(\'#api_product_list_quantity_'.$select_data[$i]['id'].'\').val(),
                        \'cart_type\' : \'front\',
                        \'ajax_url\' : \''.base_url().'cart/api_cart_express_ajax_add_cart\',
                    };
                    api_cart_express_ajax_add_cart(postData);
                ">
                    <i class="fa fa-shopping-cart padding-right-md"></i> 
                    '.lang('add_to_cart').'
                </button>
            </td>   
            </tr>
            </table>              
        ';

        $temp_display .= '
            <div class="'.$config_data_function['col_class'].'" >
                <div class="'.$config_data_function['box_class'].' api_temp_box">            
                    <table class="api_temp_table" width="100%" border="0">
                    <tr>
                    <td valign="middle" align="center" class="api_temp_td_1 api_pointer" onclick="
                        api_get_data = {
                            \'slug_0\' : \'product-detail\',
                            \'slug_1\' : \''.$select_data[$i]['slug'].'\',
                        };
                        var postData = {
                            \'page\' : \'product_detail\',
                            \'get_data\' : api_get_data,
                        };
                        api_ajax_load_page(postData);                    
                    ">
                        <div class="api_temp_image_wrapper '.$config_data_function['image_wrapper_class'].' api_temp_box_image">
                            <img class="'.$config_data_function['image_class'].' api_temp_image_'.$select_data[$i]['id'].'" src="'.$config_data_function['image_path'].'/'.$temp_image.'" />
                        </div>
                    </td>
                    <tr>
                    <td valign="top" class="api_pointer" onclick="
                        api_get_data = {
                            \'slug_0\' : \'product-detail\',
                            \'slug_1\' : \''.$select_data[$i]['slug'].'\',
                        };
                        var postData = {
                            \'page\' : \'product_detail\',
                            \'get_data\' : api_get_data,
                        };
                        api_ajax_load_page(postData);                    
                    ">
                        <div class="api_height_10"></div>
                        <div class="api_temp_title api_text_line_3">                                                  
                            '.$select_data[$i]['title'].'
                        </div>
                        <div class="api_temp_price">                    
                            '.$temp_price_discount['display_v2'].'
                        </div>
                    </td>
                    </tr>
                    <tr>
                    <td valign="bottom">
                        <div class="api_cursor_normal">
                            '.$temp_cart_qty.'
                            <div class="api_height_3"></div>
                            '.$temp_btn_cart.'
                        </div>               
                    </td>
                    </tr>
                    </table>
                </div>
            </div>
        ';
    }

    $return['display'] = '
        <div class="col-md-12 api_padding_0 api_view_as_large_icon_cart">
            <div class="col-md-12 api_temp_wrapper_1 '.$config_data_function['wrapper_class'].'">
                '.$temp_display.'
                <div class="api_clear_both"></div>
            </div>
        </div>
    ';

    return $return;
}

public function table_order_list($config_data_function) {
    $return = array();    
    $l = $this->api_shop_setting[0]['api_lang_key'];
    $select_data = $config_data_function['select_data'];

    if ($this->api_shop_setting[0]['display_company'] == 1)
        $temp_th_customer_label = lang('Company');
    else
        $temp_th_customer_label = lang('Name');

    $temp_display .= '
        <table width="100%" class="table table-striped table-hover table-va-middle" border="0">
        <thead>
            <tr>
                <th>
                    ID
                </th>
                <th>
                    '.lang('Date').'
                </th>
                <th>
                    '.lang('reference_number').'
                </th>                
                <th>
                    '.$temp_th_customer_label.'
                </th>
                <th align="right" class="api_text_align_right">
                    '.lang('Total').'
                </th>
                <th align="center" width="130" class="api_text_align_center">
                    '.lang('Status').'
                </th>
                <th align="center" class="api_td_width_auto">
                    '.lang('Action').'
                </th>
            </tr>
        </thead>
    ';
    for ($i=0;$i<count($select_data);$i++) {
        if (($k%2) != 0) 
            $class_record = "api_record_odd"; 
        else 
            $class_record = "api_record_even";
        $temp_display .= '
            <tr class="product">
                <td valign="middle" align="center" class="api_td_width_auto">
                    #'.$select_data[$i]['id'].'
                </td>
                <td valign="middle">
                    '.$select_data[$i]['date_display'].'
                </td>
                <td>
                    '.$select_data[$i]['reference_no'].'
                </td>                
                <td>
                    '.$select_data[$i]['customer'].'
                </td>
                <td align="right">
                    '.$this->sma->formatMoney($select_data[$i]['grand_total']).'
                    <div class="api_height_5"></div>
                    '.$select_data[$i]['payment_display'].'
                </td>
                <td valign="middle" align="center" class="api_td_width_auto">
                    '.$select_data[$i]['status_display'].'
                </td>
                <td valign="middle" align="center" class="api_td_width_auto">
                    <div class="btn-group" role="group">
                        <a class="api_table_view" href="javascript:void(0);" onclick="
                            api_get_data = {
                                \'slug_0\' : \''.$config_data_function['slug_0'].'\',
                                \'slug_1\' : \''.$select_data[$i]['id'].'\',
                            };
                            var postData = {                    
                                \'page\' : \''.$config_data_function['page_name'].'\',
                                \'get_data\' : api_get_data,
                            };
                            api_ajax_load_page(postData);
                        " title="'.lang('View').'">
                            <button type="button" class="btn btn-sm btn-danger">
                                <i class="fa fa-eye"></i>
                            </button>
                        </a>
                    </div>
                </td>     
            </tr>
        ';
    }
    $temp_display .= '
        </table>
    ';

    if (count($select_data) > 0)
        $return['display'] = '
            <div class="col-md-12 api_padding_0 table_order_list">
                <div class="col-md-12 api_temp_wrapper_1 '.$config_data_function['wrapper_class'].'">
                    '.$temp_display.'
                    <div class="api_clear_both"></div>
                </div>
            </div>
        ';

    return $return;
}

public function restaurant_menu_list($config_data_function) {
    $return = array();
    $select_data = $config_data_function['select_data'];
    
    $api_cart_express = new api_cart_express;
    $config_data = array(
        'table_id' => $config_data_function['table_id'],
    );
    $initial_data = $api_cart_express->initial_data_restaurant($config_data);        

    for ($i=0;$i<count($select_data);$i++) {

        $config_data = array(
            'product_id' => $select_data[$i]['id'],
            'select_data' => $initial_data,
        );        
        $temp_qty = $api_cart_express->get_quantity_by_product_id($config_data);
        $return['display'] .= ' 
            <div class="api_temp_box" id="api_temp_box_'.$select_data[$i]['cart_id'].'">
                <table width="100%" border="0">
                <tr>
               
                <td valign="top" class="api_td_width_auto api_temp_td_1">
        ';
                        
            $return['display'] .= '
                    <div class="'.$config_data_function['image_wrapper_class'].'">
                        <img class="api_temp_image" src="'.base_url().'assets/uploads/thumbs/'.$select_data[$i]['image'].'" />
                    </div>
            ';
            $return['display'] .= '
                </td>
                <td valign="top" class="api_temp_td_2">
                <div class="api_position_relative">
                    <div class="'.$config_data_function['title_class'].'">
            ';
                        if ($config_data_function['link_disable'] != 1)
                        $return['display'] .= '
                            <a class="api_temp_a_2" href="'.$select_data[$i]['link'].'">
                        ';
                        $return['display'] .= '
                            '.$select_data[$i]['title'].'
                        ';
                        if ($config_data_function['link_disable'] != 1)
                        $return['display'] .= '
                            </a>
                        ';
                $return['display'] .= '
                    </div>
                    <div class="api_temp_price">
                        '.$select_data[$i]['price_label'].'
                    </div>                
                ';

                if ($select_data[$i]['is_promotion'] == 1)
                    $return['display'] .= '
                        <div class="api_temp_promotion">
                            <span class="api_temp_promotion_label">
                                '.$select_data[$i]['promotion_label'].'
                            </span>
                            &nbsp;
                            <span class="api_temp_promotion_rate_label">
                                '.$select_data[$i]['promotion_rate_label'].' <small>Off</small>
                            </span>
                            &nbsp;              
                            <span class="'.$config_data_function['duration_class'].' label label-info api_border_r10">
                                '.$select_data[$i]['promotion_duration_left'].'
                            </span>                    
                        </div>                
                    ';

                if ($select_data[$i]['ordered'] == 0)
                    $return['display'] .= '
                        <div class="api_height_10"></div>
                        
                    ';

                if ($temp_qty <= $select_data[$i]['min_qty'])
                    $temp = 'api_temp_disabled';
                else
                    $temp = '';

                
                if($temp_qty >= 1) {
                $return['display'] .= '                            
                    <div class="api_temp_btn_qty visible-xs">
                        <table width="100%" border="0">
                        <tr>
                        <td valign="middle" align="center" height="25">
                            <a href="javascript:void(0);" class="api_temp_button_sub_'.$select_data[$i]['id'].' '.$temp.'" onclick="
                                var postData = {
                                    \'cart_id\' : \''.$select_data[$i]['id'].'\',
                                    \'cart_type\' : \''.$config_data_function['cart_type'].'\',
                                    \'type\' : \'sub\',
                                    \'qty\' : \''.$temp_qty.'\',
                                    \'min_qty\' : \''.$select_data[$i]['min_qty'].'\',
                                    \'product_id\' : \''.$select_data[$i]['id'].'\',
                                    \'table_id\' : \''.$config_data_function['table_id'].'\',
                                    \'ajax_url\' : \''.base_url().'admin/restaurant/api_cart_express_ajax_update_cart_qty_restaurant\',                                  
                                };
                                api_cart_express_ajax_update_cart_qty_restaurant(postData); 
                            ">
                                <li class="fa fa-minus api_font_size_12"></li>
                            </a>
                        </td>
                        <td class="api_padding_left_5 api_padding_right_5" valign="middle" align="center" height="25">
                            <div class="api_temp_qty_'.$select_data[$i]['cart_id'].' api_margin_top_2">
                                '.$temp_qty.'
                            </div>
                        </td>
                        <td valign="middle" align="center" height="25">
                            <a href="javascript:void(0);" class="api_temp_button_add_'.$select_data[$i]['id'].' '.$temp.'" onclick="
                                var postData = {
                                    \'cart_id\' : \''.$select_data[$i]['id'].'\',
                                    \'cart_type\' : \''.$config_data_function['cart_type'].'\',
                                    \'type\' : \'add\',
                                    \'qty\' : \''.$temp_qty.'\',
                                    \'min_qty\' : \''.$select_data[$i]['min_qty'].'\',
                                    \'product_id\' : \''.$select_data[$i]['id'].'\',
                                    \'table_id\' : \''.$config_data_function['table_id'].'\',
                                    \'ajax_url\' : \''.base_url().'admin/restaurant/api_cart_express_ajax_update_cart_qty_restaurant\',                                    
                                };
                                api_cart_express_ajax_update_cart_qty_restaurant(postData);                        
                            ">
                                <li class="fa fa-plus api_font_size_12"></li>
                            </a>
                        </td>
                        </tr>
                        </table>                           
                    </div>
                ';
                }
                else{ 
                $return['display'] .= '                            
                    <div class="api_temp_btn_qty_v2 visible-xs">
                        <table width="100%" border="0">
                        <tr>
                        <td valign="middle" align="center" height="24"  width="24">
                            <a href="javascript:void(0);" class="api_temp_button_add_'.$select_data[$i]['id'].' '.$temp.'" onclick="
                                var postData = {
                                    \'cart_id\' : \''.$select_data[$i]['id'].'\',
                                    \'cart_type\' : \''.$config_data_function['cart_type'].'\',
                                    \'type\' : \'add\',
                                    \'qty\' : \''.$temp_qty.'\',
                                    \'min_qty\' : \''.$select_data[$i]['min_qty'].'\',
                                    \'product_id\' : \''.$select_data[$i]['id'].'\',
                                    \'table_id\' : \''.$config_data_function['table_id'].'\',
                                    \'ajax_url\' : \''.base_url().'admin/restaurant/api_cart_express_ajax_update_cart_qty_restaurant\',                            
                                };
                                api_cart_express_ajax_update_cart_qty_restaurant(postData);                        
                            ">
                                <li class="fa fa-plus api_font_size_12"></li>
                            </a>
                        </td>
                        </tr>
                        </table>                           
                    </div>
                ';
                }
            $return['display'] .= '   
                </div>
                </td>
                <td valign="top" align="right" class="api_temp_td_3 hidden-xs">
                    <div class="api_height_25"></div>
                    <table border="0">
                    <tr>
            ';
                if ($temp_qty <= $select_data[$i]['min_qty'])
                    $temp = 'api_temp_button_calculation_disable';
                else
                    $temp = 'api_temp_button_calculation';
              
            $return['display'] .= '
                <td valign="middle" align="center" width="24" height="24">
                    <div class="api_temp_button_sub_'.$select_data[$i]['id'].' '.$temp.'" onclick="
                    var postData = {
                        \'cart_id\' : \''.$select_data[$i]['id'].'\',
                        \'cart_type\' : \''.$config_data_function['cart_type'].'\',
                        \'type\' : \'sub\',
                        \'qty\' : \''.$temp_qty.'\',
                        \'min_qty\' : \''.$select_data[$i]['min_qty'].'\',
                        \'product_id\' : \''.$select_data[$i]['id'].'\',
                        \'table_id\' : \''.$config_data_function['table_id'].'\',
                        \'ajax_url\' : \''.base_url().'admin/restaurant/api_cart_express_ajax_update_cart_qty_restaurant\',                                  
                    };
                    api_cart_express_ajax_update_cart_qty_restaurant(postData);                        
                ">                            
                        <i class="fa fa-minus api_font_size_12"></i>
                    </div>
                </td>
                <td class="api_padding_left_10 api_padding_right_10" valign="middle" align="center" height="24">
                    <span class="api_temp_title_2 api_temp_qty_'.$select_data[$i]['id'].'">'.$temp_qty.'</span>
            ';
            $return['display'] .= '
                <td valign="middle" align="center" width="24" height="24">
                    <div id="api_temp_button_add_'.$select_data[$i]['id'].'" class="api_temp_button_calculation" onclick="
                        var postData = {
                            \'cart_id\' : \''.$select_data[$i]['id'].'\',
                            \'cart_type\' : \''.$config_data_function['cart_type'].'\',
                            \'type\' : \'add\',
                            \'qty\' : \''.$temp_qty.'\',
                            \'min_qty\' : \''.$select_data[$i]['min_qty'].'\',
                            \'product_id\' : \''.$select_data[$i]['id'].'\',
                            \'table_id\' : \''.$config_data_function['table_id'].'\',
                            \'ajax_url\' : \''.base_url().'admin/restaurant/api_cart_express_ajax_update_cart_qty_restaurant\',                                    
                        };
                        api_cart_express_ajax_update_cart_qty_restaurant(postData);                        
                    ">
                        <i class="fa fa-plus api_font_size_12"></i>
                    </div>
                </td>
                </tr>
                </table>
                <div class="api_height_10"></div>
            ';

            if ($select_data[$i]['min_qty'] > 1)
            $return['display'] .= '
                <div class="api_temp_title_1">
                    '.lang('Minimum order quantity').' '.$select_data[$i]['min_qty'].'
                </div>
            ';
            $return['display'] .= '                            
                    </td>
                    </tr>
                    </table>
                </div>
            ';
    }
    return $return;
}

}


