<?php

class Api_helper {

public function __construct()
{

}
public function __get($var)
{
    return get_instance()->$var;
}

public function api_select_some_fields_with_where($field_names, $table_name, $condition, $type) {
    $temp = 'Select Field ==> '.$field_names.'<br/>'.'Select Table ==> '.$table_name.'<br/>'.'Condition ==> '.$condition.'<br/>';        
    $sql = "SELECT $field_names FROM $table_name WHERE ".$condition;    
    $query = $this->db->query($sql)or die($this->db->error().$temp);	  
    if ($type == 'arr')
    {
        return $query->result_array();			
    }
    else if ($type == 'obj')
    {
        return $query->result();
    }
    $query->free_result();         	
}

public function api_get_condition_add_ons_field($add_ons_field,$table,$condition){
    $return = '';
    $temp = $this->api_select_some_fields_with_where("add_ons",$table,$condition,"arr");
    if (isset($temp[0]["add_ons"])) {
        $temp = $this->api_get_add_ons_field($temp[0]["add_ons"]);
        foreach(array_keys($temp) as $key){
            $return .= ",getTranslate(".$add_ons_field.",'".$key."','".f_separate."','".v_separate."') as ".$key;
        }
    }
    return $return;
}

public function api_get_condition_add_ons_field_v2($config_data){
    // $config_data_2 = array(
    //     'add_ons_table_name' => $config_data['add_ons_table_name'],
    //     'table_name' => $config_data['sma_products'],
    // );
    // $temp = $this->api_get_condition_add_ons_field_v2($config_data_2);    
    $return = array();
    $temp = $this->api_select_some_fields_with_where("add_ons",$config_data['add_ons_table_name'],"table_name = '".$config_data['table_name']."'","arr");
    if (isset($temp[0]["add_ons"])) {
        $temp = $this->api_get_add_ons_field($temp[0]["add_ons"]);
        $return['key'] = $temp;
        foreach(array_keys($temp) as $key){
            $return['condition'] .= ",getTranslate(".$add_ons_field.",'".$key."','".f_separate."','".v_separate."') as ".$key;
        }
    }
    return $return;
}

public function api_get_add_ons_field($add_on_value){
    $return = array();
    $temp = html_entity_decode($add_on_value);
    $temp = str_replace('https:','',$temp);
    $temp = str_replace('http:','',$temp);    
    $temp = explode(':',$temp);
    $j = 1;
    $k = 0;
    for ($i=0;$i<count($temp);$i++) {
        if (($j%2) != 0) 
            $temp_1[$k] = $temp[$i];
        else {
            $temp_2[$k] = $temp[$i];
            $k++;
        }
        $j++;
    }
    for ($i=0;$i<count($temp_1);$i++) {
        if ($temp_1[$i] != '') {
            $temp_2[$i] = substr($temp_2[$i],1);
            $temp_2[$i] = substr($temp_2[$i],0,-1);
            $return[$temp_1[$i]] = $temp_2[$i];
        }
    }
    return $return;
}

public function api_select_data_v2($config_data)
{
    if ($config_data['translate'] == 'yes') {
        $language_array = unserialize(multi_language);
        for ($i=0;$i<count($language_array);$i++) {                    
            $temp_translate .= ", getTranslate(translate,'".$language_array[$i][0]."','".f_separate."','".v_separate."') as title_".$language_array[$i][0];                
        }
    }
    $temp_field = $this->api_get_condition_add_ons_field("add_ons","sma_add_ons","table_name = '".$config_data['table_name']."'");
    $select_data = $this->api_select_some_fields_with_where("*
        ".$temp_translate."
        ".$temp_field
        ,$config_data['select_table']
        ,$config_data['select_condition']
        ,"arr"
    );
    return $select_data;        
}

public function display_product_custom_field($config_data, $post, $product) {
    $temp3 = trim(strtolower($config_data['name']));
    $temp3 = str_replace(' ','_',$temp3);      

    if (strtolower($config_data['unit']) == 'select product unit') {       
        if ($config_data['temp_short_select_unit'] != 1) {
            $temp4 = ' ('.$config_data['select_unit'][$i]['code'].')';
            $temp5 = lang("Please_select_a_unit");
        }
        else {
            $temp4 = '';
            $temp5 = lang("?");
        }

        $return['display'] .= '
            <div class="form-group standard">
                <label class="control-label">
                    '.lang($config_data['name']).'
                </label>
                <div class="controls"> 
        ';
                $tr_unit[''] = $temp5;
                for ($i=0;$i<count($config_data['select_unit']);$i++) {
                    $tr_unit[$config_data['select_unit'][$i]['id']] = $config_data['select_unit'][$i]['name'].$temp4;
                }                            
                $return['display'] .= form_dropdown('api_custom_field_'.$config_data['id'], $tr_unit, set_value('api_custom_field_'.$config_data['id'], $product->{'api_custom_field_'.$config_data['id']}), ' class="form-control" '.$config_data['temp_onclick'].' ');
        $return['display'] .= '
                </div>
            </div>    
        ';                
    }
    elseif (strtolower($config_data['unit']) == 'select purchasing currency') {
        $return['display'] .= '            
            <div class="form-group standard">
                <label class="control-label">
                    '.lang($config_data['name']).'
                </label>
                <div class="controls"> 
        ';
                $tr_unit[''] = lang("Please_select_a_currency");
                for ($i=0;$i<count($config_data['select_currency_purchase']);$i++) {
                    $tr_unit[$config_data['select_currency_purchase'][$i]['id']] = $config_data['select_currency_purchase'][$i]['name'];
                }                            
                $return['display'] .= form_dropdown('api_custom_field_'.$config_data['id'], $tr_unit, set_value('api_custom_field_'.$config_data['id'], $product->{'api_custom_field_'.$config_data['id']}), ' class="form-control" id="api_custom_field_select_'.$temp3.'" '.$config_data['temp_onclick'].' ');
        $return['display'] .= '
                </div>
            </div>    
        ';                    
    }
    elseif (strtolower($config_data['unit']) == 'select tax category') {            
        $return['display'] .= '
            <div class="form-group standard">
                <label class="control-label">
                    '.lang($config_data['name']).'
                </label>
                <div class="controls"> 
        ';
                $tr_unit[''] = lang("Please_select_a_tax_category");
                for ($i=0;$i<count($config_data['select_tax_category']);$i++) {
                    $tr_unit[$config_data['select_tax_category'][$i]['id']] = $config_data['select_tax_category'][$i]['name'];
                }                            
                $return['display'] .= form_dropdown('api_custom_field_'.$config_data['id'], $tr_unit, set_value('api_custom_field_'.$config_data['id'], $product->{'api_custom_field_'.$config_data['id']}), ' class="form-control" id="api_custom_field_select_'.$temp3.'" '.$config_data['temp_onclick'].' ');
        $return['display'] .= '
                </div>
            </div>    
        ';                    
    }        
    elseif (strtolower($config_data['unit']) == 'select country') {
        $return['display'] .= '
            <div class="form-group standard">
                <label class="control-label">
                    '.lang($config_data['name']).'
                </label>
                <div class="controls"> 
        ';
                $tr_unit[''] = lang("Please_select_a_country");
                for ($i=0;$i<count($config_data['select_country']);$i++) {
                    $tr_unit[$config_data['select_country'][$i]['id']] = $config_data['select_country'][$i]['name'];
                }                            
                $return['display'] .= form_dropdown('api_custom_field_'.$config_data['id'], $tr_unit, set_value('api_custom_field_'.$config_data['id'], $product->{'api_custom_field_'.$config_data['id']}), ' class="form-control" id="api_custom_field_select_'.$temp3.'" '.$config_data['temp_onclick'].' ');
        $return['display'] .= '
                </div>
            </div>    
        ';                    
    }     
    else {
        if ($config_data['unit'] == '' || $config_data['unit'] == '-') {
            $temp = '';
            $temp2 = '';
        }
        else {                
            $temp = '
                <span id="span_custom_field_unit_'.$temp3.'" class="input-group-addon" style="padding: 1px 10px;">
                    '.$config_data['unit'].'
                </span>                   
            ';
            $temp2 = 'input-group';
        }

        $return['display'] = '
            <div class="form-group all">
                <label>'.lang($config_data['name']).'</label> 
                <div class="'.$temp2.'">
                    '.form_input('api_custom_field_'.$config_data['id'], (isset($post['api_custom_field_'.$config_data['id']]) ? $post['api_custom_field_'.$config_data['id']] : ($product ? $product->{'api_custom_field_'.$config_data['id']} : '')), 'class="form-control '.$config_data['temp_class'].'" '.$config_data['temp_readonly'].' id="api_custom_field_input_'.$temp3.'" '.$config_data['temp_onclick'].' ').'
                    '.$temp.'
                </div>
            </div>
        ';
    }
    return $return;
}

public function display_status($config_data) {
    $return = array();
    $temp_bg = '';
    if ($config_data['status'] == '' || $config_data['status'] == 'pending') 
        $temp = 'warning';
    if ($config_data['status'] == 'completed' || $config_data['status'] == 'received') 
        $temp = 'success';
    if ($config_data['status'] == 'due') 
        $temp = 'danger';
    if ($config_data['status'] == 'ordered') {
        $temp = 'danger';
        $temp_bg = 'background-color:#7ec814';
    }
    if ($config_data['status'] == 'confirmed') {
        $temp = 'danger';
        $temp_bg = 'background-color:#629e0c';
    }    
    if ($config_data['status'] == 'on the way') 
        $temp = 'info';

    $return['display'] = '
        <span class="label label-'.$temp.' api_text_transform_capitalize" style="'.$temp_bg.'">'.$config_data['status'].'</span>
    ';

    return $return;
}

public function select_currency($config_data) {
    if ($config_data['id'] > 0) {
        $select_data = $this->site->api_select_some_fields_with_where("
            *
            "
            ,"sma_currencies"
            ,"id = ".$config_data['id']
            ,"arr"
        );    
    }
    return $select_data;
}


public function insert_kh_currency_from_json($config_data) {
    $temp = file_get_contents('https://dev-invoice.camboinfo.com/assets/api/data/stock_manager/json/kh_currencies.json');
    $result = json_decode($temp, true);
    $temp_1 = date_create($result['date']);
    $temp_currency = $this->site->api_select_some_fields_with_where("
        *
        "
        ,"sma_currencies"
        ,"code = 'KHR' and YEAR(date) = YEAR('".date('Y-m-d')."') and MONTH(date) = MONTH('".date('Y-m-d')."') and DAY(date) = DAY('".date('Y-m-d')."') order by date desc limit 1"
        ,"arr"
    );
    if (count($temp_currency) <= 0) {
        if (date_format('Y-m-d',$temp_1) != date('Y-m-d')) {
            $yesterday_kh_rate = 1;
            $result['date'] = date('Y-m-d');        
            $temp = $this->site->api_select_some_fields_with_where("
                *
                "
                ,"sma_currencies"
                ,"code = 'KHR' order by id desc limit 1"
                ,"arr"
            );
            $temp_2 = array(
                'date' => date('Y-m-d'),
                'code' => 'KHR',
                'name' => 'Khmer Riel',
                'symbol' => 'KHR',
                'rate' => $temp[0]['rate'],
            );
            $this->db->insert('sma_currencies', $temp_2);    
        }            
        else {
            $yesterday_kh_rate = 0;
            $result['date'] = date('Y-m-d');        
            $this->db->insert('sma_currencies', $result);    
        }

        $temp = $this->site->api_select_some_fields_with_where("
            id
            "
            ,"sma_currencies"
            ,"code = 'KHR' order by id desc limit 1"
            ,"arr"
        );
        if ($temp[0]['id'] > 0) {
            $config_data_2 = array(
                'table_name' => 'sma_currencies',
                'id_name' => 'id',
                'field_add_ons_name' => 'add_ons',
                'selected_id' => $temp[0]['id'],
                'add_ons_title' => 'yesterday_kh_rate',
                'add_ons_value' => $yesterday_kh_rate,                    
            );
            $this->site->api_update_add_ons_field($config_data_2);      
        }
    }
}

public function display_stock_quantity($config_data) {
    if ($config_data['quantity'] <= 0)
        $return['display'] = '
            <span class="label label-danger '.$config_data['class'].'">
                '.$this->sma->formatquantity($config_data['quantity'],$config_data['decimal']).'
            </span>
        ';
    else
        $return['display'] = '
            <span class="label label-success '.$config_data['class'].'">
                '.$this->sma->formatquantity($config_data['quantity'],$config_data['decimal']).'
            </span>
        ';    
    return $return;
}

public function api_display_category_arrow($config_data) {
    $return['display'] = '';
    $config_data_2 = array(
        'id' => $config_data['id'],
        'table_name' => $config_data['table_name'],
        'field_name' => $config_data['field_name'],
        'translate' => $config_data['translate'],
    );
    $_SESSION['api_temp'] = array();
    $this->api_get_category_arrow($config_data_2);  
    for ($i2 = count($_SESSION['api_temp']) - 1; ; $i2--) {
        if ($i2 < 0) {
            break;
        }
        $temp_arrow = '';
        if ($i2 > 0)
            $temp_arrow = ' &rarr; ';
        $return['display'] .= $_SESSION['api_temp'][$i2].$temp_arrow;
    }           
    return $return;
}
public function api_remove_directory($config_data) {
    $filePath = $config_data['file_path'];
    $dir = opendir($filePath);
    while ($file = readdir($dir)) {
        if ($file != '' && is_file($filePath.'/'.$file)) {
            unlink($filePath.'/'.$file);    
        }
    }
}

public function api_zip_directory($config_data) {
    $rootPath = realpath($config_data['file_path']);
    $zip = new ZipArchive();
    $zip->open($config_data['file_path'].'/'.$config_data['zip_name'], ZipArchive::CREATE | ZipArchive::OVERWRITE);
    $files = new RecursiveIteratorIterator(
        new RecursiveDirectoryIterator($rootPath),
        RecursiveIteratorIterator::LEAVES_ONLY
    );
    foreach ($files as $name => $file)
    {
        if (!$file->isDir())
        {
            $filePath = $file->getRealPath();
            $relativePath = substr($filePath, strlen($rootPath) + 1);
            $zip->addFile($filePath, $relativePath);
        }
    }
    $zip->close();
}

public function api_force_download($config_data) {
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.basename($config_data['file_path']).'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($config_data['file_path']));
    flush();
    readfile($config_data['file_path']);
}

public function api_get_condition_category($config_data) {
    $_SESSION['api_temp'] = array();
    $_SESSION['api_temp_2'] = array();
    array_push($_SESSION['api_temp'], $config_data['id']);
    $this->api_get_condition_category_sub($config_data);
    
    if (count($_SESSION['api_temp']) > 0) {
        $condition .= " and (".$config_data['condition_id_name']." = ".$_SESSION['api_temp'][0];
        for ($i=0;$i<count($_SESSION['api_temp']);$i++) {
            $condition .= " or ".$config_data['condition_id_name']." = ".$_SESSION['api_temp'][$i];
        }        
        $condition .= ")";
    }
    else 
        $condition .= " and ".$config_data['condition_id_name']." = ".$config_data['id'];
    $_SESSION['api_temp_2'] = $_SESSION['api_temp'];
    $return['sql'] = $condition;
    return $return;
}
public function api_get_condition_category_sub($config_data) {
    $temp = $this->api_select_some_fields_with_where("
        id
        "
        ,$config_data['table_name']
        ,"parent_id = ".$config_data['id']
        ,"arr"
    );
    for ($i=0;$i<count($temp);$i++) {
        array_push($_SESSION['api_temp'], $temp[$i]['id']);
        $config_data_2 = $config_data;
        $config_data_2['id'] = $temp[$i]['id'];
        $this->api_get_condition_category_sub($config_data_2);
    }
}
public function api_unset_array_json($config_data) {
    $check_key_1 = $config_data['check_key_1'];
    for ($i2=0;$i2<count($config_data['json_array']);$i2++) {    
        if (count($config_data['check_array']) > 0) {
            $b = 0;
            for ($i3=0;$i3<count($config_data['check_array']);$i3++) {
                // if ($check_key_1 != 'id')
                // echo $config_data['json_array'][$i2][$check_key_1].' = '.$config_data['check_array'][$i3].'<br>';
                if ($config_data['json_array'][$i2][$check_key_1] == $config_data['check_array'][$i3]) {
                    $b = 1;
                    
                    break;
                }
                
                if ($config_data['check_dynamic_key_1'] != '') {
                    $check_dynamic_key_1 = $config_data['check_dynamic_key_1'].$config_data['json_array'][$i2]['id'];
                    for ($i4=0;$i4<count($config_data['json_array'][$i2][$check_dynamic_key_1]);$i4++) {
                        if ($config_data['json_array'][$i2][$check_dynamic_key_1][$i4][$check_key_1] == $config_data['check_array'][$i3]) {
                            $b = 1;
                            break;
                        }
                    }
                }
               

            }

            if ($b == 0)
                unset($config_data['json_array'][$i2]);
        }
    }
    $k = 0;
    for ($i2=0;$i2<count($config_data['json_array']);$i2++) {
        if ($config_data['json_array'][$i2]['id'] > 0) {
            $return['json_array'][$k] = $config_data['json_array'][$i2];
            $k++;
        }   
    }    
    return $return;
}




public function api_get_option_category($config_data){
    $return = array();
    $condition = '';
    if ($config_data['strip_id'] != '')
        $condition .= " and id != ".$config_data['strip_id'];

    if ($config_data['parent_id'] == '')
        $config_data['parent_id'] = 0;

    $config_data_2 = array(
        'table_name' => $config_data['table_name'],
        'select_table' => $config_data['table_name'],
        'translate' => $config_data['translate'],
        'select_condition' => "parent_id = ".$config_data['parent_id']." ".$condition." ".$config_data['condition'],
    );
    $select_data = $this->api_select_data_v2($config_data_2);

    $_SESSION['api_temp'] = array();
    if ($config_data['none_label'] != '') {
        $temp = ':{api}:'.$config_data['none_label'];
        array_push($_SESSION['api_temp'], $temp);
    }

    
    $config_data_2 = $config_data;
    for ($i=0;$i<count($select_data);$i++) {
        $temp = $select_data[$i]['id'].':{api}:'.$select_data[$i][$config_data['field_name']];
        array_push($_SESSION['api_temp'], $temp);
        $config_data_2['level'] = 0;
        $config_data_2['parent_id'] = $select_data[$i]['id'];
        if ($config_data['only_parent'] != 1)
            $this->api_get_option_category_sub($config_data_2);
    }
}

public function api_get_option_category_sub($config_data){
    $condition = '';
    if ($config_data['strip_id'] != '')
        $condition .= " and id != ".$config_data['strip_id'];

    $config_data_2 = array(
        'table_name' => $config_data['table_name'],
        'select_table' => $config_data['table_name'],
        'translate' => $config_data['translate'],
        'select_condition' => "parent_id = ".$config_data['parent_id']." ".$condition." ".$config_data['condition'],
    );
    $select_data = $this->api_select_data_v2($config_data_2);

    $config_data_2 = $config_data;
    for ($i=0;$i<count($select_data);$i++) {
        $temp = '';
        for ($i2=1;$i2<=$config_data['level'];$i2++) {
            if ($config_data['no_space'] != 1)
                $temp .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        }
        $temp = $temp.$config_data['space'];

        $temp = $select_data[$i]['id'].':{api}:'.$temp.$select_data[$i][$config_data['field_name']];
        array_push($_SESSION['api_temp'], $temp);

        $config_data_2['parent_id'] = $select_data[$i]['id'];
        
        $config_data_2['level'] = $config_data['level'] + 1;
        $this->api_get_option_category_sub($config_data_2);
    }
}

public function api_get_category_arrow($config_data){
    if ($config_data['translate'] != 'yes') {
        $select_data = $this->api_select_some_fields_with_where("
            *
            "
            ,$config_data['table_name']
            ,"id = ".$config_data['id']
            ,"arr"
        );            
    }
    else {
        $temp = str_replace('title_','',$config_data['field_name']);
        $select_data = $this->api_select_some_fields_with_where("
            *, getTranslate(translate,'".$temp."','".f_separate."','".v_separate."') as ".$config_data['field_name']."
            "
            ,$config_data['table_name']
            ,"id = ".$config_data['id']
            ,"arr"
        );
    }

    if ($select_data[0]['parent_id'] == 0) {
        if ($config_data['show_parent_name'] == 1)
            array_push($_SESSION['api_temp'],$select_data[0][$config_data['field_name']]);
    }
    else {
        array_push($_SESSION['api_temp'], $select_data[0][$config_data['field_name']]);
        $config_data_2 = $config_data;
        $config_data_2['id'] = $select_data[0]['parent_id'];
        $this->api_get_category_arrow($config_data_2);
    }
}

public function api_zip($config_data) {
    // $config_data = array(
    //     'zip_folder_path' => 'DB',
    //     'zip_file_path' => 'db.zip',
    // );
    // $this->api_helper->api_zip($config_data);

    //ini_set('max_execution_time', 600);
    $rootPath = realpath($config_data['zip_folder_path']);

    $zip = new ZipArchive();
    $zip->open($config_data['zip_file_path'], ZipArchive::CREATE | ZipArchive::OVERWRITE);

    // Create recursive directory iterator
    /** @var SplFileInfo[] $files */
    $files = new RecursiveIteratorIterator(
        new RecursiveDirectoryIterator($rootPath),
        RecursiveIteratorIterator::LEAVES_ONLY
    );

    foreach ($files as $name => $file)
    {
        if (!$file->isDir()) {
            $filePath = $file->getRealPath();
            $relativePath = substr($filePath, strlen($rootPath) + 1);
            $zip->addFile($filePath, $relativePath);
        }
    }
    $zip->close();
}


public function api_export_db($config_data) {
    // $temp_table = array('sma_add_ons');
    // $config_data = array(
    //     'host' => 'localhost',
    //     'username' => 'root',
    //     'password' => '',
    //     'database_name' => 'stock_manager_db',
    //     'export_type' => 'all', //all, specific_table, except_table
    //     'export_table' => $temp_table,
    //     'export_file_path' => 'temp.sql',
    // );
    // $this->api_helper->api_export_db($config_data);

    //ini_set('max_execution_time', 6000);
    $host = $config_data['host'];
    $username = $config_data['username'];
    $password = $config_data['password'];
    $database_name = $config_data['database_name'];

    $conn = mysqli_connect($host, $username, $password, $database_name);
    $conn->set_charset("utf8");

    $tables = array();
    $sql = "SHOW TABLES";
    $result = mysqli_query($conn, $sql);
    while ($row = mysqli_fetch_row($result)) {
        $tables[] = $row[0];
    }

    //$tables = $this->db->list_tables();

    $sqlScript = "
        SET FOREIGN_KEY_CHECKS=0;
    ";
    foreach ($tables as $table) {
        $b = 0;
        if ($config_data['export_type'] == 'all' || $config_data['export_type'] == '')
            $b = 1;
        if ($config_data['export_type'] == 'specific_table') {
            for ($i=0;$i<count($config_data['export_table']);$i++) {                
                if ($table == $config_data['export_table'][$i]) {
                    $b = 1;
                    break;
                }
            }            
        }
        if ($config_data['export_type'] == 'except_table') {
            for ($i=0;$i<count($config_data['export_table']);$i++) {
                if ($table == $config_data['export_table'][$i]) {
                    $b = 1;
                    break;
                }
            }            
        }
       
        if ($b == 1) {
            $query = "SHOW CREATE TABLE $table";
            $result = mysqli_query($conn, $query);
            $row = mysqli_fetch_row($result);
            
            $sqlScript .= "\n\n" . $row[1] . ";\n\n";

            $query = "SELECT * FROM $table";
            $result = mysqli_query($conn, $query);            
            $columnCount = mysqli_num_fields($result);
            
            for ($i = 0; $i < $columnCount; $i ++) {
                while ($row = mysqli_fetch_row($result)) {
                    $sqlScript .= "INSERT INTO $table VALUES(";
                    for ($j = 0; $j < $columnCount; $j ++) {
                        $row[$j] = $row[$j];           
                        if (isset($row[$j])) {
                            $row[$j] = str_replace("'","",$row[$j]);
                            $sqlScript .= "'". $row[$j] . "'";
                        } else {
                            $sqlScript .= "''";
                        }
                        if ($j < ($columnCount - 1)) {
                            $sqlScript .= ',';
                        }
                    }
                    $sqlScript .= ");\n";
                }
            }
            $sqlScript .= "\n"; 
        }
    }
    file_put_contents($config_data['export_file_path'], $sqlScript); 
}

public function api_add_ons_table_insert($config_data){
    // $config_data_2 = array(
    //     'add_ons_table_name' => 'sma_add_ons',
    //     'table_name' => 'sma_categories',
    //     'post' => $_POST,
    // );
    // $temp = $this->api_helper->api_add_ons_table_insert($config_data_2);     
    $return = array();

    $config_data_2 = array(
        'add_ons_table_name' => $config_data['add_ons_table_name'],
        'table_name' => $config_data['table_name'],
    );
    $temp = $this->api_get_condition_add_ons_field_v2($config_data_2);  

    $return['add_ons'] = 'initial_first_add_ons:{}:';
    foreach (array_keys($temp['key']) as $key) {
        $b = 0;
        foreach($config_data['post'] as $name => $value) {
            if (is_int(strpos($name,"add_ons_"))) {
                $temp_name = str_replace('add_ons_','',$name);
                if ($temp_name == $key) {
                    $return['add_ons'] .= $key.':{'.$value.'}:';
                    $b = 1;
                    break;
                }
            }
        }
        if ($b == 0)
            $return['add_ons'] .= $key.':{}:';
    }

    return $return;
}

function calculate_delivery_fee($distance){
// 0 - 1km : 1$
// 1-1.5km : 1.5$
// 1.5km-2.0km - 2$
// increase o.5$ every 0.5km  
  $fee = 0;
  if (is_int(strpos($distance,"km"))) {
    $distance = floatval($distance);
    if ($distance <= 1)
      $fee = 1;
    if ($distance > 1 && $distance <= 1.5)
      $fee = 1.5;
    if ($distance > 1.5 && $distance <= 2)
      $fee = 2;
    if ($distance > 2) {
      $fee = 2;
      $temp = $distance - 2;
      $temp = $temp / 0.5;
      $temp_2 = intval($temp);
      if ($temp_2 >= 1)
        $fee = $fee + ($temp_2 * 0.5);
    }
  }
  else
    $fee = 1;

  return $fee;
}

function round_up_second_decimal($value){
    //1-4 = 5
    //5-9 = 10
    $return['price'] = $value;
    $temp = $return['price'];        
    $temp_decimal = $temp - floor($return['price']);
    if ($temp_decimal > 0) {
        $temp2 = explode('.',$temp_decimal);
        $temp_decimal_array = str_split($temp2[1]);
        if ($temp_decimal_array[1] >= 6) {
            $return['price'] = round($return['price'],1);
        }
        else {
            if ($temp_decimal_array[1] > 0) {
                $temp_decimal_array[1] = 5;                
                $return['price'] = floor($return['price']) + floatval('0.'.$temp_decimal_array[0].$temp_decimal_array[1]);
            }
        }
    }

    return $return['price'];
}


function generate_ordering($config_data_function) {
    // $config_data_function = array(
    //     'table_name' => 'sma_products',
    //     'id' => $select_data[0]['id'],
    //     'value' => $this->input->post('ordering'),
    // );
    if ($config_data_function['table_name'] != 'sma_products')
        $condition = '';
    else
        $condition = ' and category_id = '.$config_data_function['category_id'];

    $config_data = array(
        'table_name' => $config_data_function['table_name'],
        'select_table' => $config_data_function['table_name'],
        'select_condition' => "id != ".$config_data_function['id']." ".$condition." order by ordering asc",
    );
    $temp = $this->api_select_data_v2($config_data);
    if ($config_data_function['value'] == 0) {
        for($i=0;$i<count($temp);$i++) {
            $ordering['ordering'] = $temp[$i]['ordering'] + 1;
            $this->db->update($config_data_function['table_name'], $ordering['ordering'] , "id = ".$temp[$i]['id']);
        } 
    } 
    else {
        $config_data = array(
            'table_name' => $config_data_function['table_name'],
            'select_table' => $config_data_function['table_name'],
            'select_condition' => "id > 0 ".$condition." order by ordering desc limit 1",
        );
        $temp3 = $this->api_select_data_v2($config_data);
        if ($config_data_function['value'] <= $temp3[0]['ordering']) {
            for($k=0;$k<count($temp);$k++) {
                if ($temp[$k]['ordering'] >= $config_data_function['value'])
                    $ordering['ordering'] = $temp[$k]['ordering'] + 1;
                else if ($temp[$k]['ordering'] < $config_data_function['value'])
                    $ordering['ordering'] = $temp[$k]['ordering'] - 1;
                
                $this->db->update($config_data_function['table_name'], $ordering , "id = ".$temp[$k]['id']);

            } 
        }
    }

    $temp = array(
        'ordering' => $config_data_function['value'],
    );
    $this->db->update($config_data_function['table_name'], $temp, "id = ".$config_data_function['id']);    

    $config_data = array(
        'table_name' => $config_data_function['table_name'],
        'select_table' => $config_data_function['table_name'],
        'select_condition' => "id > 0 ".$condition." order by ordering asc",
    );
    $temp2 = $this->api_select_data_v2($config_data);
    for($j=0;$j<count($temp2);$j++) {
        $ordering['ordering'] = $j + 1;
        $this->db->update($config_data_function['table_name'], $ordering, "id = ".$temp2[$j]['id']);
    }    
    
}

public function api_get_image($data_view) {
    if (!isset($data_view['image_icon_type'])) $data_view['image_icon_type'] = '';

    if ($data_view['resize_type'] == '' || $data_view['resize_type'] == 'full')
        $temp = $this->api_img_resize_full($data_view);
    if ($data_view['resize_type'] == 'size')
        $temp = $this->api_img_resize_size($data_view);
    // if ($data_view['resize_type'] == 'fixed')
    //     $temp = $this->api_img_resize_fixed($data_view);
    $img = $temp['image'];
    
    if (!isset($temp['no_image'])) $temp['no_image'] = '';
    
    $return['image'] = $temp['image'];
    $return['no_image'] = $temp['no_image'];    
    $return['image_src'] = $temp['image_src'];
    $return['image_full_src'] = $temp['image_full_src'];
    $return['image'] = $img;


    $return['image_table'] = '
        <div class="'.$data_view['wrapper_class'].'">
            '.$temp_resize_header.'
            <table class="'.$data_view['table_class'].'" width="100%" cellpadding="0" cellspacing="0" border="0">
            <tr>
            <td valign="middle" align="center" height="'.$data_view['table_height'].'">
            '.$img.' 
            </td>
            </tr>
            </table>
            '.$temp_resize_footer.'
        </div>
    ';
    $return['image_table_link'] = '
        <div class="'.$data_view['wrapper_class'].'">
            '.$temp_resize_header.'
            <table class="'.$data_view['table_class'].'" width="100%" cellpadding="0" cellspacing="0" border="0">
            <tr>
            <td valign="middle" align="center" height="'.$data_view['table_height'].'">
                <a href="'.$return['image_full_src'].'" target="_blank">
                    '.$temp['image'].'
                </a>  
            </td>
            </tr>
            </table>
            '.$temp_resize_footer.'
        </div>
    ';    
    $return['image_link'] = '
        <div class="'.$data_view['wrapper_class'].'">
            '.$temp_resize_header.'
            <a href="'.$return['image_full_src'].'" target="_blank">
                '.$temp['image'].'
            </a>  
            '.$temp_resize_footer.'   
        </div>
    ';

    if ($data_view['resize_type'] == 'size') {
        $img = '
            <div class="'.$data_view['wrapper_class'].'">
                '.$img.'
            </div>
        ';
        $return['image'] = $img;
        $return['image_table'] = $img;
    }

    if (!isset($page_link)) $page_link = '';
    
    $return['page_link'] = $page_link;
    return $return;
}

public function api_img_resize_full($config_data){
    if (!isset($config_data['image_width'])) $config_data['image_width'] = '';
    if (!isset($config_data['image_height'])) $config_data['image_height'] = '';
    if (!isset($config_data['image_title'])) $config_data['image_title'] = '';

    $file_path = $config_data['file_path'];
    $max_width = $config_data['max_width'];
    $max_height = $config_data['max_height'];
    $img_id = $config_data['image_id'];
    $img_class = $config_data['image_class'];
    $img_width = $config_data['image_width'];
    $img_height = $config_data['image_height'];
    $img_src = '';
    $temp_array['image_full_src'] = base_url().$config_data['file_path'];
    if (!is_file($file_path)) {  
        $temp_array['image_full_src'] = base_url().'assets/uploads/no_image.jpg';
        $file_path = 'assets/uploads/no_image.jpg';
    }

    if ($img_width <= 0 || $img_height <= 0) {
        if (is_file($file_path)) {            
            $data = GetImageSize($file_path);
            $img_width = $data[0];
            $img_height = $data[1];   
            if ($img_width <= 0)
                $img_width = $max_width;
        }         
    }


    if (is_file($file_path)) {     
        if ($img_width > $max_width && $img_height <= $max_height) {
            $temp_height = ($img_height * $max_width) / $img_width;
            $temp_height = round($temp_height);
            if ($temp_height > $max_height) $temp_height = $max_height;
            $img_1 = '<img id="'.$img_id.'" class="'.$img_class.'" src="'.$temp_array['image_full_src'].'" width="'.$max_width.'" height="'.$temp_height.'" title="'.$config_data['image_title'].'"/>';
        }
        elseif ($img_width > $max_width && $img_height > $max_height) {
            if ($img_width > $img_height) {                     
                $temp_height = ($img_height * $max_width) / $img_width;
                $temp_height = round($temp_height);                                       
                if ($temp_height > $max_height) $temp_height = $max_height;                                     
                if ($temp_height < $max_height) {
                    if ($temp_height > $img_height) $temp_height = $img_height;
                    $img_1 = '<img id="'.$img_id.'" class="'.$img_class.'" src="'.$temp_array['image_full_src'].'" width="'.$max_width.'" height="'.$temp_height.'" title="'.$config_data['image_title'].'"/>';
                }
                else {
                    $temp_width = ($img_width * $max_height) / $img_height;
                    $temp_width = round($temp_width);
                    $img_1 = '<img id="'.$img_id.'" class="'.$img_class.'" src="'.$temp_array['image_full_src'].'" width="'.$temp_width.'" height="'.$temp_height.'" title="'.$config_data['image_title'].'"/>';                    
                }                           
            }
            else {
                $temp_width = ($img_width * $max_height) / $img_height;
                $temp_width = round($temp_width);                         
                if ($temp_width > $max_width) $temp_width = $max_width;                                     
                if ($temp_width < $max_width) {
                    $img_1 = '<img id="'.$img_id.'" class="'.$img_class.'" src="'.$temp_array['image_full_src'].'" height="'.$max_height.'" width="'.$temp_width.'" title="'.$config_data['image_title'].'"/>';
                }
                else {
                    $temp_height = ($img_height * $max_width) / $img_width;
                    $temp_height = round($temp_height);                         
                    if ($temp_height > $max_height) $temp_height = $max_height;                                     
                    $img_1 = '<img id="'.$img_id.'" class="'.$img_class.'" src="'.$temp_array['image_full_src'].'" height="'.$temp_height.'" width="'.$temp_width.'" title="'.$config_data['image_title'].'"/>';                       
                }
            }
        }
        elseif ($img_width <= $max_width && $img_height <= $max_height) {   
            $img_1 = '<img id="'.$img_id.'" class="'.$img_class.'" src="'.$temp_array['image_full_src'].'" width="'.$img_width.'" height="'.$img_height.'" title="'.$config_data['image_title'].'"/>';
        }
        elseif ($img_width <= $max_width && $img_height > $max_height) {
            $temp_width = ($img_width * $max_height) / $img_height;
            $temp_width = round($temp_width);                   
            if ($temp_width > $max_width) $temp_width = $max_width;                                     
            $img_1 = '<img id="'.$img_id.'" class="'.$img_class.'" src="'.$temp_array['image_full_src'].'" height="'.$max_height.'px" width="'.$temp_width.'" title="'.$config_data['image_title'].'"/>';
        }
    }
    else 
        $temp_array['no_image'] = 1;    


    $temp_array['image'] = $img_1;
    
    $temp_array['image_table'] = '
        <table class="'.$config_data['table_class'].'" width="100%" cellpadding="0" cellspacing="0" border="1">
        <tr>
        <td valign="middle" align="center" height="'.$max_height.'">
           '.$img_1.' 
        </td>
        </tr>
        </table>
    ';
    
    return $temp_array;
}

public function api_img_resize_size($config_data){
    if (!isset($config_data['image_width'])) $config_data['image_width'] = '';
    if (!isset($config_data['image_height'])) $config_data['image_height'] = '';
    if (!isset($config_data['image_title'])) $config_data['image_title'] = '';

    $file_path = $config_data['file_path'];
    $max_width = $config_data['max_width'];
    $max_height = $config_data['max_height'];
    $img_id = $config_data['image_id'];
    $img_class = $config_data['image_class'];
    
    $img_src = '';
    $temp_array['image_full_src'] = base_url().$config_data['file_path'];

    if (!is_file($file_path)) {  
        $temp_array['image_full_src'] = base_url().'assets/uploads/no_image.jpg';
        $file_path = 'assets/uploads/no_image.jpg';
    }

    if ($img_width <= 0 || $img_height <= 0) {
        if (is_file($file_path)) {            
            $data = GetImageSize($file_path);
            $img_width = $data[0];
            $img_height = $data[1];   
            if ($img_width <= 0)
                $img_width = $max_width;
        }         
    }


    if (is_file($file_path)) {     
        if ($img_width > $max_width && $img_height <= $max_height) {
            $temp_height = ($img_height * $max_width) / $img_width;
            $temp_height = round($temp_height);
            if ($temp_height > $max_height) $temp_height = $max_height;
            $img_1 = '<img id="'.$img_id.'" class="'.$img_class.'" src="'.$temp_array['image_full_src'].'" width="'.$max_width.'" height="'.$temp_height.'" title="'.$config_data['image_title'].'"/>';
        }
        elseif ($img_width > $max_width && $img_height > $max_height) {
            if ($img_width > $img_height) {                     
                $temp_height = ($img_height * $max_width) / $img_width;
                $temp_height = round($temp_height);                                       
                if ($temp_height > $max_height) $temp_height = $max_height;                                     



                
                if ($temp_height < $max_height) {
                    if ($temp_height > $img_height) $temp_height = $img_height;
                    $img_1 = '<img id="'.$img_id.'" class="'.$img_class.'" src="'.$temp_array['image_full_src'].'" width="'.$max_width.'" height="'.$temp_height.'" title="'.$config_data['image_title'].'"/>';
                }
                else {
                    $temp_width = ($img_width * $max_height) / $img_height;
                    $temp_width = round($temp_width);

                    $img_1 = '<img id="'.$img_id.'" class="'.$img_class.'" src="'.$temp_array['image_full_src'].'" width="'.$temp_width.'" height="'.$temp_height.'" title="'.$config_data['image_title'].'"/>';                    
                }                           
            }
            else {
                $temp_width = ($img_width * $max_height) / $img_height;
                $temp_width = round($temp_width);                         
                if ($temp_width > $max_width) $temp_width = $max_width;                                     
                if ($temp_width < $max_width) {
                    $img_1 = '<img id="'.$img_id.'" class="'.$img_class.'" src="'.$temp_array['image_full_src'].'" height="'.$max_height.'" width="'.$temp_width.'" title="'.$config_data['image_title'].'"/>';
                }
                else {
                    $temp_height = ($img_height * $max_width) / $img_width;
                    $temp_height = round($temp_height);                         
                    if ($temp_height > $max_height) $temp_height = $max_height;                                     
                    $img_1 = '<img id="'.$img_id.'" class="'.$img_class.'" src="'.$temp_array['image_full_src'].'" height="'.$temp_height.'" width="'.$temp_width.'" title="'.$config_data['image_title'].'"/>';                       
                }
            }
        }
        elseif ($img_width <= $max_width && $img_height <= $max_height) {   
            $img_1 = '<img id="'.$img_id.'" class="'.$img_class.'" src="'.$temp_array['image_full_src'].'" width="'.$img_width.'" height="'.$img_height.'" title="'.$config_data['image_title'].'"/>';
        }
        elseif ($img_width <= $max_width && $img_height > $max_height) {
            $temp_width = ($img_width * $max_height) / $img_height;
            $temp_width = round($temp_width);                   
            if ($temp_width > $max_width) $temp_width = $max_width;                                     
            $img_1 = '<img id="'.$img_id.'" class="'.$img_class.'" src="'.$temp_array['image_full_src'].'" height="'.$max_height.'px" width="'.$temp_width.'" title="'.$config_data['image_title'].'"/>';
        }
    }
    else 
        $temp_array['no_image'] = 1;    


    $temp_array['image'] = $img_1;

    $temp_class = '';
    if (is_int(strpos($img_class,"api_link_box_none")))
        $temp_class = 'api_link_box_none';

    if ($temp_width != '' && $temp_height != '') {
        $max_width_2 = $max_width + 25;
        
        $temp_height_2 = ($img_height * $max_width_2) / $img_width;
        $temp_height_2 = round($temp_height_2);
        
        $temp_array['image'] = '
            <div class="'.$temp_class.'" style="width:100%; height:'.$max_height.'px; background-image:url('.$temp_array['image_full_src'].'); background-size: '.$max_width_2.'px '.$temp_height_2.'px; background-repeat:no-repeat; background-position:center;">
            </div>
        ';
    }
    else {
        $temp_array['image'] = '
            <div class="'.$temp_class.'" style="width:100%; height:'.$max_height.'px; background-image:url('.$temp_array['image_full_src'].'); background-size:cover; background-repeat:no-repeat; background-position:center;">
            </div>
        ';
    }

    return $temp_array;
}

public function is_mobile() {
    $useragent = $_SERVER['HTTP_USER_AGENT'];
    if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))) {
        return true;
    }
    else
        return false;
}
public function print_array() {
    $args = func_get_args();
    echo "<pre>";
    foreach ($args as $arg) {
        print_r($arg);
    }
    echo "</pre>";
}


}
