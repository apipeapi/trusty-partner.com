<?php

class Api_initial_data {

public function __construct()
{

}
public function __get($var)
{
    return get_instance()->$var;
}

public function blog($config_data_function) {
    $return = array();    
    $select_data = $config_data_function['select_data'];
    $l = $config_data_function['l'];
    for ($i=0;$i<count($select_data);$i++) {
        $initial_data[$i]['id'] = $select_data[$i]['id'];
        $initial_data[$i]['title'] = $select_data[$i]['title_'.$l];
        $initial_data[$i]['image'] = $select_data[$i]['image'];
        $initial_data[$i]['description'] = $this->api_helper->decode_html($select_data[$i]['description_'.$l]);
    }
    $return = $initial_data;
    return $return;
} 

public function blog_step($config_data_function) {
    $return = array();    
    $select_data = $config_data_function['select_data'];
    $l = $config_data_function['l'];
    $initial_data = $this->blog($config_data_function);
    for ($i=0;$i<count($select_data);$i++) {
        $initial_data[$i]['title_2'] = $select_data[$i]['title_2_'.$l];
    }
    $return = $initial_data;
    return $return;
} 

public function large_icon($config_data_function) {
    $return = array();    
    $select_data = $config_data_function['select_data'];
    $l = $config_data_function['l'];
    for ($i=0;$i<count($select_data);$i++) {
        $initial_data[$i]['id'] = $select_data[$i]['id'];
        $initial_data[$i]['title'] = $select_data[$i]['title_'.$l];
        $initial_data[$i]['image'] = $select_data[$i]['image'];
        $initial_data[$i]['description'] = $this->api_helper->decode_html($select_data[$i]['description_'.$l]);
    }
    $return = $initial_data;
    return $return;
} 
public function large_icon_v2($config_data_function) {
    $return = array();    
    $initial_data = $this->large_icon($config_data_function);
    $l = $config_data_function['l'];
    for ($i=0;$i<count($select_data);$i++) {
        $initial_data[$i]['image'] = $select_data[$i]['image'];
    }
    $return = $initial_data;
    return $return;
}

public function large_icon_cart($config_data_function) {
    $return = array();    
    $select_data = $config_data_function['select_data'];
    $l = $this->api_shop_setting[0]['api_lang_key'];
    for ($i=0;$i<count($select_data);$i++) {
        $initial_data[$i]['id'] = $select_data[$i]['id'];
        $initial_data[$i]['title'] = $select_data[$i]['title_'.$l];
        $initial_data[$i]['image'] = $select_data[$i]['image'];
        $initial_data[$i]['slug'] = $select_data[$i]['slug'];                
    }
    $return = $initial_data;
    return $return;
}

public function table_order_list($config_data_function) {
    $return = array();    
    $select_data = $config_data_function['select_data'];
    $l = $this->api_shop_setting[0]['api_lang_key'];
    $initial_data = $select_data;
    for ($i=0;$i<count($select_data);$i++) {
        $date = date_create($select_data[$i]['date']);            
        $initial_data[$i]['date_display'] = date_format($date, 'd M Y');

        if ($select_data[$i]['payment_status'] == 'completed' || $select_data[$i]['payment_status'] == 'paid') {
            $temp2 = '';
            $config_data = array(
                'table_name' => 'sma_payments',
                'select_table' => 'sma_payments',
                'translate' => '',
                'select_condition' => "sale_id = ".$select_data[$i]['id']." and (paid_by = 'wing' or paid_by = 'pipay' or paid_by = 'payway') limit 1",
            );
            $temp = $this->site->api_select_data_v2($config_data);
            if ($temp[0]['paid_by'] == 'wing')
                $temp2 = '
                    <div class="api_padding_top_5">
                        '.lang('Paid_by_Wing').'<br>
                        '.lang('Transaction_ID').': '.$temp[0]['wing_transaction_id'].'
                    </div>
                ';
            elseif ($temp[0]['paid_by'] == 'pipay')
                $temp2 = '
                    <div class="api_padding_top_5">
                        '.lang('Paid_by_Pipay').'<br>
                        '.lang('Transaction_ID').': '.$temp[0]['pipay_transaction_id'].'
                    </div>
                ';
            elseif ($temp[0]['paid_by'] == 'payway')
                $temp2 = '
                    <div class="api_padding_top_5">
                        '.lang('Paid_by_Payway').'<br>
                        '.lang('Transaction_ID').': '.$temp[0]['payway_transaction_id'].'
                    </div>
                ';
            $initial_data[$i]['payment_display'] = '
                <div class="label label-success">'
                    .lang('Paid').'
                </div>
                '.$temp2.'
            ';
        } 
        else
            $initial_data[$i]['payment_display'] = '
                <div class="label label-warning" >
                    '.lang('Not_paid_yet').'
                </div>
            ';
        if ($select_data[$i]['sale_status'] == 'completed')
            $initial_data[$i]['status_display'] = '
                <div class="label label-success">
                    '.lang('Completed').'
                </div>
            ';
        else
            $initial_data[$i]['status_display'] = '
                <div class="label label-warning">
                    '.lang('Pending').'
                </div>
            ';
    }
    $return = $initial_data;
    return $return;
} 

public function restaurant_menu_list($config_data_function) {
    $return = array();    
    $select_data = $config_data_function['select_data'];
    
    $l = $this->api_shop_setting[0]['api_lang_key'];
    for ($i=0;$i<count($select_data);$i++) {
        $initial_data[$i]['id'] = $select_data[$i]['id'];
        $initial_data[$i]['title'] = $select_data[$i]['title_en'];
        $initial_data[$i]['image'] = $select_data[$i]['image'];        
        $initial_data[$i]['qty'] = $select_data[$i]['qty'];

        $initial_data[$i]['price'] = $select_data[$i]['price'];
        $initial_data[$i]['price_original'] = $select_data[$i]['price_original'];
        $initial_data[$i]['price_label'] = $this->sma->formatMoney($select_data[$i]['price']);
        
        $initial_data[$i]['is_promotion'] = $select_data[$i]['is_promotion'];
        $initial_data[$i]['discount_rate'] = $select_data[$i]['discount_rate'];
        //$initial_data[$i]['min_qty'] = $select_data[$i]['min_qty'];
        $initial_data[$i]['min_qty'] = 0;
        if ($initial_data[$i]['is_promotion'] == 1) {
            $initial_data[$i]['promotion_label'] = $this->sma->formatMoney($initial_data[$i]['price_original']);
            $initial_data[$i]['promotion_rate_label'] = $initial_data[$i]['discount_rate'].'%';      
            $initial_data[$i]['promotion_duration_left'] = $select_data[$i]['duration_left'];                    
            $initial_data[$i]['min_qty'] = $select_data[$i]['min_qty'];                    
        }        
    }
    $return = $initial_data;
    return $return;
}


}
