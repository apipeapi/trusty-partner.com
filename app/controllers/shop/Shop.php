<?php defined('BASEPATH') or exit('No direct script access allowed');

class Shop extends MY_Shop_Controller
{
    public function __construct()
    {
        parent::__construct();

        if ($this->Settings->mmode) {
            redirect('notify/offline');
        }
        $this->load->library('form_validation');

        $config_data = array(
            'table_name' => 'sma_shop_settings',
            'select_table' => 'sma_shop_settings',
            'translate' => '',
            'select_condition' => "shop_id = 1",
        );
        $this->api_shop_setting = $this->site->api_select_data_v2($config_data);
        if (!$this->loggedIn && $this->api_shop_setting[0]['require_login'] == 1) {
            redirect('/login');
        }
        $this->load->library('components/api_menu_mobile', 'api_menu_mobile');
    }

    // Display Page
    public function page($slug)
    {
        $page = $this->shop_model->getPageBySlug($slug);
        $this->data['page'] = $page;
        $this->data['page_title'] = $page->title;
        $this->data['page_desc'] = $page->description;
        $this->page_construct('pages/page', $this->data);
    }

    // Display Page
    public function product($slug)
    {
        $config_data = array(
            'table_name' => 'sma_products',
            'select_table' => 'sma_products',
            'translate' => '',
            'select_condition' => "slug = '".$slug."'",
        );
        $select_data = $this->site->api_select_data_v2($config_data);

        if (count($select_data) <= 0) {
            $this->session->set_flashdata('error', lang('product_not_found'));
            $this->sma->md('/');
        }
        for ($i=0;$i<count($select_data);$i++) {
            foreach(array_keys($select_data[$i]) as $key){
                $product->{$key} = $select_data[$i][$key];
            }
        }
        
        // $this->data['barcode'] = "<img src='" . admin_url('products/gen_barcode/' . $product->code . '/' . $product->barcode_symbology . '/40/0') . "' alt='" . $product->code . "' class='pull-left' />";
        // if ($product->type == 'combo') {
        //     $this->data['combo_items'] = $this->shop_model->getProductComboItems($product->id);
        // }

      
        if ($this->api_shop_setting[0]['require_login'] == 1) {
            $temp = $this->site->api_select_some_fields_with_where(
                "DISTINCT t1.product_id",
                "sma_sale_items as t1 inner join sma_sales as t2 on t2.id = t1.sale_id",
                "t2.created_by = ".$this->sma->session->userdata('user_id'),
                "arr"
            );
            for ($i=0;$i<count($temp);$i++) {
                $product->is_ordered = 0;
                if ($product->id == $temp[$i]['product_id']) {
                    $product->is_ordered = 1;
                    break;
                }
            }

            $temp = $this->site->api_select_some_fields_with_where(
                "t1.product_id",
                "sma_wishlist as t1 inner join sma_products as t2 on t2.id = t1.product_id",
                "t1.user_id = ".$this->session->userdata('user_id'),
                "arr"
            );
            for ($i=0;$i<count($temp);$i++) {
                if ($product->id == $temp[$i]['product_id']) {
                    $product->is_favorite = 1;
                    break;
                }
            }
        }

        $config_data = array(
            'id' => $product->id,
        );
        $temp_hide = $this->shop_model->api_get_product_display($config_data);

        if ($temp_hide['result'] == 'hide') {
            $this->session->set_flashdata('error', lang('product_not_found'));
            $this->sma->md('/');
        }

        $this->shop_model->updateProductViews($product->id, $product->views);
        $this->data['product'] = $product;
        $this->data['other_products'] = $this->shop_model->getOtherProducts($product->id, $product->category_id, $product->brand);

        $this->data['unit'] = $this->site->getUnitByID($product->unit);
        $this->data['brand'] = $this->site->getBrandByID($product->brand);
        $this->data['images'] = $this->shop_model->getProductPhotos($product->id);
        $this->data['category'] = $this->site->getCategoryByID($product->category_id);
        $this->data['subcategory'] = $product->subcategory_id ? $this->site->getCategoryByID($product->subcategory_id) : null;
        $this->data['tax_rate'] = $product->tax_rate ? $this->site->getTaxRateByID($product->tax_rate) : null;

        // $this->data['warehouse'] = $this->shop_model->getAllWarehouseWithPQ($product->id);
        $this->data['warehouse'] = $this->api_shop_setting[0]['warhouse'];
        // $this->data['options'] = $this->shop_model->getProductOptionsWithWH($product->id);
        // $this->data['variants'] = $this->shop_model->getProductOptions($product->id);
        
        $this->load->helper('text');
        $this->data['page_title'] = $product->code.' - '.$product->name;
        $this->data['page_desc'] = character_limiter(strip_tags($product->product_details), 160);
        $this->page_construct('pages/view_product', $this->data);
    }
    // Products,  categories and brands page
    public function products($category_slug = null, $subcategory_slug = null, $brand_slug = null) {
        $search = $this->input->get('search');
        $search = $this->db->escape_str($search);
        $search = $this->db->escape_str($search);
        $sort_by = $this->input->get('sort_by');
        $sort_order = $this->input->get('sort_order');
        $page = $this->input->get('page') ? $this->input->get('page', true) : 1;
        $per_page = 1000;
        $offset_no = ($page*$per_page)-$per_page;

        $this->load->helper('pagination');
        $this->load->helper('text');        
        
        $config_data = array(
            'table_name' => 'sma_products',
            'table_id' => 'id',
            'search' => $search,
            'sort_by' => $sort_by,
            'sort_order' => $sort_order,
            'limit' => $per_page,
            'offset_no' => $offset_no,
            'category_slug' => $category_slug,
            'subcategory_slug' => $subcategory_slug,
            'brand_slug' => $brand_slug,
            'user_id' => $this->session->userdata('user_id'),
            'lg' => get_cookie('shop_language', TRUE),

        );
        $temp = $this->shop_model->api_get_product($config_data);
            


        // $config_data = array(
        //     'table_name' => 'sma_products',
        //     'table_id' => 'id',
        //     'search' => $search,
        //     'sort_by' => $sort_by,
        //     'sort_order' => $sort_order,
        //     'limit' => '',
        //     'offset_no' => '',
        //     'category_slug' => $category_slug,
        //     'subcategory_slug' => $subcategory_slug,
        //     'brand_slug' => $brand_slug,
        //     'user_id' => $this->session->userdata('user_id'),
        //     'lg' => get_cookie('shop_language', TRUE),
        // );
        // $temp = $this->shop_model->api_get_product($config_data);

        $select_data = $temp['select_data'];

        if (is_array($temp['select_data_total'])) {
            $total_rows = count($temp['select_data_total']);
        } else {
            $total_rows = 0;
        }
        
        $this->session->set_userdata('requested_page', $this->uri->uri_string());
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        
        if ($category_slug != '') {
            $temp_url = '/'.$category_slug;
        }
        if ($subcategory_slug != '') {
            $temp_url .= '/'.$subcategory_slug;
        }
        if ($page != '') $temp_url .= '?page=';
        if ($search != '') $temp_url .= '&search='.$search;
        if ($sort_by != '') $temp_url .= '&sort_by='.$sort_by;
        if ($sort_order != '') $temp_url .= '&sort_order='.$sort_order;
        if ($per_page != '') $temp_url .= '&per_page='.$per_page;
        
        $config_base_url = site_url() . 'shop/products'.$temp_url; //your url where the content is displayed
        $config['base_url'] = $config_base_url; //your url where the content is displayed
        $config['query_string_segment'] = 'page';
        $config['per_page'] = $per_page; //$per_page;
        $config['num_links'] = 5;
        $config['use_page_numbers'] = true;
        $config['page_query_string'] = true;
        $config['full_tag_open'] = '<ul class="pagination pagination-new-bg">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['prev_link'] = '&lt; Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next &gt;';
        $config['next_tag_open'] = '<li class="next">';
        $config['next_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['total_rows'] = $total_rows; //$this->data['total_rows_sale'];
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $this->data['pagination'] = pagination('shop/products'.$temp_url, $total_rows, $per_page);

        $this->data['select_data'] = $select_data;

        $this->data['page_info'] = array('page' => $page, 'total' => ceil($total_rows/$per_page));
        $this->data['page_title'] = lang('Products');
        $this->data['page_desc'] = '';
        $this->page_construct('pages/products', $this->data);
    }

    public function products_bk($category_slug = null, $subcategory_slug = null, $brand_slug = null, $promo = null)
    {
        $this->session->set_userdata('requested_page', $this->uri->uri_string());
        if ($this->input->get('category')) {
            $category_slug = $this->input->get('category', true);
        }
        if ($this->input->get('brand')) {
            $brand_slug = $this->input->get('brand', true);
        }
        if ($this->input->get('promo') && $this->input->get('promo') == 'yes') {
            $promo = true;
        }
        $reset = $category_slug || $subcategory_slug || $brand_slug ? true : false;
        $filters = array(
            'query' => $this->input->post('query'),
            'category' => $category_slug ? $this->shop_model->getCategoryBySlug($category_slug) : null,
            'subcategory' => $subcategory_slug ? $this->shop_model->getCategoryBySlug($subcategory_slug) : null,
            'brand' => $brand_slug ? $this->shop_model->getBrandBySlug($brand_slug) : null,
            'promo' => $promo,
            'sorting' => $reset ? null : $this->input->get('sorting'),
            'min_price' => $reset ? null : $this->input->get('min_price'),
            'max_price' => $reset ? null : $this->input->get('max_price'),
            'in_stock' => $reset ? null : $this->input->get('in_stock'),
            'page' => $this->input->get('page') ? $this->input->get('page', true) : 1,
        );
        $this->data['filters'] = $filters;
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['page_title'] = (!empty($filters['category']) ? $filters['category']->name : (!empty($filters['brand']) ? $filters['brand']->name : lang('products'))). ' - ' .$this->shop_settings->shop_name;
        $this->data['page_desc'] = !empty($filters['category']) ? $filters['category']->description : (!empty($filters['brand']) ? $filters['brand']->description : $this->shop_settings->products_description);
        $this->page_construct('pages/products', $this->data);
    }

    // Search products page - ajax
    public function search()
    {
        $filters = $this->input->post('filters') ? $this->input->post('filters', true) : false;
        $limit = 50;

        $total_rows = $this->shop_model->getProductsCount($filters);
        if ($_GET['promo'] == 'true') {
            $filters['promo'] = 1;
        }
        $filters['limit'] = $limit;
        $filters['offset'] = isset($filters['page']) && !empty($filters['page']) && ($filters['page'] > 1) ? (($filters['page']*$limit)-$limit) : null;

        if ($products = $this->shop_model->getProducts($filters)) {
            $this->load->helper(array('text', 'pagination'));
            foreach ($products as &$value) {
                $value['details'] = character_limiter(strip_tags($value['details']), 140);
                $value['image'] = product_image($value['image'], false);
                if ($this->shop_settings->hide_price) {
                    $value['price'] = $value['formated_price'] = 0;
                    $value['promo_price'] = $value['formated_promo_price'] = 0;
                    $value['special_price'] = $value['formated_special_price'] = 0;
                } else {
                    $config_data = array(
                        'id' => $value['id'],
                        'customer_id' => $this->session->userdata('company_id'),
                    );
                    $temp = $this->site->api_calculate_product_price($config_data);

                    $value['price'] = $this->sma->convertMoney($temp['price']);
                    $value['formated_price'] = $this->sma->convertMoney($temp['price']);
                    $value['formated_promo_price'] = $this->sma->convertMoney($temp['price']);
                    
                    if ($temp['promotion']['price'] > 0) {
                        $value['special_price'] = $temp['temp_price'];
                        $value['formated_special_price'] = $this->sma->convertMoney($temp['temp_price']);
                    } else {
                        $value['special_price'] = $temp['price'];
                        $value['formated_special_price'] = $this->sma->convertMoney($temp['price']);
                    }
                }
            }

            $pagination = pagination('shop/products', $total_rows, $limit);
            $info = array('page' => (isset($filters['page']) && !empty($filters['page']) ? $filters['page'] : 1), 'total' => ceil($total_rows/$limit));

            $this->sma->send_json(array('filters' => $filters, 'products' => $products, 'pagination' => $pagination, 'info' => $info));
        } else {
            $this->sma->send_json(array('filters' => $filters, 'products' => false, 'pagination' => false, 'info' => false));
        }
    }

    // Customer quotations
    public function quotes($id = null, $hash = null)
    {
        if (!$this->loggedIn && !$hash) {
            redirect('login');
        }
        if ($this->Staff) {
            admin_redirect('quotes');
        }
        if ($id) {
            if ($order = $this->shop_model->getQuote(['id' => $id, 'hash' => $hash])) {
                $this->data['inv'] = $order;
                $this->data['rows'] = $this->shop_model->getQuoteItems($id);
                $this->data['customer'] = $this->site->getCompanyByID($order->customer_id);
                $this->data['biller'] = $this->site->getCompanyByID($order->biller_id);
                $this->data['created_by'] = $this->site->getUser($order->created_by);
                $this->data['updated_by'] = $this->site->getUser($order->updated_by);
                $this->data['page_title'] = lang('view_quote');
                $this->data['page_desc'] = '';
                $this->page_construct('pages/view_quote', $this->data);
            } else {
                $this->session->set_flashdata('error', lang('access_denied'));
                redirect('/');
            }
        } else {
            if ($this->input->get('download')) {
                $id = $this->input->get('download', true);
                $order = $this->shop_model->getQuote(['id' => $id]);
                $this->data['inv'] = $order;
                $this->data['rows'] = $this->shop_model->getQuoteItems($id);
                $this->data['customer'] = $this->site->getCompanyByID($order->customer_id);
                $this->data['biller'] = $this->site->getCompanyByID($order->biller_id);
                // $this->data['created_by'] = $this->site->getUser($order->created_by);
                // $this->data['updated_by'] = $this->site->getUser($order->updated_by);
                $this->data['Settings'] = $this->Settings;
                $html = $this->load->view($this->Settings->theme.'/shop/views/pages/pdf_quote', $this->data, true);
                if ($this->input->get('view')) {
                    echo $html;
                    exit;
                } else {
                    $name = lang("quote") . "_" . str_replace('/', '_', $order->reference_no) . ".pdf";
                    $this->sma->generate_pdf($html, $name);
                }
            }
            $page = $this->input->get('page') ? $this->input->get('page', true) : 1;
            $limit = 10;
            $offset = ($page*$limit)-$limit;
            $this->load->helper('pagination');
            $total_rows = $this->shop_model->getQuotesCount();
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            $this->data['orders'] = $this->shop_model->getQuotes($limit, $offset);
            $this->data['pagination'] = pagination('shop/quotes', $total_rows, $limit);
            $this->data['page_info'] = array('page' => $page, 'total' => ceil($total_rows/$limit));
            $this->data['page_title'] = lang('my_orders');
            $this->data['page_desc'] = '';
            $this->page_construct('pages/quotes', $this->data);
        }
    }

    // Customer order/orders page
    public function orders($id = null, $hash = null, $pdf = null, $buffer_save = null)
    {
        
        $hash = $hash ? $hash : $this->input->get('hash', true);
        if (!$this->loggedIn && !$hash) {
            redirect('login');
        }
        if ($this->Staff) {
            admin_redirect('sales');
        }
        
        if ($id && !$pdf) {
            
            //$condition = $this->site->api_get_customer_order_condition();
            $condition = $this->site->api_get_customer_order_condition_v2();
            $config_data = array(
                'table_name' => 'sma_sales',
                'select_table' => 'sma_sales',
                'select_condition' => "id = ".$id." ".$condition,
            );
            $temp = $this->site->api_select_data_v2($config_data);
            if ($temp[0]['id'] <= 0) {
                $this->session->set_flashdata('error', lang('access_denied'));
                redirect('/');
            }

            if ($temp[0]['id'] > 0) {
                foreach ($temp[0] as $key => $value) {
                    $this->data['inv']->{$key} = $value;
                    if ($key == 'company_branch') {
                        if ($value != '') {
                            /*
                            $temp = $this->site->api_select_some_fields_with_where("
                                getTranslate(translate,'en','".f_separate."','".v_separate."') as company_branch_name,
                                getTranslate(add_ons,'address','".f_separate."','".v_separate."') as address
                                "
                                ,"sma_company_branch"
                                ,"id = ".$value
                                ,"arr"
                            );
                            $this->data['inv']->company_branch_name = $temp[0]['company_branch_name'];
                            $this->data['inv']->address = $temp[0]['address'];
                            */
                        }
                    }
                }

                
                $this->data['rows'] = $this->shop_model->getOrderItems($id);
                $this->data['customer'] = $this->site->getCompanyByID($this->data['inv']->customer_id);

                $temp = $this->site->api_select_some_fields_with_where(
                    "
                    *     
                    ",
                    "sma_companies",
                    "id = ".$this->data['customer']->parent_id,
                    "arr"
                );
                if (count($temp) > 0) {
                    $this->data['inv']->parent_name = $temp[0]['company'];
                }

                $this->data['biller'] = $this->site->getCompanyByID($this->data['inv']->biller_id);
                $this->data['address'] = $this->shop_model->getAddressByID($order->address_id);
                $this->data['paypal'] = $this->shop_model->getPaypalSettings();
                $this->data['skrill'] = $this->shop_model->getSkrillSettings();
                $this->data['payments'] = $this->shop_model->getInvoicePayments_v2($id);
                
                $this->data['page_title'] = lang('view_order');
                $this->data['page_desc'] = '';
                $this->page_construct('pages/view_order', $this->data);
            } else {
                $this->session->set_flashdata('error', lang('access_denied'));
                redirect('/');
            }
        } elseif ($pdf || $this->input->get('download')) {
            $id = $pdf ? $id : $this->input->get('download', true);
            $hash = $hash ? $hash : $this->input->get('hash', true);

            $condition = $this->site->api_get_customer_order_condition_v2();

            $config_data = array(
                'table_name' => 'sma_sales',
                'select_table' => 'sma_sales',
                'select_condition' => "id = ".$id." ".$condition,
            );
            $temp = $this->site->api_select_data_v2($config_data);
            if ($temp[0]['id'] <= 0) {
                $this->session->set_flashdata('error', lang('access_denied'));
                redirect('/');
            }

            $order = $this->shop_model->getOrder(['id' => $id, 'hash' => $hash]);
            
            $this->data['inv'] = $temp;
            $this->data['rows'] = $this->shop_model->getOrderItems($id);
            $this->data['customer'] = $this->site->getCompanyByID($temp[0]['customer_id']);
            $this->data['biller'] = $this->site->getCompanyByID($temp[0]['biller_id']);
            $this->data['Settings'] = $this->Settings;
            
            $this->data['shop_settings'] = $this->shop_settings;
            $html = $this->load->view($this->Settings->theme.'/shop/views/pages/pdf_invoice', $this->data, true);
            
            if ($this->input->get('view')) {
                echo $html;
                exit;
            } else {
                $name = lang("Order_Detail") . "_" . $temp[0]['id'] . ".pdf";
                
                if ($buffer_save) {
                    return $this->sma->generate_pdf($html, $name, $buffer_save, $this->data['biller']->invoice_footer);
                } else {
                    $this->sma->generate_pdf($html, $name, false, $this->data['biller']->invoice_footer);
                }
            }
        } elseif (!$id) {
            $page = $this->input->get('page') ? $this->input->get('page', true) : 1;
            $per_page = 24;
            $offset_no = ($page*$per_page)-$per_page;
            $this->load->helper('pagination');

            $config_data = array(
                'user_id' => $this->session->userdata('user_id'),
                'search' => '',
                'category' => '',
                'limit' => $per_page,
                'offset_no' => $offset_no,
            );
            $this->data['orders'] = $this->shop_model->api_get_orders($config_data);

            $config_data = array(
                'user_id' => $this->session->userdata('user_id'),
                'search' => '',
                'category' => '',
                'limit' => '',
                'offset_no' => '',
            );
            $temp = $this->shop_model->api_get_orders($config_data);
            if ($temp[0]['id'] > 0) {
                $total_rows = count($temp);
            }

            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            $this->data['pagination'] = pagination('shop/orders', $total_rows, $per_page);
            $this->data['page_info'] = array('page' => $page, 'total' => ceil($total_rows/$per_page));
            $this->data['page_title'] = lang('my_orders');
            $this->data['page_desc'] = '';
            $this->page_construct('pages/orders', $this->data);
        }
    }

    // Add new Order form shop
    public function order()
    {
        if (!$this->loggedIn) {
            redirect('login');
        }
        //$this->form_validation->set_rules('address', lang("address"), 'trim|required');
        $this->form_validation->set_rules('note', lang("comment"), 'trim');
        $this->form_validation->set_rules('payment_method', lang("payment_method"), 'required');

        if ($this->input->post('payment_method') == 'bank') {
            $this->form_validation->set_rules('your_bank', lang('your_bank'), 'trim|required');
            $this->form_validation->set_rules('your_bank_account_number', lang('your_bank_account_number'), 'trim|required');
            $this->form_validation->set_rules('transfer_reference_number', lang('transfer_reference_number'), 'trim|required');
        }
        if ($this->form_validation->run() == true) {
            $delivery_date = '';
            $temp_add_date = date('Y-m-d H:i:s');
            if ($this->input->post('delivery_date') != '') {
                $delivery_date = $this->input->post('delivery_date');
                $temp_2 = date('w');
                
                if ($temp_2 == 6) {
                    $temp = date('Y-m-d');
                    $temp = date('Y-m-d', strtotime($temp. ' + 2 days'));
                } else {
                    $temp = new DateTime('tomorrow');
                    $temp = $temp->format('Y-m-d');
                }
                                                

                if ($delivery_date == 1) {
                    $delivery_date = date('Y-m-d').' 12:00:00';
                }
                if ($delivery_date == 2) {
                    $delivery_date = $temp.' 9:00:00';
                    $temp_add_date =  $delivery_date;
                }
                if ($delivery_date == 3) {
                    $delivery_date = $temp.' 12:00:00';
                    $temp_add_date =  $delivery_date;
                }
            }
            $temp_add_date = date('Y-m-d H:i:s');
            
            $new_customer = false;
            if ($this->input->post('address') != 'new') {
                $customer = $this->site->getCompanyByID_v2($this->session->userdata('company_id'));
            } else {
                if (!($customer = $this->shop_model->getCompanyByEmail($this->input->post('email')))) {
                    $customer = new stdClass();
                    $customer->name = $this->input->post('name');
                    $customer->company = $this->input->post('company');
                    $customer->phone = $this->input->post('phone');
                    $customer->email = $this->input->post('email');
                    $customer->address = $this->input->post('billing_line1').'<br>'.$this->input->post('billing_line2');
                    $customer->city = $this->input->post('billing_city');
                    $customer->state = $this->input->post('billing_state');
                    $customer->postal_code = $this->input->post('billing_postal_code');
                    $customer->country = $this->input->post('billing_country');
                    $customer_group = $this->shop_model->getCustomerGroup($this->Settings->customer_group);
                    $price_group = $this->shop_model->getPriceGroup($this->Settings->price_group);
                    $customer->customer_group_id = (!empty($customer_group)) ? $customer_group->id : null;
                    $customer->customer_group_name = (!empty($customer_group)) ? $customer_group->name : null;
                    $customer->price_group_id = (!empty($price_group)) ? $price_group->id : null;
                    $customer->price_group_name = (!empty($price_group)) ? $price_group->name : null;
                    $new_customer = true;
                }
            }
            $biller = $this->site->getCompanyByID($this->shop_settings->biller);
            $note = $this->db->escape_str($this->input->post('comment'));
            $product_tax = 0;
            $total = 0;
            $gst_data = [];
            $total_cgst = $total_sgst = $total_igst = 0;

            $product_discount = 0;
            $total_discount = 0;
            foreach ($this->cart->contents() as $item) {
                $item_option = null;
                //MjV7ymIDoHFcXsb2
                if ($product_details = $this->shop_model->getProductForCart($item['product_id'])) {
                    $product_price = isset($product_details->special_price) && $product_details->special_price ? $product_details->special_price : $product_details->price;
                    $price = $this->sma->setCustomerGroupPrice($product_price, $this->customer_group);
                    if ($item['option']) {
                        if ($product_variant = $this->shop_model->getProductVariantByID($item['option'])) {
                            $item_option = $product_variant->id;
                            $price = $product_variant->price+$price;
                        }
                    }
                    $price = round($price, 2, PHP_ROUND_HALF_UP);

                    $item_net_price = $unit_price = $price;
                        
                    $item_quantity = $item_unit_quantity = $item['qty'];
                    $pr_item_tax = $item_tax = 0;
                    $tax = "";

                    if (!empty($product_details->tax_rate)) {
                        $tax_details = $this->site->getTaxRateByID($product_details->tax_rate);
                        $ctax = $this->site->calculateTax($product_details, $tax_details, $unit_price);
                        $item_tax = $ctax['amount'];
                        $tax = $ctax['tax'];
                        if ($product_details->tax_method != 1) {
                            $item_net_price = $unit_price - $item_tax;
                        }
                        $pr_item_tax = $this->sma->formatDecimal(($item_tax * $item_unit_quantity), 4);
                        if ($this->Settings->indian_gst && $gst_data = $this->gst->calculteIndianGST($pr_item_tax, ($biller->state == $customer->state), $tax_details)) {
                            $total_cgst += $gst_data['cgst'];
                            $total_sgst += $gst_data['sgst'];
                            $total_igst += $gst_data['igst'];
                        }
                    }

                    $discount = null;
                    $item_discount = 0;
                    $cp_discount = $this->site->getCPDiscountByIDCID($item['product_id'], $customer->id);
                    $b = 0;
                    if ($cp_discount) {
                        $b = 1;
                        if ($cp_discount->special_price != '' && $cp_discount->special_price > 0) {
                            $item_net_price = $cp_discount->special_price;
                        } elseif ($cp_discount->discount_price > 0) {
                            $p_discount = (($item_net_price * $cp_discount->discount_price) / 100);
                        }
                        $item_net_price = $item_net_price - $p_discount;
                    }
                    if ($customer->discount_product != '' && $b == 0) {
                        $temp = ($item_net_price * $customer->discount_product) / 100;
                        $item_discount = $temp * $item_unit_quantity;
                        $discount = $customer->discount_product.'%';
                        $item_net_price = $item_net_price - $temp;
                        $product_discount = $product_discount + $item_discount;
                        $total_discount = $total_discount + $item_discount;
                    }
                    $item_net_price = round($item_net_price, 1);

                    $config_data = array(
                        'id' => $product_details->id,
                        'customer_id' => $customer->id,
                    );
                    $temp = $this->site->api_calculate_product_price($config_data);
                    $item_net_price = $temp['price'];
                    if ($temp['promotion']['price'] > 0)
                        $temp_discount = $temp['temp_price'] - $temp['price'];
                    else
                        $temp_discount = 0;
                    
                    if ($temp_discount > 0)
                        $temp_2 = $temp['temp_price'];
                    else
                        $temp_2 = $temp['price'];         
                    $temp_2 = $temp_2 * $item['qty'];
                    $temp_total_before_discount += $temp_2;
                    $temp_2 = ($temp_2 * 5) / 100;
                    $temp_service_charge += $temp_2;
                    
                    $product_tax += $pr_item_tax;
                    $subtotal = (round($item_net_price * $item_unit_quantity, 2, PHP_ROUND_HALF_UP) + $pr_item_tax);
                    $subtotal = round($subtotal, 2, PHP_ROUND_HALF_UP);
                    $unit = $this->site->getUnitByID($product_details->unit);

                    if ($temp_discount <= 0)
                        $temp_unit_price = $temp['price'];
                    else
                        $temp_unit_price = $this->sma->formatDecimal($temp['temp_price']);

                    $add_ons = 'initial_first_add_ons:{}:supplier:{}:discount_rate:{'.$temp['promotion']['rate'].'}:';
                    $product = [
                        'product_id' => $product_details->id,
                        'product_code' => $product_details->code,
                        'product_name' => $product_details->name,
                        'product_type' => $product_details->type,
                        'option_id' => $item_option,
                        'net_unit_price' => $this->sma->formatDecimal($temp_unit_price),
                        'unit_price' => $this->sma->formatDecimal($temp['price']),
                        'quantity' => $item_quantity,
                        'product_unit_id' => $unit ? $unit->id : null,
                        'product_unit_code' => $unit ? $unit->code : null,
                        'unit_quantity' => $item_unit_quantity,
                        'warehouse_id' => $this->shop_settings->warehouse,
                        'item_tax' => $pr_item_tax,
                        'tax_rate_id' => $product_details->tax_rate,
                        'tax' => $tax,
                        'discount' => $temp_discount,
                        'item_discount' => 0,
                        'subtotal' => $this->sma->formatDecimal($subtotal),
                        'serial_no' => null,
                        'real_unit_price' => $this->sma->formatDecimal($item_net_price),
                        'add_ons' => $add_ons,
                    ];

                    $products[] = ($product + $gst_data);
                    $total += $this->sma->formatDecimal(round($item_net_price * $item_unit_quantity, 2, PHP_ROUND_HALF_UP), 4);
                } else {
                    $this->session->set_flashdata('error', lang('product_x_found').' ('.$item['name'].')');
                    redirect(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'cart');
                }
            }//end purchase
            $discount_method = false;
            // method discount
            if ($this->input->post('payment_method')) {
                $query_payment_method = $this->db->get_where('payment_discounts', ['name'=>$this->input->post('payment_method')])->row();
                if ($query_payment_method && $query_payment_method->end_at >= date("Y-m-d")&&($query_payment_method->active==1||$query_payment_method->active=="true")) {
                    //$discount_method = true;// get table discount from database and check if have method, return true
                    //$total = ((float)$total - (float)//$total*$query_payment_method->discount/100);
                    // switch ($this->input->post('payment_method')) {
                    //     case 'pipay':
                    //         $total = ((float)$total - (float)$total*$query_payment_method->discount/100);
                    //         break;
                    //     default:
                    //         break;
                    // }
                }
            }
            // end method discount
            
            

            if ($this->input->post('address') == '') {
                if ($customer->distance_from_restaurant != '')
                    $shipping = $this->api_helper->calculate_delivery_fee($customer->distance_from_restaurant);
                else
                    $shipping = 0;
            }
            else {
                $config_data = array(
                    'table_name' => 'sma_addresses',
                    'select_table' => 'sma_addresses',
                    'translate' => '',
                    'description' => '',
                    'select_condition' => "id = ".$this->input->post('address')." and company_id = ".$this->session->userdata('company_id') ,
                );
                $temp = $this->api_helper->api_select_data_v2($config_data);
                if ($temp[0]['distance_from_restaurant'] != '')
                    $shipping = $this->api_helper->calculate_delivery_fee($temp[0]['distance_from_restaurant']);
                else
                    $shipping = 0;                                    
            }
            $temp_total_before_discount = $this->sma->formatDecimal($temp_total_before_discount, 2);
            $total = $this->sma->formatDecimal($total, 2);
            $temp_service_charge = $this->sma->formatDecimal($temp_service_charge, 2);
            $temp_service_charge = 0;
            $shipping = $this->sma->formatDecimal($shipping, 2);

            $temp_vat = ($temp_total_before_discount + $temp_service_charge + $shipping) * 10 / 100;
            $temp_vat = $this->sma->formatDecimal($temp_vat, 2);
            $temp_vat = 0;

            $grand_total = $total + $temp_service_charge + $temp_vat + $shipping;
            //echo $grand_total.'<br>';
            $grand_total = $this->api_helper->round_up_second_decimal($grand_total);
            $grand_total = $this->sma->formatDecimal($grand_total, 4);

            // echo $temp_total_before_discount.'<br>';
            // echo $temp_service_charge.'<br>';
            // echo $shipping.'<br>';
            // echo $temp_vat.'<br>';
            // echo $grand_total.'<br>';
            // exit;

            //$this->site->getReference('so')

            $product_import_date = $this->input->post('add_ons_product_import_date');
          
            $temp_lang = get_cookie('shop_language', TRUE);
            $language_array = unserialize(multi_language);
            $temp_lang_2 = $language_array[0][0];
            if (strtolower($temp_lang) != strtolower($language_array[0][1])) {                
                for ($i=0;$i<count($language_array);$i++) {
                    if (strtolower($temp_lang) == strtolower($language_array[$i][1])) {
                        $temp_lang_2 = $language_array[$i][0];
                        break;
                    }
                }
            }
                        
            $add_ons = 'initial_first_add_ons:{}:cs_reference_no:{}:delivery_date:{'.$delivery_date.'}:product_import_date:{'.$product_import_date.'}:language:{'.$temp_lang_2.'}:';

            $customer_id = $this->input->post('customer_id');
            if ($customer_id > 0) {
                $temp = $this->site->api_select_some_fields_with_where(
                    "*",
                    "sma_companies",
                    "id = ".$customer_id,
                    "arr"
                );
                if ($temp[0]['company'] != '' && $temp[0]['company'] != '-') {
                    $temp_customer_name = $temp[0]['company'];
                } else {
                    $temp_customer_name = $temp[0]['name'];
                }
                $config_data = array(
                    'table_name' => 'sma_users',
                    'select_table' => 'sma_users',
                    'translate' => '',
                    'description' => '',
                    'select_condition' => "id = ".$this->session->userdata('user_id'),
                );
                $temp_user = $this->api_helper->api_select_data_v2($config_data);                  
            }
            $data = [
                'date' => $temp_add_date,
                'reference_no' => '',
                'customer_id' => $customer_id,
                'customer' => $temp_user[0]['first_name'].' '.$temp_user[0]['last_name'],
                'biller_id' => $biller->id,
                'biller' => ($biller->company && $biller->company != '-' ? $biller->company : $biller->name),
                'warehouse_id' => $this->shop_settings->warehouse,
                'note' => $note,
                'staff_note' => null,
                'total' => $total,
                'product_discount' => 0,
                'order_discount_id' => null,
                'order_discount' => 0,
                'total_discount' => 0,
                'product_tax' => $product_tax,
                'order_tax_id' => $temp_order_tax,
                'order_tax' => $temp_vat,
                'total_tax' => $temp_vat,
                'shipping' => $shipping,
                'grand_total' => $grand_total,
                'total_items' => $this->cart->total_items(),
                'sale_status' => 'pending',
                'payment_status' => 'pending',
                'payment_term' => null,
                'due_date' => null,
                'paid' => 0,
                'created_by' => $this->session->userdata('user_id') ? $this->session->userdata('user_id') : null,
                'shop' => 1,
                'address_id' => $this->input->post('address'),
                'hash' => hash('sha256', microtime() . mt_rand()),
                'payment_method' => $this->input->post('payment_method'),
                'service_charge' => $temp_service_charge,
                'add_ons' => $add_ons,
            ];
            if ($this->Settings->invoice_view == 2) {
                $data['cgst'] = $total_cgst;
                $data['sgst'] = $total_sgst;
                $data['igst'] = $total_igst;
            }

            if ($new_customer) {
                $customer = (array) $customer;
            }

            if ($sale_id = $this->shop_model->addSale($data, $products, $customer, $address)) {
                $this->order_received($sale_id);

                $this->load->library('sms');
                $this->sms->newSale($sale_id);
                $this->cart->destroy();
                $this->session->set_flashdata('info', lang('order_added_make_payment'));
 

                $temp_2 = $this->site->api_select_some_fields_with_where(
                    "*",
                    "sma_companies",
                    "id = ".$data['customer_id'],
                    "arr"
                );
                $temp_3 = array();
                if ($temp_2[0]['parent_id'] > 0) {
                    $temp_3 = $this->site->api_select_some_fields_with_where(
                        "*",
                        "sma_companies",
                        "id = ".$temp_2[0]['parent_id'],
                        "arr"
                    );
                }


                $temp = '\nCompany: '.$data['customer'];
                if (count($temp_3) > 0) {
                    $temp .= '\nParent Company: '.$temp_3[0]['company'];
                }

                $temp2 = '';
                if ($delivery_date != '') {
                    $temp_3 = explode(' ', $delivery_date);
                    if ($temp_3[1] == '9:00:00') {
                        $temp_4 = '9:00 AM to 12:00 AM';
                    } else {
                        $temp_4 = '12:00 PM to 7:00 PM';
                    }

                    $temp2 .= '\nDelivery Date: '.$temp_3[0].' '.$temp_4;
                }
                $temp_item = $this->site->api_select_some_fields_with_where(
                    "*",
                    "sma_sale_items",
                    "sale_id = ".$sale_id." order by id asc",
                    "arr"
                );
                if ($data['payment_method'] == 'account_receivable') {
                    $temp_method = 'account_payable';
                } elseif ($data['payment_method'] == 'cod') {
                    $temp_method = 'cash on delivery';
                } else {
                    $temp_method = $data['payment_method'];
                }

                $temp_method = str_replace('_', ' ', $temp_method);
                $temp_method = ucfirst($temp_method);
                if ($temp_method == 'Cash on delivery') {
                    $temp_method = 'COD';
                }

                $temp3 = '\nPayment Method: '.$temp_method;

                if ($this->api_shop_setting[0]['product_import_date'] == 1 && base_url() == 'https://air.phsarjapan.com/') {
                    $temp3 .= '\nImport Date: '.$product_import_date;
                }


                $temp_display_2 = 'Order ID: '.$sale_id.$temp.$temp2.$temp3.'\n\n';
                $temp_display = '';
                $j = 1;
                for ($i=0;$i<count($temp_item);$i++) {
                    $temp_display .= $j.'. '.$temp_item[$i]['product_name'].', Qty: '.number_format($temp_item[$i]['quantity'], 2).'\n';
                    $j++;
                }
                $temp_display_2 = $temp_display_2.$temp_display;
                if ($data['total_tax'] > 0) {
                    $temp_display_2 .= '\nTotal: '.$this->sma->formatMoney($data['total']).'\n';
                    $temp_display_2 .= 'Vat 10%: '.$this->sma->formatMoney($data['total_tax']).'';
                }
                $temp_display_2 .= '\nTotal Amount: '.$this->sma->formatMoney($data['grand_total']);

                if ($data['note'] != '') {
                    $temp_note = preg_replace("/[\/\&%#\$]/", "", $data['note']);
                    $temp_note = preg_replace("/[\"\']/", "", $temp_note);
                    $temp_display_2 .= '\n\n\nNote:\n'.$temp_note;
                }


                if (base_url() == 'https://order.daishintc.com/') {
                    $line_message = '
                    curl -v -X POST https://api.line.me/v2/bot/message/broadcast \
                    -H \'Content-Type:application/json\' \
                    -H \'Authorization: Bearer {FNtBe9JBGzmXbvr1dp5ODw1xdofgqtOC7KHfVosqKnRqA+9tal1O8tl3YjYZNJcamhDtZgvA0kJuHcf0LpA8q0vwKAfwzH8KVvVn5podwiWijuCl9NLeoUCp2Cyh6bDHpcuDDswGQml6UO5ICg1MRQdB04t89/1O/w1cDnyilFU=}\' \
                    -d \'{
                        "messages":[
                            {
                                "type":"text",
                                "text":"'.$temp_display_2.'"
                            }
                        ]
                    }\'';
                }
                if (base_url() == 'https://dev-invoice.camboinfo.com/' || base_url() == 'https://demo.phsarjapan.com/') {
                    $line_message = '
                    curl -v -X POST https://api.line.me/v2/bot/message/broadcast \
                    -H \'Content-Type:application/json\' \
                    -H \'Authorization: Bearer {UNTVh6YcE/XXoEOJ1lSrGjuP/ftpl1QsXvAggeidgD014fTA5KooVZ5FU9UFY7eZ0oS/ajspc+pBxzSEnBfJcnAmK+yE6nsgru8g58sjg7BzWV+mZawRtaQ7b1skK7TgefvFtxUCgD0etJD+jcFdLwdB04t89/1O/w1cDnyilFU=}\' \
                    -d \'{
                        "messages":[
                            {
                                "type":"text",
                                "text":"'.$temp_display_2.'"
                            }
                        ]
                    }\'';
                }
                if (base_url() == 'https://phsarjapan.com/' || base_url() == 'https://air.phsarjapan.com/') {
                    $line_message = '
                    curl -v -X POST https://api.line.me/v2/bot/message/broadcast \
                    -H \'Content-Type:application/json\' \
                    -H \'Authorization: Bearer {8gM7JhLa/RbLgM1xgY5dwCslVKkzARkRHEE6xIw+VQ2dYlg/1oGazuHkuJpR1hc/Xl1IbFRkx76kP3dE3aA1hKDsuc5w+SrKPtDvkW21CZPkw0IqBHuLSsZCwAFWvZ0QaIXoy96Ip8ivO+oWBTw6lAdB04t89/1O/w1cDnyilFU=}\' \
                    -d \'{
                        "messages":[
                            {
                                "type":"text",
                                "text":"'.$temp_display_2.'"
                            }
                        ]
                    }\'';
                }
    
                shell_exec($line_message);
                if ($this->input->post('payment_method') == 'paypal') {
                    redirect('pay/paypal/'.$sale_id);
                } elseif ($this->input->post('payment_method') == 'skrill') {
                    redirect('pay/skrill/'.$sale_id);
                } elseif ($this->input->post('payment_method') == 'pipay') {
                    if (base_url() == 'https://demo.phsarjapan.com/') {
                        $date = date_create($data['date']);
                        $temp_date = date_format($date, 'D M d Y H:i:s');
                        $mid = $this->api_shop_setting[0]['pipay_mid'];
                        $grand_total = number_format($grand_total, 2);
                        echo '
                        <form class="api_display_none" id="pipayform" name="pipayform" action="https://onlinepayment-test.pipay.com/starttransaction" method="post">
                                <input type="hidden" name="mid" value="103425"/>
                                <input type="hidden" name="lang" value="en"/>
                                <input type="hidden" name="orderid" value="'.$sale_id.'"/>
                                <input type="hidden" name="orderDesc" value="TESTING ENVIRONMENT"/>
                                <input type="hidden" name="orderAmount" value="'.$grand_total.'"/>
                                <input type="hidden" name="currency" value="USD"/>
                                <!--<input type="hidden" name="payerEmail" value="john@test.gr"/>-->
                                <input type="hidden" name="payerPhone" value=""/>
                                <input type="hidden" name="sid" value="8195"/>
                                <input type="hidden" name="did" value="13105"/>
                                <input type="hidden" name="orderDate" value="'.$temp_date.'"/>
                                <input type="hidden" name="payMethod" value="wallet"/>
                                <input type="hidden" name="trType" value="2"/>
                                <input type="hidden" name="cancelTimer" value="0" />
                                <input type="hidden" name="confirmURL" value="'.base_url().'pay/pipay_return/'.$sale_id.'"/>
                                <input type="hidden" name="cancelURL" value="'.base_url().'shop/orders/'.$sale_id.'"/>
                                <input type="hidden" name="var1" value="Daishin additional"/>
                                <input type="hidden" name="description" value="Remark field" />
                                <input type="hidden" name="successScreenDelay" value="0" />
                                <input type="hidden" name="digest" value="'.$digest.'"/>                            
                            </form>
                            <script>
                                document.pipayform.submit();
                            </script>
                        ';
                    } elseif (base_url() != 'https://air.phsarjapan.com/') {
                        $date = date_create($data['date']);
                        $temp_date = date_format($date, 'D M d Y H:i:s');
                        $mid = $this->api_shop_setting[0]['pipay_mid'];
                        $grand_total = number_format($grand_total, 2);
                        $digest = md5($mid.$sale_id.$grand_total);
                        echo '
                            <form class="api_display_none" id="pipayform" name="pipayform" action="https://onlinepayment.pipay.com/starttransaction" method="post">
                                <input type="hidden" name="mid" value="'.$this->api_shop_setting[0]['pipay_mid'].'"/>
                                <input type="hidden" name="lang" value="en"/>
                                <input type="hidden" name="orderid" value="'.$sale_id.'"/>
                                <input type="hidden" name="orderDesc" value="'.$this->api_shop_setting[0]['pipay_title'].'"/>
                                <input type="hidden" name="orderAmount" value="'.$grand_total.'"/>
                                <input type="hidden" name="currency" value="USD"/>
                                <!--<input type="hidden" name="payerEmail" value="john@test.gr"/>-->
                                <input type="hidden" name="payerPhone" value=""/>
                                <input type="hidden" name="sid" value="'.$this->api_shop_setting[0]['pipay_sid'].'"/>
                                <input type="hidden" name="did" value="'.$this->api_shop_setting[0]['pipay_did'].'"/>
                                <input type="hidden" name="orderDate" value="'.$temp_date.'"/>
                                <input type="hidden" name="payMethod" value="wallet"/>
                                <input type="hidden" name="trType" value="2"/>
                                <input type="hidden" name="cancelTimer" value="0" />
                                <input type="hidden" name="confirmURL" value="'.base_url().'pay/pipay_return/'.$sale_id.'"/>
                                <input type="hidden" name="cancelURL" value="'.base_url().'shop/orders/'.$sale_id.'"/>
                                <input type="hidden" name="var1" value="Daishin additional"/>
                                <input type="hidden" name="description" value="Remark field" />
                                <input type="hidden" name="successScreenDelay" value="0" />
                                <input type="hidden" name="digest" value="'.$digest.'"/>                            
                            </form>
                            <script>
                                document.pipayform.submit();
                            </script>
                        ';
                    } else {
                        $date = date_create($data['date']);
                        $temp_date = date_format($date, 'D M d Y H:i:s');
                        $mid = '117367';
                        $grand_total = number_format($grand_total, 2);
                        $digest = md5($mid.$sale_id.$grand_total);
                        echo '
                            <form class="api_display_none" id="pipayform" name="pipayform" action="https://onlinepayment.pipay.com/starttransaction" method="post">
                                <input type="hidden" name="mid" value="117367"/>
                                <input type="hidden" name="lang" value="en"/>
                                <input type="hidden" name="orderid" value="'.$sale_id.'"/>
                                <input type="hidden" name="orderDesc" value="'.$this->api_shop_setting[0]['pipay_title'].'"/>
                                <input type="hidden" name="orderAmount" value="'.$grand_total.'"/>
                                <input type="hidden" name="currency" value="USD"/>
                                <!--<input type="hidden" name="payerEmail" value="john@test.gr"/>-->
                                <input type="hidden" name="payerPhone" value=""/>
                                <input type="hidden" name="sid" value="324391"/>
                                <input type="hidden" name="did" value="324532"/>
                                <input type="hidden" name="orderDate" value="'.$temp_date.'"/>
                                <input type="hidden" name="payMethod" value="wallet"/>
                                <input type="hidden" name="trType" value="2"/>
                                <input type="hidden" name="cancelTimer" value="0" />
                                <input type="hidden" name="confirmURL" value="'.base_url().'pay/pipay_return/'.$sale_id.'"/>
                                <input type="hidden" name="cancelURL" value="'.base_url().'shop/orders/'.$sale_id.'"/>
                                <input type="hidden" name="var1" value="Daishin additional"/>
                                <input type="hidden" name="description" value="Remark field" />
                                <input type="hidden" name="successScreenDelay" value="0" />
                                <input type="hidden" name="digest" value="'.$digest.'"/>                            
                            </form>
                            <script>
                                document.pipayform.submit();
                            </script>
                        ';
                    }
                } elseif ($this->input->post('payment_method') == 'wing') {
                    if (base_url() != 'https://air.phsarjapan.com/') {
                        echo '                        
                            <form class="api_display_none" id="wingform" name="wingform" action="https://wingsdk.wingmoney.com:334/" method="POST" style="display:none;">
                                <input name="sandbox" type="text" value="0"><br/>
                                <input name="amount" type="text" id="amount" value="'.number_format($grand_total, 2).'"><br/>
                                <input name="username" type="text" value="'.$this->api_shop_setting[0]['wing_username'].'"><br/>
                                <input name="rest_api_key" type="text" value="'.$this->api_shop_setting[0]['wing_rest_api_key'].'"><br/>
                                <input name="return_url" type="text" value="'.base_url().'pay/wing_return"><br/>
                                <input name="bill_till_rbtn" type="text" value="0"><br/>
                                <input name="bill_till_number" type="text" value="'.$this->api_shop_setting[0]['wing_biller_id'].'"><br/>
                                <input name="default_wing" type="text" value="0"><br/>
                                <input name="remark" type="text" value="'.$sale_id.'"><br>
                                <input name="is_inquiry" type="text" value="1"><br/>
                            </form>
                            <script>
                                document.wingform.submit();
                            </script>
                        ';
                    } else {
                        echo '                        
                            <form class="api_display_none" id="wingform" name="wingform" action="https://wingsdk.wingmoney.com:334/" method="POST" style="display:none;">
                                <input name="sandbox" type="text" value="0"><br/>
                                <input name="amount" type="text" id="amount" value="'.number_format($grand_total, 2).'"><br/>
                                <input name="username" type="text" value="online.phsar"><br/>
                                <input name="rest_api_key" type="text" value="adfec51a4cee0c57c0e6aaf2835c4e093c1c8aebddf0f011750a3ecda722a3b8"><br/>
                                <input name="return_url" type="text" value="'.base_url().'pay/wing_return"><br/>
                                <input name="bill_till_rbtn" type="text" value="0"><br/>
                                <input name="bill_till_number" type="text" value="5225"><br/>
                                <input name="default_wing" type="text" value="0"><br/>
                                <input name="remark" type="text" value="'.$sale_id.'"><br>
                                <input name="is_inquiry" type="text" value="1"><br/>
                            </form>
                            <script>
                                document.wingform.submit();
                            </script>
                        ';
                    }
                } elseif ($this->input->post('payment_method') == 'bank') {
                    $settings = $this->shop_model->getSettings_v2(1);
                    $add_ons = 'initial_first_add_ons:{}:customer_account_number:{'.$this->input->post('your_bank_account_number').'}:company_account_number:{'.$settings->{$this->input->post('your_bank').'_account_number'}.'}:transfer_reference_number:{'.$this->input->post('transfer_reference_number').'}:transfer_amount:{'.$grand_total.'}:';
                    $payment = array(
                            'date' => date('Y-m-d H:i:s'),
                            'sale_id' => $sale_id,
                            'reference_no' => $this->site->getReference('pay'),
                            'amount' => 0,
                            'paid_by' => $this->input->post('your_bank'),
                            'transaction_id' => '',
                            'type' => 'received',
                            'note' => '',
                            'add_ons' => $add_ons
                        );
                    $this->pay_model->addPayment($payment);
                    shop_redirect('orders/'.$sale_id.'/'.($this->loggedIn ? '' : $data['hash']));
                } elseif ($this->input->post('payment_method') == 'account_receivable') {
                    if ($customer->payment_category != '' && $customer->payment_category != 'cash on delivery') {
                        $temp2['payment_status'] = 'partial';
                        $this->site->api_update_table('sma_sales', $temp2, "id = ".$sale_id);
                    }
                    shop_redirect('orders/'.$sale_id.'/'.($this->loggedIn ? '' : $data['hash']));
                } else {
                    shop_redirect('orders/'.$sale_id.'/'.($this->loggedIn ? '' : $data['hash']));
                }
            }
        } else {
            $data['customer'] = $customer;

            $this->session->set_flashdata('error', validation_errors());
            redirect('cart/checkout'.($guest_checkout ? '#guest' : ''));
        }
    }

    // Customer address list
    public function addresses()
    {
        if (!$this->loggedIn) {
            redirect('login');
        }
        if ($this->Staff) {
            admin_redirect('customers');
        }
        $uid = $this->site->getUser();
        $config_data = array(
            'table_name' => 'sma_addresses',
            'select_table' => 'sma_addresses',
            'translate' => '',
            'select_condition' => "id > 0 and company_id = ".$this->session->userdata('company_id'),
        );
        $temp = $this->site->api_select_data_v2($config_data);
    
        $this->session->set_userdata('requested_page', $this->uri->uri_string());
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['addresses'] = $this->shop_model->getAddresses();
        $this->data['addresses_v2'] = $temp;
        $this->data['page_title'] = lang('my_addresses');
        $this->data['page_desc'] = '';
        $this->page_construct('pages/addresses', $this->data);
    }

    // Add/edit customer address
    public function address($id = null)
    {
        if (!$this->loggedIn) {
            $this->sma->send_json(array('status' => 'error', 'message' => lang('please_login')));
        }
        // $this->form_validation->set_rules('line1', lang("line1"), 'trim|required');
        // $this->form_validation->set_rules('line2', lang("line2"), 'trim|required');
        // $this->form_validation->set_rules('city', lang("city"), 'trim|required');
        // $this->form_validation->set_rules('state', lang("state"), 'trim|nullable');
        // $this->form_validation->set_rules('postal_code', lang("postal_code"), 'trim|nullable');
        $this->form_validation->set_rules('phone', lang("phone"), 'trim|required');

        if ($this->form_validation->run() == true) {
            $user_addresses = $this->shop_model->getAddresses();
            if (count($user_addresses) >= 6) {
                $this->sma->send_json(array('status' => 'error', 'message' => lang('already_have_max_addresses'), 'level' => 'error'));
            }

            $add_ons = 'initial_first_add_ons:{}:';
            foreach ($_POST as $name => $value) {
                $value = $this->input->post($name);
                if (is_int(strpos($name, "add_ons_"))) {
                    $temp_name = str_replace('add_ons_', '', $name);
                    $add_ons .= $temp_name.':{'.$value.'}:';
                }
            }
            $data = ['line1' => $this->input->post('line1'),
                'line2' => $this->input->post('line2'),
                'phone' => $this->input->post('phone'),
                'city_id' => $this->input->post('city_id'),
                'state' => $this->input->post('state'),
                'postal_code' => $this->input->post('postal_code'),
                'country' => $this->input->post('country'),
                'company_id' => $this->session->userdata('company_id'),
                'add_ons' => $add_ons
            ];
                
            if ($id) {
                $this->db->update('addresses', $data, ['id' => $id]);
                $this->session->set_flashdata('message', lang('address_updated'));
                $this->sma->send_json(array('redirect' => $_SERVER['HTTP_REFERER']));
            } else {
                $this->db->insert('addresses', $data);
                $this->session->set_flashdata('message', lang('address_added'));
                $this->sma->send_json(array('redirect' => $_SERVER['HTTP_REFERER']));
            }
        } elseif ($this->input->is_ajax_request()) {
            $this->sma->send_json(array('status' => 'error', 'message' => validation_errors()));
        } else {
            shop_redirect('shop/addresses');
        }
    }

    // Customer wishlist page
    public function wishlist()
    {
        if (!$this->loggedIn) {
            redirect('login');
        }

        $search = $this->input->get('search');
        $search = $this->db->escape_str($search);
        $search_category = $this->input->get('category');

        $config_data = array(
            'user_id' => $this->session->userdata('user_id'),
        );
        $this->shop_model->api_wishlist_generate_order_number($config_data);
        
        $page = $this->input->get('page') ? $this->input->get('page', true) : 1;
        $per_page = 24;
        $offset_no = ($page*$per_page)-$per_page;

        $this->load->helper('pagination');
        $this->load->helper('text');

        $config_data = array(
            'user_id' => $this->session->userdata('user_id'),
            'search' => $search,
            'category' => $search_category,
            'limit' => $per_page,
            'offset_no' => $offset_no,
        );
        $select_data = $this->shop_model->apiGetWishlist($config_data);
        for ($i=0;$i<count($select_data);$i++) {
            $item = $this->shop_model->getProductByID($select_data[$i]['product_id']);
            $config_data = array(
                'id' => $select_data[$i]['product_id'],
            );
            $temp_hide = $this->shop_model->api_get_product_display($config_data);
            $item->is_hided = $temp_hide['result'];
            $item->details = character_limiter(strip_tags($item->details), 140);
            $item->order_number = $select_data[$i]['order_number'];
            $items[] = $item;
        }
        $this->data['items'] = $items;

        $config_data = array(
            'user_id' => $this->session->userdata('user_id'),
            'search' => $search,
            'category' => $search_category,
            'limit' => '',
            'offset_no' => '',
        );
        $temp = $this->shop_model->apiGetWishlist($config_data);
        if (is_array($temp)) {
            $total_rows = count($temp);
        } else {
            $total_rows = 0;
        }

        $config_data = array(
            'table_name' => 'sma_categories',
            'select_table' => 'sma_categories',
            'select_condition' => "parent_id = 0 and getTranslate(add_ons,'display','".f_separate."','".v_separate."') != 'no'",
        );
        $select_category = $this->site->api_select_data_v2($config_data);
        $this->data['select_category'] = $select_category;

        $temp = '';
        if ($search != '') {
            $temp .= '?search='.$search;
        }
        if ($search != '' && $search_category != '') {
            $temp .= '&category='.$search_category;
        }
        if ($search == '' && $search_category != '') {
            $temp .= '?category='.$search_category;
        }

        $this->session->set_userdata('requested_page', $this->uri->uri_string());
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        $config_base_url = site_url() . 'shop/wishlist'.$temp; //your url where the content is displayed
        $config['base_url'] = $config_base_url; //your url where the content is displayed
        $config['query_string_segment'] = 'page';
        $config['per_page'] = $per_page; //$per_page;
        $config['num_links'] = 5;
        $config['use_page_numbers'] = true;
        $config['page_query_string'] = true;
        $config['full_tag_open'] = '<ul class="pagination pagination-new-bg">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['prev_link'] = '&lt; Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next &gt;';
        $config['next_tag_open'] = '<li class="next">';
        $config['next_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['total_rows'] = $total_rows; //$this->data['total_rows_sale'];
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $user_id = ($this->sma->session->userdata('user_id')) ? $this->sma->session->userdata('user_id') : 0;
  

        //-promotion-=====================================
        $config_data = array(
            'limit' => 18,
        );
        $temp = $this->site->api_get_promotion_product($config_data);
        $this->data['promotion_products'] = $temp['select_data'];
        $this->data['promotion_products_total'] = $temp['select_data_total'];
        //-promotion-=====================================
        
        //-featured-=====================================
        $config_data = array(
            'limit' => 18,
        );
        $temp = $this->site->api_get_featured_product($config_data);
        $this->data['featured_products'] = $temp['select_data'];
        $this->data['featured_products_total'] = $temp['select_data_total'];
        //-featured-=====================================

        
        //$this->data['pagination'] = pagination('shop/wishlist', $total_rows, $per_page);
        $this->data['page_info'] = array('page' => $page, 'total' => ceil($total_rows/$per_page));
        $this->data['page_title'] = lang('wishlist');
        $this->data['page_desc'] = '';
        $this->page_construct('pages/wishlist', $this->data);
    }

    // Send us email
    public function send_message()
    {
        $this->form_validation->set_rules('name', lang("name"), 'required');
        $this->form_validation->set_rules('email', lang("email"), 'required|valid_email');
        $this->form_validation->set_rules('subject', lang("subject"), 'required');
        $this->form_validation->set_rules('message', lang("message"), 'required');

        if ($this->form_validation->run() == true) {
            try {
                if ($this->sma->send_email($this->shop_settings->email, $this->input->post('subject'), $this->input->post('message'), $this->input->post('email'), $this->input->post('name'))) {
                    $this->sma->send_json(array('status' => 'Success', 'message' => lang('message_sent')));
                }
                $this->sma->send_json(array('status' => 'error', 'message' => lang('action_failed')));
            } catch (Exception $e) {
                $this->sma->send_json(array('status' => 'error', 'message' => $e->getMessage()));
            }
        } elseif ($this->input->is_ajax_request()) {
            $this->sma->send_json(array('status' => 'Error!', 'message' => validation_errors(), 'level' => 'error'));
        } else {
            $this->session->set_flashdata('warning', 'Please try to send message from contact page!');
            shop_redirect();
        }
    }

    // Add attachment to sale on manual payment
    public function manual_payment($order_id)
    {
        if ($_FILES['payment_receipt']['size'] > 0) {
            $this->load->library('upload');
            $config['upload_path'] = 'files/';
            $config['allowed_types'] = 'zip|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif|txt';
            $config['max_size'] = 2048;
            $config['overwrite'] = false;
            $config['max_filename'] = 25;
            $config['encrypt_name'] = true;
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('payment_receipt')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('error', $error);
                redirect($_SERVER["HTTP_REFERER"]);
            }
            $manual_payment = $this->upload->file_name;
            $this->db->update('sales', ['attachment' => $manual_payment], ['id' => $order_id]);
            $this->session->set_flashdata('message', lang('file_submitted'));
            redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : '/shop/orders');
        }
    }

    // Digital products download
    public function downloads($id = null, $hash = null)
    {
        if (!$this->loggedIn) {
            redirect('login');
        }
        if ($this->Staff) {
            admin_redirect();
        }
        if ($id && $hash && md5($id) == $hash) {
            $sale = $this->shop_model->getDownloads(1, 0, $id);
            if (!empty($sale)) {
                $product = $this->site->getProductByID($id);
                if (file_exists('./files/'.$product->file)) {
                    $this->load->helper('download');
                    force_download('./files/'.$product->file, null);
                    exit;
                } else {
                    header('Content-Description: File Transfer');
                    header('Content-Type: application/octet-stream');
                    header("Content-Transfer-Encoding: Binary");
                    header("Content-disposition: attachment; filename=\"" . basename($product->file) . "\"");
                    // header('Content-Length: ' . filesize($product->file));
                    readfile($product->file);
                }
            }
            $this->session->set_flashdata('error', lang('file_x_exist'));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $page = $this->input->get('page') ? $this->input->get('page', true) : 1;
            $limit = 10;
            $offset = ($page*$limit)-$limit;
            $this->load->helper('pagination');
            $total_rows = $this->shop_model->getDownloadsCount();
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            $this->data['downloads'] = $this->shop_model->getDownloads($limit, $offset);
            $this->data['pagination'] = pagination('shop/download', $total_rows, $limit);
            $this->data['page_info'] = array('page' => $page, 'total' => ceil($total_rows/$limit));
            $this->data['page_title'] = lang('my_downloads');
            $this->data['page_desc'] = '';
            $this->page_construct('pages/downloads', $this->data);
        }
    }

    public function order_received($id = null, $hash = null)
    {
        if ($inv = $this->shop_model->getOrder(['id' => $id, 'hash' => $hash])) {
            $user = $inv->created_by ? $this->site->getUser($inv->created_by) : null;
            $customer = $this->site->getCompanyByID_v2($inv->customer_id);
            $biller = $this->site->getCompanyByID($inv->biller_id);
            $this->load->library('parser');

            $sale_id = $id;
            $config_data = array(
                'table_name' => 'sma_sales',
                'select_table' => 'sma_sales',
                'select_condition' => "id = ".$id,
            );
            $select_data = $this->site->api_select_data_v2($config_data);

            $temp = '';
            $temp_branch_post = $select_data[0]['company_branch'];

            $temp_2 = $this->site->api_select_some_fields_with_where(
                "
                    *
                    ",
                "sma_companies",
                "id = ".$select_data[0]['customer_id'],
                "arr"
            );
            $temp_3 = array();
            if ($temp_2[0]['parent_id'] > 0) {
                $temp_3 = $this->site->api_select_some_fields_with_where(
                    "
                        *
                        ",
                    "sma_companies",
                    "id = ".$temp_2[0]['parent_id'],
                    "arr"
                );
            }
            $temp = '<br>Company: '.$select_data[0]['customer'];
            if (count($temp_3) > 0) {
                $temp .= '<br>Parent Company: '.$temp_3[0]['company'];
            }


            $temp2 = '';
            if ($delivery_date != '') {
                $temp_3 = explode(' ', $delivery_date);
                if ($temp_3[1] == '9:00:00') {
                    $temp_4 = '9:00 AM to 12:00 AM';
                } else {
                    $temp_4 = '12:00 PM to 7:00 PM';
                }

                $temp2 .= '<br>Delivery Date: '.$temp_3[0].' '.$temp_4;
            }
            $temp_item = $this->site->api_select_some_fields_with_where(
                "*
                        ",
                "sma_sale_items",
                "sale_id = ".$sale_id." order by id asc",
                "arr"
            );
            if ($data['payment_method'] == 'account_receivable') {
                $temp_method = 'account_payable';
            } elseif ($data['payment_method'] == 'cod') {
                $temp_method = 'cash on delivery';
            } else {
                $temp_method = $select_data[0]['payment_method'];
            }

            $temp_method = str_replace('_', ' ', $temp_method);
            $temp_method = ucfirst($temp_method);

            $temp_method = preg_replace('/[^a-zA-Z0-9_. ()+]/', '-', $temp_method);
            if ($temp_method == 'Cash on delivery' || $temp_method == 'Cod') {
                $temp_method = 'COD';
            }

            $temp3 = '<br>Payment Method: '.$temp_method;

            if ($this->api_shop_setting[0]['product_import_date'] == 1 && base_url() == 'https://air.phsarjapan.com/') {
                $temp3 .= '<br>Import Date: '.$select_data[0]['product_import_date'];
            }

            $customer->company = preg_replace('/[^a-zA-Z0-9_. ()+]/', '-', $customer->company);
            $temp_display_2 = 'Order ID: '.$sale_id.$temp.$temp2.$temp3.'<br><br>';
            $temp_display = '';
            $j = 1;
            for ($i=0;$i<count($temp_item);$i++) {
                $temp_item[$i]['product_name'] = preg_replace('/[^a-zA-Z0-9_. ()+]/', '-', $temp_item[$i]['product_name']);
                $temp_display .= $j.'. '.$temp_item[$i]['product_name'].', Qty: '.number_format($temp_item[$i]['quantity'], 2).'<br>';
                $j++;
            }
            $temp_display_2 = $temp_display_2.$temp_display;
            if ($select_data[0]['total_tax'] > 0) {
                $temp_display_2 .= '<br>Total: '.$this->sma->formatMoney($select_data[0]['total']).'<br>';
                $temp_display_2 .= 'Vat 10: '.$this->sma->formatMoney($select_data[0]['total_tax']);
            }
            $temp_display_2 .= '<br>Total Amount: '.$this->sma->formatMoney($select_data[0]['grand_total']);
            $temp_display_2 .= '<br>Delivery Fee: '.$this->sma->formatMoney($select_data[0]['shipping']);

            if ($select_data[0]['note'] != '') {
                $temp_note = preg_replace("/[\/\&%#\$]/", "", $select_data[0]['note']);
                $temp_note = preg_replace("/[\"\']/", "", $temp_note);
                $temp_note = str_replace('\r', '', $temp_note);
                $temp_note = str_replace('\n', '<br>', $temp_note);
                $temp_display_2 .= '<br><br><br>Note:<br>'.$temp_note;
            }
            
            $this->load->model('pay_model');
            $paypal = $this->pay_model->getPaypalSettings();
            $skrill = $this->pay_model->getSkrillSettings();
            $btn_code = '<div id="payment_buttons" class="text-center margin010">';
            if (!empty($this->shop_settings->bank_details)) {
                echo '<div style="width:100%;">'.$this->shop_settings->bank_details.'</div><hr class="divider or">';
            }
            if ($paypal->active == "1" && $inv->grand_total != "0.00") {
                if (trim(strtolower($customer->country)) == $biller->country) {
                    $paypal_fee = $paypal->fixed_charges + ($inv->grand_total * $paypal->extra_charges_my / 100);
                } else {
                    $paypal_fee = $paypal->fixed_charges + ($inv->grand_total * $paypal->extra_charges_other / 100);
                }
                //$btn_code .= '<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business=' . $paypal->account_email . '&item_name=' . $inv->reference_no . '&item_number=' . $inv->id . '&image_url=' . base_url() . 'assets/uploads/logos/' . $this->Settings->logo . '&amount=' . (($inv->grand_total - $inv->paid) + $paypal_fee) . '&no_shipping=1&no_note=1&currency_code=' . $this->default_currency->code . '&bn=BuyNow&rm=2&return=' . admin_url('sales/view/' . $inv->id) . '&cancel_return=' . admin_url('sales/view/' . $inv->id) . '&notify_url=' . admin_url('payments/paypalipn') . '&custom=' . $inv->reference_no . '__' . ($inv->grand_total - $inv->paid) . '__' . $paypal_fee . '"><img src="' . base_url('assets/images/btn-paypal.png') . '" alt="Pay by PayPal"></a> ';
            }
            if ($skrill->active == "1" && $inv->grand_total != "0.00") {
                if (trim(strtolower($customer->country)) == $biller->country) {
                    $skrill_fee = $skrill->fixed_charges + ($inv->grand_total * $skrill->extra_charges_my / 100);
                } else {
                    $skrill_fee = $skrill->fixed_charges + ($inv->grand_total * $skrill->extra_charges_other / 100);
                }
                //$btn_code .= ' <a href="https://www.moneybookers.com/app/payment.pl?method=get&pay_to_email=' . $skrill->account_email . '&language=EN&merchant_fields=item_name,item_number&item_name=' . $inv->reference_no . '&item_number=' . $inv->id . '&logo_url=' . base_url() . 'assets/uploads/logos/' . $this->Settings->logo . '&amount=' . (($inv->grand_total - $inv->paid) + $skrill_fee) . '&return_url=' . admin_url('sales/view/' . $inv->id) . '&cancel_url=' . admin_url('sales/view/' . $inv->id) . '&detail1_description=' . $inv->reference_no . '&detail1_text=Payment for the sale invoice ' . $inv->reference_no . ': ' . $inv->grand_total . '(+ fee: ' . $skrill_fee . ') = ' . $this->sma->formatMoney($inv->grand_total + $skrill_fee) . '&currency=' . $this->default_currency->code . '&status_url=' . admin_url('payments/skrillipn') . '"><img src="' . base_url('assets/images/btn-skrill.png') . '" alt="Pay by Skrill"></a>';
            }


            if (base_url() == 'https://order.daishintc.com/') {
                $to = 'daishinorder@gmail.com';
            } elseif (base_url() == 'https://phsarjapan.com/' || base_url() == 'https://air.phsarjapan.com/') {
                $to = 'terashida1515@gmail.com';
            } else {
                $to = 'apipeapi@gmail.com';
                
            }

            if (base_url() == 'https://order.daishintc.com/') {
                $subject = 'Daishintc Order #'.$id;
            }
            if (base_url() == 'https://dev-invoice.camboinfo.com/' || base_url() == 'https://demo.phsarjapan.com/') {
                $subject = 'Daishintc Order #'.$id;
            }
            if (base_url() == 'https://phsarjapan.com/' || base_url() == 'https://air.phsarjapan.com/') {
                $subject = 'Japan Shop Order #'.$id;
            }
            
            $to = $this->Settings->default_email;
            $subject = $this->Settings->site_name.' Order #'.$id;



            $message = $temp_display_2;
                    
            $message = str_replace('&', 'and', $message);

            $attachment = '';
            $cc = '';
            $bcc = '';
            if (!is_int(strpos($_SERVER["HTTP_HOST"], "localhost"))) {
                if ($this->sma->send_email($to, $subject, $message, $this->Settings->default_email, null, $attachment, $cc, $bcc)) {
                }
            }

            $parse_data = array(
                'order_id' => $inv->id,
                'contact_person' => $customer->name,
                'company' => $customer->company && $customer->company != '-' ? '('.$customer->company.')' : '',
                'order_link' => shop_url('orders/'.$id.'/'.($this->loggedIn ? '' : $data['hash'])),
                'site_link' => base_url(),
                'site_name' => $this->Settings->site_name,
                'logo' => '<img src="' . base_url() . 'assets/uploads/logos/' . $this->shop_settings->logo . '" />',
            );
            $msg = file_get_contents('./themes/' . $this->Settings->theme . '/admin/views/email_templates/sale.html');
            $message = $this->parser->parse_string($msg, $parse_data);

            $attachment = $this->orders($id, null, true, 'S');

            $sent = $error = $cc = $bcc = false;
            $cc = '';
            $bcc = '';
            $warehouse = $this->site->getWarehouseByID($inv->warehouse_id);

            if ($warehouse->email) {
                //$bcc .= ','.$warehouse->email;
            }
            try {
                if (!is_int(strpos($_SERVER["HTTP_HOST"], "localhost"))) {
                    if ($this->sma->send_email(($user ? $user->email : $customer->email), $subject, $message, null, null, $attachment, $cc, $bcc)) {
                        delete_files($attachment);
                        $sent = true;
                        $this->session->set_userdata('api_order_send_mail','send');
                    }
                }
            } catch (Exception $e) {
                $error = $e->getMessage();
                $this->session->set_userdata('api_order_send_mail', $error);
            }
            return ['sent' => $sent, 'error' => $error];
        }
    }

    public function order_history()
    {
        if (!$this->loggedIn) {
            redirect('login');
        }
        
        $page = $this->input->get('page') ? $this->input->get('page', true) : 1;
        $per_page = 10;
        $offset_no = ($page*$per_page)-$per_page;
        $this->load->helper('pagination');
        $this->load->helper('text');

        $config_data = array(
            'user_id' => $this->session->userdata('user_id'),
            'limit' => $per_page,
            'offset_no' => $offset_no,
        );
        $select_data = $this->shop_model->getOrderHistory($config_data);
        for ($i=0;$i<count($select_data);$i++) {
            $item = $this->shop_model->getProductByID($select_data[$i]['product_id']);
            $item->details = character_limiter(strip_tags($item->details), 140);
            $items[] = $item;
        }
        $this->data['items'] = $items;

        $config_data = array(
            'user_id' => $this->session->userdata('user_id'),
            'limit' => '',
            'offset_no' => '',
        );
        $temp = $this->shop_model->getOrderHistory($config_data);
        $total_rows = count($temp);
        
        $this->session->set_userdata('requested_page', $this->uri->uri_string());
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['pagination'] = pagination('shop/order_history', $total_rows, $per_page);

        $this->data['page_info'] = array('page' => $page, 'total' => ceil($total_rows/$per_page));
        $this->data['page_title'] = lang('order_history');
        $this->data['page_desc'] = '';
        $this->page_construct('pages/order_history', $this->data);
    }

    public function api_change_favorite_order()
    {
        if ($this->session->userdata('user_id') != '') {
            $temp = $this->site->api_select_some_fields_with_where(
                "
                id
                ",
                "sma_wishlist",
                "product_id = ".$this->input->get('id')." and user_id = ".$this->session->userdata('user_id'),
                "arr"
            );
            if (is_array($temp)) {
                if (count($temp) > 0) {
                    $value = $this->input->get('value');
                    if ($value == 1) {
                        $value = 0;
                    }
                    $config_data = array(
                    'table_name' => 'sma_wishlist',
                    'id_name' => 'id',
                    'field_add_ons_name' => 'add_ons',
                    'selected_id' => $temp[0]['id'],
                    'add_ons_title' => 'order_number',
                    'add_ons_value' => $value,
                );
                    $this->site->api_update_add_ons_field($config_data);
                }
            }
            
            $temp3[1] = 'api-ajax-request-multiple-result-split';
            $temp3[1] .= $this->input->get('id');
            $result = $temp3[1];
            echo $result;
        } else {
            echo 'api-ajax-request-multiple-result-split'.'error';
        }
    }
    
    public function api_favorite_order_drop()
    {
        if ($this->session->userdata('user_id') != '') {
            foreach ($_GET as $name => $value) {
                ${$name} = $value;
            }
            $temp_2 = explode('-', $id);
            if (is_array($temp_2)) {
                if (count($temp_2) > 0) {
                    $j = 1;
                    for ($i=0;$i<count($temp_2);$i++) {
                        if ($temp_2[$i] != '') {
                            $temp = $this->site->api_select_some_fields_with_where(
                                "
                            id
                            ",
                                "sma_wishlist",
                                "product_id = ".$temp_2[$i]." and user_id = ".$this->session->userdata('user_id'),
                                "arr"
                            );
                            if ($temp[0]['id'] != '') {
                                $config_data = array(
                                'table_name' => 'sma_wishlist',
                                'id_name' => 'id',
                                'field_add_ons_name' => 'add_ons',
                                'selected_id' => $temp[0]['id'],
                                'add_ons_title' => 'order_number',
                                'add_ons_value' => $j,
                            );
                                $this->site->api_update_add_ons_field($config_data);
                                $j++;
                            }
                        }
                    }
                }
            }

            $temp3[1] = 'api-ajax-request-multiple-result-split';
            $temp3[1] .= 'Success';
            $result = $temp3[1];
            echo $result;
        } else {
            echo 'api-ajax-request-multiple-result-split'.'error';
        }
    }

    public function api_resend_email_activation_link()
    {        
        if ($this->session->userdata('user_id') > 0) {
            $activation_code = sha1(md5(microtime()));
            $temp = array(
                'activation_code' => $activation_code,
            );
            $this->db->update('sma_users', $temp, "id = ".$this->session->userdata('user_id'));
            $temp = $this->site->api_select_some_fields_with_where(
                "
                activation_code, active
                ",
                "sma_users",
                "id = ".$this->session->userdata('user_id'),
                "arr"
            );
        }
        $data = array(
            'id' => $this->session->userdata('user_id'),
            'email' => $this->session->userdata('email'),
            'activation' => $temp[0]['activation_code'],
        );
        $this->load->library('parser');
        
        $parse_data = array(
            'user_name' => $this->session->userdata('first_name') . ' ' . $this->session->userdata('last_name'),
            'site_link' => site_url(),
            'site_name' => $this->Settings->site_name,
            'email' => $data['email'],
            'activation_link' => anchor('activate/' . $data['id'] . '/' . $data['activation'], lang('email_activate_link')),
            'logo' => '<img src="' . base_url() . 'assets/uploads/logos/' . $this->shop_settings->logo . '" alt="' . $this->shop_settings->shop_name . '"/>'
        );

        $msg = file_get_contents('./themes/' . $this->Settings->theme . '/admin/views/email_templates/activate_email.html');

        $message = $this->parser->parse_string($msg, $parse_data);
        $subject = $this->lang->line('email_activation_subject') . ' - ' . $this->shop_settings->shop_name;

        try {
            if ($this->sma->send_email($data['email'], $subject, $message,$this->Settings->default_email, null, '', '', '')) {
                $this->session->set_flashdata('reminder', lang("An_activation_email_link_has_send_successfully"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } catch (Exception $e) {
            $this->session->set_flashdata('error', $e->getMessage());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function consignment($id = null)
    {
        if (!$this->loggedIn) {
            redirect('login');
        }
        if ($this->Staff) {
            admin_redirect('consignment');
        }

        if ($id > 0) {
            $condition = $this->site->api_get_customer_order_condition_v2();
            $config_data = array(
                'table_name' => 'sma_consignment',
                'select_table' => 'sma_consignment',
                'select_condition' => "id = ".$id." ".$condition,
            );
            $temp = $this->site->api_select_data_v2($config_data);
            if ($temp[0]['id'] <= 0) {
                $this->session->set_flashdata('error', lang('access_denied'));
                redirect('/');
            }

            if ($temp[0]['id'] > 0) {
                foreach ($temp[0] as $key => $value) {
                    $this->data['inv']->{$key} = $value;
                }

                $this->data['rows'] = $this->shop_model->getConsignmentItems($id);
                $this->data['customer'] = $this->site->getCompanyByID($this->data['inv']->customer_id);

                $temp = $this->site->api_select_some_fields_with_where(
                    "
                    *     
                    ",
                    "sma_companies",
                    "id = ".$this->data['customer']->parent_id,
                    "arr"
                );
                if (count($temp) > 0) {
                    $this->data['inv']->parent_name = $temp[0]['company'];
                }

                $this->data['biller'] = $this->site->getCompanyByID($this->data['inv']->biller_id);
                $this->data['address'] = $this->shop_model->getAddressByID($order->address_id);
                $this->data['paypal'] = $this->shop_model->getPaypalSettings();
                $this->data['skrill'] = $this->shop_model->getSkrillSettings();
                $this->data['payments'] = $this->shop_model->getInvoicePayments_v2($id);
                
                $this->data['page_title'] = lang('Consignment_Details');
                $this->data['page_desc'] = '';
                $this->page_construct('pages/view_consignment', $this->data);
            } else {
                $this->session->set_flashdata('error', lang('access_denied'));
                redirect('/');
            }
        } else {
            $page = $this->input->get('page') ? $this->input->get('page', true) : 1;
            $per_page = 24;
            $offset_no = ($page*$per_page)-$per_page;
            $this->load->helper('pagination');

            $config_data = array(
                'user_id' => $this->session->userdata('user_id'),
                'search' => '',
                'category' => '',
                'limit' => $per_page,
                'offset_no' => $offset_no,
            );
            $this->data['orders'] = $this->shop_model->api_get_consignment($config_data);

            $config_data = array(
                'user_id' => $this->session->userdata('user_id'),
                'search' => '',
                'category' => '',
                'limit' => '',
                'offset_no' => '',
            );
            $temp = $this->shop_model->api_get_consignment($config_data);
            if ($temp[0]['id'] > 0) {
                $total_rows = count($temp);
            }

            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            $this->data['pagination'] = pagination('shop/consignment', $total_rows, $per_page);
            $this->data['page_info'] = array('page' => $page, 'total' => ceil($total_rows/$per_page));
            $this->data['page_title'] = lang('Consignment');
            $this->data['page_desc'] = '';
            $this->page_construct('pages/consignment', $this->data);
        }
    }

    public function order_consignment()
    {
        if (!$this->loggedIn) {
            redirect('login');
        }
        //$this->form_validation->set_rules('address', lang("address"), 'trim|required');
        $this->form_validation->set_rules('note', lang("comment"), 'trim');
        $this->form_validation->set_rules('payment_method', lang("payment_method"), 'required');

        if ($this->input->post('payment_method') == 'bank') {
            $this->form_validation->set_rules('your_bank', lang('your_bank'), 'trim|required');
            $this->form_validation->set_rules('your_bank_account_number', lang('your_bank_account_number'), 'trim|required');
            $this->form_validation->set_rules('transfer_reference_number', lang('transfer_reference_number'), 'trim|required');
        }

        if ($this->form_validation->run() == true) {
            $delivery_date = '';
            $temp_add_date = date('Y-m-d H:i:s');
            if ($this->input->post('delivery_date') != '') {
                $delivery_date = $this->input->post('delivery_date');
                $temp_2 = date('w');
                
                if ($temp_2 == 6) {
                    $temp = date('Y-m-d');
                    $temp = date('Y-m-d', strtotime($temp. ' + 2 days'));
                } else {
                    $temp = new DateTime('tomorrow');
                    $temp = $temp->format('Y-m-d');
                }
                                                

                if ($delivery_date == 1) {
                    $delivery_date = date('Y-m-d').' 12:00';
                }
                if ($delivery_date == 2) {
                    $delivery_date = $temp.' 9:00';
                    $temp_add_date =  $delivery_date;
                }
                if ($delivery_date == 3) {
                    $delivery_date = $temp.' 12:00 ';
                    $temp_add_date =  $delivery_date;
                }
            }
            $new_customer = false;
            if ($this->input->post('address') != 'new') {
                $customer = $this->site->getCompanyByID_v2($this->session->userdata('company_id'));
            } else {
                if (!($customer = $this->shop_model->getCompanyByEmail($this->input->post('email')))) {
                    $customer = new stdClass();
                    $customer->name = $this->input->post('name');
                    $customer->company = $this->input->post('company');
                    $customer->phone = $this->input->post('phone');
                    $customer->email = $this->input->post('email');
                    $customer->address = $this->input->post('billing_line1').'<br>'.$this->input->post('billing_line2');
                    $customer->city = $this->input->post('billing_city');
                    $customer->state = $this->input->post('billing_state');
                    $customer->postal_code = $this->input->post('billing_postal_code');
                    $customer->country = $this->input->post('billing_country');
                    $customer_group = $this->shop_model->getCustomerGroup($this->Settings->customer_group);
                    $price_group = $this->shop_model->getPriceGroup($this->Settings->price_group);
                    $customer->customer_group_id = (!empty($customer_group)) ? $customer_group->id : null;
                    $customer->customer_group_name = (!empty($customer_group)) ? $customer_group->name : null;
                    $customer->price_group_id = (!empty($price_group)) ? $price_group->id : null;
                    $customer->price_group_name = (!empty($price_group)) ? $price_group->name : null;
                    $new_customer = true;
                }
            }
            $biller = $this->site->getCompanyByID($this->shop_settings->biller);
            $note = $this->db->escape_str($this->input->post('comment'));
            $product_tax = 0;
            $total = 0;
            $gst_data = [];
            $total_cgst = $total_sgst = $total_igst = 0;

            $product_discount = 0;
            $total_discount = 0;
            foreach ($this->cart->contents() as $item) {
                $item_option = null;
                //MjV7ymIDoHFcXsb2
                if ($product_details = $this->shop_model->getProductForCart($item['product_id'])) {
                    $product_price = isset($product_details->special_price) && $product_details->special_price ? $product_details->special_price : $product_details->price;
                    $price = $this->sma->setCustomerGroupPrice($product_price, $this->customer_group);
                    if ($item['option']) {
                        if ($product_variant = $this->shop_model->getProductVariantByID($item['option'])) {
                            $item_option = $product_variant->id;
                            $price = $product_variant->price+$price;
                        }
                    }
                    $price = round($price, 2, PHP_ROUND_HALF_UP);

                    $item_net_price = $unit_price = $price;
                        
                    $item_quantity = $item_unit_quantity = $item['qty'];
                    $pr_item_tax = $item_tax = 0;
                    $tax = "";

                    if (!empty($product_details->tax_rate)) {
                        $tax_details = $this->site->getTaxRateByID($product_details->tax_rate);
                        $ctax = $this->site->calculateTax($product_details, $tax_details, $unit_price);
                        $item_tax = $ctax['amount'];
                        $tax = $ctax['tax'];
                        if ($product_details->tax_method != 1) {
                            $item_net_price = $unit_price - $item_tax;
                        }
                        $pr_item_tax = $this->sma->formatDecimal(($item_tax * $item_unit_quantity), 4);
                        if ($this->Settings->indian_gst && $gst_data = $this->gst->calculteIndianGST($pr_item_tax, ($biller->state == $customer->state), $tax_details)) {
                            $total_cgst += $gst_data['cgst'];
                            $total_sgst += $gst_data['sgst'];
                            $total_igst += $gst_data['igst'];
                        }
                    }

                    $discount = null;
                    $item_discount = 0;
                    $cp_discount = $this->site->getCPDiscountByIDCID($item['product_id'], $customer->id);
                    $b = 0;
                    if ($cp_discount) {
                        $b = 1;
                        if ($cp_discount->special_price != '' && $cp_discount->special_price > 0) {
                            $item_net_price = $cp_discount->special_price;
                        } elseif ($cp_discount->discount_price > 0) {
                            $p_discount = (($item_net_price * $cp_discount->discount_price) / 100);
                        }
                        $item_net_price = $item_net_price - $p_discount;
                    }
                    if ($customer->discount_product != '' && $b == 0) {
                        $temp = ($item_net_price * $customer->discount_product) / 100;
                        $item_discount = $temp * $item_unit_quantity;
                        $discount = $customer->discount_product.'%';
                        $item_net_price = $item_net_price - $temp;
                        $product_discount = $product_discount + $item_discount;
                        $total_discount = $total_discount + $item_discount;
                    }
                    $item_net_price = round($item_net_price, 2, PHP_ROUND_HALF_UP);

                    $config_data = array(
                        'id' => $product_details->id,
                        'customer_id' => $customer->id,
                    );
                    $temp = $this->site->api_calculate_product_price($config_data);
                    $item_net_price = $temp['price'];
                                        
                    $product_tax += $pr_item_tax;
                    $subtotal = (round($item_net_price * $item_unit_quantity, 2, PHP_ROUND_HALF_UP) + $pr_item_tax);
                    $subtotal = round($subtotal, 2, PHP_ROUND_HALF_UP);
                    $unit = $this->site->getUnitByID($product_details->unit);

                        
                    $product = [
                        'product_id' => $product_details->id,
                        'product_code' => $product_details->code,
                        'product_name' => $product_details->name,
                        'product_type' => $product_details->type,
                        'option_id' => $item_option,
                        'net_unit_price' => $this->sma->formatDecimal($item_net_price),
                        'unit_price' => $this->sma->formatDecimal($item_net_price + $item_tax),
                        'quantity' => $item_quantity,
                        'product_unit_id' => $unit ? $unit->id : null,
                        'product_unit_code' => $unit ? $unit->code : null,
                        'unit_quantity' => $item_unit_quantity,
                        'warehouse_id' => $this->shop_settings->warehouse,
                        'item_tax' => $pr_item_tax,
                        'tax_rate_id' => $product_details->tax_rate,
                        'tax' => $tax,
                        'discount' => 0,
                        'item_discount' => 0,
                        'subtotal' => $this->sma->formatDecimal($subtotal),
                        'serial_no' => null,
                        'real_unit_price' => $this->sma->formatDecimal($item_net_price),
                        ];

                    $products[] = ($product + $gst_data);
                    $total += $this->sma->formatDecimal(round($item_net_price * $item_unit_quantity, 2, PHP_ROUND_HALF_UP), 4);
                } else {
                    $this->session->set_flashdata('error', lang('product_x_found').' ('.$item['name'].')');
                    redirect(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'cart');
                }
            }
                                
            $shipping = $this->shop_settings->shipping;

            if ($customer->vat_no && $customer->vat_invoice_type != 'do') {
                $temp_order_tax = 2;
            } else {
                $temp_order_tax = 1;
            }
            $order_tax = $this->site->calculateOrderTax($temp_order_tax, ($total + $product_tax));
            //$order_tax = $this->site->calculateOrderTax($this->Settings->default_tax_rate2, ($total + $product_tax));
            $total_tax = $this->sma->formatDecimal(($product_tax + $order_tax), 4);
            $grand_total = $this->sma->formatDecimal(($total + $total_tax + $shipping), 4);

            //$this->site->getReference('so')
          
            $add_ons = 'initial_first_add_ons:{}:cs_reference_no:{}:delivery_date:{'.$delivery_date.'}:';

            $customer_id = $this->input->post('customer_id');
            if ($customer_id > 0) {
                $temp = $this->site->api_select_some_fields_with_where(
                    "
                        *     
                        ",
                    "sma_companies",
                    "id = ".$customer_id,
                    "arr"
                );
                if ($temp[0]['company'] != '' && $temp[0]['company'] != '-') {
                    $temp_customer_name = $temp[0]['company'];
                } else {
                    $temp_customer_name = $temp[0]['name'];
                }
            }


            $data = [
                'date' => $temp_add_date,
                'reference_no' => '',
                'customer_id' => $customer_id,
                'customer' => $temp_customer_name,
                'biller_id' => $biller->id,
                'biller' => ($biller->company && $biller->company != '-' ? $biller->company : $biller->name),
                'warehouse_id' => $this->shop_settings->warehouse,
                'note' => $note,
                'staff_note' => null,
                'total' => $total,
                'product_discount' => 0,
                'order_discount_id' => null,
                'order_discount' => 0,
                'total_discount' => 0,
                'product_tax' => $product_tax,
                'order_tax_id' => $this->Settings->default_tax_rate2,
                'order_tax' => $order_tax,
                'total_tax' => $total_tax,
                'shipping' => $shipping,
                'grand_total' => $grand_total,
                'total_items' => $this->cart->total_items(),
                'sale_status' => 'pending',
                'payment_status' => 'pending',
                'payment_term' => null,
                'due_date' => null,
                'paid' => 0,
                'created_by' => $this->session->userdata('user_id') ? $this->session->userdata('user_id') : null,
                'shop' => 1,
                'address_id' => ($this->input->post('address') == 'new') ? '' : $address->id,
                'hash' => hash('sha256', microtime() . mt_rand()),
                'payment_method' => $this->input->post('payment_method'),
                'add_ons' => $add_ons,
                ];
                
            if ($this->Settings->invoice_view == 2) {
                $data['cgst'] = $total_cgst;
                $data['sgst'] = $total_sgst;
                $data['igst'] = $total_igst;
            }

            if ($new_customer) {
                $customer = (array) $customer;
            }


            if ($sale_id = $this->shop_model->addConsignment($data, $products, $customer, $address)) {
                $this->consignment_received($sale_id);

                $this->load->library('sms');
                $this->sms->newSale($sale_id);
                $this->cart->destroy();
                $this->session->set_flashdata('info', lang('order_added_make_payment'));
 

                $temp_2 = $this->site->api_select_some_fields_with_where(
                    "
                        *
                        ",
                    "sma_companies",
                    "id = ".$data['customer_id'],
                    "arr"
                );
                $temp_3 = array();
                if ($temp_2[0]['parent_id'] > 0) {
                    $temp_3 = $this->site->api_select_some_fields_with_where(
                        "
                            *
                            ",
                        "sma_companies",
                        "id = ".$temp_2[0]['parent_id'],
                        "arr"
                    );
                }


                $temp = '\nCompany: '.$data['customer'];
                if (count($temp_3) > 0) {
                    $temp .= '\nParent Company: '.$temp_3[0]['company'];
                }

                $temp2 = '';
                if ($delivery_date != '') {
                    $temp_3 = explode(' ', $delivery_date);
                    if ($temp_3[1] == '9:00:00') {
                        $temp_4 = '9:00 AM to 12:00 AM';
                    } else {
                        $temp_4 = '12:00 PM to 7:00 PM';
                    }

                    $temp2 .= '\nDelivery Date: '.$temp_3[0].' '.$temp_4;
                }
                $temp_item = $this->site->api_select_some_fields_with_where(
                    "*
                        ",
                    "sma_sale_items",
                    "sale_id = ".$sale_id." order by id asc",
                    "arr"
                );
                if ($data['payment_method'] == 'account_receivable') {
                    $temp_method = 'account_payable';
                } elseif ($data['payment_method'] == 'cod') {
                    $temp_method = 'cash on delivery';
                } else {
                    $temp_method = $data['payment_method'];
                }

                $temp_method = str_replace('_', ' ', $temp_method);
                $temp_method = ucfirst($temp_method);
                if ($temp_method == 'Cash on delivery') {
                    $temp_method = 'COD';
                }

                $temp3 = '\nPayment Method: '.$temp_method;

                $temp_display_2 = 'Order ID: '.$sale_id.$temp.$temp2.$temp3.'\n\n';
                $temp_display = '';
                $j = 1;
                for ($i=0;$i<count($temp_item);$i++) {
                    $temp_display .= $j.'. '.$temp_item[$i]['product_name'].', Qty: '.number_format($temp_item[$i]['quantity'], 2).'\n';
                    $j++;
                }
                $temp_display_2 = $temp_display_2.$temp_display;
                if ($data['total_tax'] > 0) {
                    $temp_display_2 .= '\nTotal: '.$this->sma->formatMoney($data['total']).'\n';
                    $temp_display_2 .= 'Vat 10%: '.$this->sma->formatMoney($data['total_tax']).'';
                }
                $temp_display_2 .= '\nTotal Amount: '.$this->sma->formatMoney($data['grand_total']);

                if ($data['note'] != '') {
                    $temp_note = preg_replace("/[\/\&%#\$]/", "", $data['note']);
                    $temp_note = preg_replace("/[\"\']/", "", $temp_note);
                    $temp_display_2 .= '\n\n\nNote:\n'.$temp_note;
                }
                if ($this->input->post('payment_method') == 'account_receivable') {
                    if ($customer->payment_category != '' && $customer->payment_category != 'cash on delivery') {
                        $temp2['payment_status'] = 'partial';
                        $this->site->api_update_table('sma_sales', $temp2, "id = ".$sale_id);
                    }
                    shop_redirect('consignment/'.$sale_id.'/'.($this->loggedIn ? '' : $data['hash']));
                } else {
                    shop_redirect('consignment/'.$sale_id.'/'.($this->loggedIn ? '' : $data['hash']));
                }
            }
        } else {
            $data['customer'] = $customer;
            $this->session->set_flashdata('error', validation_errors());
            redirect('cart/checkout'.($guest_checkout ? '#guest' : ''));
        }
    }

    public function consignment_received($id = null, $hash = null)
    {
        if ($inv = $this->shop_model->getConsignment(['id' => $id, 'hash' => $hash])) {
            $user = $inv->created_by ? $this->site->getUser($inv->created_by) : null;
            $customer = $this->site->getCompanyByID_v2($inv->customer_id);
            $biller = $this->site->getCompanyByID($inv->biller_id);
            $this->load->library('parser');

            $sale_id = $id;
            $config_data = array(
                'table_name' => 'sma_consignment',
                'select_table' => 'sma_consignment',
                'select_condition' => "id = ".$id,
            );
            $select_data = $this->site->api_select_data_v2($config_data);
            $temp = '';
            $temp_branch_post = $select_data[0]['company_branch'];

            $temp_2 = $this->site->api_select_some_fields_with_where(
                "
                    *
                    ",
                "sma_companies",
                "id = ".$select_data[0]['customer_id'],
                "arr"
            );
            $temp_3 = array();
            if ($temp_2[0]['parent_id'] > 0) {
                $temp_3 = $this->site->api_select_some_fields_with_where(
                    "
                        *
                        ",
                    "sma_companies",
                    "id = ".$temp_2[0]['parent_id'],
                    "arr"
                );
            }
            $temp = '<br>Company: '.$select_data[0]['customer'];
            if (count($temp_3) > 0) {
                $temp .= '<br>Parent Company: '.$temp_3[0]['company'];
            }


            $temp2 = '';
            if ($delivery_date != '') {
                $temp_3 = explode(' ', $delivery_date);
                if ($temp_3[1] == '9:00:00') {
                    $temp_4 = '9:00 AM to 12:00 AM';
                } else {
                    $temp_4 = '12:00 PM to 7:00 PM';
                }

                $temp2 .= '<br>Delivery Date: '.$temp_3[0].' '.$temp_4;
            }
            $temp0 .= '<br>Reference No: '.$select_data[0]['reference_no'];
            $temp_item = $this->site->api_select_some_fields_with_where(
                "*
                        ",
                "sma_consignment_items",
                "sale_id = ".$sale_id." order by id asc",
                "arr"
            );
            if ($data['payment_method'] == 'account_receivable') {
                $temp_method = 'account_payable';
            } elseif ($data['payment_method'] == 'cod') {
                $temp_method = 'cash on delivery';
            } else {
                $temp_method = $select_data[0]['payment_method'];
            }

            $temp_method = str_replace('_', ' ', $temp_method);
            $temp_method = ucfirst($temp_method);

            $temp_method = preg_replace('/[^a-zA-Z0-9_. ()+]/', '-', $temp_method);
            if ($temp_method == 'Cash on delivery' || $temp_method == 'Cod') {
                $temp_method = 'COD';
            }

            $temp3 = '<br>Payment Method: '.$temp_method;

            $customer->company = preg_replace('/[^a-zA-Z0-9_. ()+]/', '-', $customer->company);
            $temp_display_2 = 'Consignment ID: '.$sale_id.$temp0.$temp.$temp2.$temp3.'<br><br>';
            $temp_display = '';
            $j = 1;
            for ($i=0;$i<count($temp_item);$i++) {
                $temp_item[$i]['product_name'] = preg_replace('/[^a-zA-Z0-9_. ()+]/', '-', $temp_item[$i]['product_name']);
                $temp_display .= $j.'. '.$temp_item[$i]['product_name'].', Qty: '.number_format($temp_item[$i]['quantity'], 2).'<br>';
                $j++;
            }
            $temp_display_2 = $temp_display_2.$temp_display;
            if ($select_data[0]['total_tax'] > 0) {
                $temp_display_2 .= '<br>Total: '.$this->sma->formatMoney($select_data[0]['total']).'<br>';
                $temp_display_2 .= 'Vat 10: '.$this->sma->formatMoney($select_data[0]['total_tax']);
            }
            $temp_display_2 .= '<br>Total Amount: '.$this->sma->formatMoney($select_data[0]['grand_total']);

            if ($select_data[0]['note'] != '') {
                $temp_note = preg_replace("/[\/\&%#\$]/", "", $select_data[0]['note']);
                $temp_note = preg_replace("/[\"\']/", "", $temp_note);
                $temp_note = str_replace('\r', '', $temp_note);
                $temp_note = str_replace('\n', '<br>', $temp_note);
                $temp_display_2 .= '<br><br><br>Note:<br>'.$temp_note;
            }

            if (base_url() == 'https://order.daishintc.com/') {
                $to = 'daishinorder@gmail.com';
            } elseif (base_url() == 'https://phsarjapan.com/') {
                $to = 'terashida1515@gmail.com';
            } else {
                $to = 'apipeapi@gmail.com';
            }

            if (base_url() == 'https://order.daishintc.com/') {
                $subject = 'Daishintc Consignment Order #'.$id;
            }
            if (base_url() == 'https://dev-invoice.camboinfo.com/') {
                $subject = 'Daishintc Consignment Order #'.$id;
            }
            if (base_url() == 'https://phsarjapan.com/') {
                $subject = 'Japan Shop Consignment Order #'.$id;
            }

            $message = $temp_display_2;
                    
            $message = str_replace('&', 'and', $message);

            $attachment = '';
            $cc = '';
            $bcc = '';
            if (!is_int(strpos($_SERVER["HTTP_HOST"], "localhost"))) {
                if ($this->sma->send_email($to, $subject, $message, $this->Settings->default_email, null, $attachment, $cc, $bcc)) {
                }
            }

            $parse_data = array(
                'order_id' => $inv->id,
                'contact_person' => $customer->name,
                'company' => $customer->company && $customer->company != '-' ? '('.$customer->company.')' : '',
                'order_link' => shop_url('orders/'.$id.'/'.($this->loggedIn ? '' : $data['hash'])),
                'site_link' => base_url(),
                'site_name' => $this->Settings->site_name,
                'logo' => '<img src="' . base_url() . 'assets/uploads/logos/' . $this->shop_settings->logo . '" />',
            );
            $msg = file_get_contents('./themes/' . $this->Settings->theme . '/admin/views/email_templates/sale.html');
            $message = $this->parser->parse_string($msg, $parse_data);

            //$attachment = $this->orders($id, null, true, 'S');
        }
    }

    public function api_set_import_date()
    {
        foreach ($_GET as $name => $value) {
            ${$name} = $value;
        }
        $this->session->set_userdata('api_selected_import_date', $value);

        $temp3[1] = 'api-ajax-request-multiple-result-split';
        $temp3[1] .= $value;
        $temp3[2] = 'api-ajax-request-multiple-result-split';
        $result = $temp3[1].$temp3[2];
        echo $result;
    }

    public function list_category()
    {
        $category = $this->site->api_select_some_fields_with_where(
            "*",
            "sma_categories",
            "parent_id = 0",
            "arr"
        );
        $this->data['category'] = $category;
        return $this->page_construct('sub_page/list_category', $this->data);
    }

    public function api_ajax_order_insert() {
        if (!$this->loggedIn) { redirect('login'); }
        if ($this->Staff) { admin_redirect('sales'); }  
        
        foreach ($_GET as $name => $value) {
            ${$name} = $value;
        }        
        
        if ($payment_method == 'payway_credit' || $payment_method == 'payway_aba_pay') {
            
            $transaction_id = $this->session->userdata('user_id').'-'.time();
            $temp_value = $delivery_date.'<api>'.$this->session->userdata('company_id').'<api>'.$comment.'<api>'.$add_ons_product_import_date.'<api>'.$payment_method.'<api>'.$transaction_id.'<api>';
            $config_data = array(
                'table_name' => 'sma_users',
                'id_name' => 'id',
                'field_add_ons_name' => 'add_ons',
                'selected_id' => $this->session->userdata('user_id'),
                'add_ons_title' => 'payway_order_information',
                'add_ons_value' => $temp_value,
            );
            $this->site->api_update_add_ons_field($config_data);  

            $config_data = array(
                'insert' => 0,
                'delivery_date' => $delivery_date,
                'customer_id' =>$this->session->userdata('company_id'),
                'comment' => $comment,
                'product_import_date' => $add_ons_product_import_date,
                'payment_method' => $payment_method,
            );            
            $temp_result = $this->shop_model->api_order_insert($config_data);
            
            if ($temp_result['grand_total'] > 0) {
                $temp_id = 5;
                if ($this->api_shop_setting[0]['air_base_url'] == base_url()){
                    //if ($this->session->userdata('user_id') == 37)
                        $temp_id = 6;
                }

                $temp = $this->site->api_select_some_fields_with_where("
                    *     
                    "
                    ,"sma_gateways_details"
                    ,"payment_gateway_id = ".$temp_id." order by id asc"
                    ,"arr"
                );
                $temp_result['grand_total'] = $temp_result['grand_total'] - $this->api_shop_setting[0]['shipping'];
                $hash = base64_encode(hash_hmac('sha512', $temp[1]['value'].$transaction_id.number_format($temp_result['grand_total'],2).$this->api_shop_setting[0]['shipping'], $temp[2]['value'], true));

                if ($payment_method == 'payway_credit')
                    $temp_payment_option = 'cards';
                if ($payment_method == 'payway_aba_pay')
                    $temp_payment_option = 'abapay';
            
                $api_result = 'success';
            }
            else 
                $api_result = $temp_result['result'];

            $temp33[1] = 'api-ajax-request-multiple-result-split';  
            $temp33[1] .= $api_result;
            $temp33[2] = 'api-ajax-request-multiple-result-split';            
            $temp33[2] .= $hash;
            $temp33[3] = 'api-ajax-request-multiple-result-split';            
            $temp33[3] .= $transaction_id;
            $temp33[4] = 'api-ajax-request-multiple-result-split';            
            $temp33[4] .= number_format($temp_result['grand_total'],2);
            $temp33[5] = 'api-ajax-request-multiple-result-split';            
            $temp33[5] .= $temp_payment_option;
            $temp33[6] = 'api-ajax-request-multiple-result-split';          
            $result = $temp33[1].$temp33[2].$temp33[3].$temp33[4].$temp33[5].$temp33[6];
            echo $result;              
        }
    }

    public function lucky_draw($id = null)
    {
        $config_data = array(
            'table_name' => 'sma_users',
            'select_table' => 'sma_users',
            'translate' => '',
            'select_condition' => "id  > 0 and add_ons like '%:lucky_draw_code:{".$id."}:%' order by id desc ",
        );
        if($id != '')
        $temp = $this->site->api_select_data_v2($config_data);
        $this->data['lucky_draw_code'] = $temp;
        $this->load->view($this->theme . 'pages/lucky_draw',  $this->data);   
    }

    /**
     * 
     */
    public function ajax_get_product_display() {
        foreach ($_GET as $name => $value) {
            ${$name} = $value;
        }        
        $config_data = array(
            'table_name' => 'sma_users',
            'select_table' => 'sma_users',
            'translate' => '',
            'select_condition' => "id > 0 and add_ons like '%:lucky_draw_code:{".$lucky_draw_code."}:%' order by id desc ",
        );
        $temp = $this->site->api_select_data_v2($config_data);
        $rand = rand(1,100);
        if ($rand <= 90) {
            // $rand = rand(1,100);
            if ($rand >= 1 && $rand <= 25) 
                $product_id = 7007;
            if ($rand >= 26 && $rand <= 50)
                $product_id = 7008;
            if ($rand >= 51 && $rand <= 75)
                $product_id = 7515;
            if ($rand >= 76 && $rand <= 100)
                $product_id = 8027;
            
            $config_data = array(
                'table_name' => 'sma_users',
                'id_name' => 'id',
                'field_add_ons_name' => 'add_ons',
                'selected_id' => $temp[0]['id'],
                'add_ons_title' => 'lucky_draw_status',
                'add_ons_value' => 'win',                    
            );
            $this->site->api_update_add_ons_field($config_data);
            $config_data_2 = array(
                'table_name' => 'sma_users',
                'id_name' => 'id',
                'field_add_ons_name' => 'add_ons',
                'selected_id' => $temp[0]['id'],
                'add_ons_title' => 'lucky_draw_win_product',
                'add_ons_value' => $product_id.'-0',              
            );
            $this->site->api_update_add_ons_field($config_data_2);
            $config_data_3 = array(
                'table_name' => 'sma_users',
                'id_name' => 'id',
                'field_add_ons_name' => 'add_ons',
                'selected_id' => $temp[0]['id'],
                'add_ons_title' => 'lucky_draw_win_code',
                'add_ons_value' =>  $temp[0]['lucky_draw_win_code'].','.$product_id.'-0',                      //we should keep old id of win code and add new to behind it            
            );
            $this->site->api_update_add_ons_field($config_data_3);
            $config = array(
                'table_name' => 'sma_products',
                'select_table' => 'sma_products',
                'translate' => 'yes',
                'select_condition' => "id = ".$product_id
            );
            $select_data = $this->site->api_select_data_v2($config);
        
                $data_view = array (
                    'wrapper_class' => '',
                    'file_path' => 'assets/uploads/'.$select_data[0]['image'],
                    'max_width' => 200,
                    'max_height' => 100,
                    'product_link' => 'true',
                    'image_class' => 'img-responsive',
                    'image_id' => $select_data[0]["id"],   
                    'resize_type' => 'full',
                );
                $temp2 = $this->site->api_get_image($data_view);
                // $tempdisplay_2 = '<span class="style_message_1">'.$select_data[0]["title_en"].'</span><br><br>'.$temp2["image_table"];
                   $temp_display_2 ='
                                        <div class="myDiv" style="color: white; font-size: 20px; font-weight: bold;">Congratulation you win a product</div>
                                        <div style="float: left;padding-top: 10px;">'.$temp2['image_table'].'</div>
                                        <div class="style_message_1">'.$select_data[0]["title_en"].'</div>
                                    ';
                $config_data = array(
                    'table_name' => 'sma_users',
                    'select_table' => 'sma_users',
                    'translate' => '',
                    'select_condition' => "id > 0 and add_ons like '%:lucky_draw_code:{".$lucky_draw_code."}:%' order by id desc ",
                );
                $temp = $this->site->api_select_data_v2($config_data);
                '<div class="api_height_10"></div>';
                $exp = explode("," , $temp[0]['lucky_draw_win_code']);
                $temp_display = '';
                $temp_display = '<div class="product_title">Win History</div>';
                $temp_display .= '<table class="all_product">';
                $temp_display .= '<tr>
                            <th>#</th>
                            <th>Image</th>
                            <th>Product Name</th>
                            <th>Status</th>
                        </tr>';
                  $counter = 1;      
                  for($k=count($exp)-1;$k>=0;$k--) {
                    $exp_2 = explode("-" , $exp[$k]); //take the value that have exploded before for use
                    if ($exp_2[0] != '') { //hide each pages not win and lost
                        $config_data = array(
                          'table_name' => 'sma_products',
                          'select_table' => 'sma_products',
                          'translate' => 'yes',
                          'select_condition' => "id = ".$exp_2[0]." order by id desc ",
                        );
                        $temp = $this->site->api_select_data_v2($config_data);
  
                        $data_view = array (
                          'wrapper_class' => '',
                          'file_path' => 'assets/uploads/'.$temp[0]['image'],
                          'max_width' => 200,
                          'max_height' => 100,
                          'product_link' => 'true',
                          'image_class' => 'img-responsive',
                          'image_id' => '',   
                          'resize_type' => 'full',
                        );
                        $temp2 = $this->site->api_get_image($data_view);
                        
  
                        if($exp_2[1] == 1){
                            $message = "Received";
                        } if($exp_2[1] == 0){
                            $message = "Not yet receive";
                        }                    
  
                        $temp_display .= '<tr>
                            <td>
                                '.$counter++.'
                            </td>
                            <td>
                                '.$temp2['image_table'].'
                            </td>
                            <td>
                                '.$temp[0]['title_en'].'
                            </td>
                            <td>
                                '.$message.'
                            </td>
                        </tr>';
                  }
                }                
                $temp_display .= '</table>';        
        }
        else {
            $config_data = array(
                'table_name' => 'sma_users',
                'id_name' => 'id',
                'field_add_ons_name' => 'add_ons',
                'selected_id' => $temp[0]['id'], //id user
                'add_ons_title' => 'lucky_draw_status',
                'add_ons_value' => 'lost',                    
            );
            $this->site->api_update_add_ons_field($config_data);
            $config_data_2 = array(
                'table_name' => 'sma_users',
                'id_name' => 'id',
                'field_add_ons_name' => 'add_ons',
                'selected_id' => $temp[0]['id'],
                'add_ons_title' => 'lucky_draw_win_product',
                'add_ons_value' => $product_id,                    
            );
            $this->site->api_update_add_ons_field($config_data_2);
            $temp_display = $this->shop_model->display_win_history($temp); 
            $temp_display_2 = '<img src="'.base_url().'themes/default/shop/assets/images/lost.jpg" style="width: 100%; padding-top: 6px;"/>';
        }
        $temp3[1] = 'api-ajax-request-multiple-result-split';  
        $temp3[1] .= $temp_display_2;  
        $temp3[3] = 'api-ajax-request-multiple-result-split';  
        $temp3[3] .= $temp_display;    
        $temp3[4] = 'api-ajax-request-multiple-result-split';      
        $result = $temp3[1].$temp3[3];
        echo $result; 
}
public function delete_address(){
    if ($this->loggedIn) {
        foreach ($_GET as $name => $value) {
            ${$name} = $value;
        }

        $this->db->delete('sma_addresses', "id = ".$address_id." and company_id = ".$this->session->userdata('company_id'));
        $value = $element_id;
        $temp3[1] = 'api-ajax-request-multiple-result-split';  
        $temp3[1] .= $value;
        $temp3[2] = 'api-ajax-request-multiple-result-split';            
        $result = $temp3[1].$temp3[2];
        echo $result;            
    }
    else
        echo '<script>window.location = "'.base_url().'login";</script>';    
}
public function ajax_getDeliveryAddress() {
    foreach ($_GET as $name => $value) {
        ${$name} = $value;
    }
    if($field_id) {
        $config_data = array(
            'table_name' => 'sma_companies',
            'select_table' => 'sma_companies',
            'translate' => '',
            'select_condition' => "id = ".$field_id,
        );
        $select_data = $this->site->api_select_data_v2($config_data);
        $temp = $this->api_helper->calculate_delivery_fee($select_data[0]['distance_from_restaurant']);
    } else {
        if($address_id) {
            $config_data = array(
                'table_name' => 'sma_addresses',
                'select_table' => 'sma_addresses',
                'translate' => '',
                'select_condition' => "id = ".$address_id,
            );
            $select_data2 = $this->site->api_select_data_v2($config_data);
            for($i=0;$i<count($select_data2);$i++) {
                $temp = $this->api_helper->calculate_delivery_fee($select_data2[$i]['distance_from_restaurant']);
            }
        } 
    }

    $temp3[1] = 'api-ajax-request-multiple-result-split';  
    $temp3[1] .= $temp;
    $temp3[2] = 'api-ajax-request-multiple-result-split';   
    $temp3[2] .= $temp2;  
    $temp3[3] = 'api-ajax-request-multiple-result-split';   
    $temp3[3] .= $ele;       
    $result = $temp3[1].$temp3[2].$temp3[3];
    echo $result; 
}

// Add/edit customer address
public function address_v2($id = null)
{
    // if (!$this->loggedIn) {
    //     $this->sma->send_json(array('status' => 'error', 'message' => lang('please_login')));
    // }
    // if ($id) {
        $add_ons = 'initial_first_add_ons:{}:';
            foreach ($_GET as $name => $value) {
                $value = $this->input->get($name);
                if (is_int(strpos($name, "add_ons_"))) {
                    $temp_name = str_replace('add_ons_', '', $name);
                    $add_ons .= $temp_name.':{'.$value.'}:';
                }
            }
        $data = [
            'phone' => $this->input->get('phone'),
            'city_id' => $this->input->get('city_id'),
            'postal_code' => $this->input->get('postal_code'),
            'company_id' => $this->session->userdata('company_id'),
            'add_ons' => $add_ons
        ];
        // $this->sma->print_arrays($data);
        $this->db->update('addresses', $data, ['id' => $id]);
        $this->session->set_flashdata('message', lang('address_update'));
    // }
    
    shop_redirect('addresses');
}

public function add_delivery_address() {
    if (!$this->loggedIn) {
        $this->sma->send_json(array('status' => 'error', 'message' => lang('please_login')));
    }
    $add_ons = 'initial_first_add_ons:{}:';
        foreach ($_GET as $name => $value) {
            $value = $this->input->get($name);
            if (is_int(strpos($name, "add_ons_"))) {
                $temp_name = str_replace('add_ons_', '', $name);
                $add_ons .= $temp_name.':{'.$value.'}:';
            }
        }
    $data = [
        'phone' => $this->input->get('phone'),
        'city_id' => $this->input->get('city_id'),
        'postal_code' => $this->input->get('postal_code'),
        'company_id' => $this->session->userdata('company_id'),
        'add_ons' => $add_ons
    ];
    
    $this->db->insert('addresses', $data);
    $this->session->set_flashdata('message', lang('address_added'));
    redirect($_SERVER["HTTP_REFERER"]);
    // shop_redirect('addresses');
    //redirect('cart/checkout'.($guest_checkout ? '#guest' : '')); 
}
public function addresses_v2() {
        foreach ($_GET as $name => $value) {
            ${$name} = $value;
        }
        $config_data = array(
            'table_name' => 'sma_addresses',
            'select_table' => 'sma_addresses',
            'translate' => '',
            'select_condition' => "id = ".$address_id,
        );
        $select_data = $this->site->api_select_data_v2($config_data); 
        // $this->sma->print_arrays($select_data);
        for($i=0;$i<count($select_data);$i++) {
            $temp2 = $select_data[$i]['first_name'];
            $temp3 = $select_data[$i]['last_name'];
            $temp4 = $select_data[$i]['phone'];
            $temp5 = $select_data[$i]['email'];
            $temp6 = $select_data[$i]['delivery_address'];
            $temp7 = $select_data[$i]['city_id'];
            $temp8 = $select_data[$i]['postal_code'];
            $temp11 = $select_data[$i]['map_coordinate'];

        }
    
        $temp[1] = 'api-ajax-request-multiple-result-split';  
        $temp[1] .= $temp2;
        $temp[2] = 'api-ajax-request-multiple-result-split'; 
        $temp[2] .= $temp3;
        $temp[3] = 'api-ajax-request-multiple-result-split';
        $temp[3] .= $temp4;
        $temp[4] = 'api-ajax-request-multiple-result-split'; 
        $temp[4] .= $temp5;
        $temp[5] = 'api-ajax-request-multiple-result-split';
        $temp[5] .= $temp6;
        $temp[6] = 'api-ajax-request-multiple-result-split';
        $temp[6] .= $temp7;
        $temp[7] = 'api-ajax-request-multiple-result-split';  
        $temp[7] .= $temp8;
        $temp[8] = 'api-ajax-request-multiple-result-split'; 
        $temp[8] .= $temp9;
        $temp[9] = 'api-ajax-request-multiple-result-split'; 
        $temp[9] .= $temp10;
        $temp[10] = 'api-ajax-request-multiple-result-split'; 
        $temp[10] .= $temp11;
        $temp[11] = 'api-ajax-request-multiple-result-split';          
        $result = $temp[1].$temp[2].$temp[3].$temp[4].$temp[5].$temp[6].$temp[7].$temp[8].$temp[9].$temp[10].$temp[11];
        echo $result;            
}

public function addresses_v3() {
    if (!$this->loggedIn) {
        redirect('login');
    }
    $this->page_construct('pages/add_addresses', $this->data);
}
public function addresses_v4($id = null) {
    if (!$this->loggedIn) {
        redirect('login');
    }
    $this->data['address_id'] = $id;
    $this->page_construct('pages/edit_addresses', $this->data);
}
public function add_addresses($id = null) {    
    if ($this->shop_settings->private) {
        redirect('/login');
    }
    $add_ons = 'initial_first_add_ons:{}:';
    foreach ($_POST as $name => $value) {
        $value = $this->input->post($name);
        if (is_int(strpos($name, "add_ons_"))) {
            $temp_name = str_replace('add_ons_', '', $name);
            $add_ons .= $temp_name.':{'.$value.'}:';
        }
    }
    $data = [
        'phone' => $this->input->post('phone'),
        'city_id' => $this->input->post('city_id'),
        'postal_code' => $this->input->post('postal_code'),
        'company_id' => $this->session->userdata('company_id'),
        'add_ons' => $add_ons
    ];
    $this->db->insert('addresses', $data); 
    $id = $this->db->insert_id();
    shop_redirect('Edit-Address/'.$id.'?update_address=1');
    $this->session->set_flashdata('message', lang('address_added'));
}


public function edit_addresses($id = null) {
    if (!$this->loggedIn) {
        $this->sma->send_json(array('status' => 'error', 'message' => lang('please_login')));
    }
    $add_ons = 'initial_first_add_ons:{}:';
            foreach ($_POST as $name => $value) {
                $value = $this->input->post($name);
                if (is_int(strpos($name, "add_ons_"))) {
                    $temp_name = str_replace('add_ons_', '', $name);
                    $add_ons .= $temp_name.':{'.$value.'}:';
                }
            }
    $data = [
        'phone' => $this->input->post('phone'),
        'city_id' => $this->input->post('city_id'),
        'postal_code' => $this->input->post('postal_code'),
        'company_id' => $this->session->userdata('company_id'),
        'add_ons' => $add_ons
    ];
    if ($id) {
        $this->db->update('addresses', $data, ['id' => $id]);
        shop_redirect('Edit-Address/'.$id.'?update_address=1');
        $this->session->set_flashdata('message', lang('address_updated'));
    }
    shop_redirect('addresses'); 
}

public function ajax_get_update_address() {
    foreach ($_GET as $name => $value) {
        ${$name} = $value; 
    }
    
    $config_data = array(
        'table_name' => 'sma_addresses',
        'id_name' => 'id',
        'field_add_ons_name' => 'add_ons',
        'selected_id' => $field_id,
        'add_ons_title' => 'distance_from_restaurant',
        'add_ons_value' => $distance_from_restaurant,                    
    );
    $this->site->api_update_add_ons_field($config_data); 
    $config_data2 = array(
        'table_name' => 'sma_addresses',
        'id_name' => 'id',
        'field_add_ons_name' => 'add_ons',
        'selected_id' => $field_id,
        'add_ons_title' => 'delivery_fee',
        'add_ons_value' => $delivery_fee,                    
    );
    $this->site->api_update_add_ons_field($config_data2);  
    
    $temp3[1] = 'api-ajax-request-multiple-result-split';  
    $temp3[1] .= $distance_from_restaurant;
    $temp3[2] = 'api-ajax-request-multiple-result-split';  
    $temp3[2] .= $delivery_fee;
    $temp3[3] = 'api-ajax-request-multiple-result-split';            
    $result = $temp3[1].$temp3[2].$temp3[3];
    echo $result;  
}
public function ajax_get_address() {
    foreach ($_GET as $name => $value) {
        ${$name} = $value; 
    }
    $temp3[1] = 'api-ajax-request-multiple-result-split';  
    $temp3[1] .= $first_name;
    $temp3[2] = 'api-ajax-request-multiple-result-split';  
    
    $result = $temp3[1].$temp3[2];
    echo $result;
}

public function api_ajax_update_address() {

    if ($this->loggedIn) {
        foreach ($_GET as $name => $value) {
            ${$name} = $value; 
        }

        if ($field_id > 0) {
            $temp = $this->site->api_select_some_fields_with_where("
                *     
                "
                ,"sma_addresses"
                ,"id = ".$field_id." and company_id = ".$this->session->userdata('company_id')
                ,"arr"
            );
            if (count($temp) > 0) {
                $temp_data = array();
                foreach($_GET as $name => $value) {
                    if (is_int(strpos($name,"add_ons_"))) {
                        $temp_name = str_replace('add_ons_','',$name);
                        $config_data = array(
                            'table_name' => 'sma_addresses',
                            'id_name' => 'id',
                            'field_add_ons_name' => 'add_ons',
                            'selected_id' => $field_id,
                            'add_ons_title' => $temp_name,
                            'add_ons_value' => $value,                    
                        );
                        $this->site->api_update_add_ons_field($config_data);                        
                    }
                    if (is_int(strpos($name,"api_field_"))) {
                        $temp_name = str_replace('api_field_','',$name);
                        $temp_data[$temp_name] = $value;
                    }
                }

                $this->db->update('sma_addresses', $temp_data,"id = ".$field_id);


                
                $temp3[1] = 'api-ajax-request-multiple-result-split';  
                $temp3[1] .= $add_ons_delivery_address.$temp_city_display['display'].' '.$api_field_postal_code;
                $temp3[2] = 'api-ajax-request-multiple-result-split';  
                
                $result = $temp3[1].$temp3[2];
                echo $result;
            }
        }
        else {
            $temp_data = array();

            $config_data_2 = array(
                'add_ons_table_name' => 'sma_add_ons',
                'table_name' => 'sma_addresses',
                'post' => $_GET,
            );
            $temp = $this->api_helper->api_add_ons_table_insert($config_data_2);  
            $add_ons = $temp['add_ons'];

            $temp_data = array();
            foreach($_GET as $name => $value) {
                if (is_int(strpos($name,"api_field_"))) {
                    $temp_name = str_replace('api_field_','',$name);
                    $temp_data[$temp_name] = $value;
                }
            }
            $temp_data['add_ons'] = $add_ons;
            $temp_data['company_id'] = $this->session->userdata('company_id');
            $this->db->insert('sma_addresses', $temp_data);

            $temp3[1] = 'api-ajax-request-multiple-result-split';  
            $temp3[1] .= $add_ons;
            $temp3[2] = 'api-ajax-request-multiple-result-split';  
            
            $result = $temp3[1].$temp3[2];
            echo $result;
       
        }
    }
    else {
        echo '<script>window.location = "'.base_url().'/login";</script>';    
    }
}

}