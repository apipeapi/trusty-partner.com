<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Cart_ajax extends MY_Shop_Controller
{

    function __construct() {
        parent::__construct();
        if ($this->Settings->mmode) { redirect('notify/offline'); }
        if ($this->shop_settings->hide_price) { redirect('/'); }
        if ($this->shop_settings->private && !$this->loggedIn) { redirect('/login'); }
        $this->load->library('components/api_menu_mobile','api_menu_mobile');        
    }

    function index() {
        
        $this->session->set_userdata('requested_page', $this->uri->uri_string());
        if ($this->cart->total_items() < 1) {
            $this->session->set_flashdata('reminder', lang('cart_is_empty'));
            shop_redirect('products');
        }
        if ($this->loggedIn)
            $this->data['customer'] = $this->site->getCompanyByID_v2($this->session->userdata('company_id'));

        if ($cart_id = get_cookie('cart_id', TRUE)) {
            $this->cart_id = $cart_id;
            $result = $this->db->get_where('cart', array('id' => $this->cart_id))->row();
            $this->_cart_contents = $result ? json_decode($result->data, true) : NULL;

            $i = 0;
            foreach(array_keys($this->_cart_contents) as $key){
                if ($key != 'cart_total' && $key != 'total_item_tax' && $key != 'total_items' && $key != 'total_items') {
                    $select_data[$i] = $this->_cart_contents[$key];
                    if ($select_data[$i]['product_id'] != '') {
                        $config_data = array(
                            'table_name' => 'sma_products',
                            'select_table' => 'sma_products',
                            'translate' => '',
                            'select_condition' => "id = ".$select_data[$i]['product_id'],
                        );
                        $temp = $this->site->api_select_data_v2($config_data);

                        $select_data[$i]['quantity'] = $temp[0]['quantity'];

                    }
                    $i++;
                }
                else {
                    $select_data[0][$key] = $this->_cart_contents[$key];                    
                }                    
            }
        }
        $this->data['select_data'] = $select_data;            
        $this->data['page_title'] = lang('shopping_cart');
        $this->page_construct('pages/cart', $this->data);
    }

    function checkout() {
        
        $this->session->set_userdata('requested_page', $this->uri->uri_string());
        if ($this->cart->total_items() < 1) {
            $this->session->set_flashdata('reminder', lang('cart_is_empty'));
            shop_redirect('products');
        }

        if ($this->session->userdata('user_id') > 0) {
            $temp = $this->site->api_select_some_fields_with_where("
                activation_code, active
                "
                ,"sma_users"
                ,"id = ".$this->session->userdata('user_id')
                ,"arr"
            );        
        }
        if (!$this->loggedIn)
            redirect("login");
        elseif ($temp[0]['active'] != 1) {
            $this->session->set_flashdata('error', lang("Please_verify_your_email_address"));
            redirect("login");
        }

        if ($this->loggedIn) {
            $this->data['customer'] = $this->site->getCompanyByID_v2($this->session->userdata('company_id'));            
            if ($this->data['customer']->parent_id <= 0) {
                $select_company = array();
                $select_company[0]['id'] = $this->data['customer']->id;
                $select_company[0]['company'] = $this->data['customer']->company;
                $select_company[0]['address'] = $this->data['customer']->address;
                $select_company[0]['name'] = $this->data['customer']->name;

                if ($this->data['customer']->id > 0) {
                    $temp = $this->site->api_select_some_fields_with_where("
                        *     
                        "
                        ,"sma_companies"
                        ,"parent_id = ".$this->data['customer']->id." order by company asc"
                        ,"arr"
                    );
                    $j=1;
                    for ($i=0;$i<count($temp);$i++) {
                        $select_company[$j] = $temp[$i];
                        $j++;
                    }                
                }
            }
            else {
                $select_company = array();
                $temp = $this->site->api_select_some_fields_with_where("
                    *     
                    "
                    ,"sma_companies"
                    ,"id = ".$this->data['customer']->parent_id
                    ,"arr"
                );
                if (count($temp) > 0) {
                    $select_company[0]['id'] = $temp[0]['id'];
                    $select_company[0]['company'] = $temp[0]['company'];
                    $select_company[0]['address'] = $temp[0]['address'];                    
                    $select_company[1]['id'] = $this->data['customer']->id;
                    $select_company[1]['company'] = $this->data['customer']->company;
                    $select_company[1]['address'] = $this->data['customer']->address;
                    $select_company[1]['name'] = $this->data['customer']->name;
                }
                else {
                    $select_company[0]['id'] = $this->data['customer']->id;
                    $select_company[0]['company'] = $this->data['customer']->company;
                    $select_company[0]['address'] = $this->data['customer']->address;     
                    $select_company[0]['name'] = $this->data['customer']->name;               
                }
            }
            $this->data['select_company'] = $select_company;


            $config_data = array(
                'table_name' => 'sma_users',
                'select_table' => 'sma_users',
                'select_condition' => "id = ".$this->session->userdata('user_id'),
            );
            $this->data['user'] = $this->site->api_select_data_v2($config_data);
            
        }

        $this->data['settings'] = $this->shop_model->getSettings_v2(1);
        $this->data['paypal'] = $this->shop_model->getPaypalSettings();
        $this->data['skrill'] = $this->shop_model->getSkrillSettings();
        $this->data['addresses'] = $this->loggedIn ? $this->shop_model->getAddresses() : FALSE;
        $config_data = array(
            'table_name' => 'sma_addresses',
            'select_table' => 'sma_addresses',
            'translate' => '',
            'description' => '',
            'select_condition' => "company_id = ".$this->session->userdata('company_id')  ." order by id asc",
        );
        $this->data['addresses_v2'] = $this->api_helper->api_select_data_v2($config_data);        
        $this->data['page_title'] = lang('checkout');
        $this->page_construct('pages/checkout', $this->data);
    }

    function add($product_id) {
        if ($this->input->is_ajax_request() || $this->input->post('quantity')) {
            $product = $this->shop_model->getProductForCart($product_id);

            $config_data = array(
                'id' => $product_id,
                'customer_id' => $this->session->userdata('company_id'),
            );
            $temp = $this->site->api_calculate_product_price($config_data);               
            if ($temp['promotion']['price'] > 0)
                $price = $temp['price'];

            $option = FALSE;
            if (!empty($options)) {
                if ($this->input->post('option')) {
                    foreach ($options as $op) {
                        if ($op['id'] == $this->input->post('option')) {
                            $option = $op;
                        }
                    }
                } else {
                    $option = array_values($options)[0];
                }
                $price = $option['price']+$price;
            }
            $selected = $option ? $option['id'] : FALSE;

            $temp_2 = 1;
            if ($this->input->get('qty'))
                $temp_2 = $this->input->get('qty');
            elseif ($this->input->post('quantity'))
                $temp_2 = $this->input->post('quantity');

            if ($cart_id = get_cookie('cart_id', TRUE)) {
                $this->cart_id = $cart_id;
                $result = $this->db->get_where('cart', array('id' => $this->cart_id))->row();
                $this->_cart_contents = $result ? json_decode($result->data, true) : NULL;
            }
            foreach(array_keys($this->_cart_contents) as $key){
                if ($key != 'cart_total' && $key != 'total_item_tax' && $key != 'total_items' && $key != 'total_items') {
                    if ($this->_cart_contents[$key]['product_id'] == $product_id) {
                        $temp_4 = $this->_cart_contents[$key]['qty'];
                        break;
                    }
                }
            }   

            $config_data = array(
                'product_id' => $product_id,
                'warehouse_id' => $this->shop_settings->warehouse,
                'compare_quantity' => ($temp_2 + $temp_4),
            );
            $temp = $this->site->api_check_product_stock($config_data);
            if ($this->api_shop_setting[0]['out_of_stock'] == 1) {
                if ($temp['result'] <= 0) {
                    if ($this->input->is_ajax_request())
                        $this->sma->send_json(['error' => 1, 'message' => lang('item_stock_is_less_then_order_qty')]);
                    else {
                        $this->session->set_flashdata('error', lang('item_stock_is_less_then_order_qty'));
                        redirect($_SERVER['HTTP_REFERER']);
                    }
                }
            }

            $tax_rate = $this->site->getTaxRateByID($product->tax_rate);
            $ctax = $this->site->calculateTax($product, $tax_rate, $price);
            $tax = $this->sma->formatDecimal($ctax['amount']);
            $price = $this->sma->formatDecimal($price);
            $unit_price = $this->sma->formatDecimal($product->tax_method ? $price+$tax : $price);            
            $id = $this->Settings->item_addition ? md5($product->id) : md5(microtime());

            $data = array(
                'id'            => $id,
                'product_id'    => $product->id,
                'qty'           => $temp_2,
                'name'          => $product->name,
                'slug'          => $product->slug,
                'code'          => $product->code,
                'price'         => $unit_price,
                'tax'           => $tax,
                'image'         => $product->image,
                'option'        => $selected,
                'options'       => !empty($options) ? $options : NULL
            );

            $config_data = array(
                'id' => $product->id,
            );
            $temp_hide = $this->shop_model->api_get_product_display($config_data);

            if ($temp_hide['result'] != 'hide') {
                if ($this->cart->insert($data)) {
                    if ($this->input->post('quantity')) {
                        $this->session->set_flashdata('message', lang('item_added_to_cart'));
                        redirect($_SERVER['HTTP_REFERER']);
                    } else {
                        $this->cart->cart_data();
                    }
                }
            }
            else {
                $this->session->set_flashdata('error', lang('Product_is_not_found'));
                redirect($_SERVER['HTTP_REFERER']);
            }
            $this->session->set_flashdata('error', lang('unable_to_add_item_to_cart'));
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function update($data = NULL) {
        if (is_array($data)) {
            return $this->cart->update($data);
        }
        if ($this->input->is_ajax_request()) {
            if ($rowid = $this->input->post('rowid', TRUE)) {
                $item = $this->cart->get_item($rowid);
                $product = $this->shop_model->getProductForCart($item['product_id']);

                $config_data = array(
                    'id' => $product_id,
                    'customer_id' => $this->session->userdata('company_id'),
                );
                $temp = $this->site->api_calculate_product_price($config_data);               
                if ($temp['promotion']['price'] > 0)
                    $price = $temp['price'];
                    
                if ($option = $this->input->post('option')) {
                    foreach($options as $op) {
                        if ($op['id'] == $option) {
                            $price = $price + $op['price'];
                        }
                    }
                }
                $selected = $this->input->post('option') ? $this->input->post('option', TRUE) : FALSE;

                $temp_2 = 1;
                if ($this->input->post('qty') >= 0)
                    $temp_2 = $this->input->post('qty');
                elseif ($this->input->post('quantity') >= 0)
                    $temp_2 = $this->input->post('quantity');

                $config_data = array(
                    'product_id' => $item['product_id'],
                    'warehouse_id' => $this->shop_settings->warehouse,
                    'compare_quantity' => $temp_2,
                );
                $temp = $this->site->api_check_product_stock($config_data);
                if ($this->api_shop_setting[0]['out_of_stock'] == 1) {
                    if ($temp['result'] <= 0) {
                        if ($this->input->is_ajax_request()) {
                            $this->sma->send_json(['error' => 1, 'message' => lang('item_stock_is_less_then_order_qty')]);
                        } else {
                            $this->session->set_flashdata('error', lang('item_stock_is_less_then_order_qty'));
                            redirect($_SERVER['HTTP_REFERER']);
                        }
                    }
                }

                $tax_rate = $this->site->getTaxRateByID($product->tax_rate);
                $ctax = $this->site->calculateTax($product, $tax_rate, $price);
                $tax = $this->sma->formatDecimal($ctax['amount']);
                $price = $this->sma->formatDecimal($price);
                $unit_price = $this->sma->formatDecimal($product->tax_method ? $price+$tax : $price);

                $data = array(
                    'rowid' => $rowid,
                    'price' => $price,
                    'tax' => $tax,
                    'qty' => $this->input->post('qty', TRUE),
                    'option' => $selected,
                );
                if ($this->cart->update($data)) {
                    $this->sma->send_json(array('cart' => $this->cart->cart_data(true), 'status' => lang('success'), 'message' => lang('cart_updated')));
                }
            }
        }
    }

    function remove($rowid = NULL) {
        if ($rowid) {
            return $this->cart->remove($rowid);
        }
        if ($this->input->is_ajax_request()) {
            if ($rowid = $this->input->post('rowid', TRUE)) {
                if ($this->cart->remove($rowid)) {
                    $this->sma->send_json(array('cart' => $this->cart->cart_data(true), 'status' => lang('success'), 'message' => lang('cart_item_deleted')));
                }
            }
        }
    }

    function destroy() {
        if ($this->input->is_ajax_request()) {
            if ($this->cart->destroy()) {
                $this->session->set_flashdata('message', lang('cart_items_deleted'));
                $this->sma->send_json(array('redirect' => base_url()));
            } else {
                $this->sma->send_json(array('status' => lang('error'), 'message' => lang('error_occured')));
            }
        }
    }

    function add_wishlist($product_id) {
        $this->session->set_userdata('requested_page', $_SERVER['HTTP_REFERER']);
        if (!$this->loggedIn) { $this->sma->send_json(array('redirect' => site_url('login'))); }
/*
        if ($this->shop_model->getWishlist(TRUE) >= 10) {
            $this->sma->send_json(array('status' => lang('warning'), 'message' => lang('max_wishlist'), 'level' => 'warning'));
        }
*/
        if ($this->shop_model->addWishlist($product_id)) {
            $total = $this->shop_model->getWishlist(TRUE);
            $this->sma->send_json(array('status' => lang('success'), 'message' => lang('added_wishlist'), 'total' => $total));
        } else {
            if ($this->shop_model->removeWishlist($product_id)) {
                $total = $this->shop_model->getWishlist(TRUE);
                $this->sma->send_json(array('status' => lang('success'), 'message' => lang('removed_wishlist'), 'total' => $total));
            } else {
                $this->sma->send_json(array('status' => lang('error'), 'message' => lang('error_occured'), 'level' => 'error'));
            }
            //$this->sma->send_json(array('status' => lang('info'), 'message' => lang('product_exists_in_wishlist'), 'level' => 'info'));
        }
    }

    function remove_wishlist($product_id) {
        $this->session->set_userdata('requested_page', $_SERVER['HTTP_REFERER']);
        if (!$this->loggedIn) { $this->sma->send_json(array('redirect' => site_url('login'))); }
        if ($this->shop_model->removeWishlist($product_id)) {
            $total = $this->shop_model->getWishlist(TRUE);
            $this->sma->send_json(array('status' => lang('success'), 'message' => lang('removed_wishlist'), 'total' => $total));
        } else {
            $this->sma->send_json(array('status' => lang('error'), 'message' => lang('error_occured'), 'level' => 'error'));
        }
    }

    private function checkProductStock($product, $qty, $option_id = null) {
        if ($product->type == 'service' || $product->type == 'digital') {
            return false;
        }
        $chcek = [];
        $temp = $this->site->api_select_some_fields_with_where("
            quantity     
            "
            ,"sma_products"
            ,"id = ".$product->id
            ,"arr"
        );
        $chcek[] =  ($qty <= $temp[0]['quantity']);            
        /*
        if ($product->type == 'standard') {
            $quantity = 0;
            if ($pis = $this->site->getPurchasedItems($product->id, $this->shop_settings->warehouse, $option_id)) {
                foreach ($pis as $pi) {
                    $quantity += $pi->quantity_balance;
                }
            }
            $chcek[] =  ($qty <= $quantity);
        } elseif ($product->type == 'combo') {
            $combo_items = $this->site->getProductComboItems($product->id, $this->shop_settings->warehouse);
            foreach ($combo_items as $combo_item) {
                if ($combo_item->type == 'standard') {
                    $quantity = 0;
                    if ($pis = $this->site->getPurchasedItems($combo_item->id, $this->shop_settings->warehouse, $option_id)) {
                        foreach ($pis as $pi) {
                            $quantity += $pi->quantity_balance;
                        }
                    }
                    $chcek[] = (($combo_item->qty*$qty) <= $quantity);
                }
            }
        }
        */
        return empty($chcek) || in_array(false, $chcek);
    }

    function api_remove_cart() {
        $temp = explode('-',$_GET['api_id_list']);
        $temp2 = 0;
        $temp_remove_id = '';
        for ($i=0;$i<count($temp);$i++) {
            if ($temp[$i] != '') {
                $this->cart->remove($temp[$i]);
                $temp2++;
                $temp_remove_id .= $temp[$i].'-';
            }
        }
        $temp3[1] = 'api-ajax-request-multiple-result-split';
        $temp3[1] .= 'Successfully removed '.$temp2.' cart item';
        if ($temp2 > 1) $temp33 = 's';  
        $temp3[1] .= $temp33;
        $temp3[2] = 'api-ajax-request-multiple-result-split';
        $temp3[2] .= $temp_remove_id;
        $result = $temp3[1].$temp3[2];  
        echo $result;
    }


function api_update_cart() {
    if ($cart_id = get_cookie('cart_id', TRUE)) {
        $this->cart_id = $cart_id;
        $result = $this->db->get_where('cart', array('id' => $this->cart_id))->row();
        $this->_cart_contents = $result ? json_decode($result->data, true) : NULL;
    }
    
    if ($_GET['qty'] <= 0) $_GET['qty'] = 1;

    
    $config_data = array(
        'product_id' => $this->_cart_contents[$_GET['rowid']]['product_id'],
        'warehouse_id' => $this->shop_settings->warehouse,
        'compare_quantity' => $_GET['qty'],
    );
    $temp = $this->site->api_check_product_stock($config_data);
    $error = '';        
    if ($this->api_shop_setting[0]['out_of_stock'] == 1) {
        if ($temp['result'] <= 0) {
            //$error = lang('item_stock_is_less_then_order_qty');
            $error = 'out_of_stock';
        }
    }

    $config_data_2 = array(
        'id' => $this->_cart_contents[$_GET['rowid']]['product_id'],
        'customer_id' => $this->session->userdata('company_id'),
    );
    $temp5 = $this->site->api_calculate_product_price($config_data_2); 
    if ($temp5['promotion']['price'] > 0) {
        $temp_qty = $temp5['promotion']['min_qty'];
        if ($_GET['qty'] < $temp_qty) {
            $error = 'minimum_quantity';
            $_GET['qty'] = $temp_qty;
        }
    }
        
    
    $temp4 = '';
    if ($error == '') {
        $this->_cart_contents[$_GET['rowid']]['qty'] = $_GET['qty'];
        $this->_cart_contents[$_GET['rowid']]['subtotal'] = round($_GET['qty'] * $this->_cart_contents[$_GET['rowid']]['price'],2, PHP_ROUND_HALF_UP);

        $temp_total_items = 0;
        $temp_cart_total = 0;
        foreach(array_keys($this->_cart_contents) as $key){
            if ($key != 'cart_total' && $key != 'total_item_tax' && $key != 'total_items' && $key != 'total_items') {
                $temp_total_items += $this->_cart_contents[$key]['qty'];
                $temp_cart_total += $this->_cart_contents[$key]['subtotal'];
            }
            else {
                $select_data[0][$key] = $this->_cart_contents[$key];                    
            }                    
        }                
        $this->_cart_contents['total_items'] = $temp_total_items;
        $this->_cart_contents['cart_total'] = $temp_cart_total;
        
        if ($this->db->get_where('cart', array('id' => $this->cart_id))->num_rows() > 0) {
            $this->db->update('cart', array('time' => time(), 'user_id' => $this->session->userdata('user_id'), 'data' => json_encode($this->_cart_contents)), array('id' => $this->cart_id));
        }
        $temp4 = 'success';
    }

    $temp3[1] = 'api-ajax-request-multiple-result-split';
    $temp3[1] .= $temp4;
    $temp3[2] = 'api-ajax-request-multiple-result-split';
    $temp3[2] .= $error;        
    $temp3[3] = 'api-ajax-request-multiple-result-split';
    $temp3[3] .= $temp_qty;        
    $temp3[4] = 'api-ajax-request-multiple-result-split';            
    $result = $temp3[1].$temp3[2].$temp3[3].$temp3[4];  
    echo $result;
}

}
