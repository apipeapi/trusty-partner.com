<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        $this->lang->admin_load('products', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('products_model');
        $this->upload_path = 'assets/uploads/';
        $this->thumbs_path = 'assets/uploads/thumbs/';
        $this->image_types = 'gif|jpg|jpeg|png|tif';
        $this->digital_file_types = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif|txt';
        $this->allowed_file_size = '2048';
        $this->popup_attributes = array('width' => '900', 'height' => '600', 'window_name' => 'sma_popup', 'menubar' => 'yes', 'scrollbars' => 'yes', 'status' => 'no', 'resizable' => 'yes', 'screenx' => '0', 'screeny' => '0');
    }

    function index()
    {
        $this->sma->checkPermissions();
        $temp_warhouse = $this->site->api_select_some_fields_with_where("
            *     
            "
            ,"sma_warehouses"
            ,"id > 0 order by id asc"
            ,"arr"
        );
        $this->data['api_warehouse'] = $temp_warhouse;

        $sort_by = $this->input->get('sort_by');
        $sort_order = $this->input->get('sort_order');

        $per_page = $this->input->get('per_page') != '' ? $this->input->get('per_page') : 100;
        $page = $this->input->get('page') != '' ? $this->input->get('page') : 1;

        $offset_no = $page != '' ? ($page - 1 ) * $per_page : $offset_no;
        
        if ($page != '')
            $offset_no = ($page - 1 ) * $per_page;
        else
            $offset_no = 0;

        $search = $this->input->get('search');
        $search = $this->db->escape_str($search);

        if ($page != '') $temp_url .= '?page=';
        if ($search != '') $temp_url .= '&search='.$search;
        if ($this->input->get('featured') != '') $temp_url .= '&featured='.$this->input->get('featured');
        if ($this->input->get('promotion') != '') $temp_url .= '&promotion='.$this->input->get('promotion');
        if ($this->input->get('c_id') != '') $temp_url .= '&c_id='.$this->input->get('c_id');
        if ($sort_by != '') $temp_url .= '&sort_by='.$sort_by;
        if ($sort_order != '') $temp_url .= '&sort_order='.$sort_order;
        if ($per_page != '') $temp_url .= '&per_page='.$per_page;

        $config_data = array(
            'lg' => 'en',
            'table_name' => 'sma_products',
            'table_id' => 'id',
            'search' => $search,
            'sort_by' => $sort_by,
            'sort_order' => $sort_order,
            'limit' => $per_page,
            'offset_no' => $offset_no,
            'warehouse' => $temp_warhouse,
        );
        $select_data = $this->products_model->api_get_product($config_data);

        $config_data = array(
            'lg' => 'en',
            'table_name' => 'sma_products',
            'table_id' => 'id',
            'search' => $search,
            'sort_by' => '',
            'sort_order' => '',
            'limit' => '',
            'offset_no' => '',
            'warehouse' => '',
        );
        $temp_count = $this->products_model->api_get_product($config_data);
        $total_record = count($temp_count);

        /* config pagination */
        $this->data['select_data'] = $select_data;
        $this->data['page'] = $page;
        $this->data['search'] = $search;
        $this->data['sort_by'] = $sort_by;
        $this->data['sort_order'] = $sort_order;
        $this->data['per_page'] = $per_page;
        $this->data['total_rows_sale'] = $total_record;
        $this->data['index_page'] = 'admin/products';
        $this->data['show_data'] = $page;

        $config_base_url = site_url() . 'admin/products'; //your url where the content is displayed
        $config['base_url'] = $config_base_url.$temp_url; //your url where the content is displayed
        $config['query_string_segment'] = 'page';
        $config['per_page'] = $per_page; //$per_page;
        $config['num_links'] = 5;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['full_tag_open'] = '<ul class="pagination pagination-new-bg">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = FALSE;
        $config['last_link'] = FALSE;
        $config['prev_link'] = '&lt; Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next &gt;';
        $config['next_tag_open'] = '<li class="next">';
        $config['next_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['total_rows'] = $total_record; //$this->data['total_rows_sale'];        
        $this->load->library('pagination');   
        $this->pagination->initialize($config);
        /** end config pagination */
        /**Showing Label Data At Table Footer */
        $showing = $total_record != '' ? 1 : 0;
        $label_show_to= ($page == '' || $per_page < $total_record) ?  $per_page  :  $total_record;
        $label_show = "Showing " . $showing . " to ".$label_show_to." of : " . number_format($total_record) . " entries";

        $show_page = $page > 1 ? ($page * $per_page) : $total_record;
        $show_page_no = $show_page > $total_record ? $total_record : $page * $per_page;
        $label_show_page = "Showing " . (($page - 1) * ($per_page) + 1) . " to " . $show_page_no . " (filtered from " . number_format($total_record) . " total entries) ";
        $this->data['show'] = $search != '' || $page > 1 ? $label_show_page : $label_show;

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['supplier'] = $this->input->get('supplier') ? $this->site->getCompanyByID($this->input->get('supplier')) : NULL;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('products')));
        $meta = array('page_title' => lang('products'), 'bc' => $bc);
        $this->page_construct('products/index', $meta, $this->data);
    }

    function getProducts($warehouse_id = NULL)
    {
        $this->sma->checkPermissions('index', TRUE);
        $supplier = $this->input->get('supplier') ? $this->input->get('supplier') : NULL;

        if ((! $this->Owner || ! $this->Admin) && ! $warehouse_id) {
            $user = $this->site->getUser();
            $warehouse_id = $user->warehouse_id;
        }
        $detail_link = anchor('admin/products/view/$1', '<i class="fa fa-file-text-o"></i> ' . lang('product_details'));
        $delete_link = "<a href='#' class='tip po' title='<b>" . $this->lang->line("delete_product") . "</b>' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete1' id='a__$1' href='" . admin_url('products/delete/$1') . "'>"
            . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i> "
            . lang('delete_product') . "</a>";
        $single_barcode = anchor('admin/products/print_barcodes/$1', '<i class="fa fa-print"></i> ' . lang('print_barcode_label'));
        // $single_label = anchor_popup('products/single_label/$1/' . ($warehouse_id ? $warehouse_id : ''), '<i class="fa fa-print"></i> ' . lang('print_label'), $this->popup_attributes);
        $action = '<div class="text-center"><div class="btn-group text-left">'
            . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
            . lang('actions') . ' <span class="caret"></span></button>
        <ul class="dropdown-menu pull-right" role="menu">
            <li>' . $detail_link . '</li>
            <li><a href="' . admin_url('products/add/$1') . '"><i class="fa fa-plus-square"></i> ' . lang('duplicate_product') . '</a></li>
            <li><a href="' . admin_url('products/edit/$1') . '"><i class="fa fa-edit"></i> ' . lang('edit_product') . '</a></li>';
        if ($warehouse_id) {
            $action .= '<li><a href="' . admin_url('products/set_rack/$1/' . $warehouse_id) . '" data-toggle="modal" data-target="#myModal"><i class="fa fa-bars"></i> '
                . lang('set_rack') . '</a></li>';
        }
        $action .= '<li><a href="' . base_url() . 'assets/uploads/$2" data-type="image" data-toggle="lightbox"><i class="fa fa-file-photo-o"></i> '
            . lang('view_image') . '</a></li>
            <li>' . $single_barcode . '</li>
            <li class="divider"></li>
            <li>' . $delete_link . '</li>
            </ul>
        </div></div>';
        $this->load->library('datatables');
        if ($warehouse_id) {
            $this->datatables
            ->select($this->db->dbprefix('products') . ".id as productid, {$this->db->dbprefix('products')}.image as image, {$this->db->dbprefix('products')}.code as code, {$this->db->dbprefix('products')}.name as name, {$this->db->dbprefix('brands')}.name as brand, {$this->db->dbprefix('categories')}.name as cname, cost as cost, price as price, COALESCE(wp.quantity, 0) as quantity, {$this->db->dbprefix('units')}.code as unit, wp.rack as rack, alert_quantity", FALSE)
            ->from('products');
            if ($this->Settings->display_all_products) {
                $this->datatables->join("( SELECT product_id, quantity, rack from {$this->db->dbprefix('warehouses_products')} WHERE warehouse_id = {$warehouse_id}) wp", 'products.id=wp.product_id', 'left');
            } else {
                $this->datatables->join('warehouses_products wp', 'products.id=wp.product_id', 'left')
                ->where('wp.warehouse_id', $warehouse_id)
                ->where('wp.quantity !=', 0);
            }
            $this->datatables->join('categories', 'products.category_id=categories.id', 'left')
            ->join('units', 'products.unit=units.id', 'left')
            ->join('brands', 'products.brand=brands.id', 'left');
            // ->group_by("products.id");
        } else {
            $this->datatables
                ->select($this->db->dbprefix('products') . ".id as productid, {$this->db->dbprefix('products')}.image as image, {$this->db->dbprefix('products')}.code as code, {$this->db->dbprefix('products')}.name as name, {$this->db->dbprefix('brands')}.name as brand, {$this->db->dbprefix('categories')}.name as cname, cost as cost, price as price, COALESCE(quantity, 0) as quantity, {$this->db->dbprefix('units')}.code as unit, '' as rack, alert_quantity", FALSE)
                ->from('products')
                ->join('categories', 'products.category_id=categories.id', 'left')
                ->join('units', 'products.unit=units.id', 'left')
                ->join('brands', 'products.brand=brands.id', 'left')
                ->group_by("products.id");
        }
        if (!$this->Owner && !$this->Admin) {
            if (!$this->session->userdata('show_cost')) {
                $this->datatables->unset_column("cost");
            }
            if (!$this->session->userdata('show_price')) {
                $this->datatables->unset_column("price");
            }
        }
        if ($supplier) {
            $this->datatables->where('supplier1', $supplier)
            ->or_where('supplier2', $supplier)
            ->or_where('supplier3', $supplier)
            ->or_where('supplier4', $supplier)
            ->or_where('supplier5', $supplier);
        }
        $this->datatables->add_column("Actions", $action, "productid, image, code, name");
        echo $this->datatables->generate();
    }

    function set_rack($product_id = NULL, $warehouse_id = NULL)
    {
        $this->sma->checkPermissions('edit', true);

        $this->form_validation->set_rules('rack', lang("rack_location"), 'trim|required');

        if ($this->form_validation->run() == true) {
            $data = array('rack' => $this->input->post('rack'),
                'product_id' => $product_id,
                'warehouse_id' => $warehouse_id,
            );
        } elseif ($this->input->post('set_rack')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("products/".$warehouse_id);
        }

        if ($this->form_validation->run() == true && $this->products_model->setRack($data)) {
            $this->session->set_flashdata('message', lang("rack_set"));
            admin_redirect("products/" . $warehouse_id);
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['warehouse_id'] = $warehouse_id;
            $this->data['product'] = $this->site->getProductByID($product_id);
            $wh_pr = $this->products_model->getProductQuantity($product_id, $warehouse_id);
            $this->data['rack'] = $wh_pr['rack'];
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'products/set_rack', $this->data);

        }
    }

    function barcode($product_code = NULL, $bcs = 'code128', $height = 40)
    {
        if ($this->Settings->barcode_img) {
            header('Content-Type: image/png');
        } else {
            header('Content-type: image/svg+xml');
        }
        echo $this->sma->barcode($product_code, $bcs, $height, true, false, true);
    }

    function print_barcodes($product_id = NULL)
    {
        $this->sma->checkPermissions('barcode', true);
        $this->load->admin_model('settings_model');

        $this->form_validation->set_rules('style', lang("style"), 'required');

        if ($this->form_validation->run() == true) {

            $style = $this->input->post('style');
            $bci_size = ($style == 10 || $style == 12 ? 50 : ($style == 14 || $style == 18 ? 30 : 20));
            $currencies = $this->site->getAllCurrencies();
            
            if ($style == 'red') {
                $bci_size = 33;
            }
            
            $s = isset($_POST['product']) ? count($_POST['product']) : 0;
            if ($s < 1 && $parent == '') {
                $this->session->set_flashdata('error', lang('no_product_selected'));
                admin_redirect("products/print_barcodes");
            }
            
            for ($m = 0; $m < $s; $m++) {
                $pid = $_POST['product'][$m];
                $quantity = $_POST['quantity'][$m];

                $product = $this->products_model->getProductWithCategory($pid);

                $config_data_2 = array(
                    'id' => $product->id,
                    'customer_id' => '',
                );
                $temp = $this->site->api_calculate_product_price($config_data_2);                 

                //$product->price = $this->input->post('check_promo') ? ($product->promotion ? $product->promo_price : $product->price) : $product->price;

                if ($variants = $this->products_model->getProductOptions($pid)) {
                    foreach ($variants as $option) {
                        if ($this->input->post('vt_'.$product->id.'_'.$option->id)) {
                            $barcodes[] = array(
                                'site' => $this->input->post('site_name') ? $this->Settings->site_name : FALSE,
                                'name' => $this->input->post('product_name') ? $product->name.' - '.$option->name : FALSE,
                                'image' => $this->input->post('product_image') ? $product->image : FALSE,
                                'barcode' => $product->code . $this->Settings->barcode_separator . $option->id,
                                'bcs' => 'code128',
                                'bcis' => $bci_size,
                                // 'barcode' => $this->product_barcode($product->code . $this->Settings->barcode_separator . $option->id, 'code128', $bci_size),
                                'price' => $temp['price'],
                                'rprice' => $temp['price'],
                                'unit' => $this->input->post('unit') ? $product->unit : FALSE,
                                'category' => $this->input->post('category') ? $product->category : FALSE,
                                'currencies' => $this->input->post('currencies'),
                                'variants' => $this->input->post('variants') ? $variants : FALSE,
                                'quantity' => $quantity
                                );
                        }
                    }
                } else {
                    $barcodes[] = array(
                        'site' => $this->input->post('site_name') ? $this->Settings->site_name : FALSE,
                        'name' => $this->input->post('product_name') ? $product->name : FALSE,
                        'image' => $this->input->post('product_image') ? $product->image : FALSE,
                        // 'barcode' => $this->product_barcode($product->code, $product->barcode_symbology, $bci_size),
                        'barcode' => $product->code,
                        'bcs' => $product->barcode_symbology,
                        'bcis' => $bci_size,
                        'price' => $temp['price'],
                        'rprice' => $temp['price'],
                        'unit' => $this->input->post('unit') ? $product->unit : FALSE,
                        'category' => $this->input->post('category') ? $product->category : FALSE,
                        'currencies' => $this->input->post('currencies'),
                        'variants' => FALSE,
                        'quantity' => $quantity
                        );
                }

            }

            $this->data['categories'] = $this->settings_model->getParentCategories();
            $this->data['barcodes'] = $barcodes;
            $this->data['currencies'] = $currencies;
            $this->data['style'] = $style;
            $this->data['items'] = false;
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('print_barcodes')));
            $meta = array('page_title' => lang('print_barcodes'), 'bc' => $bc);
            $this->page_construct('products/print_barcodes', $meta, $this->data);

        } else {

            if ($this->input->get('purchase') || $this->input->get('transfer')) {
                if ($this->input->get('purchase')) {
                    $purchase_id = $this->input->get('purchase', TRUE);
                    $items = $this->products_model->getPurchaseItems($purchase_id);
                } elseif ($this->input->get('transfer')) {
                    $transfer_id = $this->input->get('transfer', TRUE);
                    $items = $this->products_model->getTransferItems($transfer_id);
                }
                if ($items) {
                    foreach ($items as $item) {
                        if ($row = $this->products_model->getProductByID($item->product_id)) {
                            $selected_variants = false;
                            if ($variants = $this->products_model->getProductOptions($row->id)) {
                                foreach ($variants as $variant) {
                                    $selected_variants[$variant->id] = isset($pr[$row->id]['selected_variants'][$variant->id]) && !empty($pr[$row->id]['selected_variants'][$variant->id]) ? 1 : ($variant->id == $item->option_id ? 1 : 0);
                                }
                            }
                            $pr[$row->id] = array('id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'code' => $row->code, 'name' => $row->name, 'price' => $row->price, 'qty' => $item->quantity, 'variants' => $variants, 'selected_variants' => $selected_variants);
                        }
                    }
                    $this->data['message'] = lang('products_added_to_list');
                }
            }

            if ($product_id) {
                if ($row = $this->site->getProductByID($product_id)) {

                    $selected_variants = false;
                    if ($variants = $this->products_model->getProductOptions($row->id)) {
                        foreach ($variants as $variant) {
                            $selected_variants[$variant->id] = $variant->quantity > 0 ? 1 : 0;
                        }
                    }
                    $pr[$row->id] = array('id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'code' => $row->code, 'name' => $row->name, 'price' => $row->price, 'qty' => $row->quantity, 'variants' => $variants, 'selected_variants' => $selected_variants);

                    $this->data['message'] = lang('product_added_to_list');
                }
            }

            if ($this->input->get('category')) {
                if ($products = $this->products_model->getCategoryProducts($this->input->get('category'))) {
                    foreach ($products as $row) {
                        $selected_variants = false;
                        if ($variants = $this->products_model->getProductOptions($row->id)) {
                            $j = 0;
                            foreach ($variants as $variant) {
                                $selected_variants[$variant->id] = $variant->quantity > 0 ? 1 : 0;
                                if ($j == 0) {
                                    $selected_variants[$variant->id] = 1;
                                    $j++;
                                }
                                    
                            }
                        }
                        $row->quantity = 1;
                        $pr[$row->id] = array('id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'code' => $row->code, 'name' => $row->name, 'price' => $row->price, 'qty' => $row->quantity, 'variants' => $variants, 'selected_variants' => $selected_variants);
                    }
                    $this->data['message'] = lang('products_added_to_list');
                } else {
                    $pr = array();
                    $this->session->set_flashdata('error', lang('no_product_found'));
                }
            }

            if ($this->input->get('subcategory')) {
                if ($products = $this->products_model->getSubCategoryProducts($this->input->get('subcategory'))) {
                    foreach ($products as $row) {
                        $selected_variants = false;
                        if ($variants = $this->products_model->getProductOptions($row->id)) {
                            foreach ($variants as $variant) {
                                $selected_variants[$variant->id] = $variant->quantity > 0 ? 1 : 0;
                            }
                        }
                        $pr[$row->id] = array('id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'code' => $row->code, 'name' => $row->name, 'price' => $row->price, 'qty' => $row->quantity, 'variants' => $variants, 'selected_variants' => $selected_variants);
                    }
                    $this->data['message'] = lang('products_added_to_list');
                } else {
                    $pr = array();
                    $this->session->set_flashdata('error', lang('no_product_found'));
                }
            }
            
            $this->data['categories'] = $this->settings_model->getParentCategories();
            $this->data['items'] = isset($pr) ? json_encode($pr) : false;
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('print_barcodes')));
            $meta = array('page_title' => lang('print_barcodes'), 'bc' => $bc);
            $this->page_construct('products/print_barcodes', $meta, $this->data);

        }
    }


    /* ------------------------------------------------------- */

    function add($id = NULL)
    {
        $this->sma->checkPermissions();
        $this->load->helper('security');
        $warehouses = $this->site->getAllWarehouses();
        $this->form_validation->set_rules('category', lang("category"), 'required|is_natural_no_zero');
        if ($this->input->post('type') == 'standard') {
            $this->form_validation->set_rules('cost', lang("product_cost"), 'required');
            $this->form_validation->set_rules('unit', lang("product_unit"), 'required');
        }
        $this->form_validation->set_rules('code', lang("product_code"), 'is_unique[products.code]|alpha_dash');
        if (SHOP) {
            $this->form_validation->set_rules('slug', lang("slug"), 'required|is_unique[products.slug]|alpha_dash');
        }
        $this->form_validation->set_rules('weight', lang("weight"), 'numeric');
        $this->form_validation->set_rules('product_image', lang("product_image"), 'xss_clean');
        $this->form_validation->set_rules('digital_file', lang("digital_file"), 'xss_clean');
        $this->form_validation->set_rules('userfile', lang("product_gallery_images"), 'xss_clean');
        if ($this->form_validation->run() == true) {
            $tax_rate = $this->input->post('tax_rate') ? $this->site->getTaxRateByID($this->input->post('tax_rate')) : NULL;
            
            $config_data_2 = array(
                'add_ons_table_name' => 'sma_add_ons',
                'table_name' => 'sma_products',
                'post' => $_POST,
            );
            $temp = $this->api_helper->api_add_ons_table_insert($config_data_2);  
            $add_ons = $temp['add_ons'];
            
            $data = array(
                'code' => $this->input->post('code'),
                'barcode_symbology' => $this->input->post('barcode_symbology'),
                'name' => $this->input->post('name'),
                'type' => $this->input->post('type'),
                'brand' => $this->input->post('brand'),
                'category_id' => $this->input->post('category'),
                'subcategory_id' => $this->input->post('subcategory') ? $this->input->post('subcategory') : NULL,
                'cost' => $this->sma->formatDecimal($this->input->post('cost')),
                'price' => $this->sma->formatDecimal($this->input->post('price')),
                'unit' => $this->input->post('unit'),
                'sale_unit' => $this->input->post('default_sale_unit'),
                'purchase_unit' => $this->input->post('default_purchase_unit'),
                'tax_rate' => $this->input->post('tax_rate'),
                'tax_method' => $this->input->post('tax_method'),
                'alert_quantity' => $this->input->post('alert_quantity'),
                'track_quantity' => $this->input->post('track_quantity') ? $this->input->post('track_quantity') : '0',
                'details' => $this->input->post('details'),
                'product_details' => $this->input->post('product_details'),
                'supplier1' => $this->input->post('supplier'),
                'supplier1price' => $this->sma->formatDecimal($this->input->post('supplier_price')),
                'supplier2' => $this->input->post('supplier_2'),
                'supplier2price' => $this->sma->formatDecimal($this->input->post('supplier_2_price')),
                'supplier3' => $this->input->post('supplier_3'),
                'supplier3price' => $this->sma->formatDecimal($this->input->post('supplier_3_price')),
                'supplier4' => $this->input->post('supplier_4'),
                'supplier4price' => $this->sma->formatDecimal($this->input->post('supplier_4_price')),
                'supplier5' => $this->input->post('supplier_5'),
                'supplier5price' => $this->sma->formatDecimal($this->input->post('supplier_5_price')),
                'cf1' => $this->input->post('cf1'),
                'cf2' => $this->input->post('cf2'),
                'cf3' => $this->input->post('cf3'),
                'cf4' => $this->input->post('cf4'),
                'cf5' => $this->input->post('cf5'),
                'cf6' => $this->input->post('cf6'),
                'promotion' => $this->input->post('promotion'),
                'promo_price' => $this->sma->formatDecimal($this->input->post('promo_price')),
                'start_date' => $this->input->post('start_date') ? $this->sma->fsd($this->input->post('start_date')) : NULL,
                'end_date' => $this->input->post('end_date') ? $this->sma->fsd($this->input->post('end_date')) : NULL,
                'supplier1_part_no' => $this->input->post('supplier_part_no'),
                'supplier2_part_no' => $this->input->post('supplier_2_part_no'),
                'supplier3_part_no' => $this->input->post('supplier_3_part_no'),
                'supplier4_part_no' => $this->input->post('supplier_4_part_no'),
                'supplier5_part_no' => $this->input->post('supplier_5_part_no'),
                'file' => $this->input->post('file_link'),
                'slug' => $this->input->post('slug'),
                'weight' => $this->input->post('weight'),
                'featured' => $this->input->post('featured'),
                'hsn_code' => $this->input->post('hsn_code'),
                'hide' => $this->input->post('hide') ? $this->input->post('hide') : 0,
                'second_name' => $this->input->post('second_name'),
                'barcode' => $this->input->post('barcode'),
                'add_ons' => $add_ons,
            );
            $warehouse_qty = NULL;
            $product_attributes = NULL;
            $this->load->library('upload');
            if ($this->input->post('type') == 'standard') {
                $wh_total_quantity = 0;
                $pv_total_quantity = 0;
                for ($s = 2; $s > 5; $s++) {
                    $data['suppliers' . $s] = $this->input->post('supplier_' . $s);
                    $data['suppliers' . $s . 'price'] = $this->input->post('supplier_' . $s . '_price');
                }
                foreach ($warehouses as $warehouse) {
                    if ($this->input->post('wh_qty_' . $warehouse->id)) {
                        $warehouse_qty[] = array(
                            'warehouse_id' => $this->input->post('wh_' . $warehouse->id),
                            'quantity' => $this->input->post('wh_qty_' . $warehouse->id),
                            'rack' => $this->input->post('rack_' . $warehouse->id) ? $this->input->post('rack_' . $warehouse->id) : NULL
                        );
                        $wh_total_quantity += $this->input->post('wh_qty_' . $warehouse->id);
                    }
                }

                if ($this->input->post('attributes')) {
                    $a = sizeof($_POST['attr_name']);
                    for ($r = 0; $r <= $a; $r++) {
                        if (isset($_POST['attr_name'][$r])) {
                            $product_attributes[] = array(
                                'name' => $_POST['attr_name'][$r],
                                'warehouse_id' => $_POST['attr_warehouse'][$r],
                                'quantity' => $_POST['attr_quantity'][$r],
                                'price' => $_POST['attr_price'][$r],
                            );
                            $pv_total_quantity += $_POST['attr_quantity'][$r];
                        }
                    }

                } else {
                    $product_attributes = NULL;
                }

                if ($wh_total_quantity != $pv_total_quantity && $pv_total_quantity != 0) {
                    $this->form_validation->set_rules('wh_pr_qty_issue', 'wh_pr_qty_issue', 'required');
                    $this->form_validation->set_message('required', lang('wh_pr_qty_issue'));
                }
            }

            if ($this->input->post('type') == 'service') {
                $data['track_quantity'] = 0;
            } elseif ($this->input->post('type') == 'combo') {
                $total_price = 0;
                $c = sizeof($_POST['combo_item_code']) - 1;
                for ($r = 0; $r <= $c; $r++) {
                    if (isset($_POST['combo_item_code'][$r]) && isset($_POST['combo_item_quantity'][$r]) && isset($_POST['combo_item_price'][$r])) {
                        $items[] = array(
                            'item_code' => $_POST['combo_item_code'][$r],
                            'quantity' => $_POST['combo_item_quantity'][$r],
                            'unit_price' => $_POST['combo_item_price'][$r],
                        );
                    }
                    $total_price += $_POST['combo_item_price'][$r] * $_POST['combo_item_quantity'][$r];
                }
                if ($this->sma->formatDecimal($total_price) != $this->sma->formatDecimal($this->input->post('price'))) {
                    $this->form_validation->set_rules('combo_price', 'combo_price', 'required');
                    $this->form_validation->set_message('required', lang('pprice_not_match_ciprice'));
                }
                $data['track_quantity'] = 0;
            } elseif ($this->input->post('type') == 'digital') {
                if ($_FILES['digital_file']['size'] > 0) {
                    $config['upload_path'] = $this->digital_upload_path;
                    $config['allowed_types'] = $this->digital_file_types;
                    $config['max_size'] = $this->allowed_file_size;
                    $config['overwrite'] = FALSE;
                    $config['encrypt_name'] = TRUE;
                    $config['max_filename'] = 25;
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('digital_file')) {
                        $error = $this->upload->display_errors();
                        $this->session->set_flashdata('error', $error);
                        admin_redirect("products/add");
                    }
                    $file = $this->upload->file_name;
                    $data['file'] = $file;
                } else {
                    if (!$this->input->post('file_link')) {
                        $this->form_validation->set_rules('digital_file', lang("digital_file"), 'required');
                    }
                }
                $config = NULL;
                $data['track_quantity'] = 0;
            }
            if (!isset($items)) {
                $items = NULL;
            }
            if ($_FILES['product_image']['size'] > 0) {

                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->image_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['max_width'] = $this->Settings->iwidth;
                $config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['max_filename'] = 25;
                $config['encrypt_name'] = TRUE;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('product_image')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("products/add");
                }
                $photo = $this->upload->file_name;
                $data['image'] = $photo;
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload_path . $photo;
                $config['new_image'] = $this->thumbs_path . $photo;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = $this->Settings->twidth;
                $config['height'] = $this->Settings->theight;
                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }
                if ($this->Settings->watermark) {
                    $this->image_lib->clear();
                    $wm['source_image'] = $this->upload_path . $photo;
                    $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                    $wm['wm_type'] = 'text';
                    $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                    $wm['quality'] = '100';
                    $wm['wm_font_size'] = '16';
                    $wm['wm_font_color'] = '999999';
                    $wm['wm_shadow_color'] = 'CCCCCC';
                    $wm['wm_vrt_alignment'] = 'top';
                    $wm['wm_hor_alignment'] = 'left';
                    $wm['wm_padding'] = '10';
                    $this->image_lib->initialize($wm);
                    $this->image_lib->watermark();
                }
                $this->image_lib->clear();
                $config = NULL;
            }

            if ($_FILES['userfile']['name'][0] != "") {

                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->image_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['max_width'] = $this->Settings->iwidth;
                $config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $files = $_FILES;
                $cpt = count($_FILES['userfile']['name']);
                for ($i = 0; $i < $cpt; $i++) {

                    $_FILES['userfile']['name'] = $files['userfile']['name'][$i];
                    $_FILES['userfile']['type'] = $files['userfile']['type'][$i];
                    $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
                    $_FILES['userfile']['error'] = $files['userfile']['error'][$i];
                    $_FILES['userfile']['size'] = $files['userfile']['size'][$i];

                    $this->upload->initialize($config);

                    if (!$this->upload->do_upload()) {
                        $error = $this->upload->display_errors();
                        $this->session->set_flashdata('error', $error);
                        admin_redirect("products/add");
                    } else {

                        $pho = $this->upload->file_name;

                        $photos[] = $pho;

                        $this->load->library('image_lib');
                        $config['image_library'] = 'gd2';
                        $config['source_image'] = $this->upload_path . $pho;
                        $config['new_image'] = $this->thumbs_path . $pho;
                        $config['maintain_ratio'] = TRUE;
                        $config['width'] = $this->Settings->twidth;
                        $config['height'] = $this->Settings->theight;

                        $this->image_lib->initialize($config);

                        if (!$this->image_lib->resize()) {
                            echo $this->image_lib->display_errors();
                        }

                        if ($this->Settings->watermark) {
                            $this->image_lib->clear();
                            $wm['source_image'] = $this->upload_path . $pho;
                            $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                            $wm['wm_type'] = 'text';
                            $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                            $wm['quality'] = '100';
                            $wm['wm_font_size'] = '16';
                            $wm['wm_font_color'] = '999999';
                            $wm['wm_shadow_color'] = 'CCCCCC';
                            $wm['wm_vrt_alignment'] = 'top';
                            $wm['wm_hor_alignment'] = 'left';
                            $wm['wm_padding'] = '10';
                            $this->image_lib->initialize($wm);
                            $this->image_lib->watermark();
                        }

                        $this->image_lib->clear();
                    }
                }
                $config = NULL;
            } else {
                $photos = NULL;
            }
            $data['quantity'] = isset($wh_total_quantity) ? $wh_total_quantity : 0;
            // $this->sma->print_arrays($data, $warehouse_qty, $product_attributes);
        }

        if ($this->form_validation->run() == true && $this->products_model->addProduct($data, $items, $warehouse_qty, $product_attributes, $photos)) {

            $temp2 = $this->site->api_select_some_fields_with_where("
                *    
                "
                ,"sma_products"
                ,"code = '".$data['code']."'"
                ,"arr"
            );

            $temp = $this->site->api_select_some_fields_with_where("
                *     
                "
                ,"sma_warehouses"
                ,"id > 0 order by id asc"
                ,"arr"
            );
            for ($i=0;$i<count($temp);$i++) {
                $config_data_2 = array(
                    'product_id' => $temp2[0]['id'],
                    'warehouse_id' => $temp[$i]['id'],
                );            
                $this->site->api_update_product_quantity($config_data_2);                
            }
            

            $config_data = array(
                'product_id' => $temp2[0]['id'],
                'category_id' => $data['category_id'],
                'subcategory_id' => $data['subcategory_id'],
            );            
            $this->site->api_update_product_display($config_data);
            
            $translate = 'initial_first_add_ons:{}:';
            foreach($_POST as $name => $value) {
                if (is_int(strpos($name,"translate_"))) {
                    $value = $this->input->post($name);
                    $temp_name = str_replace('translate_','',$name);        
                    $translate .= $temp_name.':{'.$value.'}:';          
                    
                    if ($temp_name == 'en')
                        $temp_product_name = $value;
                }
            }
            $temp = array(
                'name' => $temp_product_name,
                'translate' => $translate
            );
            $this->db->update('sma_products', $temp,"id = ".$temp2[0]['id']);

            $config_data = array();
            $this->api_cache->api_front_main_category($config_data);

            if ($_POST['add_ons_promotion_type'] == 'list' && $_POST['add_ons_promotion_id']) {
                $config_data = array(
                    'table_name' => 'sma_product_promotion_item',
                    'select_table' => 'sma_product_promotion_item',
                    'translate' => '',
                    'description' => '',
                    'select_condition' => "product_id = ".$temp2[0]['id']." and promotion_id = ".$_POST['add_ons_promotion_id'],
                );
                $temp = $this->api_helper->api_select_data_v2($config_data);
                if (count($temp) <= 0) {
                    $config_data = array(
                        'table_name' => 'sma_product_promotion',
                        'select_table' => 'sma_product_promotion',
                        'translate' => '',
                        'description' => '',
                        'select_condition' => "id = ".$_POST['add_ons_promotion_id'],
                    );
                    $temp_3 = $this->api_helper->api_select_data_v2($config_data);
                    $temp_2 = array(
                        'promotion_id' => $_POST['add_ons_promotion_id'],
                        'product_id' => $temp2[0]['id'],
                        'rate' => $temp_3[0]['rate'],
                    );
                    $this->db->insert('sma_product_promotion_item', $temp_2);
                }
            }

            $this->session->set_flashdata('message', lang("product_added"));
            admin_redirect('products');
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['customers'] = $this->site->getCustomers();          
            $this->data['categories'] = $this->site->getAllCategories();
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->data['brands'] = $this->site->getAllBrands();
            $this->data['base_units'] = $this->site->getAllBaseUnits();
            $this->data['warehouses'] = $warehouses;
            $this->data['warehouses_products'] = $id ? $this->products_model->getAllWarehousesWithPQ($id) : NULL;
            $this->data['product'] = $id ? $this->products_model->getProductByID($id) : NULL;

            $config_data = array(
                'table_name' => 'sma_product_promotion',
                'select_table' => 'sma_product_promotion',
                'translate' => '',
                'select_condition' => "id > 0 order by name asc",
            );
            $this->data['select_product_promotion'] = $this->site->api_select_data_v2($config_data);

            $this->data['variants'] = $this->products_model->getAllVariants();
            $this->data['combo_items'] = ($id && $this->data['product']->type == 'combo') ? $this->products_model->getProductComboItems($id) : NULL;
            $this->data['product_options'] = $id ? $this->products_model->getProductOptionsWithWH($id) : NULL;
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('add_product')));
            $meta = array('page_title' => lang('add_product'), 'bc' => $bc);
            $this->page_construct('products/add', $meta, $this->data);
        }
    }

    function suggestions()
    {
        $term = $this->input->get('term', TRUE);
        if (strlen($term) < 1 || !$term) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . admin_url('welcome') . "'; }, 10);</script>");
        }

        $rows = $this->products_model->getProductNames($term);
        if ($rows) {
            foreach ($rows as $row) {
                $pr[] = array('id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'code' => $row->code, 'name' => $row->name, 'price' => $row->price, 'qty' => 1);
            }
            $this->sma->send_json($pr);
        } else {
            $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
        }
    }

    function get_suggestions()
    {
        $term = $this->input->get('term', TRUE);
        if (strlen($term) < 1 || !$term) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . admin_url('welcome') . "'; }, 10);</script>");
        }

        //$rows = $this->products_model->getProductsForPrinting($term);
        $config_data = array(
            'table_name' => 'sma_products',
            'select_table' => 'sma_products',
            'translate' => '',
            'select_condition' => "getTranslate(add_ons,'no_use','".f_separate."','".v_separate."') != 'yes' and (name like '%" . $term . "%' or code like '%" . $term . "%') order by name asc limit 20",
        );
        $select_data = $this->site->api_select_data_v2($config_data);
        for ($i=0;$i<count($select_data);$i++) {
            foreach(array_keys($select_data[$i]) as $key){
                $rows[$i]->{$key} = $select_data[$i][$key];
            }
        }

        if ($rows) {
            foreach ($rows as $row) {
                $variants = $this->products_model->getProductOptions($row->id);
                $pr[] = array('id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'code' => $row->code, 'name' => $row->name, 'price' => $row->price, 'qty' => 1, 'variants' => $variants);
            }
            $this->sma->send_json($pr);
        } else {
            $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
        }
    }

    function addByAjax()
    {
        if (!$this->mPermissions('add')) {
            exit(json_encode(array('msg' => lang('access_denied'))));
        }
        if ($this->input->get('token') && $this->input->get('token') == $this->session->userdata('user_csrf') && $this->input->is_ajax_request()) {
            $product = $this->input->get('product');
            if (!isset($product['code']) || empty($product['code'])) {
                exit(json_encode(array('msg' => lang('product_code_is_required'))));
            }
            if (!isset($product['name']) || empty($product['name'])) {
                exit(json_encode(array('msg' => lang('product_name_is_required'))));
            }
            if (!isset($product['category_id']) || empty($product['category_id'])) {
                exit(json_encode(array('msg' => lang('product_category_is_required'))));
            }
            if (!isset($product['unit']) || empty($product['unit'])) {
                exit(json_encode(array('msg' => lang('product_unit_is_required'))));
            }
            if (!isset($product['price']) || empty($product['price'])) {
                exit(json_encode(array('msg' => lang('product_price_is_required'))));
            }
            if (!isset($product['cost']) || empty($product['cost'])) {
                exit(json_encode(array('msg' => lang('product_cost_is_required'))));
            }
            if ($this->products_model->getProductByCode($product['code'])) {
                exit(json_encode(array('msg' => lang('product_code_already_exist'))));
            }
            if ($row = $this->products_model->addAjaxProduct($product)) {
                $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                $pr = array('id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'code' => $row->code, 'qty' => 1, 'cost' => $row->cost, 'name' => $row->name, 'tax_method' => $row->tax_method, 'tax_rate' => $tax_rate, 'discount' => '0');
                $this->sma->send_json(array('msg' => 'success', 'result' => $pr));
            } else {
                exit(json_encode(array('msg' => lang('failed_to_add_product'))));
            }
        } else {
            json_encode(array('msg' => 'Invalid token'));
        }

    }


    /* -------------------------------------------------------- */

    function edit($id = NULL)
    {
        $this->sma->checkPermissions();
        $this->load->helper('security');
        if ($this->input->post('id')) {
            $id = $this->input->post('id');
        }
        $warehouses = $this->site->getAllWarehouses();
        $warehouses_products = $this->products_model->getAllWarehousesWithPQ($id);
        $product = $this->site->getProductByID_v2($id);
        if (!$id || !$product) {
            $this->session->set_flashdata('error', lang('prduct_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->form_validation->set_rules('category', lang("category"), 'required|is_natural_no_zero');
        if ($this->input->post('type') == 'standard') {
            $this->form_validation->set_rules('cost', lang("product_cost"), 'required');
            $this->form_validation->set_rules('unit', lang("product_unit"), 'required');
        }
        $this->form_validation->set_rules('code', lang("product_code"), 'alpha_dash');
        if ($this->input->post('code') !== $product->code) {
            $this->form_validation->set_rules('code', lang("product_code"), 'is_unique[products.code]');
        }
        if (SHOP) {
            $this->form_validation->set_rules('slug', lang("slug"), 'required|alpha_dash');
            if ($this->input->post('slug') !== $product->slug) {
                $this->form_validation->set_rules('slug', lang("slug"), 'required|is_unique[products.slug]|alpha_dash');
            }
        }
        $this->form_validation->set_rules('weight', lang("weight"), 'numeric');
        $this->form_validation->set_rules('product_image', lang("product_image"), 'xss_clean');
        $this->form_validation->set_rules('digital_file', lang("digital_file"), 'xss_clean');
        $this->form_validation->set_rules('userfile', lang("product_gallery_images"), 'xss_clean');

        if ($this->form_validation->run('products/add') == true) {

            $data = array('code' => $this->input->post('code'),
                'barcode_symbology' => $this->input->post('barcode_symbology'),
                'name' => $this->input->post('name'),
                'type' => $this->input->post('type'),
                'brand' => $this->input->post('brand'),
                'category_id' => $this->input->post('category'),
                'subcategory_id' => $this->input->post('subcategory') ? $this->input->post('subcategory') : NULL,
                'cost' => $this->sma->formatDecimal($this->input->post('cost')),
                'price' => $this->sma->formatDecimal($this->input->post('price')),
                'unit' => $this->input->post('unit'),
                'sale_unit' => $this->input->post('default_sale_unit'),
                'purchase_unit' => $this->input->post('default_purchase_unit'),
                'tax_rate' => $this->input->post('tax_rate'),
                'tax_method' => $this->input->post('tax_method'),
                'alert_quantity' => $this->input->post('alert_quantity'),
                'track_quantity' => $this->input->post('track_quantity') ? $this->input->post('track_quantity') : '0',
                'details' => $this->input->post('details'),
                'product_details' => $this->input->post('product_details'),
                'supplier1' => $this->input->post('supplier'),
                'supplier1price' => $this->sma->formatDecimal($this->input->post('supplier_price')),
                'supplier2' => $this->input->post('supplier_2'),
                'supplier2price' => $this->sma->formatDecimal($this->input->post('supplier_2_price')),
                'supplier3' => $this->input->post('supplier_3'),
                'supplier3price' => $this->sma->formatDecimal($this->input->post('supplier_3_price')),
                'supplier4' => $this->input->post('supplier_4'),
                'supplier4price' => $this->sma->formatDecimal($this->input->post('supplier_4_price')),
                'supplier5' => $this->input->post('supplier_5'),
                'supplier5price' => $this->sma->formatDecimal($this->input->post('supplier_5_price')),
                'cf1' => $this->input->post('cf1'),
                'cf2' => $this->input->post('cf2'),
                'cf3' => $this->input->post('cf3'),
                'cf4' => $this->input->post('cf4'),
                'cf5' => $this->input->post('cf5'),
                'cf6' => $this->input->post('cf6'),
                'promotion' => $this->input->post('promotion'),
                'promo_price' => $this->sma->formatDecimal($this->input->post('promo_price')),
                'start_date' => $this->input->post('start_date') ? $this->sma->fsd($this->input->post('start_date')) : NULL,
                'end_date' => $this->input->post('end_date') ? $this->sma->fsd($this->input->post('end_date')) : NULL,
                'supplier1_part_no' => $this->input->post('supplier_part_no'),
                'supplier2_part_no' => $this->input->post('supplier_2_part_no'),
                'supplier3_part_no' => $this->input->post('supplier_3_part_no'),
                'supplier4_part_no' => $this->input->post('supplier_4_part_no'),
                'supplier5_part_no' => $this->input->post('supplier_5_part_no'),
                'slug' => $this->input->post('slug'),
                'weight' => $this->input->post('weight'),
                'featured' => $this->input->post('featured'),
                'hsn_code' => $this->input->post('hsn_code'),
                'hide' => $this->input->post('hide') ? $this->input->post('hide') : 0,
                'second_name' => $this->input->post('second_name'),
                'barcode' => $this->input->post('barcode'),
            );
            $warehouse_qty = NULL;
            $product_attributes = NULL;
            $update_variants = array();
            $this->load->library('upload');
            if ($this->input->post('type') == 'standard') {
                if ($product_variants = $this->products_model->getProductOptions($id)) {
                    foreach ($product_variants as $pv) {
                        $update_variants[] = array(
                            'id' => $this->input->post('variant_id_'.$pv->id),
                            'name' => $this->input->post('variant_name_'.$pv->id),
                            'cost' => $this->input->post('variant_cost_'.$pv->id),
                            'price' => $this->input->post('variant_price_'.$pv->id),
                        );
                    }
                }
                for ($s = 2; $s > 5; $s++) {
                    $data['suppliers' . $s] = $this->input->post('supplier_' . $s);
                    $data['suppliers' . $s . 'price'] = $this->input->post('supplier_' . $s . '_price');
                }
                foreach ($warehouses as $warehouse) {
                    $warehouse_qty[] = array(
                        'warehouse_id' => $this->input->post('wh_' . $warehouse->id),
                        'rack' => $this->input->post('rack_' . $warehouse->id) ? $this->input->post('rack_' . $warehouse->id) : NULL
                    );
                }

                if ($this->input->post('attributes')) {
                    $a = sizeof($_POST['attr_name']);
                    for ($r = 0; $r <= $a; $r++) {
                        if (isset($_POST['attr_name'][$r])) {
                            if ($product_variatnt = $this->products_model->getPrductVariantByPIDandName($id, trim($_POST['attr_name'][$r]))) {
                                $this->form_validation->set_message('required', lang("product_already_has_variant").' ('.$_POST['attr_name'][$r].')');
                                $this->form_validation->set_rules('new_product_variant', lang("new_product_variant"), 'required');
                            } else {
                                $product_attributes[] = array(
                                    'name' => $_POST['attr_name'][$r],
                                    'warehouse_id' => $_POST['attr_warehouse'][$r],
                                    'quantity' => $_POST['attr_quantity'][$r],
                                    'price' => $_POST['attr_price'][$r],
                                );
                            }
                        }
                    }

                } else {
                    $product_attributes = NULL;
                }

            }

            if ($this->input->post('type') == 'service') {
                $data['track_quantity'] = 0;
            } elseif ($this->input->post('type') == 'combo') {
                $total_price = 0;
                $c = sizeof($_POST['combo_item_code']) - 1;
                for ($r = 0; $r <= $c; $r++) {
                    if (isset($_POST['combo_item_code'][$r]) && isset($_POST['combo_item_quantity'][$r]) && isset($_POST['combo_item_price'][$r])) {
                        $items[] = array(
                            'item_code' => $_POST['combo_item_code'][$r],
                            'quantity' => $_POST['combo_item_quantity'][$r],
                            'unit_price' => $_POST['combo_item_price'][$r],
                        );
                    }
                    $total_price += $_POST['combo_item_price'][$r] * $_POST['combo_item_quantity'][$r];
                }
                if ($this->sma->formatDecimal($total_price) != $this->sma->formatDecimal($this->input->post('price'))) {
                    $this->form_validation->set_rules('combo_price', 'combo_price', 'required');
                    $this->form_validation->set_message('required', lang('pprice_not_match_ciprice'));
                }
                $data['track_quantity'] = 0;
            } elseif ($this->input->post('type') == 'digital') {
                if ($this->input->post('file_link')) {
                    $data['file'] = $this->input->post('file_link');
                }
                if ($_FILES['digital_file']['size'] > 0) {
                    $config['upload_path'] = $this->digital_upload_path;
                    $config['allowed_types'] = $this->digital_file_types;
                    $config['max_size'] = $this->allowed_file_size;
                    $config['overwrite'] = FALSE;
                    $config['encrypt_name'] = TRUE;
                    $config['max_filename'] = 25;
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('digital_file')) {
                        $error = $this->upload->display_errors();
                        $this->session->set_flashdata('error', $error);
                        admin_redirect("products/add");
                    }
                    $file = $this->upload->file_name;
                    $data['file'] = $file;
                }
                $config = NULL;
                $data['track_quantity'] = 0;
            }
            if (!isset($items)) {
                $items = NULL;
            }
            if ($_FILES['product_image']['size'] > 0) {
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->image_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['max_width'] = $this->Settings->iwidth;
                $config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('product_image')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("products/edit/" . $id);
                }
                $photo = $this->upload->file_name;
                $data['image'] = $photo;
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload_path . $photo;
                $config['new_image'] = $this->thumbs_path . $photo;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = $this->Settings->twidth;
                $config['height'] = $this->Settings->theight;
                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }
                if ($this->Settings->watermark) {
                    $this->image_lib->clear();
                    $wm['source_image'] = $this->upload_path . $photo;
                    $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                    $wm['wm_type'] = 'text';
                    $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                    $wm['quality'] = '100';
                    $wm['wm_font_size'] = '16';
                    $wm['wm_font_color'] = '999999';
                    $wm['wm_shadow_color'] = 'CCCCCC';
                    $wm['wm_vrt_alignment'] = 'top';
                    $wm['wm_hor_alignment'] = 'left';
                    $wm['wm_padding'] = '10';
                    $this->image_lib->initialize($wm);
                    $this->image_lib->watermark();
                }
                $this->image_lib->clear();
                $config = NULL;
            }

            if ($_FILES['userfile']['name'][0] != "") {

                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->image_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['max_width'] = $this->Settings->iwidth;
                $config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $files = $_FILES;
                $cpt = count($_FILES['userfile']['name']);
                for ($i = 0; $i < $cpt; $i++) {

                    $_FILES['userfile']['name'] = $files['userfile']['name'][$i];
                    $_FILES['userfile']['type'] = $files['userfile']['type'][$i];
                    $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
                    $_FILES['userfile']['error'] = $files['userfile']['error'][$i];
                    $_FILES['userfile']['size'] = $files['userfile']['size'][$i];

                    $this->upload->initialize($config);

                    if (!$this->upload->do_upload()) {
                        $error = $this->upload->display_errors();
                        $this->session->set_flashdata('error', $error);
                        admin_redirect("products/edit/" . $id);
                    } else {

                        $pho = $this->upload->file_name;

                        $photos[] = $pho;

                        $this->load->library('image_lib');
                        $config['image_library'] = 'gd2';
                        $config['source_image'] = $this->upload_path . $pho;
                        $config['new_image'] = $this->thumbs_path . $pho;
                        $config['maintain_ratio'] = TRUE;
                        $config['width'] = $this->Settings->twidth;
                        $config['height'] = $this->Settings->theight;

                        $this->image_lib->initialize($config);

                        if (!$this->image_lib->resize()) {
                            echo $this->image_lib->display_errors();
                        }

                        if ($this->Settings->watermark) {
                            $this->image_lib->clear();
                            $wm['source_image'] = $this->upload_path . $pho;
                            $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                            $wm['wm_type'] = 'text';
                            $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                            $wm['quality'] = '100';
                            $wm['wm_font_size'] = '16';
                            $wm['wm_font_color'] = '999999';
                            $wm['wm_shadow_color'] = 'CCCCCC';
                            $wm['wm_vrt_alignment'] = 'top';
                            $wm['wm_hor_alignment'] = 'left';
                            $wm['wm_padding'] = '10';
                            $this->image_lib->initialize($wm);
                            $this->image_lib->watermark();
                        }

                        $this->image_lib->clear();
                    }
                }
                $config = NULL;
            } else {
                $photos = NULL;
            }
            //$data['quantity'] = isset($wh_total_quantity) ? $wh_total_quantity : 0;
            // $this->sma->print_arrays($data, $warehouse_qty, $update_variants, $product_attributes, $photos, $items);
        }

        if ($this->form_validation->run() == true && $this->products_model->updateProduct($id, $data, $items, $warehouse_qty, $product_attributes, $photos, $update_variants)) {
            $translate = '';
            foreach($_POST as $name => $value) {
                if (is_int(strpos($name,"translate_"))) {
                    $value = $this->input->post($name);
                    $temp_name = str_replace('translate_','',$name);        
                    $translate .= $temp_name.':{'.$value.'}:';               
                    if ($temp_name == 'en')                        
                        $temp_product_name = $value;
                }
            }
            $temp = array(
                'name' => $temp_product_name,
                'translate' => $translate
            );
            $this->db->update('sma_products', $temp,"id = ".$id);

            $config_data = array();
            $this->api_cache->api_front_main_category($config_data);
            
            
            foreach($_POST as $name => $value) {
                if (is_int(strpos($name,"add_ons_"))) {
                    $value = $this->input->post($name);
                    $temp_name = str_replace('add_ons_','',$name);                        
                    $config_data = array(
                        'table_name' => 'sma_products',
                        'id_name' => 'id',
                        'field_add_ons_name' => 'add_ons',
                        'selected_id' => $id,
                        'add_ons_title' => $temp_name,
                        'add_ons_value' => $value,                    
                    );
                    $this->site->api_update_add_ons_field($config_data);                        
                }
            }

            $config_data = array(
                'product_id' => $id,
                'category_id' => $data['category_id'],
                'subcategory_id' => $data['subcategory_id'],
            );            
            $this->site->api_update_product_display($config_data);

            if ($_POST['add_ons_promotion_type'] == 'list' && $_POST['add_ons_promotion_id']) {
                $config_data = array(
                    'table_name' => 'sma_product_promotion_item',
                    'select_table' => 'sma_product_promotion_item',
                    'translate' => '',
                    'description' => '',
                    'select_condition' => "product_id = ".$id." and promotion_id = ".$_POST['add_ons_promotion_id'],
                );
                $temp = $this->api_helper->api_select_data_v2($config_data);
                if (count($temp) <= 0) {
                    $config_data = array(
                        'table_name' => 'sma_product_promotion',
                        'select_table' => 'sma_product_promotion',
                        'translate' => '',
                        'description' => '',
                        'select_condition' => "id = ".$_POST['add_ons_promotion_id'],
                    );
                    $temp_3 = $this->api_helper->api_select_data_v2($config_data);
                    $temp_2 = array(
                        'promotion_id' => $_POST['add_ons_promotion_id'],
                        'product_id' => $id,
                        'rate' => $temp_3[0]['rate'],
                    );
                    $this->db->insert('sma_product_promotion_item', $temp_2);
                }
            }

            $this->session->set_flashdata('message', lang("product_updated"));
            admin_redirect('products');
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['customers'] = $this->site->getCustomers();          
            $this->data['categories'] = $this->site->getAllCategories();
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->data['brands'] = $this->site->getAllBrands();
            $this->data['base_units'] = $this->site->getAllBaseUnits();
            $this->data['warehouses'] = $warehouses;
            $this->data['warehouses_products'] = $warehouses_products;
            $this->data['product'] = $product;

            $config_data = array(
                'table_name' => 'sma_products',
                'select_table' => 'sma_products',
                'translate' => 'yes',
                'select_condition' => "id = ".$id,
            );
            $temp = $this->site->api_select_data_v2($config_data);
            for ($i=0;$i<count($temp);$i++) {
                foreach(array_keys($temp[$i]) as $key){
                    $this->data['product']->{$key} = $temp[$i][$key];
                }
            }
            $temp = $this->site->api_select_some_fields_with_where("
                *     
                "
                ,"sma_warehouses"
                ,"id > 0 order by id asc"
                ,"arr"
            );
            $this->data['api_warehouse'] = $temp;
            
            $config_data = array(
                'table_name' => 'sma_product_promotion',
                'select_table' => 'sma_product_promotion',
                'translate' => '',
                'select_condition' => "id > 0 order by name asc",
            );
            $this->data['select_product_promotion'] = $this->site->api_select_data_v2($config_data);
                        
            $this->data['variants'] = $this->products_model->getAllVariants();
            $this->data['subunits'] = $this->site->getUnitsByBUID($product->unit);
            $this->data['product_variants'] = $this->products_model->getProductOptions($id);
            $this->data['combo_items'] = $product->type == 'combo' ? $this->products_model->getProductComboItems($product->id) : NULL;
            $this->data['product_options'] = $id ? $this->products_model->getProductOptionsWithWH($id) : NULL;
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('edit_product')));
            $meta = array('page_title' => lang('edit_product'), 'bc' => $bc);
            $this->page_construct('products/edit', $meta, $this->data);
        }
    }

    /* ---------------------------------------------------------------- */

    function import_csv()
    {
        $this->sma->checkPermissions('csv');
        $this->load->helper('security');
        $this->form_validation->set_rules('userfile', lang("upload_file"), 'xss_clean');

        if ($this->form_validation->run() == true) {
            if (isset($_FILES["userfile"])) {
                $this->load->library('upload');
                $config = array();
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = 'csv';
                $config['max_size'] = 262144;
                $config['overwrite'] = TRUE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("products/import_csv");
                }

                $csv = $this->upload->file_name;

                $arrResult = array();
                $handle = fopen($this->digital_upload_path . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 5000, ",")) !== FALSE) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);
                $updated = 0; $items = array();
                //'image'             => isset($value[1]) ? trim($value[1]).'.jpg' : '',
                foreach ($arrResult as $key => $value) {
                    $item = [
                        'name'              => isset($value[0]) ? trim($value[0]) : '',
                        'code'              => isset($value[1]) ? trim($value[1]) : '',
                        'barcode_symbology' => isset($value[2]) ? mb_strtolower(trim($value[2]), 'UTF-8') : '',
                        'brand'             => isset($value[3]) ? trim($value[3]) : '',
                        'category_code'     => isset($value[4]) ? trim($value[4]) : '',
                        'unit'              => isset($value[5]) ? trim($value[5]) : '',
                        'sale_unit'         => isset($value[6]) ? trim($value[6]) : '',
                        'purchase_unit'     => isset($value[7]) ? trim($value[7]) : '',
                        'cost'              => isset($value[8]) ? trim($value[8]) : '',
                        'price'             => isset($value[9]) ? trim($value[9]) : '',
                        'alert_quantity'    => isset($value[10]) ? trim($value[10]) : '',
                        'tax_rate'          => isset($value[11]) ? trim($value[11]) : '',
                        'tax_method'        => isset($value[12]) ? (trim($value[12]) == 'exclusive' ? 1 : 0) : '',
                        
                        'subcategory_code'  => isset($value[14]) ? trim($value[14]) : '',
                        'variants'          => isset($value[15]) ? trim($value[15]) : '',
                        'cf1'               => isset($value[16]) ? trim($value[16]) : '',
                        'cf2'               => isset($value[17]) ? trim($value[17]) : '',
                        'cf3'               => isset($value[18]) ? trim($value[18]) : '',
                        'cf4'               => isset($value[19]) ? trim($value[19]) : '',
                        'cf5'               => isset($value[20]) ? trim($value[20]) : '',
                        'cf6'               => isset($value[21]) ? trim($value[21]) : '',
                        'hsn_code'          => isset($value[22]) ? trim($value[22]) : '',
                        'second_name'       => isset($value[23]) ? trim($value[23]) : '',
                        'slug'              => $this->sma->slug($value[0]),
                    ];


                    if ($catd = $this->products_model->getCategoryByCode($item['category_code'])) {
                        $tax_details = $this->products_model->getTaxRateByName($item['tax_rate']);
                        $prsubcat = $this->products_model->getCategoryByCode($item['subcategory_code']);
                        $brand = $this->products_model->getBrandByName($item['brand']);
                        $unit = $this->products_model->getUnitByCode($item['unit']);
                        $base_unit = $unit ? $unit->id : NULL;
                        $sale_unit = $base_unit;
                        $purcahse_unit = $base_unit;
                        if ($base_unit) {
                            $units = $this->site->getUnitsByBUID($base_unit);
                            foreach ($units as $u) {
                                if ($u->code == $item['sale_unit']) {
                                    $sale_unit = $u->id;
                                }
                                if ($u->code == $item['purchase_unit']) {
                                    $purcahse_unit = $u->id;
                                }
                            }
                        } else {
                            $this->session->set_flashdata('error', lang("check_unit") . " (" . $item['unit'] . "). " . lang("unit_code_x_exist") . " " . lang("line_no") . " " . ($key+1));
                            admin_redirect("products/import_csv");
                        }

                        unset($item['category_code'], $item['subcategory_code']);
                        $item['unit'] = $base_unit;
                        $item['sale_unit'] = $sale_unit;
                        $item['category_id'] = $catd->id;
                        $item['purchase_unit'] = $purcahse_unit;
                        $item['brand'] = $brand ? $brand->id : NULL;
                        $item['tax_rate'] = $tax_details ? $tax_details->id : NULL;
                        $item['subcategory_id'] = $prsubcat ? $prsubcat->id : NULL;

                        if ($product = $this->products_model->getProductByCode($item['code'])) {
                            if ($product->type == 'standard') {
                                if ($item['variants']) {
                                    $vs = explode('|', $item['variants']);
                                    foreach ($vs as $v) {
                                        $variants[] = ['product_id' => $product->id, 'name' => trim($v)];
                                    }
                                }
                                unset($item['variants']);
                                if ($this->products_model->updateProduct($product->id, $item, null, null, null, null, $variants)) {

                                    $config_data = array(
                                        'product_id' => $product->id,
                                        'category_id' => $item['category_id'],
                                        'subcategory_id' => $item['subcategory_id'],
                                    );            
                                    $this->site->api_update_product_display($config_data);                                    
                                    $updated++;
                                }
                            }
                            $item = false;
                        }
                    } else {
                        $this->session->set_flashdata('error', lang("check_category_code") . " (" . $item['category_code'] . "). " . lang("category_code_x_exist") . " " . lang("line_no") . " " . ($key+1));
                        admin_redirect("products/import_csv");
                    }

                    if ($item) {
                        $items[] = $item;
                    }
                }
            }
        }

        if ($this->form_validation->run() == true && !empty($items)) {
            if ($this->products_model->add_products($items)) {
                $updated = $updated ? '<p>'.sprintf(lang("products_updated"), $updated).'</p>' : '';
                $this->session->set_flashdata('message', sprintf(lang("products_added"), count($items)).$updated);

                admin_redirect('products');
            }
        } else {
            if (isset($items) && empty($items)) {
                if ($updated) {
                    $this->session->set_flashdata('message', sprintf(lang("products_updated"), $updated));
                    admin_redirect('products');
                } else {
                    $this->session->set_flashdata('warning', lang('csv_issue'));
                }
                admin_redirect('products/import_csv');
            }

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['userfile'] = array('name' => 'userfile',
                'id' => 'userfile',
                'type' => 'text',
                'value' => $this->form_validation->set_value('userfile')
            );

            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('import_products_by_csv')));
            $meta = array('page_title' => lang('import_products_by_csv'), 'bc' => $bc);
            $this->page_construct('products/import_csv', $meta, $this->data);

        }
    }

    /* ------------------------------------------------------------------ */

    function update_price()
    {
        $this->sma->checkPermissions('csv');
        $this->load->helper('security');
        $this->form_validation->set_rules('userfile', lang("upload_file"), 'xss_clean');

        if ($this->form_validation->run() == true) {

            if (DEMO) {
                $this->session->set_flashdata('message', lang("disabled_in_demo"));
                admin_redirect('welcome');
            }

            if (isset($_FILES["userfile"])) {

                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = 'csv';
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = TRUE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("products");
                }

                $csv = $this->upload->file_name;

                $arrResult = array();
                $handle = fopen($this->digital_upload_path . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 1000, ",")) !== FALSE) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);

                $keys = array('code', 'price');

                $final = array();

                foreach ($arrResult as $key => $value) {
                    $final[] = array_combine($keys, $value);
                }
                $rw = 2;
                foreach ($final as $csv_pr) {
                    if (!$this->products_model->getProductByCode(trim($csv_pr['code']))) {
                        $this->session->set_flashdata('message', lang("check_product_code") . " (" . $csv_pr['code'] . "). " . lang("code_x_exist") . " " . lang("line_no") . " " . $rw);
                        admin_redirect("products");
                    }
                    $rw++;
                }
            }

        } elseif ($this->input->post('update_price')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("system_settings/group_product_prices/".$group_id);
        }

        if ($this->form_validation->run() == true && !empty($final)) {
            $this->products_model->updatePrice($final);
            $this->session->set_flashdata('message', lang("price_updated"));
            admin_redirect('products');
        } else {

            $this->data['userfile'] = array('name' => 'userfile',
                'id' => 'userfile',
                'type' => 'text',
                'value' => $this->form_validation->set_value('userfile')
            );
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme.'products/update_price', $this->data);

        }
    }

    /* ------------------------------------------------------------------------------- */

    function delete($id = NULL)
    {
        $this->sma->checkPermissions(NULL, TRUE);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }       

        if ($this->products_model->deleteProduct($id)) {
            $config_data = array();
            $this->api_cache->api_front_main_category($config_data);   

            if($this->input->is_ajax_request()) {
                $this->sma->send_json(array('error' => 0, 'msg' => lang("product_deleted")));
            }
            $this->session->set_flashdata('message', lang('product_deleted'));
            admin_redirect('products');
        }

    }

    /* ----------------------------------------------------------------------------- */

    function quantity_adjustments()
    {
        $this->sma->checkPermissions('adjustments');

        $sort_by = $this->input->get('sort_by');
        $sort_order = $this->input->get('sort_order');
        $per_page = $this->input->get('per_page') != '' ? $this->input->get('per_page') : 100;
        $page = $this->input->get('page') != '' ? $this->input->get('page') : 1;

        $offset_no = $page != '' ? ($page - 1 ) * $per_page : $offset_no;
        
        if ($page != '')
            $offset_no = ($page - 1 ) * $per_page;
        else
            $offset_no = 0;

        if ($page != '') $temp_url .= '?page=';
        if ($per_page != '') $temp_url .= '&per_page='.$per_page;

        foreach($_GET as $name => $value) {
            if ($name != 'page' && $name != 'per_page') {
                $value = $this->input->get($name);
                if ($value != '') $temp_url .= '&'.$name.'='.$value;
            }            
        }
        $config_data = array(
            'lg' => 'en',
            'table_name' => 'sma_adjustments',
            'table_id' => 'id',
            'limit' => $per_page,
            'offset_no' => $offset_no,
        );
        $select_data = $this->products_model->api_get_adjustment($config_data);

        $config_data = array(
            'lg' => 'en',
            'table_name' => 'sma_adjustments',
            'table_id' => 'id',
            'limit' => '',
            'offset_no' => '',
        );
        $temp_count = $this->products_model->api_get_adjustment($config_data);
        $total_record = count($temp_count);

        /* config pagination */
        $this->data['select_data'] = $select_data;

        $this->data['page'] = $page;
        $this->data['search'] = $search;
        $this->data['sort_by'] = $sort_by;
        $this->data['sort_order'] = $sort_order;
        $this->data['per_page'] = $per_page;
        $this->data['total_rows_sale'] = $total_record;
        $this->data['index_page'] = 'products/quantity_adjustments';
        $this->data['show_data'] = $page;

        $config_base_url = site_url() . 'products/quantity_adjustments'; //your url where the content is displayed
        $config['base_url'] = $config_base_url.$temp_url; //your url where the content is displayed
        $config['query_string_segment'] = 'page';
        $config['per_page'] = $per_page; //$per_page;
        $config['num_links'] = 5;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['full_tag_open'] = '<ul class="pagination pagination-new-bg">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = FALSE;
        $config['last_link'] = FALSE;
        $config['prev_link'] = '&lt; Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next &gt;';
        $config['next_tag_open'] = '<li class="next">';
        $config['next_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['total_rows'] = $total_record; //$this->data['total_rows_sale'];        
        $this->load->library('pagination');   
        $this->pagination->initialize($config);
        /** end config pagination */
        /**Showing Label Data At Table Footer */
        $showing = $total_record != '' ? 1 : 0;
        $label_show_to= ($page == '' || $per_page < $total_record) ?  $per_page  :  $total_record;
        $label_show = "Showing " . $showing . " to ".$label_show_to." of : " . number_format($total_record) . " entries";

        $show_page = $page > 1 ? ($page * $per_page) : $total_record;
        $show_page_no = $show_page > $total_record ? $total_record : $page * $per_page;
        $label_show_page = "Showing " . (($page - 1) * ($per_page) + 1) . " to " . $show_page_no . " (filtered from " . number_format($total_record) . " total entries) ";
        $this->data['show'] = $search != '' || $page > 1 ? $label_show_page : $label_show;

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        $temp_warhouse = $this->site->api_select_some_fields_with_where("
            *     
            "
            ,"sma_warehouses"
            ,"id > 0 order by id asc"
            ,"arr"
        );
        $this->data['api_warehouse'] = $temp_warhouse;

        $temp = $this->site->api_select_some_fields_with_where("
            *, concat(first_name,' ',last_name) as fullname
            "
            ,"sma_users"
            ,"id > 0 order by fullname asc"
            ,"arr"
        );
        $this->data['api_user'] = $temp;

        $temp = $this->site->api_select_some_fields_with_where("
            id, concat(name,' (',code,')') as product_name   
            "
            ,"sma_products"
            ,"id > 0 order by product_name asc"
            ,"arr"
        );
        $this->data['api_product'] = $temp;

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('quantity_adjustments')));
        $meta = array('page_title' => lang('quantity_adjustments'), 'bc' => $bc);
        $this->page_construct('products/quantity_adjustments', $meta, $this->data);
    }


    function getadjustments($warehouse_id = NULL)
    {
        $this->sma->checkPermissions('adjustments');

        $delete_link = "<a href='#' class='tip po' title='<b>" . $this->lang->line("delete_adjustment") . "</b>' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('products/delete_adjustment/$1') . "'>"
            . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a>";

        $this->load->library('datatables');
        $this->datatables
            ->select("{$this->db->dbprefix('adjustments')}.id as id, date, reference_no, warehouses.name as wh_name, CONCAT({$this->db->dbprefix('users')}.first_name, ' ', {$this->db->dbprefix('users')}.last_name) as created_by, note, attachment")
            ->from('adjustments')
            ->join('warehouses', 'warehouses.id=adjustments.warehouse_id', 'left')
            ->join('users', 'users.id=adjustments.created_by', 'left')
            ->group_by("adjustments.id");
            if ($warehouse_id) {
                $this->datatables->where('adjustments.warehouse_id', $warehouse_id);
            }
        $this->datatables->add_column("Actions", "<div class='text-center'><a href='" . admin_url('products/edit_adjustment/$1') . "' class='tip' title='" . lang("edit_adjustment") . "'><i class='fa fa-edit'></i></a> " . $delete_link . "</div>", "id");

        echo $this->datatables->generate();

    }

    public function view_adjustment($id)
    {
        $this->sma->checkPermissions('adjustments', TRUE);

        $adjustment = $this->products_model->getAdjustmentByID($id);
        if (!$id || !$adjustment) {
            $this->session->set_flashdata('error', lang('adjustment_not_found'));
            $this->sma->md();
        }

        $this->data['inv'] = $adjustment;
        $temp = $this->site->api_select_some_fields_with_where("
            name
            "
            ,"sma_warehouses"
            ,"id = ".$adjustment->warehouse_id
            ,"arr"
        );
        $this->data['inv']->warehouse_name = $temp[0]['name'];
        $this->data['rows'] = $this->products_model->getAdjustmentItems($id);

        $temp = $this->site->api_select_some_fields_with_where("
            *     
            "
            ,"sma_adjustments"
            ,"date > STR_TO_DATE('".$this->data['inv']->date."', '%Y-%m-%d %H:%i:%s') and warehouse_id = ".$this->data['inv']->warehouse_id
            ,"arr"
        );
        $temp_3 = array();
        for ($i=0;$i<count($temp);$i++) {
            $temp_2 = $this->site->api_select_some_fields_with_where("
                *     
                "
                ,"sma_adjustment_items"
                ,"adjustment_id = ".$temp[$i]['id']
                ,"arr"
            );
            for ($i2=0;$i2<count($temp_2);$i2++) {                
                foreach ($this->data['rows'] as $row) {
                    if ($row->product_id == $temp_2[$i2]['product_id'] && $row->option_id == $temp_2[$i2]['option_id']) {
                        if ($temp_2[$i2]['type'] == 'addition')
                            $this->data['inv']->{'adjustment_'.$row->product_id} += $temp_2[$i2]['quantity'];
                        else
                            $this->data['inv']->{'adjustment_'.$row->product_id} -= $temp_2[$i2]['quantity'];
                        $this->data['inv']->{'adjustment_track_'.$row->product_id} .= '-'.$temp[$i]['id'];
                        break;
                    }
                }
            }
        }

        $config_data = array(
            'table_name' => 'sma_sales',
            'select_table' => 'sma_sales',
            'translate' => '',
            'select_condition' => "date > STR_TO_DATE('".$this->data['inv']->date."', '%Y-%m-%d %H:%i:%s') and warehouse_id = ".$this->data['inv']->warehouse_id." and getTranslate(add_ons,'cs_reference_no','".f_separate."','".v_separate."') = ''",
        );
        $temp = $this->site->api_select_data_v2($config_data);        
        $temp_3 = array();
        for ($i=0;$i<count($temp);$i++) {
            $temp_2 = $this->site->api_select_some_fields_with_where("
                *     
                "
                ,"sma_sale_items"
                ,"sale_id = ".$temp[$i]['id']
                ,"arr"
            );
            for ($i2=0;$i2<count($temp_2);$i2++) {                
                foreach ($this->data['rows'] as $row) {
                    if ($row->product_id == $temp_2[$i2]['product_id'] && $row->option_id == $temp_2[$i2]['option_id']) {
                        if (($temp[$i]['sale_status'] == 'delivering' || $temp[$i]['sale_status'] == 'completed') || $temp[$i]['sample'] == 1) {

                            if ($temp_2[$i2]['option_id'] > 0)
                                $condition = " and option_id = ".$temp_2[$i2]['option_id'];
                            else
                                $condition = '';                            
                            $temp5 = $this->site->api_select_some_fields_with_where("
                                *     
                                "
                                ,"sma_consignment_sale_items"
                                ,"sale_id = ".$temp[$i]['id']." and product_id = ".$temp_2[$i2]['product_id']." ".$condition
                                ,"arr"
                            );
                            $temp6 = 0;
                            for ($i5=0;$i5<count($temp5);$i5++) {
                                $temp6 = $temp6 + $temp5[$i5]['quantity'];
                            }
                            $temp_2[$i2]['quantity'] = $temp_2[$i2]['quantity'] - $temp6;

                            $this->data['inv']->{'sale_'.$row->product_id} += $temp_2[$i2]['quantity'];
                            $this->data['inv']->{'adjustment_sale_track'.$row->product_id} .= '-'.$temp[$i]['id'];

                            $temp_3 = $this->site->api_select_some_fields_with_where("
                                *     
                                "
                                ,"sma_returns"
                                ,"add_ons like '%:sale_id:{".$temp[$i]['id']."}:%'"
                                ,"arr"
                            );
                            for ($i3=0;$i3<count($temp_3);$i3++) {
                                $temp_4 = $this->site->api_select_some_fields_with_where("
                                    *     
                                    "
                                    ,"sma_return_items"
                                    ,"return_id = ".$temp_3[$i3]['id']
                                    ,"arr"
                                );
                                for ($i4=0;$i4<count($temp_4);$i4++) {
                                    if ($row->product_id == $temp_4[$i4]['product_id'] && $row->option_id == $temp_4[$i4]['option_id']) {
                                        $this->data['inv']->{'sale_'.$row->product_id} -= $temp_4[$i4]['quantity'];
                                    }
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }

        $config_data = array(
            'table_name' => 'sma_consignment',
            'select_table' => 'sma_consignment',
            'translate' => '',
            'select_condition' => "date > STR_TO_DATE('".$this->data['inv']->date."', '%Y-%m-%d %H:%i:%s') and warehouse_id = ".$this->data['inv']->warehouse_id." and sale_status = 'delivering' or sale_status = 'completed'",
        );
        $temp = $this->site->api_select_data_v2($config_data);        
        $temp_3 = array();
        for ($i=0;$i<count($temp);$i++) {
            $temp_2 = $this->site->api_select_some_fields_with_where("
                *     
                "
                ,"sma_consignment_items"
                ,"sale_id = ".$temp[$i]['id']
                ,"arr"
            );
            for ($i2=0;$i2<count($temp_2);$i2++) {                
                foreach ($this->data['rows'] as $row) {
                    if ($row->product_id == $temp_2[$i2]['product_id'] && $row->option_id == $temp_2[$i2]['option_id']) {
                        $this->data['inv']->{'consignment_'.$row->product_id} += $temp_2[$i2]['quantity'];
                        $this->data['inv']->{'adjustment_consignment_track'.$row->product_id} .= '-'.$temp[$i]['id'];
                        $temp_3 = $this->site->api_select_some_fields_with_where("
                            *     
                            "
                            ,"sma_returns"
                            ,"add_ons like '%:cs_reference_no:{".$temp[$i]['reference_no']."}:%'"
                            ,"arr"
                        );
                        for ($i3=0;$i3<count($temp_3);$i3++) {
                            $temp_4 = $this->site->api_select_some_fields_with_where("
                                *     
                                "
                                ,"sma_return_items"
                                ,"return_id = ".$temp_3[$i3]['id']
                                ,"arr"
                            );
                            for ($i4=0;$i4<count($temp_4);$i4++) {
                                if ($row->product_id == $temp_4[$i4]['product_id'] && $row->option_id == $temp_4[$i4]['option_id']) {
                                    $this->data['inv']->{'consignment_'.$row->product_id} -= $temp_4[$i4]['quantity'];
                                }
                            }
                        }                        
                        break;
                    }
                }
            }
        }

        $temp = $this->site->api_select_some_fields_with_where("
            *     
            "
            ,"sma_purchases"
            ,"date > STR_TO_DATE('".$this->data['inv']->date."', '%Y-%m-%d %H:%i:%s') and warehouse_id = ".$this->data['inv']->warehouse_id." and getTranslate(add_ons,'consignment_status','".f_separate."','".v_separate."') = ''"
            ,"arr"  
        );        
        $temp_3 = array();
        for ($i=0;$i<count($temp);$i++) {
            $temp_2 = $this->site->api_select_some_fields_with_where("
                *     
                "
                ,"sma_purchase_items"
                ,"purchase_id = ".$temp[$i]['id']
                ,"arr"  
            );
            for ($i2=0;$i2<count($temp_2);$i2++) {                
                foreach ($this->data['rows'] as $row) {
                    if ($row->product_id == $temp_2[$i2]['product_id'] && $row->option_id == $temp_2[$i2]['option_id']) {
                        if ($temp[$i]['status'] == 'received') {
                            $this->data['inv']->{'purchase_'.$row->product_id} += $temp_2[$i2]['unit_quantity'];
                            $this->data['inv']->{'adjustment_purchase_track'.$row->product_id} .= '-'.$temp[$i]['id'];
                            break;
                        }
                    }
                }
            }
        }

        $temp = $this->site->api_select_some_fields_with_where("
            *     
            "
            ,"sma_purchases"
            ,"date > STR_TO_DATE('".$this->data['inv']->date."', '%Y-%m-%d %H:%i:%s') and warehouse_id = ".$this->data['inv']->warehouse_id." and getTranslate(add_ons,'consignment_status','".f_separate."','".v_separate."') != ''"
            ,"arr"  
        );        
        $temp_3 = array();
        for ($i=0;$i<count($temp);$i++) {
            $temp_2 = $this->site->api_select_some_fields_with_where("
                *     
                "
                ,"sma_purchase_items"
                ,"purchase_id = ".$temp[$i]['id']
                ,"arr"  
            );
            for ($i2=0;$i2<count($temp_2);$i2++) {                
                foreach ($this->data['rows'] as $row) {
                    if ($row->product_id == $temp_2[$i2]['product_id'] && $row->option_id == $temp_2[$i2]['option_id']) {
                        if ($temp[$i]['status'] == 'received') {
                            $this->data['inv']->{'consignment_purchase_'.$row->product_id} += $temp_2[$i2]['unit_quantity'];
                            $this->data['inv']->{'adjustment_consignment_purchase_track'.$row->product_id} .= '-'.$temp[$i]['id'];

                            $temp_3 = $this->site->api_select_some_fields_with_where("
                                *     
                                "
                                ,"sma_returns"
                                ,"add_ons like '%:purchase_id:{".$temp[$i]['id']."}:%'"
                                ,"arr"
                            );
                            for ($i3=0;$i3<count($temp_3);$i3++) {
                                $temp_4 = $this->site->api_select_some_fields_with_where("
                                    *     
                                    "
                                    ,"sma_return_items"
                                    ,"return_id = ".$temp_3[$i3]['id']
                                    ,"arr"
                                );
                                for ($i4=0;$i4<count($temp_4);$i4++) {
                                    if ($row->product_id == $temp_4[$i4]['product_id'] && $row->option_id == $temp_4[$i4]['option_id']) {
                                        $this->data['inv']->{'consignment_purchase_'.$row->product_id} -= $temp_4[$i4]['quantity'];
                                    }
                                }
                            }                        
                            break;
                        }
                    }
                }
            }
        }

        $temp = $this->site->api_select_some_fields_with_where("
            *     
            "
            ,"sma_transfers"
            ,"date > STR_TO_DATE('".$this->data['inv']->date."', '%Y-%m-%d %H:%i:%s') and (from_warehouse_id = ".$this->data['inv']->warehouse_id." or to_warehouse_id = ".$this->data['inv']->warehouse_id.")"
            ,"arr"  
        );        
        $temp_3 = array();
        for ($i=0;$i<count($temp);$i++) {
            $temp_2 = $this->site->api_select_some_fields_with_where("
                *     
                "
                ,"sma_transfer_items"
                ,"transfer_id = ".$temp[$i]['id']
                ,"arr"  
            );
            for ($i2=0;$i2<count($temp_2);$i2++) {                
                foreach ($this->data['rows'] as $row) {
                    if ($row->product_id == $temp_2[$i2]['product_id'] && $row->option_id == $temp_2[$i2]['option_id']) {
                        if ($temp[$i]['status'] == 'sent' || $temp[$i]['status'] == 'completed') {
                            if ($temp[$i]['to_warehouse_id'] == $this->data['inv']->warehouse_id)
                                $this->data['inv']->{'transfer_'.$row->product_id} += $temp_2[$i2]['quantity'];
                            if ($temp[$i]['from_warehouse_id'] == $this->data['inv']->warehouse_id)
                                $this->data['inv']->{'transfer_'.$row->product_id} -= $temp_2[$i2]['quantity'];
                            $this->data['inv']->{'adjustment_transfer_track_'.$row->product_id} .= '-'.$temp[$i]['id'];
                            break;
                        }
                    }
                }
            }
        }

        $this->data['created_by'] = $this->site->getUser($adjustment->created_by);
        $this->data['updated_by'] = $this->site->getUser($adjustment->updated_by);
        $this->data['warehouse'] = $this->site->getWarehouseByID($adjustment->warehouse_id);
        $this->load->view($this->theme.'products/view_adjustment', $this->data);
    }

    function add_adjustment($count_id = NULL)
    {
        $this->sma->checkPermissions('adjustments', true);
        $this->form_validation->set_rules('warehouse', lang("warehouse"), 'required');

        if ($this->form_validation->run() == true) {

            $date = date('Y-m-d H:i:s');

            $warehouse_id = $this->input->post('warehouse');
            $note = $this->sma->clear_tags($this->input->post('note'));


            $i = isset($_POST['product_id']) ? sizeof($_POST['product_id']) : 0;

            $b = 1;
            for ($r = 0; $r < $i; $r++) {
                if ($_POST['actual_qty'][$r] < 0) {
                    $b = 0;
                    break;
                }
            }
            if ($b == 0) {
                $this->session->set_flashdata('error', lang("All_actual_quantity_must_not_less_than_zero"));
                redirect('admin/products/add_adjustment');
            }
                        
            for ($r = 0; $r < $i; $r++) {

                $product_id = $_POST['product_id'][$r];
                $type = $_POST['type'][$r];
                $quantity = $_POST['quantity'][$r];
                $serial = $_POST['serial'][$r];
                $variant = isset($_POST['variant'][$r]) && !empty($_POST['variant'][$r]) ? $_POST['variant'][$r] : NULL;

                if (!$this->Settings->overselling && $type == 'subtraction' && !$count_id) {
                    if ($variant) {
                        if($op_wh_qty = $this->products_model->getProductWarehouseOptionQty($variant, $warehouse_id)) {
                            if ($op_wh_qty->quantity < $quantity) {
                                $this->session->set_flashdata('error', lang('warehouse_option_qty_is_less_than_damage'));
                                redirect($_SERVER["HTTP_REFERER"]);
                            }
                        } else {
                            $this->session->set_flashdata('error', lang('warehouse_option_qty_is_less_than_damage'));
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                    }
                    if($wh_qty = $this->products_model->getProductQuantity($product_id, $warehouse_id)) {
                        if ($wh_qty['quantity'] < $quantity) {
                            $this->session->set_flashdata('error', lang('warehouse_qty_is_less_than_damage'));
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                    } else {
                        $this->session->set_flashdata('error', lang('warehouse_qty_is_less_than_damage'));
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                }
                
                if ($quantity < 0) {
                    $quantity = abs($quantity);
                    if ($type == 'addition') 
                        $type = 'subtraction';
                    elseif ($type == 'subtraction') 
                        $type = 'addition';                    
                }

                $products[] = array(
                    'product_id' => $product_id,
                    'type' => $type,
                    'quantity' => $quantity,
                    'warehouse_id' => $warehouse_id,
                    'option_id' => $variant,
                    'serial_no' => $serial,
                    );

            }

            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("products"), 'required');
            } else {
                krsort($products);
            }

            $data = array(
                'date' => $date,
                'warehouse_id' => $warehouse_id,
                'note' => $note,
                'created_by' => $this->session->userdata('user_id'),
                'count_id' => $this->input->post('count_id') ? $this->input->post('count_id') : NULL,
                );

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }

            // $this->sma->print_arrays($data, $products);

        }

        if ($this->form_validation->run() == true && $this->products_model->addAdjustment($data, $products)) {
            $this->session->set_userdata('remove_qals', 1);
            $this->session->set_flashdata('message', lang("quantity_adjusted"));
            admin_redirect('products/quantity_adjustments');
        } else {

            if ($count_id) {
                //$stock_count = $this->products_model->getStouckCountByID($count_id);
                //$items = $this->products_model->getStockCountItems($count_id);
                foreach ($items as $item) {
                    $c = sha1(uniqid(mt_rand(), true));
                    if ($item->counted != $item->expected) {
                        $product = $this->site->getProductByID($item->product_id);
                        $row = json_decode('{}');
                        $row->id = $item->product_id;
                        $row->code = $product->code;
                        $row->name = $product->name;
                        $row->qty = $item->counted-$item->expected;
                        $row->type = $row->qty > 0 ? 'addition' : 'subtraction';
                        $row->qty = $row->qty > 0 ? $row->qty : (0-$row->qty);
                        $options = $this->products_model->getProductOptions($product->id);
                        $row->option = $item->product_variant_id ? $item->product_variant_id : 0;
                        $row->serial = '';
                        $ri = $this->Settings->item_addition ? $product->id : $c;
                        $milliseconds = round(microtime(true) * 1000);
                        
                        $pr[$ri] = array('order' => $milliseconds, 'type_disabled' => '', 'id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")",
                            'row' => $row, 'options' => $options);
                        $c++;
                    }
                }
            }

            $temp_warhouse = $this->site->api_select_some_fields_with_where("
                *     
                "
                ,"sma_warehouses"
                ,"id > 0 order by id asc"
                ,"arr"
            );
            $this->data['api_warehouse'] = $temp_warhouse;
            
            $this->data['adjustment_items'] = $count_id ? json_encode($pr) : FALSE;
            $this->data['warehouse_id'] = $count_id ? $stock_count->warehouse_id : FALSE;
            $this->data['count_id'] = $count_id;
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('add_adjustment')));
            $meta = array('page_title' => lang('add_adjustment'), 'bc' => $bc);
            $this->page_construct('products/add_adjustment', $meta, $this->data);

        }
    }

    function edit_adjustment($id)
    {
        $this->sma->checkPermissions('adjustments', true);
        $adjustment = $this->products_model->getAdjustmentByID($id);
        if (!$id || !$adjustment) {
            $this->session->set_flashdata('error', lang('adjustment_not_found'));
            $this->sma->md();
        }
        $this->form_validation->set_rules('warehouse', lang("warehouse"), 'required');

        if ($this->form_validation->run() == true) {

            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld($this->input->post('date'));
            } else {
                $date = $adjustment->date;
            }

            $reference_no = $this->input->post('reference_no');
            $warehouse_id = $this->input->post('warehouse');
            $note = $this->sma->clear_tags($this->input->post('note'));

            $i = isset($_POST['product_id']) ? sizeof($_POST['product_id']) : 0;
            for ($r = 0; $r < $i; $r++) {

                $product_id = $_POST['product_id'][$r];
                $type = $_POST['type'][$r];
                $quantity = $_POST['quantity'][$r];
                $serial = $_POST['serial'][$r];
                $variant = isset($_POST['variant'][$r]) && !empty($_POST['variant'][$r]) ? $_POST['variant'][$r] : null;

                if (!$this->Settings->overselling && $type == 'subtraction') {
                    if ($variant) {
                        if($op_wh_qty = $this->products_model->getProductWarehouseOptionQty($variant, $warehouse_id)) {
                            if ($op_wh_qty->quantity < $quantity) {
                                $this->session->set_flashdata('error', lang('warehouse_option_qty_is_less_than_damage'));
                                redirect($_SERVER["HTTP_REFERER"]);
                            }
                        } else {
                            $this->session->set_flashdata('error', lang('warehouse_option_qty_is_less_than_damage'));
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                    }
                    if($wh_qty = $this->products_model->getProductQuantity($product_id, $warehouse_id)) {
                        if ($wh_qty['quantity'] < $quantity) {
                            $this->session->set_flashdata('error', lang('warehouse_qty_is_less_than_damage'));
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                    } else {
                        $this->session->set_flashdata('error', lang('warehouse_qty_is_less_than_damage'));
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                }

                if ($quantity < 0) {
                    $quantity = abs($quantity);
                    if ($type == 'addition') 
                        $type = 'subtraction';
                    elseif ($type == 'subtraction') 
                        $type = 'addition';                    
                }
                
                $products[] = array(
                    'product_id' => $product_id,
                    'type' => $type,
                    'quantity' => $quantity,
                    'warehouse_id' => $warehouse_id,
                    'option_id' => $variant,
                    'serial_no' => $serial,
                    );

            }

            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("products"), 'required');
            } else {
                krsort($products);
            }

            $data = array(
                'date' => $date,
                'reference_no' => $reference_no,
                'warehouse_id' => $warehouse_id,
                'note' => $note,
                'created_by' => $this->session->userdata('user_id')
                );

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }

            // $this->sma->print_arrays($data, $products);

        }

        if ($this->form_validation->run() == true && $this->products_model->updateAdjustment($id, $data, $products)) {
            $this->session->set_userdata('remove_qals', 1);
            $this->session->set_flashdata('message', lang("quantity_adjusted"));
            admin_redirect('products/quantity_adjustments');
        } else {
            $temp_warhouse = $this->site->api_select_some_fields_with_where("
                *     
                "
                ,"sma_warehouses"
                ,"id > 0 order by id asc"
                ,"arr"
            );
            $this->data['api_warehouse'] = $temp_warhouse;
            $inv_items = $this->products_model->getAdjustmentItems($id);

            //asort($inv_items);
            foreach ($inv_items as $item) {
                $c = sha1(uniqid(mt_rand(), true));
                $product = $this->site->getProductByID($item->product_id);
                $row = json_decode('{}');
                $row->id = $item->product_id;
                $row->code = $product->code;
                $row->name = $product->name;
                $row->qty = $item->quantity;
                $row->type = $item->type;
                $temp_qty = $row->qty;
                $options = $this->products_model->getProductOptions($product->id);
                if (is_array($options)) if (count($options) > 0) {
                    $temp_qty = 0;
                    for ($i=0;$i<count($options);$i++) {
                        $options[$i]->name = $options[$i]->name.' ('.number_format($options[$i]->quantity,2).')';
                    }
                }

                $config_data_2 = array(
                    'table_name' => 'sma_products',
                    'select_table' => 'sma_products',
                    'translate' => '',
                    'select_condition' => "id = ".$item->product_id,
                );
                $temp = $this->site->api_select_data_v2($config_data_2);

                $temp_qty = $temp[0]['quantity'];

                $temp_select = '';
                $total_quantity = 0;
                for ($i=0;$i<count($temp_warhouse);$i++) {
                    $row->{'quantity_'.$temp_warhouse[$i]['id']} = number_format($temp[0]['quantity_'.$temp_warhouse[$i]['id']],2);

                    if ($row->{'quantity_'.$temp_warhouse[$i]['id']} == '') 
                        $row->{'quantity_'.$temp_warhouse[$i]['id']} = 0;
                    if ($temp_warhouse[$i]['id'] == 1) {
                        $temp_select .=  '<strong>'.$temp_warhouse[$i]['code'].':</strong> '.number_format($temp_qty,2);
                        $row->{'quantity_'.$temp_warhouse[$i]['id']} = $temp_qty;
                        $total_quantity = $total_quantity + $temp_qty;
                    }
                    else {
                        $temp_select .=  '</br><strong>'.$temp_warhouse[$i]['code'].':</strong> '.number_format($row->{'quantity_'.$temp_warhouse[$i]['id']},2);
                        $total_quantity = $total_quantity + $row->{'quantity_'.$temp_warhouse[$i]['id']};
                    }
                }            
                $row->quantity = number_format($temp_qty,2);
                $row->total_quantity = number_format($total_quantity,2);

                $row->option = $item->option_id ? $item->option_id : 0;                

                $row->serial = $item->serial_no ? $item->serial_no : '';
                $ri = $this->Settings->item_addition ? $item->product_id : $c;

                $row->code = $product->code;
                $milliseconds = round(microtime(true) * 1000);

                $pr[$ri] = array('order' => $milliseconds, 'type_disabled' => 1, 'id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")",
                    'row' => $row, 'options' => $options);
                $c++;
            }
            $this->data['adjustment'] = $adjustment;
            $this->data['adjustment_items'] = json_encode($pr);

            $temp = $this->site->api_select_some_fields_with_where("
                product_id
                "
                ,"sma_adjustment_items"
                ,"adjustment_id = ".$id
                ,"arr"
            );
            $temp2 = '';
            for ($i=0;$i<count($temp);$i++) {
                $temp2 .= '-'.$temp[$i]['product_id'];
            }
            $this->data['api_adjustment_items'] = $temp2;
                        
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('edit_adjustment')));
            $meta = array('page_title' => lang('edit_adjustment'), 'bc' => $bc);
            $this->page_construct('products/edit_adjustment', $meta, $this->data);

        }
    }

    function add_adjustment_by_csv()
    {
        $this->sma->checkPermissions('adjustments', true);
        $this->form_validation->set_rules('warehouse', lang("warehouse"), 'required');

        if ($this->form_validation->run() == true) {

            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld($this->input->post('date'));
            } else {
                $date = date('Y-m-d H:s:i');
            }

            $reference_no = $this->input->post('reference_no') ? $this->input->post('reference_no') : $this->site->getReference('qa');
            $warehouse_id = $this->input->post('warehouse');
            $note = $this->sma->clear_tags($this->input->post('note'));
            $data = array(
                'date' => $date,
                'reference_no' => $reference_no,
                'warehouse_id' => $warehouse_id,
                'note' => $note,
                'created_by' => $this->session->userdata('user_id'),
                'count_id' => NULL,
                );

            if ($_FILES['csv_file']['size'] > 0) {

                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = 'csv';
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('csv_file')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                $csv = $this->upload->file_name;
                $data['attachment'] = $csv;

                $arrResult = array();
                $handle = fopen($this->digital_upload_path . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 5000, ",")) !== FALSE) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);
                $keys = array('code', 'quantity', 'variant');
                $final = array();
                foreach ($arrResult as $key => $value) {
                    $final[] = array_combine($keys, $value);
                }
                // $this->sma->print_arrays($final);
                $rw = 2;
                foreach ($final as $pr) {
                    if ($product = $this->products_model->getProductByCode(trim($pr['code']))) {
                        $csv_variant = trim($pr['variant']);
                        $variant = !empty($csv_variant) ? $this->products_model->getProductVariantID($product->id, $csv_variant) : FALSE;

                        $csv_quantity = trim($pr['quantity']);
                        $type = $csv_quantity > 0 ? 'addition' : 'subtraction';
                        $quantity = $csv_quantity > 0 ? $csv_quantity : (0-$csv_quantity);

                        if (!$this->Settings->overselling && $type == 'subtraction') {
                            if ($variant) {
                                if($op_wh_qty = $this->products_model->getProductWarehouseOptionQty($variant, $warehouse_id)) {
                                    if ($op_wh_qty->quantity < $quantity) {
                                        $this->session->set_flashdata('error', lang('warehouse_option_qty_is_less_than_damage'). ' - ' . lang('line_no') . ' ' . $rw);
                                        redirect($_SERVER["HTTP_REFERER"]);
                                    }
                                } else {
                                    $this->session->set_flashdata('error', lang('warehouse_option_qty_is_less_than_damage'). ' - ' . lang('line_no') . ' ' . $rw);
                                    redirect($_SERVER["HTTP_REFERER"]);
                                }
                            }
                            if($wh_qty = $this->products_model->getProductQuantity($product->id, $warehouse_id)) {
                                if ($wh_qty['quantity'] < $quantity) {
                                    $this->session->set_flashdata('error', lang('warehouse_qty_is_less_than_damage'). ' - ' . lang('line_no') . ' ' . $rw);
                                    redirect($_SERVER["HTTP_REFERER"]);
                                }
                            } else {
                                $this->session->set_flashdata('error', lang('warehouse_qty_is_less_than_damage'). ' - ' . lang('line_no') . ' ' . $rw);
                                redirect($_SERVER["HTTP_REFERER"]);
                            }
                        }

                        $products[] = array(
                            'product_id' => $product->id,
                            'type' => $type,
                            'quantity' => $quantity,
                            'warehouse_id' => $warehouse_id,
                            'option_id' => $variant,
                            );

                    } else {
                        $this->session->set_flashdata('error', lang('check_product_code') . ' (' . $pr['code'] . '). ' . lang('product_code_x_exist') . ' ' . lang('line_no') . ' ' . $rw);
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                    $rw++;
                }

            } else {
                $this->form_validation->set_rules('csv_file', lang("upload_file"), 'required');
            }

            // $this->sma->print_arrays($data, $products);

        }

        if ($this->form_validation->run() == true && $this->products_model->addAdjustment($data, $products)) {
            $this->session->set_flashdata('message', lang("quantity_adjusted"));
            admin_redirect('products/quantity_adjustments');
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('add_adjustment')));
            $meta = array('page_title' => lang('add_adjustment_by_csv'), 'bc' => $bc);
            $this->page_construct('products/add_adjustment_by_csv', $meta, $this->data);

        }
    }

    function delete_adjustment($id = NULL)
    {
        $this->sma->checkPermissions('delete', TRUE);

        if ($this->products_model->deleteAdjustment($id)) {
            $this->session->set_flashdata('message', lang("adjustment_deleted"));
            admin_redirect("products/quantity_adjustments");
        }

    }

    /* --------------------------------------------------------------------------------------------- */

    function modal_view($id = NULL)
    {
        $this->sma->checkPermissions('index', TRUE);

        $pr_details = $this->site->getProductByID_v2($id);
        if (!$id || !$pr_details) {
            $this->session->set_flashdata('error', lang('prduct_not_found'));
            $this->sma->md();
        }
        $this->data['barcode'] = "<img src='" . admin_url('products/gen_barcode/' . $pr_details->code . '/' . $pr_details->barcode_symbology . '/40/0') . "' alt='" . $pr_details->code . "' class='pull-left' />";
        if ($pr_details->type == 'combo') {
            $this->data['combo_items'] = $this->products_model->getProductComboItems($id);
        }
        $this->data['product'] = $pr_details;
        $config_data = array(
            'table_name' => 'sma_products',
            'select_table' => 'sma_products',
            'translate' => 'yes',
            'select_condition' => "id = ".$id,
        );
        $temp = $this->site->api_select_data_v2($config_data);
        for ($i=0;$i<count($temp);$i++) {
            foreach(array_keys($temp[$i]) as $key){
                $this->data['product']->{$key} = $temp[$i][$key];
            }
        }        
        $temp = $this->site->api_select_some_fields_with_where("
            *     
            "
            ,"sma_warehouses"
            ,"id > 0 order by id asc"
            ,"arr"
        );
        $this->data['api_warehouse'] = $temp;

        $this->data['unit'] = $this->site->getUnitByID($pr_details->unit);
        $this->data['brand'] = $this->site->getBrandByID($pr_details->brand);
        $this->data['images'] = $this->products_model->getProductPhotos($id);
        $this->data['category'] = $this->site->getCategoryByID($pr_details->category_id);
        $this->data['subcategory'] = $pr_details->subcategory_id ? $this->site->getCategoryByID($pr_details->subcategory_id) : NULL;
        $this->data['tax_rate'] = $pr_details->tax_rate ? $this->site->getTaxRateByID($pr_details->tax_rate) : NULL;
        $this->data['warehouses'] = $this->products_model->getAllWarehousesWithPQ($id);
        $this->data['options'] = $this->products_model->getProductOptionsWithWH($id);
        $this->data['variants'] = $this->products_model->getProductOptions($id);

        $this->load->view($this->theme.'products/modal_view', $this->data);
    }

    function view($id = NULL)
    {
        $this->sma->checkPermissions('index');

        $pr_details = $this->products_model->getProductByID($id);
        if (!$id || !$pr_details) {
            $this->session->set_flashdata('error', lang('prduct_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->data['barcode'] = "<img src='" . admin_url('products/gen_barcode/' . $pr_details->code . '/' . $pr_details->barcode_symbology . '/40/0') . "' alt='" . $pr_details->code . "' class='pull-left' />";
        if ($pr_details->type == 'combo') {
            $this->data['combo_items'] = $this->products_model->getProductComboItems($id);
        }
        $this->data['product'] = $pr_details;
        $this->data['unit'] = $this->site->getUnitByID($pr_details->unit);
        $this->data['brand'] = $this->site->getBrandByID($pr_details->brand);
        $this->data['images'] = $this->products_model->getProductPhotos($id);
        $this->data['category'] = $this->site->getCategoryByID($pr_details->category_id);
        $this->data['subcategory'] = $pr_details->subcategory_id ? $this->site->getCategoryByID($pr_details->subcategory_id) : NULL;
        $this->data['tax_rate'] = $pr_details->tax_rate ? $this->site->getTaxRateByID($pr_details->tax_rate) : NULL;
        $this->data['popup_attributes'] = $this->popup_attributes;
        $this->data['warehouses'] = $this->products_model->getAllWarehousesWithPQ($id);
        $this->data['options'] = $this->products_model->getProductOptionsWithWH($id);
        $this->data['variants'] = $this->products_model->getProductOptions($id);
        $this->data['sold'] = $this->products_model->getSoldQty($id);
        $this->data['purchased'] = $this->products_model->getPurchasedQty($id);

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => $pr_details->name));
        $meta = array('page_title' => $pr_details->name, 'bc' => $bc);
        $this->page_construct('products/view', $meta, $this->data);
    }

    function pdf($id = NULL, $view = NULL)
    {
        $this->sma->checkPermissions('index');

        $pr_details = $this->products_model->getProductByID($id);
        if (!$id || !$pr_details) {
            $this->session->set_flashdata('error', lang('prduct_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->data['barcode'] = "<img src='" . admin_url('products/gen_barcode/' . $pr_details->code . '/' . $pr_details->barcode_symbology . '/40/0') . "' alt='" . $pr_details->code . "' class='pull-left' />";
        if ($pr_details->type == 'combo') {
            $this->data['combo_items'] = $this->products_model->getProductComboItems($id);
        }
        $this->data['product'] = $pr_details;
        $this->data['unit'] = $this->site->getUnitByID($pr_details->unit);
        $this->data['brand'] = $this->site->getBrandByID($pr_details->brand);
        $this->data['images'] = $this->products_model->getProductPhotos($id);
        $this->data['category'] = $this->site->getCategoryByID($pr_details->category_id);
        $this->data['subcategory'] = $pr_details->subcategory_id ? $this->site->getCategoryByID($pr_details->subcategory_id) : NULL;
        $this->data['tax_rate'] = $pr_details->tax_rate ? $this->site->getTaxRateByID($pr_details->tax_rate) : NULL;
        $this->data['popup_attributes'] = $this->popup_attributes;
        $this->data['warehouses'] = $this->products_model->getAllWarehousesWithPQ($id);
        $this->data['options'] = $this->products_model->getProductOptionsWithWH($id);
        $this->data['variants'] = $this->products_model->getProductOptions($id);

        $name = $pr_details->code . '_' . str_replace('/', '_', $pr_details->name) . ".pdf";
        if ($view) {
            $this->load->view($this->theme . 'products/pdf', $this->data);
        } else {
            $html = $this->load->view($this->theme . 'products/pdf', $this->data, TRUE);
            if (! $this->Settings->barcode_img) {
                $html = preg_replace("'\<\?xml(.*)\?\>'", '', $html);
            }
            $this->sma->generate_pdf($html, $name);
        }
    }

    function getSubCategories($category_id = NULL)
    {
        if ($rows = $this->products_model->getSubCategories($category_id)) {
            $data = json_encode($rows);
        } else {
            $data = false;
        }
        echo $data;
    }

    function product_actions($wh = NULL)
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('api_action', lang("api_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['check_value'])) {
                $temp_val = explode('-',$_POST['check_value']);

                if ($this->input->post('api_action') == 'delete') {

                    $this->sma->checkPermissions('delete');
                    foreach ($temp_val as $id) {
                        if ($id != '') {
                            $this->products_model->deleteProduct($id);
                        }
                    }
                    
                    $config_data = array();
                    $this->api_cache->api_front_main_category($config_data);   

                    $this->session->set_flashdata('message', $this->lang->line("products_deleted"));
                    redirect($_SERVER["HTTP_REFERER "]);
                } elseif ($this->input->post('api_action') == 'set_no_use') {
                    foreach ($temp_val as $id) {
                        if ($id != '') {
                            $config_data = array(
                                'table_name' => 'sma_products',
                                'id_name' => 'id',
                                'field_add_ons_name' => 'add_ons',
                                'selected_id' => $id,
                                'add_ons_title' => 'no_use',
                                'add_ons_value' => 'yes',
                            );
                            $this->site->api_update_add_ons_field($config_data);
                        }
                    }

                    $config_data = array();
                    $this->api_cache->api_front_main_category($config_data);   

                    $this->session->set_flashdata('message', lang("Product(s)_updated"));
                    redirect($_SERVER["HTTP_REFERER"]);
                } elseif ($this->input->post('api_action') == 'unset_no_use') {
                    foreach ($temp_val as $id) {
                        if ($id != '') {
                            $config_data = array(
                                'table_name' => 'sma_products',
                                'id_name' => 'id',
                                'field_add_ons_name' => 'add_ons',
                                'selected_id' => $id,
                                'add_ons_title' => 'no_use',
                                'add_ons_value' => 'no',
                            );
                            $this->site->api_update_add_ons_field($config_data);
                        }
                    }

                    $config_data = array();
                    $this->api_cache->api_front_main_category($config_data);   

                    $this->session->set_flashdata('message', lang("Product(s)_updated"));
                    redirect($_SERVER["HTTP_REFERER"]);                    
                } elseif ($this->input->post('api_action') == 'labels') {
                    foreach ($temp_val as $id) {
                        if ($id != '') {
                            $row = $this->products_model->getProductByID($id);
                            $selected_variants = false;
                            if ($variants = $this->products_model->getProductOptions($row->id)) {
                                foreach ($variants as $variant) {
                                    $selected_variants[$variant->id] = $variant->quantity > 0 ? 1 : 0;
                                }
                            }
                            $pr[$row->id] = array('id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'code' => $row->code, 'name' => $row->name, 'price' => $row->price, 'qty' => $row->quantity, 'variants' => $variants, 'selected_variants' => $selected_variants);
                        }
                    }

                    $this->data['items'] = isset($pr) ? json_encode($pr) : false;
                    $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
                    $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('print_barcodes')));
                    $meta = array('page_title' => lang('print_barcodes'), 'bc' => $bc);
                    $this->page_construct('products/print_barcodes', $meta, $this->data);

                } elseif ($this->input->post('api_action') == 'export_excel') {
                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle('Products');
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('name'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('code'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('barcode_symbology'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('brand'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('category_code'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('unit_code'));
                    $this->excel->getActiveSheet()->SetCellValue('G1', lang('sale').' '.lang('unit_code'));
                    $this->excel->getActiveSheet()->SetCellValue('H1', lang('purchase').' '.lang('unit_code'));
                    $this->excel->getActiveSheet()->SetCellValue('I1', lang('cost'));
                    $this->excel->getActiveSheet()->SetCellValue('J1', lang('price'));
                    $this->excel->getActiveSheet()->SetCellValue('K1', lang('alert_quantity'));
                    $this->excel->getActiveSheet()->SetCellValue('L1', lang('tax_rate'));
                    $this->excel->getActiveSheet()->SetCellValue('M1', lang('tax_method'));
                    $this->excel->getActiveSheet()->SetCellValue('N1', lang('image'));
                    $this->excel->getActiveSheet()->SetCellValue('O1', lang('subcategory_code'));
                    $this->excel->getActiveSheet()->SetCellValue('P1', lang('product_variants'));
                    $this->excel->getActiveSheet()->SetCellValue('Q1', lang('pcf1'));
                    $this->excel->getActiveSheet()->SetCellValue('R1', lang('pcf2'));
                    $this->excel->getActiveSheet()->SetCellValue('S1', lang('pcf3'));
                    $this->excel->getActiveSheet()->SetCellValue('T1', lang('pcf4'));
                    $this->excel->getActiveSheet()->SetCellValue('U1', lang('pcf5'));
                    $this->excel->getActiveSheet()->SetCellValue('V1', lang('pcf6'));
                    $this->excel->getActiveSheet()->SetCellValue('W1', lang('Qty_PP'));
                    $this->excel->getActiveSheet()->SetCellValue('X1', lang('Qty_SR'));
                    $this->excel->getActiveSheet()->SetCellValue('Y1', lang('Qty_SK'));


                    $row = 2;
                    foreach ($temp_val as $id) {
                        if ($id != '') {
                            $product = $this->products_model->getProductDetail($id);
                            $brand = $this->site->getBrandByID($product->brand);
                            $base_unit = $sale_unit = $purchase_unit = '';
                            if($units = $this->site->getUnitsByBUID($product->unit)) {
                                foreach($units as $u) {
                                    if ($u->id == $product->unit) {
                                        $base_unit = $u->code;
                                    }
                                    if ($u->id == $product->sale_unit) {
                                        $sale_unit = $u->code;
                                    }
                                    if ($u->id == $product->purchase_unit) {
                                        $purchase_unit = $u->code;
                                    }
                                }
                            }
                            $variants = $this->products_model->getProductOptions($id);
                            $product_variants = '';
                            if ($variants) {
                                foreach ($variants as $variant) {
                                    $product_variants .= trim($variant->name) . '|';
                                }
                            }
                            $quantity = $product->quantity;
                            // if ($wh) {
                            //     if($wh_qty = $this->products_model->getProductQuantity($id, $wh)) {
                            //         $quantity = $wh_qty['quantity'];
                            //     } else {
                            //         $quantity = 0;
                            //     }
                            // }
                            $config_data = array(
                                'table_name' => 'sma_products',
                                'select_table' => 'sma_products',
                                'translate' => '',
                                'select_condition' => "id = ".$id,
                            );
                            $select_data = $this->site->api_select_data_v2($config_data);                        


                            $this->excel->getActiveSheet()->SetCellValue('A' . $row, $product->name);
                            $this->excel->getActiveSheet()->SetCellValue('B' . $row, $product->code);
                            $this->excel->getActiveSheet()->SetCellValue('C' . $row, $product->barcode_symbology);
                            $this->excel->getActiveSheet()->SetCellValue('D' . $row, ($brand ? $brand->name : ''));
                            $this->excel->getActiveSheet()->SetCellValue('E' . $row, $product->category_code);
                            $this->excel->getActiveSheet()->SetCellValue('F' . $row, $base_unit);
                            $this->excel->getActiveSheet()->SetCellValue('G' . $row, $sale_unit);
                            $this->excel->getActiveSheet()->SetCellValue('H' . $row, $purchase_unit);
                            if ($this->Owner || $this->Admin || $this->session->userdata('show_cost')) {
                                $this->excel->getActiveSheet()->SetCellValue('I' . $row, $product->cost);
                            }
                            if ($this->Owner || $this->Admin || $this->session->userdata('show_price')) {
                                $this->excel->getActiveSheet()->SetCellValue('J' . $row, $product->price);
                            }
                            $this->excel->getActiveSheet()->SetCellValue('K' . $row, $product->alert_quantity);
                            $this->excel->getActiveSheet()->SetCellValue('L' . $row, $product->tax_rate_name);
                            $this->excel->getActiveSheet()->SetCellValue('M' . $row, $product->tax_method ? lang('exclusive') : lang('inclusive'));
                            $this->excel->getActiveSheet()->SetCellValue('N' . $row, $product->image);
                            $this->excel->getActiveSheet()->SetCellValue('O' . $row, $product->subcategory_code);
                            $this->excel->getActiveSheet()->SetCellValue('P' . $row, $product_variants);
                            $this->excel->getActiveSheet()->SetCellValue('Q' . $row, $product->cf1);
                            $this->excel->getActiveSheet()->SetCellValue('R' . $row, $product->cf2);
                            $this->excel->getActiveSheet()->SetCellValue('S' . $row, $product->cf3);
                            $this->excel->getActiveSheet()->SetCellValue('T' . $row, $product->cf4);
                            $this->excel->getActiveSheet()->SetCellValue('U' . $row, $product->cf5);
                            $this->excel->getActiveSheet()->SetCellValue('V' . $row, $product->cf6);
                            $this->excel->getActiveSheet()->SetCellValue('W' . $row, intval($select_data[0]['quantity']));
                            $this->excel->getActiveSheet()->SetCellValue('X' . $row, intval($select_data[0]['quantity_3']));
                            $this->excel->getActiveSheet()->SetCellValue('Y' . $row, intval($select_data[0]['quantity_4']));


                            $row++;
                        }
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
                    $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(40);
                    $this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(30);
                    $this->excel->getActiveSheet()->getColumnDimension('P')->setWidth(30);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'products_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                }
            } else {
                $this->session->set_flashdata('error', $this->lang->line("no_product_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'admin/products');
        }
    }

    public function delete_image($id = NULL)
    {
        $this->sma->checkPermissions('edit', true);
        if ($id && $this->input->is_ajax_request()) {
            header('Content-Type: application/json');
            $this->db->delete('product_photos', array('id' => $id));
            $this->sma->send_json(array('error' => 0, 'msg' => lang("image_deleted")));
        }
        $this->sma->send_json(array('error' => 1, 'msg' => lang("ajax_error")));
    }

    public function getSubUnits($unit_id)
    {
        // $unit = $this->site->getUnitByID($unit_id);
        // if ($units = $this->site->getUnitsByBUID($unit_id)) {
        //     array_push($units, $unit);
        // } else {
        //     $units = array($unit);
        // }
        $units = $this->site->getUnitsByBUID($unit_id);
        $this->sma->send_json($units);
    }

    public function qa_suggestions()
    {
        $term = $this->input->get('term', true);

        if (strlen($term) < 1 || !$term) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . admin_url('welcome') . "'; }, 10);</script>");
        }

        $analyzed = $this->sma->analyze_term($term);
        $sr = $analyzed['term'];
        $option_id = $analyzed['option_id'];

        //$rows = $this->products_model->getQASuggestions($sr);
        $config_data = array(
            'table_name' => 'sma_products',
            'select_table' => 'sma_products',
            'translate' => '',
            'select_condition' => "getTranslate(add_ons,'no_use','".f_separate."','".v_separate."') != 'yes' and (name like '%" . $sr . "%' or code like '%" . $sr . "%') order by name asc limit 20",
        );
        $select_data = $this->site->api_select_data_v2($config_data);
        for ($i=0;$i<count($select_data);$i++) {
            foreach(array_keys($select_data[$i]) as $key){
                $rows[$i]->{$key} = $select_data[$i][$key];
            }
        }        

        $temp_warhouse = $this->site->api_select_some_fields_with_where("
            *     
            "
            ,"sma_warehouses"
            ,"id > 0 order by id asc"
            ,"arr"
        );

        if ($rows) {
            foreach ($rows as $row) {
                $row->qty = 1;
                $options = $this->products_model->getProductOptions($row->id);
                $row->option = $option_id;

                if (is_array($options)) if (count($options) > 0)
                for ($i=0;$i<count($options);$i++) {
                    $temp = $this->site->api_select_some_fields_with_where("
                        quantity                        "
                        ,"sma_warehouses_products_variants"
                        ,"product_id = ".$row->id." and warehouse_id = ".$this->Settings->default_warehouse." and option_id = ".$options[$i]->id
                        ,"arr"
                    );
                    if ($temp[0]['quantity'] == '') $temp[0]['quantity'] = 0;
                    $options[$i]->name = $options[$i]->name.' ('.number_format($temp[0]['quantity'],2).')';
                }

                $condition = '';
                for ($i=0;$i<count($temp_warhouse);$i++) {
                    $condition .= ",getTranslate(add_ons,'quantity_".$temp_warhouse[$i]['id']."','".f_separate."','".v_separate."') as quantity_".$temp_warhouse[$i]['id'];
                }
                $temp = $this->site->api_select_some_fields_with_where("
                    * 
                    ".$condition
                    ,"sma_products"
                    ,"id = ".$row->id
                    ,"arr"
                );

                $temp_qty = $temp[0]['quantity'];

                $temp_select = '';
                $total_quantity = 0;
                for ($i=0;$i<count($temp_warhouse);$i++) {
                    $row->{'quantity_'.$temp_warhouse[$i]['id']} = number_format(floatval($temp[0]['quantity_'.$temp_warhouse[$i]['id']]),2);

                    if ($row->{'quantity_'.$temp_warhouse[$i]['id']} == '') 
                        $row->{'quantity_'.$temp_warhouse[$i]['id']} = 0;
                    if ($temp_warhouse[$i]['id'] == 1) {
                        $temp_select .=  '<strong>'.$temp_warhouse[$i]['code'].':</strong> '.number_format($temp_qty,2);
                        $row->{'quantity_'.$temp_warhouse[$i]['id']} = $temp_qty;
                        $total_quantity = $total_quantity + $temp_qty;
                    }
                    else {
                        $temp_select .=  '</br><strong>'.$temp_warhouse[$i]['code'].':</strong> '.number_format(floatval($row->{'quantity_'.$temp_warhouse[$i]['id']}),2);
                        $total_quantity = $total_quantity + floatval($row->{'quantity_'.$temp_warhouse[$i]['id']});
                    }
                }            
                $row->quantity = number_format($temp_qty,2);
                $row->total_quantity = number_format($total_quantity,2);

                $row->serial = '';
                $c = sha1(uniqid(mt_rand(), true));
                $milliseconds = round(microtime(true) * 1000);

                $pr[] = array('order' => $milliseconds, 'id' => $c, 'type_disabled' => '', 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")",
                    'row' => $row, 'options' => $options);
            }
            $this->sma->send_json($pr);
        } else {
            $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
        }
    }

    function adjustment_actions()
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if (!empty($_POST['check_value'])) {
            $temp_val = explode('-',$_POST['check_value']);
            if ($this->input->post('api_action') == 'delete') {

                $this->sma->checkPermissions('delete');
                foreach ($temp_val as $id) {
                    if ($id != '')
                        $this->products_model->deleteAdjustment($id);
                }
                $this->session->set_flashdata('message', $this->lang->line("adjustment_deleted"));
                redirect($_SERVER["HTTP_REFERER"]);

            } elseif ($this->input->post('api_action') == 'export_excel') {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle('quantity_adjustments');
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference_no'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('warehouse'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('created_by'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('note'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('items'));

                $row = 2;
                foreach ($temp_val as $id) {
                    if ($id != '') {
                        $adjustment = $this->products_model->getAdjustmentByID($id);
                        $created_by = $this->site->getUser($adjustment->created_by);
                        $warehouse = $this->site->getWarehouseByID($adjustment->warehouse_id);
                        $items = $this->products_model->getAdjustmentItems($id);
                        $products = '';
                        if ($items) {
                            foreach ($items as $item) {
                                $products .= $item->product_name.'('.$this->sma->formatQuantity($item->type == 'subtraction' ? -$item->quantity : $item->quantity).')'."\n";
                            }
                        }

                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($adjustment->date));
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $adjustment->reference_no);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $warehouse->name);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $created_by->first_name.' ' .$created_by->last_name);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $this->sma->decode_html($adjustment->note));
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, $products);
                        $row++;
                    }
                }

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(40);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $filename = 'quantity_adjustments_' . date('Y_m_d_H_i_s');
                $this->load->helper('excel');
                create_excel($this->excel, $filename);
            }
        } else {
            $this->session->set_flashdata('error', $this->lang->line("no_record_selected"));
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function stock_counts($warehouse_id = NULL)
    {
        $this->sma->checkPermissions('stock_count');

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        if ($this->Owner || $this->Admin || !$this->session->userdata('warehouse_id')) {
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['warehouse_id'] = $warehouse_id;
            $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : NULL;
        } else {
            $this->data['warehouses'] = NULL;
            $this->data['warehouse_id'] = $this->session->userdata('warehouse_id');
            $this->data['warehouse'] = $this->session->userdata('warehouse_id') ? $this->site->getWarehouseByID($this->session->userdata('warehouse_id')) : NULL;
        }

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('stock_counts')));
        $meta = array('page_title' => lang('stock_counts'), 'bc' => $bc);
        $this->page_construct('products/stock_counts', $meta, $this->data);
    }

    function getCounts($warehouse_id = NULL)
    {
        $this->sma->checkPermissions('stock_count', TRUE);

        if ((! $this->Owner || ! $this->Admin) && ! $warehouse_id) {
            $user = $this->site->getUser();
            $warehouse_id = $user->warehouse_id;
        }
        $detail_link = anchor('admin/products/view_count/$1', '<label class="label label-primary pointer">'.lang('details').'</label>', 'class="tip" title="'.lang('details').'" data-toggle="modal" data-target="#myModal"');

        $this->load->library('datatables');
        $this->datatables
            ->select("{$this->db->dbprefix('stock_counts')}.id as id, date, reference_no, {$this->db->dbprefix('warehouses')}.name as wh_name, type, brand_names, category_names, initial_file, final_file")
            ->from('stock_counts')
            ->join('warehouses', 'warehouses.id=stock_counts.warehouse_id', 'left');
        if ($warehouse_id) {
            $this->datatables->where('warehouse_id', $warehouse_id);
        }

        $this->datatables->add_column('Actions', '<div class="text-center">'.$detail_link.'</div>', "id");
        echo $this->datatables->generate();
    }

    function view_count($id)
    {
        $this->sma->checkPermissions('stock_count', TRUE);
        $stock_count = $this->products_model->getStouckCountByID($id);
        if ( ! $stock_count->finalized) {
            $this->sma->md('admin/products/finalize_count/'.$id);
        }

        $this->data['stock_count'] = $stock_count;
        $this->data['stock_count_items'] = $this->products_model->getStockCountItems($id);
        $this->data['warehouse'] = $this->site->getWarehouseByID($stock_count->warehouse_id);
        $this->data['adjustment'] = $this->products_model->getAdjustmentByCountID($id);
        $this->load->view($this->theme.'products/view_count', $this->data);
    }

    function count_stock($page = NULL)
    {
        $this->sma->checkPermissions('stock_count');
        $this->form_validation->set_rules('warehouse', lang("warehouse"), 'required');
        $this->form_validation->set_rules('type', lang("type"), 'required');

        if ($this->form_validation->run() == true) {

            $warehouse_id = $this->input->post('warehouse');
            $type = $this->input->post('type');
            $categories = $this->input->post('category') ? $this->input->post('category') : NULL;
            $brands = $this->input->post('brand') ? $this->input->post('brand') : NULL;
            $this->load->helper('string');
            $name = random_string('md5').'.csv';
            $products = $this->products_model->getStockCountProducts($warehouse_id, $type, $categories, $brands);
            $pr = 0; $rw = 0;
            foreach ($products as $product) {
                if ($variants = $this->products_model->getStockCountProductVariants($warehouse_id, $product->id)) {
                    foreach ($variants as $variant) {
                        $items[] = array(
                            'product_code' => $product->code,
                            'product_name' => $product->name,
                            'variant' => $variant->name,
                            'expected' => $variant->quantity,
                            'counted' => ''
                            );
                        $rw++;
                    }
                } else {
                    $items[] = array(
                        'product_code' => $product->code,
                        'product_name' => $product->name,
                        'variant' => '',
                        'expected' => $product->quantity,
                        'counted' => ''
                        );
                    $rw++;
                }
                $pr++;
            }
            if ( ! empty($items)) {
                $csv_file = fopen('./files/'.$name, 'w');
                fprintf($csv_file, chr(0xEF).chr(0xBB).chr(0xBF));
                fputcsv($csv_file, array(lang('product_code'), lang('product_name'), lang('variant'), lang('expected'), lang('counted')));
                foreach ($items as $item) {
                    fputcsv($csv_file, $item);
                }
                // file_put_contents('./files/'.$name, $csv_file);
                // fwrite($csv_file, $txt);
                fclose($csv_file);
            } else {
                $this->session->set_flashdata('error', lang('no_product_found'));
                redirect($_SERVER["HTTP_REFERER"]);
            }

            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld($this->input->post('date'));
            } else {
                $date = date('Y-m-d H:s:i');
            }
            $category_ids = '';
            $brand_ids = '';
            $category_names = '';
            $brand_names = '';
            if ($categories) {
                $r = 1; $s = sizeof($categories);
                foreach ($categories as $category_id) {
                    $category = $this->site->getCategoryByID($category_id);
                    if ($r == $s) {
                        $category_names .= $category->name;
                        $category_ids .= $category->id;
                    } else {
                        $category_names .= $category->name.', ';
                        $category_ids .= $category->id.', ';
                    }
                    $r++;
                }
            }
            if ($brands) {
                $r = 1; $s = sizeof($brands);
                foreach ($brands as $brand_id) {
                    $brand = $this->site->getBrandByID($brand_id);
                    if ($r == $s) {
                        $brand_names .= $brand->name;
                        $brand_ids .= $brand->id;
                    } else {
                        $brand_names .= $brand->name.', ';
                        $brand_ids .= $brand->id.', ';
                    }
                    $r++;
                }
            }
            $data = array(
                'date' => $date,
                'warehouse_id' => $warehouse_id,
                'reference_no' => $this->input->post('reference_no'),
                'type' => $type,
                'categories' => $category_ids,
                'category_names' => $category_names,
                'brands' => $brand_ids,
                'brand_names' => $brand_names,
                'initial_file' => $name,
                'products' => $pr,
                'rows' => $rw,
                'created_by' => $this->session->userdata('user_id')
            );

        }

        if ($this->form_validation->run() == true && $this->products_model->addStockCount($data)) {
            $this->session->set_flashdata('message', lang("stock_count_intiated"));
            admin_redirect('products/stock_counts');

        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['categories'] = $this->site->getAllCategories();
            $this->data['brands'] = $this->site->getAllBrands();
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('count_stock')));
            $meta = array('page_title' => lang('count_stock'), 'bc' => $bc);
            $this->page_construct('products/count_stock', $meta, $this->data);

        }

    }

    function finalize_count($id)
    {
        $this->sma->checkPermissions('stock_count');
        $stock_count = $this->products_model->getStouckCountByID($id);
        if ( ! $stock_count || $stock_count->finalized) {
            $this->session->set_flashdata('error', lang("stock_count_finalized"));
            admin_redirect('products/stock_counts');
        }

        $this->form_validation->set_rules('count_id', lang("count_stock"), 'required');

        if ($this->form_validation->run() == true) {

            if ($_FILES['csv_file']['size'] > 0) {
                $note = $this->sma->clear_tags($this->input->post('note'));
                $data = array(
                    'updated_by' => $this->session->userdata('user_id'),
                    'updated_at' => date('Y-m-d H:s:i'),
                    'note' => $note
                );

                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = 'csv';
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('csv_file')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                $csv = $this->upload->file_name;

                $arrResult = array();
                $handle = fopen($this->digital_upload_path . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 5000, ",")) !== FALSE) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);
                $keys = array('product_code', 'product_name', 'product_variant', 'expected', 'counted');
                $final = array();
                foreach ($arrResult as $key => $value) {
                    $final[] = array_combine($keys, $value);
                }
                // $this->sma->print_arrays($final);
                $rw = 2; $differences = 0; $matches = 0;
                foreach ($final as $pr) {
                    if ($product = $this->products_model->getProductByCode(trim($pr['product_code']))) {
                        $pr['counted'] = !empty($pr['counted']) ? $pr['counted'] : 0;
                        if ($pr['expected'] == $pr['counted']) {
                            $matches++;
                        } else {
                            $pr['stock_count_id'] = $id;
                            $pr['product_id'] = $product->id;
                            $pr['cost'] = $product->cost;
                            $pr['product_variant_id'] = empty($pr['product_variant']) ? NULL : $this->products_model->getProductVariantID($pr['product_id'], $pr['product_variant']);
                            $products[] = $pr;
                            $differences++;
                        }
                    } else {
                        $this->session->set_flashdata('error', lang('check_product_code') . ' (' . $pr['product_code'] . '). ' . lang('product_code_x_exist') . ' ' . lang('line_no') . ' ' . $rw);
                        admin_redirect('products/finalize_count/'.$id);
                    }
                    $rw++;
                }

                $data['final_file'] = $csv;
                $data['differences'] = $differences;
                $data['matches'] = $matches;
                $data['missing'] = $stock_count->rows-($rw-2);
                $data['finalized'] = 1;
            }

            // $this->sma->print_arrays($data, $products);
        }

        if ($this->form_validation->run() == true && $this->products_model->finalizeStockCount($id, $data, $products)) {
            $this->session->set_flashdata('message', lang("stock_count_finalized"));
            admin_redirect('products/stock_counts');
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['stock_count'] = $stock_count;
            $this->data['warehouse'] = $this->site->getWarehouseByID($stock_count->warehouse_id);
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => admin_url('products/stock_counts'), 'page' => lang('stock_counts')), array('link' => '#', 'page' => lang('finalize_count')));
            $meta = array('page_title' => lang('finalize_count'), 'bc' => $bc);
            $this->page_construct('products/finalize_count', $meta, $this->data);

        }

    }

    function upload_image_overide()
    {
        $this->load->helper('security');
        $this->form_validation->set_rules('product_image', lang("upload_file"), 'xss_clean');

        $this->load->library('upload');
        $this->load->library('image_lib');

        if ($_FILES['product_image']['name'][0] != "") {
            
            $config['upload_path'] = $this->upload_path;
            $config['allowed_types'] = $this->image_types;
            $config['max_size'] = $this->allowed_file_size;
            $config['max_width'] = $this->Settings->iwidth;
            $config['max_height'] = $this->Settings->iheight;
            $config['overwrite'] = TRUE;
            $config['max_filename'] = 25000000;
            $config['encrypt_name'] = FALSE;
            $files = $_FILES;
            $cpt = count($_FILES['product_image']['name']);
            for ($i = 0; $i < $cpt; $i++) {

                $_FILES['product_image']['name'] = trim($files['product_image']['name'][$i]);
                $_FILES['product_image']['type'] = $files['product_image']['type'][$i];
                $_FILES['product_image']['tmp_name'] = $files['product_image']['tmp_name'][$i];
                $_FILES['product_image']['error'] = $files['product_image']['error'][$i];
                $_FILES['product_image']['size'] = $files['product_image']['size'][$i];

                if ($_FILES['product_image']['size'] > 0) {
                    $config['upload_path'] = $this->upload_path;
                    $config['allowed_types'] = $this->image_types;
                    $config['max_size'] = $this->allowed_file_size;
                    $config['width'] = $this->Settings->iwidth;
                    $config['height'] = $this->Settings->iheight;
                    $config['overwrite'] = TRUE;
                    $config['max_filename'] = 25000000;
                    $config['encrypt_name'] = FALSE;
                    
                    $this->upload->initialize($config);
                    
                    if (!$this->upload->do_upload('product_image')) {
                        //$error = $this->upload->display_errors();
                        //$this->session->set_flashdata('error', $error);
                        //admin_redirect("products/import_csv");
                    }
                    
                    $photo = $this->upload->file_name;
                    $data['image'] = $photo;
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = $this->upload_path . trim($photo);
                    $config['new_image'] = $this->thumbs_path . trim($photo);
                    $config['maintain_ratio'] = TRUE;
                    $config['width'] = $this->Settings->twidth;
                    $config['height'] = $this->Settings->twidth;
                    $this->image_lib->clear();
                    $this->image_lib->initialize($config);
                    
                    
                    if (!$this->image_lib->resize()) {
                        echo $this->image_lib->display_errors();
                    }
                    

                    /*
                    if ($this->Settings->watermark) {
                        $this->image_lib->clear();
                        $wm['source_image'] = $this->upload_path . $photo;
                        $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                        $wm['wm_type'] = 'text';
                        $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                        $wm['quality'] = '100';
                        $wm['wm_font_size'] = '16';
                        $wm['wm_font_color'] = '999999';
                        $wm['wm_shadow_color'] = 'CCCCCC';
                        $wm['wm_vrt_alignment'] = 'top';
                        $wm['wm_hor_alignment'] = 'left';
                        $wm['wm_padding'] = '10';
                        $this->image_lib->initialize($wm);
                        $this->image_lib->watermark();
                    }
                    */
                    $this->image_lib->clear();
                    $config = NULL;
                }

            }
            $config = NULL;
            $this->session->set_flashdata('message', lang("Successfully_upload_images"));
            admin_redirect("products/import_csv");
        }
        else {
            $this->session->set_flashdata('error', lang("No_image_selected"));
            admin_redirect("products/import_csv");
        }
    }

    function hide_customer_list_add()
    {
        if ($this->Owner || $this->Admin) {
            foreach ($_GET as $name => $value) {
                ${$name} = $value;
            }            
            if ($product_id > 0) {
                $temp = $this->site->api_select_some_fields_with_where("
                    getTranslate(add_ons,'hide_customer_list','".f_separate."','".v_separate."') as hide_customer_list,
                    getTranslate(add_ons,'hide_customer_list_selected','".f_separate."','".v_separate."') as hide_customer_list_selected 
                    "
                    ,"sma_products"
                    ,"id = ".$product_id
                    ,"arr"
                );            
                $b = 0;
                $temp_hide_customer_list = $temp[0]['hide_customer_list'];
                $temp_hide_customer_list_selected = $temp[0]['hide_customer_list_selected'];
                $temp2 = $temp[0]['hide_customer_list'];
                $temp2 = explode('-',$temp2);
                if (is_array($temp2))
                for ($i=0;$i<count($temp2);$i++) {
                    if ($temp2[$i] == $customer_id) {
                        $b = 1;
                        break;
                    }
                }
                $temp_result = '';
                if ($b == 0) {
                    $temp_hide_customer_list = $temp[0]['hide_customer_list'].'-'.$customer_id;
                    $config_data = array(
                        'table_name' => 'sma_products',
                        'id_name' => 'id',
                        'field_add_ons_name' => 'add_ons',
                        'selected_id' => $product_id,
                        'add_ons_title' => 'hide_customer_list',
                        'add_ons_value' => $temp_hide_customer_list,                    
                    );
                    $this->site->api_update_add_ons_field($config_data);

                    $temp_hide_customer_list_selected = $temp[0]['hide_customer_list_selected'].'-'.$customer_id;
                    $config_data = array(
                        'table_name' => 'sma_products',
                        'id_name' => 'id',
                        'field_add_ons_name' => 'add_ons',
                        'selected_id' => $product_id,
                        'add_ons_title' => 'hide_customer_list_selected',
                        'add_ons_value' => $temp_hide_customer_list_selected,                    
                    );
                    $this->site->api_update_add_ons_field($config_data);

                    $temp_result = 'success';
                }
                
                $temp3[1] = 'api-ajax-request-multiple-result-split';  
                $temp3[1] .= $temp_result;
                $temp3[2] = 'api-ajax-request-multiple-result-split';
                $temp3[2] .= $temp_hide_customer_list;
                $temp3[3] = 'api-ajax-request-multiple-result-split';  
                $temp3[3] .= $temp_hide_customer_list_selected;
                $result = $temp3[1].$temp3[2];
            }
            echo $result;
        }  
        else
            echo '<script>window.location = "'.base_url().'";</script>';
    }        

    function hide_customer_list_reload()
    {
        if ($this->Owner || $this->Admin) {
            foreach ($_GET as $name => $value) {
                ${$name} = $value;
            }            
            if ($product_id > 0) {
                $temp_selected = $this->site->api_select_some_fields_with_where("
                    getTranslate(add_ons,'hide_customer_list','".f_separate."','".v_separate."') as hide_customer_list, 
                    getTranslate(add_ons,'hide_customer_list_selected','".f_separate."','".v_separate."') as hide_customer_list_selected
                    "
                    ,"sma_products"
                    ,"id = ".$product_id
                    ,"arr"
                );            
                $temp_hide_customer_list = $temp_selected[0]['hide_customer_list'];
                $temp_hide_customer_list_selected = $temp_selected[0]['hide_customer_list_selected'];
                $temp = explode("-",$temp_selected[0]['hide_customer_list']);
                $temp_2 = explode("-",$temp_selected[0]['hide_customer_list_selected']);
                $customers = $this->site->getCustomers();    
                $k = 1;
                $select_value = array();
                foreach ($customers as $customer) {
                    if (($k%2) != 0) $class_record = "#fff;"; else $class_record = "#eee;";
            		$temp_checkbox = "";
                    $b = 0;
                    if (is_array($temp))
            		for ($j=0;$j<count($temp);$j++) {
                        if ($temp[$j] != '') {
                            if ($temp[$j] == $customer->id) {
                                $b = 1;
                                break;
                            }
                        }
            		}
                    if ($b == 1) {
                        $temp_checkbox = '';
                		for ($j=0;$j<count($temp_2);$j++) {
                            if ($temp_2[$j] != '') {
                                if ($temp_2[$j] == $customer->id) {
                                    $temp_checkbox = 'checked="checked"';
                                    break;
                                }
                            }
                		}                        
                		array_push($select_value, '
                			<div class="col-md-12 api_padding_top_5 api_padding_bottom_5" style="background-color:'.$class_record.'">                
                				<div class="api_float_left">
                					<input class="skip" type="checkbox" name="hide_customer_list" id="hide_customer_list_'.$customer->id.'" value="'.$customer->id.'" '.$temp_checkbox.' onclick="api_setValueCheckbox(\'hide_customer_list\',\'temp_hide_customer_list_selected\',\'api_form_product\',\'-\'); hide_customer_list_selected_update(\''.$product_id.'\',\'temp_hide_customer_list_selected\');" style="width:20px;"/>
                				</div>
                				<div class="api_float_left api_padding_left_5 api_padding_top_2">'.$customer->name.'</div>
                                <div class="api_float_right ">
                                    <i class="fa fa-minus-circle api_pointer api_padding_top_3" id="addIcon" onclick="hide_customer_list_remove('.$product_id.','.$customer->id.')" style="font-size: 1.2em;"></i>
                                </div>
                			</div>	
                		');
                        $k++;
                    }
                }
                $temp_result = '';
                foreach ($select_value as $value) {
                    $temp_result .= $value;
                }
                
                $temp3[1] = 'api-ajax-request-multiple-result-split';  
                $temp3[1] .= $temp_result;
                $temp3[2] = 'api-ajax-request-multiple-result-split';
                $temp3[2] .= $temp_hide_customer_list;
                $temp3[3] = 'api-ajax-request-multiple-result-split';
                $temp3[3] .= $temp_hide_customer_list_selected;
                
                $result = $temp3[1].$temp3[2].$temp3[3];
            }
            echo $result;
        }  
        else
            echo '<script>window.location = "'.base_url().'";</script>';
    }        
    function hide_customer_list_reload_add()
    {
        if ($this->Owner || $this->Admin) {
            foreach ($_GET as $name => $value) {
                ${$name} = $value;
            }            
            $temp_selected[0]['hide_customer_list'] = $hide_customer_list;
            $temp_selected[0]['hide_customer_list_selected'] = $hide_customer_list_selected;
            $temp_hide_customer_list = $temp_selected[0]['hide_customer_list'];
            $temp_hide_customer_list_selected = $temp_selected[0]['hide_customer_list_selected'];
            $temp = explode("-",$temp_selected[0]['hide_customer_list']);
            $temp_2 = explode("-",$temp_selected[0]['hide_customer_list_selected']);
            $customers = $this->site->getCustomers();    
            $k = 1;
            $select_value = array();
            foreach ($customers as $customer) {
                if (($k%2) != 0) $class_record = "#fff;"; else $class_record = "#eee;";
        		$temp_checkbox = "";
                $b = 0;
                if (is_array($temp))
        		for ($j=0;$j<count($temp);$j++) {
                    if ($temp[$j] != '') {
                        if ($temp[$j] == $customer->id) {
                            $b = 1;
                            break;
                        }
                    }
        		}
                if ($b == 1) {
                    $temp_checkbox = '';
            		for ($j=0;$j<count($temp_2);$j++) {
                        if ($temp_2[$j] != '') {
                            if ($temp_2[$j] == $customer->id) {
                                $temp_checkbox = 'checked="checked"';
                                break;
                            }
                        }
            		}                        
            		array_push($select_value, '
            			<div class="col-md-12 api_padding_top_5 api_padding_bottom_5" style="background-color:'.$class_record.'">                
            				<div class="api_float_left">
            					<input class="skip" type="checkbox" name="hide_customer_list" id="hide_customer_list_'.$customer->id.'" value="'.$customer->id.'" '.$temp_checkbox.' onclick="api_setValueCheckbox(\'hide_customer_list\',\'add_ons_hide_customer_list_selected\',\'api_form_product\',\'-\'); " style="width:20px;"/>
            				</div>
            				<div class="api_float_left api_padding_left_5 api_padding_top_2">'.$customer->name.'</div>
                            <div class="api_float_right ">
                                <i class="fa fa-minus-circle api_pointer api_padding_top_3" id="addIcon" onclick="hide_customer_list_remove('.$customer->id.')" style="font-size: 1.2em;"></i>
                            </div>
            			</div>	
            		');
                    $k++;
                }
                $temp_result = '';
                foreach ($select_value as $value) {
                    $temp_result .= $value;
                }
                
                $temp3[1] = 'api-ajax-request-multiple-result-split';  
                $temp3[1] .= $temp_result;
                $temp3[2] = 'api-ajax-request-multiple-result-split';
                $temp3[2] .= $temp_hide_customer_list;
                $temp3[3] = 'api-ajax-request-multiple-result-split';
                $temp3[3] .= $temp_hide_customer_list_selected;
                
                $result = $temp3[1].$temp3[2].$temp3[3];
            }
            echo $result;
        }  
        else
            echo '<script>window.location = "'.base_url().'";</script>';
    }        
    function hide_customer_list_remove()
    {
        if ($this->Owner || $this->Admin) {
            foreach ($_GET as $name => $value) {
                ${$name} = $value;
            }            
            if ($product_id > 0) {
                $temp = $this->site->api_select_some_fields_with_where("
                    getTranslate(add_ons,'hide_customer_list','".f_separate."','".v_separate."') as hide_customer_list,
                    getTranslate(add_ons,'hide_customer_list_selected','".f_separate."','".v_separate."') as hide_customer_list_selected 
                    "
                    ,"sma_products"
                    ,"id = ".$product_id
                    ,"arr"
                );            
                $temp_2 = explode("-",$temp[0]['hide_customer_list']);
                $temp_3 = explode("-",$temp[0]['hide_customer_list_selected']);
                $temp_4 = '';
        		for ($j=0;$j<count($temp_2);$j++) {
                    if ($temp_2[$j] != '') {
                        if ($temp_2[$j] != $customer_id) {
                            $temp_4 .= '-'.$temp_2[$j];
                        }
                    }
        		}
                $temp_5 = '';
        		for ($j=0;$j<count($temp_3);$j++) {
                    if ($temp_3[$j] != '') {
                        if ($temp_3[$j] != $customer_id) {
                            $temp_5 .= '-'.$temp_3[$j];
                        }
                    }
        		}

                $config_data = array(
                    'table_name' => 'sma_products',
                    'id_name' => 'id',
                    'field_add_ons_name' => 'add_ons',
                    'selected_id' => $product_id,
                    'add_ons_title' => 'hide_customer_list',
                    'add_ons_value' => $temp_4,                    
                );
                $this->site->api_update_add_ons_field($config_data);

                $config_data = array(
                    'table_name' => 'sma_products',
                    'id_name' => 'id',
                    'field_add_ons_name' => 'add_ons',
                    'selected_id' => $product_id,
                    'add_ons_title' => 'hide_customer_list_selected',
                    'add_ons_value' => $temp_5,                    
                );
                $this->site->api_update_add_ons_field($config_data);

                $temp_result = 'success';

                $temp3[1] = 'api-ajax-request-multiple-result-split';  
                $temp3[1] .= $temp_result;
                $temp3[2] = 'api-ajax-request-multiple-result-split';
                $result = $temp3[1].$temp3[2];
            }
            echo $result;
        }  
        else
            echo '<script>window.location = "'.base_url().'";</script>';
    }        
    function hide_customer_list_selected_update()
    {
        if ($this->Owner || $this->Admin) {
            foreach ($_GET as $name => $value) {
                ${$name} = $value;
            }            
            if ($product_id > 0) {
                $config_data = array(
                    'table_name' => 'sma_products',
                    'id_name' => 'id',
                    'field_add_ons_name' => 'add_ons',
                    'selected_id' => $product_id,
                    'add_ons_title' => 'hide_customer_list_selected',
                    'add_ons_value' => $value,
                );
                $this->site->api_update_add_ons_field($config_data);

                $temp_result = 'success';

                $temp3[1] = 'api-ajax-request-multiple-result-split';  
                $temp3[1] .= $temp_result;
                $temp3[2] = 'api-ajax-request-multiple-result-split';
                $result = $temp3[1].$temp3[2];
            }
            echo $result;
        }  
        else
            echo '<script>window.location = "'.base_url().'";</script>';
    }        
    function hide_customer_list_check_all()
    {
        if ($this->Owner || $this->Admin) {
            foreach ($_GET as $name => $value) {
                ${$name} = $value;
            }            
            if ($product_id > 0) {
                if ($checked == 'true') {
                    $temp = $this->site->api_select_some_fields_with_where("
                        getTranslate(add_ons,'hide_customer_list','".f_separate."','".v_separate."') as hide_customer_list,
                        getTranslate(add_ons,'hide_customer_list_selected','".f_separate."','".v_separate."') as hide_customer_list_selected 
                        "
                        ,"sma_products"
                        ,"id = ".$product_id
                        ,"arr"
                    );                    
                    $value = $temp[0]['hide_customer_list'];
                }
                else
                    $value = '';
                $config_data = array(
                    'table_name' => 'sma_products',
                    'id_name' => 'id',
                    'field_add_ons_name' => 'add_ons',
                    'selected_id' => $product_id,
                    'add_ons_title' => 'hide_customer_list_selected',
                    'add_ons_value' => $value,
                );
                $this->site->api_update_add_ons_field($config_data);

                $temp_result = 'success';

                $temp3[1] = 'api-ajax-request-multiple-result-split';  
                $temp3[1] .= $temp_result;
                $temp3[2] = 'api-ajax-request-multiple-result-split';
                $result = $temp3[1].$temp3[2];
            }
            echo $result;
        }  
        else
            echo '<script>window.location = "'.base_url().'";</script>';
    }        

    function show_customer_list_add()
    {
        if ($this->Owner || $this->Admin) {
            foreach ($_GET as $name => $value) {
                ${$name} = $value;
            }            
            if ($product_id > 0) {
                $temp = $this->site->api_select_some_fields_with_where("
                    getTranslate(add_ons,'show_customer_list','".f_separate."','".v_separate."') as show_customer_list,
                    getTranslate(add_ons,'show_customer_list_selected','".f_separate."','".v_separate."') as show_customer_list_selected 
                    "
                    ,"sma_products"
                    ,"id = ".$product_id
                    ,"arr"
                );            
                $b = 0;
                $temp_show_customer_list = $temp[0]['show_customer_list'];
                $temp_show_customer_list_selected = $temp[0]['show_customer_list_selected'];
                $temp2 = $temp[0]['show_customer_list'];
                $temp2 = explode('-',$temp2);
                if (is_array($temp2))
                for ($i=0;$i<count($temp2);$i++) {
                    if ($temp2[$i] == $customer_id) {
                        $b = 1;
                        break;
                    }
                }
                $temp_result = '';
                if ($b == 0) {
                    $temp_show_customer_list = $temp[0]['show_customer_list'].'-'.$customer_id;
                    $config_data = array(
                        'table_name' => 'sma_products',
                        'id_name' => 'id',
                        'field_add_ons_name' => 'add_ons',
                        'selected_id' => $product_id,
                        'add_ons_title' => 'show_customer_list',
                        'add_ons_value' => $temp_show_customer_list,                    
                    );
                    $this->site->api_update_add_ons_field($config_data);

                    $temp_show_customer_list_selected = $temp[0]['show_customer_list_selected'].'-'.$customer_id;
                    $config_data = array(
                        'table_name' => 'sma_products',
                        'id_name' => 'id',
                        'field_add_ons_name' => 'add_ons',
                        'selected_id' => $product_id,
                        'add_ons_title' => 'show_customer_list_selected',
                        'add_ons_value' => $temp_show_customer_list_selected,                    
                    );
                    $this->site->api_update_add_ons_field($config_data);

                    $temp_result = 'success';
                }
                
                $temp3[1] = 'api-ajax-request-multiple-result-split';  
                $temp3[1] .= $temp_result;
                $temp3[2] = 'api-ajax-request-multiple-result-split';
                $temp3[2] .= $temp_show_customer_list;
                $temp3[3] = 'api-ajax-request-multiple-result-split';  
                $temp3[3] .= $temp_show_customer_list_selected;
                $result = $temp3[1].$temp3[2];
            }
            echo $result;
        }  
        else
            echo '<script>window.location = "'.base_url().'";</script>';
    }        

    function show_customer_list_reload()
    {
        if ($this->Owner || $this->Admin) {
            foreach ($_GET as $name => $value) {
                ${$name} = $value;
            }            
            if ($product_id > 0) {
                $temp_selected = $this->site->api_select_some_fields_with_where("
                    getTranslate(add_ons,'show_customer_list','".f_separate."','".v_separate."') as show_customer_list, 
                    getTranslate(add_ons,'show_customer_list_selected','".f_separate."','".v_separate."') as show_customer_list_selected
                    "
                    ,"sma_products"
                    ,"id = ".$product_id
                    ,"arr"
                );            
                $temp_show_customer_list = $temp_selected[0]['show_customer_list'];
                $temp_show_customer_list_selected = $temp_selected[0]['show_customer_list_selected'];
                $temp = explode("-",$temp_selected[0]['show_customer_list']);
                $temp_2 = explode("-",$temp_selected[0]['show_customer_list_selected']);
                $customers = $this->site->getCustomers();    
                $k = 1;
                $select_value = array();
                foreach ($customers as $customer) {
                    if (($k%2) != 0) $class_record = "#fff;"; else $class_record = "#eee;";
                    $temp_checkbox = "";
                    $b = 0;
                    if (is_array($temp))
                    for ($j=0;$j<count($temp);$j++) {
                        if ($temp[$j] != '') {
                            if ($temp[$j] == $customer->id) {
                                $b = 1;
                                break;
                            }
                        }
                    }
                    if ($b == 1) {
                        $temp_checkbox = '';
                        for ($j=0;$j<count($temp_2);$j++) {
                            if ($temp_2[$j] != '') {
                                if ($temp_2[$j] == $customer->id) {
                                    $temp_checkbox = 'checked="checked"';
                                    break;
                                }
                            }
                        }                        
                        array_push($select_value, '
                            <div class="col-md-12 api_padding_top_5 api_padding_bottom_5" style="background-color:'.$class_record.'">                
                                <div class="api_float_left">
                                    <input class="skip" type="checkbox" name="show_customer_list" id="show_customer_list_'.$customer->id.'" value="'.$customer->id.'" '.$temp_checkbox.' onclick="api_setValueCheckbox(\'show_customer_list\',\'temp_show_customer_list_selected\',\'api_form_product\',\'-\'); show_customer_list_selected_update(\''.$product_id.'\',\'temp_show_customer_list_selected\');" style="width:20px;"/>
                                </div>
                                <div class="api_float_left api_padding_left_5 api_padding_top_2">'.$customer->name.'</div>
                                <div class="api_float_right ">
                                    <i class="fa fa-minus-circle api_pointer api_padding_top_3" id="addIcon" onclick="show_customer_list_remove('.$product_id.','.$customer->id.')" style="font-size: 1.2em;"></i>
                                </div>
                            </div>  
                        ');
                        $k++;
                    }
                }
                $temp_result = '';
                foreach ($select_value as $value) {
                    $temp_result .= $value;
                }
                
                $temp3[1] = 'api-ajax-request-multiple-result-split';  
                $temp3[1] .= $temp_result;
                $temp3[2] = 'api-ajax-request-multiple-result-split';
                $temp3[2] .= $temp_show_customer_list;
                $temp3[3] = 'api-ajax-request-multiple-result-split';
                $temp3[3] .= $temp_show_customer_list_selected;
                
                $result = $temp3[1].$temp3[2].$temp3[3];
            }
            echo $result;
        }  
        else
            echo '<script>window.location = "'.base_url().'";</script>';
    }        
    function show_customer_list_reload_add()
    {
        if ($this->Owner || $this->Admin) {
            foreach ($_GET as $name => $value) {
                ${$name} = $value;
            }            
            $temp_selected[0]['show_customer_list'] = $show_customer_list;
            $temp_selected[0]['show_customer_list_selected'] = $show_customer_list_selected;
            $temp_show_customer_list = $temp_selected[0]['show_customer_list'];
            $temp_show_customer_list_selected = $temp_selected[0]['show_customer_list_selected'];
            $temp = explode("-",$temp_selected[0]['show_customer_list']);
            $temp_2 = explode("-",$temp_selected[0]['show_customer_list_selected']);
            $customers = $this->site->getCustomers();    
            $k = 1;
            $select_value = array();
            foreach ($customers as $customer) {
                if (($k%2) != 0) $class_record = "#fff;"; else $class_record = "#eee;";
                $temp_checkbox = "";
                $b = 0;
                if (is_array($temp))
                for ($j=0;$j<count($temp);$j++) {
                    if ($temp[$j] != '') {
                        if ($temp[$j] == $customer->id) {
                            $b = 1;
                            break;
                        }
                    }
                }
                if ($b == 1) {
                    $temp_checkbox = '';
                    for ($j=0;$j<count($temp_2);$j++) {
                        if ($temp_2[$j] != '') {
                            if ($temp_2[$j] == $customer->id) {
                                $temp_checkbox = 'checked="checked"';
                                break;
                            }
                        }
                    }                        
                    array_push($select_value, '
                        <div class="col-md-12 api_padding_top_5 api_padding_bottom_5" style="background-color:'.$class_record.'">                
                            <div class="api_float_left">
                                <input class="skip" type="checkbox" name="show_customer_list" id="show_customer_list_'.$customer->id.'" value="'.$customer->id.'" '.$temp_checkbox.' onclick="api_setValueCheckbox(\'show_customer_list\',\'add_ons_show_customer_list_selected\',\'api_form_product\',\'-\'); " style="width:20px;"/>
                            </div>
                            <div class="api_float_left api_padding_left_5 api_padding_top_2">'.$customer->name.'</div>
                            <div class="api_float_right ">
                                <i class="fa fa-minus-circle api_pointer api_padding_top_3" id="addIcon" onclick="show_customer_list_remove('.$customer->id.')" style="font-size: 1.2em;"></i>
                            </div>
                        </div>  
                    ');
                    $k++;
                }
                $temp_result = '';
                foreach ($select_value as $value) {
                    $temp_result .= $value;
                }
                
                $temp3[1] = 'api-ajax-request-multiple-result-split';  
                $temp3[1] .= $temp_result;
                $temp3[2] = 'api-ajax-request-multiple-result-split';
                $temp3[2] .= $temp_show_customer_list;
                $temp3[3] = 'api-ajax-request-multiple-result-split';
                $temp3[3] .= $temp_show_customer_list_selected;
                
                $result = $temp3[1].$temp3[2].$temp3[3];
            }
            echo $result;
        }  
        else
            echo '<script>window.location = "'.base_url().'";</script>';
    }        
    function show_customer_list_remove()
    {
        if ($this->Owner || $this->Admin) {
            foreach ($_GET as $name => $value) {
                ${$name} = $value;
            }            
            if ($product_id > 0) {
                $temp = $this->site->api_select_some_fields_with_where("
                    getTranslate(add_ons,'show_customer_list','".f_separate."','".v_separate."') as show_customer_list,
                    getTranslate(add_ons,'show_customer_list_selected','".f_separate."','".v_separate."') as show_customer_list_selected 
                    "
                    ,"sma_products"
                    ,"id = ".$product_id
                    ,"arr"
                );            
                $temp_2 = explode("-",$temp[0]['show_customer_list']);
                $temp_3 = explode("-",$temp[0]['show_customer_list_selected']);
                $temp_4 = '';
                for ($j=0;$j<count($temp_2);$j++) {
                    if ($temp_2[$j] != '') {
                        if ($temp_2[$j] != $customer_id) {
                            $temp_4 .= '-'.$temp_2[$j];
                        }
                    }
                }
                $temp_5 = '';
                for ($j=0;$j<count($temp_3);$j++) {
                    if ($temp_3[$j] != '') {
                        if ($temp_3[$j] != $customer_id) {
                            $temp_5 .= '-'.$temp_3[$j];
                        }
                    }
                }

                $config_data = array(
                    'table_name' => 'sma_products',
                    'id_name' => 'id',
                    'field_add_ons_name' => 'add_ons',
                    'selected_id' => $product_id,
                    'add_ons_title' => 'show_customer_list',
                    'add_ons_value' => $temp_4,                    
                );
                $this->site->api_update_add_ons_field($config_data);

                $config_data = array(
                    'table_name' => 'sma_products',
                    'id_name' => 'id',
                    'field_add_ons_name' => 'add_ons',
                    'selected_id' => $product_id,
                    'add_ons_title' => 'show_customer_list_selected',
                    'add_ons_value' => $temp_5,                    
                );
                $this->site->api_update_add_ons_field($config_data);

                $temp_result = 'success';

                $temp3[1] = 'api-ajax-request-multiple-result-split';  
                $temp3[1] .= $temp_result;
                $temp3[2] = 'api-ajax-request-multiple-result-split';
                $result = $temp3[1].$temp3[2];
            }
            echo $result;
        }  
        else
            echo '<script>window.location = "'.base_url().'";</script>';
    }        
    function show_customer_list_selected_update()
    {
        if ($this->Owner || $this->Admin) {
            foreach ($_GET as $name => $value) {
                ${$name} = $value;
            }            
            if ($product_id > 0) {
                $config_data = array(
                    'table_name' => 'sma_products',
                    'id_name' => 'id',
                    'field_add_ons_name' => 'add_ons',
                    'selected_id' => $product_id,
                    'add_ons_title' => 'show_customer_list_selected',
                    'add_ons_value' => $value,
                );
                $this->site->api_update_add_ons_field($config_data);

                $temp_result = 'success';

                $temp3[1] = 'api-ajax-request-multiple-result-split';  
                $temp3[1] .= $temp_result;
                $temp3[2] = 'api-ajax-request-multiple-result-split';
                $result = $temp3[1].$temp3[2];
            }
            echo $result;
        }  
        else
            echo '<script>window.location = "'.base_url().'";</script>';
    }        
    function show_customer_list_check_all()
    {
        if ($this->Owner || $this->Admin) {
            foreach ($_GET as $name => $value) {
                ${$name} = $value;
            }            
            if ($product_id > 0) {
                if ($checked == 'true') {
                    $temp = $this->site->api_select_some_fields_with_where("
                        getTranslate(add_ons,'show_customer_list','".f_separate."','".v_separate."') as show_customer_list,
                        getTranslate(add_ons,'show_customer_list_selected','".f_separate."','".v_separate."') as show_customer_list_selected 
                        "
                        ,"sma_products"
                        ,"id = ".$product_id
                        ,"arr"
                    );                    
                    $value = $temp[0]['show_customer_list'];
                }
                else
                    $value = '';
                $config_data = array(
                    'table_name' => 'sma_products',
                    'id_name' => 'id',
                    'field_add_ons_name' => 'add_ons',
                    'selected_id' => $product_id,
                    'add_ons_title' => 'show_customer_list_selected',
                    'add_ons_value' => $value,
                );
                $this->site->api_update_add_ons_field($config_data);

                $temp_result = 'success';

                $temp3[1] = 'api-ajax-request-multiple-result-split';  
                $temp3[1] .= $temp_result;
                $temp3[2] = 'api-ajax-request-multiple-result-split';
                $result = $temp3[1].$temp3[2];
            }
            echo $result;
        }  
        else
            echo '<script>window.location = "'.base_url().'";</script>';
    }        

    public function ajax_update_hide() {
        if ($this->Owner || $this->Admin) {
            foreach ($_GET as $name => $value) {
                ${$name} = $value;
            }
            if ($field_id > 0) {
                $config_data = array(
                    'table_name' => 'sma_products',
                    'select_table' => 'sma_products',
                    'select_condition' => "id = ".$field_id,
                );
                $select_data = $this->site->api_select_data_v2($config_data);
                if ($select_data[0]['hide'] == 0)
                    $value = 1;
                else
                    $value = 0;

                $temp = array(
                    'hide' => $value
                );
                $this->db->update('sma_products', $temp,'id = '.$field_id);

                if ($value == 0) {       
                    $temp_display = '
                        <a href="javascript:void(0);" onclick="
                var postData = {
                    \'ajax_file\' : \''.admin_url('products/ajax_update_hide').'\',
                    \'field_id\' : \''.$field_id.'\',
                    \'element_id\' : \'api_page_admin_product_hide_'.$field_id.'\',
                };
                ajax_update_hide(postData);  
                        ">
                            <span class="label label-success">
                            <i class="fa fa-check"></i> No</span>
                        </a>
                    ';
                }
                else {
                    $temp_display = '
                        <a href="javascript:void(0);" onclick="
                var postData = {
                    \'ajax_file\' : \''.admin_url('products/ajax_update_hide').'\',
                    \'field_id\' : \''.$field_id.'\',
                    \'element_id\' : \'api_page_admin_product_hide_'.$field_id.'\',
                };
                ajax_update_hide(postData);  
                        ">
                            <span class="label label-danger">
                            <i class="fa fa-times"></i> Yes</span>
                        </a>       
                    ';
                }

            }


            $temp3[1] = 'api-ajax-request-multiple-result-split';  
            $temp3[1] .= $value;
            $temp3[2] = 'api-ajax-request-multiple-result-split';            
            $temp3[2] .= $temp_display;
            $temp3[3] = 'api-ajax-request-multiple-result-split';            
            $result = $temp3[1].$temp3[2].$temp3[3];
            echo $result;            
        }
        else
            echo '<script>window.location = "'.base_url().'admin/login";</script>';        
    }

    public function ajax_update_featured() {
        if ($this->Owner || $this->Admin) {
            foreach ($_GET as $name => $value) {
                ${$name} = $value;
            }
            if ($field_id > 0) {
                $config_data = array(
                    'table_name' => 'sma_products',
                    'select_table' => 'sma_products',
                    'select_condition' => "id = ".$field_id,
                );
                $select_data = $this->site->api_select_data_v2($config_data);
                if ($select_data[0]['featured'] == 0)
                    $value = 1;
                else
                    $value = 0;

                $temp = array(
                    'featured' => $value
                );
                $this->db->update('sma_products', $temp,'id = '.$field_id);

                if ($value == 1) {       
                    $temp_display = '
                        <a href="javascript:void(0);" onclick="
                var postData = {
                    \'ajax_file\' : \''.admin_url('products/ajax_update_featured').'\',
                    \'field_id\' : \''.$field_id.'\',
                    \'element_id\' : \'api_page_admin_product_featured_'.$select_data[0]['id'].'\',
                };
                ajax_update_featured(postData);
                        ">
                            <span class="label label-success">
                            <i class="fa fa-check"></i> Yes</span>
                        </a>
                    ';
                }
                else {
                    $temp_display = '
                        <a href="javascript:void(0);" onclick="
                var postData = {
                    \'ajax_file\' : \''.admin_url('products/ajax_update_featured').'\',
                    \'field_id\' : \''.$field_id.'\',
                    \'element_id\' : \'api_page_admin_product_featured_'.$select_data[0]['id'].'\',
                };
                ajax_update_featured(postData);
                        ">
                            <span class="label label-danger">
                            <i class="fa fa-check"></i> No</span>
                        </a>
                    ';
                }

            }


            $temp3[1] = 'api-ajax-request-multiple-result-split';  
            $temp3[1] .= $value;
            $temp3[2] = 'api-ajax-request-multiple-result-split';            
            $temp3[2] .= $temp_display;
            $temp3[3] = 'api-ajax-request-multiple-result-split';            
            $result = $temp3[1].$temp3[2].$temp3[3];
            echo $result;            
        }
        else
            echo '<script>window.location = "'.base_url().'admin/login";</script>';        
    }

    public function api_ajax_remove_product_image() {
        if ($this->Owner || $this->Admin) {
            foreach ($_GET as $name => $value) {
                ${$name} = $value;
            }
            if ($selected_id > 0) {
                if ($cover == 1) {
                    $temp = $this->site->api_select_some_fields_with_where("
                        image
                        "
                        ,"sma_products"
                        ,"id = ".$selected_id
                        ,"arr"
                    );

                    $this->site->api_unlink('assets/uploads/'.$temp[0]['image']);
                    $this->site->api_unlink('assets/uploads/thumbs/'.$temp[0]['image']);
                    $config_data_2 = array(
                        'image' => 'no_image.png',
                    );
                    $this->db->update('sma_products', $config_data_2,'id = '.$selected_id);

                    $temp_display = '
                        <a href="'.base_url().'assets/uploads/no_image.png" target="_blank">
                            <img src="'.base_url().'assets/uploads/thumbs/no_image.png" class="file-preview-image" >
                        </a>
                    ';
                }
                else {
                    $temp = $this->site->api_select_some_fields_with_where("
                        photo
                        "
                        ,"sma_product_photos"
                        ,"id = ".$element_id
                        ,"arr"
                    );
                    $this->site->api_unlink('assets/uploads/'.$temp[0]['photo']);
                    $this->site->api_unlink('assets/uploads/thumbs/'.$temp[0]['photo']);
                    $this->db->delete('sma_product_photos', 'id = '.$element_id);
                }
            }

            $temp3[1] = 'api-ajax-request-multiple-result-split';  
            $temp3[1] .= $temp_display;
            $temp3[2] = 'api-ajax-request-multiple-result-split';            
            $result = $temp3[1].$temp3[2];
            echo $result;            
        }
        else
            echo '<script>window.location = "'.base_url().'admin/login";</script>';        
    }

    public function ajax_update_hide_show() {
        if ($this->Owner || $this->Admin) {
            foreach ($_GET as $name => $value) {
                ${$name} = $value;
            }
            if ($field_id > 0) {
                $config_data = array(
                    'table_name' => 'sma_products',
                    'select_table' => 'sma_products',
                    'select_condition' => "id = ".$field_id,
                );
                $select_data = $this->site->api_select_data_v2($config_data);
                if ($select_data[0]['hide'] == 0) {
                    $value = 1;
                    $temp6 = '<span class="label label-danger"><i class="fa fa-check"></i> '.lang('Hide').'</span>';

                }
                if ($select_data[0]['hide'] == 1) {
                    $value = 2;
                    $temp6 = '<span class="label label-success"><i class="fa fa-check"></i> '.lang('Show').'</span>';
                }
                if ($select_data[0]['hide'] == 2) {
                    $value = 0;
                    $temp6 = '<span class="label label-default"></i> '.lang('None').'</span>';
                }

                $temp = array(
                    'hide' => $value
                );
                $this->db->update('sma_products', $temp,'id = '.$field_id);

                    $temp_display = '
                        <a href="javascript:void(0);" onclick="
                var postData = {
                    \'ajax_file\' : \''.admin_url('products/ajax_update_hide_show').'\',
                    \'field_id\' : \''.$field_id.'\',
                    \'element_id\' : \'api_page_admin_product_hide_'.$select_data[0]['id'].'\',
                };
                ajax_update_hide_show(postData);
                        ">
                            '.$temp6.'
                        </a>
                    ';
            }


            $temp3[1] = 'api-ajax-request-multiple-result-split';  
            $temp3[1] .= $value;
            $temp3[2] = 'api-ajax-request-multiple-result-split';            
            $temp3[2] .= $temp_display;
            $temp3[3] = 'api-ajax-request-multiple-result-split';            
            $result = $temp3[1].$temp3[2].$temp3[3];
            echo $result;            
        }
        else
            echo '<script>window.location = "'.base_url().'admin/login";</script>';        
    }

function promotion() {
    $this->sma->checkPermissions();

    $sort_by = $this->input->get('sort_by');
    $sort_order = $this->input->get('sort_order');

    $per_page = $this->input->get('per_page') != '' ? $this->input->get('per_page') : 25;
    $page = $this->input->get('page') != '' ? $this->input->get('page') : 1;

    $offset_no = $page != '' ? ($page - 1 ) * $per_page : $offset_no;
    
    $offset_no = ($page - 1 ) * $per_page;

    $search = $this->input->get('search');
    $search = $this->db->escape_str($search);

    $temp_url = '';
    if ($page != '') 
        $temp_url .= '?page='; 
    if ($per_page != '') $temp_url .= '&per_page='.$per_page;
    if ($_GET['mode'] == 'sample') $temp_url .= '&mode='.$_GET['mode'];
    foreach ($_GET as $name => $value) {
        if ($name != 'page' && $name != 'per_page' && $name != 'mode')
            if ($value != '') $temp_url .= '&'.$name.'='.$value;
    }

    $config_data = array(
        'lg' => 'en',
        'table_name' => 'sma_product_promotion',
        'table_id' => 'id',
        'search' => $search,
        'sort_by' => $sort_by,
        'sort_order' => $sort_order,
        'limit' => $per_page,            
        'offset_no' => $offset_no,
    );
    $select_data = $this->products_model->get_promotion($config_data);

    $config_data = array(
        'lg' => 'en',
        'table_name' => 'sma_product_promotion',
        'table_id' => 'id',
        'search' => $search,
        'sort_by' => '',
        'sort_order' => '',
        'limit' => '',            
        'offset_no' => '',
    );
    $temp_count = $this->products_model->get_promotion($config_data);

    $total_record = count($temp_count);

    /* config pagination */
    $this->data['select_data'] = $select_data;
    $this->data['per_page'] = $per_page;
    $this->data['sort_by'] = $sort_by;
    $this->data['sort_order'] = $sort_order;
    $this->data['total_rows_sale'] = $total_record;
    $this->data['index_page'] = 'admin/products/promotion';
    $this->data['show_data'] = $page;

    $config_base_url = site_url().'admin/products/promotion'; //your url where the content is displayed
    $config['base_url'] = $config_base_url.$temp_url; //your url where the content is displayed
    $config['query_string_segment'] = 'page';
    $config['per_page'] = $per_page; //$per_page;
    $config['num_links'] = 5;
    $config['use_page_numbers'] = TRUE;
    $config['page_query_string'] = TRUE;
    $config['full_tag_open'] = '<ul class="pagination pagination-new-bg">';
    $config['full_tag_close'] = '</ul>';
    $config['first_link'] = FALSE;
    $config['last_link'] = FALSE;
    $config['prev_link'] = '&lt; Previous';
    $config['prev_tag_open'] = '<li class="prev">';
    $config['prev_tag_close'] = '</li>';
    $config['next_link'] = 'Next &gt;';
    $config['next_tag_open'] = '<li class="next">';
    $config['next_tag_close'] = '</li>';
    $config['cur_tag_open'] = '<li class="active"><a href="#">';
    $config['cur_tag_close'] = '</a></li>';
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $config['total_rows'] = $total_record; //$this->data['total_rows_sale'];        
    $this->load->library('pagination');   
    $this->pagination->initialize($config);
    /** end config pagination */
    /**Showing Label Data At Table Footer */
    $showing = $total_record != '' ? 1 : 0;
    $label_show_to= ($page == '' || $per_page < $total_record) ?  $per_page  :  $total_record;
    $label_show = "Showing " . $showing . " to ".$label_show_to." of : " . number_format($total_record) . " entries";

    $show_page = $page > 1 ? ($page * $per_page) : $total_record;
    $show_page_no = $show_page > $total_record ? $total_record : $page * $per_page;
    $label_show_page = "Showing " . (($page - 1) * ($per_page) + 1) . " to " . $show_page_no . " (filtered from " . number_format($total_record) . " total entries) ";
    $this->data['show'] = $search != '' || $page > 1 ? $label_show_page : $label_show;
    /** End Showing Label Data At Table Footer */          

    $this->data['api_settings'] = $this->api_settings;

    $api_list_view = array(
        'home_title' => lang('home'),
        'home_link' => base_url().'admin',
        'page_title' => lang('Promotion'),
        'controller' => 'products',
        'object_name' => 'promotion',
        'object_title' => lang('Promotion'),
        'form_action' => 'products/promotion',
        'form_bulk_action' => 'products/promotion_action',
        'form_name' => 'api_form_product_promotion',
    );        
    $this->data['api_list_view'] = $api_list_view;

    $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
    $bc = array(array('link' => $api_list_view['home_link'], 'page' => $api_list_view['home_title']), array('link' => '#', 'page' => $api_list_view['page_title']));
    $meta = array('page_title' => $api_list_view['page_title'], 'bc' => $bc);
    
    $this->page_construct('products/promotion', $meta, $this->data);
}

function delete_promotion($id = null)
{
    $this->sma->checkPermissions(null, true);
    if ($id > 0) {
        $config_data = array(
            'table_name' => 'sma_product_promotion',
            'select_table' => 'sma_product_promotion',
            'translate' => '',
            'select_condition' => "id = ".$id,
        );
        $select_data = $this->site->api_select_data_v2($config_data);            
        $this->db->delete('sma_product_promotion', array('id' => $id));        
        $this->session->set_flashdata('message', lang('Promotion').' '.lang('deleted')); 
    }
    redirect($_SERVER["HTTP_REFERER"]);
}

public function promotion_action()
{
    if (!$this->Owner && !$this->GP['bulk_actions']) {
        $this->session->set_flashdata('warning', lang('access_denied'));
        redirect($_SERVER["HTTP_REFERER"]);
    }

    $this->form_validation->set_rules('api_action', lang("api_action"), 'required');
    if ($this->form_validation->run() == true) {
        if (!empty($_POST['check_value'])) {
            $temp_val = explode('-',$_POST['check_value']);
            if ($this->input->post('api_action') == 'delete') {
                $this->sma->checkPermissions('delete');
                $j = 0;
                foreach ($temp_val as $id) {
                    if ($id != '') {
                        $this->db->delete('sma_product_promotion', array('id' => $id));
                        $j++;
                    }
                }
                $this->session->set_flashdata('message', $j.' '.lang("Promotion(s)").' '.lang("deleted"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        }
        else {
            $this->session->set_flashdata('error', lang("no").' '.lang("Promotion").' '.lang("selected"));
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }       
    else {
        $this->session->set_flashdata('error', validation_errors());
        redirect($_SERVER["HTTP_REFERER"]);
    }
}

public function edit_promotion($id = null)
{
    $this->sma->checkPermissions();

    $this->form_validation->set_rules('name', lang("name"), 'required');    

    $select_data = array();
    if ($id > 0) {
        $config_data = array(
            'table_name' => 'sma_product_promotion',
            'select_table' => 'sma_product_promotion',
            'select_condition' => "id = ".$id,
        );
        $select_data = $this->site->api_select_data_v2($config_data);
    }
    if ($this->form_validation->run() == true) {            

        $start_date = $this->input->post('start_date');
        if ($start_date == '')
            $start_date = date('Y-m-d H:i:s');
        else
            $start_date = $this->sma->fld(trim($this->input->post('start_date').':'.date('s')));    
        $end_date = $this->input->post('start_date');
        if ($end_date == '')
            $end_date = date('Y-m-d H:i:s');
        else
            $end_date = $this->sma->fld(trim($this->input->post('end_date').':'.date('s')));  


        // if($this->input->post('ordering') == 0) {
        //     $config_data = array(
        //         'table_name' => 'sma_product_promotion',
        //         'select_table' => 'sma_product_promotion',
        //         'select_condition' => "id > 0 order by ordering asc",
        //     );
        //     $temp = $this->site->api_select_data_v2($config_data);
        //     for($i=0;$i<count($temp);$i++) {
        //         $ordering['ordering'] = $temp[$i]['ordering'] + 1;
        //         $this->db->update("sma_product_promotion", $ordering['ordering'] , "id = ".$temp[$i]['id']);
        //     } 
        // } else {
        //     $temp_ordering = $this->input->post('ordering');
        //     $config_data = array(
        //         'table_name' => 'sma_product_promotion',
        //         'select_table' => 'sma_product_promotion',
        //         'select_condition' => "id > 0 order by ordering desc limit 1",
        //     );
        //     $temp3 = $this->site->api_select_data_v2($config_data);
        //     if($temp_ordering <= $temp3[0]['ordering']) {
        //         $config_data = array(
        //             'table_name' => 'sma_product_promotion',
        //             'select_table' => 'sma_product_promotion',
        //             'select_condition' => "id > 0 order by ordering asc",
        //         );
        //         $temp4 = $this->site->api_select_data_v2($config_data);
        //         for($k=0;$k<count($temp4);$k++) {
        //             if ($temp4[$k]['ordering'] >= $temp_ordering) {
        //                 $ordering2['ordering'] = $temp4[$k]['ordering'] + 1;
        //                 $this->db->update("sma_product_promotion", $ordering2 , "id = ".$temp4[$k]['id']);
        //             }
                    
                    
        //         } 
        //     }
        // }
        
        $data = array(
            'name' => $this->input->post('name'),
            'start_date' => $start_date,
            'end_date' => $end_date,
            'type_discount' => $this->input->post('type_discount'),
            'fixed_price' => $this->input->post('fixed_price'),
            'rate' => $this->input->post('rate'),
            'min_qty' => $this->input->post('min_qty'),
            'status' => $this->input->post('status'),
            'ordering' => $temp_ordering,
        ); 
        
        if ($_FILES['userfile']['size'] > 0) {
            $this->load->library('upload');
            $config['upload_path'] = $this->upload_path;
            $config['allowed_types'] = $this->image_types;
            $config['max_size'] = $this->allowed_file_size;
            $config['max_width'] = $this->Settings->iwidth;
            $config['max_height'] = $this->Settings->iheight;
            $config['overwrite'] = FALSE;
            $config['encrypt_name'] = TRUE;
            $config['max_filename'] = 25;
            $this->upload->initialize($config);
            if (!$this->upload->do_upload()) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('error', $error);
                redirect($_SERVER["HTTP_REFERER"]);
            }
            $photo = $this->upload->file_name;
            $data['image'] = $photo;
            $this->load->library('image_lib');
            $config['image_library'] = 'gd2';
            $config['source_image'] = $this->upload_path . $photo;
            $config['new_image'] = $this->thumbs_path . $photo;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = $this->Settings->twidth;
            $config['height'] = $this->Settings->theight;
            $this->image_lib->clear();
            $this->image_lib->initialize($config);
            if (!$this->image_lib->resize()) {
                echo $this->image_lib->display_errors();
            }
            if ($this->Settings->watermark) {
                $this->image_lib->clear();
                $wm['source_image'] = $this->upload_path . $photo;
                $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                $wm['wm_type'] = 'text';
                $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                $wm['quality'] = '100';
                $wm['wm_font_size'] = '16';
                $wm['wm_font_color'] = '999999';
                $wm['wm_shadow_color'] = 'CCCCCC';
                $wm['wm_vrt_alignment'] = 'top';
                $wm['wm_hor_alignment'] = 'left';
                $wm['wm_padding'] = '10';
                $this->image_lib->initialize($wm);
                $this->image_lib->watermark();
            }
            $this->image_lib->clear();
            $config = NULL;
        }
        if ($select_data[0]['id'] == '') {
            $this->db->insert('sma_product_promotion', $data);
            $id = $this->db->insert_id();                  
            
            if ($this->input->post('ordering') != '') {
                $config_data_function = array(
                    'table_name' => 'sma_product_promotion',
                    'id' => $id,
                    'value' => $this->input->post('ordering'),
                );                
                $this->api_helper->generate_ordering($config_data_function);
            }

            $this->session->set_flashdata('message', lang("Successfully_added_a_promotion"));
        }
        else {
            $this->db->update('sma_product_promotion', $data, array('id' => $select_data[0]['id']));

            // foreach($_POST as $name => $value) {
            //     if (is_int(strpos($name,"temp_rate_"))) {
            //         $temp_p_id = str_replace('temp_rate_','',$name);
            //         $temp = array(
            //             'rate' => $value
            //         );
            //         $this->db->update('sma_product_promotion_item', $temp,"product_id = ".$temp_p_id." and promotion_id = ".$select_data[0]['id']);                    
            //     }
            // }

            if ($this->input->post('ordering') != '') {
                $config_data_function = array(
                    'table_name' => 'sma_product_promotion',
                    'id' => $select_data[0]['id'],
                    'value' => $this->input->post('ordering'),
                );                
                $this->api_helper->generate_ordering($config_data_function);
            }
            $this->session->set_flashdata('message', lang("Successfully_updated_promotion"));
        }

        admin_redirect("products/promotion");
    } else {

        $api_list_view = array(
            'home_title' => lang('home'),
            'home_link' => base_url().'admin',
            'page_title' => lang('Promotion'),
            'controller' => 'products',
            'object_name' => 'promotion',
            'object_title' => lang('Promotion'),
            'form_action' => 'products/promotion',
            'form_bulk_action' => 'products/promotion_action',
            'form_name' => 'api_form_product_promotion',
        );        
        $this->data['api_list_view'] = $api_list_view;

        $this->data['select_data'] = $select_data;
        $this->data['modal_js'] = $this->site->modal_js();
        $this->load->view($this->theme . 'products/edit_promotion', $this->data);
    }
}

function ajax_check_add_product() {
    if ($this->Owner || $this->Admin) {
        foreach ($_GET as $name => $value) {
            ${$name} = $value;
        }
        $config_data = array(
            'table_name' => 'sma_products',
            'select_table' => 'sma_products',
            'select_condition' => "id > 0 ",
        );
        $temp = $this->site->api_select_data_v2($config_data);
        $b = 0;
        $b2 = 0;
        for ($i=0;$i<count($temp);$i++) {
            if ($temp[$i]['code'] == $product_code) {
                $b = 1;
                break;
            }
        }
        for ($i2=0;$i2<count($temp);$i2++) {
            if ($temp[$i2]['slug'] == $product_slug) {
                $b2 = 1;
                break;
            }
        }  
        $temp3[1] = 'api-ajax-request-multiple-result-split';  
        $temp3[1] .= $b;  
        $temp3[3] = 'api-ajax-request-multiple-result-split';  
        $temp3[3] .= $b2;    
        $temp3[4] = 'api-ajax-request-multiple-result-split'; 
        $temp3[4] .= '';    
        $temp3[5] = 'api-ajax-request-multiple-result-split';  
        $result = $temp3[1].$temp3[3].$temp3[4]; 
        echo $result;
    } 
    else
        echo '<script>window.location = "'.base_url().'admin/login";</script>';   
}
function ajax_check_edit_product() {
    if ($this->Owner || $this->Admin) {
        foreach ($_GET as $name => $value) {
            ${$name} = $value;
        }   
        $config_data = array(
            'table_name' => 'sma_products',
            'select_table' => 'sma_products',
            'select_condition' => "id != ".$product_id,
        );
        $temp = $this->site->api_select_data_v2($config_data);
        $b = 0;
        $b2 = 0;
        for ($i=0;$i<count($temp);$i++) {
            if ($temp[$i]['code'] == $product_code) {
                $b = 1;
                break;
            }
        } 
        for ($i2=0;$i2<count($temp);$i2++) {
            if ($temp[$i2]['slug'] == $product_slug) {
                $b2 = 1;
                break;
            }
        } 

        $temp3[1] = 'api-ajax-request-multiple-result-split';  
        $temp3[1] .= $b;  
        $temp3[3] = 'api-ajax-request-multiple-result-split';  
        $temp3[3] .= $b2;    
        $temp3[4] = 'api-ajax-request-multiple-result-split'; 
        $temp3[4] .= '';    
        $temp3[5] = 'api-ajax-request-multiple-result-split';  
        $result = $temp3[1].$temp3[3].$temp3[4]; 
        echo $result;
    } 
    else
        echo '<script>window.location = "'.base_url().'admin/login";</script>';   
}


function ajax_promotion_remove() {
    if ($this->Owner || $this->Admin) {
        foreach ($_GET as $name => $value) {
            ${$name} = $value;
        }   
        $this->db->delete('sma_product_promotion_item', "id = ".$id);

        $config_data = array(
            'table_name' => 'sma_products',
            'select_table' => 'sma_products',
            'translate' => '',
            'description' => '',
            'select_condition' => "id = ".$product_id,
        );
        $temp = $this->api_helper->api_select_data_v2($config_data);
                
        $temp_2 = array(
            'promotion' => 0
        );
        $this->db->update('sma_products', $temp_2,"id = ".$product_id);

        $config_data = array(
            'table_name' => 'sma_products',
            'id_name' => 'id',
            'field_add_ons_name' => 'add_ons',
            'selected_id' => $product_id,
            'add_ons_title' => 'promotion_type',
            'add_ons_value' => '',                    
        );
        $this->site->api_update_add_ons_field($config_data);      

        $config_data = array(
            'table_name' => 'sma_products',
            'id_name' => 'id',
            'field_add_ons_name' => 'add_ons',
            'selected_id' => $product_id,
            'add_ons_title' => 'promotion_id',
            'add_ons_value' => '',                    
        );
        $this->site->api_update_add_ons_field($config_data);                                   
        

        $temp3[1] = 'api-ajax-request-multiple-result-split';  
        $temp3[1] .= $product_id;  
        $temp3[2] = 'api-ajax-request-multiple-result-split';  
        $temp3[2] .= '';          
        $result = $temp3[1].$temp3[2]; 
        echo $result;
    } 
    else
        echo '<script>window.location = "'.base_url().'admin/login";</script>';   
}

function ajax_promotion_add() {
    if ($this->Owner || $this->Admin) {
        foreach ($_GET as $name => $value) {
            ${$name} = $value;
        }   

        $config_data = array(
            'table_name' => 'sma_products',
            'select_table' => 'sma_products',
            'translate' => '',
            'description' => '',
            'select_condition' => "id = ".$product_id,
        );
        $temp_product = $this->api_helper->api_select_data_v2($config_data);    

        $config_data = array(
            'table_name' => 'sma_product_promotion_item',
            'select_table' => 'sma_product_promotion_item',
            'translate' => '',
            'description' => '',
            'select_condition' => "product_id = ".$product_id." and promotion_id = ".$promotion_id,
        );
        $temp = $this->api_helper->api_select_data_v2($config_data);        

        if (count($temp) <= 0) {
            $this->db->delete('sma_product_promotion_item', "product_id = ".$product_id);

            $temp_2 = array(
                'promotion' => 1
            );
            $this->db->update('sma_products', $temp_2,"id = ".$product_id);

            $config_data = array(
                'table_name' => 'sma_products',
                'id_name' => 'id',
                'field_add_ons_name' => 'add_ons',
                'selected_id' => $product_id,
                'add_ons_title' => 'promotion_type',
                'add_ons_value' => 'list',                    
            );
            $this->site->api_update_add_ons_field($config_data);      

            $config_data = array(
                'table_name' => 'sma_products',
                'id_name' => 'id',
                'field_add_ons_name' => 'add_ons',
                'selected_id' => $product_id,
                'add_ons_title' => 'promotion_id',
                'add_ons_value' => $promotion_id,                    
            );
            $this->site->api_update_add_ons_field($config_data);
                        
            $config_data = array(
                'table_name' => 'sma_product_promotion',
                'select_table' => 'sma_product_promotion',
                'translate' => '',
                'description' => '',
                'select_condition' => "id = ".$promotion_id,
            );
            $temp_2 = $this->api_helper->api_select_data_v2($config_data);

            $temp = array(
                'product_id' => $product_id,
                'promotion_id' => $promotion_id,
                'rate' => $temp_2[0]['rate'],
            );
            $this->db->insert('sma_product_promotion_item', $temp);


$config_data_2 = array(
    'table_name' => 'sma_product_promotion_item',
    'select_table' => 'sma_product_promotion_item',
    'translate' => '',
    'select_condition' => "id > 0 and promotion_id = ".$promotion_id." order by id desc ",
);
$select_list = $this->site->api_select_data_v2($config_data_2);                  
$row = 1;
for($i=0;$i<count($select_list);$i++){
    $config_data_2 = array(
        'table_name' => 'sma_products',
        'select_table' => 'sma_products',
        'translate' => '',
        'select_condition' => "id = ".$select_list[$i]['product_id'],
    );
    $select_data_product = $this->site->api_select_data_v2($config_data_2);    

    $data_view = array (
        'wrapper_class' => '',
        'file_path' => 'assets/uploads/thumbs/'.$select_data_product[0]['image'],
        'max_width' => 50,
        'max_height' => 50,
        'product_link' => '',
        'image_class' => 'img-responsive',
        'image_id' => '',   
        'resize_type' => 'full',       
    );
    $temp_image = $this->site->api_get_image($data_view);                            

    $element_html .= '
        <tr class="api_temp_tr_promote_'.$select_list[$i]['id'].'">
            <td align="center">'.$row.'</td>
            <td align="center" valign="middle">
                '.$temp_image['image'].'
            </td>
            <td>
                '.$select_data_product[0]['name'].' ('.$select_data_product[0]['code'].')
            </td>
            <td align="right">
                '.$this->sma->formatMoney($select_data_product[0]['price']).'
            </td>
            <td align="right">
                <select name="temp_rate_'.$select_list[$i]['id'].'" style="width:80px; height:30px;" onchange="
                var postData = {
                    \'field_value\' : this.value,
                    \'field_id\' : \''.$select_list[$i]['id'].'\',
                    \'element_id\' : \'temp_price_after_discount_'.$select_list[$i]['id'].'\',
                };
                api_ajax_rate_change(postData);
                ">
        ';
                    for ($j=0;$j<=100;$j++) {
                        if ($j == $select_list[$i]['rate'])
                            $temp_selected = 'selected';
                        else
                            $temp_selected = '';

                        $element_html .= '
                            <option value="'.$j.'" '.$temp_selected.'>
                                '.$j.'%
                            </option>
                        ';
                    }
        $element_html .= '
                </select>
            </td>
        ';

        $config_data_2 = array(
            'id' => $select_data_product[0]['id'],
            'customer_id' => '',
        );
        $temp_price = $this->site->api_calculate_product_price($config_data_2);   

        $element_html .= '
            <td align="right">
                <span id="temp_price_after_discount_'.$select_list[$i]['id'].'">'.$this->sma->formatMoney($temp_price['price']).'</span>
            </td>
            <td align="center">
                <a href="javascript:void(0);" onclick="
                var postData = {
                    \'id\' : \''.$select_list[$i]['id'].'\',
                    \'product_id\' : \''.$select_data_product[0]['id'].'\',
                    \'element_id\' : \'api_temp_tr_promote_'.$select_list[$i]['id'].'\',
                };                                            
                api_temp_promotion_remove(postData);
                " title="Remove">
                    <li class="fa fa-trash-o"></li>
                </a>
            </td>
        </tr>
        ';
    $row++;
}


        }
        else
            $message = $temp_product[0]['name'].' ('.$temp_product[0]['code'].') is already added';

        $temp3[1] = 'api-ajax-request-multiple-result-split';  
        $temp3[1] .= $element_html;  
        $temp3[2] = 'api-ajax-request-multiple-result-split';  
        $temp3[2] .= $message;          
        $temp3[3] = 'api-ajax-request-multiple-result-split';  
        $result = $temp3[1].$temp3[2].$temp3[3]; 
        echo $result;
    } 
    else
        echo '<script>window.location = "'.base_url().'admin/login";</script>';   
}

public function api_ajax_rate_change() {
    if ($this->Owner || $this->Admin) {
        foreach ($_GET as $name => $value) {
            ${$name} = $value;
        }
        if ($field_id > 0) {
            $temp = array(
                'rate' => $field_value
            );
            $this->db->update('sma_product_promotion_item', $temp,"id = ".$field_id);

            $config_data = array(
                'table_name' => 'sma_product_promotion_item',
                'select_table' => 'sma_product_promotion_item',
                'translate' => '',
                'description' => '',
                'select_condition' => "id = ".$field_id,
            );
            $select_data = $this->api_helper->api_select_data_v2($config_data);            

            $config_data_2 = array(
                'id' => $select_data[0]['product_id'],
                'customer_id' => '',
            );
            $temp_price = $this->site->api_calculate_product_price($config_data_2); 
            //if ($temp_price['promotion'][''])    
        }
        $temp3[1] = 'api-ajax-request-multiple-result-split';  
        $temp3[1] .= $this->sma->formatMoney($temp_price['price']);
        $temp3[2] = 'api-ajax-request-multiple-result-split';    
        $temp3[2] .= $field_value;                
        $result = $temp3[1].$temp3[2];
        echo $result;            
    }
    else
        echo '<script>window.location = "'.base_url().'admin/login";</script>';        
}


}





