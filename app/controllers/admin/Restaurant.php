<?php defined('BASEPATH') or exit('No direct script access allowed');
class Restaurant extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        if ($this->Customer || $this->Supplier) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->load->admin_model('restaurant_model');
        $this->session->set_userdata('last_activity', now());
        $this->lang->admin_load('restaurant', $this->Settings->user_language);
        $this->load->library('form_validation');
    }
public function view($id = null) {
    $this->sma->checkPermissions();

    $this->load->view($this->theme.'restaurant/view', $this->data);
}

function api_ajax_load_menu() {
    $return['error'] = '';
    if (!$this->Owner && !$this->Admin)
        $return['error'] = 'not_logged_in';
    else {
        foreach ($_GET as $name => $value) {
            ${$name} = $value;
        }
        if ($table_id > 0) {
            $config_data = array(
                'table_name' => 'sma_restaurant',
                'select_table' => 'sma_restaurant',
                'translate' => '',
                'description' => '',
                'select_condition' => "id = ".$table_id,
            );
            $select_data = $this->api_helper->api_select_data_v2($config_data);
        }
        include 'assets/api/component/api_cart_express/api_cart_express.php';
        $api_cart_express = new api_cart_express;
   
        $config_data = array(
            'cart_type' => 'restaurant',
            'step' => '',        
            'btn_back_to_cart' => '',
            'table_id' => $table_id, 
            'category_id' => $category_id, 
            'category_name' =>  $category_name, 
            'table_name' => $select_data[0]['table_name'],
        );
        $config_data_2 = $api_cart_express->config($config_data);        
        $temp = $api_cart_express->view_product_list($config_data_2);
        $temp_display_menu = $temp['display'];
        $return['display'] = $temp_display_menu;
        // call_function
        $config_data_function['table_id'] = $table_id;
        $temp_display_total_detail = $this->restaurant_model->view_total_detail($config_data_function);
        $return['display_total'] = '
            '.$temp_display_total_detail['display'].' 
        ';
        $return['status'] = $select_data[0]['status'];

        $return['display_top_menu_icons'] = '
            <ul class="api_kh api_temp_btn pull-right">                
                <li class="dropdown">
                    <a class="btn bgrey  pos-tip api_btn_view_menu" title="View Menu" data-placement="bottom" href="javascript:void(0);" onclick="
                        var postData = {
                            \'table_id\' : \''.$table_id.'\',
                            \'ajax_url\' : \''.base_url().'admin/restaurant/api_ajax_load_menu\',
                        };
                        api_ajax_load_menu(postData); 
                    ">
                        <i class="fa fa-list-alt" aria-hidden="true"></i>
                    </a>
                </li>
                <li class="dropdown">
                    <a class="btn blightOrange pos-tip api_btn_view" title="View Detail" data-placement="bottom" href="javascript:void(0);" onclick="
                        var postData = {
                            \'table_id\' : \''.$table_id.'\',
                            \'ajax_url\' : \''.base_url().'admin/restaurant/api_ajax_load_view_detail\',
                        };
                        api_ajax_load_view_detail(postData); 
                    ">
                        <i class="fa fa-eye text-center" aria-hidden="true"></i>
                    </a>
                </li>
                <li class="dropdown">
                    <a class="btn bblue pos-tip api_btn_payment" title="View Detail Payment" data-placement="bottom" href="javascript:void(0);" onclick="
                    var postData = {
                        \'table_id\' : \''.$table_id.'\',
                        \'ajax_url\' : \''.base_url().'admin/restaurant/api_ajax_load_payment_detail\',
                    };
                    api_ajax_load_payment_detail(postData); 
                "> 
                        <i class="fa fa-money text-center" aria-hidden="true"></i>
                    </a>
                </li>
                <li class="dropdown">
                    <a class="btn bblue pos-tip api_btn_menu" title="Menu" data-placement="bottom" href="javascript:void(0);" onclick="click_menu();"  data-original-title="Menu">
                        <i class="fa fa-list" aria-hidden="true"></i>
                    </a>
                </li>
                <li class="dropdown">
                    <a class="btn bdarkGreen pos-tip api_btn_table" title="View Table" data-placement="bottom" ​​​href="javascript:void(0);" onclick="
                        var postData = {
                            \'table_id\' : \''.$table_id.'\', 
                            \'ajax_url\' : \''.base_url().'admin/restaurant/api_ajax_load_table\',
                        };
                        api_ajax_load_table(postData); 
                    ">
                        <i class="fa fa-table api_table" aria-hidden="true"></i>
                    </a>
                </li>
                <li class="dropdown">
                    <a class="btn bpink pos-tip api_btn_dashboard" title="Dashboard" data-placement="bottom" href="'.base_url().'admin/welcome" data-original-title="Dashboard">
                        <i class="fa fa-dashboard"></i>
                    </a>
                </li>
                <li class="dropdown">
                        <a class="btn bred tip pos-tip api_btn_logout" title="LogOut" href="'.base_url().'admin/logout">
                            <i class="fa fa-sign-out"></i>
                        </a>
                </li>
            </ul>        
        ';

        $return['page_url'] = base_url().'admin/restaurant/view?table_id='.$select_data[0]['id'];
    }
    echo json_encode($return);
}
function api_ajax_load_view_detail() {
    $return['error'] = '';
    if (!$this->Owner && !$this->Admin)
        $return['error'] = 'not_logged_in';
    else {
        foreach ($_GET as $name => $value) {
            ${$name} = $value;
        }

        if ($table_id > 0) {
            $config_data = array(
                'table_name' => 'sma_restaurant',
                'select_table' => 'sma_restaurant',
                'translate' => '',
                'description' => '',
                'select_condition' => "id = ".$table_id,
            );
            $select_data = $this->api_helper->api_select_data_v2($config_data);
        }

        $return['display'] = '
            <i class="fa fa-eye text-center" aria-hidden="true"></i>
            View detail: '.$select_data[0]['id'].', '.$select_data[0]['table_name'].'
        ';   
        // call_function
        $config_data_function['table_id'] = $table_id;
        $temp_display_total_detail = $this->restaurant_model->view_total_detail($config_data_function);
        $return['display_total'] = '
            '.$temp_display_total_detail['display'].' 
        '; 
        $return['page_url'] = base_url().'admin/restaurant/view?table_id='.$select_data[0]['id'];
    }
    echo json_encode($return);
}

function api_ajax_load_payment_detail() {
    $return['error'] = '';
    if (!$this->Owner && !$this->Admin)
        $return['error'] = 'not_logged_in';
    else {
        foreach ($_GET as $name => $value) {
            ${$name} = $value;
        }

        if ($table_id > 0) {
            $config_data = array(
                'table_name' => 'sma_restaurant',
                'select_table' => 'sma_restaurant',
                'translate' => '',
                'select_condition' => "id = ".$table_id,
            );
            $temp_select_data = $this->site->api_select_data_v2($config_data);
        }

        $config_data = array(
            'table_id' => $table_id,
            'table_name' => $temp_select_data[0]['table_name'],
        );
        $temp = $this->restaurant_model->view_payment_detail($config_data);
        $return['display'] = $temp['display'];   

        $return['page_url'] = base_url().'admin/restaurant/view?table_id='.$temp_select_data[0]['id'];
    }
    echo json_encode($return);
}
function api_ajax_load_table() {
    $return['error'] = '';
    if (!$this->Owner && !$this->Admin)
        $return['error'] = 'not_logged_in';
    else {
        foreach ($_GET as $name => $value) {
            ${$name} = $value;
        }

        if ($table_id > 0) {
            $config_data = array(
                'table_name' => 'sma_restaurant',
                'select_table' => 'sma_restaurant',
                'translate' => '',
                'select_condition' => "id = ".$table_id,
            );
            $temp_select_data = $this->site->api_select_data_v2($config_data);
        }

        $config_data = array(
            'table_id' => $table_id,
        );
        $temp = $this->restaurant_model->view_table_selection($config_data);

        $return['display'] = $temp['display']; 
        $return['status'] = $temp_select_data[0]['status'];  

        $return['page_url'] = base_url().'admin/restaurant/view?table_id='.$temp_select_data[0]['id'];
    }
    echo json_encode($return);
}

//mobile
function api_ajax_load_menu_mobile() {
    $return['error'] = '';
    if (!$this->Owner && !$this->Admin)
        $return['error'] = 'not_logged_in';
    else {
        foreach ($_GET as $name => $value) {
            ${$name} = $value;
        }

        if ($table_id > 0) {
            $config_data = array(
                'table_name' => 'sma_restaurant',
                'select_table' => 'sma_restaurant',
                'translate' => '',
                'select_condition' => "id = ".$table_id,
            );
            $temp_select_data = $this->site->api_select_data_v2($config_data);
        }
        
        include 'assets/api/component/api_cart_express/api_cart_express.php';
        $api_cart_express = new api_cart_express;
        $config_data = array(
            'cart_type' => 'restaurant',
            'step' => '',        
            'btn_back_to_cart' => '',
            'table_id' => $table_id, 
            'category_id' => $category_id, 
            'category_name' =>  $category_name, 
            'table_name' => $select_data[0]['table_name'],
        );
        $config_data_2 = $api_cart_express->config($config_data);        
        $temp = $api_cart_express->view_product_list($config_data_2);
        $config_data_2['table_id'] = $table_id;
        // $config_data_2 = $api_cart_express->config_restaurant($config_data);
        $temp_display_menu = $temp['display'];
        $return['display'] = $temp_display_menu;

        $config_data = array(
            'table_id' => $table_id,
            'clicked' => 'view_list_table',
        );        
        $temp = $this->restaurant_model->display_top_header_menu_icon($config_data);
        $return['display_top_header_menu_icon_mobile'] = $temp['display_mobile'];

        $temp_footer = $this->restaurant_model->view_total_detail($config_data);

        $return['display_total_mobile'] = $temp_footer['display_mobile'];
        $return['page_url'] = base_url().'admin/restaurant/view?table_id='.$temp_select_data[0]['id'];
    }
    echo json_encode($return);
}
function api_ajax_load_view_detail_mobile() {
    $return['error'] = '';
    if (!$this->Owner && !$this->Admin)
        $return['error'] = 'not_logged_in';
    else {
        foreach ($_GET as $name => $value) {
            ${$name} = $value;
        }

        if ($table_id > 0) {
            $config_data = array(
                'table_name' => 'sma_restaurant',
                'select_table' => 'sma_restaurant',
                'translate' => '',
                'description' => '',
                'select_condition' => "id = ".$table_id,
            );
            $select_data = $this->api_helper->api_select_data_v2($config_data);
        }

        $return['display'] = '
            <div class="api_temp_menu_mobile">
                <i class="fa fa-eye text-center" aria-hidden="true"></i>
                View detail: '.$select_data[0]['id'].', '.$select_data[0]['table_name'].'
            </div>    
        '; 

        $config_data = array(
            'table_id' => $table_id,
            'clicked' => 'view_detail',

        );
        $temp = $this->restaurant_model->display_top_header_menu_icon($config_data);

        $return['display_top_header_menu_icon_mobile'] = $temp['display_mobile'];

        $temp_footer = $this->restaurant_model->view_total_detail($config_data);
        $return['display_total_mobile'] = $temp_footer['display_mobile'];
        
        $return['page_url'] = base_url().'admin/restaurant/view?table_id='.$temp_select_data[0]['id'];
    }
    echo json_encode($return);
}
function api_ajax_load_payment_detail_mobile() {
    $return['error'] = '';
    if (!$this->Owner && !$this->Admin)
        $return['error'] = 'not_logged_in';
    else {
        foreach ($_GET as $name => $value) {
            ${$name} = $value;
        }

        if ($table_id > 0) {
            $config_data = array(
                'table_name' => 'sma_restaurant',
                'select_table' => 'sma_restaurant',
                'translate' => '',
                'select_condition' => "id = ".$table_id,
            );
            $temp_select_data = $this->site->api_select_data_v2($config_data);
        }

        $config_data = array(
            'table_id' => $table_id,
            'clicked' => 'view_payment',
            'hide_btn_submit' => 1,
            'table_name' => $temp_select_data[0]['table_name'],
        );
        $temp = $this->restaurant_model->view_payment_detail($config_data);
        $return['display'] = $temp['display'];   
        
        $temp = $this->restaurant_model->display_top_header_menu_icon($config_data);
        $return['display_top_header_menu_icon'] = $temp['display'];
        $return['display_top_header_menu_icon_mobile'] = $temp['display_mobile'];
        
        $temp = $this->restaurant_model->display_payment_submit($config_data);
        $return['display_payment_submit'] = $temp['display'];

        $return['page_url'] = base_url().'admin/restaurant/view?table_id='.$temp_select_data[0]['id'];
    }
    echo json_encode($return);
}
function api_ajax_load_table_mobile() {
    $return['error'] = '';
    if (!$this->Owner && !$this->Admin)
        $return['error'] = 'not_logged_in';
    else {
        foreach ($_GET as $name => $value) {
            ${$name} = $value;
        }

        if ($table_id > 0) {
            $config_data = array(
                'table_name' => 'sma_restaurant',
                'select_table' => 'sma_restaurant',
                'translate' => '',
                'select_condition' => "id = ".$table_id,
            );
            $temp_select_data = $this->site->api_select_data_v2($config_data);
        }

        $config_data = array(
            'table_id' => $table_id,
            'clicked' => 'view_all_table',
        );
        $temp = $this->restaurant_model->view_table_selection($config_data);
        $return['display'] = $temp['display'];

        $temp = $this->restaurant_model->display_top_header_menu_icon($config_data);
        $return['display_top_header_menu_icon'] = $temp['display'];
        $return['display_top_header_menu_icon_mobile'] = $temp['display_mobile'];

        $temps = $this->restaurant_model->display_footer($config_data_function);
        $return['display_total'] = $temps['display']; 

        $temp = $this->restaurant_model->view_total_detail($config_data);
        $return['display_total'] = $temp['display_mobile'];

        $return['page_url'] = base_url().'admin/restaurant/view?table_id='.$temp_select_data[0]['id'];
    }
    echo json_encode($return);
}

function api_cart_express_ajax_update_cart_qty_restarant() {
    $return['error'] = '';
    if ($this->session->userdata('user_id') <= 0)
        $return['error'] = 'not_logged_in';
    else {
        foreach ($_GET as $name => $value) {
            ${$name} = $value;
        }

        $qty = floatval($qty);
        if ($qty <= 0) $qty = 1;
        
        $return['status'] = 'success';
        $config_data = array(
            'cart_type' => $cart_type,
            'cart_id' => $cart_id,
        );
        $select_cart_item = $this->get_data_by_cart_id($config_data);
        

        if ($this->api_shop_setting[0]['out_of_stock'] == 1) {
            $config_data = array(
                'product_id' => $select_cart_item['id'],
                'warehouse_id' => $this->shop_settings->warehouse,
                'compare_quantity' => $qty,
            );
            $temp = $this->site->api_check_product_stock($config_data);
            if ($temp['result'] <= 0)
                $return['status'] = 'out_of_stock';
        }

        $config_data_2 = array(
            'id' => $select_cart_item['id'],
            'customer_id' => $this->session->userdata('company_id'),
        );    
        $temp5 = $this->site->api_calculate_product_price_v2($config_data_2);
        $temp_min_qty = 1; 
        if ($temp5['promotion']['price'] > 0) {
            $temp_min_qty = $temp5['promotion']['min_qty'];
            if ($qty < $temp_min_qty) {
                $qty = $temp_min_qty;
                $return['status'] = 'minimum_quantity';
            }
        }

        if ($return['status'] == 'success') {
            $select_cart_item['qty'] = $qty;
            $config_data = array(
                'cart_type' => $cart_type,
                'cart_item' => $select_cart_item,                                
            );
            //$this->update_cart_item($config_data);
        }
        
        if ($qty <= $temp_min_qty)
            $btn_sub_status = 'sub_disable';
        else
            $btn_sub_status = 'sub_enable';

        $return['cart_total_display'] = $temp_cart_total['display'];
        $return['total_cart'] = count($temp_data['cart_item']);
        $return['total_cart_qty'] = $temp_data['total_cart_qty'];
        $return['btn_sub_status'] = $btn_sub_status;

    }
    
    echo json_encode($return);
}


function api_cart_express_ajax_update_cart_qty_restaurant() {
    $return['error'] = '';
    if ($this->session->userdata('user_id') <= 0 || ($this->session->userdata('group_id') != 1 && $this->session->userdata('group_id') != 2))
        $return['error'] = 'not_logged_in';
    else {
        foreach ($_GET as $name => $value) {
            ${$name} = $value;
        }

        $qty = floatval($qty);
        if ($qty <= 0) $qty = 0;

        $return['status'] = 'success';
        include 'assets/api/component/api_cart_express/api_cart_express.php';
        $api_cart_express = new api_cart_express;
        $config_data = array(
            'cart_type' => 'restaurant',    
            'table_id' => $table_id, 
        ); 
        $temp = $api_cart_express->view_product_list($config_data);
        $temp_display_menu = $temp['display'];
        $return['display'] = $temp_display_menu;
        
        $config_data = array(
            'table_id' => $table_id, 
        ); 
        $select_data = $api_cart_express->initial_data_restaurant($config_data);

        $config_data_2 = $api_cart_express->config($config_data);
        $temp = $api_cart_express->get_quantity_by_product_id($config_data_2);
        $temp_display_qty = $temp['display'];
        $return['display'] = $temp_display_qty;
        
        if (count($select_data['cart_item']) <= 0) {
            $select_data['cart_item'][0]['id'] = $product_id;
            $select_data['cart_item'][0]['qty'] = $qty;
            $select_data['cart_item'][0]['min_qty'] = 0;
            $temp_json = json_encode($select_data);
            $temp = array(
                'json_data' => $temp_json,
            );
            $this->db->update('sma_restaurant', $temp,"id = ".$table_id);            
        }
        else {
            $b = 0;
            $selected_index = 0;
            for ($i=0;$i<count($select_data['cart_item']);$i++) {
                if ($select_data['cart_item'][$i]['id'] == $product_id) {
                    $b = 1;
                    $selected_index = $i;
                    break;
                }
            }            
            if ($b == 0) {
                $temp_index = count($select_data['cart_item']);
                $select_data['cart_item'][$temp_index]['id'] = $product_id;
                $select_data['cart_item'][$temp_index]['qty'] = $qty;                
                $select_data['cart_item'][$temp_index]['min_qty'] = 0;                
            }
            else {
                $select_data['cart_item'][$selected_index]['qty'] = $qty;
            }

            $temp_json = json_encode($select_data);
            $temp = array(
                'json_data' => $temp_json,
            );
            $this->db->update('sma_restaurant', $temp,"id = ".$table_id);  
            // $return['test'] = $qty;
        }

        // $config_data_2 = array(
        //     'id' => $select_cart_item['id'],
        //     'customer_id' => $this->session->userdata('company_id'),
        // );    
        // $temp5 = $this->site->api_calculate_product_price_v2($config_data_2);
        // $temp_min_qty = 1; 
        // if ($temp5['promotion']['price'] > 0) {
        //     $temp_min_qty = $temp5['promotion']['min_qty'];
        //     if ($qty < $temp_min_qty) {
        //         $qty = $temp_min_qty;
        //         $return['status'] = 'minimum_quantity';
        //     }
        // }

        // if ($return['status'] == 'success') {
        //     $select_cart_item['qty'] = $qty;
        //     $config_data = array(
        //         'cart_type' => $cart_type,
        //         'cart_item' => $select_cart_item,                                
        //     );
        //     $this->update_cart_item($config_data);
        //     $config_data = array(
        //         'cart_type' => $cart_type,
        //         'btn_back_to_cart' => 0,
        //         'step' => 'cart',
        //     );
        //     $temp_data = $this->config($config_data);
        //     $temp_cart_total = $this->view_cart_total($temp_data);
        // }


        // if ($qty <= $temp_min_qty)
        //     $btn_sub_status = 'sub_disable';
        // else
        //     $btn_sub_status = 'sub_enable';

        // $return['cart_total_display'] = $temp_cart_total['display'];
        // $return['total_cart'] = count($temp_data['cart_item']);
        // $return['total_cart_qty'] = $temp_data['total_cart_qty'];
        // $return['btn_sub_status'] = $btn_sub_status;

    }
    
    echo json_encode($return);
}


}
