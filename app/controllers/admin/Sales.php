<?php defined('BASEPATH') or exit('No direct script access allowed');

class Sales extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        if ($this->Supplier) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->lang->admin_load('sales', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('sales_model');
        $this->digital_upload_path = 'files/';
        $this->upload_path = 'assets/uploads/';
        $this->thumbs_path = 'assets/uploads/thumbs/';
        $this->image_types = 'gif|jpg|jpeg|png|tif';
        $this->digital_file_types = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif|txt';
        $this->allowed_file_size = '1024';
        $this->data['logo'] = true;
        
        $this->role_owner = ($this->Owner || $this->Admin) ? true : false;
        $this->data['role_owner'] = $this->role_owner;

        $config_data = array(
            'table_name' => 'sma_settings',
            'select_table' => 'sma_settings',
            'select_condition' => "setting_id = 1",
        );
        $this->api_settings = $this->site->api_select_data_v2($config_data);
    }

    public function index()
    {
        $this->sma->checkPermissions();
        $temp_warhouse = $this->site->api_select_some_fields_with_where(
            "
            *     
            ",
            "sma_warehouses",
            "id > 0 order by id asc",
            "arr"
        );
        $this->data['api_warehouse'] = $temp_warhouse;

        $sort_by = $this->input->get('sort_by');
        $sort_order = $this->input->get('sort_order');

        $per_page = $this->input->get('per_page') != '' ? $this->input->get('per_page') : 100;
        $page = $this->input->get('page') != '' ? $this->input->get('page') : 1;

        $offset_no = $page != '' ? ($page - 1) * $per_page : $offset_no;
        
        if ($page != '') {
            $offset_no = ($page - 1) * $per_page;
        } else {
            $offset_no = 0;
        }

        $search = $this->input->get('search');
        $search = $this->db->escape_str($search);

        $temp_url = '';
        if ($page != '') {
            $temp_url .= '?page=';
        }
        if ($per_page != '') {
            $temp_url .= '&per_page='.$per_page;
        }
        if ($_GET['mode'] == 'sample') {
            $temp_url .= '&mode='.$_GET['mode'];
        }
        foreach ($_GET as $name => $value) {
            if ($name != 'page' && $name != 'per_page' && $name != 'mode') {
                if ($value != '') {
                    $temp_url .= '&'.$name.'='.$value;
                }
            }
        }

        $config_data = array(
            'lg' => 'en',
            'table_name' => 'sma_sales',
            'table_id' => 'id',
            'search' => $search,
            'sort_by' => $sort_by,
            'sort_order' => $sort_order,
            'limit' => $per_page,
            'offset_no' => $offset_no,
        );
        $select_data = $this->sales_model->api_get_sale($config_data);


        $config_data = array(
            'lg' => 'en',
            'table_name' => 'sma_sales',
            'table_id' => 'id',
            'search' => $search,
            'sort_by' => '',
            'sort_order' => '',
            'limit' => '',
            'offset_no' => '',
        );
        $temp_count = $this->sales_model->api_get_sale($config_data);
        $total_record = count($temp_count);


        /* config pagination */
        $this->data['select_data'] = $select_data;

        $this->data['page'] = $page;
        $this->data['search'] = $search;
        $this->data['sort_by'] = $sort_by;
        $this->data['sort_order'] = $sort_order;
        $this->data['per_page'] = $per_page;
        $this->data['total_rows_sale'] = $total_record;
        $this->data['index_page'] = 'admin/sales';
        $this->data['show_data'] = $page;
            
        $config_base_url = site_url() . 'admin/sales'; //your url where the content is displayed
        $config['base_url'] = $config_base_url.$temp_url; //your url where the content is displayed
        $config['query_string_segment'] = 'page';
        $config['per_page'] = $per_page; //$per_page;
        $config['num_links'] = 5;
        $config['use_page_numbers'] = true;
        $config['page_query_string'] = true;
        $config['full_tag_open'] = '<ul class="pagination pagination-new-bg">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['prev_link'] = '&lt; Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next &gt;';
        $config['next_tag_open'] = '<li class="next">';
        $config['next_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['total_rows'] = $total_record; //$this->data['total_rows_sale'];
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        /** end config pagination */
        /**Showing Label Data At Table Footer */
        $showing = $total_record != '' ? 1 : 0;
        $label_show_to= ($page == '' || $per_page < $total_record) ?  $per_page  :  $total_record;
        $label_show = "Showing " . $showing . " to ".$label_show_to." of : " . number_format($total_record) . " entries";

        $show_page = $page > 1 ? ($page * $per_page) : $total_record;
        $show_page_no = $show_page > $total_record ? $total_record : $page * $per_page;
        $label_show_page = "Showing " . (($page - 1) * ($per_page) + 1) . " to " . $show_page_no . " (filtered from " . number_format($total_record) . " total entries) ";
        $this->data['show'] = $search != '' || $page > 1 ? $label_show_page : $label_show;

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        $temp = array();
        $temp[0]['value'] = 'pending';
        $temp[0]['name'] = lang('Pending');
        $temp[1]['value'] = 'print needed';
        $temp[1]['name'] = lang('Print_Needed');
        $temp[2]['value'] = 'preparing';
        $temp[2]['name'] = lang('preparing');
        $temp[3]['value'] = 'delivering';
        $temp[3]['name'] = lang('delivering');
        $temp[4]['value'] = 'completed';
        $temp[4]['name'] = lang('completed');
        $this->data['sale_status'] = $temp;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('sales')));
        $meta = array('page_title' => lang('sales'), 'bc' => $bc);
        $this->page_construct('sales/index', $meta, $this->data);
    }

    public function index_bk($warehouse_id = null)
    {
        $this->sma->checkPermissions();
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        if ($this->Owner || $this->Admin || !$this->session->userdata('warehouse_id')) {
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['warehouse_id'] = $warehouse_id;
            $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : null;
        } else {
            $this->data['warehouses'] = null;
            $this->data['warehouse_id'] = $this->session->userdata('warehouse_id');
            $this->data['warehouse'] = $this->session->userdata('warehouse_id') ? $this->site->getWarehouseByID($this->session->userdata('warehouse_id')) : null;
        }
        if (!$this->role_owner) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('sales')));
        $meta = array('page_title' => lang('sales'), 'bc' => $bc);

        /* config default, query data*/
        $sort_by = $this->input->post('sort_by');
        $sort_order = $this->input->post('sort_order');

        $per_page = $this->input->post('per_page') != '' ? $this->input->post('per_page') : 50;
        $page = $this->input->get('page') != '' ? $this->input->get('page') : 1;

        $offset_no = $page != '' ? ($page - 1) * $per_page : $offset_no;

        $search = $this->input->post('search');
        if ($this->input->get('search') != '') {
            $search = $this->input->get('search');
        }
        $search = $this->db->escape_str($search);

        $config_data = array();
        $temp = $this->input->post('sale_status');
        if ($temp != '') {
            $config_data['sale_status'] = $temp;
        }
        $temp = $this->input->get('mode');
        if ($temp != '') {
            $config_data['mode'] = $temp;
        }
                    
        $temp = $this->input->post('start_date');
        if ($temp != '') {
            $config_data['start_date'] = $temp;
        }
        $temp = $this->input->post('end_date');
        if ($temp != '') {
            $config_data['end_date'] = $temp;
        }

        $temp = $this->input->post('order_tax_id');
        if ($temp != '') {
            $config_data['order_tax_id'] = $temp;
        }

        $data_sale = $this->sales_model
                                ->get_paging_data(
                                    $warehouse_id,
                                    $search,
                                    $config_data,
                                    $sort_by,
                                    $sort_order,
                                    $per_page,
                                    $offset_no
                                )->result();
        /*
                $count_data_sale = $this->sales_model
                                    ->get_paging_data(
                                        $warehouse_id,
                                        $search, $config_data
                                    )->num_rows();
        */
        /* config pagination */
        if ($warehouse_id) {
            $config_base_url = site_url() . 'admin/sales/' . $warehouse_id; //your url where the content is displayed
        } else {
            $config_base_url = site_url() . 'admin/sales'; //your url where the content is displayed
        }
        $this->data['per_page'] = $per_page;
        $this->data['sort_by'] = $sort_by;
        $this->data['sort_order'] = $sort_order;
        $this->data['sl_sale'] = $data_sale;
        $this->data['total_rows_sale'] = $count_data_sale;
        $this->data['index_page'] = 'admin/sales/index';
        $this->data['show_data'] = $page;

        $config['base_url'] = $config_base_url; //your url where the content is displayed
        $config['query_string_segment'] = 'page';
        $config['per_page'] = $per_page; //$per_page;
        $config['num_links'] = 5;
        $config['use_page_numbers'] = true;
        $config['page_query_string'] = true;
        $config['full_tag_open'] = '<ul class="pagination pagination-new-bg">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['prev_link'] = '&lt; Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next &gt;';
        $config['next_tag_open'] = '<li class="next">';
        $config['next_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['total_rows'] = $count_data_sale; //$this->data['total_rows_sale'];
        $this->pagination->initialize($config);
        /** end config pagination */
        
        /**Showing Label Data At Table Footer */
        $showing = $count_data_sale != '' ? 1 : 0;
        $label_show_to= ($page == '' || $per_page < $count_data_sale) ?  $per_page  :  $count_data_sale;
        $label_show = "Showing " . $showing . " to ".$label_show_to." of : " . number_format($count_data_sale) . " entries";

        $show_page = $page > 1 ? ($page * $per_page) : $count_data_sale;
        $show_page_no = $show_page > $count_data_sale ? $count_data_sale : $page * $per_page;
        $label_show_page = "Showing " . (($page - 1) * ($per_page) + 1) . " to " . $show_page_no . " (filtered from " . number_format($count_data_sale) . " total entries) ";
        $this->data['show'] = $search != '' || $page > 1 ? $label_show_page : $label_show;
        /** End Showing Label Data At Table Footer */

        $this->page_construct('sales/index', $meta, $this->data);
    } 

    public function getSales($warehouse_id = null)
    {
        $this->sma->checkPermissions('index');

        if ((!$this->Owner || !$this->Admin) && !$warehouse_id) {
            $user = $this->site->getUser();
            $warehouse_id = $user->warehouse_id;
        }
        $detail_link = anchor('admin/sales/view/$1', '<i class="fa fa-file-text-o"></i> ' . lang('sale_details'));
        $duplicate_link = anchor('admin/sales/add?sale_id=$1', '<i class="fa fa-plus-circle"></i> ' . lang('duplicate_sale'));
        $payments_link = anchor('admin/sales/payments/$1', '<i class="fa fa-money"></i> ' . lang('view_payments'), 'data-toggle="modal" data-target="#myModal"');
        $add_payment_link = anchor('admin/sales/add_payment/$1', '<i class="fa fa-money"></i> ' . lang('add_payment'), 'data-toggle="modal" data-target="#myModal"');
        $packagink_link = anchor('admin/sales/packaging/$1', '<i class="fa fa-archive"></i> ' . lang('packaging'), 'data-toggle="modal" data-target="#myModal"');
        $add_delivery_link = anchor('admin/sales/add_delivery/$1', '<i class="fa fa-truck"></i> ' . lang('add_delivery'), 'data-toggle="modal" data-target="#myModal"');
        $email_link = anchor('admin/sales/email/$1', '<i class="fa fa-envelope"></i> ' . lang('email_sale'), 'data-toggle="modal" data-target="#myModal"');
        $edit_link = anchor('admin/sales/edit/$1', '<i class="fa fa-edit"></i> ' . lang('edit_sale'), 'class="sledit"');
        $pdf_link = anchor('admin/sales/pdf/$1', '<i class="fa fa-file-pdf-o"></i> ' . lang('download_pdf'));
        $return_link = anchor('admin/sales/return_sale/$1', '<i class="fa fa-angle-double-left"></i> ' . lang('return_sale'));
        $delete_link = "<a href='#' class='po' title='<b>" . lang("delete_sale") . "</b>' data-content=\"<p>"
        . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('sales/delete/$1') . "'>"
        . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i> "
        . lang('delete_sale') . "</a>";
        $action = '<div class="text-center"><div class="btn-group text-left">'
        . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
        . lang('actions') . ' <span class="caret"></span></button>
        <ul class="dropdown-menu pull-right" role="menu">
            <li>' . $detail_link . '</li>
            <li>' . $duplicate_link . '</li>
            <li>' . $payments_link . '</li>
            <li>' . $add_payment_link . '</li>
            <li>' . $packagink_link . '</li>
            <li>' . $add_delivery_link . '</li>
            <li>' . $edit_link . '</li>
            <li>' . $pdf_link . '</li>
            <li>' . $email_link . '</li>
            <li>' . $return_link . '</li>
            <li>' . $delete_link . '</li>
        </ul>
    </div></div>';
        //$action = '<div class="text-center">' . $detail_link . ' ' . $edit_link . ' ' . $email_link . ' ' . $delete_link . '</div>';

        $this->load->library('datatables');
        if ($warehouse_id) {
            $this->datatables
                ->select("{$this->db->dbprefix('sales')}.id as id, DATE_FORMAT({$this->db->dbprefix('sales')}.date, '%Y-%m-%d %T') as date, reference_no, biller, {$this->db->dbprefix('sales')}.customer, sale_status, grand_total, paid, (grand_total-paid) as balance, payment_status, {$this->db->dbprefix('sales')}.attachment, return_id")
                ->from('sales')
                ->where('warehouse_id', $warehouse_id);
        } else {
            $this->datatables
                ->select("{$this->db->dbprefix('sales')}.id as id, DATE_FORMAT({$this->db->dbprefix('sales')}.date, '%Y-%m-%d %T') as date, reference_no, biller, {$this->db->dbprefix('sales')}.customer, sale_status, grand_total, paid, (grand_total-paid) as balance, payment_status, {$this->db->dbprefix('sales')}.attachment, return_id")
                ->from('sales');
        }
        if ($this->input->get('shop') == 'yes') {
            $this->datatables->where('shop', 1);
        } elseif ($this->input->get('shop') == 'no') {
            $this->datatables->where('shop !=', 1);
        }
        if ($this->input->get('delivery') == 'no') {
            $this->datatables->join('deliveries', 'deliveries.sale_id=sales.id', 'left')
            ->where('sales.sale_status', 'completed')->where('sales.payment_status', 'paid')
            ->where("({$this->db->dbprefix('deliveries')}.status != 'delivered' OR {$this->db->dbprefix('deliveries')}.status IS NULL)", null);
        }
        if ($this->input->get('attachment') == 'yes') {
            $this->datatables->where('payment_status !=', 'paid')->where('attachment !=', null);
        }
        $this->datatables->where('pos !=', 1); // ->where('sale_status !=', 'returned');
        if (!$this->Customer && !$this->Supplier && !$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            $this->datatables->where('created_by', $this->session->userdata('user_id'));
        } elseif ($this->Customer) {
            $this->datatables->where('customer_id', $this->session->userdata('user_id'));
        }
        $this->datatables->add_column("Actions", $action, "id");
        
        echo $this->datatables->generate();
    }

    public function modal_view($id = null)
    {
        
        $this->sma->checkPermissions('index', true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->sales_model->getInvoiceByID($id);
        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by, true);
        }
        $this->data['customer'] = $this->site->getCompanyByID_v2($inv->customer_id);
        if ($this->data['customer']->vat_no) {
            $temp = $this->site->api_select_some_fields_with_where(
                "id
                ",
                "sma_companies",
                "name = 'DAISHIN TRADING (CAMBODIA) CO., LTD'",
                "arr"
            );
            if (count($temp) > 0) {
                $this->data['biller'] = $this->site->getCompanyByID($temp[0]['id']);
            }
        } else {
            $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        }
        $this->data['created_by'] = $this->site->getUser($inv->created_by);
        $this->data['updated_by'] = $inv->updated_by ? $this->site->getUser($inv->updated_by) : null;
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv'] = $inv;

        
        $temp_field = $this->site->api_get_condition_add_ons_field("add_ons", "sma_add_ons", "table_name = 'sma_sales'");
        $temp = $this->site->api_select_some_fields_with_where(
            "id
            ".$temp_field,
            "sma_sales",
            "id = ".$id,
            "arr"
        );
        foreach ($temp[0] as $key => $value) {
            $this->data['inv']->{$key} = $value;
        }

        /*
        $config_data = array(
            'table_name' => 'sma_company_branch',
            'select_table' => 'sma_company_branch',
            'translate' => 'en',
            'select_condition' => "parent_id = ".$this->data['inv']->customer_id." order by id asc",
        );
        $temp = $this->site->api_select_data_v2($config_data);

        if ($temp[0]['id'] > 0) {
            for ($i=0;$i<count($temp);$i++) {
                if ($this->data['inv']->company_branch == '') {
                    $this->data['inv']->company_branch = lang('Branch').': '.$temp[0]['title'];
                    $this->data['inv']->address = $temp[0]['address'];
                    break;
                }
                elseif ($this->data['inv']->company_branch == $temp[$i]['id']) {
                    $this->data['inv']->company_branch = lang('Branch').': '.$temp[$i]['title'];
                    $this->data['inv']->address = $temp[$i]['address'];
                    break;
                }
            }
        }
        */


        if ($this->data['customer']->parent_id != '') {
            $temp = $this->site->api_select_some_fields_with_where(
                "
                *     
                ",
                "sma_companies",
                "id = ".$this->data['customer']->parent_id,
                "arr"
            );
            if (count($temp) > 0) {
                $this->data['inv']->parent_name = $temp[0]['company'];
            }
        }


        $this->data['rows'] = $this->sales_model->getAllInvoiceItems($id);
        
        $this->data['return_sale'] = $inv->return_id ? $this->sales_model->getInvoiceByID($inv->return_id) : null;
        $this->data['return_rows'] = $inv->return_id ? $this->sales_model->getAllInvoiceItems($inv->return_id) : null;

        $this->load->view($this->theme . 'sales/modal_view', $this->data);
        
    }

    public function view($id = null)
    {
        $this->sma->checkPermissions('index');

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->sales_model->getInvoiceByID($id);
        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by);
        }
        $this->data['barcode'] = "<img src='" . admin_url('products/gen_barcode/' . $inv->reference_no) . "' alt='" . $inv->reference_no . "' class='pull-left' />";
        $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
        $this->data['payments'] = $this->sales_model->getPaymentsForSale($id);
        $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        $this->data['created_by'] = $this->site->getUser($inv->created_by);
        $this->data['updated_by'] = $inv->updated_by ? $this->site->getUser($inv->updated_by) : null;
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv'] = $inv;
        $this->data['rows'] = $this->sales_model->getAllInvoiceItems($id);
        $this->data['return_sale'] = $inv->return_id ? $this->sales_model->getInvoiceByID($inv->return_id) : null;
        $this->data['return_rows'] = $inv->return_id ? $this->sales_model->getAllInvoiceItems($inv->return_id) : null;
        $this->data['paypal'] = $this->sales_model->getPaypalSettings();
        $this->data['skrill'] = $this->sales_model->getSkrillSettings();

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('view')));
        $meta = array('page_title' => lang('view_sales_details'), 'bc' => $bc);
        $this->page_construct('sales/view', $meta, $this->data);
    }

    public function pdf($id = null, $view = null, $save_bufffer = null)
    {
        $this->sma->checkPermissions();

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->sales_model->getInvoiceByID($id);

        $config_data = array(
            'table_name' => 'sma_sales',
            'select_table' => 'sma_sales',
            'translate' => '',
            'select_condition' => "id = ".$id,
        );
        $temp = $this->site->api_select_data_v2($config_data);
        for ($i=0;$i<count($temp);$i++) {
            foreach (array_keys($temp[$i]) as $key) {
                $inv->{$key} = $temp[$i][$key];
            }
        }

        if ($_GET['print'] == 1) {
            if ($inv->reference_no != '') {
                if ($inv->sale_status == 'print needed') {
                    $update_array = array(
                        'sale_status' => 'preparing',
                    );
                    $this->site->api_update_table('sma_sales', $update_array, "id = ".$id);

                    /*
                    $temp_riel = $this->site->api_select_some_fields_with_where("rate
                        "
                        ,"sma_currencies"
                        ,"code = 'KHR' order by date desc limit 1"
                        ,"arr"
                    );
                    $config_data_2 = array(
                        'table_name' => 'sma_sales',
                        'id_name' => 'id',
                        'field_add_ons_name' => 'add_ons',
                        'selected_id' => $id,
                        'add_ons_title' => 'kh_currency_rate',
                        'add_ons_value' => $temp_riel[0]['rate'],
                    );
                    $this->site->api_update_add_ons_field($config_data_2);
                    */

                    //$this->api_send_mail_customer_order($id);
                }
            } else {
                $this->session->set_flashdata('error', lang("action_print_error"));
                admin_redirect("sales");
            }
        }
        
        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by);
        }
        $this->data['barcode'] = "<img src='" . admin_url('products/gen_barcode/' . $inv->reference_no) . "' alt='" . $inv->reference_no . "' class='pull-left' />";
        $this->data['customer'] = $this->site->getCompanyByID_v2($inv->customer_id);
        $this->data['payments'] = $this->sales_model->getPaymentsForSale($id);

        if ($this->data['customer']->vat_no) {
            $temp = $this->site->api_select_some_fields_with_where(
                "id
                ",
                "sma_companies",
                "name = 'DAISHIN TRADING (CAMBODIA) CO., LTD'",
                "arr"
            );
            if (count($temp) > 0) {
                $this->data['biller'] = $this->site->getCompanyByID($temp[0]['id']);
            }
        } else {
            $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        }
        
        $temp_type = $this->input->get('type');
        if ($temp_type == 'sl') {
            $temp = $this->site->api_select_some_fields_with_where(
                "id
                ",
                "sma_companies",
                "name = 'DAISHIN TRADING (CAMBODIA) CO., LTD'",
                "arr"
            );
            if (count($temp) > 0) {
                $this->data['biller'] = $this->site->getCompanyByID($temp[0]['id']);
            }
        }
        if ($temp_type == 'do') {
            $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        }

        $this->data['user'] = $this->site->getUser($inv->created_by);
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);

        $this->data['inv'] = $inv;
        $temp_field = $this->site->api_get_condition_add_ons_field("add_ons", "sma_add_ons", "table_name = 'sma_sales'");
        $temp = $this->site->api_select_some_fields_with_where(
            "id
            ".$temp_field,
            "sma_sales",
            "id = ".$id,
            "arr"
        );
        foreach ($temp[0] as $key => $value) {
            $this->data['inv']->{$key} = $value;
        }

        /*
        $config_data = array(
            'table_name' => 'sma_company_branch',
            'select_table' => 'sma_company_branch',
            'translate' => 'en',
            'select_condition' => "parent_id = ".$this->data['inv']->customer_id." order by id asc",
        );
        $temp = $this->site->api_select_data_v2($config_data);

        if ($temp[0]['id'] > 0) {
            for ($i=0;$i<count($temp);$i++) {
                if ($this->data['inv']->company_branch == '') {
                    $this->data['inv']->company_branch = lang('Branch').': '.$temp[0]['title'].'<br>'.$temp[0]['address'];
                    break;
                }
                elseif ($this->data['inv']->company_branch == $temp[$i]['id']) {
                    $this->data['inv']->company_branch = lang('Branch').': '.$temp[$i]['title'].'<br>'.$temp[$i]['address'];
                    break;
                }
            }
        }
        */
                
        $this->data['rows'] = $this->sales_model->getAllInvoiceItems($id);
        $this->data['return_sale'] = $inv->return_id ? $this->sales_model->getInvoiceByID($inv->return_id) : null;
        $this->data['return_rows'] = $inv->return_id ? $this->sales_model->getAllInvoiceItems($inv->return_id) : null;

        if ($this->data['customer']->parent_id != '') {
            $temp = $this->site->api_select_some_fields_with_where(
                "
                *     
                ",
                "sma_companies",
                "id = ".$this->data['customer']->parent_id,
                "arr"
            );
            if (count($temp) > 0) {
                $this->data['inv']->parent_name = $temp[0]['company'];
            }
        }

        if (is_int(strpos($inv->reference_no, "SL")) || $temp_type == 'sl') {
            $temp_riel = $this->site->api_select_some_fields_with_where(
                "*
                ",
                "sma_currencies",
                "code = 'KHR' order by date desc limit 1",
                "arr"
            );
            $this->data['riel_name'] = $temp_riel[0]['name'];

            $temp_field = $this->site->api_get_condition_add_ons_field("add_ons", "sma_add_ons", "table_name = 'sma_sales'");
            $temp_2 = $this->site->api_select_some_fields_with_where(
                "id 
                ".$temp_field,
                "sma_sales",
                "id = ".$id,
                "arr"
            );
            if ($temp_2[0]['kh_currency_rate'] > 0) {
                $temp_riel[0]['rate'] = intval($temp_2[0]['kh_currency_rate']);
            }
            $temp = number_format(intval($temp_riel[0]['rate']), 0);

            $this->data['riel_rate'] = '1$ = '.$temp.$temp_riel[0]['code'];
            $this->data['total_riel'] = $this->sma->api_convert_number($inv->grand_total, 2) * intval($temp_riel[0]['rate']);

            if ($temp_type == 'sl') {
                $temp = ($this->sma->api_convert_number($inv->total, 2) * 10) / 100;
                $this->data['total_riel'] = ($this->sma->api_convert_number($inv->total, 2) + $temp) * intval($temp_riel[0]['rate']);
            }
        }
        
        $temp_ref = $inv->reference_no;
        if (is_int(strpos($inv->reference_no, "SL")) && $temp_type == 'sl') {
            $temp_ref = $inv->reference_no;
        }
        if (is_int(strpos($inv->reference_no, "SL")) && $temp_type == 'do') {
            $temp_ref = 'DO';
        }

        if (is_int(strpos($inv->reference_no, "DO")) && $temp_type == 'sl') {
            $temp_ref = 'SL';
        }
        if (is_int(strpos($inv->reference_no, "DO")) && $temp_type == 'do') {
            $temp_ref = $inv->reference_no;
        }

        $name = lang("sale") . "_" . str_replace('/', '_', $temp_ref) . ".pdf";
        $html = $this->load->view($this->theme . 'sales/pdf', $this->data, true);
        if (! $this->Settings->barcode_img) {
            $html = preg_replace("'\<\?xml(.*)\?\>'", '', $html);
        }

        if ($view) {
            $this->load->view($this->theme . 'sales/pdf', $this->data);
        } elseif ($save_bufffer) {
            return $this->sma->generate_pdf($html, $name, $save_bufffer, $this->data['biller']->invoice_footer);
        } else {
            
            $this->sma->generate_pdf($html, $name, false, $this->data['biller']->invoice_footer);
        }
    }

    public function combine_pdf($sales_id)
    {
        $this->sma->checkPermissions('pdf');

        foreach ($sales_id as $id) {
            if ($id != '') {
                $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
                $inv = $this->sales_model->getInvoiceByID($id);

                $config_data = array(
                    'table_name' => 'sma_sales',
                    'select_table' => 'sma_sales',
                    'translate' => '',
                    'select_condition' => "id = ".$id,
                );
                $temp = $this->site->api_select_data_v2($config_data);
                for ($i=0;$i<count($temp);$i++) {
                    foreach (array_keys($temp[$i]) as $key) {
                        $inv->{$key} = $temp[$i][$key];
                    }
                }
                                
                if (!$this->session->userdata('view_right')) {
                    $this->sma->view_rights($inv->created_by);
                }


                $this->data['barcode'] = "<img src='" . admin_url('products/gen_barcode/' . $inv->reference_no) . "' alt='" . $inv->reference_no . "' class='pull-left' />";
                $this->data['customer'] = $this->site->getCompanyByID_v2($inv->customer_id);
                $this->data['payments'] = $this->sales_model->getPaymentsForSale($id);
        
                if ($this->data['customer']->vat_no) {
                    $temp = $this->site->api_select_some_fields_with_where(
                        "id
                        ",
                        "sma_companies",
                        "name = 'DAISHIN TRADING (CAMBODIA) CO., LTD'",
                        "arr"
                    );
                    if (count($temp) > 0) {
                        $this->data['biller'] = $this->site->getCompanyByID($temp[0]['id']);
                    }
                } else {
                    $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
                }
                
                $this->data['user'] = $this->site->getUser($inv->created_by);
                $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
                $this->data['inv'] = $inv;
                $this->data['rows'] = $this->sales_model->getAllInvoiceItems($id);
                $this->data['return_sale'] = $inv->return_id ? $this->sales_model->getInvoiceByID($inv->return_id) : null;
                $this->data['return_rows'] = $inv->return_id ? $this->sales_model->getAllInvoiceItems($inv->return_id) : null;

                if ($this->data['customer']->parent_id != '') {
                    $temp = $this->site->api_select_some_fields_with_where(
                        "
                        *     
                        ",
                        "sma_companies",
                        "id = ".$this->data['customer']->parent_id,
                        "arr"
                    );
                    if (count($temp) > 0) {
                        $this->data['inv']->parent_name = $temp[0]['company'];
                    }
                }


                if (is_int(strpos($inv->reference_no, "SL"))) {
                    $temp_riel = $this->site->api_select_some_fields_with_where(
                        "*
                        ",
                        "sma_currencies",
                        "code = 'KHR' order by date desc limit 1",
                        "arr"
                    );
                    $this->data['riel_name'] = $temp_riel[0]['name'];
                        
                    $temp_field = $this->site->api_get_condition_add_ons_field("add_ons", "sma_add_ons", "table_name = 'sma_sales'");
                    $temp_2 = $this->site->api_select_some_fields_with_where(
                        "*
                        ".$temp_field,
                        "sma_sales",
                        "id = ".$id,
                        "arr"
                    );
                    if (intval($temp_2[0]['kh_currency_rate']) > 0) {
                        $temp_riel[0]['rate'] = intval($temp_2[0]['kh_currency_rate']);
                    }
                    $temp = number_format(intval($temp_riel[0]['rate']), 0);

                    $this->data['riel_rate'] = '1$ = '.$temp.$temp_riel[0]['code'];
                    $this->data['total_riel'] = $this->sma->api_convert_number($inv->grand_total, 2) * intval($temp_riel[0]['rate']);
                }
                $html_data = $this->load->view($this->theme . 'sales/pdf', $this->data, true);
                if (! $this->Settings->barcode_img) {
                    $html_data = preg_replace("'\<\?xml(.*)\?\>'", '', $html_data);
                }

                $html[] = array(
                    'content' => $html_data,
                    'footer' => $this->data['biller']->invoice_footer,
                );
            }
        }

        $name = lang("Sales_".date('Y-m-d H-i-s')) . ".pdf";
        $this->sma->generate_pdf($html, $name);
    }

    public function email($id = null)
    {
        $this->sma->checkPermissions(false, true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $inv = $this->sales_model->getInvoiceByID($id);
        $this->form_validation->set_rules('to', lang("to") . " " . lang("email"), 'trim|required|valid_email');
        $this->form_validation->set_rules('subject', lang("subject"), 'trim|required');
        $this->form_validation->set_rules('cc', lang("cc"), 'trim|valid_emails');
        $this->form_validation->set_rules('bcc', lang("bcc"), 'trim|valid_emails');
        $this->form_validation->set_rules('note', lang("message"), 'trim');

        if ($this->form_validation->run() == true) {
            if (!$this->session->userdata('view_right')) {
                $this->sma->view_rights($inv->created_by);
            }
            $to = $this->input->post('to');
            $subject = $this->input->post('subject');
            if ($this->input->post('cc')) {
                $cc = $this->input->post('cc');
            } else {
                $cc = null;
            }
            if ($this->input->post('bcc')) {
                $bcc = $this->input->post('bcc');
            } else {
                $bcc = null;
            }
            $customer = $this->site->getCompanyByID($inv->customer_id);
            $biller = $this->site->getCompanyByID($inv->biller_id);
            $this->load->library('parser');
            $parse_data = array(
                'reference_number' => $inv->reference_no,
                'contact_person' => $customer->name,
                'company' => $customer->company && $customer->company != '-' ? '('.$customer->company.')' : '',
                'order_link' => $inv->shop ? shop_url('orders/'.$inv->id.'/'.($this->loggedIn ? '' : $inv->hash)) : base_url(),
                'site_link' => base_url(),
                'site_name' => $this->Settings->site_name,
                'logo' => '<img src="' . base_url() . 'assets/uploads/logos/' . $biller->logo . '" alt="' . ($biller->company != '-' ? $biller->company : $biller->name) . '"/>',
            );
            $msg = $this->input->post('note');
            $message = $this->parser->parse_string($msg, $parse_data);
            $paypal = $this->sales_model->getPaypalSettings();
            $skrill = $this->sales_model->getSkrillSettings();
            $btn_code = '<div id="payment_buttons" class="text-center margin010">';
            if ($paypal->active == "1" && $inv->grand_total != "0.00") {
                if (trim(strtolower($customer->country)) == $biller->country) {
                    $paypal_fee = $paypal->fixed_charges + ($inv->grand_total * $paypal->extra_charges_my / 100);
                } else {
                    $paypal_fee = $paypal->fixed_charges + ($inv->grand_total * $paypal->extra_charges_other / 100);
                }
                $btn_code .= '<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business=' . $paypal->account_email . '&item_name=' . $inv->reference_no . '&item_number=' . $inv->id . '&image_url=' . base_url() . 'assets/uploads/logos/' . $this->Settings->logo . '&amount=' . (($inv->grand_total - $inv->paid) + $paypal_fee) . '&no_shipping=1&no_note=1&currency_code=' . $this->default_currency->code . '&bn=FC-BuyNow&rm=2&return=' . admin_url('sales/view/' . $inv->id) . '&cancel_return=' . admin_url('sales/view/' . $inv->id) . '&notify_url=' . admin_url('payments/paypalipn') . '&custom=' . $inv->reference_no . '__' . ($inv->grand_total - $inv->paid) . '__' . $paypal_fee . '"><img src="' . base_url('assets/images/btn-paypal.png') . '" alt="Pay by PayPal"></a> ';
            }
            if ($skrill->active == "1" && $inv->grand_total != "0.00") {
                if (trim(strtolower($customer->country)) == $biller->country) {
                    $skrill_fee = $skrill->fixed_charges + ($inv->grand_total * $skrill->extra_charges_my / 100);
                } else {
                    $skrill_fee = $skrill->fixed_charges + ($inv->grand_total * $skrill->extra_charges_other / 100);
                }
                $btn_code .= ' <a href="https://www.moneybookers.com/app/payment.pl?method=get&pay_to_email=' . $skrill->account_email . '&language=EN&merchant_fields=item_name,item_number&item_name=' . $inv->reference_no . '&item_number=' . $inv->id . '&logo_url=' . base_url() . 'assets/uploads/logos/' . $this->Settings->logo . '&amount=' . (($inv->grand_total - $inv->paid) + $skrill_fee) . '&return_url=' . admin_url('sales/view/' . $inv->id) . '&cancel_url=' . admin_url('sales/view/' . $inv->id) . '&detail1_description=' . $inv->reference_no . '&detail1_text=Payment for the sale invoice ' . $inv->reference_no . ': ' . $inv->grand_total . '(+ fee: ' . $skrill_fee . ') = ' . $this->sma->formatMoney($inv->grand_total + $skrill_fee) . '&currency=' . $this->default_currency->code . '&status_url=' . admin_url('payments/skrillipn') . '"><img src="' . base_url('assets/images/btn-skrill.png') . '" alt="Pay by Skrill"></a>';
            }

            $btn_code .= '<div class="clearfix"></div></div>';
            $message = $message . $btn_code;
            $attachment = $this->pdf($id, null, 'S');

            try {
                if ($this->sma->send_email($to, $subject, $message, null, null, $attachment, $cc, $bcc)) {
                    delete_files($attachment);
                    $this->session->set_flashdata('message', lang("email_sent"));
                    admin_redirect("sales");
                }
            } catch (Exception $e) {
                $this->session->set_flashdata('error', $e->getMessage());
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } elseif ($this->input->post('send_email')) {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->session->set_flashdata('error', $this->data['error']);
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            if (file_exists('./themes/' . $this->Settings->theme . '/admin/views/email_templates/sale.html')) {
                $sale_temp = file_get_contents('themes/' . $this->Settings->theme . '/admin/views/email_templates/sale.html');
            } else {
                $sale_temp = file_get_contents('./themes/default/admin/views/email_templates/sale.html');
            }

            $this->data['subject'] = array('name' => 'subject',
                'id' => 'subject',
                'type' => 'text',
                'value' => $this->form_validation->set_value('subject', lang('invoice') . ' (' . $inv->reference_no . ') ' . lang('from') . ' ' . $this->Settings->site_name),
            );
            $this->data['note'] = array('name' => 'note',
                'id' => 'note',
                'type' => 'text',
                'value' => $this->form_validation->set_value('note', $sale_temp),
            );
            $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);

            $this->data['id'] = $id;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'sales/email', $this->data);
        }
    }

    /* ------------------------------------------------------------------ */

    public function add($quote_id = null)
    {
        $this->sma->checkPermissions();
        $sale_id = $this->input->get('sale_id') ? $this->input->get('sale_id') : null;

        $this->form_validation->set_message('is_natural_no_zero', lang("no_zero_required"));
        $this->form_validation->set_rules('customer', lang("customer"), 'required');
        $this->form_validation->set_rules('biller', lang("biller"), 'required');
        $this->form_validation->set_rules('sale_status', lang("sale_status"), 'required');
        $this->form_validation->set_rules('payment_status', lang("payment_status"), 'required');

        if ($this->form_validation->run() == true) {
            $customer_id = $this->input->post('customer');//18-01-2019
            $customers = $this->site->getCompanyByID($customer_id);//18-01-2019
            $is_vat = $customers->vat_no;

            $date = $this->input->post('date');
            if ($date == '') {
                $date = date('Y-m-d H:i:s');
            } else {
                $date = $this->sma->fld(trim($this->input->post('date').':'.date('s')));
            }

            $delivery_date = '';
            if ($this->input->post('delivery_date') != '') {
                $delivery_date = $this->sma->fld(trim($this->input->post('delivery_date')));
            }
            
            $warehouse_id = $this->input->post('warehouse');
            //$customer_id = $this->input->post('customer');//18-01-2019
            $biller_id = $this->input->post('biller');
            $total_items = $this->input->post('total_items');
            $sale_status = $this->input->post('sale_status');
            $payment_status = $this->input->post('payment_status');

            $payment_term = $this->input->post('payment_term');
            $due_date = $payment_term ? date('Y-m-d', strtotime('+' . $payment_term . ' days', strtotime($date))) : null;
            $shipping = $this->input->post('shipping') ? $this->input->post('shipping') : 0;
            $customer_details = $this->site->getCompanyByID_v2($customer_id);
            $customer = !empty($customer_details->company) && $customer_details->company != '-' ? $customer_details->company : $customer_details->name;
            $biller_details = $this->site->getCompanyByID($biller_id);
            $biller = !empty($biller_details->company) && $biller_details->company != '-' ? $biller_details->company : $biller_details->name;
            //$note = $this->sma->clear_tags($this->input->post('note'));
            //$staff_note = $this->sma->clear_tags($this->input->post('staff_note'));
            $note = $this->input->post('note');
            $staff_note = $this->input->post('staff_note');

            $quote_id = $this->input->post('quote_id') ? $this->input->post('quote_id') : null;

            $total = 0;
            $product_tax = 0;
            $product_discount = 0;
            $digital = false;
            $gst_data = [];
            $total_cgst = $total_sgst = $total_igst = 0;
            $i = isset($_POST['product_code']) ? sizeof($_POST['product_code']) : 0;
            for ($r = 0; $r < $i; $r++) {                
                $item_id = $_POST['product_id'][$r];
                $item_type = $_POST['product_type'][$r];
                $item_code = $_POST['product_code'][$r];
                $item_name = $_POST['product_name'][$r];
                $item_option = isset($_POST['product_option'][$r]) && $_POST['product_option'][$r] != 'false' && $_POST['product_option'][$r] != 'null' ? $_POST['product_option'][$r] : null;
                $real_unit_price = $this->sma->formatDecimal($_POST['real_unit_price'][$r]);
                $unit_price = $this->sma->formatDecimal($_POST['unit_price'][$r]);
                $item_unit_quantity = $_POST['quantity'][$r];
                $item_serial = isset($_POST['serial'][$r]) ? $_POST['serial'][$r] : '';
                $item_tax_rate = isset($_POST['product_tax'][$r]) ? $_POST['product_tax'][$r] : null;
                $item_discount = isset($_POST['product_discount'][$r]) ? $_POST['product_discount'][$r] : null;
                $item_unit = $_POST['product_unit'][$r];
                $item_quantity = $_POST['product_base_quantity'][$r];

                if (isset($item_code) && isset($real_unit_price) && isset($unit_price) && isset($item_quantity)) {
                    $product_details = $item_type != 'manual' ? $this->sales_model->getProductByCode($item_code) : null;
                    // $unit_price = $real_unit_price;
                    if ($item_type == 'digital') {
                        $digital = true;
                    }
                    $pr_discount = $this->site->calculateDiscount($item_discount, $unit_price);
                    $unit_price = $this->sma->formatDecimal($unit_price - $pr_discount);
                    $item_net_price = $unit_price;
                    $pr_item_discount = $this->sma->formatDecimal($pr_discount * $item_unit_quantity);
                    $product_discount += $pr_item_discount;
                    $pr_item_tax = $item_tax = 0;
                    $tax = "";

                    if (isset($item_tax_rate) && $item_tax_rate != 0) {
                        $tax_details = $this->site->getTaxRateByID($item_tax_rate);
                        $ctax = $this->site->calculateTax($product_details, $tax_details, $unit_price);
                        $item_tax = $ctax['amount'];
                        $tax = $ctax['tax'];
                        if (!$product_details || (!empty($product_details) && $product_details->tax_method != 1)) {
                            $item_net_price = $unit_price - $item_tax;
                        }
                        $pr_item_tax = $this->sma->formatDecimal(($item_tax * $item_unit_quantity), 4);
                        if ($this->Settings->indian_gst && $gst_data = $this->gst->calculteIndianGST($pr_item_tax, ($biller_details->state == $customer_details->state), $tax_details)) {
                            $total_cgst += $gst_data['cgst'];
                            $total_sgst += $gst_data['sgst'];
                            $total_igst += $gst_data['igst'];
                        }
                    }

                    $config_data = array(
                        'id' => $item_id,
                        'customer_id' => $customer->id,
                    );
                    $temp = $this->site->api_calculate_product_price($config_data);
                    $item_net_price = $temp['price'];
                    if ($temp['promotion']['price'] > 0)
                        $temp_discount = $temp['temp_price'] - $temp['price'];
                    else
                        $temp_discount = 0;
                    
                    $row_temp_original_price = $this->sma->formatDecimal($temp['temp_price']);
                    $row_temp_unit_price = $temp['price'];
                    $row_temp_discount = $temp_discount;
                    $row_unit_price = $temp['price'];
                    $row_real_unit_price = $row_temp_original_price;
                    $row_temp_discount_rate = '';
                    if ($temp_discount > 0) {
                        $row_temp_discount_rate = $this->sma->formatMoney($temp_discount).' ('.number_format($temp['promotion']['rate'],2).'%)';
                        $row_temp_discount_rate_value = $temp['promotion']['rate'];
                    }                    

                    if ($temp_discount > 0)
                        $temp_2 = $temp['temp_price'];
                    else
                        $temp_2 = $temp['price'];
                    $temp_2 = $temp_2 * $item_unit_quantity;
                    $temp_total_before_discount += $temp_2;
                    $temp_2 = ($temp_2 * 5) / 100;
                    $temp_service_charge += $temp_2;

                    $product_tax += $pr_item_tax;
                    $subtotal = (round($item_net_price * $item_unit_quantity, 2, PHP_ROUND_HALF_UP) + $pr_item_tax);
                    $unit = $this->site->getUnitByID($item_unit);
                    $add_ons = 'initial_first_add_ons:{}:supplier:{}:discount_rate:{'.$row_temp_discount_rate_value.'}:';

                    if ($temp_discount <= 0)
                        $temp_unit_price = $temp['price'];
                    else
                        $temp_unit_price = $this->sma->formatDecimal($temp['temp_price']);

                    $product = array(
                        'product_id' => $item_id,
                        'product_code' => $item_code,
                        'product_name' => $item_name,
                        'product_type' => $item_type,
                        'option_id' => $item_option,
                        'net_unit_price' => $this->sma->formatDecimal($row_temp_original_price),
                        'unit_price' => $this->sma->formatDecimal($row_temp_unit_price),
                        'quantity' => $item_quantity,
                        'product_unit_id' => $unit ? $unit->id : null,
                        'product_unit_code' => $unit ? $unit->code : null,
                        'unit_quantity' => $item_unit_quantity,
                        'warehouse_id' => $warehouse_id,
                        'item_tax' => $pr_item_tax,
                        'tax_rate_id' => $item_tax_rate,
                        'tax' => $tax,
                        'discount' => $row_temp_discount,
                        'item_discount' => $pr_item_discount,
                        'subtotal' => $this->sma->formatDecimal($subtotal),
                        'serial_no' => $item_serial,
                        'real_unit_price' => $this->sma->formatDecimal($row_temp_unit_price),
                        'add_ons' => $add_ons,
                    );

                    $products[] = ($product + $gst_data);
                    $total += $this->sma->formatDecimal(round($item_net_price * $item_unit_quantity, 2, PHP_ROUND_HALF_UP), 4);
                }
            }
            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("order_items"), 'required');
            } else {
                krsort($products);
            }

            $order_discount = $this->site->calculateDiscount($this->input->post('order_discount'), ($total + $product_tax));
            $total_discount = $this->sma->formatDecimal(($order_discount + $product_discount), 4);

            // if ($customer_details->vat_no && $customer_details->vat_invoice_type != 'do') {
            //     $temp_order_tax = 2;
            // } else {
            //     $temp_order_tax = 1;
            // }
            // $order_tax = $this->site->calculateOrderTax($temp_order_tax, ($total + $product_tax - $order_discount));
            // $total_tax = $this->sma->formatDecimal(($product_tax + $order_tax), 4);
            // $grand_total = $this->sma->formatDecimal(($total + $total_tax + $this->sma->formatDecimal($shipping) - $order_discount), 4);
            
            $add_ons = 'initial_first_add_ons:{}:kh_currency_rate:{}:delivery_date:{'.$delivery_date.'}:mode:{'.$this->input->post('mode').'}:company_branch:{'.$this->input->post('company_branch_id').'}:po_number:{'.$this->input->post('po_number').'}:cashier:{'.$this->input->post('cashier').'}:';
            foreach ($_POST as $name => $value) {
                if (is_int(strpos($name, "add_ons_"))) {
                    $temp_name = str_replace('add_ons_', '', $name);
                    $add_ons .= $temp_name.':{'.$value.'}:';
                }
            }


            $sample = $this->input->post('mode');
            if ($sample == 'sample') {
                $sample = 1;
            } else {
                $sample = 0;
            }
            
            if ($this->input->post('address') == '') {
                $config_data = array(
                    'table_name' => 'sma_companies',
                    'select_table' => 'sma_companies',
                    'translate' => '',
                    'description' => '',
                    'select_condition' => "id = ".$customer_id,
                );
                $temp = $this->api_helper->api_select_data_v2($config_data);

                if ($temp[0]['distance_from_restaurant'] != '') {
                    $shipping = $this->api_helper->calculate_delivery_fee($temp[0]['distance_from_restaurant']);
                }
                else
                    $shipping = 0;
            }

            $temp_total_before_discount = $this->sma->formatDecimal($temp_total_before_discount, 2);
            $total = $this->sma->formatDecimal($total, 2);
            $temp_service_charge = $this->sma->formatDecimal($temp_service_charge, 2);            
            $shipping = $this->sma->formatDecimal($shipping, 2);
            $temp_service_charge = 0;

            $temp_vat = ($temp_total_before_discount + $temp_service_charge + $shipping) * 10 / 100;
            $temp_vat = $this->sma->formatDecimal($temp_vat, 2);
            $temp_vat = 0;
            
            $grand_total = $total + $temp_service_charge + $temp_vat + $shipping;
            $grand_total = $this->api_helper->round_up_second_decimal($grand_total);
            $grand_total = $this->sma->formatDecimal($grand_total, 4);            

            // echo $temp_total_before_discount.'<br>';
            // echo $temp_service_charge.'<br>';
            // echo $shipping.'<br>';
            // echo $temp_vat.'<br>';
            // echo $grand_total.'<br>';
            // exit;

            $data = array(
                'is_vat' => $is_vat,
                'date' => $date,
                'customer_id' => $customer_id,
                'customer' => $customer,
                'biller_id' => $biller_id,
                'biller' => $biller,
                'warehouse_id' => $warehouse_id,
                'note' => $note,
                'staff_note' => $staff_note,
                'total' => $total,
                'product_discount' => $product_discount,
                'order_discount_id' => $this->input->post('order_discount'),
                'order_discount' => $order_discount,
                'total_discount' => $total_discount,
                'product_tax' => $product_tax,
                'order_tax_id' => $temp_order_tax,
                'order_tax' => $temp_vat,
                'total_tax' => $temp_vat,
                'shipping' => $this->sma->formatDecimal($shipping),
                'grand_total' => $grand_total,
                'total_items' => $total_items,
                'sale_status' => $sale_status,
                'payment_status' => $payment_status,
                'payment_term' => $payment_term,
                'due_date' => $due_date,
                'paid' => 0,
                'created_by' => $this->session->userdata('user_id'),
                'hash' => hash('sha256', microtime() . mt_rand()),
                'sample' => $sample,
                'service_charge' => $temp_service_charge,
                'add_ons' => $add_ons,
            );
            if ($this->Settings->indian_gst) {
                $data['cgst'] = $total_cgst;
                $data['sgst'] = $total_sgst;
                $data['igst'] = $total_igst;
            }

            if ($payment_status == 'partial' || $payment_status == 'paid' || $payment_status == 'AR') {
                if ($this->input->post('paid_by') == 'deposit') {
                    if (! $this->site->check_customer_deposit($customer_id, $this->input->post('amount-paid'))) {
                        $this->session->set_flashdata('error', lang("amount_greater_than_deposit"));
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                }
                if ($this->input->post('paid_by') == 'gift_card') {
                    $gc = $this->site->getGiftCardByNO($this->input->post('gift_card_no'));
                    $amount_paying = $grand_total >= $gc->balance ? $gc->balance : $grand_total;
                    $gc_balance = $gc->balance - $amount_paying;
                    $payment = array(
                        'date' => $date,
                        'reference_no' => $this->input->post('payment_reference_no'),
                        'amount' => $this->sma->formatDecimal($amount_paying),
                        'paid_by' => $this->input->post('paid_by'),
                        'cheque_no' => $this->input->post('cheque_no'),
                        'cc_no' => $this->input->post('gift_card_no'),
                        'cc_holder' => $this->input->post('pcc_holder'),
                        'cc_month' => $this->input->post('pcc_month'),
                        'cc_year' => $this->input->post('pcc_year'),
                        'cc_type' => $this->input->post('pcc_type'),
                        'created_by' => $this->session->userdata('user_id'),
                        'note' => $this->input->post('payment_note'),
                        'type' => 'received',
                        'gc_balance' => $gc_balance,
                    );
                } else {
                    
                    if ($payment_status != 'AR') {
                        $temp = $this->sma->formatDecimal($this->input->post('amount-paid'));
                        $temp_2 = $this->input->post('paid_by');
                    }
                    else {
                        $temp = 0;          
                        $temp_2 = 'AR';
                    }          
                    $payment = array(
                        'date' => $date,
                        'reference_no' => $this->input->post('payment_reference_no'),
                        'amount' => $temp,
                        'paid_by' => $temp_2,
                        'cheque_no' => $this->input->post('cheque_no'),
                        'cc_no' => $this->input->post('pcc_no'),
                        'cc_holder' => $this->input->post('pcc_holder'),
                        'cc_month' => $this->input->post('pcc_month'),
                        'cc_year' => $this->input->post('pcc_year'),
                        'cc_type' => $this->input->post('pcc_type'),
                        'created_by' => $this->session->userdata('user_id'),
                        'note' => $this->input->post('payment_note'),
                        'type' => 'received',
                    );
                }
            } else {
                $payment = array();
            }

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }
        }
        if ($this->form_validation->run() == true && $this->sales_model->addSale($data, $products, $payment)) {
            $this->session->set_userdata('remove_slls', 1);
            if ($quote_id) {
                $this->db->update('quotes', array('status' => 'completed'), array('id' => $quote_id));
            }
            $this->session->set_flashdata('message', lang("sale_added"));
            if ($this->input->post('mode') != 'sample') {
                admin_redirect("sales");
            } else {
                admin_redirect("sales?mode=sample");
            }
        } else {
            if ($quote_id || $sale_id) {
                if ($quote_id) {
                    $this->data['quote'] = $this->sales_model->getQuoteByID($quote_id);
                    $items = $this->sales_model->getAllQuoteItems($quote_id);
                } elseif ($sale_id) {
                    $this->data['quote'] = $this->sales_model->getInvoiceByID($sale_id);
                    $items = $this->sales_model->getAllInvoiceItems($sale_id);
                }
                krsort($items);
                $c = rand(100000, 9999999);
                foreach ($items as $item) {
                    $row = $this->site->getProductByID($item->product_id);
                    if (!$row) {
                        $row = json_decode('{}');
                        $row->tax_method = 0;
                    } else {
                        unset($row->cost, $row->details, $row->product_details, $row->image, $row->barcode_symbology, $row->cf1, $row->cf2, $row->cf3, $row->cf4, $row->cf5, $row->cf6, $row->supplier1price, $row->supplier2price, $row->cfsupplier3price, $row->supplier4price, $row->supplier5price, $row->supplier1, $row->supplier2, $row->supplier3, $row->supplier4, $row->supplier5, $row->supplier1_part_no, $row->supplier2_part_no, $row->supplier3_part_no, $row->supplier4_part_no, $row->supplier5_part_no);
                    }
                    $row->quantity = 0;
                    $pis = $this->site->getPurchasedItems($item->product_id, $item->warehouse_id, $item->option_id);
                    if ($pis) {
                        foreach ($pis as $pi) {
                            $row->quantity += $pi->quantity_balance;
                        }
                    }
                    $row->id = $item->product_id;
                    $row->code = $item->product_code;
                    $row->name = $item->product_name;
                    $row->type = $item->product_type;
                    $row->qty = $item->quantity;
                    $row->base_quantity = $item->quantity;
                    $row->base_unit = $row->unit ? $row->unit : $item->product_unit_id;
                    $row->base_unit_price = $row->price ? $row->price : $item->unit_price;
                    $row->unit = $item->product_unit_id;
                    $row->qty = $item->unit_quantity;
                    $row->discount = $item->discount ? $item->discount : '0';
                    $row->price = $this->sma->formatDecimal($item->net_unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity));
                    $row->unit_price = $row->tax_method ? $item->unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity) + $this->sma->formatDecimal($item->item_tax / $item->quantity) : $item->unit_price + ($item->item_discount / $item->quantity);
                    $row->real_unit_price = $item->real_unit_price;
                    $row->tax_rate = $item->tax_rate_id;
                    $row->serial = '';
                    $row->option = $item->option_id;
                    $options = $this->sales_model->getProductOptions($row->id, $item->warehouse_id);
                    if ($options) {
                        $option_quantity = 0;
                        foreach ($options as $option) {
                            $pis = $this->site->getPurchasedItems($row->id, $item->warehouse_id, $item->option_id);
                            if ($pis) {
                                foreach ($pis as $pi) {
                                    $option_quantity += $pi->quantity_balance;
                                }
                            }
                            if ($option->quantity > $option_quantity) {
                                $option->quantity = $option_quantity;
                            }
                        }
                    }
                    $combo_items = false;
                    if ($row->type == 'combo') {
                        $combo_items = $this->sales_model->getProductComboItems($row->id, $item->warehouse_id);
                    }
                    $units = $this->site->getUnitsByBUID($row->base_unit);
                    $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                    $ri = $this->Settings->item_addition ? $row->id : $c;

                    $pr[$ri] = array('id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")",
                            'row' => $row, 'combo_items' => $combo_items, 'tax_rate' => $tax_rate, 'units' => $units, 'options' => $options);
                    $c++;
                }
                $this->data['quote_items'] = json_encode($pr);
            }

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['quote_id'] = $quote_id ? $quote_id : $sale_id;
            $this->data['billers'] = $this->site->getAllCompanies('biller');
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->data['units'] = $this->site->getAllBaseUnits();
            //$this->data['currencies'] = $this->sales_model->getAllCurrencies();
            $this->data['slnumber'] = ''; //$this->site->getReference('so');
            $this->data['payment_ref'] = ''; //$this->site->getReference('pay');
            $temp = $this->site->api_select_some_fields_with_where(
                "company
                ",
                "sma_companies",
                "id > 0 order by company asc",
                "obj"
            );
            $this->data['company'] = $temp;
            $this->data['delivery_person'] = $this->sales_model->get_delivery_person();
            $this->load->admin_model('companies_model');
            $this->data['sales_person'] = $this->companies_model->getAllSalePerson();
            
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('add_sale')));
            $meta = array('page_title' => lang('add_sale'), 'bc' => $bc);
            $this->page_construct('sales/add', $meta, $this->data);
        }
    }

    /* ------------------------------------------------------------------------ */

    public function edit($id = null)
    {
        $this->sma->checkPermissions();

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $inv = $this->sales_model->getInvoiceByID($id);
        
        $temp_return = $this->site->api_select_some_fields_with_where(
            "id,getTranslate(add_ons,'sale_id','".f_separate."','".v_separate."') as sale_id
            ",
            "sma_returns",
            "getTranslate(add_ons,'sale_id','".f_separate."','".v_separate."') = ".$id,
            "arr"
        );
        if (count($temp_return) > 0) {
            $this->session->set_flashdata('error', lang('sale_x_action'));
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'welcome');
        }

        if ($inv->sale_status == 'returned' || $inv->return_id || $inv->return_sale_ref) {
            $this->session->set_flashdata('error', lang('sale_x_action'));
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'welcome');
        }
        
        if (!$this->session->userdata('edit_right')) {
            $this->sma->view_rights($inv->created_by);
        }
        $this->form_validation->set_message('is_natural_no_zero', lang("no_zero_required"));
        $this->form_validation->set_rules('customer', lang("customer"), 'required');
        $this->form_validation->set_rules('biller', lang("biller"), 'required');
        
        if ($this->form_validation->run() == true) {
            $customer_id = $this->input->post('customer');//18-01-2019
            $customers = $this->site->getCompanyByID($customer_id);//18-01-2019
            $is_vat = $customers->vat_no;

            $reference = $this->input->post('reference_no');
            
            if ($this->Owner || $this->Admin) {
                $temp = substr($inv->date, -2);
                $date = $this->sma->fld(trim($this->input->post('date').':'.$temp));
            } else {
                $date = $inv->date;
            }

            $delivery_date = '';
            if ($this->input->post('delivery_date') != '') {
                $delivery_date = $this->sma->fld(trim($this->input->post('delivery_date')));
            }
            
            $warehouse_id = $this->input->post('warehouse');
            //$customer_id = $this->input->post('customer');
            $biller_id = $this->input->post('biller');
            $total_items = $this->input->post('total_items');
            $sale_status = $this->input->post('sale_status');
            $kh_currency_rate = $this->input->post('kh_currency_rate');
            $payment_status = $this->input->post('payment_status');
            $payment_term = $this->input->post('payment_term');
            $due_date = $payment_term ? date('Y-m-d', strtotime('+' . $payment_term . ' days', strtotime($date))) : null;
            $shipping = $this->input->post('shipping') ? $this->input->post('shipping') : 0;
            $customer_details = $this->site->getCompanyByID_v2($customer_id);
            $customer = !empty($customer_details->company) && $customer_details->company != '-'  ? $customer_details->company : $customer_details->name;
            $biller_details = $this->site->getCompanyByID($biller_id);
            $biller = !empty($biller_details->company) && $biller_details->company != '-' ? $biller_details->company : $biller_details->name;
            //$note = $this->sma->clear_tags($this->input->post('note'));
            //$staff_note = $this->sma->clear_tags($this->input->post('staff_note'));
            $note = $this->input->post('note');
            $staff_note = $this->input->post('staff_note');
            

            $total = 0;
            $product_tax = 0;
            $product_discount = 0;
            $gst_data = [];
            $total_cgst = $total_sgst = $total_igst = 0;
            $i = isset($_POST['product_code']) ? sizeof($_POST['product_code']) : 0;
            //$this->sma->print_arrays($_POST);
            for ($r = 0; $r < $i; $r++) {
                
                $item_id = $_POST['product_id'][$r];
                $item_type = $_POST['product_type'][$r];
                $item_code = $_POST['product_code'][$r];
                $item_name = $_POST['product_name'][$r];
                $item_option = isset($_POST['product_option'][$r]) && $_POST['product_option'][$r] != 'false' && $_POST['product_option'][$r] != 'null' ? $_POST['product_option'][$r] : null;
                $real_unit_price = $this->sma->formatDecimal($_POST['real_unit_price'][$r]);
                $unit_price = $this->sma->formatDecimal($_POST['unit_price'][$r]);
                $item_unit_quantity = $_POST['quantity'][$r];
                $item_serial = isset($_POST['serial'][$r]) ? $_POST['serial'][$r] : '';
                $item_tax_rate = isset($_POST['product_tax'][$r]) ? $_POST['product_tax'][$r] : null;
                $item_discount = isset($_POST['product_discount'][$r]) ? $_POST['product_discount'][$r] : null;
                $item_unit = $_POST['product_unit'][$r];
                $item_quantity = $_POST['product_base_quantity'][$r];

                if (isset($item_code) && isset($real_unit_price) && isset($unit_price) && isset($item_quantity)) {
                    $product_details = $item_type != 'manual' ? $this->sales_model->getProductByCode($item_code) : null;

                    $pr_discount = $this->site->calculateDiscount($item_discount, $unit_price);
                    $unit_price = $this->sma->formatDecimal($unit_price - $pr_discount);
                    $item_net_price = $unit_price;
                    $pr_item_discount = $this->sma->formatDecimal($pr_discount * $item_unit_quantity);
                    $product_discount += $pr_item_discount;
                    $pr_item_tax = $item_tax = 0;
                    $tax = "";

                    if (isset($item_tax_rate) && $item_tax_rate != 0) {
                        $tax_details = $this->site->getTaxRateByID($item_tax_rate);
                        $ctax = $this->site->calculateTax($product_details, $tax_details, $unit_price);
                        $item_tax = $ctax['amount'];
                        $tax = $ctax['tax'];
                        if (!$product_details || (!empty($product_details) && $product_details->tax_method != 1)) {
                            $item_net_price = $unit_price - $item_tax;
                        }
                        $pr_item_tax = $this->sma->formatDecimal(($item_tax * $item_unit_quantity), 4);
                        if ($this->Settings->indian_gst && $gst_data = $this->gst->calculteIndianGST($pr_item_tax, ($biller_details->state == $customer_details->state), $tax_details)) {
                            $total_cgst += $gst_data['cgst'];
                            $total_sgst += $gst_data['sgst'];
                            $total_igst += $gst_data['igst'];
                        }
                    }

                    $product_tax += $pr_item_tax;
                    $subtotal = (round($item_net_price * $item_unit_quantity, 2, PHP_ROUND_HALF_UP) + $pr_item_tax);
                    $unit = $this->site->getUnitByID($item_unit);
                    
                    $add_ons = 'initial_first_add_ons:{}:supplier:{}:discount_rate:{'.$_POST['add_ons_discount_rate'][$r].'}:';

                    if ($_POST['discount'][$r] > 0)
                        $temp_2 = $_POST['net_price'][$r];
                    else
                        $temp_2 = $_POST['unit_price'][$r];    
                    $temp_2 = $temp_2 * $item_unit_quantity;
                    $temp_total_before_discount += $temp_2;
                                        
                    $product = array(
                        'product_id' => $item_id,
                        'product_code' => $item_code,
                        'product_name' => $item_name,
                        'product_type' => $item_type,
                        'option_id' => $item_option,
                        'net_unit_price' => $this->sma->formatDecimal($_POST['net_price'][$r]),
                        'unit_price' => $this->sma->formatDecimal($item_net_price + $item_tax),
                        'quantity' => $item_quantity,
                        'product_unit_id' => $unit ? $unit->id : null,
                        'product_unit_code' => $unit ? $unit->code : null,
                        'unit_quantity' => $item_unit_quantity,
                        'warehouse_id' => $warehouse_id,
                        'item_tax' => $pr_item_tax,
                        'tax_rate_id' => $item_tax_rate,
                        'tax' => $tax,
                        'discount' => $this->sma->formatDecimal($_POST['discount'][$r]),
                        'item_discount' => $pr_item_discount,
                        'subtotal' => $this->sma->formatDecimal($subtotal),
                        'serial_no' => $item_serial,
                        'real_unit_price' => $this->sma->formatDecimal($_POST['net_price'][$r]),
                        'add_ons' => $add_ons,
                    );

                    $products[] = ($product + $gst_data);
                    $total += $this->sma->formatDecimal(round($item_net_price * $item_unit_quantity, 2, PHP_ROUND_HALF_UP), 4);
                }
            }
            
            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("order_items"), 'required');
            } else {
                krsort($products);
            }

            $order_discount = $this->site->calculateDiscount($this->input->post('order_discount'), ($total + $product_tax));
            $total_discount = $this->sma->formatDecimal(($order_discount + $product_discount), 4);
            
            // if ($customer_details->vat_no && $customer_details->vat_invoice_type != 'do')
            //     $temp_order_tax = 2;
            // else
            //     $temp_order_tax = 1;

            $temp_order_tax_id = $this->input->post('order_tax_id');
            if ($temp_order_tax_id <= 0) {
                $temp_order_tax_id = 1;
            }
            $order_tax = $this->site->calculateOrderTax($temp_order_tax_id, ($total + $product_tax - $order_discount));
            $total_tax = $this->sma->formatDecimal(($product_tax + $order_tax), 4);
            $grand_total = $this->sma->formatDecimal(($total + $total_tax + $this->sma->formatDecimal($shipping) - $order_discount), 4);
                
            $shipping = $inv->shipping;

                        
            $temp_service_charge = ($temp_total_before_discount * 5) / 100;
            
            $temp_total_before_discount = $this->sma->formatDecimal($temp_total_before_discount, 2);
            $total = $this->sma->formatDecimal($total, 2);
            $temp_service_charge = $this->sma->formatDecimal($temp_service_charge, 2);
            $temp_service_charge = 0;
            $shipping = $this->sma->formatDecimal($shipping, 2);

            $temp_vat = ($temp_total_before_discount + $temp_service_charge + $shipping) * 10 / 100;
            $temp_vat = $this->sma->formatDecimal($temp_vat, 2);
            $temp_vat = 0;

            // echo $temp_total_before_discount.'<br>';
            // echo $temp_service_charge.'<br>';
            // echo $shipping.'<br>';
            // echo $temp_vat.'<br>';
            // exit;

            $grand_total = $total + $temp_service_charge + $temp_vat + $shipping;
            $grand_total = $this->api_helper->round_up_second_decimal($grand_total);
            $grand_total = $this->sma->formatDecimal($grand_total, 4);            

            $data = array(
                'is_vat' => $is_vat,
                'date' => $date,
                'customer_id' => $customer_id,
                'customer' => $customer,
                'biller_id' => $biller_id,
                'biller' => $biller,
                'warehouse_id' => $warehouse_id,
                'note' => $note,
                'staff_note' => $staff_note,
                'total' => $total,
                'product_discount' => $product_discount,
                'order_discount_id' => $this->input->post('order_discount'),
                'order_discount' => $order_discount,
                'total_discount' => $total_discount,
                'product_tax' => $product_tax,
                'order_tax_id' => $temp_order_tax_id,
                'order_tax' => $order_tax,
                'total_tax' => $total_tax,
                'order_tax' => $temp_vat,
                'total_tax' => $temp_vat,                
                'shipping' => $this->sma->formatDecimal($shipping),
                'grand_total' => $grand_total,
                'total_items' => $total_items,
                'payment_status' => $payment_status,
                'payment_term' => $payment_term,
                'due_date' => $due_date,
                'updated_by' => $this->session->userdata('user_id'),
                'updated_at' => date('Y-m-d H:i:s'),
                'service_charge' => $temp_service_charge,
                'sample' => $this->input->post('sample'),
            );



            if ($this->Settings->indian_gst) {
                $data['cgst'] = $total_cgst;
                $data['sgst'] = $total_sgst;
                $data['igst'] = $total_igst;
            }

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }

            
        }

        if ($this->form_validation->run() == true && $this->sales_model->updateSale($id, $data, $products)) {
            $cashier = $this->input->post('cashier');
            $config_data = array(
                'table_name' => 'sma_sales',
                'id_name' => 'id',
                'field_add_ons_name' => 'add_ons',
                'selected_id' => $id,
                'add_ons_title' => 'cashier',
                'add_ons_value' => $cashier,
            );
            $this->site->api_update_add_ons_field($config_data);

            $config_data = array(
                'table_name' => 'sma_sales',
                'id_name' => 'id',
                'field_add_ons_name' => 'add_ons',
                'selected_id' => $id,
                'add_ons_title' => 'kh_currency_rate',
                'add_ons_value' => $kh_currency_rate,
            );
            $this->site->api_update_add_ons_field($config_data);
            $config_data = array(
                'table_name' => 'sma_sales',
                'id_name' => 'id',
                'field_add_ons_name' => 'add_ons',
                'selected_id' => $id,
                'add_ons_title' => 'delivery_date',
                'add_ons_value' => $delivery_date,
            );
            $this->site->api_update_add_ons_field($config_data);
            /*
            $config_data = array(
                'table_name' => 'sma_sales',
                'id_name' => 'id',
                'field_add_ons_name' => 'add_ons',
                'selected_id' => $id,
                'add_ons_title' => 'company_branch',
                'add_ons_value' => $this->input->post('company_branch_id'),
            );
            $this->site->api_update_add_ons_field($config_data);
            $config_data = array(
                'table_name' => 'sma_sales',
                'id_name' => 'id',
                'field_add_ons_name' => 'add_ons',
                'selected_id' => $id,
                'add_ons_title' => 'po_number',
                'add_ons_value' => $this->input->post('po_number'),
            );
            $this->site->api_update_add_ons_field($config_data);
            */

            foreach ($_POST as $name => $value) {
                if (is_int(strpos($name, "add_ons_"))) {
                    $temp_name = str_replace('add_ons_', '', $name);
                    $config_data = array(
                        'table_name' => 'sma_sales',
                        'id_name' => 'id',
                        'field_add_ons_name' => 'add_ons',
                        'selected_id' => $id,
                        'add_ons_title' => $temp_name,
                        'add_ons_value' => $value,
                    );
                    $this->site->api_update_add_ons_field($config_data);
                }
            }
            

            if (is_int(strpos($inv->reference_no, "SL"))) {
                $temp2 = substr($inv->date, -2);
                $date = $this->sma->fld(trim($this->input->post('date').':'.$temp2));
                $temp22['date'] = $date;

                $temp3 = explode(' ', $inv->date);
                $temp4 = explode(' ', $temp22['date']);

                if ($temp3[0] != $temp4[0]) {
                    $this->change_sl_sale_date($id);
                }
            }

            if ($this->input->post('consignment_id') != '') {
                $temp = $this->site->api_select_some_fields_with_where(
                    "*
                    ",
                    "sma_sale_items",
                    "sale_id = ".$id,
                    "arr"
                );
                for ($i=0;$i<count($temp);$i++) {
                    $temp_2 = $this->site->api_select_some_fields_with_where(
                        "*
                        ",
                        "sma_consignment_sale_items",
                        "sale_id = ".$id." and product_id = ".$temp[$i]['product_id'],
                        "arr"
                    );
                    if (count($temp_2) > 0) {
                        $temp_3 = array(
                            'quantity' => $temp[$i]['quantity']
                        );
                        $this->db->update('sma_consignment_sale_items', $temp_3,"id = ".$temp_2[0]['id']);
                    }
                }                
            }

            $this->session->set_userdata('remove_slls', 1);
            $this->session->set_flashdata('message', lang("sale_updated"));
            if ($this->input->post('sample') == 0) {
                // admin_redirect($inv->pos ? 'pos/sales' : 'sales');
                admin_redirect($inv->pos ? 'sales' : 'sales');
            } else {
                admin_redirect("sales?mode=sample");
            }
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['inv'] = $this->sales_model->getInvoiceByID($id);
            $temp_field = $this->site->api_get_condition_add_ons_field("add_ons", "sma_add_ons", "table_name = 'sma_sales'");
            $temp = $this->site->api_select_some_fields_with_where(
                "id
                ".$temp_field,
                "sma_sales",
                "id = ".$id,
                "arr"
            );
            foreach ($temp[0] as $key => $value) {
                $this->data['inv']->{$key} = $value;
            }
            if ($this->data['inv']->company_branch > 0) {
                $temp2 = $this->site->api_select_some_fields_with_where(
                    "id,
                    getTranslate(translate,'en','".f_separate."','".v_separate."') as title,
                    getTranslate(add_ons,'branch_number','".f_separate."','".v_separate."') as branch_number
                    ",
                    "sma_company_branch",
                    "id = ".$this->data['inv']->company_branch." order by title asc",
                    "arr"
                );
                $this->data['inv']->temp_company_branch_id = $temp2[0]['id'];
                $this->data['inv']->temp_company_branch_title = $temp2[0]['title'];
                $this->data['inv']->temp_company_branch_branch_number = $temp2[0]['branch_number'];
            }
            

            if ($this->Settings->disable_editing) {
                if ($this->data['inv']->date <= date('Y-m-d', strtotime('-'.$this->Settings->disable_editing.' days'))) {
                    $this->session->set_flashdata('error', sprintf(lang("sale_x_edited_older_than_x_days"), $this->Settings->disable_editing));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            }
            $inv_items = $this->sales_model->getAllInvoiceItems($id);
            // krsort($inv_items);
            $c = rand(100000, 9999999);
            $pr = array();

            if ($inv_items) {
                foreach ($inv_items as $item) {
                    //if ($item->quantity > 0) {
                    
                    $row = $this->site->getProductByID($item->product_id);
                    if (!$row) {
                        $row = json_decode('{}');
                        $row->tax_method = 0;
                        $row->quantity = 0;
                    } else {
                        unset($row->cost, $row->details, $row->product_details, $row->image, $row->barcode_symbology, $row->cf1, $row->cf2, $row->cf3, $row->cf4, $row->cf5, $row->cf6, $row->supplier1price, $row->supplier2price, $row->cfsupplier3price, $row->supplier4price, $row->supplier5price, $row->supplier1, $row->supplier2, $row->supplier3, $row->supplier4, $row->supplier5, $row->supplier1_part_no, $row->supplier2_part_no, $row->supplier3_part_no, $row->supplier4_part_no, $row->supplier5_part_no);
                    }
                    $pis = $this->site->getPurchasedItems($item->product_id, $item->warehouse_id, $item->option_id);
                    if ($pis) {
                        $row->quantity = 0;
                        foreach ($pis as $pi) {
                            $row->quantity += $pi->quantity_balance;
                        }
                    }
                    
                    
                    $row->id = $item->product_id;
                    $row->code = $item->product_code;
                    $row->name = $item->product_name;
                    $row->type = $item->product_type;
                    $row->base_quantity = $item->quantity;
                    $row->base_unit = !empty($row->unit) ? $row->unit : $item->product_unit_id;
                    $row->base_unit_price = !empty($row->price) ? $row->price : $item->unit_price;
                    $row->unit = $item->product_unit_id;
                    $row->qty = $item->quantity;
                    $row->quantity += $item->quantity;

                    $row->discount = $item->discount ? $item->discount : '0';

                    if ($item->quantity > 0) {
                        $row->price = $this->sma->formatDecimal($item->net_unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity));
                    }
                    if ($item->quantity > 0) {
                        $row->unit_price = $row->tax_method ? $item->unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity) + $this->sma->formatDecimal($item->item_tax / $item->quantity) : $item->unit_price + ($item->item_discount / $item->quantity);
                    }
                    $row->real_unit_price = $item->real_unit_price;
                    $row->tax_rate = $item->tax_rate_id;
                    $row->serial = $item->serial_no;
                    $row->option = $item->option_id;
                    $options = $this->sales_model->getProductOptions($row->id, $item->warehouse_id);

                    if ($options) {
                        $option_quantity = 0;
                        foreach ($options as $option) {
                            $pis = $this->site->getPurchasedItems($row->id, $item->warehouse_id, $item->option_id);
                            if ($pis) {
                                foreach ($pis as $pi) {
                                    $option_quantity += $pi->quantity_balance;
                                }
                            }
                            $option_quantity += $item->quantity;
                            if ($option->quantity > $option_quantity) {
                                $option->quantity = $option_quantity;
                            }
                        }
                    }

                    $combo_items = false;
                    if ($row->type == 'combo') {
                        $combo_items = $this->sales_model->getProductComboItems($row->id, $item->warehouse_id);
                        $te = $combo_items;
                        foreach ($combo_items as $combo_item) {
                            $combo_item->quantity = $combo_item->qty * $item->quantity;
                        }
                    }
                    $units = !empty($row->base_unit) ? $this->site->getUnitsByBUID($row->base_unit) : null;
                    $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                    $ri = $this->Settings->item_addition ? $row->id : $c;
    
                    $item_net_price = $temp['price'];                    
                    
                    
                    $row->temp_original_price = $this->sma->formatDecimal($item->net_unit_price);
                    $row->temp_unit_price = $this->sma->formatDecimal($item->unit_price);
                    $row->temp_discount = $this->sma->formatDecimal($item->discount);
                    $row->unit_price = $this->sma->formatDecimal($item->unit_price);
                    $row->real_unit_price = $row->temp_original_price;
                    $row->temp_discount_rate = '';
                    $config_data = array(
                        'table_name' => 'sma_sale_items',
                        'select_table' => 'sma_sale_items',
                        'translate' => '',
                        'description' => '',
                        'select_condition' => "id = ".$item->id,
                    );
                    $temp = $this->api_helper->api_select_data_v2($config_data);                        
                    if ($temp[0]['discount_rate'] > 0) {
                        $row->temp_discount_rate = $this->sma->formatMoney($item->discount).' ('.number_format($temp[0]['discount_rate'],2).'%)';
                        $row->temp_discount_rate_value = number_format($temp[0]['discount_rate'],2);
                    }     
                                        
                        
                    $pr[$ri] = array('id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")",
                         'row' => $row, 'combo_items' => $combo_items, 'tax_rate' => $tax_rate, 'units' => $units, 'options' => $options);
                    $c++;
                    //}
                }
            }
            $this->data['inv_items'] = json_encode($pr);
            $this->data['id'] = $id;
            //$this->data['currencies'] = $this->site->getAllCurrencies();
            $this->data['billers'] = ($this->Owner || $this->Admin || !$this->session->userdata('biller_id')) ? $this->site->getAllCompanies('biller') : null;
            $this->data['units'] = $this->site->getAllBaseUnits();
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
            $this->data['delivery_person'] = $this->sales_model->get_delivery_person();

            $this->load->admin_model('companies_model');
            $this->data['sales_person'] = $this->companies_model->getAllSalePerson();

            $temp = $this->site->api_select_some_fields_with_where(
                "*
                ",
                "sma_consignment_sale_items",
                "sale_id = ".$id." limit 1",
                "arr"
            );
            if (count($temp) > 0) {                
                $this->data['consignment_id'] = $temp[0]['consignment_id'];
                // $this->session->set_flashdata('error', lang('Cannot_edit_any_sale_with_a_sale_consignment.'));
                // admin_redirect($_SERVER["HTTP_REFERER"]);
            }

            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('edit_sale')));
            $meta = array('page_title' => lang('edit_sale'), 'bc' => $bc);
            $this->page_construct('sales/edit', $meta, $this->data);
        }
    }

    /* ------------------------------- */

    public function return_sale($id = null)
    {
        $this->sma->checkPermissions('return_sales');

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $sale = $this->sales_model->getInvoiceByID($id);
        if ($sale->return_id) {
            $this->session->set_flashdata('error', lang("sale_already_returned"));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('return_surcharge', lang("return_surcharge"), 'required');

        if ($this->form_validation->run() == true) {
            $reference = $this->input->post('reference_no') ? $this->input->post('reference_no') : $this->site->getReference('re');
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }

            $return_surcharge = $this->input->post('return_surcharge') ? $this->input->post('return_surcharge') : 0;
            $note = $this->sma->clear_tags($this->input->post('note'));
            $customer_details = $this->site->getCompanyByID($sale->customer_id);
            $biller_details = $this->site->getCompanyByID($sale->biller_id);

            $total = 0;
            $product_tax = 0;
            $product_discount = 0;
            $gst_data = [];
            $total_cgst = $total_sgst = $total_igst = 0;
            $i = isset($_POST['product_code']) ? sizeof($_POST['product_code']) : 0;
            for ($r = 0; $r < $i; $r++) {
                $item_id = $_POST['product_id'][$r];
                $item_type = $_POST['product_type'][$r];
                $item_code = $_POST['product_code'][$r];
                $item_name = $_POST['product_name'][$r];
                $sale_item_id = $_POST['sale_item_id'][$r];
                $item_option = isset($_POST['product_option'][$r]) && $_POST['product_option'][$r] != 'false' && $_POST['product_option'][$r] != 'null' ? $_POST['product_option'][$r] : null;
                $real_unit_price = $this->sma->formatDecimal($_POST['real_unit_price'][$r]);
                $unit_price = $this->sma->formatDecimal($_POST['unit_price'][$r]);
                $item_unit_quantity = (0-$_POST['quantity'][$r]);
                $item_serial = isset($_POST['serial'][$r]) ? $_POST['serial'][$r] : '';
                $item_tax_rate = isset($_POST['product_tax'][$r]) ? $_POST['product_tax'][$r] : null;
                $item_discount = isset($_POST['product_discount'][$r]) ? $_POST['product_discount'][$r] : null;
                $item_unit = $_POST['product_unit'][$r];
                $item_quantity = (0-$_POST['product_base_quantity'][$r]);

                if (isset($item_code) && isset($real_unit_price) && isset($unit_price) && isset($item_quantity)) {
                    $product_details = $item_type != 'manual' ? $this->sales_model->getProductByCode($item_code) : null;
                    // $unit_price = $real_unit_price;
                    $pr_discount = $this->site->calculateDiscount($item_discount, $unit_price);
                    $unit_price = $this->sma->formatDecimal(($unit_price - $pr_discount), 4);
                    $item_net_price = $unit_price;
                    $pr_item_discount = $this->sma->formatDecimal($pr_discount * $item_unit_quantity, 4);
                    $product_discount += $pr_item_discount;
                    $pr_item_tax = $item_tax = 0;
                    $tax = "";

                    if (isset($item_tax_rate) && $item_tax_rate != 0) {
                        $tax_details = $this->site->getTaxRateByID($item_tax_rate);
                        $ctax = $this->site->calculateTax($product_details, $tax_details, $unit_price);
                        $item_tax = $ctax['amount'];
                        $tax = $ctax['tax'];
                        if (!$product_details || (!empty($product_details) && $product_details->tax_method != 1)) {
                            $item_net_price = $unit_price - $item_tax;
                        }
                        $pr_item_tax = $this->sma->formatDecimal(($item_tax * $item_unit_quantity), 4);
                        if ($this->Settings->indian_gst && $gst_data = $this->gst->calculteIndianGST($pr_item_tax, ($biller_details->state == $customer_details->state), $tax_details)) {
                            $total_cgst += $gst_data['cgst'];
                            $total_sgst += $gst_data['sgst'];
                            $total_igst += $gst_data['igst'];
                        }
                    }

                    $product_tax += $pr_item_tax;
                    $subtotal = $this->sma->formatDecimal((($item_net_price * $item_unit_quantity) + $pr_item_tax), 4);
                    $unit = $item_unit ? $this->site->getUnitByID($item_unit) : false;

                    $product = array(
                        'product_id' => $item_id,
                        'product_code' => $item_code,
                        'product_name' => $item_name,
                        'product_type' => $item_type,
                        'option_id' => $item_option,
                        'net_unit_price' => $item_net_price,
                        'unit_price' => $this->sma->formatDecimal($item_net_price + $item_tax),
                        'quantity' => $item_quantity,
                        'product_unit_id' => $item_unit,
                        'product_unit_code' => $unit ? $unit->code : null,
                        'unit_quantity' => $item_unit_quantity,
                        'warehouse_id' => $sale->warehouse_id,
                        'item_tax' => $pr_item_tax,
                        'tax_rate_id' => $item_tax_rate,
                        'tax' => $tax,
                        'discount' => $item_discount,
                        'item_discount' => $pr_item_discount,
                        'subtotal' => $this->sma->formatDecimal($subtotal),
                        'serial_no' => $item_serial,
                        'real_unit_price' => $real_unit_price,
                        'sale_item_id' => $sale_item_id,
                    );

                    $si_return[] = array(
                        'id' => $sale_item_id,
                        'sale_id' => $id,
                        'product_id' => $item_id,
                        'option_id' => $item_option,
                        'quantity' => (0-$item_quantity),
                        'warehouse_id' => $sale->warehouse_id,
                        );

                    $products[] = ($product + $gst_data);
                    $total += $this->sma->formatDecimal(($item_net_price * $item_unit_quantity), 4);
                }
            }
            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("order_items"), 'required');
            } else {
                krsort($products);
            }

            $order_discount = $this->site->calculateDiscount($this->input->post('discount') ? $this->input->post('order_discount') : null, ($total + $product_tax));
            $total_discount = $this->sma->formatDecimal(($order_discount + $product_discount), 4);
            $order_tax = $this->site->calculateOrderTax($this->input->post('order_tax'), ($total + $product_tax - $order_discount));
            $total_tax = $this->sma->formatDecimal($product_tax + $order_tax, 4);
            $grand_total = $this->sma->formatDecimal(($total + $total_tax + $this->sma->formatDecimal($return_surcharge) - $order_discount), 4);
            $data = array('date' => $date,
                'sale_id' => $id,
                'reference_no' => $sale->reference_no,
                'customer_id' => $sale->customer_id,
                'customer' => $sale->customer,
                'biller_id' => $sale->biller_id,
                'biller' => $sale->biller,
                'warehouse_id' => $sale->warehouse_id,
                'note' => $note,
                'total' => $total,
                'product_discount' => $product_discount,
                'order_discount_id' => $this->input->post('discount') ? $this->input->post('order_discount') : null,
                'order_discount' => $order_discount,
                'total_discount' => $total_discount,
                'product_tax' => $product_tax,
                'order_tax_id' => $this->input->post('order_tax'),
                'order_tax' => $order_tax,
                'total_tax' => $total_tax,
                'surcharge' => $this->sma->formatDecimal($return_surcharge),
                'grand_total' => $grand_total,
                'created_by' => $this->session->userdata('user_id'),
                'return_sale_ref' => $reference,
                'sale_status' => 'returned',
                'pos' => $sale->pos,
                'payment_status' => $sale->payment_status == 'paid' ? 'due' : 'pending',
            );
            if ($this->Settings->indian_gst) {
                $data['cgst'] = $total_cgst;
                $data['sgst'] = $total_sgst;
                $data['igst'] = $total_igst;
            }

            if ($this->input->post('amount-paid') && $this->input->post('amount-paid') > 0) {
                $pay_ref = $this->input->post('payment_reference_no') ? $this->input->post('payment_reference_no') : $this->site->getReference('pay');
                $payment = array(
                    'date' => $date,
                    'reference_no' => $pay_ref,
                    'amount' => (0-$this->input->post('amount-paid')),
                    'paid_by' => $this->input->post('paid_by'),
                    'cheque_no' => $this->input->post('cheque_no'),
                    'cc_no' => $this->input->post('pcc_no'),
                    'cc_holder' => $this->input->post('pcc_holder'),
                    'cc_month' => $this->input->post('pcc_month'),
                    'cc_year' => $this->input->post('pcc_year'),
                    'cc_type' => $this->input->post('pcc_type'),
                    'created_by' => $this->session->userdata('user_id'),
                    'type' => 'returned',
                );
                $data['payment_status'] = $grand_total == $this->input->post('amount-paid') ? 'paid' : 'partial';
            } else {
                $payment = array();
            }

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }

            
        }

        if ($this->form_validation->run() == true && $this->sales_model->addSale($data, $products, $payment, $si_return)) {
            $this->session->set_flashdata('message', lang("return_sale_added"));
            admin_redirect($sale->pos ? "pos/sales" : "sales");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['inv'] = $sale;
            if ($this->data['inv']->sale_status != 'completed') {
                $this->session->set_flashdata('error', lang("sale_status_x_competed"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
            if ($this->Settings->disable_editing) {
                if ($this->data['inv']->date <= date('Y-m-d', strtotime('-'.$this->Settings->disable_editing.' days'))) {
                    $this->session->set_flashdata('error', sprintf(lang("sale_x_edited_older_than_x_days"), $this->Settings->disable_editing));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            }
            $inv_items = $this->sales_model->getAllInvoiceItems($id);
            // krsort($inv_items);
            $c = rand(100000, 9999999);
            foreach ($inv_items as $item) {
                $row = $this->site->getProductByID($item->product_id);
                if (!$row) {
                    $row = json_decode('{}');
                    $row->tax_method = 0;
                    $row->quantity = 0;
                } else {
                    unset($row->cost, $row->details, $row->product_details, $row->image, $row->barcode_symbology, $row->cf1, $row->cf2, $row->cf3, $row->cf4, $row->cf5, $row->cf6, $row->supplier1price, $row->supplier2price, $row->cfsupplier3price, $row->supplier4price, $row->supplier5price, $row->supplier1, $row->supplier2, $row->supplier3, $row->supplier4, $row->supplier5, $row->supplier1_part_no, $row->supplier2_part_no, $row->supplier3_part_no, $row->supplier4_part_no, $row->supplier5_part_no);
                }
                $pis = $this->site->getPurchasedItems($item->product_id, $item->warehouse_id, $item->option_id);
                if ($pis) {
                    $row->quantity = 0;
                    foreach ($pis as $pi) {
                        $row->quantity += $pi->quantity_balance;
                    }
                }
                $row->id = $item->product_id;
                $row->sale_item_id = $item->id;
                $row->code = $item->product_code;
                $row->name = $item->product_name;
                $row->type = $item->product_type;
                $row->base_quantity = $item->quantity;
                $row->base_unit = $row->unit ? $row->unit : $item->product_unit_id;
                $row->base_unit_price = $row->price ? $row->price : $item->unit_price;
                $row->unit = $item->product_unit_id;
                $row->qty = $item->quantity;
                $row->oqty = $item->quantity;
                $row->discount = $item->discount ? $item->discount : '0';
                $row->price = $this->sma->formatDecimal($item->net_unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity));
                $row->unit_price = $row->tax_method ? $item->unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity) + $this->sma->formatDecimal($item->item_tax / $item->quantity) : $item->unit_price + ($item->item_discount / $item->quantity);
                $row->real_unit_price = $item->real_unit_price;
                $row->tax_rate = $item->tax_rate_id;
                $row->serial = $item->serial_no;
                $row->option = $item->option_id;
                $options = $this->sales_model->getProductOptions($row->id, $item->warehouse_id, true);
                $units = $this->site->getUnitsByBUID($row->base_unit);
                $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                $ri = $this->Settings->item_addition ? $row->id : $c;

                $pr[$ri] = array('id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'row' => $row, 'units' => $units, 'tax_rate' => $tax_rate, 'options' => $options);
                $c++;
            }
            $this->data['inv_items'] = json_encode($pr);
            $this->data['id'] = $id;
            $this->data['payment_ref'] = '';
            $this->data['reference'] = ''; // $this->site->getReference('re');
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('return_sale')));
            $meta = array('page_title' => lang('return_sale'), 'bc' => $bc);
            $this->page_construct('sales/return_sale', $meta, $this->data);
        }
    }

    /* ------------------------------- */

    public function delete($id = null)
    {
        $this->sma->checkPermissions(null, true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $temp = $this->sales_model->api_delete($id);
        if ($temp['error'] != '') {
            $this->session->set_flashdata('error', $temp['error']);
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $this->session->set_flashdata('message', lang('sale_deleted'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function delete_return($id = null)
    {
        $this->sma->checkPermissions(null, true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->sales_model->deleteReturn($id)) {
            if ($this->input->is_ajax_request()) {
                $this->sma->send_json(array('error' => 0, 'msg' => lang("return_sale_deleted")));
            }
            $this->session->set_flashdata('message', lang('return_sale_deleted'));
            admin_redirect('welcome');
        }
    }

    public function sale_actions()
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('api_action', lang("api_action"), 'required');

        if ($this->form_validation->run() == true) {
            if (!empty($_POST['check_value'])) {
                $temp_val = explode('-', $_POST['check_value']);

                if ($this->input->post('api_action') == 'delete') {
                    $this->sma->checkPermissions('delete');
                    $b = 0;
                    foreach ($temp_val as $id) {
                        if ($id != '') {
                            $temp = $this->sales_model->api_delete($id);
                            if ($temp['error'] == '') {
                                $b++;
                            } else {
                                $temp_error = $temp['error'];
                            }
                        }
                    }
                    if ($b > 0) {
                        $this->session->set_flashdata('message', $b.' '.lang('sale(s)_deleted'));
                    } else {
                        $this->session->set_flashdata('error', $temp_error);
                    }
                    redirect($_SERVER["HTTP_REFERER"]);
                } elseif ($this->input->post('api_action') == 'combine') {
                    $html = $this->combine_pdf($temp_val);
                } elseif ($this->input->post('api_action') == 'print_combine_pdf') {
                    $j = 0;
                    foreach ($temp_val as $id) {
                        if ($id != '') {
                            $sale = $this->sales_model->getInvoiceByID($id);
                            if ($sale->reference_no == '') {
                                $j = 1;
                            }
                        }
                    }
                    if ($j == 1) {
                        $this->session->set_flashdata('error', lang("action_print_error"));
                        admin_redirect("sales");
                    } else {
                        foreach ($temp_val as $id) {
                            if ($id != '') {
                                $sale = $this->sales_model->getInvoiceByID($id);
                                if ($sale->reference_no != '') {
                                    if ($sale->sale_status == 'print needed') {
                                        $update_array = array(
                                            'sale_status' => 'preparing',
                                        );
                                        $this->site->api_update_table('sma_sales', $update_array, "id = ".$id);
                                        //$this->api_send_mail_customer_order($id);
                                    }
                                }
                            }
                        }
                        $html = $this->combine_pdf($temp_val);
                    }
                } elseif ($this->input->post('api_action') == 'set_delivering') {
                    $temp = '?sales=';
                    foreach ($temp_val as $id) {
                        if ($id != '') {
                            $temp .= $id.'-';
                        }
                    }
                    admin_redirect("sales/set_delivering_bulk".$temp);
                } elseif ($this->input->post('api_action') == 'generate_reference_no') {
                    $temp2 = 0;
                    sort($_POST['val']);
                    $temp_array = array();
                    foreach ($temp_val as $id) {
                        if ($id != '') {
                            $sale = $this->sales_model->getInvoiceByID($id);
                            if ($sale->reference_no == '') {
                                $customers = $this->site->getCompanyByID($sale->customer_id);
                                $config_data = array(
                                    'type' => 'sale',
                                    'date' => $sale->date,
                                    'customer_id' => $sale->customer_id,
                                    'update' => 1,
                                );
                                $temp = $this->site->api_calculate_reference_no($config_data);
                                $reference_no = $temp['reference_no'];
                                
                                $update_array = array(
                                    'reference_no' => $reference_no,
                                    'sale_status' => 'preparing',
                                );
                                $this->site->api_update_table('sma_sales', $update_array, "id = ".$id);

                                $config_data4 = array(
                                    'date' => $sale->date
                                );
                                //$this->site->api_calculate_sl_reference_no_by_date($config_data4);

                                $temp_riel = $this->site->api_select_some_fields_with_where(
                                    "rate
                                    ",
                                    "sma_currencies",
                                    "code = 'KHR' and YEAR(date) = YEAR('".$sale->date."') and MONTH(date) = MONTH('".$sale->date."') and DAY(date) = DAY('".$sale->date."') order by date desc limit 1",
                                    "arr"
                                );
                                if ($temp_riel[0]['rate'] > 0) {
                                    $config_data_2 = array(
                                        'table_name' => 'sma_sales',
                                        'id_name' => 'id',
                                        'field_add_ons_name' => 'add_ons',
                                        'selected_id' => $id,
                                        'add_ons_title' => 'kh_currency_rate',
                                        'add_ons_value' => $temp_riel[0]['rate'],
                                    );
                                    $this->site->api_update_add_ons_field($config_data_2);
                                }

                                array_push($temp_array, $id);
                                $temp2++;
                            }
                        }
                    }
                    $this->session->set_flashdata('message', $temp2.' '.lang("record_is_generated"));
                    redirect($_SERVER["HTTP_REFERER"]);
                } elseif ($this->input->post('api_action') == 'export_excel') {
                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('sales'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference_no'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('biller'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('customer'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('grand_total'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('paid'));
                    $this->excel->getActiveSheet()->SetCellValue('G1', lang('payment_status'));

                    $row = 2;
                    foreach ($temp_val as $id) {
                        if ($id != '') {
                            $sale = $this->sales_model->getInvoiceByID($id);
                            $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($sale->date));
                            $this->excel->getActiveSheet()->SetCellValue('B' . $row, $sale->reference_no);
                            $this->excel->getActiveSheet()->SetCellValue('C' . $row, $sale->biller);
                            $this->excel->getActiveSheet()->SetCellValue('D' . $row, $sale->customer);
                            $this->excel->getActiveSheet()->SetCellValue('E' . $row, $sale->grand_total);
                            $this->excel->getActiveSheet()->SetCellValue('F' . $row, lang($sale->paid));
                            $this->excel->getActiveSheet()->SetCellValue('G' . $row, lang($sale->payment_status));
                            $row++;
                        }
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'sales_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                } elseif ($this->input->post('api_action') == 'change_payment_status_to_due') {
                    $temp_count = 0;
                    $temp_count_payment_category = 0;
                    foreach ($temp_val as $id) {
                        if ($id != '') {
                            $temp_3 = $this->site->api_select_some_fields_with_where(
                                "
                                *
                                ",
                                "sma_sales",
                                "id = ".$id,
                                "arr"
                            );
                            if (count($temp_3) > 0) {
                                $temp_2 = $this->site->api_select_some_fields_with_where(
                                    "
                                    *
                                    ",
                                    "sma_companies",
                                    "id = ".$temp_3[0]['customer_id'],
                                    "arr"
                                );
                                if ($temp_2[0]['payment_category'] == 'cash on delivery') {
                                    $config_data = array(
                                        'id' => $id,
                                        'payment_status' => 'due',
                                    );
                                    $temp = $this->sales_model->api_update_completed_payment_status($config_data);
                                    if ($temp['updated'] == 1) {
                                        $temp_count++;
                                    }
                                } else {
                                    $temp_count_payment_category++;
                                }
                            }
                        }
                    }

                    if ($temp_count > 0) {
                        $this->session->set_flashdata('message', $temp_count.' '.lang('sale(s)_have_successfully_changed_payment_status'));
                    } else {
                        if ($temp_count_payment_category > 0) {
                            $this->session->set_flashdata('error', $temp_count.' '.lang("sale_is_effected."));
                        } else {
                            $this->session->set_flashdata('error', $temp_count.' '.lang("sale_is_effected._Only_sales_with_staus_delivering_or_completed_are_effected"));
                        }
                    }

                    redirect($_SERVER["HTTP_REFERER"]);
                } elseif ($this->input->post('api_action') == 'change_payment_status_to_partial') {
                    $b = 0;
                    $b2 = 0;
                    foreach ($temp_val as $id) {
                        if ($id != '') {
                            $config_data = array(
                                'id' => $id,
                                'payment_status' => 'partial',
                            );
                            $temp = $this->sales_model->api_update_completed_payment_status($config_data);
                            if ($temp['updated'] == 1) {
                                $b = 1;
                            } else {
                                $b2 = 1;
                            }
                        }
                    }
                    if ($b == 0) {
                        $this->session->set_flashdata('error', lang("Only_sales_with_status_delivering_or_completed_are_effected"));
                    }
                    if ($b == 1 && $b2 == 0) {
                        $this->session->set_flashdata('message', lang('successfully_change_payment_status'));
                    }
                    if ($b == 1 && $b2 == 1) {
                        $this->session->set_flashdata('message', lang('Only_sales_with_status_delivering_or_completed_are_effected'));
                    }
                    redirect($_SERVER["HTTP_REFERER"]);
                } elseif ($this->input->post('api_action') == 'change_payment_status_to_ar') {
                    $b = 0;
                    $b2 = 0;
                    foreach ($temp_val as $id) {
                        if ($id != '') {
                            $config_data = array(
                                'id' => $id,
                                'payment_status' => 'AR',
                            );
                            $temp = $this->sales_model->api_update_completed_payment_status($config_data);
                            if ($temp['updated'] == 1) {
                                $b = 1;
                            } else {
                                $b2 = 1;
                            }
                        }
                    }
                    if ($b == 0) {
                        $this->session->set_flashdata('error', lang("Only_sales_with_status_delivering_or_completed_are_effected"));
                    }
                    if ($b == 1 && $b2 == 0) {
                        $this->session->set_flashdata('message', lang('successfully_change_payment_status'));
                    }
                    if ($b == 1 && $b2 == 1) {
                        $this->session->set_flashdata('message', lang('Only_sales_with_status_delivering_or_completed_are_effected'));
                    }
                    redirect($_SERVER["HTTP_REFERER"]);
                } elseif ($this->input->post('api_action') == 'change_payment_status_to_paid') {
                    $b = 0;
                    $b2 = 0;
                    foreach ($temp_val as $id) {
                        if ($id != '') {
                            $config_data = array(
                                'id' => $id,
                                'payment_status' => 'paid',
                            );                            
                            $temp = $this->sales_model->api_update_completed_payment_status($config_data);
                            if ($temp['updated'] == 1) {
                                $b = 1;
                            } else {
                                $b2 = 1;
                            }

                            $config_data = array(
                                'table_name' => 'sma_payments',
                                'select_table' => 'sma_payments',
                                'translate' => '',
                                'select_condition' => "sale_id = ".$id,
                            );
                            $temp_3 = $this->site->api_select_data_v2($config_data);
                            if (count($temp_3) <= 0) {
                                $config_data_3 = array(
                                    'type' => 'payment',
                                    'date' => date('Y-m-d H:i:s'),
                                    'update' => 1,
                                );
                                $temp_4 = $this->site->api_calculate_reference_no($config_data_3);                    
            
                                $temp_5 = array(
                                    'date' => date('Y-m-d H:i:s'),
                                    'sale_id' => $id,
                                    'reference_no' => $temp_4['reference_no'],
                                    'amount' => $temp['grand_total'],
                                    'paid_by' => 'cash',
                                    'type' => 'received',
                                    'created_by' => $this->session->userdata('user_id'),
                                );
                                $this->db->insert('sma_payments', $temp_5);                  
                            }
            
                        }
                    }
                    if ($b == 0) {
                        $this->session->set_flashdata('error', lang("Only_sales_with_status_delivering_or_completed_are_effected"));
                    }
                    if ($b == 1 && $b2 == 0) {
                        $this->session->set_flashdata('message', lang('successfully_change_payment_status'));
                    }
                    if ($b == 1 && $b2 == 1) {
                        $this->session->set_flashdata('message', lang('Only_sales_with_status_delivering_or_completed_are_effected'));
                    }
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_sale_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    /* ------------------------------- */

    public function deliveries()
    {
        $this->sma->checkPermissions();

        $data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('deliveries')));
        $meta = array('page_title' => lang('deliveries'), 'bc' => $bc);
        $this->page_construct('sales/deliveries', $meta, $this->data);
    }

    public function getDeliveries()
    {
        $this->sma->checkPermissions('deliveries');

        $detail_link = anchor('admin/sales/view_delivery/$1', '<i class="fa fa-file-text-o"></i> ' . lang('delivery_details'), 'data-toggle="modal" data-target="#myModal"');
        $email_link = anchor('admin/sales/email_delivery/$1', '<i class="fa fa-envelope"></i> ' . lang('email_delivery'), 'data-toggle="modal" data-target="#myModal"');
        $edit_link = anchor('admin/sales/edit_delivery/$1', '<i class="fa fa-edit"></i> ' . lang('edit_delivery'), 'data-toggle="modal" data-target="#myModal"');
        $pdf_link = anchor('admin/sales/pdf_delivery/$1', '<i class="fa fa-file-pdf-o"></i> ' . lang('download_pdf'));
        $delete_link = "<a href='#' class='po' title='<b>" . lang("delete_delivery") . "</b>' data-content=\"<p>"
        . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('sales/delete_delivery/$1') . "'>"
        . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i> "
        . lang('delete_delivery') . "</a>";
        $action = '<div class="text-center"><div class="btn-group text-left">'
        . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
        . lang('actions') . ' <span class="caret"></span></button>
    <ul class="dropdown-menu pull-right" role="menu">
        <li>' . $detail_link . '</li>
        <li>' . $edit_link . '</li>
        <li>' . $pdf_link . '</li>
        <li>' . $delete_link . '</li>
    </ul>
</div></div>';

        $this->load->library('datatables');
        //GROUP_CONCAT(CONCAT('Name: ', sale_items.product_name, ' Qty: ', sale_items.quantity ) SEPARATOR '<br>')
        $this->datatables
            ->select("deliveries.id as id, date, do_reference_no, sale_reference_no, customer, address, status, attachment")
            ->from('deliveries')
            ->join('sale_items', 'sale_items.sale_id=deliveries.sale_id', 'left')
            ->group_by('deliveries.id');
        $this->datatables->add_column("Actions", $action, "id");

        echo $this->datatables->generate();
    }

    public function pdf_delivery($id = null, $view = null, $save_bufffer = null)
    {
        $this->sma->checkPermissions();

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $deli = $this->sales_model->getDeliveryByID($id);

        $this->data['delivery'] = $deli;
        $sale = $this->sales_model->getInvoiceByID($deli->sale_id);
        $this->data['biller'] = $this->site->getCompanyByID($sale->biller_id);
        $this->data['rows'] = $this->sales_model->getAllInvoiceItemsWithDetails($deli->sale_id);
        $this->data['user'] = $this->site->getUser($deli->created_by);

        $name = lang("delivery") . "_" . str_replace('/', '_', $deli->do_reference_no) . ".pdf";
        $html = $this->load->view($this->theme . 'sales/pdf_delivery', $this->data, true);
        if (! $this->Settings->barcode_img) {
            $html = preg_replace("'\<\?xml(.*)\?\>'", '', $html);
        }
        if ($view) {
            $this->load->view($this->theme . 'sales/pdf_delivery', $this->data);
        } elseif ($save_bufffer) {
            return $this->sma->generate_pdf($html, $name, $save_bufffer);
        } else {
            $this->sma->generate_pdf($html, $name);
        }
    }

    public function view_delivery($id = null)
    {
        $this->sma->checkPermissions('deliveries');

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $deli = $this->sales_model->getDeliveryByID($id);
        $sale = $this->sales_model->getInvoiceByID($deli->sale_id);
        if (!$sale) {
            $this->session->set_flashdata('error', lang('sale_not_found'));
            $this->sma->md();
        }
        $this->data['delivery'] = $deli;
        $this->data['biller'] = $this->site->getCompanyByID($sale->biller_id);
        $this->data['rows'] = $this->sales_model->getAllInvoiceItemsWithDetails($deli->sale_id);
        $this->data['user'] = $this->site->getUser($deli->created_by);
        $this->data['page_title'] = lang("delivery_order");

        $this->load->view($this->theme . 'sales/view_delivery', $this->data);
    }

    public function add_delivery($id = null)
    {
        $this->sma->checkPermissions();

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $sale = $this->sales_model->getInvoiceByID($id);
        if ($sale->sale_status != 'completed') {
            $this->session->set_flashdata('error', lang('status_is_x_completed'));
            $this->sma->md();
        }

        if ($delivery = $this->sales_model->getDeliveryBySaleID($id)) {
            $this->edit_delivery($delivery->id);
        } else {
            $this->form_validation->set_rules('sale_reference_no', lang("sale_reference_no"), 'required');
            $this->form_validation->set_rules('customer', lang("customer"), 'required');
            $this->form_validation->set_rules('address', lang("address"), 'required');

            if ($this->form_validation->run() == true) {
                if ($this->Owner || $this->Admin) {
                    $date = $this->sma->fld(trim($this->input->post('date')));
                } else {
                    $date = date('Y-m-d H:i:s');
                }
                $dlDetails = array(
                    'date' => $date,
                    'sale_id' => $this->input->post('sale_id'),
                    'do_reference_no' => $this->input->post('do_reference_no') ? $this->input->post('do_reference_no') : $this->site->getReference('do'),
                    'sale_reference_no' => $this->input->post('sale_reference_no'),
                    'customer' => $this->input->post('customer'),
                    'address' => $this->input->post('address'),
                    'status' => $this->input->post('status'),
                    'delivered_by' => $this->input->post('delivered_by'),
                    'received_by' => $this->input->post('received_by'),
                    'note' => $this->sma->clear_tags($this->input->post('note')),
                    'created_by' => $this->session->userdata('user_id'),
                    );
                if ($_FILES['document']['size'] > 0) {
                    $this->load->library('upload');
                    $config['upload_path'] = $this->digital_upload_path;
                    $config['allowed_types'] = $this->digital_file_types;
                    $config['max_size'] = $this->allowed_file_size;
                    $config['overwrite'] = false;
                    $config['encrypt_name'] = true;
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('document')) {
                        $error = $this->upload->display_errors();
                        $this->session->set_flashdata('error', $error);
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                    $photo = $this->upload->file_name;
                    $dlDetails['attachment'] = $photo;
                }
            } elseif ($this->input->post('add_delivery')) {
                if ($sale->shop) {
                    $this->load->library('sms');
                    $this->sms->delivering($sale->id, $dlDetails['do_reference_no']);
                }
                $this->session->set_flashdata('error', validation_errors());
                redirect($_SERVER["HTTP_REFERER"]);
            }

            if ($this->form_validation->run() == true && $this->sales_model->addDelivery($dlDetails)) {
                $this->session->set_flashdata('message', lang("delivery_added"));
                admin_redirect("sales/deliveries");
            } else {
                $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
                $this->data['customer'] = $this->site->getCompanyByID($sale->customer_id);
                $this->data['address'] = $this->site->getAddressByID($sale->address_id);
                $this->data['inv'] = $sale;
                $this->data['do_reference_no'] = ''; //$this->site->getReference('do');
                $this->data['modal_js'] = $this->site->modal_js();

                $this->load->view($this->theme . 'sales/add_delivery', $this->data);
            }
        }
    }

    public function edit_delivery($id = null)
    {
        $this->sma->checkPermissions();

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $this->form_validation->set_rules('do_reference_no', lang("do_reference_no"), 'required');
        $this->form_validation->set_rules('sale_reference_no', lang("sale_reference_no"), 'required');
        $this->form_validation->set_rules('customer', lang("customer"), 'required');
        $this->form_validation->set_rules('address', lang("address"), 'required');

        if ($this->form_validation->run() == true) {
            $dlDetails = array(
                'sale_id' => $this->input->post('sale_id'),
                'do_reference_no' => $this->input->post('do_reference_no'),
                'sale_reference_no' => $this->input->post('sale_reference_no'),
                'customer' => $this->input->post('customer'),
                'address' => $this->input->post('address'),
                'status' => $this->input->post('status'),
                'delivered_by' => $this->input->post('delivered_by'),
                'received_by' => $this->input->post('received_by'),
                'note' => $this->sma->clear_tags($this->input->post('note')),
                'created_by' => $this->session->userdata('user_id'),
            );

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $dlDetails['attachment'] = $photo;
            }

            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
                $dlDetails['date'] = $date;
            }
        } elseif ($this->input->post('edit_delivery')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->sales_model->updateDelivery($id, $dlDetails)) {
            $this->session->set_flashdata('message', lang("delivery_updated"));
            admin_redirect("sales/deliveries");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['delivery'] = $this->sales_model->getDeliveryByID($id);
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'sales/edit_delivery', $this->data);
        }
    }

    public function delete_delivery($id = null)
    {
        $this->sma->checkPermissions(null, true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->sales_model->deleteDelivery($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("delivery_deleted")));
        }
    }

    public function delivery_actions()
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {
            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    $this->sma->checkPermissions('delete_delivery');
                    foreach ($_POST['val'] as $id) {
                        $this->sales_model->deleteDelivery($id);
                    }
                    $this->session->set_flashdata('message', lang("deliveries_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel') {
                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('deliveries'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('do_reference_no'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('sale_reference_no'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('customer'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('address'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('status'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $delivery = $this->sales_model->getDeliveryByID($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($delivery->date));
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $delivery->do_reference_no);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $delivery->sale_reference_no);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $delivery->customer);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $delivery->address);
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, lang($delivery->status));
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(35);

                    $filename = 'deliveries_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_delivery_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    /* -------------------------------------------------------------------------------- */

    public function payments($id = null)
    {
        $this->sma->checkPermissions(false, true);
        $this->data['payments'] = $this->sales_model->getInvoicePayments_v2($id);
        $this->data['inv'] = $this->sales_model->getInvoiceByID($id);
        $this->load->view($this->theme . 'sales/payments', $this->data);
    }
    // public function get_payments($id = null)
    // {
    //     $this->sma->checkPermissions(false, true);
    //     $data = $this->sales_model->getInvoicePayments_v2($id);
    //     // echo json_encode($data);
    //     echo $data[0]['paid_by'];
    //     exit();
    // }

    public function payment_note($id = null)
    {
        $this->sma->checkPermissions('payments', true);
        $payment = $this->sales_model->getPaymentByID($id);
        $inv = $this->sales_model->getInvoiceByID($payment->sale_id);
        $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
        $this->data['inv'] = $inv;
        $this->data['payment'] = $payment;
        $this->data['page_title'] = lang("payment_note");

        $this->load->view($this->theme . 'sales/payment_note', $this->data);
    }

    public function email_payment($id = null)
    {
        $this->sma->checkPermissions('payments', true);
        $payment = $this->sales_model->getPaymentByID($id);
        $inv = $this->sales_model->getInvoiceByID($payment->sale_id);
        $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        $customer = $this->site->getCompanyByID($inv->customer_id);
        if (! $customer->email) {
            $this->sma->send_json(array('msg' => lang("update_customer_email")));
        }
        $this->data['inv'] = $inv;
        $this->data['payment'] = $payment;
        $this->data['customer'] =$customer;
        $this->data['page_title'] = lang("payment_note");
        $html = $this->load->view($this->theme . 'sales/payment_note', $this->data, true);

        $html = str_replace(array('<i class="fa fa-2x">&times;</i>', 'modal-', '<p>&nbsp;</p>', '<p style="border-bottom: 1px solid #666;">&nbsp;</p>', '<p>'.lang("stamp_sign").'</p>'), '', $html);
        $html = preg_replace("/<img[^>]+\>/i", '', $html);
        // $html = '<div style="border:1px solid #DDD; padding:10px; margin:10px 0;">'.$html.'</div>';

        $this->load->library('parser');
        $parse_data = array(
            'stylesheet' => '<link href="'.$this->data['assets'].'styles/helpers/bootstrap.min.css" rel="stylesheet"/>',
            'name' => $customer->company && $customer->company != '-' ? $customer->company :  $customer->name,
            'email' => $customer->email,
            'heading' => lang('payment_note').'<hr>',
            'msg' => $html,
            'site_link' => base_url(),
            'site_name' => $this->Settings->site_name,
            'logo' => '<img src="' . base_url('assets/uploads/logos/' . $this->Settings->logo) . '" alt="' . $this->Settings->site_name . '"/>'
        );
        $msg = file_get_contents('./themes/' . $this->Settings->theme . '/admin/views/email_templates/email_con.html');
        $message = $this->parser->parse_string($msg, $parse_data);
        $subject = lang('payment_note') . ' - ' . $this->Settings->site_name;

        if ($this->sma->send_email($customer->email, $subject, $message)) {
            $this->sma->send_json(array('msg' => lang("email_sent")));
        } else {
            $this->sma->send_json(array('msg' => lang("email_failed")));
        }
    }

    public function add_payment($id = null)
    {
        $this->sma->checkPermissions('payments', true);
        $this->load->helper('security');
        $sale = $this->sales_model->getInvoiceByID($id);
        // if ($sale->payment_status == 'paid' || $sale->grand_total == $sale->paid) {
        //     $this->session->set_flashdata('error', lang("sale_already_paid"));
        //     $this->sma->md();
        // }
        
        //$this->form_validation->set_rules('reference_no', lang("reference_no"), 'required');
        $this->form_validation->set_rules('amount-paid', lang("amount"), 'required');
        $this->form_validation->set_rules('paid_by', lang("paid_by"), 'required');
        $this->form_validation->set_rules('userfile', lang("attachment"), 'xss_clean');
        
        if ($this->form_validation->run() == true) {

            $sale = $this->sales_model->getInvoiceByID($this->input->post('sale_id'));
            if ($this->input->post('paid_by') == 'deposit') {
                $customer_id = $sale->customer_id;
                if (! $this->site->check_customer_deposit($customer_id, $this->input->post('amount-paid'))) {
                    $this->session->set_flashdata('error', lang("amount_greater_than_deposit"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $customer_id = null;
            }
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }
            $add_ons = 'initial_first_add_ons:{}:';
            foreach ($_POST as $name => $value) {
                $value = $this->input->post($name);
                if (is_int(strpos($name, "add_ons_"))) {
                    $temp_name = str_replace('add_ons_', '', $name);
                    $add_ons .= $temp_name.':{'.$value.'}:';
                }
            }
            $payment = array(
                'date' => $date,
                'sale_id' => $this->input->post('sale_id'),
                'reference_no' => $this->input->post('reference_no') ? $this->input->post('reference_no') : $this->site->getReference('pay'),
                'amount' => $this->input->post('amount-paid'),
                'paid_by' => $this->input->post('paid_by'),
                'cheque_no' => $this->input->post('cheque_no'),
                'cc_no' => $this->input->post('paid_by') == 'gift_card' ? $this->input->post('gift_card_no') : $this->input->post('pcc_no'),
                'cc_holder' => $this->input->post('pcc_holder'),
                'cc_month' => $this->input->post('pcc_month'),
                'cc_year' => $this->input->post('pcc_year'),
                'cc_type' => $this->input->post('pcc_type'),
                'note' => $this->input->post('note'),
                'created_by' => $this->session->userdata('user_id'),
                'type' => $sale->sale_status == 'returned' ? 'returned' : 'received',
                'add_ons' => $add_ons,
            );
            

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $payment['attachment'] = $photo;
            }

            
        } elseif ($this->input->post('add_payment')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->sales_model->addPayment($payment, $customer_id)) {
            if ($sale->shop) {
                $this->load->library('sms');
                $this->sms->paymentReceived($sale->id, $payment['reference_no'], $payment['amount']);
            }

            $this->session->set_flashdata('message', lang("payment_added"));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            if ($sale->sale_status == 'returned' && $sale->paid == $sale->grand_total) {
                $this->session->set_flashdata('warning', lang('payment_was_returned'));
                $this->sma->md();
            }
            $this->data['inv'] = $sale;
            $this->data['payment_ref'] = ''; //$this->site->getReference('pay');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'sales/add_payment', $this->data);
        }
    }

    public function edit_payment($id = null)
    {
        $this->sma->checkPermissions('edit', true);
        $this->load->helper('security');
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $payment = $this->sales_model->getPaymentByID_v2($id);
        // if ($payment->paid_by == 'ppp' || $payment->paid_by == 'stripe' || $payment->paid_by == 'paypal' || $payment->paid_by == 'skrill') {
        //     $this->session->set_flashdata('error', lang('x_edit_payment'));
        //     $this->sma->md();
        // }
        $this->form_validation->set_rules('reference_no', lang("reference_no"), 'required');
        $this->form_validation->set_rules('amount-paid', lang("amount"), 'required');
        $this->form_validation->set_rules('paid_by', lang("paid_by"), 'required');
        $this->form_validation->set_rules('userfile', lang("attachment"), 'xss_clean');
        if ($this->form_validation->run() == true) {
            if ($this->input->post('paid_by') == 'deposit') {
                $sale = $this->sales_model->getInvoiceByID($this->input->post('sale_id'));
                $customer_id = $sale->customer_id;
                $amount = $this->input->post('amount-paid')-$payment->amount;
                if (! $this->site->check_customer_deposit($customer_id, $amount)) {
                    $this->session->set_flashdata('error', lang("amount_greater_than_deposit"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $customer_id = null;
            }
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = $payment->date;
            }
            $payment = array(
                'date' => $date,
                'sale_id' => $this->input->post('sale_id'),
                'reference_no' => $this->input->post('reference_no'),
                'amount' => $this->input->post('amount-paid'),
                'paid_by' => $this->input->post('paid_by'),
                'cheque_no' => $this->input->post('cheque_no'),
                'cc_no' => $this->input->post('pcc_no'),
                'cc_holder' => $this->input->post('pcc_holder'),
                'cc_month' => $this->input->post('pcc_month'),
                'cc_year' => $this->input->post('pcc_year'),
                'cc_type' => $this->input->post('pcc_type'),
                'note' => $this->input->post('note'),
                'created_by' => $this->session->userdata('user_id'),
            );

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $payment['attachment'] = $photo;
            }

            
        } elseif ($this->input->post('edit_payment')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->sales_model->updatePayment($id, $payment, $customer_id)) {
            foreach ($_POST as $name => $value) {
                if (is_int(strpos($name, "add_ons_"))) {
                    $value = $this->input->post($name);
                    $temp_name = str_replace('add_ons_', '', $name);
                    $config_data = array(
                        'table_name' => 'sma_payments',
                        'id_name' => 'id',
                        'field_add_ons_name' => 'add_ons',
                        'selected_id' => $id,
                        'add_ons_title' => $temp_name,
                        'add_ons_value' => $value,
                    );
                    $this->site->api_update_add_ons_field($config_data);
                }
            }
            $this->session->set_flashdata('message', lang("payment_updated"));
            admin_redirect("sales");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['payment'] = $payment;

            $temp = $this->site->api_select_some_fields_with_where("
                sale_id
                "
                ,"sma_payments"
                ,"id = ".$id
                ,"arr"
            );            
            $this->data['inv'] = $this->sales_model->getInvoiceByID($temp[0]['sale_id']);
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'sales/edit_payment', $this->data);
        }
    }
    
    public function delete_payment($id = null)
    {
        $this->sma->checkPermissions('delete');

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
           
        if ($this->sales_model->deletePayment($id)) {
            $this->session->set_flashdata('message', lang("payment_deleted"));
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    /* --------------------------------------------------------------------------------------------- */

    public function suggestions()
    {
        $term = $this->input->get('term', true);
        $warehouse_id = $this->input->get('warehouse_id', true);
        $customer_id = $this->input->get('customer_id', true);
        $mode = $this->input->get('mode', true);

        if (strlen($term) < 1 || !$term) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . admin_url('welcome') . "'; }, 10);</script>");
        }

        
        $analyzed = $this->sma->analyze_term($term);
        $sr = $analyzed['term'];
        $option_id = $analyzed['option_id'];
        // $pr =
        // return $this->sma->send_json($pr);
        $warehouse = $this->site->getWarehouseByID($warehouse_id);

        $customer = $this->site->getCompanyByID_v2($customer_id);
        $customer_group = $this->site->getCustomerGroupByID($customer->customer_group_id);

        //$rows = $this->sales_model->getProductNames($sr, $warehouse_id);
        $config_data = array(
            'table_name' => 'sma_products',
            'select_table' => 'sma_products',
            'translate' => '',
            'select_condition' => "getTranslate(add_ons,'no_use','".f_separate."','".v_separate."') != 'yes' and (name like '%" . $sr . "%' or code like '%" . $sr . "%') order by name asc limit 20",
        );
        $select_data = $this->site->api_select_data_v2($config_data);
        for ($i=0;$i<count($select_data);$i++) {
            foreach(array_keys($select_data[$i]) as $key){
                $rows[$i]->{$key} = $select_data[$i][$key];
            }
        }
       
        if ($rows) {
            $r = 0;
            foreach ($rows as $row) {
                $c = uniqid(mt_rand(), true);
                unset($row->cost, $row->details, $row->product_details, $row->image, $row->barcode_symbology, $row->cf1, $row->cf2, $row->cf3, $row->cf4, $row->cf5, $row->cf6, $row->supplier1price, $row->supplier2price, $row->cfsupplier3price, $row->supplier4price, $row->supplier5price, $row->supplier1, $row->supplier2, $row->supplier3, $row->supplier4, $row->supplier5, $row->supplier1_part_no, $row->supplier2_part_no, $row->supplier3_part_no, $row->supplier4_part_no, $row->supplier5_part_no);
                $option = false;
                $row->quantity = 0;
                $row->item_tax_method = $row->tax_method;
                $row->qty = 1;
                $row->discount = '0';
                $row->serial = '';

                $options = $this->sales_model->getProductOptions($row->id, $warehouse_id);

                if ($options) {
                    $opt = $option_id && $r == 0 ? $this->sales_model->getProductOptionByID($option_id) : $options[0];
                    if (!$option_id || $r > 0) {
                        $option_id = $opt->id;
                    }
                } else {
                    $opt = json_decode('{}');
                    $opt->price = 0;
                    $option_id = false;
                }

                $row->option = $option_id;
                /*
                    $pis = $this->site->getPurchasedItems($row->id, $warehouse_id, $row->option);
                    if ($pis) {
                        $row->quantity = 0;
                        foreach ($pis as $pi) {
                            $row->quantity += $pi->quantity_balance;
                        }
                    }
                    if ($options) {
                        $option_quantity = 0;
                        foreach ($options as $option) {
                            $pis = $this->site->getPurchasedItems($row->id, $warehouse_id, $row->option);
                            if ($pis) {
                                foreach ($pis as $pi) {
                                    $option_quantity += $pi->quantity_balance;
                                }
                            }
                            if ($option->quantity > $option_quantity) {
                                $option->quantity = $option_quantity;
                            }
                        }
                    }
                */


                $config_data = array(
                    'id' => $row->id,
                    'customer_id' => $customer_id,
                    'option_id' => $option_id
                );
                $temp = $this->site->api_calculate_product_price($config_data);
                $item_net_price = $temp['price'];
                if ($temp['promotion']['price'] > 0)
                    $temp_discount = $temp['temp_price'] - $temp['price'];
                else
                    $temp_discount = 0;                
                $row->temp_original_price = $this->sma->formatDecimal($temp['temp_price']);
                $row->temp_unit_price = $temp['price'];
                $row->temp_discount = $temp_discount;
                $row->unit_price = $temp['price'];
                $row->real_unit_price = $row->temp_original_price;
                $row->temp_discount_rate = '';
                if (intval($temp['promotion']['rate']) > 0 && $temp['promotion']['price'] > 0) {
                    $row->temp_discount_rate = $this->sma->formatMoney($temp_discount).' ('.number_format($temp['promotion']['rate'],2).'%)';
                    $row->temp_discount_rate_value = number_format($temp['promotion']['rate'],2);
                }
                $row->price = $temp['price'];

                if ($mode == 'sample') {
                    $row->price = 0;
                }


                $price = $row->price;
                                
                $row->price = $this->sma->formatDecimal($price, 2);
                /*** END ***/
                
                  
                $row->discount_product = $customer->discount_product;
                              
                $row->base_quantity = 1;
                $row->base_unit = $row->unit;
                $row->base_unit_price = $temp['temp_price'];
                $row->unit = $row->sale_unit ? $row->sale_unit : $row->unit;
                $row->comment = '';
                $combo_items = false;
                $row->api_discount = 0;                

                if ($row->type == 'combo') {
                    $combo_items = $this->sales_model->getProductComboItems($row->id, $warehouse_id);
                }
                $units = $this->site->getUnitsByBUID($row->base_unit);
                $tax_rate = $this->site->getTaxRateByID($row->tax_rate);

                $pr[] = array('id' => sha1($c.$r), 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'category' => $row->category_id,
                    'row' => $row, 'combo_items' => $combo_items, 'tax_rate' => $tax_rate, 'units' => $units, 'options' => $options);
                $r++;
            }
            $this->sma->send_json($pr);
        } else {
            $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
        }
    }

    /* ------------------------------------ Gift Cards ---------------------------------- */

    public function gift_cards()
    {
        $this->sma->checkPermissions();

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('gift_cards')));
        $meta = array('page_title' => lang('gift_cards'), 'bc' => $bc);
        $this->page_construct('sales/gift_cards', $meta, $this->data);
    }

    public function getGiftCards()
    {
        $this->load->library('datatables');
        $this->datatables
            ->select($this->db->dbprefix('gift_cards') . ".id as id, card_no, value, balance, CONCAT(" . $this->db->dbprefix('users') . ".first_name, ' ', " . $this->db->dbprefix('users') . ".last_name) as created_by, customer, expiry", false)
            ->join('users', 'users.id=gift_cards.created_by', 'left')
            ->from("gift_cards")
            ->add_column("Actions", "<div class=\"text-center\"><a href='" . admin_url('sales/view_gift_card/$1') . "' class='tip' title='" . lang("view_gift_card") . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-eye\"></i></a> <a href='" . admin_url('sales/topup_gift_card/$1') . "' class='tip' title='" . lang("topup_gift_card") . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-dollar\"></i></a> <a href='" . admin_url('sales/edit_gift_card/$1') . "' class='tip' title='" . lang("edit_gift_card") . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("delete_gift_card") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('sales/delete_gift_card/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");
        //->unset_column('id');

        echo $this->datatables->generate();
    }

    public function view_gift_card($id = null)
    {
        $this->data['page_title'] = lang('gift_card');
        $gift_card = $this->site->getGiftCardByID($id);
        $this->data['gift_card'] = $this->site->getGiftCardByID($id);
        $this->data['customer'] = $this->site->getCompanyByID($gift_card->customer_id);
        $this->data['topups'] = $this->sales_model->getAllGCTopups($id);
        $this->load->view($this->theme . 'sales/view_gift_card', $this->data);
    }

    public function topup_gift_card($card_id)
    {
        $this->sma->checkPermissions('add_gift_card', true);
        $card = $this->site->getGiftCardByID($card_id);
        $this->form_validation->set_rules('amount', lang("amount"), 'trim|integer|required');

        if ($this->form_validation->run() == true) {
            $data = array('card_id' => $card_id,
                'amount' => $this->input->post('amount'),
                'date' => date('Y-m-d H:i:s'),
                'created_by' => $this->session->userdata('user_id'),
            );
            $card_data['balance'] = ($this->input->post('amount')+$card->balance);
            // $card_data['value'] = ($this->input->post('amount')+$card->value);
            if ($this->input->post('expiry')) {
                $card_data['expiry'] = $this->sma->fld(trim($this->input->post('expiry')));
            }
        } elseif ($this->input->post('topup')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("sales/gift_cards");
        }

        if ($this->form_validation->run() == true && $this->sales_model->topupGiftCard($data, $card_data)) {
            $this->session->set_flashdata('message', lang("topup_added"));
            admin_redirect("sales/gift_cards");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['card'] = $card;
            $this->data['page_title'] = lang("topup_gift_card");
            $this->load->view($this->theme . 'sales/topup_gift_card', $this->data);
        }
    }

    public function validate_gift_card($no)
    {
        //$this->sma->checkPermissions();
        if ($gc = $this->site->getGiftCardByNO($no)) {
            if ($gc->expiry) {
                if ($gc->expiry >= date('Y-m-d')) {
                    $this->sma->send_json($gc);
                } else {
                    $this->sma->send_json(false);
                }
            } else {
                $this->sma->send_json($gc);
            }
        } else {
            $this->sma->send_json(false);
        }
    }

    public function add_gift_card()
    {
        $this->sma->checkPermissions(false, true);

        $this->form_validation->set_rules('card_no', lang("card_no"), 'trim|is_unique[gift_cards.card_no]|required');
        $this->form_validation->set_rules('value', lang("value"), 'required');

        if ($this->form_validation->run() == true) {
            $customer_details = $this->input->post('customer') ? $this->site->getCompanyByID($this->input->post('customer')) : null;
            $customer = $customer_details ? $customer_details->company : null;
            $data = array('card_no' => $this->input->post('card_no'),
                'value' => $this->input->post('value'),
                'customer_id' => $this->input->post('customer') ? $this->input->post('customer') : null,
                'customer' => $customer,
                'balance' => $this->input->post('value'),
                'expiry' => $this->input->post('expiry') ? $this->sma->fsd($this->input->post('expiry')) : null,
                'created_by' => $this->session->userdata('user_id'),
            );
            $sa_data = array();
            $ca_data = array();
            if ($this->input->post('staff_points')) {
                $sa_points = $this->input->post('sa_points');
                $user = $this->site->getUser($this->input->post('user'));
                if ($user->award_points < $sa_points) {
                    $this->session->set_flashdata('error', lang("award_points_wrong"));
                    admin_redirect("sales/gift_cards");
                }
                $sa_data = array('user' => $user->id, 'points' => ($user->award_points - $sa_points));
            } elseif ($customer_details && $this->input->post('use_points')) {
                $ca_points = $this->input->post('ca_points');
                if ($customer_details->award_points < $ca_points) {
                    $this->session->set_flashdata('error', lang("award_points_wrong"));
                    admin_redirect("sales/gift_cards");
                }
                $ca_data = array('customer' => $this->input->post('customer'), 'points' => ($customer_details->award_points - $ca_points));
            }
            
        } elseif ($this->input->post('add_gift_card')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("sales/gift_cards");
        }

        if ($this->form_validation->run() == true && $this->sales_model->addGiftCard($data, $ca_data, $sa_data)) {
            $this->session->set_flashdata('message', lang("gift_card_added"));
            admin_redirect("sales/gift_cards");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['users'] = $this->sales_model->getStaff();
            $this->data['page_title'] = lang("new_gift_card");
            $this->load->view($this->theme . 'sales/add_gift_card', $this->data);
        }
    }

    public function edit_gift_card($id = null)
    {
        $this->sma->checkPermissions(false, true);

        $this->form_validation->set_rules('card_no', lang("card_no"), 'trim|required');
        $gc_details = $this->site->getGiftCardByID($id);
        if ($this->input->post('card_no') != $gc_details->card_no) {
            $this->form_validation->set_rules('card_no', lang("card_no"), 'is_unique[gift_cards.card_no]');
        }
        $this->form_validation->set_rules('value', lang("value"), 'required');
        //$this->form_validation->set_rules('customer', lang("customer"), 'xss_clean');

        if ($this->form_validation->run() == true) {
            $gift_card = $this->site->getGiftCardByID($id);
            $customer_details = $this->input->post('customer') ? $this->site->getCompanyByID($this->input->post('customer')) : null;
            $customer = $customer_details ? $customer_details->company : null;
            $data = array('card_no' => $this->input->post('card_no'),
                'value' => $this->input->post('value'),
                'customer_id' => $this->input->post('customer') ? $this->input->post('customer') : null,
                'customer' => $customer,
                'balance' => ($this->input->post('value') - $gift_card->value) + $gift_card->balance,
                'expiry' => $this->input->post('expiry') ? $this->sma->fsd($this->input->post('expiry')) : null,
            );
        } elseif ($this->input->post('edit_gift_card')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("sales/gift_cards");
        }

        if ($this->form_validation->run() == true && $this->sales_model->updateGiftCard($id, $data)) {
            $this->session->set_flashdata('message', lang("gift_card_updated"));
            admin_redirect("sales/gift_cards");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['gift_card'] = $this->site->getGiftCardByID($id);
            $this->data['id'] = $id;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'sales/edit_gift_card', $this->data);
        }
    }

    public function sell_gift_card()
    {
        $this->sma->checkPermissions('gift_cards', true);
        $error = null;
        $gcData = $this->input->get('gcdata');
        if (empty($gcData[0])) {
            $error = lang("value") . " " . lang("is_required");
        }
        if (empty($gcData[1])) {
            $error = lang("card_no") . " " . lang("is_required");
        }

        $customer_details = (!empty($gcData[2])) ? $this->site->getCompanyByID($gcData[2]) : null;
        $customer = $customer_details ? $customer_details->company : null;
        $data = array('card_no' => $gcData[0],
            'value' => $gcData[1],
            'customer_id' => (!empty($gcData[2])) ? $gcData[2] : null,
            'customer' => $customer,
            'balance' => $gcData[1],
            'expiry' => (!empty($gcData[3])) ? $this->sma->fsd($gcData[3]) : null,
            'created_by' => $this->session->userdata('user_id'),
        );

        if (!$error) {
            if ($this->sales_model->addGiftCard($data)) {
                $this->sma->send_json(array('result' => 'success', 'message' => lang("gift_card_added")));
            }
        } else {
            $this->sma->send_json(array('result' => 'failed', 'message' => $error));
        }
    }

    public function delete_gift_card($id = null)
    {
        $this->sma->checkPermissions();

        if ($this->sales_model->deleteGiftCard($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("gift_card_deleted")));
        }
    }

    public function gift_card_actions()
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {
            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    $this->sma->checkPermissions('delete_gift_card');
                    foreach ($_POST['val'] as $id) {
                        $this->sales_model->deleteGiftCard($id);
                    }
                    $this->session->set_flashdata('message', lang("gift_cards_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel') {
                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('gift_cards'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('card_no'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('value'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('customer'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $sc = $this->site->getGiftCardByID($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $sc->card_no);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $sc->value);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $sc->customer);
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'gift_cards_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_gift_card_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function get_award_points($id = null)
    {
        $this->sma->checkPermissions('index');

        $row = $this->site->getUser($id);
        $this->sma->send_json(array('sa_points' => $row->award_points));
    }

    /* -------------------------------------------------------------------------------------- */

    public function sale_by_csv()
    {
        $this->sma->checkPermissions('csv');
        $this->load->helper('security');
        $this->form_validation->set_rules('userfile', lang("upload_file"), 'xss_clean');
        $this->form_validation->set_message('is_natural_no_zero', lang("no_zero_required"));
        $this->form_validation->set_rules('customer', lang("customer"), 'required');
        $this->form_validation->set_rules('biller', lang("biller"), 'required');
        $this->form_validation->set_rules('sale_status', lang("sale_status"), 'required');
        $this->form_validation->set_rules('payment_status', lang("payment_status"), 'required');

        if ($this->form_validation->run() == true) {

            //$reference = $this->input->post('reference_no') ? $this->input->post('reference_no') : $this->site->getReference('so');
            $customer_id = $this->input->post('customer');//18-01-2019
            $customers = $this->site->getCompanyByID($customer_id);//18-01-2019
            $is_vat = $customers->vat_no;
            $reference = $this->input->post('reference_no') ?
                                $this->input->post('reference_no') :
                                $this->site->getSaleReference($is_vat);
                         
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }
            $warehouse_id = $this->input->post('warehouse');
            $biller_id = $this->input->post('biller');
            $total_items = $this->input->post('total_items');
            $sale_status = $this->input->post('sale_status');
            $payment_status = $this->input->post('payment_status');
            $payment_term = $this->input->post('payment_term');
            $due_date = $payment_term ? date('Y-m-d', strtotime('+' . $payment_term . ' days')) : null;
            $shipping = $this->input->post('shipping') ? $this->input->post('shipping') : 0;
            $customer_details = $this->site->getCompanyByID($customer_id);
            $customer = $customer_details->company != '-'  ? $customer_details->company : $customer_details->name;
            $biller_details = $this->site->getCompanyByID($biller_id);
            $biller = $biller_details->company != '-' ? $biller_details->company : $biller_details->name;
            $note = $this->sma->clear_tags($this->input->post('note'));
            $staff_note = $this->sma->clear_tags($this->input->post('staff_note'));

            $total = 0;
            $product_tax = 0;
            $product_discount = 0;
            $gst_data = [];
            $total_cgst = $total_sgst = $total_igst = 0;
            if (isset($_FILES["userfile"])) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = 'csv';
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("sales/sale_by_csv");
                }
                $csv = $this->upload->file_name;

                $arrResult = array();
                $handle = fopen($this->digital_upload_path . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 1000, ",")) !== false) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);

                $keys = array('code', 'net_unit_price', 'quantity', 'variant', 'item_tax_rate', 'discount', 'serial');
                $final = array();
                foreach ($arrResult as $key => $value) {
                    $final[] = array_combine($keys, $value);
                }
                $rw = 2;
                foreach ($final as $csv_pr) {
                    if (isset($csv_pr['code']) && isset($csv_pr['net_unit_price']) && isset($csv_pr['quantity'])) {
                        if ($product_details = $this->sales_model->getProductByCode($csv_pr['code'])) {
                            if ($csv_pr['variant']) {
                                $item_option = $this->sales_model->getProductVariantByName($csv_pr['variant'], $product_details->id);
                                if (!$item_option) {
                                    $this->session->set_flashdata('error', lang("pr_not_found") . " ( " . $product_details->name . " - " . $csv_pr['variant'] . " ). " . lang("line_no") . " " . $rw);
                                    redirect($_SERVER["HTTP_REFERER"]);
                                }
                            } else {
                                $item_option = json_decode('{}');
                                $item_option->id = null;
                            }

                            $item_id = $product_details->id;
                            $item_type = $product_details->type;
                            $item_code = $product_details->code;
                            $item_name = $product_details->name;
                            $item_net_price = $this->sma->formatDecimal($csv_pr['net_unit_price']);
                            $item_quantity = $csv_pr['quantity'];
                            $item_tax_rate = $csv_pr['item_tax_rate'];
                            $item_discount = $csv_pr['discount'];
                            $item_serial = $csv_pr['serial'];

                            if (isset($item_code) && isset($item_net_price) && isset($item_quantity)) {
                                $product_details = $this->sales_model->getProductByCode($item_code);
                                $pr_discount = $this->site->calculateDiscount($item_discount, $item_net_price);
                                $item_net_price = $this->sma->formatDecimal(($item_net_price - $pr_discount), 4);
                                $pr_item_discount = $this->sma->formatDecimal(($pr_discount * $item_quantity), 4);
                                $product_discount += $pr_item_discount;

                                $tax = "";
                                $pr_item_tax = 0;
                                $unit_price = $item_net_price;
                                $tax_details = ((isset($item_tax_rate) && !empty($item_tax_rate)) ? $this->sales_model->getTaxRateByName($item_tax_rate) : $this->site->getTaxRateByID($product_details->tax_rate));
                                if ($tax_details) {
                                    $ctax = $this->site->calculateTax($product_details, $tax_details, $unit_price);
                                    $item_tax = $ctax['amount'];
                                    $tax = $ctax['tax'];
                                    if (!$product_details || (!empty($product_details) && $product_details->tax_method != 1)) {
                                        $item_net_price = $unit_price - $item_tax;
                                    }
                                    $pr_item_tax = $this->sma->formatDecimal($item_tax * $item_quantity, 4);
                                    if ($this->Settings->indian_gst && $gst_data = $this->gst->calculteIndianGST($pr_item_tax, ($biller_details->state == $customer_details->state), $tax_details)) {
                                        $total_cgst += $gst_data['cgst'];
                                        $total_sgst += $gst_data['sgst'];
                                        $total_igst += $gst_data['igst'];
                                    }
                                }

                                $product_tax += $pr_item_tax;
                                $subtotal = $this->sma->formatDecimal((($item_net_price * $item_quantity) + $pr_item_tax), 4);
                                $unit = $this->site->getUnitByID($product_details->unit);

                                $product = array(
                                    'product_id' => $product_details->id,
                                    'product_code' => $item_code,
                                    'product_name' => $item_name,
                                    'product_type' => $item_type,
                                    'option_id' => $item_option->id,
                                    'net_unit_price' => $item_net_price,
                                    'quantity' => $item_quantity,
                                    'product_unit_id' => $product_details->unit,
                                    'product_unit_code' => $unit->code,
                                    'unit_quantity' => $item_quantity,
                                    'warehouse_id' => $warehouse_id,
                                    'item_tax' => $pr_item_tax,
                                    'tax_rate_id' => $tax_details ? $tax_details->id : null,
                                    'tax' => $tax,
                                    'discount' => $item_discount,
                                    'item_discount' => $pr_item_discount,
                                    'subtotal' => $subtotal,
                                    'serial_no' => $item_serial,
                                    'unit_price' => $this->sma->formatDecimal(($item_net_price + $item_tax), 4),
                                    'real_unit_price' => $this->sma->formatDecimal(($item_net_price + $item_tax + $pr_discount), 4),
                                );

                                $products[] = ($product+$gst_data);
                                $total += $this->sma->formatDecimal(($item_net_price * $item_quantity), 4);
                            }
                        } else {
                            $this->session->set_flashdata('error', lang("pr_not_found") . " ( " . $csv_pr['code'] . " ). " . lang("line_no") . " " . $rw);
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                        $rw++;
                    }
                }
            }

            $order_discount = $this->site->calculateDiscount($this->input->post('order_discount'), ($total + $product_tax));
            $total_discount = $this->sma->formatDecimal(($order_discount + $product_discount), 4);
            $order_tax = $this->site->calculateOrderTax($this->input->post('order_tax'), ($total + $product_tax - $order_discount));
            $total_tax = $this->sma->formatDecimal(($product_tax + $order_tax), 4);
            $grand_total = $this->sma->formatDecimal(($total + $total_tax + $this->sma->formatDecimal($shipping) - $order_discount), 4);
            $data = array(
                'is_vat' => $is_vat,
                'date' => $date,
                'reference_no' => $reference,
                'customer_id' => $customer_id,
                'customer' => $customer,
                'biller_id' => $biller_id,
                'biller' => $biller,
                'warehouse_id' => $warehouse_id,
                'note' => $note,
                'staff_note' => $staff_note,
                'total' => $total,
                'product_discount' => $product_discount,
                'order_discount_id' => $this->input->post('order_discount'),
                'order_discount' => $order_discount,
                'total_discount' => $total_discount,
                'product_tax' => $product_tax,
                'order_tax_id' => $this->input->post('order_tax'),
                'order_tax' => $order_tax,
                'total_tax' => $total_tax,
                'shipping' => $this->sma->formatDecimal($shipping),
                'grand_total' => $grand_total,
                'total_items' => $total_items,
                'sale_status' => $sale_status,
                'payment_status' => $payment_status,
                'payment_term' => $payment_term,
                'due_date' => $due_date,
                'paid' => 0,
                'created_by' => $this->session->userdata('user_id'),
            );
            if ($this->Settings->indian_gst) {
                $data['cgst'] = $total_cgst;
                $data['sgst'] = $total_sgst;
                $data['igst'] = $total_igst;
            }

            if ($payment_status == 'paid') {
                $payment = array(
                    'date' => $date,
                    'reference_no' => $this->site->getReference('pay'),
                    'amount' => $grand_total,
                    'paid_by' => 'cash',
                    'cheque_no' => '',
                    'cc_no' => '',
                    'cc_holder' => '',
                    'cc_month' => '',
                    'cc_year' => '',
                    'cc_type' => '',
                    'created_by' => $this->session->userdata('user_id'),
                    'note' => lang('auto_added_for_sale_by_csv') . ' (' . lang('sale_reference_no') . ' ' . $reference . ')',
                    'type' => 'received',
                );
            } else {
                $payment = array();
            }

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }

            
        }

        if ($this->form_validation->run() == true && $this->sales_model->addSale($data, $products, $payment)) {
            $this->session->set_userdata('remove_slls', 1);
            $this->session->set_flashdata('message', lang("sale_added"));
            admin_redirect("sales");
        } else {
            $data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->data['billers'] = $this->site->getAllCompanies('biller');
            //$this->data['slnumber'] = $this->site->getReference('so');

            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('add_sale_by_csv')));
            $meta = array('page_title' => lang('add_sale_by_csv'), 'bc' => $bc);
            $this->page_construct('sales/sale_by_csv', $meta, $this->data);
        }
    }

    public function update_status($id)
    {
        $this->form_validation->set_rules('status', lang("sale_status"), 'required');

        if ($this->form_validation->run() == true) {
            $status = $this->input->post('status');
            $note = $this->sma->clear_tags($this->input->post('note'));
        } elseif ($this->input->post('update')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'sales');
        }

        if ($this->form_validation->run() == true && $this->sales_model->updateStatus($id, $status, $note)) {
            $this->session->set_flashdata('message', lang('status_updated'));
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'sales');
        } else {
            $this->data['inv'] = $this->sales_model->getInvoiceByID($id);
            $this->data['returned'] = false;
            if ($this->data['inv']->sale_status == 'returned' || $this->data['inv']->return_id) {
                $this->data['returned'] = true;
            }
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme.'sales/update_status', $this->data);
        }
    }

    public function packaging($id)
    {
        $sale = $this->sales_model->getInvoiceByID($id);
        $this->data['returned'] = false;
        if ($sale->sale_status == 'returned' || $sale->return_id) {
            $this->data['returned'] = true;
        }
        $this->data['warehouse'] = $this->site->getWarehouseByID($sale->warehouse_id);
        $items = $this->sales_model->getAllInvoiceItems($sale->id);
        foreach ($items as $item) {
            $packaging[] = array(
                    'name' => $item->product_code.' - '.$item->product_name,
                    'quantity' => $item->quantity.' '.$item->product_unit_code,
                    'rack' => $this->sales_model->getItemRack($item->product_id, $sale->warehouse_id),
                    );
        }
        $this->data['packaging'] = $packaging;
        $this->data['sale'] = $sale;

        $this->load->view($this->theme.'sales/packaging', $this->data);
    }

    /*** Bulk Actions On Sale ***/
    public function getPaymentCate($id)
    {
        if ($this->input->get('id')) {
            $id = $this->input->get('id', true);
        }
        $rows['results'] = $this->sales_model->getInvoiceByIDPayCate($id);
        echo json_encode($rows);
    }

    public function return_sale_v2($id = null)
    {
        $inv = $this->sales_model->getInvoiceByID_v2($id);
        if ($inv->sale_status != 'completed') {
            $this->session->set_flashdata('error', lang("You_can_only_add_return_with_sale_status_completed."));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $temp = $this->site->api_get_consignment_sale_items($id);
        if (count($temp) > 0) {
            $this->session->set_flashdata('error', lang("Can_not_add_return_with_consignment_sales."));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        
        $this->form_validation->set_message('is_natural_no_zero', lang("no_zero_required"));
        $this->form_validation->set_rules('customer', lang("customer"), 'required');
        $this->form_validation->set_rules('biller', lang("biller"), 'required');
        $this->form_validation->set_rules('surcharge', lang("surcharge"), 'required');

        if ($this->form_validation->run() == true) {
            $i = isset($_POST['product_code']) ? sizeof($_POST['product_code']) : 0;
            $temp = 0;
            for ($r = 0; $r < $i; $r++) {
                $temp = $temp + $_POST['quantity'][$r];
            }
            if ($temp <= 0) {
                $this->session->set_flashdata('error', lang("please_input_return_quantity"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
                        
            $date = ($this->Owner || $this->Admin) ? $this->sma->fld(trim($this->input->post('date'))) : date('Y-m-d H:i:s');

            $reference = $this->site->getReference('re');
            $warehouse_id = $this->input->post('warehouse');
            $customer_id = $this->input->post('customer');
            $biller_id = $this->input->post('biller');
            $total_items = $this->input->post('total_items');
            $customer_details = $this->site->getCompanyByID($customer_id);
            $customer = !empty($customer_details->company) && $customer_details->company != '-' ? $customer_details->company : $customer_details->name;
            $biller_details = $this->site->getCompanyByID($biller_id);
            $biller = !empty($biller_details->company) && $biller_details->company != '-' ? $biller_details->company : $biller_details->name;
            $note = $this->sma->clear_tags($this->input->post('note'));
            $staff_note = $this->sma->clear_tags($this->input->post('staff_note'));

            $total = 0;
            $product_tax = 0;
            $product_discount = 0;
            $gst_data = [];
            $total_cgst = $total_sgst = $total_igst = 0;
            $i = isset($_POST['product_code']) ? sizeof($_POST['product_code']) : 0;
            for ($r = 0; $r < $i; $r++) {
                if ($_POST['quantity'][$r] > 0) {
                    $item_id = $_POST['product_id'][$r];
                    $item_type = $_POST['product_type'][$r];
                    $item_code = $_POST['product_code'][$r];
                    $item_name = $_POST['product_name'][$r];
                    $item_option = isset($_POST['product_option'][$r]) && $_POST['product_option'][$r] != 'false' && $_POST['product_option'][$r] != 'null' ? $_POST['product_option'][$r] : null;
                    $real_unit_price = $this->sma->formatDecimal($_POST['real_unit_price'][$r]);
                    $unit_price = $this->sma->formatDecimal($_POST['unit_price'][$r]);
                    $item_unit_quantity = $_POST['quantity'][$r];
                    $item_serial = isset($_POST['serial'][$r]) ? $_POST['serial'][$r] : '';
                    $item_tax_rate = isset($_POST['product_tax'][$r]) ? $_POST['product_tax'][$r] : null;
                    $item_discount = isset($_POST['product_discount'][$r]) ? $_POST['product_discount'][$r] : null;
                    $item_unit = $_POST['product_unit'][$r];
                    $item_quantity = $_POST['product_base_quantity'][$r];
    
                    if (isset($item_code) && isset($real_unit_price) && isset($unit_price) && isset($item_quantity)) {
                        $product_details = $item_type != 'manual' ? $this->site->getProductByCode($item_code) : null;
                        $pr_discount = $this->site->calculateDiscount($item_discount, $unit_price);
                        $unit_price = $this->sma->formatDecimal($unit_price - $pr_discount);
                        $item_net_price = $unit_price;
                        $pr_item_discount = $this->sma->formatDecimal($pr_discount * $item_unit_quantity);
                        $product_discount += $pr_item_discount;
                        $pr_item_tax = $item_tax = 0;
                        $tax = "";
    
                        if (isset($item_tax_rate) && $item_tax_rate != 0) {
                            $tax_details = $this->site->getTaxRateByID($item_tax_rate);
                            $ctax = $this->site->calculateTax($product_details, $tax_details, $unit_price);
                            $item_tax = $ctax['amount'];
                            $tax = $ctax['tax'];
                            if (!$product_details || (!empty($product_details) && $product_details->tax_method != 1)) {
                                $item_net_price = $unit_price - $item_tax;
                            }
                            $pr_item_tax = $this->sma->formatDecimal(($item_tax * $item_unit_quantity), 4);
                            if ($this->Settings->indian_gst && $gst_data = $this->gst->calculteIndianGST($pr_item_tax, ($biller_details->state == $customer_details->state), $tax_details)) {
                                $total_cgst += $gst_data['cgst'];
                                $total_sgst += $gst_data['sgst'];
                                $total_igst += $gst_data['igst'];
                            }
                        }
    
                        $product_tax += $pr_item_tax;
                        $subtotal = (($item_net_price * $item_unit_quantity) + $pr_item_tax);
                        $unit = $this->site->getUnitByID($item_unit);
    
                        $product = array(
                            'product_id' => $item_id,
                            'product_code' => $item_code,
                            'product_name' => $item_name,
                            'product_type' => $item_type,
                            'option_id' => $item_option,
                            'net_unit_price' => $item_net_price,
                            'unit_price' => $this->sma->formatDecimal($item_net_price + $item_tax),
                            'quantity' => $item_quantity,
                            'product_unit_id' => $unit ? $unit->id : null,
                            'product_unit_code' => $unit ? $unit->code : null,
                            'unit_quantity' => $item_unit_quantity,
                            'warehouse_id' => $warehouse_id,
                            'item_tax' => $pr_item_tax,
                            'tax_rate_id' => $item_tax_rate,
                            'tax' => $tax,
                            'discount' => $item_discount,
                            'item_discount' => $pr_item_discount,
                            'subtotal' => $this->sma->formatDecimal($subtotal),
                            'serial_no' => $item_serial,
                            'real_unit_price' => $real_unit_price,
                        );
    
                        $products[] = ($product + $gst_data);
                        $total += $this->sma->formatDecimal(($item_net_price * $item_unit_quantity), 4);
                    }
                }
            }
            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("order_items"), 'required');
            } else {
                krsort($products);
            }

            $surcharge = $this->sma->formatDecimal($this->input->post('surcharge'), 4);
            $order_discount = $this->site->calculateDiscount($this->input->post('order_discount'), ($total + $product_tax));
            $total_discount = $this->sma->formatDecimal(($order_discount + $product_discount), 4);
            $order_tax = $this->site->calculateOrderTax($this->input->post('order_tax'), ($total + $product_tax - $order_discount));
            $total_tax = $this->sma->formatDecimal(($product_tax + $order_tax), 4);
            $grand_total = $this->sma->formatDecimal(($total + $total_tax - $order_discount + $surcharge), 4);
            $data = array('date' => $date,
                'reference_no' => $reference,
                'customer_id' => $customer_id,
                'customer' => $customer,
                'biller_id' => $biller_id,
                'biller' => $biller,
                'warehouse_id' => $warehouse_id,
                'note' => $note,
                'staff_note' => $staff_note,
                'total' => $total,
                'product_discount' => $product_discount,
                'order_discount_id' => $this->input->post('order_discount'),
                'order_discount' => 0,
                'total_discount' => $total_discount,
                'product_tax' => $product_tax,
                'order_tax_id' => $this->input->post('order_tax'),
                'order_tax' => $order_tax,
                'total_tax' => $total_tax,
                'grand_total' => $grand_total,
                'total_items' => $total_items,
                'paid' => 0,
                'created_by' => $this->session->userdata('user_id'),
                'hash' => hash('sha256', microtime() . mt_rand()),
                'surcharge' => $surcharge,
            );
            if ($this->Settings->indian_gst) {
                $data['cgst'] = $total_cgst;
                $data['sgst'] = $total_sgst;
                $data['igst'] = $total_igst;
            }

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }

            
        }

        $this->load->admin_model('returns_model');
        if ($this->form_validation->run() == true && $this->returns_model->addReturn($data, $products, $id)) {
            $temp_auto_id = $this->site->api_auto_id('sma_returns', 'id');
            $temp_auto_id = $temp_auto_id - 1;
            $config_data = array(
                'table_name' => 'sma_returns',
                'id_name' => 'id',
                'field_add_ons_name' => 'add_ons',
                'selected_id' => $temp_auto_id,
                'add_ons_title' => 'sale_id',
                'add_ons_value' => $id,
            );
            $this->site->api_update_add_ons_field($config_data);
            
            $select_data = $this->sales_model->api_select_data($id);
            $temp = $this->site->api_get_consignment_sale_items($id);
            if (count($temp) > 0) {
                $config_data = array(
                    'table_name' => 'sma_returns',
                    'id_name' => 'id',
                    'field_add_ons_name' => 'add_ons',
                    'selected_id' => $temp_auto_id,
                    'add_ons_title' => 'cs_reference_no',
                    'add_ons_value' => $select_data[0]['cs_reference_no'],
                );
                $this->site->api_update_add_ons_field($config_data);
            }
            $this->sales_model->api_reverse_consignment_purchase($id);
            $this->sales_model->api_calculate_consignment_purchase_wrapper($id);
                        
            $this->session->set_userdata('remove_rels', 1);
            $this->session->set_flashdata('message', lang("return_added"));
            admin_redirect("sales");
        } else {
            $this->data['inv'] = $inv;
            $temp_return = $this->site->api_select_some_fields_with_where(
                "id,getTranslate(add_ons,'sale_id','".f_separate."','".v_separate."') as sale_id
                ",
                "sma_returns",
                "getTranslate(add_ons,'sale_id','".f_separate."','".v_separate."') = ".$id,
                "arr"
            );

            $inv_items = $this->sales_model->getAllInvoiceItems($id);
            $c = rand(100000, 9999999);
            foreach ($inv_items as $item) {
                $row = $this->site->getProductByID($item->product_id);

                $temp_returned_qty = 0;
                for ($i=0;$i<count($temp_return);$i++) {
                    $temp_return_item = $this->site->api_select_some_fields_with_where(
                        "quantity
                        ",
                        "sma_return_items",
                        "return_id = ".$temp_return[$i]['id']." and product_id = ".$item->product_id,
                        "arr"
                    );
                    if (count($temp_return_item) > 0) {
                        $temp_returned_qty = $temp_returned_qty + $temp_return_item[0]['quantity'];
                    }
                }

                
                if (!$row) {
                    $row = json_decode('{}');
                    $row->tax_method = 0;
                    $row->quantity = 0;
                } else {
                    unset($row->cost, $row->details, $row->product_details, $row->image, $row->barcode_symbology, $row->cf1, $row->cf2, $row->cf3, $row->cf4, $row->cf5, $row->cf6, $row->supplier1price, $row->supplier2price, $row->cfsupplier3price, $row->supplier4price, $row->supplier5price, $row->supplier1, $row->supplier2, $row->supplier3, $row->supplier4, $row->supplier5, $row->supplier1_part_no, $row->supplier2_part_no, $row->supplier3_part_no, $row->supplier4_part_no, $row->supplier5_part_no);
                }

                $row->id = $item->product_id;
                $row->code = $item->product_code;
                $row->name = $item->product_name;
                $row->type = $item->product_type;
                $row->item_tax_method = $row->tax_method;
                $options = $this->sales_model->getProductOptions($row->id, $item->warehouse_id);
                
                if ($options) {
                    $opt = $option_id && $r == 0 ? $this->returns_model->getProductOptionByID($option_id) : current($options);
                    if (!$option_id || $r > 0) {
                        $option_id = $opt->id;
                    }
                } else {
                    $opt = json_decode('{}');
                    $opt->price = 0;
                    $option_id = false;
                }
                if ($row->promotion) {
                    $row->price = $row->promo_price;
                }
                $row->real_unit_price = $row->price;
                $row->base_quantity = $item->quantity;
                $row->base_unit = !empty($row->unit) ? $row->unit : $item->product_unit_id;
                $row->base_unit_price = !empty($row->price) ? $row->price : $item->unit_price;
                $row->unit = $item->product_unit_id;
                $row->qty = 0;
                
                $temp = $item->quantity - $temp_returned_qty;
                $row->oqty = $temp;
                $row->sale_qty = $item->quantity;
                $row->returned_qty = $temp_returned_qty;
                                
                $row->discount = $item->discount ? $item->discount : '0';
                $row->serial = $item->serial_no;
                $row->option = $item->option_id;
                $row->tax_rate = $item->tax_rate_id;
                $row->comment = '';
                $combo_items = false;
                if ($row->type == 'combo') {
                    $combo_items = $this->site->getProductComboItems($row->id);
                }
                $units = $this->site->getUnitsByBUID($row->base_unit);
                $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                $ri = $this->Settings->item_addition ? $row->id : $c;

                $pr[$ri] = array('id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")",
                    'row' => $row, 'combo_items' => $combo_items, 'tax_rate' => $tax_rate, 'units' => $units, 'options' => $options);
                $c++;
            }
            $this->data['inv_items'] = json_encode($pr);
            $this->data['id'] = $id;
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['billers'] = $this->site->getAllCompanies('biller');
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->data['units'] = $this->site->getAllBaseUnits();
            $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
                        
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('return_sale')));
            $meta = array('page_title' => lang('return_sale'), 'bc' => $bc);
            $this->page_construct('sales/return_sale_v2', $meta, $this->data);
        }
    }

    public function generate_ref_no($id = null)
    {
        $this->sma->checkPermissions();
        $sale = $this->sales_model->getInvoiceByID($id);
        $config_data = array(
            'table_name' => 'sma_companies',
            'select_table' => 'sma_companies',
            'select_condition' => "id = ".$sale->customer_id,
        );
        $customer = $this->site->api_select_data_v2($config_data);

        if ($this->input->post('reference_no')) {
            $config_data = array(
                'type' => 'sale',
                'date' => $sale->date,
                'customer_id' => $sale->customer_id,
                'update' => 1,
            );
            $temp = $this->site->api_calculate_reference_no($config_data);

            $update_array = array(
                'reference_no' => $temp['reference_no'],
                'sale_status' => 'preparing',
            );
            $this->site->api_update_table('sma_sales', $update_array, "id = ".$id);
            $config_data4 = array(
                'date' => $sale->date
            );
            //$this->site->api_calculate_sl_reference_no_by_date($config_data4);

            $temp_riel = $this->site->api_select_some_fields_with_where(
                "rate
                ",
                "sma_currencies",
                "code = 'KHR' and YEAR(date) = YEAR('".$sale->date."') and MONTH(date) = MONTH('".$sale->date."') and DAY(date) = DAY('".$sale->date."') order by date desc limit 1",
                "arr"
            );
            if ($temp_riel[0]['rate'] > 0) {
                $config_data_2 = array(
                    'table_name' => 'sma_sales',
                    'id_name' => 'id',
                    'field_add_ons_name' => 'add_ons',
                    'selected_id' => $id,
                    'add_ons_title' => 'kh_currency_rate',
                    'add_ons_value' => $temp_riel[0]['rate'],
                );
                $this->site->api_update_add_ons_field($config_data_2);
            }
            
            $this->session->set_flashdata('message', $temp['reference_no'].' '.lang("Reference_no_is_generated"));
            admin_redirect("sales");
        } else {
            $this->data['customer'] = $this->site->getCompanyByID($sale->customer_id);
            $this->data['address'] = $this->site->getAddressByID($sale->address_id);
            $this->data['inv'] = $sale;

            $config_data = array(
                'type' => 'sale',
                'date' => $sale->date,
                'customer_id' => $sale->customer_id,
                'update' => '',
            );
            $temp = $this->site->api_calculate_reference_no($config_data);

            $this->data['reference_no'] = $temp['reference_no'];
            $this->data['modal_js'] = $this->site->modal_js();

            $this->load->view($this->theme . 'sales/generate_ref_no', $this->data);
        }
    }
    public function getSelectCompanyBranch()
    {
        if ($this->session->userdata('user_id') != '') {
            foreach ($_GET as $name => $value) {
                ${$name} = $value;
            }
            $temp = $this->site->api_select_some_fields_with_where(
                "*
                ",
                "sma_companies",
                "id = ".$id,
                "arr"
            );
            $temp2 = $this->site->api_select_some_fields_with_where(
                "id,
                getTranslate(translate,'en','".f_separate."','".v_separate."') as title,
                getTranslate(add_ons,'branch_number','".f_separate."','".v_separate."') as branch_number,
                getTranslate(add_ons,'address','".f_separate."','".v_separate."') as address
                ",
                "sma_company_branch",
                "parent_id = ".$temp[0]['id']." order by title asc",
                "arr"
            );
            $temp_value = '';
            $temp_selected_0 = '';
            $temp_selected = '';
            for ($i=0;$i<count($temp2);$i++) {
                $temp_value = $temp2[$i]['id'].'<api>'.$temp2[$i]['branch_number'];
                $tr[$temp_value] = $temp2[$i]['title'];
                if ($i == 0) {
                    $temp_selected_0 = $temp_value;
                }
            }
            if (count($temp2) <= 0) {
                $tr[''] = lang("none");
            }
 
            $result = form_dropdown('company_branch', $tr, $temp_selected_0, 'data-placeholder="'.lang("none").'" class="form-control" id="company_branch" onchange="var temp = this.value.split(\'<api>\'); $(\'#branch_number\').val(temp[1]); $(\'#company_branch_id\').val(temp[0]);"').'api-ajax-request-multiple-result-split';
            echo $result;
        }
    }

    public function set_delivering($id = null)
    {
        $select_data = array();
        if ($id > 0) {
            $select_data = $this->sales_model->api_select_data($id);
        } else {
            $this->session->set_flashdata('error', lang("no_sale_selected"));
            admin_redirect("sales");
        }

        $this->form_validation->set_rules('add_ons_delivery_person', lang("delivery_person"), 'trim|required');

        if ($this->form_validation->run() == true) {
            if ($select_data[0]['sale_status'] != 'preparing' && $select_data[0]['sale_status'] != 'delivering' && $select_data[0]['sale_status'] != 'completed') {
                $this->session->set_flashdata('error', lang("Can_not_set_delivering_with_sale_status_pending_or_print_needed."));
                admin_redirect("sales");
            }

            $config_data = array(
                'table_name' => 'sma_sales',
                'id_name' => 'id',
                'field_add_ons_name' => 'add_ons',
                'selected_id' => $select_data[0]['id'],
                'add_ons_title' => 'delivery_person',
                'add_ons_value' => $this->input->post('add_ons_delivery_person'),
            );
            $this->site->api_update_add_ons_field($config_data);
            
            if ($select_data[0]['sale_status'] == 'preparing') {
                $update_array = array(
                    'sale_status' => 'delivering',
                );
                $this->site->api_update_table('sma_sales', $update_array, "id = ".$select_data[0]['id']);

                $config_data_3 = array(
                    'sale_id' => $id,
                );
                $this->sales_model->api_reduce_stock_from_sale($config_data_3);
                $this->sales_model->api_calculate_consignment_purchase_wrapper($id);
            }
        
            $this->session->set_flashdata('message', lang("delivery_person").' '.lang("updated"));
            admin_redirect("sales");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();

            $this->data['select_data'] = $select_data;
            $this->data['delivery_person'] = $this->sales_model->get_delivery_person();
            $this->load->view($this->theme . 'sales/set_delivering', $this->data);
        }
    }

    public function set_delivering_bulk($val)
    {
        $select_data = array();

        $this->form_validation->set_rules('add_ons_delivery_person', lang("delivery_person"), 'trim|required');
        if ($this->form_validation->run() == true) {
            $temp = $this->input->post('sales');
            $temp = explode('-', $temp);
            $temp2 = array();
            for ($i=0;$i<count($temp);$i++) {
                if ($temp[$i] > 0) {
                    array_push($temp2, $temp[$i]);
                }
            }
            
            $temp_count = 0;
            foreach ($temp2 as $id) {
                $select_data = $this->sales_model->api_select_data($id);
                
                if ($select_data[0]['sale_status'] != 'pending' && $select_data[0]['sale_status'] != 'print needed') {
                    $temp_count++;
                    $config_data = array(
                        'table_name' => 'sma_sales',
                        'id_name' => 'id',
                        'field_add_ons_name' => 'add_ons',
                        'selected_id' => $id,
                        'add_ons_title' => 'delivery_person',
                        'add_ons_value' => $this->input->post('add_ons_delivery_person'),
                    );
                    $this->site->api_update_add_ons_field($config_data);
                    
                    if ($select_data[0]['sale_status'] == 'preparing') {
                        $update_array = array(
                            'sale_status' => 'delivering',
                        );
                        $this->site->api_update_table('sma_sales', $update_array, "id = ".$id);


                        $config_data_3 = array(
                            'sale_id' => $id,
                        );
                        $this->sales_model->api_reduce_stock_from_sale($config_data_3);


                        $this->sales_model->api_calculate_consignment_purchase_wrapper($id);
                    }
                }
            }
            if ($temp_count > 0) {
                $this->session->set_flashdata('message', $temp_count.' '.lang("record").'(s) '.lang("updated"));
            } else {
                $this->session->set_flashdata('error', lang("Can_not_set_delivering_with_sale_status_pending_or_print_needed."));
            }
            admin_redirect("sales");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();

            $this->data['select_data'] = '';
            $this->data['delivery_person'] = $this->sales_model->get_delivery_person();
            $this->data['sales'] = $val;
            $this->load->view($this->theme . 'sales/set_delivering', $this->data);
        }
    }

    public function no_sale_seleted()
    {
        $this->session->set_flashdata('error', lang("no_sale_selected"));
        redirect($_SERVER["HTTP_REFERER"]);
    }

    public function api_order_notification()
    {
        if ($this->Owner || $this->Admin) {
            $temp5 = $this->site->api_select_some_fields_with_where(
                "id,
                getTranslate(add_ons,'order_notification','".f_separate."','".v_separate."') as order_notification
                ",
                "sma_sales",
                "date > DATE_SUB(NOW(), INTERVAL 1 HOUR) order by id asc",
                "arr"
            );
            $selected_id = '';
            if (count($temp5) > 0) {
                for ($j=0;$j<count($temp5);$j++) {
                    $temp2 = explode('-', $temp5[$j]['order_notification']);
                    $b = 1;
                    for ($i=0;$i<count($temp2);$i++) {
                        if ($temp2[$i] == $this->session->userdata('user_id')) {
                            $b = 0;
                            break;
                        }
                    }
                    if ($b == 1) {
                        $selected_id = $temp5[$j]['id'];
                        break;
                    }
                }
                
                if ($selected_id != '') {
                    $temp = $this->site->api_select_some_fields_with_where(
                        "id,customer,grand_total,
                        getTranslate(add_ons,'company_branch','".f_separate."','".v_separate."') as company_branch,
                        getTranslate(add_ons,'delivery_date','".f_separate."','".v_separate."') as delivery_date,
                        getTranslate(add_ons,'order_notification','".f_separate."','".v_separate."') as order_notification
                        ",
                        "sma_sales",
                        "id = ".$selected_id,
                        "arr"
                    );
                } else {
                    $temp = array();
                }
            }

            $result = '';
            if (count($temp) > 0) {
                $config_data = array(
                    'table_name' => 'sma_sales',
                    'id_name' => 'id',
                    'field_add_ons_name' => 'add_ons',
                    'selected_id' => $temp[0]['id'],
                    'add_ons_title' => 'order_notification',
                    'add_ons_value' => $temp[0]['order_notification'].'-'.$this->session->userdata('user_id'),
                );
                $this->site->api_update_add_ons_field($config_data);

                $temp_img = $this->site->api_select_some_fields_with_where(
                    "product_id
                    ",
                    "sma_sale_items",
                    "sale_id = ".$temp[0]['id']." order by id asc limit 1",
                    "arr"
                );
                $temp_img_2 = $this->site->api_select_some_fields_with_where(
                    "image
                    ",
                    "sma_products",
                    "id = ".$temp_img[0]['product_id'],
                    "arr"
                );
                
                $temp4 = '';
                if ($temp[0]['company_branch'] > 0) {
                    $temp_branch = $this->site->api_select_some_fields_with_where(
                        "
                        getTranslate(translate,'en','".f_separate."','".v_separate."') as title
                        ",
                        "sma_company_branch",
                        "id = ".$temp[0]['company_branch'],
                        "arr"
                    );
                    $temp4 .= ', BRANCH: '.$temp_branch[0]['title'];
                }
                if ($temp[0]['delivery_date'] != '') {
                    $temp4 .= ', DELIVERY DATE: '.$temp[0]['delivery_date'];
                }
                
                
                
                $temp3[1] = 'api-ajax-request-multiple-result-split';
                $temp3[1] .= $temp[0]['id'];
                $temp3[2] = 'api-ajax-request-multiple-result-split';
                $temp3[2] .= 'Customer Order: '.$this->sma->formatMoney($temp[0]['grand_total']);
                $temp3[3] = 'api-ajax-request-multiple-result-split';
                $temp3[3] .= base_url().'assets/uploads/'.$temp_img_2[0]['image'];
                $temp3[4] = 'api-ajax-request-multiple-result-split';
                $temp3[4] .= $temp[0]['customer'];
                $temp3[4] .= $temp4;
                $temp3[5] = 'api-ajax-request-multiple-result-split';
                $temp3[5] .= base_url().'admin/sales/view/'.$temp[0]['id'];
                $result = $temp3[1].$temp3[2].$temp3[3].$temp3[4].$temp3[5];
            }
            if ($result != '') {
                echo $result;
            } else {
                echo '<script>window.location = "'.base_url().'";</script>';
            }
        } else {
            echo '<script>window.location = "'.base_url().'";</script>';
        }
    }


    public function sale_summary($id = null)
    {
        $this->sma->checkPermissions();

        $sort_by = $this->input->post('sort_by');
        $sort_order = $this->input->post('sort_order');

        $per_page = $this->api_settings[0]['rows_per_page'];

        if ($this->input->get('per_page') != '') {
            $per_page = $this->input->get('per_page');
        }
        
        $page = $this->input->get('page') != '' ? $this->input->get('page') : 1;

        $offset_no = $page != '' ? ($page - 1) * $per_page : $offset_no;
        
        if ($page != '') {
            $offset_no = ($page - 1) * $per_page;
        } else {
            $offset_no = 0;
        }

        $search = $this->input->get('search');
        $search = $this->db->escape_str($search);

        $config_data = array(
            'lg' => 'en',
            'table_name' => 'sma_sale_summary',
            'table_id' => 'id',
            'search' => $search,
            'sort_by' => $sort_by,
            'sort_order' => $sort_order,
            'limit' => $per_page,
            'offset_no' => $offset_no,
            'customer_id' => $this->input->post('customer_id'),
        );
        $select_data = $this->sales_model->getSaleSummary($config_data);

        $config_data = array(
            'lg' => 'en',
            'table_name' => 'sma_sale_summary',
            'table_id' => 'id',
            'search' => $search,
            'sort_by' => '',
            'sort_order' => '',
            'limit' => '',
            'offset_no' => '',
            'customer_id' => $this->input->post('customer_id'),
        );
        $temp_count = $this->sales_model->getSaleSummary($config_data);

        $total_record = count($temp_count);

        /* config pagination */
        $this->data['select_data'] = $select_data;
        $this->data['per_page'] = $per_page;
        $this->data['sort_by'] = $sort_by;
        $this->data['sort_order'] = $sort_order;
        $this->data['total_rows_sale'] = $total_record;
        $this->data['index_page'] = 'admin/sales/sale_summary';
        $this->data['show_data'] = $page;

        $config_base_url = site_url() . 'admin/sales/sale_summary?per_page='.$per_page; //your url where the content is displayed
        $config['base_url'] = $config_base_url; //your url where the content is displayed
        $config['query_string_segment'] = 'page';
        $config['per_page'] = $per_page; //$per_page;
        $config['num_links'] = 5;
        $config['use_page_numbers'] = true;
        $config['page_query_string'] = true;
        $config['full_tag_open'] = '<ul class="pagination pagination-new-bg">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['prev_link'] = '&lt; Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next &gt;';
        $config['next_tag_open'] = '<li class="next">';
        $config['next_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['total_rows'] = $total_record; //$this->data['total_rows_sale'];
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        /** end config pagination */
        /**Showing Label Data At Table Footer */
        $showing = $total_record != '' ? 1 : 0;
        $label_show_to= ($page == '' || $per_page < $total_record) ?  $per_page  :  $total_record;
        $label_show = "Showing " . $showing . " to ".$label_show_to." of : " . number_format($total_record) . " entries";

        $show_page = $page > 1 ? ($page * $per_page) : $total_record;
        $show_page_no = $show_page > $total_record ? $total_record : $page * $per_page;
        $label_show_page = "Showing " . (($page - 1) * ($per_page) + 1) . " to " . $show_page_no . " (filtered from " . number_format($total_record) . " total entries) ";
        $this->data['show'] = $search != '' || $page > 1 ? $label_show_page : $label_show;
        /** End Showing Label Data At Table Footer */
        $temp = $this->site->api_select_some_fields_with_where(
            "id,company
            ",
            "sma_companies",
            "id > 0 and vat_no != '' and getTranslate(add_ons,'vat_invoice_type','".f_separate."','".v_separate."') = 'do' order by company asc",
            "obj"
        );
        $this->data['company'] = $temp;

        $this->data['api_settings'] = $this->api_settings;
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('Sale_Summary')));
        $meta = array('page_title' => lang('Sale_Summary'), 'bc' => $bc);

        $this->page_construct('sales/sale_summary', $meta, $this->data);
    }
    
    public function edit_sale_summary($id = null)
    {
        $this->sma->checkPermissions();
        $this->form_validation->set_rules('customer', lang("customer"), 'required');
        $this->form_validation->set_rules('start_date', lang("Start_Date"), 'required');
        $this->form_validation->set_rules('end_date', lang("End_Date"), 'required');
        $this->form_validation->set_rules('add_ons_kh_currency_rate', lang("Khmer_Currency_Rate"), 'required');
        $company_branch = $this->sma->fld(trim($this->input->post('company_branch')));
        $start_date = $this->sma->fld(trim($this->input->post('start_date')));
        $end_date = $this->sma->fld(trim($this->input->post('end_date')));
        $temp_start_date = $this->sma->fld(trim($this->input->post('temp_start_date')));
        $temp_end_date = $this->sma->fld(trim($this->input->post('temp_end_date')));

        $select_data = array();
        if ($id > 0) {
            $config_data = array(
                'table_name' => 'sma_sale_summary',
                'select_table' => 'sma_sale_summary',
                'select_condition' => "id = ".$id,
            );
            $select_data = $this->site->api_select_data_v2($config_data);
        }
        
        if ($this->form_validation->run() == true) {
            $customer = $this->site->getCompanyByID($this->input->post('customer'));
            
            $config_data = array(
                'table_name' => 'sma_sales',
                'select_table' => 'sma_sales',
                'select_condition' => "customer_id = ".$customer->id." and sale_status = 'completed' and convert(date, Date) between STR_TO_DATE('".$start_date."', '%Y-%m-%d') and STR_TO_DATE('".$end_date."', '%Y-%m-%d')",
            );
            $temp = $this->site->api_select_data_v2($config_data);
            
            if ($select_data[0]['id'] == '') {
                $config_data = array(
                    'type' => 'sale_summary',
                    'date' => date('Y'),
                    'customer_id' => $customer->id,
                    'update' => 1,
                );
                $temp_2 = $this->site->api_calculate_reference_no($config_data);

                $temp = array();
                $temp['reference_no'] = $temp_2['reference_no'];
                $temp['customer_id'] = $customer->id;
                $temp['customer'] = $customer->name;
                $temp['start_date'] = $start_date;
                $temp['end_date'] = $end_date;
                $temp['date'] = date('Y-m-d H:i:s');
                $temp['payment_status'] = 'pending';

                $date = $this->input->post('date');
                if ($date == '') {
                    $date = date('Y-m-d H:i:s');
                } else {
                    $date = $this->sma->fld(trim($this->input->post('date').':'.date('s')));
                }
                $temp['date'] = $date;

                $this->db->insert('sma_sale_summary', $temp);
                $id = $this->db->insert_id();

                foreach ($_POST as $name => $value) {
                    if (is_int(strpos($name, "add_ons_"))) {
                        $value = $this->input->post($name);
                        $temp_name = str_replace('add_ons_', '', $name);
                        $config_data = array(
                            'table_name' => 'sma_sale_summary',
                            'id_name' => 'id',
                            'field_add_ons_name' => 'add_ons',
                            'selected_id' => $id,
                            'add_ons_title' => $temp_name,
                            'add_ons_value' => $value,
                        );
                        $this->site->api_update_add_ons_field($config_data);
                    }
                }
                $this->session->set_flashdata('message', lang("Successfully_add_sale_summary"));
            } else {
                $temp = array();
                $temp['customer_id'] = $customer->id;
                $temp['customer'] = $customer->name;
                $temp['start_date'] = $start_date;
                $temp['end_date'] = $end_date;

                $temp2 = substr($select_data[0]['date'], -2);
                $date = $this->sma->fld(trim($this->input->post('date').':'.$temp2));
                $temp['date'] = $date;

                $temp3 = explode(' ', $select_data[0]['date']);
                $temp4 = explode(' ', $temp['date']);
                if ($temp3[0] != $temp4[0]) {
                    $temp['disabled'] = 1;

                    $config_data2 = array(
                        'type' => 'sale_summary',
                        'date' => date('Y'),
                        'customer_id' => $customer->id,
                        'update' => 1,
                    );
                    $temp_2 = $this->site->api_calculate_reference_no($config_data2);

                    $config_data = array(
                        'duplicate_table' => 'sma_sale_summary',
                        'into_table' => 'sma_sale_summary',
                        'duplicate_id_col_name' => 'id',
                        'condition' => "id = ".$id,
                        'update_1' => "date = '".$date."'",
                        'update_2' => "reference_no = '".$temp_2['reference_no']."'",
                        'count_update' => 2,
                    );
                    $this->site->api_duplicate_record_update($config_data);
                }

                $this->db->update('sma_sale_summary', $temp, array('id' => $select_data[0]['id']));

                foreach ($_POST as $name => $value) {
                    if (is_int(strpos($name, "add_ons_"))) {
                        $value = $this->input->post($name);
                        $temp_name = str_replace('add_ons_', '', $name);
                        $config_data = array(
                            'table_name' => 'sma_sale_summary',
                            'id_name' => 'id',
                            'field_add_ons_name' => 'add_ons',
                            'selected_id' => $select_data[0]['id'],
                            'add_ons_title' => $temp_name,
                            'add_ons_value' => $value,
                        );
                        $this->site->api_update_add_ons_field($config_data);
                    }
                }
                
                $this->session->set_flashdata('message', lang("Successfully_updated_sale_summary"));
            }
            admin_redirect("sales/sale_summary");
        } else {
            $temp = $this->site->api_select_some_fields_with_where(
                "id,company,name,email
                ",
                "sma_companies",
                "id > 0 and vat_no != '' and getTranslate(add_ons,'vat_invoice_type','".f_separate."','".v_separate."') = 'do' order by company asc",
                "obj"
            );
            $this->data['company'] = $temp;

            $config_data = array(
                'table_name' => 'sma_currencies',
                'select_table' => 'sma_currencies',
                'select_condition' => "code = 'KHR' order by date desc",
            );
            $select_currency = $this->site->api_select_data_v2($config_data);
            
            $this->data['select_currency'] = $select_currency;
            $this->data['modal_js'] = $this->site->modal_js();
            
            $this->data['select_data'] = $select_data;
            $this->load->view($this->theme . 'sales/edit_sale_summary', $this->data);
        }
    }
    public function sale_summary_delete($id = null)
    {
        $this->sma->checkPermissions(null, true);
        if ($id > 0) {
            $config_data = array(
                'table_name' => 'sma_sale_summary',
                'select_table' => 'sma_sale_summary',
                'translate' => '',
                'select_condition' => "id = ".$id,
            );
            $select_data = $this->site->api_select_data_v2($config_data);
            $this->db->delete('sma_sale_summary', array('id' => $id));
        }
        $this->session->set_flashdata('message', lang('Sale_summary').' '.lang('deleted'));
        redirect($_SERVER["HTTP_REFERER"]);
    }
    public function sale_summary_actions()
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('api_action', lang("api_action"), 'required');
        if ($this->form_validation->run() == true) {
            if (!empty($_POST['check_value'])) {
                $temp_val = explode('-', $_POST['check_value']);
                if ($this->input->post('api_action') == 'delete') {
                    $this->sma->checkPermissions('delete');
                    $j = 0;
                    foreach ($temp_val as $id) {
                        if ($id != '') {
                            $this->db->delete('sma_sale_summary', array('id' => $id));
                            $j++;
                        }
                    }
                    $this->session->set_flashdata('message', $j.' '.lang("Sale_summary").' '.lang("deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                } elseif ($this->input->post('api_action') == 'change_payment_status_to_pending') {
                    foreach ($temp_val as $id) {
                        if ($id != '') {
                            $config_data = array(
                                'payment_status' => 'pending',
                            );
                            $this->db->update('sma_sale_summary', $config_data, "id = ".$id);

                            // $temp2 = $this->sales_model->api_calculate_sale_summary($id);
                            // $temp3 = explode('-',$temp2['sales']);
                            // for ($i=0;$i<count($temp3);$i++) {
                            //     if ($temp3[$i] != '')
                            //         $this->db->update('sma_sales', $config_data,"id = ".$temp3[$i]);
                            // }
                        }
                    }
                    $this->session->set_flashdata('message', lang('successfully_change_payment_status'));
                    redirect($_SERVER["HTTP_REFERER"]);
                } elseif ($this->input->post('api_action') == 'change_payment_status_to_paid') {
                    foreach ($temp_val as $id) {
                        if ($id != '') {
                            $config_data = array(
                                'payment_status' => 'paid',
                            );
                            $this->db->update('sma_sale_summary', $config_data, "id = ".$id);

                            // $temp2 = $this->sales_model->api_calculate_sale_summary($id);
                            // $temp3 = explode('-',$temp2['sales']);
                            // for ($i=0;$i<count($temp3);$i++) {
                            //     if ($temp3[$i] != '')
                            //         $this->db->update('sma_sales', $config_data,"id = ".$temp3[$i]);
                            // }
                        }
                    }
                    $this->session->set_flashdata('message', lang('successfully_change_payment_status'));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $this->session->set_flashdata('error', lang("no").' '.lang("Sale_summary").' '.lang("selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function export_no_vat_summary($vat = null)
    {
        $this->sma->checkPermissions();
        $rebate = $this->input->post('rebate');

        $vat_type = $this->input->post('vat_type');
        if ($vat_type == 'no') {
            $vat = '';
        }

        $this->form_validation->set_rules('customer', lang("customer"), 'required');
        $this->form_validation->set_rules('start_date', lang("Start_Date"), 'required');
        $this->form_validation->set_rules('end_date', lang("End_Date"), 'required');

        $company_branch = $this->input->post('add_ons_company_branch');
        $start_date = $this->sma->fld(trim($this->input->post('start_date')));
        $end_date = $this->sma->fld(trim($this->input->post('end_date')));

        if ($this->form_validation->run() == true) {
            //=Vat==============================================================================
            if ($vat != '') {
                $customer = $this->site->getCompanyByID($this->input->post('customer'));
                $condition = '';

                if ($vat_type != 'no') {
                    $condition .= " and reference_no like 'SL%'";
                } else {
                    $condition .= " and reference_no like 'DO%'";
                }
                        
                $config_data = array(
                    'table_name' => 'sma_sales',
                    'select_table' => 'sma_sales',
                    'select_condition' => "customer_id = ".$customer->id." and payment_status != 'paid' and grand_total > 0 ".$condition." and convert(date, Date) between STR_TO_DATE('".$start_date."', '%Y-%m-%d') and STR_TO_DATE('".$end_date."', '%Y-%m-%d') order by date asc",
                );
    
                $select_data = $this->site->api_select_data_v2($config_data);
    
                $b = 0;
                if (is_array($select_data)) {
                    if (count($select_data) > 0) {
                        $b = 1;
                    }
                }
                if ($b == 0) {
                    $this->session->set_flashdata('error', lang("No_record_found"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                
    
                if ($vat == 'vat') {
                    $temp_title = lang('Export_to_Vat_Summary');
                } else {
                    $temp_title = lang('Export_to_No_Vat_Summary');
                }
                
                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle($temp_title);
                
                
                $objDrawing = new PHPExcel_Worksheet_Drawing();
                $row = 1;
                $this->excel->getActiveSheet()->mergeCells('A'.$row.':E1');
                if ($vat != '') {
                    $objDrawing = new PHPExcel_Worksheet_Drawing();
                    $objDrawing->setName('logo');
                    $objDrawing->setDescription('logo');
                    $objDrawing->setPath('assets/uploads/logos/daishin-logo.jpg');
                    $objDrawing->setCoordinates('B'.$row);
                    $objDrawing->setOffsetX(125);
                    $objDrawing->setOffsetY(0);
                    $objDrawing->setWidth(50);
                    $objDrawing->setHeight(55);
                    $objDrawing->setWorksheet($this->excel->getActiveSheet());
    
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, 'VAT SUMMARY');
                } else {
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, 'SUMMARY');
                }
                $this->excel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setSize(30);
                
                
                $row = $row + 1;
                $this->excel->getActiveSheet()->getStyle('A'.$row.':E12')->getAlignment()->setWrapText(true);
                
                $config_data = array(
                    'table_name' => 'sma_companies',
                    'select_table' => 'sma_companies',
                    'select_condition' => "id = 480",
                );
                $biller = $this->site->api_select_data_v2($config_data);
    
                $config_data = array(
                    'table_name' => 'sma_companies',
                    'select_table' => 'sma_companies',
                    'select_condition' => "id = ".$select_data[0]['customer_id'],
                );
                $customer = $this->site->api_select_data_v2($config_data);
                            
                $this->excel->getActiveSheet()->getStyle('A'.$row.':E'.($row + 1))->getFont()->setBold(true);
    
                if ($vat == '') {
                    $this->excel->getActiveSheet()->mergeCells('A'.$row.':B'.($row+7));
                    $objDrawing = new PHPExcel_Worksheet_Drawing();
                    $objDrawing->setName('logo');
                    $objDrawing->setDescription('logo');
                    $objDrawing->setPath('assets/uploads/logos/daishin-logo.jpg');
                    $objDrawing->setCoordinates('A'.$row);
                    $objDrawing->setOffsetX(0);
                    $objDrawing->setOffsetY(0);
                    $objDrawing->setWidth(150);
                    $objDrawing->setHeight(165);
                    $objDrawing->setWorksheet($this->excel->getActiveSheet());
                }
                $row++;

                $this->excel->getActiveSheet()->getStyle('A'.$row.':E'.($row + 1))->getFont()->setBold(true);
                $this->excel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, $biller[0]['name_kh']);

                $this->excel->getActiveSheet()->mergeCells('D'.$row.':E'.$row);
                $this->excel->getActiveSheet()->SetCellValue('D'.$row, $customer[0]['name_kh']);
    
                $row++;
                $this->excel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, $biller[0]['name']);

                $this->excel->getActiveSheet()->mergeCells('D'.$row.':E'.$row);
                $this->excel->getActiveSheet()->SetCellValue('D'.$row, $customer[0]['name']);
                
                $row++;
                

    
                $row++;
                $row++;
                $temp_2 = $biller[0]['address'];
                if ($biller[0]['address'] != '') {
                    $temp_2 = $biller[0]['address'];
                }
                if ($biller[0]['city'] != '') {
                    $temp_3 = ' '.$biller[0]['city'];
                }
                if ($biller[0]['state'] != '') {
                    $temp_4 = ' '.$biller[0]['state'];
                }
                if ($biller[0]['country'] != '') {
                    $temp_5 = ' '.$biller[0]['country'];
                }
                $temp_2 = $temp_2.$temp_3.$temp_4.$temp_5.' '.$biller[0]['postal_code'];
                $this->excel->getActiveSheet()->mergeCells('A'.($row - 1).':B'.$row);
                $this->excel->getActiveSheet()->SetCellValue('A'.($row - 1), $temp_2);
                $this->excel->getActiveSheet()->getStyle('A'.($row - 1))->getAlignment()->setWrapText(true);

                $temp_2 = $customer[0]['address'];
                if ($customer[0]['address'] != '') {
                    $temp_2 = $customer[0]['address'];
                }
                if ($customer[0]['city'] != '') {
                    $temp_3 = ' '.$customer[0]['city'];
                }
                if ($customer[0]['state'] != '') {
                    $temp_4 = ' '.$customer[0]['state'];
                }
                if ($customer[0]['country'] != '') {
                    $temp_5 = ' '.$customer[0]['country'];
                }
                $temp_2 = $temp_2.$temp_3.$temp_4.$temp_5.' '.$customer[0]['postal_code'];
                $this->excel->getActiveSheet()->mergeCells('D'.($row - 1).':E'.$row);
                $this->excel->getActiveSheet()->SetCellValue('D'.($row - 1), $temp_2);
                $this->excel->getActiveSheet()->getStyle('D'.($row - 1))->getAlignment()->setWrapText(true);
                
                $row++;
                $row++;
                $temp_2 = $biller[0]['address_kh'];
                if ($biller[0]['address_kh'] != '') {
                    $temp_2 = $biller[0]['address_kh'];
                }
                if ($biller[0]['city_kh'] != '') {
                    $temp_3 = ' '.$biller[0]['city_kh'];
                }
                if ($biller[0]['state_kh'] != '') {
                    $temp_4 = ' '.$biller[0]['state_kh'];
                }
                if ($biller[0]['country_kh'] != '') {
                    $temp_5 = ' '.$biller[0]['country_kh'];
                }
                $temp_2 = $temp_2.$temp_3.$temp_4.$temp_5.' '.$biller[0]['postal_code'];
                $this->excel->getActiveSheet()->mergeCells('A'.($row - 1).':B'.$row);
                $this->excel->getActiveSheet()->SetCellValue('A'.($row - 1), $temp_2);
                $this->excel->getActiveSheet()->getStyle('A'.($row - 1))->getAlignment()->setWrapText(true);

                $temp_2 = $customer[0]['address_kh'];
                if ($customer[0]['address_kh'] != '') {
                    $temp_2 = $customer[0]['address_kh'];
                }
                if ($customer[0]['city_kh'] != '') {
                    $temp_3 = ' '.$customer[0]['city_kh'];
                }
                if ($customer[0]['state_kh'] != '') {
                    $temp_4 = ' '.$customer[0]['stat_kh'];
                }
                if ($customer[0]['country_kh'] != '') {
                    $temp_5 = ' '.$customer[0]['country_kh'];
                }
                $temp_2 = $temp_2.$temp_3.$temp_4.$temp_5.' '.$customer[0]['postal_code'];
                $this->excel->getActiveSheet()->mergeCells('D'.($row - 1).':E'.$row);
                $this->excel->getActiveSheet()->SetCellValue('D'.($row - 1), $temp_2);
                $this->excel->getActiveSheet()->getStyle('D'.($row - 1))->getAlignment()->setWrapText(true);


    
                $row++;
                if ($biller[0]['vat_no']) {
                    $this->excel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("vat_no_kh").'  (' . lang("vat_no") . ") : ".$biller[0]['vat_no']);
                }
                if ($customer[0]['vat_no'] && $vat_type != 'no') {
                    $this->excel->getActiveSheet()->mergeCells('D'.$row.':E'.$row);
                    $this->excel->getActiveSheet()->SetCellValue('D'.$row, lang("vat_no_kh").'  (' . lang("vat_no") . ") : ".$customer[0]['vat_no']);
                }
                            
                $row++;
                if ($vat != '') {
                    $this->excel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('tel_kh').' '.lang('tel').':'.$biller[0]['phone']);
                }
                $this->excel->getActiveSheet()->mergeCells('D'.$row.':E'.$row);
                $this->excel->getActiveSheet()->SetCellValue('D'.$row, lang('tel_kh').' '.lang('tel').':'.$customer[0]['phone']);
    
                $row++;
                if ($vat != '') {
                    $this->excel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('email_kh').' '.lang('email').':'.$biller[0]['email']);
                }
                $this->excel->getActiveSheet()->mergeCells('D'.$row.':E'.$row);
                $this->excel->getActiveSheet()->SetCellValue('D'.$row, lang('email_kh').' '.lang('email').':'.$customer[0]['email']);
    
                if ($vat == '') {
                    $row = $row + 2;
                    $this->excel->getActiveSheet()->getStyle('A'.$row.':B'.($row + 1))->getFont()->setBold(true);
                    $this->excel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, '     DTC Order System');
                }
                
                $row++;
                $this->excel->getActiveSheet()->getStyle('A'.$row.':A'.($row + 3))->getFont()->setBold(true);
                $this->excel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, '');
    
                $row++;
                $start_date = date_create($start_date);
                $end_date = date_create($end_date);
                $this->excel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('period_kh').' '.lang('Period').': '.date_format($start_date, 'F d, Y').' - '.date_format($end_date, 'F d, Y'));

                $this->excel->getActiveSheet()->mergeCells('D'.$row.':E'.$row);
                $this->excel->getActiveSheet()->SetCellValue('D'.$row, lang('date_kh').' '.lang('date').': '.date('F d, Y'));
                $this->excel->getActiveSheet()->getStyle('A'.$row.':E'.$row)->getFont()->setBold(true);
                                        
                $this->excel->getActiveSheet()->getStyle('A3:E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                
    
                $row++;
                
                $row++;
                
                
                $this->excel->getActiveSheet()->getStyle('A'.$row.':D'.($row + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('E'.$row.':E'.($row + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $this->excel->getActiveSheet()->getStyle('A'.$row.':E'.($row + 1))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('c9c9c9');
                
                if ($vat != '') {
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('no_kh'));
                    $this->excel->getActiveSheet()->SetCellValue('B'.$row, lang('date_kh'));
                    $this->excel->getActiveSheet()->mergeCells('C'.$row.':D'.$row);
                    $this->excel->getActiveSheet()->SetCellValue('C'.$row, lang('reference_no_kh'));
                    $temp = str_replace(' Sub Total', '', lang('subtotal_kh'));
                    $this->excel->getActiveSheet()->SetCellValue('E'.$row, $temp);
                }
    
                $row++;
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('No.'));
                $this->excel->getActiveSheet()->SetCellValue('B'.$row, lang('Date'));
                $this->excel->getActiveSheet()->mergeCells('C'.$row.':D'.$row);
                $this->excel->getActiveSheet()->SetCellValue('C'.$row, lang('reference_no'));
                $this->excel->getActiveSheet()->SetCellValue('E'.$row, lang('Subtotal'));
                
                
    
                $row++;
    
                $temp_row = $row;
                $j = 1;
                $grand_total = 0;
                if (is_array($select_data) && count($select_data) > 0) {
                    for ($i=0;$i<count($select_data);$i++) {
                        $date = date_create($select_data[$i]['date']);
                        $this->excel->getActiveSheet()->SetCellValue('A'.$row, $j);
                        $this->excel->getActiveSheet()->SetCellValue('B'.$row, date_format($date, 'F d, Y'));
                        $this->excel->getActiveSheet()->SetCellValue('C'.$row, $select_data[$i]['reference_no']);
                        $this->excel->getActiveSheet()->mergeCells('C'.$row.':D'.$row);
                        $this->excel->getActiveSheet()->SetCellValue('E'.$row, $select_data[$i]['grand_total']);
                        $grand_total += $select_data[$i]['grand_total'];
                        $row++;
                        $j++;
                    }
                } else {
                    $this->excel->getActiveSheet()->mergeCells('A'.$row.':D'.$row);
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, 'No record found');
                }
    
                $this->excel->getActiveSheet()->getStyle('E'.$temp_row.':E'.$row)->getNumberFormat()->setFormatCode('#,##0.00');
    
                $this->excel->getActiveSheet()->getStyle('A'.$row.':E'.$row)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    
                $this->excel->getActiveSheet()->getStyle('A'.$row.':E'.($row + 4))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $this->excel->getActiveSheet()->mergeCells('A'.$row.':D'.$row);
                
    
                $temp_currency = $this->input->post('khmer_currency');
    
                if ($temp_currency != '') {
                    $temp_3 = explode('-', $temp_currency);
                    $temp_3 = $temp_3[2];
                    $temp_2 = $grand_total * $temp_3;
    
                    $this->excel->getActiveSheet()->mergeCells('A'.$row.':D'.$row);
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('total_amount_kh').' '.lang('total_amount').' (Khmer Riel)');
                    $this->excel->getActiveSheet()->getStyle('A'.$row.':E'.$row)->getFont()->setBold(true);
                    $this->excel->getActiveSheet()->SetCellValue('E'.$row, 'R '.number_format($temp_2, 0));
                }
    
                $this->excel->getActiveSheet()->getStyle('B15:D'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    
                $temp_exclude_vat = ($grand_total * 10) / 100;
                $temp_exclude_vat = $grand_total - $temp_exclude_vat;
    
                if ($rebate == 'yes') {
                    $temp5 = $this->sales_model->api_get_rebate_options($temp_exclude_vat);
                }
    
                if ($temp5['value'] > 0) {
                    $temp6 = ($temp5['value'] * $temp_exclude_vat) / 100;
                    $temp7 = $grand_total - $temp6;
                    $temp7_2 = $temp_exclude_vat - $temp6;
    
                    if ($vat != '') {
                        $this->excel->getActiveSheet()->mergeCells('A'.$row.':D'.$row);
                        $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('total_include_vat_kh').' '.lang('Total_Include_VAT').' (USD)');
                        $this->excel->getActiveSheet()->SetCellValue('E'.$row, $grand_total);
                        $this->excel->getActiveSheet()->getStyle('E'.$row)->getNumberFormat()->setFormatCode('#,##0.00');
    
                        $row++;
                        $this->excel->getActiveSheet()->mergeCells('A'.$row.':D'.$row);
                        $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('total_exclude_vat_kh').' '.lang('Total_Exclude_VAT').' (USD)');
                        $this->excel->getActiveSheet()->SetCellValue('E'.$row, $temp_exclude_vat);
                        $this->excel->getActiveSheet()->getStyle('E'.$row)->getNumberFormat()->setFormatCode('#,##0.00');
                    } else {
                        $this->excel->getActiveSheet()->mergeCells('A'.$row.':D'.$row);
                        $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('total_amount_kh').' '.lang('Total_Amount').' (USD)');
                        $this->excel->getActiveSheet()->SetCellValue('E'.$row, $temp_exclude_vat);
                        $this->excel->getActiveSheet()->getStyle('E'.$row)->getNumberFormat()->setFormatCode('#,##0.00');
                    }
    
                    $row++;
                    $this->excel->getActiveSheet()->mergeCells('A'.$row.':D'.$row);
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('rebate_kh').' '.lang('Rebate').' '.$temp5['value'].'% (USD)');
                    $this->excel->getActiveSheet()->getStyle('A'.$row.':E'.$row)->getFont()->setBold(true);
                    $this->excel->getActiveSheet()->SetCellValue('E'.$row, $temp6);
                    $this->excel->getActiveSheet()->getStyle('E'.$row)->getNumberFormat()->setFormatCode('#,##0.00');
    
                    $row++;
                    $this->excel->getActiveSheet()->mergeCells('A'.$row.':D'.$row);
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('grand_total_kh').' '.lang('grand_total').' (USD)');
                    $this->excel->getActiveSheet()->getStyle('A'.$row.':E'.$row)->getFont()->setBold(true);
                    $this->excel->getActiveSheet()->SetCellValue('E'.$row, $temp7);
                    $this->excel->getActiveSheet()->getStyle('E'.$row)->getNumberFormat()->setFormatCode('#,##0.00');
                } else {
                    $this->excel->getActiveSheet()->mergeCells('A'.$row.':D'.$row);
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('total_amount_kh').' '.lang('Total_Amount').' (USD)');
                    $this->excel->getActiveSheet()->getStyle('A'.$row.':E'.$row)->getFont()->setBold(true);
                    $this->excel->getActiveSheet()->SetCellValue('E'.$row, $grand_total);
                    $this->excel->getActiveSheet()->getStyle('E'.$row)->getNumberFormat()->setFormatCode('#,##0.00');
                }
    
    
                if ($temp_currency != '') {
                    $row++;
                    $this->excel->getActiveSheet()->mergeCells('A'.$row.':E'.$row);
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, '1$ = '.number_format($temp_3, 0).' KHR');
                }
                
                $row = $row + 2;
                if ($temp_currency != '') {
                    $row++;
                    $this->excel->getActiveSheet()->getStyle('A'.$row.':E'.($row + 2))->getAlignment()->setWrapText(true);
                }
    
                $row++;
                $temp_row = $row;
                if ($biller[0]['company_kh'] != '-') {
                    $temp = $biller[0]['company_kh'];
                } else {
                    $temp = [0]['name_kh'];
                }
                $this->excel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('seller_kh').': '.$temp);
    
                if ($customer[0]['company_kh'] != '-') {
                    $temp = $customer[0]['company_kh'];
                } else {
                    $temp = [0]['name_kh'];
                }
                $this->excel->getActiveSheet()->mergeCells('D'.$row.':E'.$row);
                $this->excel->getActiveSheet()->SetCellValue('D'.$row, lang('customer_kh').': '.$temp);
                
                $row++;
                if ($biller[0]['company'] != '-') {
                    $temp = $biller[0]['company'];
                } else {
                    $temp = [0]['name'];
                }
                $temp = lang('seller').': '.$temp;
                $this->excel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, $temp);
    
                if ($customer[0]['company'] != '-') {
                    $temp = $customer[0]['company'];
                } else {
                    $temp = [0]['name'];
                }
                $temp = lang('customer').': '.$temp;
                $this->excel->getActiveSheet()->mergeCells('D'.$row.':E'.$row);
    
                $this->excel->getActiveSheet()->getStyle('A'.$temp_row.':E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    
                $this->excel->getActiveSheet()->SetCellValue('D'.$row, $temp);
                
                $row = $row + 5;
                
                $row++;
                $this->excel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
                $this->excel->getActiveSheet()->getStyle('A'.$row.':B'.$row)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    
                $this->excel->getActiveSheet()->mergeCells('D'.$row.':E'.$row);
                $this->excel->getActiveSheet()->getStyle('D'.$row.':E'.$row)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                
                $row++;
                $this->excel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('stamp_sign_kh').' Stamp & Signature');
    
                $this->excel->getActiveSheet()->mergeCells('D'.$row.':E'.$row);
                $this->excel->getActiveSheet()->SetCellValue('D'.$row, lang('stamp_sign_kh').' Stamp & Signature');
                $row = $row + 5;
                $row++;
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, '');
                            
                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(22);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(22);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $this->excel->getActiveSheet()->getStyle('B15:B'.$row)->getAlignment()->setWrapText(true);
    
                if ($vat == 'vat') {
                    $filename = 'Sale_Summary_Vat_' . date('Y_m_d_H_i_s');
                } else {
                    $filename = 'Sale_Summary_No_Vat_' . date('Y_m_d_H_i_s');
                }
                
                $this->load->helper('excel');
                create_excel($this->excel, $filename);
                admin_redirect("sales/sale_summary");
            //=Vat==============================================================================
            } else {
                //=No-Vat====================================================================================

                $customer = $this->site->getCompanyByID($this->input->post('customer'));
                $condition = '';

                if ($vat_type != 'no') {
                    $condition .= " and reference_no like 'SL%'";
                } else {
                    $condition .= " and reference_no like 'DO%'";
                }
        
                $config_data = array(
    'table_name' => 'sma_sales',
    'select_table' => 'sma_sales',
    'select_condition' => "customer_id = ".$customer->id." and payment_status != 'paid' ".$condition." and grand_total > 0 and convert(date, Date) between STR_TO_DATE('".$start_date."', '%Y-%m-%d') and STR_TO_DATE('".$end_date."', '%Y-%m-%d') order by date asc",
);
                $select_data = $this->site->api_select_data_v2($config_data);

                $b = 0;
                if (is_array($select_data)) {
                    if (count($select_data) > 0) {
                        $b = 1;
                    }
                }
                if ($b == 0) {
                    $this->session->set_flashdata('error', lang("No_record_found"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }


                $temp_title = lang('Export_to_No_Vat_Summary');

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle($temp_title);

                $objDrawing = new PHPExcel_Worksheet_Drawing();
                $row = 1;
                $row++;

                $this->excel->getActiveSheet()->mergeCells('A'.$row.':E'.$row);
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, 'ダイシン');
                $this->excel->getActiveSheet()->mergeCells('A'.$row.':E'.$row);
                $this->excel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setSize(12);

                $row++;
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('Summary'));
                $this->excel->getActiveSheet()->mergeCells('A'.$row.':E'.$row);
                $this->excel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setSize(22);


                $row = $row + 1;
                $this->excel->getActiveSheet()->getStyle('A'.$row.':E12')->getAlignment()->setWrapText(true);

                $config_data = array(
    'table_name' => 'sma_companies',
    'select_table' => 'sma_companies',
    'select_condition' => "id = 480",
);
                $biller = $this->site->api_select_data_v2($config_data);

                $config_data = array(
    'table_name' => 'sma_companies',
    'select_table' => 'sma_companies',
    'select_condition' => "id = ".$select_data[0]['customer_id'],
);
                $customer = $this->site->api_select_data_v2($config_data);

          

                $row++;
                $row++;

                // $this->excel->getActiveSheet()->mergeCells('A'.$row.':A'.($row + 2));
                // $objDrawing = new PHPExcel_Worksheet_Drawing();
                // $objDrawing->setName('logo');
                // $objDrawing->setDescription('logo');
                // $objDrawing->setPath('assets/uploads/logos/DTC.png');
                // $objDrawing->setCoordinates('A'.$row);
                // $objDrawing->setOffsetX(0);
                // $objDrawing->setOffsetY(0);
                // $objDrawing->setWidth(48);
                // $objDrawing->setHeight(48);
                // $objDrawing->setWorksheet($this->excel->getActiveSheet());
                // $this->excel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                // $this->excel->getActiveSheet()->mergeCells('B'.$row.':C'.$row);
                // $this->excel->getActiveSheet()->SetCellValue('B'.$row,' '.$biller[0]['name']);
                // $this->excel->getActiveSheet()->getStyle('B'.$row)->getFont()->setBold(true);


                // $row++;
                // $this->excel->getActiveSheet()->mergeCells('B'.$row.':C'.($row + 1));
                // $temp_2 = $biller[0]['address'];
                // if ($biller[0]['address'] != '') $temp_2 = $biller[0]['address'];
                // if ($biller[0]['city'] != '') $temp_3 = ' '.$biller[0]['city'];
                // if ($biller[0]['state'] != '') $temp_4 = ' '.$biller[0]['state'];
                // if ($biller[0]['country'] != '') $temp_5 = ' '.$biller[0]['country'];
                // $temp_2 = $temp_2.$temp_3.$temp_4.$temp_5.' '.$biller[0]['postal_code'];
                // $this->excel->getActiveSheet()->SetCellValue('B'.$row,' '.$temp_2);

                // $row = $row + 3;

                // $this->excel->getActiveSheet()->SetCellValue('A'.$row,lang('tel').':');
                // $this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setBold(true);
                // $this->excel->getActiveSheet()->SetCellValue('B'.$row,' '.$biller[0]['phone']);

                $customer[0]['name'] = str_replace('<br>', ' ', $customer[0]['name']);
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('Bill_to').':');
                $this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setBold(true);
                $this->excel->getActiveSheet()->mergeCells('B'.$row.':C'.($row + 3));
                $this->excel->getActiveSheet()->SetCellValue('B'.$row, $customer[0]['name']);
                $this->excel->getActiveSheet()->getStyle('B'.$row)->getFont()->setSize(12);
                $this->excel->getActiveSheet()->getStyle('B'.$row)->getAlignment()->setWrapText(true);
                $this->excel->getActiveSheet()->getStyle('B'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

                $row++;
                $row++;

                $start_date = date_create($start_date);
                $end_date = date_create($end_date);
                $this->excel->getActiveSheet()->SetCellValue('D'.$row, lang('date').': ');
                $this->excel->getActiveSheet()->getStyle('D'.$row)->getFont()->setBold(true);
                $this->excel->getActiveSheet()->getStyle('D'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $this->excel->getActiveSheet()->SetCellValue('E'.$row, date('F d, Y'));

                $row++;

                // $this->excel->getActiveSheet()->SetCellValue('A'.$row,lang('email').':');
                // $this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setBold(true);
                // $this->excel->getActiveSheet()->SetCellValue('B'.$row,' '.$biller[0]['email']);

                $this->excel->getActiveSheet()->SetCellValue('D'.$row, lang('Period').':');
                $this->excel->getActiveSheet()->getStyle('D'.$row)->getFont()->setBold(true);
                $this->excel->getActiveSheet()->getStyle('D'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $this->excel->getActiveSheet()->SetCellValue('E'.$row, date_format($start_date, 'd/m/Y').' - '.date_format($end_date, 'd/m/Y'));


                $row++;

                $row++;
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('No.'));
                $this->excel->getActiveSheet()->SetCellValue('B'.$row, lang('Date'));
                $this->excel->getActiveSheet()->SetCellValue('C'.$row, lang('Reference'));
                $this->excel->getActiveSheet()->SetCellValue('D'.$row, lang('Amount'));
                $this->excel->getActiveSheet()->SetCellValue('E'.$row, lang('Remark'));

                $this->excel->getActiveSheet()->getStyle('A'.$row.':E'.$row)->getFont()->setBold(true);
                $this->excel->getActiveSheet()->getStyle('A'.$row.':E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('A'.$row.':E'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('595959');



                $this->excel->getActiveSheet()->getStyle('A'.$row.':E'.$row)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $this->excel->getActiveSheet()->getStyle('E'.$row)->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $this->excel->getActiveSheet()->getStyle('A'.$row)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $this->excel->getActiveSheet()->getStyle('A'.$row.':E'.$row)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

                $styleArray = array(
    'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'ffffff'),
        'size'  => 12,
    ));
                $this->excel->getActiveSheet()->getStyle('A'.$row.':E'.$row)->applyFromArray($styleArray);

                $row++;

                $temp_row = $row;
                $j = 1;
                $grand_total = 0;
                if (is_array($select_data) && count($select_data) > 0) {
                    for ($i=0;$i<count($select_data);$i++) {
                        $date = date_create($select_data[$i]['date']);
                        $this->excel->getActiveSheet()->SetCellValue('A'.$row, $j);
                        $this->excel->getActiveSheet()->SetCellValue('B'.$row, date_format($date, 'F d, Y'));
                        $this->excel->getActiveSheet()->SetCellValue('C'.$row, $select_data[$i]['reference_no']);
                        $this->excel->getActiveSheet()->SetCellValue('D'.$row, $select_data[$i]['grand_total']);
                        $this->excel->getActiveSheet()->getStyle('D'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                        $this->excel->getActiveSheet()->SetCellValue('E'.$row, '');
                        $grand_total += $select_data[$i]['grand_total'];
                        $row++;
                        $j++;
                    }
                } else {
                    $this->excel->getActiveSheet()->mergeCells('A'.$row.':E'.$row);
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, 'No record found');
                }


                $this->excel->getActiveSheet()->getStyle('A'.$temp_row.':C'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('D'.$temp_row.':D'.$row)->getNumberFormat()->setFormatCode('#,##0.00');

                $styleArray = array(
    'borders' => array(
      'allborders' => array(
        'style' => PHPExcel_Style_Border::BORDER_THIN
      )
    )
  );
                $this->excel->getActiveSheet()->getStyle('A'.$temp_row.':E'.($row - 1))->applyFromArray($styleArray);

                $this->excel->getActiveSheet()->getStyle('A'.$row.':E'.($row + 4))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);


                $temp_currency = $this->input->post('khmer_currency');

                if ($temp_currency != '') {
                    $temp_3 = explode('-', $temp_currency);
                    $temp_3 = $temp_3[2];
                    $temp_2 = $grand_total * $temp_3;

                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('total_amount').' (Khmer Riel)');
                    $this->excel->getActiveSheet()->getStyle('A'.$row.':E'.$row)->getFont()->setBold(true);
                    $this->excel->getActiveSheet()->SetCellValue('E'.$row, 'R '.number_format($temp_2, 0));
                }

                $temp_exclude_vat = $grand_total;

                if ($rebate == 'yes') {
                    $temp5 = $this->sales_model->api_get_rebate_options($grand_total);
                }

                $temp_row = $row;
                if ($temp5['value'] > 0) {
                    $temp6 = ($temp5['value'] * $temp_exclude_vat) / 100;
                    $temp7 = $grand_total - $temp6;
                    $temp7_2 = $temp_exclude_vat - $temp6;

                    $this->excel->getActiveSheet()->mergeCells('B'.$row.':C'.$row);
                    $this->excel->getActiveSheet()->SetCellValue('B'.$row, lang('Total_Amount').' (USD)');
                    $this->excel->getActiveSheet()->SetCellValue('D'.$row, $temp_exclude_vat);
                    $this->excel->getActiveSheet()->getStyle('D'.$row)->getNumberFormat()->setFormatCode('#,##0.00');

                    $styleArray = array(
        'borders' => array(
          'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
          )
        )
      );
                    $this->excel->getActiveSheet()->getStyle('D'.$row.':E'.$row)->applyFromArray($styleArray);
                    $this->excel->getActiveSheet()->getStyle('D'.$row.':E'.$row)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_NONE);

                    $row++;
                    $this->excel->getActiveSheet()->mergeCells('B'.$row.':C'.$row);
                    $this->excel->getActiveSheet()->SetCellValue('B'.$row, lang('Rebate').' '.$temp5['value'].'% (USD)');
                    $this->excel->getActiveSheet()->SetCellValue('D'.$row, $temp6);
                    $this->excel->getActiveSheet()->getStyle('D'.$row)->getNumberFormat()->setFormatCode('#,##0.00');
                    $this->excel->getActiveSheet()->getStyle('D'.$row)->getNumberFormat()->setFormatCode('#,##0.00');

                    $styleArray = array(
        'borders' => array(
          'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
          )
        )
      );
                    $this->excel->getActiveSheet()->getStyle('D'.$row.':E'.$row)->applyFromArray($styleArray);
                    $this->excel->getActiveSheet()->getStyle('D'.$row.':E'.$row)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_NONE);
                    $this->excel->getActiveSheet()->getStyle('D'.$row.':E'.$row)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_NONE);

                    $row++;
                    $this->excel->getActiveSheet()->mergeCells('B'.$row.':C'.$row);
                    $this->excel->getActiveSheet()->SetCellValue('B'.$row, lang('grand_total').' (USD)');
                    $this->excel->getActiveSheet()->SetCellValue('D'.$row, $temp7_2);
                    $this->excel->getActiveSheet()->getStyle('D'.$row)->getNumberFormat()->setFormatCode('#,##0.00');
                    $styleArray = array(
        'borders' => array(
          'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
          )
        )
      );
                    $this->excel->getActiveSheet()->getStyle('D'.$row.':E'.$row)->applyFromArray($styleArray);
                    $this->excel->getActiveSheet()->getStyle('D'.$row.':E'.$row)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_NONE);
                } else {
                    $this->excel->getActiveSheet()->mergeCells('B'.$row.':C'.$row);
                    $this->excel->getActiveSheet()->SetCellValue('B'.$row, lang('grand_total').' (USD)');
                    $this->excel->getActiveSheet()->SetCellValue('D'.$row, $grand_total);
                    $this->excel->getActiveSheet()->getStyle('D'.$row)->getNumberFormat()->setFormatCode('#,##0.00');

                    $styleArray = array(
        'borders' => array(
          'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
          )
        )
      );
                    $this->excel->getActiveSheet()->getStyle('D'.$temp_row.':E'.$row)->applyFromArray($styleArray);
                }

                $styleArray = array(
    'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'ffffff'),
        'size'  => 12,
    ));
                $this->excel->getActiveSheet()->getStyle('B'.$temp_row.':C'.$row)->applyFromArray($styleArray);
                $this->excel->getActiveSheet()->getStyle('B'.$temp_row.':C'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('595959');
                $this->excel->getActiveSheet()->getStyle('B'.$temp_row.':D'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);



                $styleArray = array(
    'borders' => array(
      'allborders' => array(
        'style' => PHPExcel_Style_Border::BORDER_NONE
      )
    )
  );
                $this->excel->getActiveSheet()->getStyle('A'.$temp_row.':B'.$row)->applyFromArray($styleArray);



                $row++;
                $row++;

                $row++;
                $this->excel->getActiveSheet()->mergeCells('A'.$row.':C'.$row);
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('Bank_Information'));
                $styleArray = array(
    'font'  => array(
        'bold'  => true,
        'size'  => 12,
    ));
                $this->excel->getActiveSheet()->getStyle('A'.$row.':C'.$row)->applyFromArray($styleArray);
                $this->excel->getActiveSheet()->getStyle('A'.$row.':C'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);


                $row++;
                $this->excel->getActiveSheet()->mergeCells('A'.$row.':C'.$row);
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, '   '.lang('Bank').'             : ABA BANK');

                $row++;
                $this->excel->getActiveSheet()->mergeCells('A'.$row.':C'.$row);
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, '   '.lang('A/C No.').'        : 001 574 727');
                $this->excel->getActiveSheet()->getStyle('A'.$row.':C'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);


                $row++;
                $this->excel->getActiveSheet()->mergeCells('A'.$row.':C'.$row);
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, '   '.lang('A/C Name').'    : KOIKE SATOSHI');


                $this->excel->getActiveSheet()->getStyle('A'.($row - 3).':C'.($row - 3))->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $this->excel->getActiveSheet()->getStyle('C'.($row - 3).':C'.$row)->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $this->excel->getActiveSheet()->getStyle('A'.($row - 3).':A'.$row)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $this->excel->getActiveSheet()->getStyle('A'.$row.':C'.$row)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);



                if ($temp_currency != '') {
                    $row++;
                    $this->excel->getActiveSheet()->mergeCells('A'.$row.':E'.$row);
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, '1$ = '.number_format($temp_3, 0).' KHR');
                }

                $row = $row + 2;
                if ($temp_currency != '') {
                    $row++;
                    $this->excel->getActiveSheet()->getStyle('A'.$row.':E'.($row + 2))->getAlignment()->setWrapText(true);
                }

                $row++;
                $temp_row = $row;

                $this->excel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('customer').': '.$customer[0]['company']);

                $this->excel->getActiveSheet()->mergeCells('D'.$row.':E'.$row);
                $this->excel->getActiveSheet()->SetCellValue('D'.$row, lang('Seller'));

                $this->excel->getActiveSheet()->getStyle('A'.$temp_row.':E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

                $row = $row + 5;

                $row++;
                $this->excel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
                $this->excel->getActiveSheet()->getStyle('A'.$row.':B'.$row)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

                $this->excel->getActiveSheet()->mergeCells('D'.$row.':E'.$row);
                $this->excel->getActiveSheet()->getStyle('D'.$row.':E'.$row)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);


                $row = $row + 5;
                $row++;
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, '');
            
                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(7);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(18);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(22);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(22);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $this->excel->getActiveSheet()->getStyle('B15:B'.$row)->getAlignment()->setWrapText(true);

                if ($vat == 'vat') {
                    $filename = 'Sale_Summary_Vat_' . date('Y_m_d_H_i_s');
                } else {
                    $filename = 'Sale_Summary_No_Vat_' . date('Y_m_d_H_i_s');
                }

                $this->load->helper('excel');
                create_excel($this->excel, $filename);
                admin_redirect("sales/sale_summary");

                //=No-Vat====================================================================================
            }
        } else {
            if ($vat == 'vat') {
                $condition = "and vat_no != ''";
                $this->data['title'] = lang('Export_to_Vat_Summary');
                $this->data['vat'] = 'vat';
            } else {
                $condition = "and (vat_no is null or vat_no = '')";
                $this->data['title'] = lang('Export_to_No_Vat_Summary');
                $this->data['vat'] = '';
            }
            
            $temp = $this->site->api_select_some_fields_with_where(
                "id,company,name
                ",
                "sma_companies",
                "id > 0 ".$condition." order by company asc",
                "obj"
            );
            $this->data['company'] = $temp;
            $config_data = array(
                'table_name' => 'sma_currencies',
                'select_table' => 'sma_currencies',
                'select_condition' => "code = 'KHR'",
            );
            $select_currency = $this->site->api_select_data_v2($config_data);
            
            $this->data['select_currency'] = $select_currency;
            $this->data['modal_js'] = $this->site->modal_js();
            
            $this->load->view($this->theme . 'sales/export_no_vat_summary', $this->data);
        }
    }

    public function export_sale_summary($id)
    {
        $this->sma->checkPermissions();
        if ($id > 0) {
            $config_data = array(
                'table_name' => 'sma_sale_summary',
                'select_table' => 'sma_sale_summary',
                'select_condition' => "id = ".$id,
            );
            $select_data = $this->site->api_select_data_v2($config_data);
        }

        if (count($select_data) > 0) {
            $customer = $this->site->getCompanyByID($select_data[0]['customer_id']);

            $condition = '';

            $config_data = array(
                'table_name' => 'sma_sales',
                'select_table' => 'sma_sales',
                'select_condition' => "customer_id = ".$customer->id." and reference_no like '%DO%' and grand_total > 0 ".$condition." and payment_status != 'paid' and convert(date, Date) between STR_TO_DATE('".$select_data[0]['start_date']."', '%Y-%m-%d') and STR_TO_DATE('".$select_data[0]['end_date']."', '%Y-%m-%d') order by date asc",
            );
            $temp = $this->site->api_select_data_v2($config_data);

            $b = 0;
            if (count($temp) > 0) {
                $b = 1;
            }
            if ($b == 0) {
                $this->session->set_flashdata('error', lang("No_record_found"));
                redirect($_SERVER["HTTP_REFERER"]);
            }

            $this->load->library('excel');
            $this->excel->setActiveSheetIndex(0);
            $this->excel->getActiveSheet()->setTitle(lang('sale_summary'));
            
            //-Header-=======================================================================
            $objDrawing = new PHPExcel_Worksheet_Drawing();
            $row = 1;

            $this->excel->getActiveSheet()->mergeCells('A'.$row.':E4');

            $objDrawing = new PHPExcel_Worksheet_Drawing();
            $objDrawing->setName('logo');
            $objDrawing->setDescription('logo');
            $objDrawing->setPath('assets/uploads/logos/logo-tax.jpg');
            $objDrawing->setCoordinates('C'.$row);
            $objDrawing->setOffsetX(-80);
            $objDrawing->setOffsetY(0);
            $objDrawing->setWidth(300);
            $objDrawing->setHeight(80);
            $objDrawing->setWorksheet($this->excel->getActiveSheet());

            $this->excel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setSize(30);
            
            
            $row = $row + 5;
            $this->excel->getActiveSheet()->getStyle('A'.$row.':E12')->getAlignment()->setWrapText(true);
            
            $config_data = array(
                'table_name' => 'sma_companies',
                'select_table' => 'sma_companies',
                'select_condition' => "id = 480",
            );
            $biller = $this->site->api_select_data_v2($config_data);

            $config_data = array(
                'table_name' => 'sma_companies',
                'select_table' => 'sma_companies',
                'select_condition' => "id = ".$select_data[0]['customer_id'],
            );
            $customer = $this->site->api_select_data_v2($config_data);
                        

            $this->excel->getActiveSheet()->getStyle('A'.$row.':E'.($row + 1))->getFont()->setBold(true);

            $this->excel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
            $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('seller_kh').' '.lang('seller'));

            $this->excel->getActiveSheet()->mergeCells('D'.$row.':E'.$row);
            $this->excel->getActiveSheet()->SetCellValue('D'.$row, lang('customer_kh').' '.lang('customer'));

            $row++;

            $this->excel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
            $this->excel->getActiveSheet()->SetCellValue('A'.$row, $biller[0]['name_kh']);

            $this->excel->getActiveSheet()->mergeCells('D'.$row.':E'.$row);
            $this->excel->getActiveSheet()->SetCellValue('D'.$row, $customer[0]['name_kh']);

            $row++;

            $this->excel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
            $this->excel->getActiveSheet()->SetCellValue('A'.$row, $biller[0]['name']);

            $this->excel->getActiveSheet()->mergeCells('D'.$row.':E'.$row);
            $this->excel->getActiveSheet()->SetCellValue('D'.$row, $customer[0]['name']);

            $row++;
            $row++;

            $temp_2 = $biller[0]['address'];
            if ($biller[0]['address'] != '') {
                $temp_2 = $biller[0]['address'];
            }
            if ($biller[0]['city'] != '') {
                $temp_3 = ' '.$biller[0]['city'];
            }
            if ($biller[0]['state'] != '') {
                $temp_4 = ' '.$biller[0]['state'];
            }
            if ($biller[0]['country'] != '') {
                $temp_5 = ' '.$biller[0]['country'];
            }
            $temp_2 = $temp_2.$temp_3.$temp_4.$temp_5.' '.$biller[0]['postal_code'];
            $this->excel->getActiveSheet()->mergeCells('A'.($row - 1).':B'.$row);
            $this->excel->getActiveSheet()->SetCellValue('A'.($row - 1), $temp_2);
            $this->excel->getActiveSheet()->getStyle('A'.($row - 1))->getAlignment()->setWrapText(true);

            $temp_2 = $customer[0]['address'];
            if ($customer[0]['address'] != '') {
                $temp_2 = $customer[0]['address'];
            }
            if ($customer[0]['city'] != '') {
                $temp_3 = ' '.$customer[0]['city'];
            }
            if ($customer[0]['state'] != '') {
                $temp_4 = ' '.$customer[0]['state'];
            }
            if ($customer[0]['country'] != '') {
                $temp_5 = ' '.$customer[0]['country'];
            }
            $temp_2 = $temp_2.$temp_3.$temp_4.$temp_5.' '.$customer[0]['postal_code'];
            $this->excel->getActiveSheet()->mergeCells('D'.($row - 1).':E'.$row);
            $this->excel->getActiveSheet()->SetCellValue('D'.($row - 1), $temp_2);
            $this->excel->getActiveSheet()->getStyle('D'.($row - 1))->getAlignment()->setWrapText(true);
            $this->excel->getActiveSheet()->getStyle('A'.($row - 1).':E'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

            $row++;
            $row++;
            $temp_2 = $biller[0]['address_kh'];
            if ($biller[0]['address_kh'] != '') {
                $temp_2 = $biller[0]['address_kh'];
            }
            if ($biller[0]['city_kh'] != '') {
                $temp_3 = ' '.$biller[0]['city_kh'];
            }
            if ($biller[0]['state_kh'] != '') {
                $temp_4 = ' '.$biller[0]['state_kh'];
            }
            if ($biller[0]['country_kh'] != '') {
                $temp_5 = ' '.$biller[0]['country_kh'];
            }
            $temp_2 = $temp_2.$temp_3.$temp_4.$temp_5.' '.$biller[0]['postal_code'];
            $this->excel->getActiveSheet()->mergeCells('A'.($row - 1).':B'.$row);
            $this->excel->getActiveSheet()->SetCellValue('A'.($row - 1), $temp_2);
            $this->excel->getActiveSheet()->getStyle('A'.($row - 1))->getAlignment()->setWrapText(true);

            $temp_2 = $customer[0]['address_kh'];
            if ($customer[0]['address_kh'] != '') {
                $temp_2 = $customer[0]['address_kh'];
            }
            if ($customer[0]['city_kh'] != '') {
                $temp_3 = ' '.$customer[0]['city_kh'];
            }
            if ($customer[0]['state_kh'] != '') {
                $temp_4 = ' '.$customer[0]['stat_kh'];
            }
            if ($customer[0]['country_kh'] != '') {
                $temp_5 = ' '.$customer[0]['country_kh'];
            }
            $temp_2 = $temp_2.$temp_3.$temp_4.$temp_5.' '.$customer[0]['postal_code'];
            $this->excel->getActiveSheet()->mergeCells('D'.($row - 1).':E'.$row);
            $this->excel->getActiveSheet()->SetCellValue('D'.($row - 1), $temp_2);
            $this->excel->getActiveSheet()->getStyle('D'.($row - 1))->getAlignment()->setWrapText(true);

            $this->excel->getActiveSheet()->getStyle('A'.($row - 1).':E'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

            $row++;
            $this->excel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
            $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('vat_no_kh').' ('.lang('vat_no').'):'.$biller[0]['vat_no']);
            $this->excel->getActiveSheet()->mergeCells('D'.$row.':E'.$row);
            $this->excel->getActiveSheet()->SetCellValue('D'.$row, lang('vat_no_kh').' ('.lang('vat_no').'):'.$customer[0]['vat_no']);

            $row++;
            $this->excel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
            $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('tel_kh').' '.lang('tel').':'.$biller[0]['phone']);
            $this->excel->getActiveSheet()->mergeCells('D'.$row.':E'.$row);
            $this->excel->getActiveSheet()->SetCellValue('D'.$row, lang('tel_kh').' '.lang('tel').':'.$customer[0]['phone']);

            $row++;
            $this->excel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
            $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('email_kh').' '.lang('email').':'.$biller[0]['email']);
            $this->excel->getActiveSheet()->mergeCells('D'.$row.':E'.$row);
            $this->excel->getActiveSheet()->SetCellValue('D'.$row, lang('email_kh').' '.lang('email').':'.$customer[0]['email']);
            
            $row++;
            
            $row++;
            $this->excel->getActiveSheet()->getStyle('A'.$row.':A'.($row + 3))->getFont()->setBold(true);
            $this->excel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
            $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('invoice_number_kh').' No: '.$select_data[0]['reference_no']);

            $row++;
            $date = date_create($select_data[0]['date']);
            $start_date = date_create($select_data[0]['start_date']);
            $end_date = date_create($select_data[0]['end_date']);
            $this->excel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
            $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('date_kh').' Date: '.date_format($date, 'F d, Y'));
            $row++;
            $this->excel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
            $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('period_kh').' Period: '.date_format($start_date, 'F d, Y').' - '.date_format($end_date, 'F d, Y'));
                                    
            $this->excel->getActiveSheet()->getStyle('A3:E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            //-Header-=======================================================================

            
            $row++;
            
            $row++;
            $this->excel->getActiveSheet()->getStyle('A'.$row.':E'.($row + 1))->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('A'.$row.':B'.($row + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('C'.$row.':E'.($row + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $this->excel->getActiveSheet()->getStyle('A'.$row.':E'.($row + 1))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('c9c9c9');
            $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('no_kh'));
            $this->excel->getActiveSheet()->SetCellValue('B'.$row, lang('description_kh'));
            $this->excel->getActiveSheet()->SetCellValue('C'.$row, lang('quantity_kh'));
            $this->excel->getActiveSheet()->SetCellValue('D'.$row, lang('unit_price_kh'));
            $this->excel->getActiveSheet()->SetCellValue('E'.$row, lang('subtotal_kh'));

            $row++;
            $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('No.'));
            $this->excel->getActiveSheet()->SetCellValue('B'.$row, lang('Item'));
            $this->excel->getActiveSheet()->SetCellValue('C'.$row, lang('Qty'));
            $this->excel->getActiveSheet()->SetCellValue('D'.$row, lang('Unit_Price'));
            $this->excel->getActiveSheet()->SetCellValue('E'.$row, lang('Subtotal'));
            
            

            $row++;

            if (is_array($temp)) {
                if (count($temp) > 0) {
                    $condition = 'and (sale_id = '.$temp[0]['id'];
                    for ($i=1;$i<count($temp);$i++) {
                        if ($temp[$i]['id'] != '') {
                            $condition .= ' or sale_id = '.$temp[$i]['id'];
                        }
                    }
                    $condition .= ')';

                    $config_data = array(
                    'table_name' => 'sma_sale_items',
                    'select_table' => 'sma_sale_items',
                    'select_condition' => "id > 0 ".$condition,
                );
                    $temp3 = $this->site->api_select_data_v2($config_data);
    
                    $temp4 = array();
                    $temp5 = $this->site->api_select_some_fields_with_where(
                        "*                
                    ",
                        "sma_sale_items",
                        "id > 0 ".$condition,
                        "arr"
                    );

                    if (is_array($temp5)) {
                        for ($i=0;$i<count($temp5);$i++) {
                            $temp4[$i] = $temp5[$i]['product_id'].'_'.$temp5[$i]['unit_price'];
                        }
                    }
                            
                    $temp4 = array_unique($temp4);
    
                    $temp2 = array();
                    $j = 0;
                    foreach ($temp4 as $value) {
                        for ($i=0;$i<count($temp5);$i++) {
                            $temp6 = $temp5[$i]['product_id'].'_'.$temp5[$i]['unit_price'];
                            if ($temp6 == $value) {
                                $temp2[$j]['product_name'] = $temp5[$i]['product_name'];
                                $temp2[$j]['product_id'] = $temp5[$i]['product_id'];
                                $temp2[$j]['quantity'] += $temp5[$i]['quantity'];
                                $temp2[$j]['unit_price'] = $temp5[$i]['unit_price'];
                                $temp2[$j]['subtotal'] += $temp5[$i]['subtotal'];
                            }
                        }
                        $j++;
                    }
                    sort($temp2);
                
                    $j = 1;
                    $grand_total = 0;
                
                    $temp_row = $row;
                    if (is_array($temp2)) {
                        for ($i=0;$i<count($temp2);$i++) {
                            $this->excel->getActiveSheet()->SetCellValue('A'.$row, $j);
                            $this->excel->getActiveSheet()->SetCellValue('B'.$row, $temp2[$i]['product_name']);
                            $this->excel->getActiveSheet()->SetCellValue('C'.$row, $temp2[$i]['quantity']);
                            $this->excel->getActiveSheet()->SetCellValue('D'.$row, $temp2[$i]['unit_price']);
                            $this->excel->getActiveSheet()->SetCellValue('E'.$row, $temp2[$i]['subtotal']);
                            $grand_total += $temp2[$i]['subtotal'];
                            $row++;
                            $j++;
                        }
                    }


                    $this->excel->getActiveSheet()->getStyle('D'.$temp_row.':E'.$row)->getNumberFormat()->setFormatCode('#,##0.00');

                    $this->excel->getActiveSheet()->getStyle('A'.$row.':E'.$row)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);


                    $this->excel->getActiveSheet()->getStyle('D'.$temp_row.':E'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                    $this->excel->getActiveSheet()->getStyle('D'.$temp_row.':E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
    
                    $this->excel->getActiveSheet()->getStyle('A'.$row.':E'.($row + 4))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                    $this->excel->getActiveSheet()->mergeCells('A'.$row.':D'.$row);
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('total_kh').' '.lang('total').' (USD)');
                    $this->excel->getActiveSheet()->SetCellValue('E'.$row, $grand_total);
                    $this->excel->getActiveSheet()->getStyle('E'.$row)->getNumberFormat()->setFormatCode('#,##0.00');
                
                    $row++;
                    $temp = ($grand_total * 10) / 100;
                    $this->excel->getActiveSheet()->mergeCells('A'.$row.':D'.$row);
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('vat_in_kh').' (USD)');
                    $this->excel->getActiveSheet()->SetCellValue('E'.$row, $temp);
                    $this->excel->getActiveSheet()->getStyle('E'.$row)->getNumberFormat()->setFormatCode('#,##0.00');

                    $temp_kh_currency_rate = $this->site->api_select_some_fields_with_where(
                        "
                    *     
                    ",
                        "sma_currencies",
                        "id = ".$select_data[0]['kh_currency_rate'],
                        "arr"
                    );
                    $temp_3 = 0;
                    if (count($temp_kh_currency_rate) > 0) {
                        $temp_3 = $temp_kh_currency_rate[0]['rate'];
                    }
                                
                    $temp_2 = ($grand_total + $temp) * $temp_3;

                    $temp = $grand_total + $temp;

                    $row++;
                    $temp_row = $row;
                    $this->excel->getActiveSheet()->mergeCells('A'.$row.':D'.$row);
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('total_amount_kh').' '.lang('total_amount').' (KHR)');
                    $this->excel->getActiveSheet()->SetCellValue('E'.$row, $temp_2);
                    $this->excel->getActiveSheet()->getStyle('E'.$row)->getNumberFormat()->setFormatCode('#,##0.00');

                    $row++;
                    $this->excel->getActiveSheet()->mergeCells('A'.$row.':D'.$row);
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('total_amount_kh').' '.lang('total_amount').' (USD)');
                    $this->excel->getActiveSheet()->SetCellValue('E'.$row, $temp);
                    $this->excel->getActiveSheet()->getStyle('E'.$row)->getNumberFormat()->setFormatCode('#,##0.00');

                    if ($select_data[0]['rebate'] == '' || $select_data[0]['rebate'] == 'yes') {
                        $temp5 = $this->sales_model->api_get_rebate_options($grand_total);
                    }
                
                    if ($temp5['value'] > 0) {
                        $temp_rebate = ($grand_total * $temp5['value']) / 100;

                        $temp = $temp - $temp_rebate;
                        $row++;
                        $this->excel->getActiveSheet()->mergeCells('A'.$row.':D'.$row);
                        $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('rebate_kh').' '.lang('Rebate').' '.$temp5['value'].'% (USD)');
                        $this->excel->getActiveSheet()->getStyle('A'.$row.':E'.$row)->getFont()->setBold(true);
                        $this->excel->getActiveSheet()->SetCellValue('E'.$row, $temp_rebate);
                        $this->excel->getActiveSheet()->getStyle('E'.$row)->getNumberFormat()->setFormatCode('#,##0.00');

                        $row++;
                        $this->excel->getActiveSheet()->mergeCells('A'.$row.':D'.$row);
                        $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('grand_total_kh').' '.lang('grand_total').' (USD)');
                        $this->excel->getActiveSheet()->getStyle('A'.$row.':E'.$row)->getFont()->setBold(true);
                        $this->excel->getActiveSheet()->SetCellValue('E'.$row, $temp);
                        $this->excel->getActiveSheet()->getStyle('E'.$row)->getNumberFormat()->setFormatCode('#,##0.00');
                        $this->excel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                    } else {
                        $this->excel->getActiveSheet()->getStyle('A'.$temp_row.':E'.$row)->getFont()->setBold(true);
                    }

                    $row++;
                    $temp = number_format($temp_kh_currency_rate[0]['rate'], 0);
                    $this->excel->getActiveSheet()->SetCellValue('E'.$row, '1$ = '.$temp.' KHR');

                
                    $row = $row + 2;
                
                    $row++;
                    if ($biller[0]['company_kh'] != '-') {
                        $temp = $biller[0]['company_kh'];
                    } else {
                        $temp = [0]['name_kh'];
                    }
                    $this->excel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('seller_kh').': '.$temp);

                    if ($customer[0]['company_kh'] != '-') {
                        $temp = $customer[0]['company_kh'];
                    } else {
                        $temp = [0]['name_kh'];
                    }
                    $this->excel->getActiveSheet()->mergeCells('D'.$row.':E'.$row);
                    $this->excel->getActiveSheet()->SetCellValue('D'.$row, lang('customer_kh').': '.$temp);
                
                    $row++;
                    if ($biller[0]['company'] != '-') {
                        $temp = $biller[0]['company'];
                    } else {
                        $temp = [0]['name'];
                    }
                    $temp = lang('seller').': '.$temp;
                    $this->excel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, $temp);

                    if ($customer[0]['company'] != '-') {
                        $temp = $customer[0]['company'];
                    } else {
                        $temp = [0]['name'];
                    }
                    $temp = lang('customer').': '.$temp;
                    $this->excel->getActiveSheet()->mergeCells('D'.$row.':E'.$row);
                    $this->excel->getActiveSheet()->SetCellValue('D'.$row, $temp);
                
                    $row = $row + 5;
                
                    $row++;
                    $this->excel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
                    $this->excel->getActiveSheet()->getStyle('A'.$row.':B'.$row)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

                    $this->excel->getActiveSheet()->mergeCells('D'.$row.':E'.$row);
                    $this->excel->getActiveSheet()->getStyle('D'.$row.':E'.$row)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                
                    $row++;
                    $this->excel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('stamp_sign_kh').' Stamp & Signature');

                    $this->excel->getActiveSheet()->mergeCells('D'.$row.':E'.$row);
                    $this->excel->getActiveSheet()->SetCellValue('D'.$row, lang('stamp_sign_kh').' Stamp & Signature');

                    $row = $row + 5;
                    $row++;
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, '');
                } else {
                    $this->excel->getActiveSheet()->mergeCells('A'.$row.':E'.$row);
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, 'No record found');
                }
            }

            
            
            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
            $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(22);
            $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(22);
            $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $this->excel->getActiveSheet()->getStyle('B17:B'.$row)->getAlignment()->setWrapText(true);
                

            $filename = 'Sale_Summary_Tax_Invoice_' . date('Y_m_d_H_i_s');
            $this->load->helper('excel');
            create_excel($this->excel, $filename);
            admin_redirect("sales/sale_summary");
        } else {
            $this->session->set_flashdata('error', lang('Sale_summary_is_not_found'));
            admin_redirect("sales/sale_summary");
        }
    }
    public function change_sl_to_do($id)
    {
        if ($id > 0) {
            $config_data = array(
                'table_name' => 'sma_sales',
                'select_table' => 'sma_sales',
                'select_condition' => "id = ".$id,
            );
            $select_data = $this->site->api_select_data_v2($config_data);

            if (is_int(strpos($select_data[0]['reference_no'], "SL"))) {
                $temp = $this->site->api_select_some_fields_with_where(
                    "
                    id
                    ",
                    "sma_sales",
                    "getTranslate(add_ons,'do_sl_reference_no','".f_separate."','".v_separate."') = '".$select_data[0]['reference_no']."'",
                    "arr"
                );
                if (is_array($temp)) {
                    if (count($temp) > 0) {
                        $this->session->set_flashdata('error', 'This sale has a DO sale already.');
                        admin_redirect("sales/index");
                    }
                }

                $reference_no = $this->site->getSaleReference('');
                $up_sale_ref = $this->site->getSaleReference('', true);
                $this->site->updateReference($up_sale_ref);
                $config_data = array(
                    'duplicate_table' => 'sma_sales',
                    'into_table' => 'sma_sales',
                    'duplicate_id_col_name' => 'id',
                    'condition' => "id = ".$id,
                    'update_1' => "reference_no = '".$reference_no."'",
                    'update_2' => "order_tax_id = 1",
                    'update_3' => "order_tax = 0",
                    'update_4' => "total_tax = 0",
                    'update_5' => "grand_total = ".($select_data[0]['grand_total'] - $select_data[0]['total_tax']),
                    'count_update' => 5,
                );
                $this->site->api_duplicate_record_update($config_data);

                $temp_2 = $this->site->api_select_some_fields_with_where("id", "sma_sales", "reference_no = '".$reference_no."'", "arr");
                $config_data = array(
                    'table_name' => 'sma_sales',
                    'id_name' => 'id',
                    'field_add_ons_name' => 'add_ons',
                    'selected_id' => $temp_2[0]['id'],
                    'add_ons_title' => 'do_sl_reference_no',
                    'add_ons_value' => $select_data[0]['reference_no'],
                );
                $this->site->api_update_add_ons_field($config_data);

                $temp_sale_items = $this->site->api_select_some_fields_with_where(
                    "*
                    ",
                    "sma_sale_items",
                    "sale_id = ".$id,
                    "arr"
                );
                for ($i=0;$i<count($temp_sale_items);$i++) {
                    $config_data = array(
                        'duplicate_table' => 'sma_sale_items',
                        'into_table' => 'sma_sale_items',
                        'duplicate_id_col_name' => 'id',
                        'condition' => "id = ".$temp_sale_items[$i]['id'],
                        'update_1' => "sale_id = ".$temp_2[0]['id'],
                        'count_update' => 1,
                    );
                    $this->site->api_duplicate_record_update($config_data);
                }

                $this->db->delete('sma_sale_items', 'sale_id = '.$select_data[0]['id']);
                $temp = array(
                    'order_tax' => 0,
                    'total_tax' => 0,
                    'total' => 0,
                    'grand_total' => 0,
                );
                $this->db->update('sma_sales', $temp, 'id = '.$select_data[0]['id']);

                $this->session->set_flashdata('message', $reference_no.' sale is added.');
                admin_redirect("sales/index");
            } else {
                $this->session->set_flashdata('error', lang('Please_select_a_SL_sale'));
                admin_redirect("sales/index");
            }
        }
    }

    public function change_do_to_sl($id)
    {
        if ($id > 0) {
            $config_data = array(
                'table_name' => 'sma_sales',
                'select_table' => 'sma_sales',
                'select_condition' => "id = ".$id,
            );
            $select_data = $this->site->api_select_data_v2($config_data);

            if (is_int(strpos($select_data[0]['reference_no'], "DO"))) {
                $temp = $this->site->api_select_some_fields_with_where(
                    "
                    id
                    ",
                    "sma_sales",
                    "add_ons like '%:do_to_sl_reference_no:{".$select_data[0]['reference_no']."}:%' limit 1",
                    "arr"
                );
                if (is_array($temp)) {
                    if (count($temp) > 0) {
                        $this->session->set_flashdata('error', 'This sale has a SL sale already.');
                        admin_redirect("sales/index");
                    }
                }

                $temp_return = $this->site->api_select_some_fields_with_where(
                    "id,getTranslate(add_ons,'sale_id','".f_separate."','".v_separate."') as sale_id
                    ",
                    "sma_returns",
                    "getTranslate(add_ons,'sale_id','".f_separate."','".v_separate."') = ".$id,
                    "arr"
                );
                if (count($temp_return) > 0) {
                    $this->session->set_flashdata('error', lang('Cannot_change_any_sale_that_has_a_return_record.'));
                    admin_redirect("sales/index");
                }

                $config_data = array(
                    'type' => 'sale',
                    'sale_reference_type' => 'sale_sl',
                    'date' => $select_data[0]['date'],
                    'customer_id' => $select_data[0]['customer_id'],
                    'update' => 1,
                );
                $temp = $this->site->api_calculate_reference_no($config_data);
                $reference_no = $temp['reference_no'];

                $order_tax = ($select_data[0]['grand_total'] * 10) / 100;
                $total_tax = $order_tax;
                $grand_total = $select_data[0]['grand_total'] + $order_tax;
                $config_data = array(
                    'duplicate_table' => 'sma_sales',
                    'into_table' => 'sma_sales',
                    'duplicate_id_col_name' => 'id',
                    'condition' => "id = ".$id,
                    'update_1' => "reference_no = '".$reference_no."'",
                    'update_2' => "order_tax_id = 2",
                    'update_3' => "order_tax = ".$order_tax,
                    'update_4' => "total_tax = ".$total_tax,
                    'update_5' => "grand_total = ".$grand_total,
                    'count_update' => 5,
                );
                $this->site->api_duplicate_record_update($config_data);
                

                $temp_2 = $this->site->api_select_some_fields_with_where("id", "sma_sales", "reference_no = '".$reference_no."'", "arr");

                $config_data = array(
                    'table_name' => 'sma_sales',
                    'id_name' => 'id',
                    'field_add_ons_name' => 'add_ons',
                    'selected_id' => $temp_2[0]['id'],
                    'add_ons_title' => 'do_to_sl_reference_no',
                    'add_ons_value' => $select_data[0]['reference_no'],
                );
                $this->site->api_update_add_ons_field($config_data);

                $temp_sale_items = $this->site->api_select_some_fields_with_where(
                    "*
                    ",
                    "sma_sale_items",
                    "sale_id = ".$id,
                    "arr"
                );
                for ($i=0;$i<count($temp_sale_items);$i++) {
                    $config_data = array(
                        'duplicate_table' => 'sma_sale_items',
                        'into_table' => 'sma_sale_items',
                        'duplicate_id_col_name' => 'id',
                        'condition' => "id = ".$temp_sale_items[$i]['id'],
                        'update_1' => "sale_id = ".$temp_2[0]['id'],
                        'count_update' => 1,
                    );
                    $this->site->api_duplicate_record_update($config_data);
                }


                $this->db->delete('sma_sale_items', 'sale_id = '.$select_data[0]['id']);
                $temp = array(
                    'disabled' => 1,
                    'order_tax' => 0,
                    'total_tax' => 0,
                    'total' => 0,
                    'grand_total' => 0,
                );
                $this->db->update('sma_sales', $temp, 'id = '.$select_data[0]['id']);


                $this->session->set_flashdata('message', $reference_no.' sale is added.');
                admin_redirect("sales/index");
            } else {
                $this->session->set_flashdata('error', lang('Please_select_a_DO_sale'));
                admin_redirect("sales/index");
            }
        }
    }

    public function api_temp_customer_change()
    {
        if ($this->Owner || $this->Admin) {
            foreach ($_GET as $name => $value) {
                ${$name} = $value;
            }
            $value = '';
            if ($id > 0) {
                $config_data = array(
                'table_name' => 'sma_company_branch',
                'select_table' => 'sma_company_branch',
                'translate' => 'en',
                'select_condition' => "parent_id = ".$id,
            );
                $temp = $this->site->api_select_data_v2($config_data);
            }

            $temp_2 = '';
            if (is_array($temp)) {
                if (count($temp) > 0) {
                    for ($i=0;$i<count($temp);$i++) {
                        $tr2[$temp[$i]['id']] = $temp[$i]['title'];
                    }
                } else {
                    $tr2[''] = lang("Please_select_a_company_branch");
                    $temp_2 = 'disabled="diabled"';
                }
            }
        
            $value = form_dropdown('add_ons_company_branch', $tr2, $company_branch, 'data-placeholder="'.lang('Please_select_company_branch').'" id="add_ons_company_branch" class="form-control" '.$temp_2.' ');

            $temp3[1] = 'api-ajax-request-multiple-result-split';
            $temp3[1] .= $value;
            $temp3[2] = 'api-ajax-request-multiple-result-split';
            $result = $temp3[1].$temp3[2];
            echo $result;
        } else {
            echo '<script>window.location = "'.base_url().'admin/login";</script>';
        }
    }

    public function ajax_update_rebate()
    {
        if ($this->Owner || $this->Admin) {
            foreach ($_GET as $name => $value) {
                ${$name} = $value;
            }
            if ($field_id > 0) {
                $config_data = array(
                    'table_name' => 'sma_sale_summary',
                    'select_table' => 'sma_sale_summary',
                    'select_condition' => "id = ".$field_id,
                );
                $select_data = $this->site->api_select_data_v2($config_data);
                if ($select_data[0]['rebate'] == '' || $select_data[0]['rebate'] == 'yes') {
                    $value = 'no';
                } else {
                    $value = 'yes';
                }

                $config_data = array(
                    'table_name' => 'sma_sale_summary',
                    'id_name' => 'id',
                    'field_add_ons_name' => 'add_ons',
                    'selected_id' => $select_data[0]['id'],
                    'add_ons_title' => 'rebate',
                    'add_ons_value' => $value,
                );
                $this->site->api_update_add_ons_field($config_data);

                if ($value == 'yes') {
                    $temp_display = '
                        <a href="javascript:void(0);" onclick="
                var postData = {
                    \'ajax_file\' : \''.admin_url('sales/ajax_update_rebate').'\',
                    \'field_id\' : \''.$select_data[0]['id'].'\',
                    \'element_id\' : \'api_rebate_'.$select_data[0]['id'].'\',
                };
                ajax_update_rebate(postData);
                        ">
                            <span class="label label-success">
                            <i class="fa fa-check"></i> Yes</span>
                        </a>
                    ';
                } else {
                    $temp_display = '
                        <a href="javascript:void(0);" onclick="
                var postData = {
                    \'ajax_file\' : \''.admin_url('sales/ajax_update_rebate').'\',
                    \'field_id\' : \''.$select_data[0]['id'].'\',
                    \'element_id\' : \'api_rebate_'.$select_data[0]['id'].'\',
                };
                ajax_update_rebate(postData);
                        ">
                            <span class="label label-danger">
                            <i class="fa fa-check"></i> No</span>
                        </a>
                    ';
                }
            }


            $temp3[1] = 'api-ajax-request-multiple-result-split';
            $temp3[1] .= $value;
            $temp3[2] = 'api-ajax-request-multiple-result-split';
            $temp3[2] .= $temp_display;
            $temp3[3] = 'api-ajax-request-multiple-result-split';
            $result = $temp3[1].$temp3[2].$temp3[3];
            echo $result;
        } else {
            echo '<script>window.location = "'.base_url().'admin/login";</script>';
        }
    }


    public function api_send_mail_customer_order($id)
    {
        $config_data = array(
            'table_name' => 'sma_sales',
            'select_table' => 'sma_sales',
            'select_condition' => "id = ".$id,
        );
        $temp = $this->site->api_select_data_v2($config_data);

        $temp_created_by = $this->site->api_select_some_fields_with_where(
            "
            *
            ",
            "sma_users",
            "id = ".$temp[0]['created_by'],
            "arr"
        );
        if ($temp_created_by[0]['group_id'] == 3) {
            $temp_shop = $this->site->api_select_some_fields_with_where(
                "
                logo
                ",
                "sma_shop_settings",
                "shop_id = 1",
                "arr"
            );

            $customer = $this->site->getCompanyByID($temp[0]['customer_id']);
            $biller = $this->site->getCompanyByID($temp[0]['biller_id']);

            $subject = 'Daishin Tranding Order #'.$id;
            $parse_data = array(
                'order_id' => $temp[0]['id'],
                'contact_person' => $customer->name,
                'company' => $customer->company && $customer->company != '-' ? '('.$customer->company.')' : '',
                'order_link' => shop_url('orders/'.$temp[0]['id']),
                'site_link' => substr(base_url(), 0, -1),
                'site_name' => $this->Settings->site_name,
                'logo' => '<img src="' . base_url() . 'assets/uploads/logos/' . $temp_shop[0]['logo'] . '" />',
            );
            $msg = file_get_contents('./themes/' . $this->Settings->theme . '/admin/views/email_templates/sale_v2.html');
            $message = $this->parser->parse_string($msg, $parse_data);
            
            $name = lang("Order_Detail") . "_" . $temp[0]['id'] . ".pdf";

            $this->db->select('sale_items.*, tax_rates.code as tax_code, tax_rates.name as tax_name, tax_rates.rate as tax_rate, products.image, products.details as details, product_variants.name as variant, products.hsn_code as hsn_code,  products.second_name as second_name')
                ->join('products', 'products.id=sale_items.product_id', 'left')
                ->join('product_variants', 'product_variants.id=sale_items.option_id', 'left')
                ->join('tax_rates', 'tax_rates.id=sale_items.tax_rate_id', 'left')
                ->group_by('sale_items.id')
                ->order_by('id', 'asc');
            $row = $this->db->get_where('sale_items', ['sale_id' => $id])->result();

            $this->data['inv'] = $temp;
            $this->data['rows'] = $row;
            $this->data['customer'] = $customer;
            $this->data['biller'] = $biller;
            $this->data['Settings'] = $this->Settings;
            $html = $this->load->view($this->Settings->theme.'/shop/views/pages/pdf_invoice', $this->data, true);
            $attachment = $this->sma->generate_pdf($html, $name, 'S', $this->data['biller']->invoice_footer);

            $cc = '';
            $bcc = '';
            if (!is_int(strpos($_SERVER["HTTP_HOST"], "localhost"))) {
                $this->sma->send_email($temp_created_by[0]['email'], $subject, $message, null, null, $attachment, $cc, $bcc);
            }
        }
    }


    public function change_payment_status_to_due()
    {
        $check_value = $this->input->get('check_value');
        $temp_val = explode('-', $check_value);
        $this->sma->checkPermissions('index', true);

        $j = 0;
        $select_data = array();
        foreach ($temp_val as $id) {
            if ($id != '') {
                $temp = $this->site->api_select_some_fields_with_where(
                    "
                    *
                    ",
                    "sma_sales",
                    "id = ".$id,
                    "arr"
                );
                if (count($temp) > 0) {
                    $temp_2 = $this->site->api_select_some_fields_with_where(
                        "
                        *
                        ",
                        "sma_companies",
                        "id = ".$temp[0]['customer_id'],
                        "arr"
                    );
                    if ($temp_2[0]['payment_category'] != 'cash on delivery' && $temp[0]['reference_no'] != '') {
                        $select_data[$j]['reference_no'] = $temp[0]['reference_no'];
                        $select_data[$j]['customer'] = $temp[0]['customer'];
                        $select_data[$j]['payment_category'] = $temp_2[0]['payment_category'];
                        $j++;
                    }
                }
            }
        }

        $this->data['select_data'] = $select_data;

        $this->load->view($this->theme . 'sales/change_payment_status_to_due', $this->data);
    }


    public function change_sl_sale_date($id)
    {
        if ($id > 0) {
            $config_data = array(
            'table_name' => 'sma_sales',
            'select_table' => 'sma_sales',
            'select_condition' => "id = ".$id,
        );
            $select_data = $this->site->api_select_data_v2($config_data);

            $config_data = array(
            'type' => 'sale',
            'sale_reference_type' => 'sale_sl',
            'date' => $select_data[0]['date'],
            'customer_id' => $select_data[0]['customer_id'],
            'update' => 1,
        );
            $temp = $this->site->api_calculate_reference_no($config_data);
            $reference_no = $temp['reference_no'];

            $config_data = array(
            'duplicate_table' => 'sma_sales',
            'into_table' => 'sma_sales',
            'duplicate_id_col_name' => 'id',
            'condition' => "id = ".$id,
            'update_1' => "reference_no = '".$reference_no."'",
            'count_update' => 6,
        );
            $this->site->api_duplicate_record_update($config_data);

            $temp_2 = $this->site->api_select_some_fields_with_where("id", "sma_sales", "reference_no = '".$reference_no."'", "arr");
                   

            $temp_sale_items = $this->site->api_select_some_fields_with_where(
                "*
            ",
                "sma_sale_items",
                "sale_id = ".$id,
                "arr"
            );
            for ($i=0;$i<count($temp_sale_items);$i++) {
                $config_data = array(
                'duplicate_table' => 'sma_sale_items',
                'into_table' => 'sma_sale_items',
                'duplicate_id_col_name' => 'id',
                'condition' => "id = ".$temp_sale_items[$i]['id'],
                'update_1' => "sale_id = ".$temp_2[0]['id'],
                'count_update' => 1,
            );
                $this->site->api_duplicate_record_update($config_data);
            }

            $this->db->delete('sma_sale_items', 'sale_id = '.$select_data[0]['id']);
            $temp = array(
            'disabled' => 1,
            'order_tax' => 0,
            'total_tax' => 0,
            'total' => 0,
            'grand_total' => 0,
        );
            $this->db->update('sma_sales', $temp, 'id = '.$select_data[0]['id']);


            $this->session->set_flashdata('message', $reference_no.' sale is added.');
            admin_redirect("sales/index");
        }
    }
    public function get_tranc_sale()
    {
        !empty($_GET['transaction_search'])?$search = $_GET['transaction_search']:$search='';
        $search = strtolower($search);
        // $str_num = preg_split('/(?=[a-z]+)(?<=[0-9])/i',$search);
        // array search
        // $array_search = array_flip(explode(" ",$search));
        // $config_data = array(
        //     'table_name' => 'sma_payments',
        //     'select_table' => 'sma_payments',
        // );
        // foreach($array_search as $key){
        //     $str_num = $this->f_preg_split($key);
        //     if(is_numeric($str_num['num']) && is_string($str_num['str'])){
        //         $data = "add_ons like '%:%transfer_reference_number:{%".$str_num['num']."%}:' OR add_ons like '%:%transaction_id:{%".$str_num['num']."%}:' AND paid_by LIKE '%".$str_num['str']."%'";
        //         array_key_exists('select_condition',$config_data) ? $config_data['select_condition'].' AND '.$data : $config_data['select_condition'] = $data;
        //     }elseif(is_numeric($str_num['num']) && is_string($str_num['str'])==""){
        //         $data = "paid_by LIKE '%".$str_num['str']."%'";
        //         array_key_exists('select_condition',$config_data) ? $config_data['select_condition'].' AND '.$data : $config_data['select_condition'] = $data;
        //     }elseif(is_numeric($str_num['num'])=="" && is_string($str_num['str'])){
        //         $data = "add_ons like '%:%transfer_reference_number:{%".$str_num['num']."%}:' OR add_ons like '%:%transaction_id:{%".$str_num['num']."%}:'";
        //         array_key_exists('select_condition',$config_data) ? $config_data['select_condition'].' AND '.$data : $config_data['select_condition'] = $data;
        //     }
        // }
        // var_dump($config_data);
        // var_dump($array_search);
        // die();
        // return $this->get_searchID($config_data);

        $config_data = array(
            'table_name' => 'sma_payments',
            'select_table' => 'sma_payments',
        );
        if(is_numeric($search)){
            $config_data['select_condition'] = "add_ons like '%:%transfer_reference_number:{%$search%}:' OR add_ons like '%:%transaction_id:{%$search%}:'";
            $this->get_searchID($config_data);
        }elseif(is_string($search)){
            $config_data['select_condition'] = "paid_by like '%$search%'";
            $this->get_searchID($config_data);
        }
    }
    function get_searchID($config_data){
        $sqls = $this->site->api_select_data_v2($config_data);

        $sales_id = '';
        foreach ($sqls as $key => $value) {
            $sales_id .='-'.$value['sale_id'];
        }
        echo $sales_id;
        return $sales_id;
    }
    function f_preg_split($search){
        $array = [];
        $str_num = preg_split('/(?=[a-z]+)(?<=[0-9])/i',$search);
        !empty($array['str'])?$array['str']=$str_num[0]:"";
        !empty($array['num'])?$array['num']=$str_num[1]:"";
        if(is_numeric($array['num'])||is_string($array['str'])){
            return $array;
        }else{
            $str_num = preg_split('/(?<=[0-9])(?=[a-z]+)/i',$search);
            !empty($array['str'])?$array['str']=$str_num[1]:"";
            !empty($array['num'])?$array['num']=$str_num[0]:"";
            return $array;
        }
    }
}
