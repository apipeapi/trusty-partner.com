<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        if ($this->Customer || $this->Supplier) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
		$this->load->helper('string');
        $this->lang->admin_load('customers', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('companies_model');
    }

    function index($action = NULL)
    {
        $this->sma->checkPermissions();

        $sort_by = $this->input->get('sort_by');
        $sort_order = $this->input->get('sort_order');

        $per_page = $this->input->get('per_page') != '' ? $this->input->get('per_page') : 100;
        $page = $this->input->get('page') != '' ? $this->input->get('page') : 1;

        $offset_no = $page != '' ? ($page - 1 ) * $per_page : $offset_no;
        
        if ($page != '')
            $offset_no = ($page - 1 ) * $per_page;
        else
            $offset_no = 0;

        $search = $this->input->get('search');
        $search = $this->db->escape_str($search);
        
        $temp_url = '';
        if ($page != '') 
            $temp_url .= '?page='; 
        if ($per_page != '') $temp_url .= '&per_page='.$per_page;
        foreach ($_GET as $name => $value) {
            if ($name != 'page' && $name != 'per_page' && $name != 'val')
                if ($value != '') $temp_url .= '&'.$name.'='.$value;
        }

        $config_data = array(
            'lg' => 'en',
            'table_name' => 'sma_companies',
            'table_id' => 'id',
            'search' => $search,
            'sort_by' => $sort_by,
            'sort_order' => $sort_order,
            'limit' => $per_page,
            'offset_no' => $offset_no,
        );
        $select_data = $this->companies_model->api_get_data($config_data);

        $config_data = array(
            'lg' => 'en',
            'table_name' => 'sma_companies',
            'table_id' => 'id',
            'search' => $search,
            'sort_by' => '',
            'sort_order' => '',
            'limit' => '',
            'offset_no' => '',
        );
        $temp_count = $this->companies_model->api_get_data($config_data);
        
        $total_record = count($temp_count);


        /* config pagination */
        $this->data['select_data'] = $select_data;

        $this->data['page'] = $page;
        $this->data['search'] = $search;
        $this->data['sort_by'] = $sort_by;
        $this->data['sort_order'] = $sort_order;
        $this->data['per_page'] = $per_page;
        $this->data['total_rows_sale'] = $total_record;
        $this->data['index_page'] = 'admin/customers';
        $this->data['show_data'] = $page;

        $config_base_url = site_url() . 'admin/customers'; //your url where the content is displayed
        $config['base_url'] = $config_base_url.$temp_url; //your url where the content is displayed
        $config['query_string_segment'] = 'page';
        $config['per_page'] = $per_page; //$per_page;
        $config['num_links'] = 5;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['full_tag_open'] = '<ul class="pagination pagination-new-bg">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = FALSE;
        $config['last_link'] = FALSE;
        $config['prev_link'] = '&lt; Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next &gt;';
        $config['next_tag_open'] = '<li class="next">';
        $config['next_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['total_rows'] = $total_record; //$this->data['total_rows_sale'];        
        $this->load->library('pagination');   
        $this->pagination->initialize($config);
        /** end config pagination */
        /**Showing Label Data At Table Footer */
        $showing = $total_record != '' ? 1 : 0;
        $label_show_to= ($page == '' || $per_page < $total_record) ?  $per_page  :  $total_record;
        $label_show = "Showing " . $showing . " to ".$label_show_to." of : " . number_format($total_record) . " entries";

        $show_page = $page > 1 ? ($page * $per_page) : $total_record;
        $show_page_no = $show_page > $total_record ? $total_record : $page * $per_page;
        $label_show_page = "Showing " . (($page - 1) * ($per_page) + 1) . " to " . $show_page_no . " (filtered from " . number_format($total_record) . " total entries) ";
        $this->data['show'] = $search != '' || $page > 1 ? $label_show_page : $label_show;

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

/*
        $temp = $this->site->api_select_some_fields_with_where("
            id, company
            "
            ,"sma_companies"
            ,"parent_id = 0 order by company asc"
            ,"arr"
        );
        $temp_3 = array();
        $j = 0;
        for ($i=0;$i<count($temp);$i++) {
            $temp_2 = $this->site->api_select_some_fields_with_where("
                id
                "
                ,"sma_companies"
                ,"parent_id = ".$temp[$i]['id']." limit 1"
                ,"arr"
            );
            if (count($temp_2) > 0) {
                $temp_3[$j] = $temp[$i];
                $j++;
            }
        }
        $this->data['api_parent_company'] = $temp_3;
*/
        
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('customers')));
        $meta = array('page_title' => lang('customers'), 'bc' => $bc);
        $this->page_construct('customers/index', $meta, $this->data);
    }

    function getCustomers()
    {
        $this->sma->checkPermissions('index');
        $this->load->library('datatables');
        $this->datatables
            ->select(
                "id, company, name, email, phone, 
                price_group_name, customer_group_name, 
                vat_no"
            )
            ->from("companies")
            ->where('group_name', 'customer')
            ->add_column(
                    "Actions", 
                    "<div class=\"text-center\">
                        <a class=\"tip\" title='" . lang("list_deposits") . "' href='" . admin_url('customers/deposits/$1') . "' 
                            data-toggle='modal' data-target='#myModal'>
                            <i class=\"fa fa-money\"></i>
                        </a> 
                        <a class=\"tip\" title='" . lang("add_deposit") . "' href='" . admin_url('customers/add_deposit/$1') . "' 
                            data-toggle='modal' data-target='#myModal'>
                            <i class=\"fa fa-plus\"></i>
                        </a> 
                        <a class=\"tip\" title='" . lang("list_addresses") . "' href='" . admin_url('customers/addresses/$1') . "' 
                            data-toggle='modal' data-target='#myModal'><i class=\"fa fa-location-arrow\"></i></a> 
                        <a class=\"tip\" title='" . lang("list_users") . "' href='" . admin_url('customers/users/$1') . "' 
                            data-toggle='modal' data-target='#myModal'><i class=\"fa fa-users\"></i></a> 
                        <a class=\"tip\" title='" . lang("add_user") . "' href='" . admin_url('customers/add_user/$1') . "' 
                            data-toggle='modal' data-target='#myModal'><i class=\"fa fa-user-plus\"></i></a> 
                        <a class=\"tip\" title='" . lang("edit_customer") . "' href='" . admin_url('customers/edit/$1') . "' 
                            data-toggle='modal' data-target='#myModal'><i class=\"fa fa-edit\"></i>
                        </a> 
                        <a href='#' class='tip po' title='<b>" . lang("delete_customer") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p>
                            <a class='btn btn-danger po-delete' href='" . admin_url('customers/delete/$1') . "'>" . 
                                lang('i_m_sure') . 
                            "</a> 
                            <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'>
                            <i class=\"fa fa-trash-o\"></i>
                        </a>
                    </div>",  
            "id");
            $this->db->order_by('id desc');            
            // ->unset_column('id');
        echo $this->datatables->generate();
    }

    function view($id = NULL)
    {
        $this->sma->checkPermissions('index', true);
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['customer'] = $this->companies_model->getCompanyByID($id);
        $this->load->view($this->theme.'customers/view',$this->data);
    }

    function add()
    {
        $this->sma->checkPermissions(false, true);
        $this->form_validation->set_rules('city_id', lang("city"));
        $this->form_validation->set_rules('email', lang("email_address"), 'is_unique[companies.email]');

        if ($this->form_validation->run('companies/add') == true) {
            $cg = $this->site->getCustomerGroupByID($this->input->post('customer_group'));
            $pg = $this->site->getPriceGroupByID($this->input->post('price_group'));
            $add_ons = 'initial_first_add_ons:{}:';    
            $opening_date = $this->input->post('add_ons_opening_date');
                if ($opening_date == ''){
                    $opening_date = date('Y-m-d H:i:s');
                }
                else{
                    $opening_date = $this->sma->fld(trim($this->input->post('add_ons_opening_date').':'.date('s')));
                    $_POST['add_ons_opening_date'] = $opening_date;  
                }
            
                $birthday = $this->input->post('add_ons_birth_day');
                if ($birthday == '')
                    $birthday = date('Y-m-d H:i:s');
                else {
                    $birthday = $this->sma->fld(trim($this->input->post('add_ons_birth_day').':'.date('s')));
                    $_POST['add_ons_birth_day'] = $birthday; 
                }

                $payment_due_date = $this->input->post('add_ons_payment_due_date');
                if ($payment_due_date == '')
                    $payment_due_date = date('Y-m-d H:i:s');
                else {
                    $payment_due_date = $this->sma->fld(trim($this->input->post('add_ons_payment_due_date').':'.date('s')));
                    $_POST['add_ons_payment_due_date'] = $payment_due_date; 
                }

                $_POST['add_ons_customer_paid_by'] = $this->input->post('add_ons_customer_paid_by');
                
            // check national ID for customer      
                $config_data = array(
                    'table_name' => 'sma_companies',
                    'select_table' => 'sma_companies',
                    'translate' => '',
                    'select_condition' => "id > 0 order by id desc ",
                );
                $temp_national_id = $this->site->api_select_data_v2($config_data);
                $national_id = $this->input->post('add_ons_national_id');

                $facebook = $this->input->post('add_ons_facebook');
                $telegram = $this->input->post('add_ons_telegram');
                $instagram = $this->input->post('add_ons_instagram');
                $google = $this->input->post('add_ons_google');
                if($national_id == '')
                    $_POST['add_ons_national_id'] = '';
                else {
                    for($i=0;$i<count($temp_national_id);$i++){
                        if(strtolower(trim($temp_national_id[$i]['national_id'])) == strtolower(trim($national_id))) {   
                            $this->session->set_flashdata('error', lang("National ID : ".$national_id." is already existed"));
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                    }
                } 
                
            // check passport ID for customer 
                $config_data = array(
                    'table_name' => 'sma_companies',
                    'select_table' => 'sma_companies',
                    'translate' => '',
                    'select_condition' => "id > 0 order by id desc ",
                );
                $temp_passport_id = $this->site->api_select_data_v2($config_data);
                $passport_id = $this->input->post('add_ons_passport_id');
                if($passport_id == '')
                    $_POST['add_ons_passport_id'] = '';
                else {
                    for($j=0;$j<count($temp_passport_id);$j++){
                        if(strtolower(trim($temp_passport_id[$j]['passport_id'])) == strtolower(trim($passport_id))) {   
                            $this->session->set_flashdata('error', lang("Passport ID : ".$passport_id." is already existed"));
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                    }
                }

            foreach($_POST as $name => $value) {
                if (is_int(strpos($name,"add_ons_"))) {
                    $temp_name = str_replace('add_ons_','',$name);
                    $add_ons .= $temp_name.':{'.$value.'}:';
                }
            }            
            $data = array('name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'group_id' => '3',
                'group_name' => 'customer',
                'customer_group_id' => $this->input->post('customer_group'),
                'customer_group_name' => $cg->name,
                'price_group_id' => $this->input->post('price_group') ? $this->input->post('price_group') : NULL,
                'price_group_name' => $this->input->post('price_group') ? $pg->name : NULL,
                'company' => $this->input->post('company'),
                'address' => $this->input->post('address'),
                'vat_no' => $this->input->post('vat_no'),
                'city' => $this->input->post('city'),
                'city_id' => $this->input->post('city_id'),
                'state' => $this->input->post('state'),
                'postal_code' => $this->input->post('postal_code'),
                'country' => $this->input->post('country'),
                'phone' => $this->input->post('phone'),
                'cf1' => $this->input->post('cf1'),
                'cf2' => $this->input->post('cf2'),
                'cf3' => $this->input->post('cf3'),
                'cf4' => $this->input->post('cf4'),
                'cf5' => $this->input->post('cf5'),
                'cf6' => $this->input->post('cf6'),
                'gst_no' => $this->input->post('gst_no'),
				'name_kh' => $this->input->post('name_kh'),
				'company_kh' => $this->input->post('company_kh'),
                'address_kh' => $this->input->post('address_kh'),
                'state_kh' => $this->input->post('state_kh'),
				'city_kh' => $this->input->post('city_kh'),
                'country' => $this->input->post('country_kh'),
                'sale_person_id' => $this->input->post('sale_person_id'),
				'payment_category' => $this->input->post('payment_category'),
				'warehouse_id' => $this->input->post('warehouse'),
                'add_ons' => $add_ons,
            );
        } elseif ($this->input->post('add_customer')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('customers');
        }
        
        if ($this->form_validation->run() == true && $cid = $this->companies_model->addCompany($data)) {            
            $this->session->set_flashdata('message', lang("customer_added"));
            admin_redirect('customers');
        } else {
			$this->data['sales_person'] = $this->companies_model->getAllSalePerson();
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['customer_groups'] = $this->companies_model->getAllCustomerGroups();
            $this->data['price_groups'] = $this->companies_model->getAllPriceGroups();
			$this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->load->view($this->theme . 'customers/add', $this->data);
        }
    }

    function edit($id = NULL)
    {
        $this->sma->checkPermissions(false, true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $company_details = $this->companies_model->getCompanyByID($id);
        if ($this->input->post('email') != $company_details->email) {
            $this->form_validation->set_rules('code', lang("email_address"), 'is_unique[companies.email]');
        }

        if ($this->form_validation->run('companies/add') == true) {
            $cg = $this->site->getCustomerGroupByID($this->input->post('customer_group'));
            $pg = $this->site->getPriceGroupByID($this->input->post('price_group'));
            $data = array('name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'group_id' => '3',
                'group_name' => 'customer',
                'customer_group_id' => $this->input->post('customer_group'),
                'customer_group_name' => $cg->name,
                'price_group_id' => $this->input->post('price_group') ? $this->input->post('price_group') : NULL,
                'price_group_name' => $this->input->post('price_group') ? $pg->name : NULL,
                'company' => $this->input->post('company'),
                'address' => $this->input->post('address'),
                'vat_no' => $this->input->post('vat_no'),
                'city' => $this->input->post('city'),
                'city_id' => $this->input->post('city_id'),
                'state' => $this->input->post('state'),
                'postal_code' => $this->input->post('postal_code'),
                'country' => $this->input->post('country'),
                'phone' => $this->input->post('phone'),
                'cf1' => $this->input->post('cf1'),
                'cf2' => $this->input->post('cf2'),
                'cf3' => $this->input->post('cf3'),
                'cf4' => $this->input->post('cf4'),
                'cf5' => $this->input->post('cf5'),
                'cf6' => $this->input->post('cf6'),
                'award_points' => $this->input->post('award_points'),
				'name_kh' => $this->input->post('name_kh'),
				'company_kh' => $this->input->post('company_kh'),
                'address_kh' => $this->input->post('address_kh'),
                'state_kh' => $this->input->post('state_kh'),
				'city_kh' => $this->input->post('city_kh'),
                'country' => $this->input->post('country_kh'),
				'sale_person_id' => $this->input->post('sale_person_id'),
				'payment_category' => $this->input->post('payment_category'),
				'warehouse_id' => $this->input->post('warehouse'),
            );
        } elseif ($this->input->post('edit_customer')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->companies_model->updateCompany($id, $data)) {
            $opening_date = $this->input->post('add_ons_opening_date');
                if ($opening_date == ''){
                   $opening_date = date('Y-m-d H:i:s');
                }
                else{
                    $opening_date = $this->sma->fld(trim($this->input->post('add_ons_opening_date').':'.date('s')));
                    $_POST['add_ons_opening_date'] = $opening_date;
                } 

                $birthday = $this->input->post('add_ons_birth_day');
                if ($birthday == '') 
                    $birthday = date('Y-m-d H:i:s');
                else{
                    $birthday = $this->sma->fld(trim($this->input->post('add_ons_birth_day').':'.date('s')));
                    $_POST['add_ons_birth_day'] = $birthday;
                }

                $payment_due_date = $this->input->post('add_ons_payment_due_date');
                if ($payment_due_date == '')
                    $payment_due_date = date('Y-m-d H:i:s');
                else {
                    $payment_due_date = $this->sma->fld(trim($this->input->post('add_ons_payment_due_date').':'.date('s')));
                    $_POST['add_ons_payment_due_date'] = $payment_due_date; 
                }

                $_POST['add_ons_customer_paid_by'] = $this->input->post('add_ons_customer_paid_by');
                
    
          // check national ID for customer      
                $config_data = array(
                    'table_name' => 'sma_companies',
                    'select_table' => 'sma_companies',
                    'translate' => '',
                    'select_condition' => "id > 0 and id != ".$id." order by id desc ",
                );
                $temp_national_id = $this->site->api_select_data_v2($config_data);
                $national_id = $this->input->post('add_ons_national_id');
                if($national_id == '')
                    $_POST['add_ons_national_id'] = '';
                else {
                    for($i=0;$i<count($temp_national_id);$i++){
                        if(strtolower(trim($temp_national_id[$i]['national_id'])) == strtolower(trim($national_id))) {   
                            $this->session->set_flashdata('error', lang("National ID : ".$national_id." is already existed"));
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                    } 
                }
                
            // check passport ID for customer 
                $config_data = array(
                    'table_name' => 'sma_companies',
                    'select_table' => 'sma_companies',
                    'translate' => '',
                    'select_condition' => "id > 0 and id != ".$id." order by id desc ",
                );
                $temp_passport_id = $this->site->api_select_data_v2($config_data);
                $passport_id = $this->input->post('add_ons_passport_id');
                if($passport_id == '')
                    $_POST['add_ons_passport_id'] = '';
                else {
                    for($j=0;$j<count($temp_passport_id);$j++){
                        if(strtolower(trim($temp_passport_id[$j]['passport_id'])) == strtolower(trim($passport_id))) {   
                            $this->session->set_flashdata('error', lang("Passport ID : ".$passport_id." is already existed"));
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                    } 
                }

                $facebook = $this->input->post('add_ons_facebook');
                $telegram = $this->input->post('add_ons_telegram');
                $instagram = $this->input->post('add_ons_instagram');
                $google = $this->input->post('add_ons_google');
                
            foreach($_POST as $name => $value) {
                if (is_int(strpos($name,"add_ons_"))) {
                    $value = $this->input->post($name);
                    $temp_name = str_replace('add_ons_','',$name);                        
                    $config_data = array(
                        'table_name' => 'sma_companies',
                        'id_name' => 'id',
                        'field_add_ons_name' => 'add_ons',
                        'selected_id' => $id,
                        'add_ons_title' => $temp_name,
                        'add_ons_value' => $value,                    
                    );
                    $this->site->api_update_add_ons_field($config_data);                        
                }
            }                            
            $this->session->set_flashdata('message', lang("customer_updated"));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
			$this->data['sales_person'] = $this->companies_model->getAllSalePerson();
            $this->data['customer'] = $this->companies_model->getCompanyByID_v2($id);
            
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['customer_groups'] = $this->companies_model->getAllCustomerGroups();
            $this->data['price_groups'] = $this->companies_model->getAllPriceGroups();
			$this->data['warehouses'] = $this->site->getAllWarehouses();

            $this->load->view($this->theme . 'customers/edit', $this->data);                        
        }
    }

    function users($company_id = NULL)
    {
        $this->sma->checkPermissions(false, true);

        if ($this->input->get('id')) {
            $company_id = $this->input->get('id');
        }


        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['modal_js'] = $this->site->modal_js();
        $this->data['company'] = $this->companies_model->getCompanyByID($company_id);
        $this->data['users'] = $this->companies_model->getCompanyUsers($company_id);
        $this->load->view($this->theme . 'customers/users', $this->data);

    }

    function add_user($company_id = NULL)
    {
        $this->sma->checkPermissions(false, true);
        $this->load->helper('generate_password');

        if ($this->input->get('id')) {
            $company_id = $this->input->get('id');
        }
        $company = $this->companies_model->getCompanyByID($company_id);

        $this->form_validation->set_rules('username', lang("username"), 'trim|is_unique[users.username]');
        $this->form_validation->set_rules('email', lang("email_address"), 'is_unique[users.email]');
        $this->form_validation->set_rules('password', lang('password'), 'required|min_length[8]|max_length[20]|matches[password_confirm]');
        $this->form_validation->set_rules('password_confirm', lang('confirm_password'), 'required');

        if ($this->form_validation->run('companies/add_user') == true) {
            $active = $this->input->post('status');
            $notify = $this->input->post('notify');
            //list($username, $domain) = explode("@", $this->input->post('email'));
            $username = trim($this->input->post('username'));
            $email = strtolower($this->input->post('email'));
            $password = $this->input->post('password');
            $additional_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'phone' => $this->input->post('phone'),
                'gender' => $this->input->post('gender'),
                'company_id' => $company->id,
                'company' => $company->company,
                'group_id' => 3
            );
            $this->load->library('ion_auth');
        } elseif ($this->input->post('add_user')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('customers');
        }

        if ($this->form_validation->run() == true && $this->ion_auth->register($username, $password, $email, $additional_data, $active, $notify)) {
            $this->session->set_flashdata('message', lang("user_added"));
            admin_redirect("customers");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['company'] = $company;
            $this->data['random_password'] = randomPassword(12, false);
            $this->load->view($this->theme . 'customers/add_user', $this->data);
        }
    }

    function import_csv()
    {
        $this->sma->checkPermissions('add', true);
        $this->load->helper('security');
        $this->form_validation->set_rules('csv_file', lang("upload_file"), 'xss_clean');

        if ($this->form_validation->run() == true) {

            if (DEMO) {
                $this->session->set_flashdata('warning', lang("disabled_in_demo"));
                redirect($_SERVER["HTTP_REFERER"]);
            }

            if (isset($_FILES["csv_file"])) {

                $this->load->library('upload');

                $config['upload_path'] = 'files/';
                $config['allowed_types'] = 'csv';
                $config['max_size'] = '2000';
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;

                $this->upload->initialize($config);

                if (!$this->upload->do_upload('csv_file')) {

                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("customers");
                }

                $csv = $this->upload->file_name;

                $arrResult = array();
                $handle = fopen("files/" . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 5001, ",")) !== FALSE) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);
                $rw = 2;
                $updated = '';
                $data = array();
                $customer_group = $this->site->getCustomerGroupByID($this->Settings->customer_group);
                $price_group = $this->site->getPriceGroupByID($this->Settings->price_group);
                foreach ($arrResult as $key => $value) {
                    $customer = [
                        'company'       => isset($value[0]) ? trim($value[0]) : '',
                        'name'          => isset($value[1]) ? trim($value[1]) : '',
                        'email'         => isset($value[2]) ? trim($value[2]) : '',
                        'phone'         => isset($value[3]) ? trim($value[3]) : '',
                        'address'       => isset($value[4]) ? trim($value[4]) : '',
                        'city'          => isset($value[5]) ? trim($value[5]) : '',
                        'state'         => isset($value[6]) ? trim($value[6]) : '',
                        'postal_code'   => isset($value[7]) ? trim($value[7]) : '',
                        'country'       => isset($value[8]) ? trim($value[8]) : '',
                        'vat_no'        => isset($value[9]) ? trim($value[9]) : '',
                        'gst_no'        => isset($value[10]) ? trim($value[10]) : '',
                        'cf1'           => isset($value[11]) ? trim($value[11]) : '',
                        'cf2'           => isset($value[12]) ? trim($value[12]) : '',
                        'cf3'           => isset($value[13]) ? trim($value[13]) : '',
                        'cf4'           => isset($value[14]) ? trim($value[14]) : '',
                        'cf5'           => isset($value[15]) ? trim($value[15]) : '',
                        'cf6'           => isset($value[16]) ? trim($value[16]) : '',
                        'group_id'      => 3,
                        'group_name'    => 'customer',
                        'customer_group_id' => (!empty($customer_group)) ? $customer_group->id : NULL,
                        'customer_group_name' => (!empty($customer_group)) ? $customer_group->name : NULL,
                        'price_group_id' => (!empty($price_group)) ? $price_group->id : NULL,
                        'price_group_name' => (!empty($price_group)) ? $price_group->name : NULL,
                    ];
                    if (empty($customer['company']) || empty($customer['name']) || empty($customer['email'])) {
                        $this->session->set_flashdata('error', lang('company').', '.lang('name').', '.lang('email').' '.lang('are_required'). ' (' . lang('line_no') . ' ' . $rw . ')');
                        admin_redirect("customers");
                    } else {
                        if ($this->Settings->indian_gst && empty($customer['state'])) {
                            $this->session->set_flashdata('error', lang('state').' '.lang('is_required'). ' (' . lang('line_no') . ' ' . $rw . ')');
                            admin_redirect("customers");
                        }
                        if ($customer_details = $this->companies_model->getCompanyByEmail($customer['email'])) {
                            if ($customer_details->group_id == 3) {
                                $updated .= '<p>'.lang('customer_updated').' ('.$customer['email'].')</p>';
                                $this->companies_model->updateCompany($customer_details->id, $customer);
                            }
                        } else {
                            $data[] = $customer;
                        }
                        $rw++;
                    }
                }

                // $this->sma->print_arrays($data, $updated);
            }

        } elseif ($this->input->post('import')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('customers');
        }

        if ($this->form_validation->run() == true && !empty($data)) {
            if ($this->companies_model->addCompanies($data)) {
                $this->session->set_flashdata('message', lang("customers_added").$updated);
                admin_redirect('customers');
            }
        } else {
            if (isset($data) && empty($data)) {
                if ($updated) {
                    $this->session->set_flashdata('message', $updated);
                } else {
                    $this->session->set_flashdata('warning', lang('data_x_customers'));
                }
                admin_redirect('customers');
            }

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'customers/import', $this->data);
        }
    }

    function delete($id = NULL)
    {
        $this->sma->checkPermissions(NULL, TRUE);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->input->get('id') == 1) {
            $this->session->set_flashdata('error', lang("customer_x_deleted"));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $temp = $this->site->api_select_some_fields_with_where("
            id
            "
            ,"sma_sales"
            ,"customer_id = ".$id." limit 1"
            ,"arr"
        );
        if (count($temp) > 0) {
            $this->session->set_flashdata('error', lang("Cannot_delete. This_customer_has_sale."));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        else {
            $this->db->delete('sma_companies', 'id = '.$id);
            $this->session->set_flashdata('message', lang("customer_deleted"));
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function suggestions($term = NULL, $limit = NULL, $a = NULL)
    {
        // $this->sma->checkPermissions('index');
        if ($this->input->get('term')) {
            $term = $this->input->get('term', TRUE);
        }
        if (strlen($term) < 1) {
            return FALSE;
        }
        $limit = $this->input->get('limit', TRUE);
        //$result = $this->companies_model->getCustomerSuggestions($term, $limit);

        $condition = " and name != '' and group_id = 3 and (name LIKE '%" . $term . "%' OR company LIKE '%" . $term . "%') and add_ons like '%:closed:{}:%'";

        $config_data = array(
            'table_name' => 'sma_companies',
            'select_table' => 'sma_companies',
            'translate' => '',
            'select_condition' => "parent_id = 0 ".$condition." order by company asc limit 20",
        );      
        $select_customer = $this->site->api_select_data_v2($config_data);
        $k = 0;
        for ($i=0;$i<count($select_customer);$i++) {
            foreach(array_keys($select_customer[$i]) as $key){
                if ($key == 'id') {
                    $result[$k]->{'id'} = $select_customer[$i][$key];
                }                
                if ($key == 'company') {
                    $result[$k]->{'text'} = trim($select_customer[$i][$key]).' [Name: '.$select_customer[$i]['name'].']';
                    $result[$k]->{'value'} = trim($select_customer[$i][$key]);
                }
                if ($key == 'phone')
                    $result[$k]->{'phone'} = trim($select_customer[$i][$key]);                
            }
            $config_data = array(
                'table_name' => 'sma_companies',
                'select_table' => 'sma_companies',
                'translate' => '',
                'select_condition' => "parent_id = ".$select_customer[$i]['id']." order by company asc",
            );      
            $temp = $this->site->api_select_data_v2($config_data);
            for ($i2=0;$i2<count($temp);$i2++) {
                $k++;
                foreach(array_keys($temp[$i2]) as $key){
                    if ($key == 'id') {
                        $result[$k]->{'id'} = $temp[$i2][$key];
                    }                
                    if ($key == 'company') {
                        $result[$k]->{'text'} = ':....'.trim($temp[$i2][$key]).' [Name: '.$select_customer[$i]['name'].']';
                        $result[$k]->{'value'} = trim($temp[$i2][$key]);
                    }
                    if ($key == 'phone')
                        $result[$k]->{'phone'} = trim($temp[$i2][$key]);                
                }                
            }
            $k++;
        }

        if ($a) {
            $this->sma->send_json($result);
        }

        $rows['results'] = $result;
        $this->sma->send_json($rows);
    }

    function getCustomer($id = NULL)
    {
        // $this->sma->checkPermissions('index');
        $row = $this->companies_model->getCompanyByID($id);
        $this->sma->send_json(array(array('id' => $row->id, 'text' => ($row->company != '-' ? $row->company : $row->name))));
    }

    function get_customer_details($id = NULL)
    {
        $this->sma->send_json($this->companies_model->getCompanyByID($id));
    }

    function get_award_points($id = NULL)
    {
        $this->sma->checkPermissions('index');
        $row = $this->companies_model->getCompanyByID($id);
        $this->sma->send_json(array('ca_points' => $row->award_points));
    }

    function customer_actions()
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('api_action', lang("api_action"), 'required');

        if ($this->form_validation->run() == true) {
            if (!empty($_POST['check_value'])) {
                $temp_val = explode('-',$_POST['check_value']);
                if ($this->input->post('api_action') == 'delete') {
                    $this->sma->checkPermissions('delete');
                    $b = 0;
                    foreach ($temp_val as $id) {
                        if ($id != '') {
                            $temp = $this->site->api_select_some_fields_with_where("
                                id
                                "
                                ,"sma_sales"
                                ,"customer_id = ".$id." limit 1"
                                ,"arr"
                            );
                            if (count($temp) > 0)
                                $b = 1;
                            else
                                $this->db->delete('sma_companies', 'id = '.$id);
                        }
                    }
                    if ($b == 1)
                        $this->session->set_flashdata('warning', lang('customers_x_deleted_have_sales'));
                    else
                        $this->session->set_flashdata('message', lang("customers_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('api_action') == 'export_excel') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('customer'));

                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('company'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('Company_KH'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('name'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('Name_KH'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('email'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('phone'));
                    $this->excel->getActiveSheet()->SetCellValue('G1', lang('address'));
                    $this->excel->getActiveSheet()->SetCellValue('H1', lang('Address_KH'));
                    $this->excel->getActiveSheet()->SetCellValue('I1', lang('city'));
                    $this->excel->getActiveSheet()->SetCellValue('J1', lang('city_kh'));
                    $this->excel->getActiveSheet()->SetCellValue('K1', lang('state'));
                    $this->excel->getActiveSheet()->SetCellValue('L1', lang('state_kh'));
                    $this->excel->getActiveSheet()->SetCellValue('M1', lang('country'));
                    $this->excel->getActiveSheet()->SetCellValue('N1', lang('country_kh'));
                    $this->excel->getActiveSheet()->SetCellValue('O1', lang('postal_code'));
                    $this->excel->getActiveSheet()->SetCellValue('P1', lang('vat_no'));
                    $this->excel->getActiveSheet()->SetCellValue('Q1', lang('gst_no'));
                    $this->excel->getActiveSheet()->SetCellValue('R1', lang('ccf1'));
                    $this->excel->getActiveSheet()->SetCellValue('S1', lang('ccf2'));
                    $this->excel->getActiveSheet()->SetCellValue('T1', lang('ccf3'));
                    $this->excel->getActiveSheet()->SetCellValue('U1', lang('ccf4'));
                    $this->excel->getActiveSheet()->SetCellValue('V1', lang('ccf5'));
                    $this->excel->getActiveSheet()->SetCellValue('W1', lang('ccf6'));
                    $this->excel->getActiveSheet()->SetCellValue('X1', lang('deposit_amount'));
                    $this->excel->getActiveSheet()->SetCellValue('Y1', lang('Customer_Group'));

                    $row = 2;
                    foreach ($temp_val as $id) {
                        if ($id != '') {
                            $customer = $this->site->getCompanyByID($id);
                            $this->excel->getActiveSheet()->SetCellValue('A' . $row, $customer->company);
                            $this->excel->getActiveSheet()->SetCellValue('B' . $row, $customer->company_kh);
                            $this->excel->getActiveSheet()->SetCellValue('C' . $row, $customer->name);
                            $this->excel->getActiveSheet()->SetCellValue('D' . $row, $customer->name_kh);
                            $this->excel->getActiveSheet()->SetCellValue('E' . $row, $customer->email);
                            $this->excel->getActiveSheet()->SetCellValue('F' . $row, $customer->phone);
                            $this->excel->getActiveSheet()->SetCellValue('G' . $row, $customer->address);
                            $this->excel->getActiveSheet()->SetCellValue('H' . $row, $customer->address_kh);
                            $this->excel->getActiveSheet()->SetCellValue('I' . $row, $customer->city);
                            $this->excel->getActiveSheet()->SetCellValue('J' . $row, $customer->city_kh);
                            $this->excel->getActiveSheet()->SetCellValue('K' . $row, $customer->state);
                            $this->excel->getActiveSheet()->SetCellValue('L' . $row, $customer->state_kh);
                            $this->excel->getActiveSheet()->SetCellValue('M' . $row, $customer->country);
                            $this->excel->getActiveSheet()->SetCellValue('N' . $row, $customer->country_kh);
                            $this->excel->getActiveSheet()->SetCellValue('O' . $row, $customer->postal_code);
                            $this->excel->getActiveSheet()->SetCellValue('P' . $row, $customer->vat_no);
                            $this->excel->getActiveSheet()->SetCellValue('Q' . $row, $customer->gst_no);
                            $this->excel->getActiveSheet()->SetCellValue('R' . $row, $customer->cf1);
                            $this->excel->getActiveSheet()->SetCellValue('S' . $row, $customer->cf2);
                            $this->excel->getActiveSheet()->SetCellValue('T' . $row, $customer->cf3);
                            $this->excel->getActiveSheet()->SetCellValue('U' . $row, $customer->cf4);
                            $this->excel->getActiveSheet()->SetCellValue('V' . $row, $customer->cf5);
                            $this->excel->getActiveSheet()->SetCellValue('W' . $row, $customer->cf6);
                            $this->excel->getActiveSheet()->SetCellValue('X' . $row, $customer->deposit_amount);

                            $temp2 = '';
                            if ($customer->customer_group_id > 0) {                     
                                $temp = $this->site->api_select_some_fields_with_where("
                                    *     
                                    "
                                    ,"sma_customer_groups"
                                    ,"id = ".$customer->customer_group_id
                                    ,"arr"
                                );
                                $temp2 = $temp[0]['name'].' ('.$temp[0]['percent'].')';
                            }

                            $this->excel->getActiveSheet()->SetCellValue('Y' . $row, $temp2);

                            $row++;
                        }
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('Y')->setWidth(30);
                    $this->excel->getActiveSheet()->getStyle('A1:X'.$row)->getAlignment()->setWrapText(true);
                    $this->excel->getActiveSheet()->getStyle('A1:Y1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $this->excel->getActiveSheet()->getStyle('A1:Y1')->getFont()->setBold(true);
                    $this->excel->getActiveSheet()->getStyle('A2:X'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                    $this->excel->getActiveSheet()->getStyle('F2:F'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

                    $filename = 'customers_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_customer_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function deposits($company_id = NULL)
    {
        $this->sma->checkPermissions(false, true);

        if ($this->input->get('id')) {
            $company_id = $this->input->get('id');
        }

        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['modal_js'] = $this->site->modal_js();
        $this->data['company'] = $this->companies_model->getCompanyByID($company_id);
        $this->load->view($this->theme . 'customers/deposits', $this->data);

    }

    function get_deposits($company_id = NULL)
    {
        $this->sma->checkPermissions('deposits');
        $this->load->library('datatables');
        $this->datatables
            ->select("deposits.id as id, date, amount, paid_by, CONCAT({$this->db->dbprefix('users')}.first_name, ' ', {$this->db->dbprefix('users')}.last_name) as created_by", false)
            ->from("deposits")
            ->join('users', 'users.id=deposits.created_by', 'left')
            ->where($this->db->dbprefix('deposits').'.company_id', $company_id)
            ->add_column("Actions", "<div class=\"text-center\"><a class=\"tip\" title='" . lang("deposit_note") . "' href='" . admin_url('customers/deposit_note/$1') . "' data-toggle='modal' data-target='#myModal2'><i class=\"fa fa-file-text-o\"></i></a> <a class=\"tip\" title='" . lang("edit_deposit") . "' href='" . admin_url('customers/edit_deposit/$1') . "' data-toggle='modal' data-target='#myModal2'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("delete_deposit") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('customers/delete_deposit/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id")
        ->unset_column('id');
        echo $this->datatables->generate();
    }

    function add_deposit($company_id = NULL)
    {
        $this->sma->checkPermissions('deposits', true);

        if ($this->input->get('id')) {
            $company_id = $this->input->get('id');
        }
        $company = $this->companies_model->getCompanyByID($company_id);

        if ($this->Owner || $this->Admin) {
            $this->form_validation->set_rules('date', lang("date"), 'required');
        }
        $this->form_validation->set_rules('amount', lang("amount"), 'required|numeric');

        if ($this->form_validation->run() == true) {

            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }
            $data = array(
                'date' => $date,
                'amount' => $this->input->post('amount'),
                'paid_by' => $this->input->post('paid_by'),
                'note' => $this->input->post('note'),
                'company_id' => $company->id,
                'created_by' => $this->session->userdata('user_id'),
            );

            $cdata = array(
                'deposit_amount' => ($company->deposit_amount+$this->input->post('amount'))
            );

        } elseif ($this->input->post('add_deposit')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('customers');
        }

        if ($this->form_validation->run() == true && $this->companies_model->addDeposit($data, $cdata)) {
            $this->session->set_flashdata('message', lang("deposit_added"));
            admin_redirect("customers");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['company'] = $company;
            $this->load->view($this->theme . 'customers/add_deposit', $this->data);
        }
    }

    function edit_deposit($id = NULL)
    {
        $this->sma->checkPermissions('deposits', true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $deposit = $this->companies_model->getDepositByID($id);
        $company = $this->companies_model->getCompanyByID($deposit->company_id);

        if ($this->Owner || $this->Admin) {
            $this->form_validation->set_rules('date', lang("date"), 'required');
        }
        $this->form_validation->set_rules('amount', lang("amount"), 'required|numeric');

        if ($this->form_validation->run() == true) {

            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = $deposit->date;
            }
            $data = array(
                'date' => $date,
                'amount' => $this->input->post('amount'),
                'paid_by' => $this->input->post('paid_by'),
                'note' => $this->input->post('note'),
                'company_id' => $deposit->company_id,
                'updated_by' => $this->session->userdata('user_id'),
                'updated_at' => $date = date('Y-m-d H:i:s'),
            );

            $cdata = array(
                'deposit_amount' => (($company->deposit_amount-$deposit->amount)+$this->input->post('amount'))
            );

        } elseif ($this->input->post('edit_deposit')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('customers');
        }

        if ($this->form_validation->run() == true && $this->companies_model->updateDeposit($id, $data, $cdata)) {
            $this->session->set_flashdata('message', lang("deposit_updated"));
            admin_redirect("customers");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();

            $this->data['company'] = $company;
            $this->data['deposit'] = $deposit;
            $this->load->view($this->theme . 'customers/edit_deposit', $this->data);
        }
    }

    public function delete_deposit($id)
    {
        $this->sma->checkPermissions(NULL, TRUE);

        if ($this->companies_model->deleteDeposit($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("deposit_deleted")));
        }
    }

    public function deposit_note($id = null)
    {
        $this->sma->checkPermissions('deposits', true);
        $deposit = $this->companies_model->getDepositByID($id);
        $this->data['customer'] = $this->companies_model->getCompanyByID($deposit->company_id);
        $this->data['deposit'] = $deposit;
        $this->data['page_title'] = $this->lang->line("deposit_note");
        $this->load->view($this->theme . 'customers/deposit_note', $this->data);
    }

    function addresses($company_id = NULL)
    {
        $this->sma->checkPermissions('index', true);
        $this->data['modal_js'] = $this->site->modal_js();
        $this->data['company'] = $this->companies_model->getCompanyByID($company_id);
        $this->data['addresses'] = $this->companies_model->getCompanyAddresses($company_id);
        $this->load->view($this->theme . 'customers/addresses', $this->data);

    }

    function add_address($company_id = NULL)
    {
        $this->sma->checkPermissions('add', true);
        $company = $this->companies_model->getCompanyByID($company_id);

        $this->form_validation->set_rules('line1', lang("line1"), 'required');
        $this->form_validation->set_rules('city', lang("city"), 'required');
        $this->form_validation->set_rules('state', lang("state"), 'required');
        $this->form_validation->set_rules('country', lang("country"), 'required');
        $this->form_validation->set_rules('phone', lang("phone"), 'required');

        if ($this->form_validation->run() == true) {

            $data = array(
                'line1' => $this->input->post('line1'),
                'line2' => $this->input->post('line2'),
                'city' => $this->input->post('city'),
                'postal_code' => $this->input->post('postal_code'),
                'state' => $this->input->post('state'),
                'country' => $this->input->post('country'),
                'phone' => $this->input->post('phone'),
                'company_id' => $company->id,
            );

        } elseif ($this->input->post('add_address')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('customers');
        }

        if ($this->form_validation->run() == true && $this->companies_model->addAddress($data)) {
            $this->session->set_flashdata('message', lang("address_added"));
            admin_redirect("customers");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['company'] = $company;
            $this->load->view($this->theme . 'customers/add_address', $this->data);
        }
    }

    function edit_address($id = NULL)
    {
        $this->sma->checkPermissions('edit', true);

        $this->form_validation->set_rules('line1', lang("line1"), 'required');
        $this->form_validation->set_rules('city', lang("city"), 'required');
        $this->form_validation->set_rules('state', lang("state"), 'required');
        $this->form_validation->set_rules('country', lang("country"), 'required');
        $this->form_validation->set_rules('phone', lang("phone"), 'required');

        if ($this->form_validation->run() == true) {

            $data = array(
                'line1' => $this->input->post('line1'),
                'line2' => $this->input->post('line2'),
                'city' => $this->input->post('city'),
                'postal_code' => $this->input->post('postal_code'),
                'state' => $this->input->post('state'),
                'country' => $this->input->post('country'),
                'phone' => $this->input->post('phone'),
                'updated_at' => date('Y-m-d H:i:s'),
            );

        } elseif ($this->input->post('edit_address')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('customers');
        }

        if ($this->form_validation->run() == true && $this->companies_model->updateAddress($id, $data)) {
            $this->session->set_flashdata('message', lang("address_updated"));
            admin_redirect("customers");
        } else {

            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['address'] = $this->companies_model->getAddressByID($id);
            $this->load->view($this->theme . 'customers/edit_address', $this->data);
        }
    }

    public function delete_address($id)
    {
        $this->sma->checkPermissions('delete', TRUE);

        if ($this->companies_model->deleteAddress($id)) {
            $this->session->set_flashdata('message', lang("address_deleted"));
            admin_redirect("customers");
        }
    }

    function company_branch($id = null)
    {
        $this->sma->checkPermissions();

        $sort_by = $this->input->post('sort_by');
        $sort_order = $this->input->post('sort_order');

        $per_page = $this->input->post('per_page') != '' ? $this->input->post('per_page') : 100;
        $page = $this->input->get('page') != '' ? $this->input->get('page') : 1;

        $offset_no = $page != '' ? ($page - 1 ) * $per_page : $offset_no;
        
        if ($page != '')
            $offset_no = ($page - 1 ) * $per_page;
        else
            $offset_no = 0;


        $search = $this->input->post('search');

        $config_data = array(
            'lg' => 'en',
            'table_name' => 'sma_company_branch',
            'table_id' => 'id',
            'search' => $search,
            'sort_by' => $sort_by,
            'sort_order' => $sort_order,
            'limit' => $per_page,            
            'offset_no' => $offset_no,
            'company' => $this->input->post('company'),
        );
        $select_data = $this->companies_model->getCompanyBranch($config_data);

        $config_data = array(
            'lg' => 'en',
            'table_name' => 'sma_company_branch',
            'table_id' => 'id',
            'search' => $search,
            'sort_by' => '',
            'sort_order' => '',
            'limit' => '',            
            'offset_no' => '',
            'company' => $this->input->post('company'),
        );
        $temp_count = $this->companies_model->getCompanyBranch($config_data);
        $total_record = count($temp_count);

        /* config pagination */
        $this->data['select_data'] = $select_data;
        $this->data['per_page'] = $per_page;
        $this->data['sort_by'] = $sort_by;
        $this->data['sort_order'] = $sort_order;
        $this->data['total_rows_sale'] = $total_record;
        $this->data['index_page'] = 'admin/customers/company_branch';
        $this->data['show_data'] = $page;

        $config_base_url = site_url() . 'admin/customers/company_branch'; //your url where the content is displayed
        $config['base_url'] = $config_base_url; //your url where the content is displayed
        $config['query_string_segment'] = 'page';
        $config['per_page'] = $per_page; //$per_page;
        $config['num_links'] = 5;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['full_tag_open'] = '<ul class="pagination pagination-new-bg">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = FALSE;
        $config['last_link'] = FALSE;
        $config['prev_link'] = '&lt; Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next &gt;';
        $config['next_tag_open'] = '<li class="next">';
        $config['next_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['total_rows'] = $total_record; //$this->data['total_rows_sale'];        
        $this->load->library('pagination');   
        $this->pagination->initialize($config);
        /** end config pagination */
        /**Showing Label Data At Table Footer */
        $showing = $total_record != '' ? 1 : 0;
        $label_show_to= ($page == '' || $per_page < $total_record) ?  $per_page  :  $total_record;
        $label_show = "Showing" . $showing . " to ".$label_show_to." of : " . number_format($total_record) . " entries";

        $show_page = $page > 1 ? ($page * $per_page) : $total_record;
        $show_page_no = $show_page > $total_record ? $total_record : $page * $per_page;
        $label_show_page = "Showing " . (($page - 1) * ($per_page) + 1) . " to " . $show_page_no . " (filtered from " . number_format($total_record) . " total entries) ";
        $this->data['show'] = $search != '' || $page > 1 ? $label_show_page : $label_show;
        /** End Showing Label Data At Table Footer */ 
        $temp = $this->site->api_select_some_fields_with_where("*
            "
            ,"sma_companies"
            ,"id > 0 order by company asc"
            ,"obj"
        );
        $this->data['company'] = $temp;            

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('Company_Branch')));
        $meta = array('page_title' => lang('Branch'), 'bc' => $bc);

        $this->page_construct('customers/company_branch', $meta, $this->data);
    }

    function company_branch_edit($id = null)
    {
        $this->sma->checkPermissions(false, true);
        $select_data = array();
        if ($id > 0) {
            $config_data = array(
                'lg' => 'en',
                'table_name' => 'sma_company_branch',
                'table_id' => 'id',
                'selected_id' => $id,
            );
            $select_data = $this->companies_model->getCompanyBranch($config_data);
        }

        $this->form_validation->set_rules('branch_name', lang("branch_name"), 'trim|required');
        $this->form_validation->set_rules('company', lang("company"), 'trim|required');
        $this->form_validation->set_rules('address', lang("address"), 'trim|required');
        
        if ($this->form_validation->run() == true) {            
            $data = array(
                'translate' => 'en:{'.$this->input->post('branch_name').'}:',
                'parent_id' => $this->input->post('company'),
                'add_ons' => 'initial_first_add_ons:{}:branch_number:{'.$this->input->post('branch_number').'}:address:{'.$this->input->post('address').'}:',
            );
            if ($select_data[0]['id'] > 0) {
                $this->site->api_update_table('sma_company_branch',$data,"id = ".$select_data[0]['id']);  
                $this->session->set_flashdata('message', lang("branch").' '.lang("updated"));
            }
            else {
                $this->companies_model->addCompanyBranch($data);            
                $this->session->set_flashdata('message', lang("branch").' '.lang("added"));                
            }
            
            admin_redirect('customers/company_branch');
        }
        else {            
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();

            $temp = $this->site->api_select_some_fields_with_where("*
                "
                ,"sma_companies"
                ,"id > 0 order by company asc"
                ,"obj"
            );
            $this->data['company'] = $temp;            
            $this->data['select_data'] = $select_data;
            $this->load->view($this->theme . 'customers/company_branch_edit', $this->data);
        }
        
    }

    function company_branch_delete($id = null)
    {
        $this->sma->checkPermissions(null, true);
        if ($id > 0)
            $this->db->delete('sma_company_branch', array('id' => $id));
        $this->session->set_flashdata('message', lang('branch').' '.lang('deleted'));
        redirect($_SERVER["HTTP_REFERER"]);
    }
    public function company_branch_actions()
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {
            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {

                    $this->sma->checkPermissions('delete');
                    $j = 0;
                    foreach ($_POST['val'] as $id) {
                        $this->db->delete('sma_company_branch', array('id' => $id));
                        $j++;
                    }
                    $this->session->set_flashdata('message', $j.' '.lang("branch").' '.lang("deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);

                } 
            } else {
                $this->session->set_flashdata('error', lang("no").' '.lang("branch").' '.lang("selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function add_branch($id = null)
    {        
        $this->form_validation->set_rules('company_id', lang("Company"), 'required');

        if ($this->form_validation->run() == true) {

            $company_id = $this->input->post('company_id');
            $temp = array(
                'parent_id' => $id
            );
            $this->db->update('sma_companies', $temp,'id = '.$company_id);
            // $this->sma->print_arrays($temp);
            $this->session->set_flashdata('message', lang("Branch_added"));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();

            $select_data = $this->site->api_select_some_fields_with_where("
                *
                "
                ,"sma_companies"
                ,"id = ".$id
                ,"arr"
            );
    
            $this->data['api_parent_company'] = $temp;

            $temp = $this->site->api_select_some_fields_with_where("
                id, company, parent_id
                "
                ,"sma_companies"
                ,"parent_id = 0 and id != ".$id." and group_id = 3 order by company asc"
                ,"arr"
            );
            $temp_3 = array();
            $j = 0;
            for ($i=0;$i<count($temp);$i++) {
                $temp2 = $this->site->api_select_some_fields_with_where("
                    id
                    "
                    ,"sma_companies"
                    ,"parent_id = ".$temp[$i]['id']." limit 1"
                    ,"arr"
                );
                if (count($temp2) <= 0) {
                    $temp_3[$j] = $temp[$i];
                    $j++;
                }
            }

            $this->data['api_company'] = $temp_3;
            $this->data['select_data'] = $select_data;
            $this->load->view($this->theme . 'customers/add_branch', $this->data);
        }
    }

    function remove_branch($id = null)
    {        
        
        $temp = $this->input->post('check_value_remove_branch');
        
        if ($temp != '') {
            $temp_val = explode('-',$temp);
            
            foreach ($temp_val as $branch_id) {

                if ($branch_id != '') {
                    
                    $temp_2 = array(
                        'parent_id' => 0
                    );
                    $this->db->update('sma_companies', $temp_2,'id = '.$branch_id);
                    
                }
            }            

            $this->session->set_flashdata('message', lang("Branch_removed"));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();

            $select_data = $this->site->api_select_some_fields_with_where("
                *
                "
                ,"sma_companies"
                ,"id = ".$id
                ,"arr"
            );
    
            $this->data['api_parent_company'] = $temp;

            $temp = $this->site->api_select_some_fields_with_where("
                id, company, parent_id
                "
                ,"sma_companies"
                ,"parent_id = ".$id." order by company asc"
                ,"arr"
            );

            $this->data['api_company'] = $temp;
            $this->data['select_data'] = $select_data;
            $this->load->view($this->theme . 'customers/remove_branch', $this->data);
        }
    }
    function favorite_items($id = NULL) 
    {
        $this->form_validation->set_rules('products_id', lang("Products"), 'required');

        if ($this->form_validation->run() == true) {

            $products_id = $this->input->post('products_id');
            
            $config_data = array(
                'table_name' => 'sma_companies',
                'select_table' => 'sma_companies',
                'translate' => '',
                'select_condition' => "id = ".$id,
            );
            $select_companies = $this->site->api_select_data_v2($config_data);
            for($i=0;$i<count($select_companies);$i++) {
                if (is_int(strpos($select_companies[$i]['products_list'],'-'.$products_id.'-'))) {
                    $value = str_replace('-'.$products_id.'-','',$select_companies[$i].['products_list']);
                } else {
                    $value = '-'.$products_id.'-'.$select_companies[$i]['products_list'];
                }
            }
            $config_data = array(
                'table_name' => 'sma_companies',
                'id_name' => 'id',
                'field_add_ons_name' => 'add_ons',
                'selected_id' => $id,
                'add_ons_title' => 'products_list',
                'add_ons_value' => $value,                    
            );
            $this->site->api_update_add_ons_field($config_data);
            
            $this->session->set_flashdata('message', lang("Products added"));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();

            $select_data = $this->site->api_select_some_fields_with_where("
                *
                "
                ,"sma_companies"
                ,"id = ".$id
                ,"arr"
            );
            
            $config_data = array(
                'table_name' => 'sma_companies',
                'select_table' => 'sma_companies',
                'select_condition' => "id = ".$id,
            );
            $select_products_list = $this->site->api_select_data_v2($config_data);
            $explode_products_id = explode("-",$select_products_list[0]['products_list'],-1);
            
            $this->data['api_products'] = $temp;
            $this->data['explode_products_id'] = $explode_products_id;
            $this->data['select_data'] = $select_products_list;
            $this->load->view($this->theme . 'customers/favorite_items', $this->data);
        }
    }
    function remove_favorite_items($id = null)
    {        
        $temp = $this->input->post('check_value_remove_favorite_items');
        if ($temp != '') {
            $explode_products_id = explode('-',$temp);
            for($i=0;$i<count($explode_products_id);$i++) {
                if($explode_products_id[$i] != 0) {
                    $products_id = $products_id.'-'.$explode_products_id[$i].'-';
                }
            }
            $config_data = array(
                'table_name' => 'sma_companies',
                'select_table' => 'sma_companies',
                'select_condition' => "id = ".$id,
            );
            $select_products_id = $this->site->api_select_data_v2($config_data);   
            $value = str_replace($products_id,'',$select_products_id[0]['products_list']);
            $config_data = array(
                'table_name' => 'sma_companies',
                'id_name' => 'id',
                'field_add_ons_name' => 'add_ons',
                'selected_id' => $id,
                'add_ons_title' => 'products_list',
                'add_ons_value' => $value,                    
            );
            $this->site->api_update_add_ons_field($config_data);
            
            $this->session->set_flashdata('message', lang("Products removed"));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();

            $select_data = $this->site->api_select_some_fields_with_where("
                *
                "
                ,"sma_companies"
                ,"id = ".$id
                ,"arr"
            );
    
            $this->data['api_parent_company'] = $temp;

            $config_data = array(
                'table_name' => 'sma_companies',
                'select_table' => 'sma_companies',
                'select_condition' => "id = ".$id,
            );
            $select_products_list = $this->site->api_select_data_v2($config_data);
            $explode_products_id = explode("-",$select_products_list[0]['products_list'],-1);

            $temp = $this->site->api_select_some_fields_with_where("
                id, company, parent_id
                "
                ,"sma_companies"
                ,"parent_id = ".$id." order by company asc"
                ,"arr"
            );
            $this->data['api_company'] = $temp;
            $this->data['api_products_list'] = $select_products_list;
            $this->data['explode_products_id'] = $explode_products_id;
            $this->data['select_data'] = $select_data;
            $this->load->view($this->theme . 'customers/remove_favorite_items', $this->data);
        }
    }

}
