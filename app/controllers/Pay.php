<?php defined('BASEPATH') or exit('No direct script access allowed');

class Pay extends MY_Shop_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('pay_model');
    }

    public function index()
    {
        if (!SHOP) {
            redirect('admin');
        }
        redirect();
    }

    public function wing_requestData($requestUrl, $requestBody, $method)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $requestUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => $requestBody,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_HTTPHEADER => array("content-type: application/json"),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            $error = array(
                'errorCode' => 'ERROR',
                'errorText' => "cURL Error #:" . $err
            );
            return json_encode($error);
        } else {
            return $response;
        }
    }

    public function wing_return()
    {
        if ($_GET['status'] == 'success') {
            $id = 0;
            if ($this->session->userdata('company_id') != '') {
                $temp = $this->site->api_select_some_fields_with_where(
                    "
                    id
                    ",
                    "sma_sales",
                    "customer_id = ".$this->session->userdata('company_id')." and created_by = ".$this->session->userdata('user_id')." order by id desc limit 1",
                    "arr"
                );
                if (is_array($temp)) {
                    if (count($temp) > 0) {
                        $id = $temp[0]['id'];
                    }
                }
            }

            if (base_url() != 'https://air.phsarjapan.com/') {
                $data = array(
                    'username' => $this->api_shop_setting[0]['wing_username'],
                    'rest_api_key' => $this->api_shop_setting[0]['wing_rest_api_key'],
                    'sandbox' => 0,
                    'remark' => $id,
                );
            } elseif (base_url() != 'https://demo.phsarjapan.com/') {
                $data = array(
                    'username' => "TESTING ENVIRONMENT",
                    'rest_api_key' => "https://onlinepayment-test.pipay.com/rest-api/verifyTransaction",
                    'sandbox' => 0,
                    'remark' => $id,
                );
            } else {
                $data = array(
                    'username' => 'online.phsar',
                    'rest_api_key' => 'adfec51a4cee0c57c0e6aaf2835c4e093c1c8aebddf0f011750a3ecda722a3b8',
                    'sandbox' => 0,
                    'remark' => $id,
                );
            }
            $resp = $this->wing_requestData('https://wingsdk.wingmoney.com:334/inquiry', json_encode($data), "POST");
            $response = json_decode($resp, true);

            $data_session = array();
            $this->session->set_userdata('api_payment_response', $data_session);

            if ($response['transaction_id'] != '') {
                $inv = $this->pay_model->getSaleByID($id);

                $payment = array(
                    'date' => date('Y-m-d H:i:s'),
                    'sale_id' => $inv->id,
                    'amount' => $inv->grand_total,
                    'paid_by' => 'wing',
                    'transaction_id' => $response['transaction_id'],
                    'type' => 'received',
                    'note' => '',
                    'add_ons' => 'initial_first_add_ons:{}:wing_transaction_id:{'.$response['transaction_id'].'}:'
                );
                if ($this->pay_model->addPayment($payment)) {
                    $customer = $this->pay_model->getCompanyByID($inv->customer_id);
                    //$this->pay_model->updateStatus($inv->id, 'pending');

                    $this->load->library('parser');
                    $parse_data = array(
                        'reference_number' => '#'.$id,
                        'contact_person' => $customer->name,
                        'company' => $customer->company,
                        'site_link' => base_url(),
                        'site_name' => $this->Settings->site_name,
                        'logo' => '<img src="' . base_url('assets/uploads/logos/' . $this->Settings->logo) . '" alt="' . $this->Settings->site_name . '"/>'
                    );

                    $msg = file_get_contents('./themes/' . $this->Settings->theme . '/admin/views/email_templates/payment.html');
                    $message = $this->parser->parse_string($msg, $parse_data);
                    $this->session->set_flashdata('SUCCESS', 'Payment has been made for Sale #' . $id . ' via Wing ('.$response['transaction_id'].')');
                    try {
                        $this->sma->send_email($this->Settings->default_email, 'Payment made for sale #'.$id, $message);
                    } catch (Exception $e) {
                        //$this->sma->log_payment('Email Notification Failed: ' . $e->getMessage());
                    }
                    $this->session->set_flashdata('message', lang('payment_added'));
                    $ipnstatus = true;
                    if ($inv->shop) {
                        $this->load->library('sms');
                        $this->sms->paymentReceived($inv->id, $payment['reference_no'], $payment['amount']);
                    }
                }
                
                $response['api_payment_type'] = 'wing';
                $this->session->set_userdata('api_payment_response', $response);
            }
        }
        redirect("shop/orders/".$id);
    }

    public function pipay_return($id)
    {
        if ($inv = $this->pay_model->getSaleByID($id)) {
            //$paypal = $this->pay_model->getPaypalSettings();
            $status = $this->input->get('status');
            $processorID = $this->input->get('processorID');
            $orderID = $this->input->get('orderID');
            if ($status == '0000' && $processorID != '') {
                if (base_url()=="https://demo.phsarjapan.com/") {
                    $url = 'https://onlinepayment-test.pipay.com/rest-api/verifyTransaction';
                } else {
                    $url = 'https://onlinepayment.pipay.com/rest-api/verifyTransaction';
                }
                $data = array("orderID" => $orderID, "processorID" => $processorID);
                $ch = curl_init($url);
                $payload = json_encode(array( "data" => $data ));
                curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
                # Return response instead of printing.
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                # Send request.
                $result = curl_exec($ch);
                curl_close($ch);
                
                $data = json_decode($result, true);
                if ($data['resultCode'] == '0000') {
                    //$paypal = $this->pay_model->getPaypalSettings();
                    $status = $this->input->get('status');
                    $processorID = $this->input->get('processorID');
                    if ($status == '0000' && $processorID != '') {
                        $payment = array(
                            'date' => date('Y-m-d H:i:s'),
                            'sale_id' => $inv->id,
                            'amount' => $inv->grand_total,
                            'paid_by' => 'pipay',
                            'transaction_id' => $processorID,
                            'type' => 'received',
                            'note' => '',
                            'add_ons' => 'initial_first_add_ons:{}:pipay_transaction_id:{'.$processorID.'}:'
                        );
                        if ($this->pay_model->addPayment($payment)) {
                            $customer = $this->pay_model->getCompanyByID($inv->customer_id);
                            //$this->pay_model->updateStatus($inv->id, 'pending');
        
                            $this->load->library('parser');
                            $parse_data = array(
                                'reference_number' => '#'.$id,
                                'contact_person' => $customer->name,
                                'company' => $customer->company,
                                'site_link' => base_url(),
                                'site_name' => $this->Settings->site_name,
                                'logo' => '<img src="' . base_url('assets/uploads/logos/' . $this->Settings->logo) . '" alt="' . $this->Settings->site_name . '"/>'
                            );
                            $msg = file_get_contents('./themes/' . $this->Settings->theme . '/admin/views/email_templates/payment.html');
                            $message = $this->parser->parse_string($msg, $parse_data);
                            $this->session->set_flashdata('SUCCESS', 'Payment has been made for Sale #' . $id . ' via Pipay ('.$processorID.')');
                            try {
                                $this->sma->send_email($this->Settings->default_email, 'Payment made for sale #'.$id, $message);
                            } catch (Exception $e) {
                                //$this->sma->log_payment('Email Notification Failed: ' . $e->getMessage());
                            }
                            $this->session->set_flashdata('message', lang('payment_added'));
                            $ipnstatus = true;
                            if ($inv->shop) {
                                $this->load->library('sms');
                                $this->sms->paymentReceived($inv->id, $payment['reference_no'], $payment['amount']);
                            }
                        } else {
                            $this->session->set_flashdata('error', 'Payment failed for Sale #' . $id . ' via Pipay ('.$processorID.')');
                        }
                    }
                }
            }
            $response['api_payment_type'] = 'pipay';
            $this->session->set_userdata('api_payment_response', $response);             
            redirect("shop/orders/".$id);
        } else {
            $this->session->set_flashdata('error', lang('sale_x_found'));
            redirect('/');
        }
    }

    public function paypal($id)
    {
        if ($inv = $this->pay_model->getSaleByID($id)) {
            $paypal = $this->pay_model->getPaypalSettings();
            if ($paypal->active && (($inv->grand_total - $inv->paid) > 0)) {
                $customer = $this->pay_model->getCompanyByID($inv->customer_id);
                $biller = $this->pay_model->getCompanyByID($inv->biller_id);
                if (trim(strtolower($customer->country)) == $biller->country) {
                    $paypal_fee = $paypal->fixed_charges + ($inv->grand_total * $paypal->extra_charges_my / 100);
                } else {
                    $paypal_fee = $paypal->fixed_charges + ($inv->grand_total * $paypal->extra_charges_other / 100);
                }
                $data = array(
                    'rm' => 2,
                    'no_note' => 1,
                    'no_shipping' => 1,
                    'bn' => 'BuyNow',
                    'item_number' => $inv->id,
                    'item_name' => $inv->reference_no,
                    'return' => urldecode(site_url('pay/paypal_return_no_ipn')),
                    'notify_url' => urldecode(site_url('pay/pipn')),
                    'currency_code' => $this->default_currency->code,
                    'cancel_return' => urldecode(site_url('pay/pipn')),
                    'amount' => (($inv->grand_total - $inv->paid) + $paypal_fee),
                    'image_url' => base_url() . 'assets/uploads/logos/' . $this->Settings->logo,
                    'business' => $paypal->account_email,
                    'custom' => $inv->reference_no . '__' . ($inv->grand_total - $inv->paid) . '__' . $paypal_fee,
                );
                $query = http_build_query($data, null, '&');
                redirect('https://www'.PayPalMode.'.paypal.com/cgi-bin/webscr?cmd=_xclick&'.$query);
            }
        }
        $this->session->set_flashdata('error', lang('sale_x_found'));
        redirect('/');
    }

    public function paypal_return_no_ipn()
    {
        $id = $_POST['item_number'];
        if ($inv = $this->pay_model->getSaleByID($id)) {
            $paypal = $this->pay_model->getPaypalSettings();
    
            $req = 'cmd=_notify-validate';
            foreach ($_POST as $key => $value) {
                $value = urlencode(stripslashes($value));
                $req .= "&$key=$value";
            }
            $header = "POST /cgi-bin/webscr HTTP/1.1\r\n";
            $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
            $header .= "Host: www".PayPalMode.".paypal.com\r\n";
            $header .= "Content-Length: " . strlen($req) . "\r\n";
            $header .= "Connection: close\r\n\r\n";
    
    
            $custom = explode('__', $_POST['custom']);
            $payer_email = $_POST['payer_email'];
            $invoice_no = $_POST['item_number'];
            $reference = $_POST['item_name'];
    
            if (($_POST['payment_status'] == 'Completed' || $_POST['payment_status'] == 'Processed' || $_POST['payment_status'] == 'Pending') &&
                ($_POST['business'] == $paypal->account_email) &&
                ($_POST['mc_gross'] == (round($custom[1], 2) + round($custom[2], 2)))
            ) {
                if ($_POST['mc_currency'] == $this->Settings->default_currency) {
                    $amount = $_POST['mc_gross'];
                } else {
                    $currency = $this->site->getCurrencyByCode($_POST['mc_currency']);
                    $amount = $_POST['mc_gross'] * (1 / $currency->rate);
                }
                if ($inv = $this->pay_model->getSaleByID($invoice_no)) {
                    $payment = array(
                                'date' => date('Y-m-d H:i:s'),
                                'sale_id' => $invoice_no,
                                'reference_no' => $this->site->getReference('pay'),
                                'amount' => $amount,
                                'paid_by' => 'paypal',
                                'transaction_id' => $_POST['txn_id'],
                                'type' => 'received',
                                'note' => $_POST['mc_currency'] . ' ' . $_POST['mc_gross'] . ' had been paid for the Sale Reference No ' . $inv->reference_no
                            );
                    if ($this->pay_model->addPayment($payment)) {
                        $customer = $this->pay_model->getCompanyByID($inv->customer_id);
                        //$this->pay_model->updateStatus($inv->id, 'pending');

                        $this->load->library('parser');
                        $parse_data = array(
                                    'reference_number' => $reference,
                                    'contact_person' => $customer->name,
                                    'company' => $customer->company,
                                    'site_link' => base_url(),
                                    'site_name' => $this->Settings->site_name,
                                    'logo' => '<img src="' . base_url('assets/uploads/logos/' . $this->Settings->logo) . '" alt="' . $this->Settings->site_name . '"/>'
                                );

                        $msg = file_get_contents('./themes/' . $this->Settings->theme . '/admin/views/email_templates/payment.html');
                        $message = $this->parser->parse_string($msg, $parse_data);
                        $this->session->set_flashdata('SUCCESS', 'Payment has been made for Sale Reference #' . $_POST['item_name'] . ' via Paypal (' . $_POST['txn_id'] . ')');
                        try {
                            $this->sma->send_email($paypal->account_email, 'Payment made for sale '.$inv->reference_no, $message);
                        } catch (Exception $e) {
                            //$this->sma->log_payment('Email Notification Failed: ' . $e->getMessage());
                        }
                        $this->session->set_flashdata('message', lang('payment_added'));
                        $ipnstatus = true;
                        if ($inv->shop) {
                            $this->load->library('sms');
                            $this->sms->paymentReceived($inv->id, $payment['reference_no'], $payment['amount']);
                        }
                    }
                }
            } else {
                $this->session->set_flashdata('error', 'Payment failed for Sale Reference #' . $reference . ' via Paypal (' . $_POST['txn_id'] . ')');
            }
            redirect("shop/orders/".$id);
        } else {
            $this->session->set_flashdata('error', lang('sale_x_found'));
            redirect('/');
        }
    }

    public function skrill($id)
    {
        if ($inv = $this->pay_model->getSaleByID($id)) {
            $skrill = $this->pay_model->getSkrillSettings();
            if ($skrill->active && (($inv->grand_total - $inv->paid) > 0)) {
                $customer = $this->pay_model->getCompanyByID($inv->customer_id);
                $biller = $this->pay_model->getCompanyByID($inv->biller_id);
                if (trim(strtolower($customer->country)) == $biller->country) {
                    $skrill_fee = $skrill->fixed_charges + ($inv->grand_total * $skrill->extra_charges_my / 100);
                } else {
                    $skrill_fee = $skrill->fixed_charges + ($inv->grand_total * $skrill->extra_charges_other / 100);
                }
                redirect('https://www.moneybookers.com/app/payment.pl?method=get&pay_to_email=' . $skrill->account_email . '&language=EN&merchant_fields=item_name,item_number&item_name=' . $inv->reference_no . '&item_number=' . $inv->id . '&logo_url=' . base_url() . 'assets/uploads/logos/' . $this->Settings->logo . '&amount=' . (($inv->grand_total - $inv->paid) + $skrill_fee) . '&return_url=' . shop_url('orders/'.$inv->id)  . '&cancel_url=' . site_url('/')  . '&detail1_description=' . $inv->reference_no . '&detail1_text=Payment for the sale invoice ' . $inv->reference_no . ': ' . $inv->grand_total . '(+ fee: ' . $skrill_fee . ') = ' . $this->sma->formatMoney($inv->grand_total + $skrill_fee) . '&currency=' . $this->default_currency->code . '&status_url=' . site_url('pay/sipn'));
            }
        }
        $this->session->set_flashdata('error', lang('sale_x_found'));
        redirect('/');
    }

    public function pipn()
    {
        $paypal = $this->pay_model->getPaypalSettings();
        $this->sma->log_payment('INFO', 'Paypal IPN called');
        $ipnstatus = false;

        $req = 'cmd=_notify-validate';
        foreach ($_POST as $key => $value) {
            $value = urlencode(stripslashes($value));
            $req .= "&$key=$value";
        }

        $header = "POST /cgi-bin/webscr HTTP/1.1\r\n";
        $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
        $header .= "Host: www".(DEMO ? '.sandbox' : '').".paypal.com\r\n";
        // $header .= "Host: www.paypal.com\r\n";
        $header .= "Content-Length: " . strlen($req) . "\r\n";
        $header .= "Connection: close\r\n\r\n";

        $fp = fsockopen('ssl://www'.(DEMO ? '.sandbox' : '').'.paypal.com', 443, $errno, $errstr, 30);
        // $fp = fsockopen('ssl://www.paypal.com', 443, $errno, $errstr, 30);

        if (!$fp) {
            $this->sma->log_payment('ERROR', 'Paypal Payment Failed (IPN HTTP ERROR)', $errstr);
            $this->session->set_flashdata('error', lang('payment_failed'));
        } else {
            fputs($fp, $header . $req);
            while (!feof($fp)) {
                $res = fgets($fp, 1024);
                //log_message('error', 'Paypal IPN - fp handler -'.$res);
                if (stripos($res, "VERIFIED") !== false) {
                    $this->sma->log_payment('INFO', 'Paypal IPN - VERIFIED');

                    $custom = explode('__', $_POST['custom']);
                    $payer_email = $_POST['payer_email'];
                    $invoice_no = $_POST['item_number'];
                    $reference = $_POST['item_name'];

                    if (($_POST['payment_status'] == 'Completed' || $_POST['payment_status'] == 'Processed' || $_POST['payment_status'] == 'Pending') && ($_POST['business'] == $paypal->account_email || $_POST['business'] == 'saleem-facilitator@tecdiary.com')) {
                        if ($_POST['mc_currency'] == $this->Settings->default_currency) {
                            $amount = $_POST['mc_gross'];
                        } else {
                            $currency = $this->site->getCurrencyByCode($_POST['mc_currency']);
                            $amount = $_POST['mc_gross'] * (1 / $currency->rate);
                        }
                        if ($inv = $this->pay_model->getSaleByID($invoice_no)) {
                            $payment = array(
                                'date' => date('Y-m-d H:i:s'),
                                'sale_id' => $invoice_no,
                                'reference_no' => $this->site->getReference('pay'),
                                'amount' => $amount,
                                'paid_by' => 'paypal',
                                'transaction_id' => $_POST['txn_id'],
                                'type' => 'received',
                                'note' => $_POST['mc_currency'] . ' ' . $_POST['mc_gross'] . ' had been paid for the Sale Reference No ' . $inv->reference_no
                            );
                            if ($this->pay_model->addPayment($payment)) {
                                $customer = $this->pay_model->getCompanyByID($inv->customer_id);
                                //$this->pay_model->updateStatus($inv->id, 'completed');

                                $this->load->library('parser');
                                $parse_data = array(
                                    'reference_number' => $reference,
                                    'contact_person' => $customer->name,
                                    'company' => $customer->company,
                                    'site_link' => base_url(),
                                    'site_name' => $this->Settings->site_name,
                                    'logo' => '<img src="' . base_url('assets/uploads/logos/' . $this->Settings->logo) . '" alt="' . $this->Settings->site_name . '"/>'
                                );

                                $msg = file_get_contents('./themes/' . $this->Settings->theme . '/admin/views/email_templates/payment.html');
                                $message = $this->parser->parse_string($msg, $parse_data);
                                $this->sma->log_payment('SUCCESS', 'Payment has been made for Sale Reference #' . $_POST['item_name'] . ' via Paypal (' . $_POST['txn_id'] . ').', json_encode($_POST));
                                try {
                                    $this->sma->send_email($paypal->account_email, 'Payment made for sale '.$inv->reference_no, $message);
                                } catch (Exception $e) {
                                    $this->sma->log_payment('Email Notification Failed: ' . $e->getMessage());
                                }
                                $this->session->set_flashdata('message', lang('payment_added'));
                                $ipnstatus = true;
                                if ($inv->shop) {
                                    $this->load->library('sms');
                                    $this->sms->paymentReceived($inv->id, $payment['reference_no'], $payment['amount']);
                                }
                            }
                        }
                    } else {
                        $this->sma->log_payment('ERROR', 'Payment failed for Sale Reference #' . $reference . ' via Paypal (' . $_POST['txn_id'] . ').', json_encode($_POST));
                        $this->session->set_flashdata('error', lang('payment_failed'));
                    }
                } elseif (stripos($res, "INVALID") !== false) {
                    $this->sma->log_payment('ERROR', 'INVALID response from Paypal. Payment failed via Paypal.', json_encode($_POST));
                    $this->session->set_flashdata('error', lang('payment_failed'));
                }
            }
            fclose($fp);
        }

        if ($inv->shop) {
            shop_redirect('orders/'.$inv->id.'/'.($this->loggedIn ? '' : $inv->hash));
        }

        redirect(SHOP ? '/' : site_url($ipnstatus ? 'notify/payment_success' : 'notify/payment_failed'));
        exit();
    }

    public function sipn()
    {
        $skrill = $this->pay_model->getSkrillSettings();
        $this->sma->log_payment('INFO', 'Skrill IPN called', json_encode($_POST));
        $ipnstatus = false;

        if (isset($_POST['merchant_id']) && isset($_POST['md5sig'])) {
            $concatFields = $_POST['merchant_id'] . $_POST['transaction_id'] . strtoupper(md5($skrill->secret_word)) . $_POST['mb_amount'] . $_POST['mb_currency'] . $_POST['status'];

            if (strtoupper(md5($concatFields)) == $_POST['md5sig'] && $_POST['status'] == 2 && $_POST['pay_to_email'] == $skrill->account_email) {
                $invoice_no = $_POST['item_number'];
                $reference = $_POST['item_name'];
                if ($_POST['mb_currency'] == $this->Settings->default_currency) {
                    $amount = $_POST['mb_amount'];
                } else {
                    $currency = $this->site->getCurrencyByCode($_POST['mb_currency']);
                    $amount = $_POST['mb_amount'] * (1 / $currency->rate);
                }
                if ($inv = $this->pay_model->getSaleByID($invoice_no)) {
                    $payment = array(
                        'date' => date('Y-m-d H:i:s'),
                        'sale_id' => $invoice_no,
                        'reference_no' => $this->site->getReference('pay'),
                        'amount' => $amount,
                        'paid_by' => 'skrill',
                        'transaction_id' => $_POST['mb_transaction_id'],
                        'type' => 'received',
                        'note' => $_POST['mb_currency'] . ' ' . $_POST['mb_amount'] . ' had been paid for the Sale Reference No ' . $reference
                        );
                    if ($this->pay_model->addPayment($payment)) {
                        $customer = $this->site->getCompanyByID($inv->customer_id);
                        //$this->pay_model->updateStatus($inv->id, 'completed');

                        $this->load->library('parser');
                        $parse_data = array(
                            'reference_number' => $reference,
                            'contact_person' => $customer->name,
                            'company' => $customer->company,
                            'site_link' => base_url(),
                            'site_name' => $this->Settings->site_name,
                            'logo' => '<img src="' . base_url('assets/uploads/logos/' . $this->Settings->logo) . '" alt="' . $this->Settings->site_name . '"/>'
                            );

                        $msg = file_get_contents('./themes/' . $this->Settings->theme . '/admin/views/email_templates/payment.html');
                        $message = $this->parser->parse_string($msg, $parse_data);
                        $this->sma->log_payment('SUCCESS', 'Payment has been made for Sale Reference #' . $_POST['item_name'] . ' via Skrill (' . $_POST['mb_transaction_id'] . ').', json_encode($_POST));
                        try {
                            $this->sma->send_email($skrill->account_email, 'Payment made for sale '.$inv->reference_no, $message);
                        } catch (Exception $e) {
                            $this->sma->log_payment('Email Notification Failed: ' . $e->getMessage());
                        }
                        $this->session->set_flashdata('message', lang('payment_added'));
                        $ipnstatus = true;
                        if ($inv->shop) {
                            $this->load->library('sms');
                            $this->sms->paymentReceived($inv->id, $payment['reference_no'], $payment['amount']);
                        }
                    }
                }
            } else {
                $this->sma->log_payment('ERROR', 'Payment failed for via Skrill.', json_encode($_POST));
                $this->session->set_flashdata('error', lang('payment_failed'));
            }
        } else {
            redirect('notify/payment');
        }

        if ($inv->shop) {
            shop_redirect('orders/'.$inv->id.'/'.($this->loggedIn ? '' : $inv->hash));
        }

        redirect(SHOP ? '/' : site_url($ipnstatus ? 'notify/payment_success' : 'notify/payment_failed'));
        exit();
    }

    function payway_checkTransaction($url, $merchantId, $tranId, $key)
    {
        $hash = base64_encode(hash_hmac('sha512', $merchantId . $tranId, $key, true));
        $postfields = array(
            'tran_id' => $tranId,
            'hash' => $hash
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_PROXY, null);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); // On dev server only!
        $result = curl_exec($ch);//Result Json
        $output = json_decode($result, true);//Result Array
        return $result;
    }    
    function payway_return() {
        if ($this->session->userdata('user_id') > 0) {
            $this->load->model('Shop_model');
            $temp_id = 5;
            if ($this->api_shop_setting[0]['air_base_url'] == base_url()){
                //if ($this->session->userdata('user_id') == 37)
                    $temp_id = 6;
            }

            $temp = $this->site->api_select_some_fields_with_where("
                *     
                "
                ,"sma_gateways_details"
                ,"payment_gateway_id = ".$temp_id." order by id asc"
                ,"arr"
            );
            if ($temp[0]['value'] == 'sandbox')
                $url = $temp[3]['value'];
            if ($temp[0]['value'] == 'live')
                $url = $temp[4]['value'];

            $url = $url.'check/transaction/';
            $merchantId = $temp[1]['value'];
            $key = $temp[2]['value'];
            $tranId = $id;
            
            $hash = base64_encode(hash_hmac('sha512', $merchantId . $tranId, $key, true));
            $jsonResponse = $this->payway_checkTransaction($url, $merchantId, $tranId, $key);
            $response = json_decode($jsonResponse, true);

            if ($response['status'] == 0) {

                $config_data = array(
                    'table_name' => 'sma_users',
                    'select_table' => 'sma_users',
                    'translate' => '',
                    'select_condition' => "id = ".$this->session->userdata('user_id'),
                );
                $api_user = $this->site->api_select_data_v2($config_data);

                $temp = explode('<api>',$api_user[0]['payway_order_information']);
                $config_data = array(
                    'insert' => 1,
                    'delivery_date' => $temp[0],
                    'customer_id' => $temp[1],
                    'comment' => $temp[2],
                    'product_import_date' => $temp[3],
                    'payment_method' => $temp[4],
                );            
                $temp_result = $this->shop_model->api_order_insert($config_data);

                $inv = $this->pay_model->getSaleByID($temp_result['result']);
                $id = $inv->id;
                
                $payment = array(
                    'date' => date('Y-m-d H:i:s'),
                    'sale_id' => $inv->id,
                    'amount' => $inv->grand_total,
                    'paid_by' => 'payway',
                    'transaction_id' => $response['transaction_id'],
                    'type' => 'received',
                    'note' => '',
                    'add_ons' => 'initial_first_add_ons:{}:payway_transaction_id:{'.$temp[5].'}:'
                );
                if ($this->pay_model->addPayment($payment)) {
                    $customer = $this->pay_model->getCompanyByID($inv->customer_id);
                    $this->load->library('parser');
                    $parse_data = array(
                        'reference_number' => '#'.$id,
                        'contact_person' => $customer->name,
                        'company' => $customer->company,
                        'site_link' => base_url(),
                        'site_name' => $this->Settings->site_name,
                        'logo' => '<img src="' . base_url('assets/uploads/logos/' . $this->Settings->logo) . '" alt="' . $this->Settings->site_name . '"/>'
                    );

                    $msg = file_get_contents('./themes/' . $this->Settings->theme . '/admin/views/email_templates/payment.html');
                    $message = $this->parser->parse_string($msg, $parse_data);
                    try {
                        $this->sma->send_email($this->Settings->default_email, 'Payment made for sale #'.$id, $message);
                    } catch (Exception $e) {
                        //$this->sma->log_payment('Email Notification Failed: ' . $e->getMessage());
                    }
                    $ipnstatus = TRUE;
                    if ($inv->shop) {
                        $this->load->library('sms');
                        $this->sms->paymentReceived($inv->id, $payment['reference_no'], $payment['amount']);
                    }
                }                    

                $this->shop_model->order_received($id);
                $this->shop_model->api_order_line_notification($id);
    
            }

            $config_data = array(
                'table_name' => 'sma_users',
                'id_name' => 'id',
                'field_add_ons_name' => 'add_ons',
                'selected_id' => $this->session->userdata('user_id'),
                'add_ons_title' => 'payway_order_information',
                'add_ons_value' => '',
            );
            $this->site->api_update_add_ons_field($config_data);  
            
            $response['api_payment_type'] = 'payway';
            $this->session->set_userdata('api_payment_response', $response);              
            redirect("shop/orders/".$id);
        }
        else {
            redirect('/login');
        }

    }    

}
