<?php defined('BASEPATH') or exit('No direct script access allowed');

    

class Main extends MY_Shop_Controller
{
    public function __construct()
    {
        parent::__construct();

        if ($this->Settings->mmode && $this->v != 'login') {
            redirect('notify/offline');
        }
        $this->load->library('ion_auth');
        $this->load->library('form_validation');
        $this->lang->admin_load('auth', $this->Settings->user_language);
        $this->load->library('components/api_menu_mobile', 'api_menu_mobile');
    }

    public function index()
    {
        //if (!$this->loggedIn) redirect('/login');
        $user_id = ($this->sma->session->userdata('user_id')) ?
                                        $this->sma->session->userdata('user_id') : 0;
        $shop_settings = $this->shop_settings;
        if (!SHOP) {
            redirect('admin');
        }

        if (!$this->loggedIn && $this->api_shop_setting[0]['require_login'] == 1) {
            redirect('/login');
        }


        $condition = '';
        if ($this->api_shop_setting[0]['air_base_url'] == base_url())
            $condition .= " and add_ons like '%:air_display:{yes}:%' ";
        $list_categories = array(
            'table_name' => 'sma_categories',
            'select_table' => 'sma_categories',
            'translate' => 'yes',
            'select_condition' => "parent_id = 0 ".$condition." and add_ons LIKE '%:display:{yes}:%' order by ordering asc",
        );
        $this->data['list_categories'] = $this->site->api_select_data_v2($list_categories);

        //-promotion-=====================================
        // $config_data = array(
        //     'limit' => 18,
        // );
        // $temp = $this->site->api_get_promotion_product($config_data);
        // $this->data['promotion_products'] = $temp['select_data'];        

        // $this->data['promotion_products_total'] = $temp['select_data_total'];
        //-promotion-=====================================
        
        //-featured-=====================================
        // $config_data = array(
        //     'limit' => 18,
        // );
        // $temp = $this->site->api_get_featured_product($config_data);
        // $this->data['featured_products'] = $temp['select_data'];
        // $this->data['featured_products_total'] = $temp['select_data_total'];
        //-featured-=====================================

        //-user_main_category-=====================================
        // if ($this->api_shop_setting[0]['air_base_url'] == base_url())
        //     $condition .= " and add_ons like '%:air_display:{yes}:%' ";
        // $config_data = array(
        //     'table_name' => 'sma_categories',
        //     'select_table' => 'sma_categories',
        //     'translate' => 'yes',
        //     'select_condition' => "parent_id = 0 ".$condition." order by CAST(order_number AS SIGNED) asc",
        // );
        // $temp = $this->site->api_select_data_v2($config_data);
        // $this->data['main_categories'] = $temp;        
        // for ($i=0;$i<count($temp);$i++) {
        //     if (!is_file("assets/api/data/stock_manager/json/front_main_category/parent_category_".$temp[$i]['id'].".json")) {
        //         $config_data = array(
        //             'category_id' => $temp[$i]['id'],
        //             'user_id' => $user_id,
        //             'limit' => 18,
        //         );
        //         // $this->data['products_'.$temp[$i]['id']] = $this->shop_model->getProductsByCategoryV2($config_data);
        //         $this->data['products_'.$temp[$i]['id']] = $this->site->getProductsByCategoryV2($config_data);
                
        //         $json = json_encode($this->data['products_'.$temp[$i]['id']]);
                
        //         $bytes = file_put_contents("assets/api/data/stock_manager/json/front_main_category/parent_category_".$temp[$i]['id'].".json", $json); 
        //     }
        //     $temp_2 = file_get_contents("assets/api/data/stock_manager/json/front_main_category/parent_category_".$temp[$i]['id'].".json");
        //     $this->data['products_'.$temp[$i]['id']] = json_decode($temp_2, true);                            
        // }
        //-user_main_category-=====================================
        
        $this->data['slider'] = json_decode($shop_settings->slider);
        $this->data['page_title'] = $shop_settings->shop_name;
        $this->data['page_desc'] = $shop_settings->description;
        $this->page_construct('index', $this->data);
    }

    public function profile($act = null)
    {
        if (!$this->loggedIn) {
            redirect('/');
        }
        if (!SHOP || $this->Staff) {
            redirect('admin/users/profile/'.$this->session->userdata('user_id'));
        }
        $user = $this->ion_auth->user()->row();
        if ($act == 'user') {
            $this->form_validation->set_rules('first_name', lang("first_name"), 'required');
            $this->form_validation->set_rules('last_name', lang("last_name"), 'required');
            $this->form_validation->set_rules('phone', lang("phone"), 'required');
            //$this->form_validation->set_rules('email', lang("email"), 'required|valid_email');
            $this->form_validation->set_rules('company', lang("company"), 'trim');
            $this->form_validation->set_rules('vat_no', lang("vat_no"), 'trim');
            $this->form_validation->set_rules('address', lang("billing_address"), 'required');
            if ($user->email != $this->input->post('email')) {
                $this->form_validation->set_rules('email', lang("email"), 'trim|is_unique[users.email]');
            }
            
            
            if ($this->form_validation->run() === true) {
                $bdata = [
                    'name' => $this->input->post('first_name').' '.$this->input->post('last_name'),
                    'phone' => $this->input->post('phone'),
                    'company' => $this->input->post('first_name').' '.$this->input->post('last_name'),
                    'vat_no' => $this->input->post('vat_no'),
                    'address' => $this->input->post('address'),
                    'city' => $this->input->post('city'),
                    'city_id' => $this->input->post('city_id'),
                    'state' => $this->input->post('state'),
                    'postal_code' => $this->input->post('postal_code'),
                    'country' => $this->input->post('country'),
                ];

                $udata = [
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                    'company' => $this->input->post('first_name').' '.$this->input->post('last_name'),
                    'phone' => $this->input->post('phone'),
                ];
                foreach($_POST as $name => $value) {  
                    if (is_int(strpos($name,"add_ons_"))) {
                        $value = $this->input->post($name);
                        $temp_name = str_replace('add_ons_','',$name);    
                        $config_data = array(
                            'table_name' => 'sma_companies',
                            'id_name' => 'id',
                            'field_add_ons_name' => 'add_ons',
                            'selected_id' => $user->company_id,
                            'add_ons_title' => $temp_name,
                            'add_ons_value' => $value,                    
                        );
                        $this->site->api_update_add_ons_field($config_data); 
                    }
                }

                if ($this->ion_auth->update($user->id, $udata) && $this->shop_model->updateCompany($user->company_id, $bdata)) {
                    $this->session->set_flashdata('message', lang('user_updated'));
                    $this->session->set_flashdata('message', lang('billing_data_updated'));
                    redirect("profile");
                }
                
            } else {
                $this->session->set_flashdata('error', validation_errors());
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } elseif ($act == 'password') {
            $this->form_validation->set_rules('old_password', lang('old_password'), 'required');
            $this->form_validation->set_rules('new_password', lang('new_password'), 'required|min_length[8]|max_length[25]');
            $this->form_validation->set_rules('new_password_confirm', lang('confirm_password'), 'required|matches[new_password]');

            if ($this->form_validation->run() == false) {
                $this->session->set_flashdata('error', validation_errors());
                redirect('profile');
            } else {
                if (DEMO) {
                    $this->session->set_flashdata('warning', lang('disabled_in_demo'));
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                $identity = $this->session->userdata($this->config->item('identity', 'ion_auth'));
                $identity = $user->id;

                $change = $this->ion_auth->change_password($identity, $this->input->post('old_password'), $this->input->post('new_password'));

                if ($change) {
                    $this->session->set_flashdata('message', $this->ion_auth->messages());
                    $this->logout('m');
                } else {
                    $this->session->set_flashdata('error', lang('Please_type_correct_old_password').'<br>'.lang('Password_change_failed'));
                    redirect('profile');
                }
            }
        }

        $this->data['featured_products'] = $this->shop_model->getFeaturedProducts();
        $this->data['customer'] = $this->site->getCompanyByID($this->session->userdata('company_id'));
        $config_data = array(
            'table_name' => 'sma_companies',
            'select_table' => 'sma_companies',
            'translate' => '',
            'select_condition' => "id = ".$this->session->userdata('company_id'),
        );
        $this->data['customer_v2'] = $this->site->api_select_data_v2($config_data);

        $this->data['user'] = $this->site->getUser();
        $this->data['page_title'] = lang('profile');
        $this->data['page_desc'] = $this->shop_settings->description;
        $this->page_construct('user/profile', $this->data);
        
    }
    

    public function login($m = null)
    {
        if (!SHOP || $this->Settings->mmode) {
            redirect('admin/login');
        }

        if ($this->api_shop_setting[0]['require_login'] == 1) {
            $this->cart->destroy();
        }
        
        if ($this->loggedIn) {
            $this->session->set_flashdata('error', $this->session->flashdata('error'));
            redirect('/');
        }

        if ($this->Settings->captcha) {
            $this->form_validation->set_rules('captcha', lang('captcha'), 'required|callback_captcha_check');
        }

        if ($this->form_validation->run('auth/login') == true) {
            $remember = (bool)$this->input->post('remember_me');

            if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember)) {
                if ($this->Settings->mmode) {
                    if (!$this->ion_auth->in_group('owner')) {
                        $this->session->set_flashdata('error', lang('site_is_offline_plz_try_later'));
                        redirect('logout');
                    }
                }

                $temp = '<p>'.lang('hello').', <strong>'.$this->session->userdata('username').'</strong> !! '.lang('thank_you_login').'!!</p>';
                $this->session->set_userdata('api_message_title', lang('Login_Successfully'));
                $this->session->set_userdata('api_message', $temp);
                $this->session->set_flashdata('message', $temp);

                $referrer = ($this->session->userdata('requested_page') && $this->session->userdata('requested_page') != 'admin') ? $this->session->userdata('requested_page') : '/';
                // redirect('shop/wishlist');
                redirect($_SERVER["HTTP_REFERER"]);
            } else { 
                if($this->session->userdata('user_id') > 0) {
                    $data_user = $this->site->api_select_some_fields_with_where("
                        *"
                        ,"sma_users"
                        ,"id = ".$this->session->userdata('user_id')
                        ,"arr"
                    );
                
                    if($data_user[0]['active'] == 0) {
                        $message = '
                            <b>'.lang('Please_verify_your_email_address').'</b>
                            <br><br>
                            '.lang('text_activation') .' <b>'. $this->session->userdata('email').'</b> '.lang('text_activation_2').'
                            <br><br>
                            '.lang('text_activation_3').'
                            <br><br> 
                        ';                        
                        $this->session->set_flashdata('reminder', $message);
                        redirect(base_url().'profile?verify_email_address=1');
                    } 
                } else {
                    $this->session->set_flashdata('error', lang('login_error_message'));
                    $this->session->set_userdata('api_message_title', lang('Login_failed'));
                    $this->session->set_userdata('api_message', lang('Incorrect_username_or_password'));
                    redirect('login');
                }
            }
        } else {
            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            $this->data['message'] = $m ? lang('password_changed') : $this->session->flashdata('message');
            $this->data['page_title'] = lang('login');
            $this->data['page_desc'] = $this->shop_settings->description;
            if ($this->shop_settings->private) {
                $this->data['message'] = isset($data['message']) ? $data['message'] : $this->session->flashdata('message');
                $this->data['error'] = isset($data['error']) ? $data['error'] : $this->session->flashdata('error');
                $this->data['warning'] = isset($data['warning']) ? $data['warning'] : $this->session->flashdata('warning');
                $this->data['reminder'] = isset($data['reminder']) ? $data['reminder'] : $this->session->flashdata('reminder');
                $this->data['Settings'] = $this->Settings;
                $this->data['shop_settings'] = $this->shop_settings;
                $this->load->view($this->theme.'user/private_login.php', $this->data);
            } else {
                $this->page_construct('user/login', $this->data);
            }
        }
    }

    public function logout($m = null)
    {
        $this->ion_auth->logout();
        if (!SHOP) {
            redirect('admin/logout');
        }
        $this->cart->destroy();
        $this->session->set_userdata('api_message_login_first', 0);
        $this->cart->destroy();
        $referrer = (isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : '/');
        $this->session->set_flashdata('message', $this->ion_auth->messages());
        return redirect($m ? 'login/m' : $referrer);
    }

    public function is_login()
    {
        echo $this->loggedIn;
    }

    public function register()
    {
        if ($this->shop_settings->private) {
            redirect('/login');
        }

        $username = $this->input->post('username');
        $username = strtolower($username);
        $config_data = array(
            'table_name' => 'sma_users',
            'select_table' => 'sma_users',
            'select_condition' => "username = '".$username."'",
        );
        $temp = $this->site->api_select_data_v2($config_data);
        if ($temp[0]['username'] == $username) {
            $temp_display_2 = "This username ".$username." is already exists.";
            $this->session->set_flashdata('error', $temp_display_2);
            redirect('login#register');            
        }
    
        $email = $this->input->post('email');
        $email = strtolower($email);
        $config_data = array(
            'table_name' => 'sma_users',
            'select_table' => 'sma_users',
            'select_condition' => "email = '".$email."'",
        );
        $temp = $this->site->api_select_data_v2($config_data);
        if ($temp[0]['email'] == $email) {
            $temp_display = "This email ".$temp_display." is already exists.";
            $this->session->set_flashdata('error', $temp_display);
            redirect('login#register');            
        }            

        $this->form_validation->set_rules('first_name', lang("first_name"), 'required');
        $this->form_validation->set_rules('last_name', lang("last_name"), 'required');
        $this->form_validation->set_rules('phone', lang("phone"), 'required');
        //$this->form_validation->set_rules('company', lang("company"), 'required');

        $temp_email = strtolower($this->input->post('email'));
        $config_data = array(
            'table_name' => 'sma_users',
            'select_table' => 'sma_users',
            'select_condition' => "email = '".$temp_email."'",
        );
        $temp = $this->site->api_select_data_v2($config_data);
        $b = 0;
        if (is_array($temp)) {
            if (count($temp) > 0) {
                for ($i=0;$i<count($temp);$i++) {
                    if ($temp[$i]['allow_same_email'] == 'yes') {
                        $b = 1;
                        break;
                    }
                }
            }
        }

        // if ($b != 1) {
        //     $this->form_validation->set_rules('email', lang("email_address"), 'required|is_unique[users.email]');
        // } else {
        //     $this->form_validation->set_rules('email', lang("email_address"), 'required');
        // }
        // $this->form_validation->set_rules('username', lang("username"), 'required|is_unique[users.username]');

        $this->form_validation->set_rules('username', lang("username"), 'required');
        $this->form_validation->set_rules('email', lang("email_address"), 'required');

        // $this->form_validation->set_rules('password', lang('password'), 'required|min_length[8]|max_length[20]|matches[password_confirm]');
        // $this->form_validation->set_rules('password_confirm', lang('confirm_password'), 'required');

        if ($this->form_validation->run('') == true) {
            $email = strtolower($this->input->post('email'));
            $username = strtolower($this->input->post('username'));
            $password = $this->input->post('password');

            $customer_group = $this->shop_model->getCustomerGroup($this->Settings->customer_group);
            $price_group = $this->shop_model->getPriceGroup($this->Settings->price_group);
            
            $add_ons = 'initial_first_add_ons:{}:';
            foreach ($_POST as $name => $value) {
                $value = $this->input->post($name);
                if (is_int(strpos($name, "add_ons_"))) {
                    $temp_name = str_replace('add_ons_', '', $name);
                    $add_ons .= $temp_name.':{'.$value.'}:';
                }
            }
            if ($this->input->post('city_id') == '')
                $temp_city = 0;
            else
                $temp_city = $this->input->post('city_id');  
            $company_data = [
                'company' => $this->input->post('first_name').' '.$this->input->post('last_name'),
                'name' => $this->input->post('first_name').' '.$this->input->post('last_name'),
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
                'address' => $this->input->post('address'),
                'country' => $this->input->post('country'),
                'city_id' => $temp_city,
                'group_id' => 3,
                'group_name' => 'customer',
                'customer_group_id' => (!empty($customer_group)) ? $customer_group->id : null,
                'customer_group_name' => (!empty($customer_group)) ? $customer_group->name : null,
                'price_group_id' => (!empty($price_group)) ? $price_group->id : null,
                'price_group_name' => (!empty($price_group)) ? $price_group->name : null,
                'add_ons' => $add_ons,
            ];

            $company_id = $this->shop_model->addCustomer($company_data);
            $additional_data = [
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'phone' => $this->input->post('phone'),
                'company' => $this->input->post('first_name').' '.$this->input->post('last_name'),
                'gender' => 'male',
                'company_id' => $company_id,
                'group_id' => 3
            ];

            $this->load->library('ion_auth');
        }

        if ($this->form_validation->run() == true && $this->ion_auth->register($username, $password, $email, $additional_data)) {
            $message = '
                <br><br>
                <b>'.lang('Please_verify_your_email_address').'</b>
                <br><br>
                '.lang('text_activation') .' <b>'.$email.'</b> '.lang('text_activation_2').'
                <br><br>
                '.lang('text_activation_3').'
                <br><br>
            ';    

            $this->session->set_flashdata('message', lang("account_creation_success").$message);
            redirect('login');
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect('login#register');
        }
    }

    public function activate($id, $code)
    {

        if (!SHOP) {
            redirect('admin/auth/activate/'.$id.'/'.$code);
        }
        $config_data = array(
            'table_name' => 'sma_users',
            'select_table' => 'sma_users',
            'translate' => '',
            'description' => '',
            'select_condition' => "id = ".$id." ",
        );
        $temp = $this->api_helper->api_select_data_v2($config_data);
        if ($temp[0]['id'] > 0) {
            if ($temp[0]['active'] == 1) {
                $this->session->set_flashdata('message', lang('Your account is already activated'));
                redirect(base_url());
            }
            else {

                if ($code == $temp[0]['activation_code']) {
                    $temp_2 = array(
                        'active' => 1,
                    );
                    $this->db->update('sma_users', $temp_2,"id = ".$id);                    
                    
                    $this->session->set_flashdata('message', lang('Your account is activated successfully'));
                    redirect(base_url());
                }
                else {
                    
                    $this->session->set_flashdata('error', lang('Incorrect activation code'));
                    redirect(base_url());                    
                }

            }
        }
        else {
            $this->session->set_flashdata('error', lang('This user ID: '.$id.' is not found'));
            redirect("login");                
        }

    }

    public function forgot_password()
    {
        if (!SHOP) {
            redirect('admin/auth/forgot_password');
        }
        $this->form_validation->set_rules('email', lang('email_address'), 'required|valid_email');

        if ($this->form_validation->run() == false) {
            $this->sma->send_json(validation_errors());
        } else {
            $identity = $this->ion_auth->where('email', strtolower($this->input->post('email')))->users()->row();
            if (empty($identity)) {
                $this->sma->send_json(lang('forgot_password_email_not_found'));
            }

            $forgotten = $this->ion_auth->forgotten_password($identity->email);
            if ($forgotten) {
                $this->sma->send_json(['status' => 'success', 'message' => $this->ion_auth->messages()]);
            } else {
                $this->sma->send_json(['status' => 'error', 'message' => $this->ion_auth->errors()]);
            }
        }
    }

    public function reset_password($code = null)
    {
        if (!SHOP) {
            redirect('admin/auth/reset_password/'.$code);
        }
        if (!$code) {
            $this->session->set_flashdata('error', lang('page_not_found'));
            redirect('/');
        }

        $user = $this->ion_auth->forgotten_password_check($code);

        if ($user) {
            $this->form_validation->set_rules('new', lang('password'), 'required|min_length[8]|max_length[25]|matches[new_confirm]');
            $this->form_validation->set_rules('new_confirm', lang('confirm_password'), 'required');

            if ($this->form_validation->run() == false) {
                $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
                $this->data['message'] = $this->session->flashdata('message');
                $this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
                $this->data['new_password'] = [
                    'name' => 'new',
                    'id' => 'new',
                    'type' => 'password',
                    'class' => 'form-control',
                    'required' => 'required',
                    'pattern' => '(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}',
                    'data-fv-regexp-message' => lang('pasword_hint'),
                    'placeholder' => lang('new_password')
                ];
                $this->data['new_password_confirm'] = [
                    'name' => 'new_confirm',
                    'id' => 'new_confirm',
                    'type' => 'password',
                    'class' => 'form-control',
                    'required' => 'required',
                    'data-fv-identical' => 'true',
                    'data-fv-identical-field' => 'new',
                    'data-fv-identical-message' => lang('pw_not_same'),
                    'placeholder' => lang('confirm_password')
                ];
                $this->data['user_id'] = [
                    'name' => 'user_id',
                    'id' => 'user_id',
                    'type' => 'hidden',
                    'value' => $user->id,
                ];
                $this->data['code'] = $code;
                $this->data['identity_label'] = $user->email;
                $this->data['page_title'] = lang('reset_password');
                $this->data['page_desc'] = '';
                $this->page_construct('user/reset_password', $this->data);
            } else {
                // do we have a valid request?
                if ($user->id != $this->input->post('user_id')) {
                    $this->ion_auth->clear_forgotten_password_code($code);
                    redirect('notify/csrf');
                } else {
                    // finally change the password
                    $identity = $user->email;

                    $change = $this->ion_auth->reset_password($identity, $this->input->post('new'));
                    if ($change) {
                        //if the password was successfully changed
                        $this->session->set_flashdata('message', $this->ion_auth->messages());
                        redirect('login');
                    } else {
                        $this->session->set_flashdata('error', $this->ion_auth->errors());
                        redirect('reset_password/' . $code);
                    }
                }
            }
        } else {
            //if the code is invalid then send them back to the forgot password page
            $this->session->set_flashdata('error', $this->ion_auth->errors());
            redirect('/');
        }
    }

    public function captcha_check($cap)
    {
        $expiration = time() - 300; // 5 minutes limit
        $this->db->delete('captcha', ['captcha_time <' => $expiration]);

        $this->db->select('COUNT(*) AS count')
        ->where('word', $cap)
        ->where('ip_address', $this->input->ip_address())
        ->where('captcha_time >', $expiration);

        if ($this->db->count_all_results('captcha')) {
            return true;
        } else {
            $this->form_validation->set_message('captcha_check', lang('captcha_wrong'));
            return false;
        }
    }

    public function hide($id = null)
    {
        $this->session->set_userdata('hidden' . $id, 1);
        echo true;
    }

    public function language($lang)
    {
        $folder = 'app/language/';
        $languagefiles = scandir($folder);
        if (in_array($lang, $languagefiles)) {
            set_cookie('shop_language', $lang, 31536000);
        }
        redirect($_SERVER["HTTP_REFERER"]);
    }

    public function currency($currency)
    {
        set_cookie('shop_currency', $currency, 31536000);
        redirect($_SERVER["HTTP_REFERER"]);
    }

    public function cookie($val)
    {
        set_cookie('shop_use_cookie', $val, 31536000);
        redirect($_SERVER["HTTP_REFERER"]);
    }

    public function api_facebook_loginsfvesvsevesfevesaf()
    {
        foreach ($_GET as $temp_name => $value) {
            ${$temp_name} = $value;
        }
        if (isset($email)) {
            $this->session->set_userdata('api_facebook_login_email', $email);
        } else {
            $this->session->set_userdata('api_facebook_login_email', $id);
        }
        $this->session->set_userdata('api_facebook_login_name', $name);
        $this->session->set_userdata('api_facebook_login_id', $id);

        if (isset($email)) {
            if ($email == '') {
                $email = $id;
            }

            $temp = $this->site->api_select_some_fields_with_where(
                "
            username, email, id, password, active, last_login, last_ip_address, avatar, gender, group_id, warehouse_id, biller_id, company_id, view_right, edit_right, allow_discount, show_cost, show_price
                ",
                "sma_users",
                "email = '".$email ."' and group_id = 3",
                "arr"
            );
            if (count($temp) > 0) {
                foreach (array_keys($temp[0]) as $key) {
                    $session_data[$key] = $temp[0][$key];
                }
                $this->session->set_userdata($session_data);
                if ($temp[0]['action'] != 1) {
                    $temp3 = array(
                        'active' => 1
                    );
                    $this->db->update('sma_users', $temp3, "email = '".$email ."'");
                }

                $temp2 = 'Hello, <strong>'.$name.'</strong> !! Thank you so much for login!!';
                $this->session->set_userdata('api_message_title', lang('Login_Successfully'));
                $this->session->set_userdata('api_message', $temp2);
                $this->session->set_flashdata('message', $temp2);
                $this->session->set_userdata('user_id', $temp[0]['id']);
                $this->session->set_userdata('identity', $temp[0]['username']);
                $temp_result = 1;
            } else {
                $temp_result = 2;
            }
        } else {
            $this->session->set_flashdata('error', $this->ion_auth->errors());
            $this->session->set_userdata('api_message_title', lang('Login_failed'));
            $this->session->set_userdata('api_message', lang('Login_failed'));
            $temp_result = 0;
        }

        
        $temp3[1] = 'api-ajax-request-multiple-result-split';
        $temp3[1] .= $id;
        $temp3[2] = 'api-ajax-request-multiple-result-split';
        $temp3[2] .= $temp_result;
        $temp3[3] = 'api-ajax-request-multiple-result-split';
        $result = $temp3[1].$temp3[2].$temp3[3];
        echo $result;
    }
    public function api_facebook_register()
    {
        $this->form_validation->set_rules('name', lang("name"), 'trim|required');
        $this->form_validation->set_rules('phone', lang("phone"), 'trim|required');   
        if ($this->session->userdata('api_social_email') == '') {
            $this->form_validation->set_rules('email', lang("email"), 'trim|is_unique[users.email]');
        }         
        $this->form_validation->set_rules('address', lang("address"), 'trim|required');

        if ($this->form_validation->run() == true) {
            $social_id =  $this->session->userdata('api_social_id');
            $name = $this->session->userdata('api_social_email');
            $username = $this->session->userdata('api_social_id');
            if ($this->session->userdata('api_social_email') != '') 
                $email = $this->session->userdata('api_social_email');
            else 
                $email = $this->input->post('email');

            $rand = rand(1111, 9999);
            $password = 'User_'.$rand.'_'.$social_id;

            $customer_group = $this->shop_model->getCustomerGroup($this->Settings->customer_group);
            $price_group = $this->shop_model->getPriceGroup($this->Settings->price_group);

            $company_data = [
                'company' => $name,
                'name' => $name,
                'email' => $email,
                'phone' => $this->input->post('phone'),
                'city_id' => $this->input->post('city_id'),
                'address' => nl2br($this->input->post('address')),
                'country' => 'Cambodia',
                'group_id' => 3,
                'group_name' => 'customer',
                'customer_group_id' => (!empty($customer_group)) ? $customer_group->id : null,
                'customer_group_name' => (!empty($customer_group)) ? $customer_group->name : null,
                'price_group_id' => (!empty($price_group)) ? $price_group->id : null,
                'price_group_name' => (!empty($price_group)) ? $price_group->name : null,
            ];
            $company_id = $this->shop_model->addCustomer($company_data);

            $temp5 = explode(' ', $this->input->post('name'));

            if ($this->session->userdata('api_social_type') == 'facebook')
                $temp = $social_id;
            else
                $temp = '';

            $additional_data = [
                'first_name' => $temp5[0],
                'last_name' => $temp5[1],
                'phone' => $phone,
                'company' => $name,
                'gender' => 'male',
                'company_id' => $company_id,
                'group_id' => 3,
                'facebook_id' => $temp
            ];
            $this->load->library('ion_auth');
            if ($this->ion_auth->register($username, $password, $email, $additional_data, 1)) {
                $temp = $this->site->api_select_some_fields_with_where(
                    "
                username, email, id, password, active, last_login, last_ip_address, avatar, gender, group_id, warehouse_id, biller_id, company_id, view_right, edit_right, allow_discount, show_cost, show_price
                    ",
                    "sma_users",
                    "email = '".$email ."'",
                    "arr"
                );
                foreach (array_keys($temp[0]) as $key) {
                    $session_data[$key] = $temp[0][$key];
                }
                $this->session->set_userdata($session_data);
                if ($temp[0]['action'] != 1) {
                    $temp3 = array(
                        'active' => 1
                    );
                    $this->db->update('sma_users', $temp3, "email = '".$email ."'");
                }

                $temp2 = '<p>'.lang('hello').', <strong>'.$this->session->userdata('email').'</strong> !! '.lang('thank_you_login').'!!</p>';
                // $temp2 = 'Hello, <strong>'.$name.'</strong> !! Thank you so much for login!!';
                $this->session->set_userdata('api_message_title', lang('Login_Successfully'));
                $this->session->set_userdata('api_message', $temp2);
                $this->session->set_flashdata('message', $temp2);
                $this->session->set_userdata('user_id', $temp[0]['id']);
                $this->session->set_userdata('identity', $temp[0]['username']);
                redirect('/shop/wishlist');
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect('login');
        }
    }

    public function api_generate_password()
    {
        $value = $this->site->api_generate_password();
        $temp3[1] = 'api-ajax-request-multiple-result-split';
        $temp3[1] .= $value;
        $temp3[2] = 'api-ajax-request-multiple-result-split';
        $result = $temp3[1].$temp3[2];
        echo $result;
    }

public function api_rest_api_get_products() {
    foreach ($_POST as $name => $value) {
        ${$name} = $value;
    }    
    if ($_POST['return_url'] == 'https://demo.phsarjapan.com/api/products') {
        echo '
            <form class="api_display_none" id="api_rest_api_form" name="api_rest_api_form" action="http://localhost/stock_manager/api/products" method="post" enctype="application/json">
                <input name="pet[0][species]" value="Dahut">
                <input name="pet[0][name]" value="n_Hypatia">
                <input name="pet[1][species]" value="Felis Stultus">
                <input name="pet[1][name]" value="n_Billie">
            </form>
            <script>
                document.api_rest_api_form.submit();
            </script>        
        ';
    }
}

public function api_product_status() {
    if ($_GET['security'] == 'dse3ii4248dk22nf83e932weoi32') {
        $config_data = array(
            'table_name' => 'sma_products',
            'select_table' => 'sma_products',
            'translate' => '',
            'select_condition' => "id > 0",
        );
        $select_data = $this->site->api_select_data_v2($config_data);
        for ($i=0;$i<count($select_data);$i++) {
            if ($select_data[$i]['product_status'] == 'unavailable_today') {
                $config_data = array(
                    'table_name' => 'sma_products',
                    'id_name' => 'id',
                    'field_add_ons_name' => 'add_ons',
                    'selected_id' => $select_data[$i]['id'],
                    'add_ons_title' => 'product_status',
                    'add_ons_value' => '',                    
                );
                $this->site->api_update_add_ons_field($config_data); 
            }
        }
    } 
}
function api_refresh_member_main_category() {
    if ($_GET['security'] == '9jjmidse3ii4248rgvrsdk22nf83e932') {
        if ($this->api_shop_setting[0]['air_base_url'] == base_url())
            $condition .= " and add_ons like '%:air_display:{yes}:%' ";
        $config_data = array(
            'table_name' => 'sma_categories',
            'select_table' => 'sma_categories',
            'translate' => 'yes',
            'select_condition' => "parent_id = 0 ".$condition." order by CAST(order_number AS SIGNED) asc",
        );
        $temp = $this->site->api_select_data_v2($config_data);
        $this->data['main_categories'] = $temp;        
        for ($i=0;$i<count($temp);$i++) {
            if (is_file("assets/api/data/stock_manager/json/front_main_category/parent_category_".$temp[$i]['id'].".json")) {
                unlink("assets/api/data/stock_manager/json/front_main_category/parent_category_".$temp[$i]['id'].".json");
            }
        }
        for ($i=0;$i<count($temp);$i++) {
            if (!is_file("assets/api/data/stock_manager/json/front_main_category/parent_category_".$temp[$i]['id'].".json")) {
                $config_data = array(
                    'category_id' => $temp[$i]['id'],
                    'user_id' => $user_id,
                    'limit' => 18,
                );
                $this->data['products_'.$temp[$i]['id']] = $this->site->getProductsByCategoryV2($config_data);
                
                $json = json_encode($this->data['products_'.$temp[$i]['id']]);
                
                $bytes = file_put_contents("assets/api/data/stock_manager/json/front_main_category/parent_category_".$temp[$i]['id'].".json", $json); 
            }
        }

    }
}

public function ajax_getCityId() {
    foreach ($_GET as $name => $value) {
        ${$name} = $value;  
    } 

    $config_data = array(
        'table_name' => 'sma_city',
        'select_table' => 'sma_city',
        'translate' => '',
        'select_condition' => "id = ".$field_id,
    );
    $select_data = $this->site->api_select_data_v2($config_data); 
    for($i=0;$i<count($select_data);$i++) {
        $temp = $select_data[$i]['delivery_fee'];
        $temp_2 = $select_data[$i]['address_discount'];
    }
    
    $temp3[1] = 'api-ajax-request-multiple-result-split';  
    $temp3[1] .= $temp;
    $temp3[2] = 'api-ajax-request-multiple-result-split';
    $temp3[2] .= $temp_2;             
    $result = $temp3[1].$temp3[2];
    echo $result;  
}
public function ajax_check_username_email() {
    foreach ($_GET as $name => $value) {
        ${$name} = $value;
    }
    $username = strtolower($username);
    $config_data = array(
        'table_name' => 'sma_users',
        'select_table' => 'sma_users',
        'select_condition' => "username = '".$username."'",
    );
    $temp = $this->site->api_select_data_v2($config_data);
    $temp_display_2 = '';
    if ($temp[0]['username'] == $username) {
        $temp_display_2 = lang('Username').'&nbsp;'.$username.'&nbsp;'.lang('is_already_exists');
    }

    $email = strtolower($email);
    $config_data = array(
        'table_name' => 'sma_users',
        'select_table' => 'sma_users',
        'select_condition' => "email = '".$email."'",
    );
    $temp2 = $this->site->api_select_data_v2($config_data);
    $temp_display = '';
    if ($temp2[0]['email'] == $email) {
        $temp_display = lang('email').'&nbsp;'.$email.'&nbsp;'.lang('is_already_exists');
    }

    $temp3[1] = 'api-ajax-request-multiple-result-split';  
    $temp3[1] .= $temp_display;  
    $temp3[3] = 'api-ajax-request-multiple-result-split';  
    $temp3[3] .= $temp_display_2;    
    $temp3[4] = 'api-ajax-request-multiple-result-split'; 
    $temp3[4] .= $temp_display_3;    
    $temp3[5] = 'api-ajax-request-multiple-result-split';  
    $result = $temp3[1].$temp3[3].$temp3[4]; 
    echo $result;                  
}


}
