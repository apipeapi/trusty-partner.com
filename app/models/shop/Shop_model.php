<?php defined('BASEPATH') or exit('No direct script access allowed');

class Shop_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getSpecialPrice()
    {
        $sp = new stdClass();
        $sp->cgp = ($this->customer && $this->customer->price_group_id) ? "( SELECT {$this->db->dbprefix('product_prices')}.price as price, {$this->db->dbprefix('product_prices')}.product_id as product_id, {$this->db->dbprefix('product_prices')}.price_group_id as price_group_id from {$this->db->dbprefix('product_prices')} WHERE {$this->db->dbprefix('product_prices')}.price_group_id = {$this->customer->price_group_id} ) cgp" : null;

        $sp->wgp = ($this->warehouse && $this->warehouse->price_group_id && !$this->customer) ? "( SELECT {$this->db->dbprefix('product_prices')}.price as price, {$this->db->dbprefix('product_prices')}.product_id as product_id, {$this->db->dbprefix('product_prices')}.price_group_id as price_group_id from {$this->db->dbprefix('product_prices')} WHERE {$this->db->dbprefix('product_prices')}.price_group_id = {$this->warehouse->price_group_id} ) wgp" : null;

        return $sp;
    }

    public function getSettings()
    {
        return $this->db->get('settings')->row();
    }
    public function getSettings_v2($id)
    {
        if ($id != '') {
            $temp_field = $this->site->api_get_condition_add_ons_field("add_ons", "sma_add_ons", "table_name = 'sma_settings'");
            $temp = $this->site->api_select_some_fields_with_where(
                "*
                ".$temp_field,
                "sma_settings",
                "setting_id = ".$id,
                "arr"
            );
            $q = $this->db->get_where('sma_settings', array('setting_id' => $id), 1);
            $temp2 = $q->row();
            foreach ($temp[0] as $key => $value) {
                $temp2->{$key} = $value;
            }
            return $temp2;
        } else {
            return false;
        }
    }


    public function getShopSettings()
    {
        return $this->db->get('shop_settings')->row();
    }

    public function getCustomerGroup($id)
    {
        return $this->db->get_where('customer_groups', ['id' => $id])->row();
    }

    public function getPriceGroup($id)
    {
        return $this->db->get_where('price_groups', ['id' => $id])->row();
    }

    public function getDateFormat($id)
    {
        return $this->db->get_where('date_format', ['id' => $id], 1)->row();
    }

    public function addCustomer($data)
    {
        if ($this->db->insert('companies', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    public function addWishlist($product_id)
    {
        $user_id = $this->session->userdata('user_id');
        if (!$this->getWishlistItem($product_id, $user_id)) {
            return $this->db->insert('wishlist', ['product_id' => $product_id, 'user_id' => $user_id]);
        }
        return false;
    }

    public function removeWishlist($product_id)
    {
        $user_id = $this->session->userdata('user_id');
        return $this->db->delete('wishlist', ['product_id' => $product_id, 'user_id' => $user_id]);
    }

    public function getWishlistItem($product_id, $user_id)
    {
        return $this->db->get_where('wishlist', ['product_id' => $product_id, 'user_id' => $user_id])->row();
    }

    public function getAllCurrencies()
    {
        return $this->db->get('currencies')->result();
    }

    public function getNotifications()
    {
        $date = date('Y-m-d H:i:s', time());
        $this->db->where("from_date <=", $date)
        ->where("till_date >=", $date)->where('scope !=', 2);
        return $this->db->get("notifications")->result();
    }

    public function getAddresses()
    {
        return $this->db->get_where("addresses", ['company_id' => $this->session->userdata('company_id')])->result();
    }

    public function getCurrencyByCode($code)
    {
        return $this->db->get_where('currencies', ['code' => $code], 1)->row();
    }

    public function getAllCategories()
    {
        if ($this->shop_settings->hide0) {
            $pc = "(SELECT count(*) FROM {$this->db->dbprefix('products')} WHERE {$this->db->dbprefix('products')}.category_id = {$this->db->dbprefix('categories')}.id)";
            $this->db->select("{$this->db->dbprefix('categories')}.*, {$pc} AS product_count", false);
            $this->db->having("product_count >", 0);
        }
        $this->db->group_start()->where('parent_id', null)->or_where('parent_id', 0)->group_end()->order_by('name');
        return $this->db->get("categories")->result();
    }

    public function getSubCategories($parent_id)
    {
        $this->db->where('parent_id', $parent_id)->order_by('name');
        return $this->db->get("categories")->result();
    }

    public function getCategoryBySlug($slug)
    {
        return $this->db->get_where('categories', ['slug' => $slug], 1)->row();
    }

    public function getAllBrands()
    {
        if ($this->shop_settings->hide0) {
            $pc = "(SELECT count(*) FROM {$this->db->dbprefix('products')} WHERE {$this->db->dbprefix('products')}.brand = {$this->db->dbprefix('brands')}.id)";
            $this->db->select("{$this->db->dbprefix('brands')}.*, {$pc} AS product_count", false)->order_by('name');
            $this->db->having("product_count >", 0);
        }
        return $this->db->get('brands')->result();
    }

    public function getBrandBySlug($slug)
    {
        return $this->db->get_where('brands', ['slug' => $slug], 1)->row();
    }

    public function getUserByEmail($email)
    {
        return $this->db->get_where('users', ['email' => $email], 1)->row();
    }

    public function getUserByFacebookId($value)
    {
        return $this->db->get_where('users', ['facebook_id' => $value], 1)->row();
    }

    public function getAllPages()
    {
        $this->db->select('name, slug')->order_by('order_no asc');
        return $this->db->get_where("pages", ['active' => 1])->result();
    }

    public function getPageBySlug($slug)
    {
        return $this->db->get_where('pages', ['slug' => $slug], 1)->row();
    }

    public function getFeaturedProducts($limit = 18, $promo = true)
    {
        $this->db->select("{$this->db->dbprefix('products')}.id as id, {$this->db->dbprefix('products')}.name as name, {$this->db->dbprefix('products')}.code as code, {$this->db->dbprefix('products')}.image as image, {$this->db->dbprefix('products')}.slug as slug, {$this->db->dbprefix('products')}.price, quantity, type, promotion, promo_price, start_date, end_date, b.name as brand_name, b.slug as brand_slug, c.name as category_name, c.slug as category_slug")
        ->join('brands b', 'products.brand=b.id', 'left')
        ->join('categories c', 'products.category_id=c.id', 'left')
        ->where('products.featured', 1)
        ->where('hide !=', 1)
        ->limit($limit);

        $sp = $this->getSpecialPrice();
        if ($sp->cgp) {
            $this->db->select("cgp.price as special_price", false)->join($sp->cgp, 'products.id=cgp.product_id', 'left');
        } elseif ($sp->wgp) {
            $this->db->select("wgp.price as special_price", false)->join($sp->wgp, 'products.id=wgp.product_id', 'left');
        }

        if ($promo) {
            $this->db->order_by('id desc');
        }
        $this->db->order_by('RAND()');
        return $this->db->get("products")->result();
    }

    public function getProducts($filters = [])
    {
        $this->db->select("{$this->db->dbprefix('products')}.id as id, {$this->db->dbprefix('products')}.name as name, {$this->db->dbprefix('products')}.code as code, {$this->db->dbprefix('products')}.image as image, {$this->db->dbprefix('products')}.slug as slug, {$this->db->dbprefix('products')}.price, {$this->db->dbprefix('warehouses_products')}.quantity as quantity, type, promotion, promo_price, start_date, end_date, product_details as details, product_details as is_ordered")
        ->from('products')
        ->join('warehouses_products', 'products.id=warehouses_products.product_id', 'left')
        ->where('warehouses_products.warehouse_id', $this->shop_settings->warehouse)
        ->group_by('products.id');

        $sp = $this->getSpecialPrice();
        if ($sp->cgp) {
            $this->db->select("cgp.price as special_price", false)->join($sp->cgp, 'products.id=cgp.product_id', 'left');
        } elseif ($sp->wgp) {
            $this->db->select("wgp.price as special_price", false)->join($sp->wgp, 'products.id=wgp.product_id', 'left');
        }

        $this->db->where('hide !=', 1)
        ->limit($filters['limit'], $filters['offset']);
        if (!empty($filters)) {
            if (!empty($filters['promo'])) {
                $today = date('Y-m-d');
                $this->db->where('promotion', 1)->where('start_date <=', $today)->where('end_date >=', $today);
            }
            if (!empty($filters['featured'])) {
                $this->db->where('featured', 1);
            }
            if (!empty($filters['query'])) {
                $this->db->group_start()->like('name', $filters['query'], 'both')->or_like('code', $filters['query'], 'both')->group_end();
            }
            if (!empty($filters['category'])) {
                $this->db->where('category_id', $filters['category']['id']);
            }
            if (!empty($filters['subcategory'])) {
                $this->db->where('subcategory_id', $filters['subcategory']['id']);
            }
            if (!empty($filters['brand'])) {
                $this->db->where('brand', $filters['brand']['id']);
            }
            if (!empty($filters['min_price'])) {
                $this->db->where('price >=', $filters['min_price']);
            }
            if (!empty($filters['max_price'])) {
                $this->db->where('price <=', $filters['max_price']);
            }
            if (!empty($filters['in_stock'])) {
                $this->db->group_start()->where('warehouses_products.quantity >=', 1)->or_where('type !=', 'standard')->group_end();
            }
            if (!empty($filters['sorting'])) {
                $sort = explode('-', $filters['sorting']);
                $this->db->order_by($sort[0], $this->db->escape_str($sort[1]));
            } else {
                $this->db->order_by('name asc');
            }
        } else {
            $this->db->order_by('name asc');
        }

        $temp2 = $this->db->get()->result_array();

        $temp = $this->site->api_select_some_fields_with_where(
            "
                    DISTINCT t1.product_id as product_id
            ",
            "sma_sale_items as t1 inner join sma_sales as t2 on t2.id = t1.sale_id",
            "t2.created_by = ".$this->session->userdata('user_id'),
            "arr"
        );

        if (is_array($temp2)) {
            if (count($temp2) > 0) {
                for ($i=0;$i<count($temp2);$i++) {
                    if (is_array($temp)) {
                        if (count($temp) > 0) {
                            for ($i2=0;$i2<count($temp);$i2++) {
                                $temp2[$i]['is_ordered'] = 0;
                                if ($temp2[$i]['id'] == $temp[$i2]['product_id']) {
                                    $temp2[$i]['is_ordered'] = 1;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        $temp = $this->site->api_select_some_fields_with_where(
            "
                    t1.product_id
            ",
            "sma_wishlist as t1 inner join sma_products as t2 on t2.id = t1.product_id",
            "t1.user_id = ".$this->session->userdata('user_id'),
            "arr"
        );
        if (is_array($temp)) {
            if (count($temp) > 0) {
                for ($i=0;$i<count($temp);$i++) {
                    for ($j=0;$j<count($temp2);$j++) {
                        if ($temp2[$j]['id'] == $temp[$i]['product_id']) {
                            $temp2[$j]['is_favorite'] = 1;
                            break;
                        }
                    }
                }
            }
        }

        $temp3 = array();
        if (is_array($temp2)) {
            if (count($temp2) > 0) {
                $j = 0;
                for ($i=0;$i<count($temp2);$i++) {
                    $config_data = array(
                    'id' => $temp2[$i]['id'],
                );
                    $temp_hide = $this->api_get_product_display($config_data);
                    if ($temp_hide['result'] == 'hide') {
                        $temp2[$i]['is_hided'] = 1;
                    } else {
                        $temp2[$i]['is_hided'] = 0;
                        $temp3[$j] = $temp2[$i];
                        $j++;
                    }
                }
            }
        }
        return $temp3;
    }

    public function getProductsCount($filters = [])
    {
        $this->db->select("{$this->db->dbprefix('products')}.id as id")
        ->join('warehouses_products', 'products.id=warehouses_products.product_id', 'left')
        ->where('warehouses_products.warehouse_id', $this->shop_settings->warehouse)
        ->group_by('products.id');

        $sp = $this->getSpecialPrice();
        if ($sp->cgp) {
            $this->db->select("cgp.price as special_price", false)->join($sp->cgp, 'products.id=cgp.product_id', 'left');
        } elseif ($sp->wgp) {
            $this->db->select("wgp.price as special_price", false)->join($sp->wgp, 'products.id=wgp.product_id', 'left');
        }

        if (!empty($filters)) {
            if (!empty($filters['promo'])) {
                $today = date('Y-m-d');
                $this->db->where('promotion', 1)->where('start_date <=', $today)->where('end_date >=', $today);
            }
            if (!empty($filters['featured'])) {
                $this->db->where('featured', 1);
            }
            if (!empty($filters['query'])) {
                $this->db->group_start()->like('name', $filters['query'], 'both')->or_like('code', $filters['query'], 'both')->group_end();
            }
            if (!empty($filters['category'])) {
                $this->db->where('category_id', $filters['category']['id']);
            }
            if (!empty($filters['subcategory'])) {
                $this->db->where('subcategory_id', $filters['subcategory']['id']);
            }
            if (!empty($filters['brand'])) {
                $this->db->where('brand', $filters['brand']['id']);
            }
            if (!empty($filters['min_price'])) {
                $this->db->where('price >=', $filters['min_price']);
            }
            if (!empty($filters['max_price'])) {
                $this->db->where('price <=', $filters['max_price']);
            }
            $this->db->where("getTranslate(add_ons,'hide_customer_list_selected','".f_separate."','".v_separate."') not like '%-".$this->session->userdata('company_id')."-%'");
            if (!empty($filters['in_stock'])) {
                $this->db->group_start()->where('warehouses_products.quantity >=', 1)->or_where('type !=', 'standard')->group_end();
            }
        }
        $this->db->where('hide !=', 1);

        return $this->db->count_all_results("products");
    }

    public function getWishlist($limit = null, $offset = null)
    {
        $this->db->where('user_id', $this->session->userdata('user_id'));
        if ($limit != '') {
            $this->db->order_by('id', 'desc')->limit($limit, $offset);
        }
        return $no ? $this->db->count_all_results('wishlist') : $this->db->get('wishlist')->result();
    }

    public function getProductBySlug($slug)
    {
        $this->db->select("{$this->db->dbprefix('products')}.*, details as is_ordered");
        $this->db->where("getTranslate(add_ons,'hide_customer_list_selected','".f_separate."','".v_separate."') not like '%-".$this->session->userdata('company_id')."-%' and getTranslate(add_ons,'display','".f_separate."','".v_separate."') != 'no'");

        $sp = $this->getSpecialPrice();
        if ($sp->cgp) {
            $this->db->select("cgp.price as special_price", false)->join($sp->cgp, 'products.id=cgp.product_id', 'left');
        } elseif ($sp->wgp) {
            $this->db->select("wgp.price as special_price", false)->join($sp->wgp, 'products.id=wgp.product_id', 'left');
        }
        
        return $this->db->get_where('products', ['slug' => $slug, 'hide !=' => 1], 1)->row();
    }

    public function getProductByID($id)
    {
        $this->db->select("{$this->db->dbprefix('products')}.id as id, {$this->db->dbprefix('products')}.name as name, {$this->db->dbprefix('products')}.code as code, {$this->db->dbprefix('products')}.image as image, {$this->db->dbprefix('products')}.slug as slug, price, quantity, type, promotion, promo_price, start_date, end_date, product_details as details");
        return $this->db->get_where('products', ['id' => $id], 1)->row();
    }

    public function getProductVariants($product_id, $warehouse_id = null, $all = null)
    {
        if (!$warehouse_id) {
            $warehouse_id = $this->shop_settings->warehouse;
        }
        $wpv = "( SELECT option_id, warehouse_id, quantity from {$this->db->dbprefix('warehouses_products_variants')} WHERE product_id = {$product_id}) FWPV";
        $this->db->select('product_variants.id as id, product_variants.name as name, product_variants.price as price, product_variants.quantity as total_quantity, FWPV.quantity as quantity', false)
            ->join($wpv, 'FWPV.option_id=product_variants.id', 'left')
            //->join('warehouses', 'warehouses.id=product_variants.warehouse_id', 'left')
            ->where('product_variants.product_id', $product_id)
            ->group_by('product_variants.id');

        if (! $this->Settings->overselling && ! $all) {
            $this->db->where('FWPV.warehouse_id', $warehouse_id);
            $this->db->where('FWPV.quantity >', 0);
        }
        return $this->db->get('product_variants')->result_array();
    }

    public function getProductVariantByID($id)
    {
        return $this->db->get_where('product_variants', ['id' => $id])->row();
    }

    public function getProductVariantWarehouseQty($option_id, $warehouse_id)
    {
        return $this->db->get_where('warehouses_products_variants', array('option_id' => $option_id, 'warehouse_id' => $warehouse_id), 1)->row();
    }

    public function getAddressByID($id)
    {
        return $this->db->get_where('addresses', ['id' => $id], 1)->row();
    }

    public function addSale($data, $items, $customer, $address)
    {
        
        if ($data['grand_total'] > 0) {
            $cost = $this->site->costing($items);
            if (is_array($customer) && !empty($customer)) {
                $this->db->insert('companies', $customer);
                $data['customer_id'] = $this->db->insert_id();
            }

            if (is_array($address) && !empty($address)) {
                $address['company_id'] = $data['customer_id'];
                $this->db->insert('addresses', $address);
                $data['address_id'] = $this->db->insert_id();
            }
            if ($data['address_id'] > 0) {
                $config_data = array(
                    'table_name' => 'sma_addresses',
                    'select_table' => 'sma_addresses',
                    'translate' => '',
                    'description' => '',
                    'select_condition' => "id = ".$data['address_id'],
                );
                $temp = $this->api_helper->api_select_data_v2($config_data);
                if ($temp[0]['note_to_driver'] != '')
                    $data['note'] .= '
                        <br><b>'.lang('Note_to_Driver').'</b>
                        : '.$temp[0]['note_to_driver'].'
                    ';
            }
            else {
                $config_data = array(
                    'table_name' => 'sma_companies',
                    'select_table' => 'sma_companies',
                    'translate' => '',
                    'description' => '',
                    'select_condition' => "id = ".$data['customer_id'],
                );
                $temp = $this->api_helper->api_select_data_v2($config_data);
                if ($temp[0]['note_to_driver'] != '')
                    $data['note'] .= '
                        <br><b>'.lang('Note_to_Driver').'</b>
                        : '.$temp[0]['note_to_driver'].'
                    ';                
            }
            $data['payment_status'] == 'partial';
            if ($this->db->insert('sales', $data)) {
                
                $sale_id = $this->db->insert_id();

                foreach ($items as $item) {
                    $item['sale_id'] = $sale_id;
                    $this->db->insert('sale_items', $item);
                    $sale_item_id = $this->db->insert_id();
                    if ($data['sale_status'] == 'completed') {
                        $item_costs = $this->site->item_costing($item);
                        foreach ($item_costs as $item_cost) {
                            if (isset($item_cost['date']) || isset($item_cost['pi_overselling'])) {
                                $item_cost['sale_item_id'] = $sale_item_id;
                                $item_cost['sale_id'] = $sale_id;
                                $item_cost['date'] = date('Y-m-d', strtotime($data['date']));
                                if (! isset($item_cost['pi_overselling'])) {
                                    $this->db->insert('costing', $item_cost);
                                }
                            } else {
                                foreach ($item_cost as $ic) {
                                    $ic['sale_item_id'] = $sale_item_id;
                                    $ic['sale_id'] = $sale_id;
                                    $ic['date'] = date('Y-m-d', strtotime($data['date']));
                                    
                                    if (! isset($ic['pi_overselling'])) {
                                        $this->db->insert('costing', $ic);
                                    }
                                }
                            }
                        }
                    }
                }
                
                $config_data = array(
                    'id' => $sale_id,
                );
                $this->site->api_calculate_sale_supplier($config_data);
                

                $config_data = array(
                    'type' => 'sale',
                    'date' => $data['date'],
                    'customer_id' => $data['customer_id'],
                    'update' => 1,
                );
                $temp = $this->site->api_calculate_reference_no($config_data);
                $temp = array(
                    'reference_no' => $temp['reference_no'],
                    'sale_status' => 'preparing',
                );
                $this->db->update('sma_sales', $temp,"id = ".$sale_id);  
                
                if ($_POST['payment_method'] == 'aba') {
                    
                    $payment = array(
                        'date' => date('Y-m-d H:i:s'),
                        'sale_id' => $sale_id,
                        'amount' => $data['grand_total'],
                        'paid_by' => 'aba',
                        'type' => '',
                        'note' => '',
                        'add_ons' => 'initial_first_add_ons:{}:customer_account_number:{'.$_POST['your_bank_account_number'].'}:company_account_number:{'.$_POST['our_company_bank_account_number'].'}:transfer_reference_number:{'.$_POST['transfer_reference_number'].'}:transfer_amount:{'.$_POST['transfer_amount'].'}:pipay_transaction_id:{}:wing_transaction_id:{}:aba_qr_customer_account_number:{}:aba_qr_company_account_number:{}:aba_qr_transfer_reference_number:{}:aba_qr_transfer_amount:{}:payway_transaction_id:{}:aba_daiki_transaction_id:{}:wing_daiki_transaction_id:{}:acode_daiki_transaction_id:{}:'
                    );
                    $this->db->insert('sma_payments', $payment);

                    
                }

                return $sale_id;
            }
        }
        return false;
    }

    public function api_reduce_stock_from_consignment($config_data)
    {
        $temp = $this->site->api_select_some_fields_with_where(
            "
            *     
            ",
            "sma_consignment_items",
            "sale_id = ".$config_data['sale_id'],
            "arr"
        );
        if (is_array($temp)) {
            if (count($temp) > 0) {
                for ($i=0;$i<count($temp);$i++) {
                    $config_data_2 = array(
                'product_id' => $temp[$i]['product_id'],
                'type' => 'subtraction',
                'quantity' => $temp[$i]['quantity'],
                'warehouse_id' => $temp[$i]['warehouse_id'],
                'option_id' => $temp[$i]['option_id'],
            );
                    $this->site->api_update_quantity_stock($config_data_2);
                }
            }
        }
    }
        
    public function getOrder($clause)
    {
        if ($this->loggedIn) {
            $this->db->order_by('id desc');
            $sale = $this->db->get_where('sales', ['id' => $clause['id']], 1)->row();
            return ($sale->customer_id == $this->session->userdata('company_id')) ? $sale : false;
        } elseif (!empty($clause['hash'])) {
            return $this->db->get_where('sales', $clause, 1)->row();
        }
        return false;
    }
    public function getConsignment($clause)
    {
        if ($this->loggedIn) {
            $this->db->order_by('id desc');
            $sale = $this->db->get_where('consignment', ['id' => $clause['id']], 1)->row();
            return ($sale->customer_id == $this->session->userdata('company_id')) ? $sale : false;
        } elseif (!empty($clause['hash'])) {
            return $this->db->get_where('consignment', $clause, 1)->row();
        }
        return false;
    }
    public function getOrders($limit, $offset)
    {
        if ($this->loggedIn) {
            $this->db->select("sales.*, deliveries.status as delivery_status")
            ->join('deliveries', 'deliveries.sale_id=sales.id', 'left')
            ->order_by('id', 'desc')->limit($limit, $offset);
            return $this->db->get_where('sales', ['sales.created_by' => $this->session->userdata('user_id')])->result();
        }
        return false;
    }
    
    public function getOrdersCount()
    {
        $this->db->where('created_by', $this->session->userdata('user_id'));
        return $this->db->count_all_results("sales");
    }

    public function getOrderItems($sale_id)
    {
        $this->db->select('sale_items.*, tax_rates.code as tax_code, tax_rates.name as tax_name, tax_rates.rate as tax_rate, products.image, products.details as details, product_variants.name as variant, products.hsn_code as hsn_code,  products.second_name as second_name')
            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('product_variants', 'product_variants.id=sale_items.option_id', 'left')
            ->join('tax_rates', 'tax_rates.id=sale_items.tax_rate_id', 'left')
            ->group_by('sale_items.id')
            ->order_by('id', 'asc');

        return $this->db->get_where('sale_items', ['sale_id' => $sale_id])->result();
    }

    public function getConsignmentItems($sale_id)
    {
        $this->db->select('consignment_items.*, tax_rates.code as tax_code, tax_rates.name as tax_name, tax_rates.rate as tax_rate, products.image, products.details as details, product_variants.name as variant, products.hsn_code as hsn_code,  products.second_name as second_name')
            ->join('products', 'products.id=consignment_items.product_id', 'left')
            ->join('product_variants', 'product_variants.id=consignment_items.option_id', 'left')
            ->join('tax_rates', 'tax_rates.id=consignment_items.tax_rate_id', 'left')
            ->group_by('consignment_items.id')
            ->order_by('id', 'asc');

        return $this->db->get_where('consignment_items', ['sale_id' => $sale_id])->result();
    }

    public function getQuote($clause)
    {
        if ($this->loggedIn) {
            $this->db->order_by('id desc');
            $sale = $this->db->get_where('quotes', ['id' => $clause['id']], 1)->row();
            return ($sale->customer_id == $this->session->userdata('company_id')) ? $sale : false;
        } elseif (!empty($clause['hash'])) {
            return $this->db->get_where('quotes', $clause, 1)->row();
        }
        return false;
    }

    public function getQuotes($limit, $offset)
    {
        if ($this->loggedIn) {
            $this->db->order_by('id', 'desc')->limit($limit, $offset);
            return $this->db->get_where('quotes', ['customer_id' => $this->session->userdata('company_id')])->result();
        }
        return false;
    }

    public function getQuotesCount()
    {
        $this->db->where('customer_id', $this->session->userdata('company_id'));
        return $this->db->count_all_results("quotes");
    }

    public function getQuoteItems($quote_id)
    {
        $this->db->select('quote_items.*, tax_rates.code as tax_code, tax_rates.name as tax_name, tax_rates.rate as tax_rate, products.image, products.details as details, product_variants.name as variant, products.hsn_code as hsn_code,  products.second_name as second_name')
            ->join('products', 'products.id=quote_items.product_id', 'left')
            ->join('product_variants', 'product_variants.id=quote_items.option_id', 'left')
            ->join('tax_rates', 'tax_rates.id=quote_items.tax_rate_id', 'left')
            ->group_by('quote_items.id')
            ->order_by('id', 'asc');
        return $this->db->get_where('quote_items', ['quote_id' => $quote_id])->result();
    }

    public function getProductComboItems($pid)
    {
        $this->db->select($this->db->dbprefix('products') . '.id as id, ' . $this->db->dbprefix('products') . '.code as code, ' . $this->db->dbprefix('combo_items') . '.quantity as qty, ' . $this->db->dbprefix('products') . '.name as name, ' . $this->db->dbprefix('combo_items') . '.unit_price as price')->join('products', 'products.code=combo_items.item_code', 'left')->group_by('combo_items.id');
        $q = $this->db->get_where('combo_items', array('product_id' => $pid));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
        return false;
    }

    public function getProductPhotos($id)
    {
        $q = $this->db->get_where("product_photos", array('product_id' => $id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getAllWarehouseWithPQ($product_id, $warehouse_id = null)
    {
        if (!$warehouse_id) {
            $warehouse_id = $this->shop_settings->warehouse;
        }
        $this->db->select('' . $this->db->dbprefix('warehouses') . '.*, ' . $this->db->dbprefix('warehouses_products') . '.quantity, ' . $this->db->dbprefix('warehouses_products') . '.rack')
            ->join('warehouses_products', 'warehouses_products.warehouse_id=warehouses.id', 'left')
            ->where('warehouses_products.product_id', $product_id)
            ->where('warehouses_products.warehouse_id', $warehouse_id)
            ->group_by('warehouses.id');
        return $this->db->get('warehouses')->row();
    }

    public function getProductOptionsWithWH($product_id, $warehouse_id = null)
    {
        if (!$warehouse_id) {
            $warehouse_id = $this->shop_settings->warehouse;
        }
        $this->db->select($this->db->dbprefix('product_variants') . '.*, ' . $this->db->dbprefix('warehouses') . '.name as wh_name, ' . $this->db->dbprefix('warehouses') . '.id as warehouse_id, ' . $this->db->dbprefix('warehouses_products_variants') . '.quantity as wh_qty')
            ->join('warehouses_products_variants', 'warehouses_products_variants.option_id=product_variants.id', 'left')
            ->join('warehouses', 'warehouses.id=warehouses_products_variants.warehouse_id', 'left')
            ->group_by(['' . $this->db->dbprefix('product_variants') . '.id', '' . $this->db->dbprefix('warehouses_products_variants') . '.warehouse_id'])
            ->order_by('product_variants.id');
        return $this->db->get_where('product_variants', ['product_variants.product_id' => $product_id, 'warehouses.id' => $warehouse_id, 'warehouses_products_variants.quantity !=' => null])->result();
    }

    public function getProductOptions($product_id)
    {
        return $this->db->get_where('product_variants', array('product_id' => $product_id))->result();
    }

    public function getSaleByID($id)
    {
        return $this->db->get_where('sales', ['id' => $id])->row();
    }

    public function getCompanyByID($id)
    {
        return $this->db->get_where('companies', ['id' => $id])->row();
    }

    public function getPaypalSettings()
    {
        return $this->db->get_where('paypal', ['id' => 1])->row();
    }

    public function getSkrillSettings()
    {
        return $this->db->get_where('skrill', ['id' => 1])->row();
    }

    public function updateCompany($id, $data = array())
    {
        return $this->db->update('companies', $data, ['id' => $id]);
    }

    public function getDownloads($limit, $offset, $product_id = null)
    {
        if ($this->loggedIn) {
            $this->db->select("{$this->db->dbprefix('sale_items')}.product_id, {$this->db->dbprefix('sale_items')}.product_code, {$this->db->dbprefix('sale_items')}.product_name, {$this->db->dbprefix('sale_items')}.product_type")
            ->distinct()
            ->join('sale_items', 'sales.id=sale_items.sale_id', 'left')
            ->where('sales.sale_status', 'completed')->where('sales.payment_status', 'paid')
            ->where('sales.customer_id', $this->session->userdata('company_id'))
            ->where('sale_items.product_type', 'digital')
            ->order_by('sales.id', 'desc')->limit($limit, $offset);
            if ($product_id) {
                $this->db->where('sale_items.product_id', $product_id);
            }
            return $this->db->get('sales')->result();
        }
        return false;
    }

    public function getDownloadsCount()
    {
        $this->db->select("{$this->db->dbprefix('sale_items')}.product_id, {$this->db->dbprefix('sale_items')}.product_code, {$this->db->dbprefix('sale_items')}.product_name, {$this->db->dbprefix('sale_items')}.product_type")
        ->distinct()
            ->join('sale_items', 'sales.id=sale_items.sale_id', 'left')
            ->where('sales.sale_status', 'completed')->where('sales.payment_status', 'paid')
            ->where('sales.customer_id', $this->session->userdata('company_id'))
            ->where('sale_items.product_type', 'digital');
        return $this->db->count_all_results("sales");
    }

    public function updateProductViews($id, $views)
    {
        $views = is_numeric($views) ? ($views+1) : 1;
        return $this->db->update('products', ['views' => $views], ['id' => $id]);
    }

    public function getProductForCart($id)
    {
        $this->db->select("{$this->db->dbprefix('products')}.*")->where('products.id', $id);
        $sp = $this->getSpecialPrice();
        if ($sp->cgp) {
            $this->db->select("cgp.price as special_price", false)->join($sp->cgp, 'products.id=cgp.product_id', 'left');
        } elseif ($sp->wgp) {
            $this->db->select("wgp.price as special_price", false)->join($sp->wgp, 'products.id=wgp.product_id', 'left');
        }
        $q = $this->db->get('products', 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getOtherProducts($id, $category_id, $brand)
    {
        $this->db->select("{$this->db->dbprefix('products')}.id as id, {$this->db->dbprefix('products')}.name as name, {$this->db->dbprefix('products')}.code as code, {$this->db->dbprefix('products')}.image as image, {$this->db->dbprefix('products')}.slug as slug, {$this->db->dbprefix('products')}.price, quantity, type, promotion, promo_price, start_date, end_date, b.name as brand_name, b.slug as brand_slug, c.name as category_name, c.slug as category_slug")
        ->join('brands b', 'products.brand=b.id', 'left')
        ->join('categories c', 'products.category_id=c.id', 'left')
        ->where('category_id', $category_id)->where('brand', $brand)
        ->where('products.id !=', $id)->where('hide !=', 1)
        //->order_by('rand()')->limit(4);
        ->order_by('id desc')->limit(4);

        $sp = $this->getSpecialPrice();
        if ($sp->cgp) {
            $this->db->select("cgp.price as special_price", false)->join($sp->cgp, 'products.id=cgp.product_id', 'left');
        } elseif ($sp->wgp) {
            $this->db->select("wgp.price as special_price", false)->join($sp->wgp, 'products.id=wgp.product_id', 'left');
        }

        $temp2 = $this->db->get('products')->result();
        if (is_array($temp2)) {
            if (count($temp2) > 0) {
                for ($i=0;$i<count($temp2);$i++) {
                    $config_data = array(
                    'id' => $temp2[$i]->id,
                );
                    $temp_hide = $this->api_get_product_display($config_data);
                    if ($temp_hide['result'] == 'hide') {
                        unset($temp2[$i]);
                    }
                }
                $temp_arr = array_values($temp2);
                $temp2 = $temp_arr;
            }
        }


        return $temp2;
    }

    public function getCompanyByEmail($email)
    {
        $q = $this->db->get_where('companies', array('email' => $email), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function isPromo()
    {
        $today = date('Y-m-d');
        $this->db->where('promotion', 1)->where('start_date <=', $today)->where('end_date >=', $today);
        return $this->db->count_all_results("products");
    }
    
    public function getProductsByCategory($category_name = null, $user_id = 0, $limit = 16)
    {
        $this->db->select("
            {$this->db->dbprefix('products')}.id as id, 
            {$this->db->dbprefix('products')}.name as name, 
            {$this->db->dbprefix('products')}.code as code, 
            {$this->db->dbprefix('products')}.image as image, 
            {$this->db->dbprefix('products')}.slug as slug, 
            {$this->db->dbprefix('products')}.price, 
            {$this->db->dbprefix('products')}.quantity, 
            type, promotion, promo_price, start_date, end_date, b.name as brand_name, b.slug as brand_slug, c.name as category_name, c.slug as category_slug,
		(
			SELECT vc.sale_id FROM v_customer_products vc WHERE vc.user_id ={$user_id} AND vc.product_id = sma_products.id
		) AS sale_id")
        ->join('brands b', 'products.brand=b.id', 'left')
        ->join('categories c', 'products.category_id=c.id', 'left')
        ->where('c.name', $category_name)
        ->where('hide !=', 1)
        ->order_by('sale_id DESC')
        ->limit($limit);

        $query = $this->db->get("products");
        if ($query) {
            return $query->result();
        }
        return false;
    }
    public function getProductsByCategoryV2($config_data) {
        $category_id = $config_data['category_id'];
        $featured = $config_data['featured'];

        $condition_category = '(t1.category_id = '.$category_id;
        $temp = $this->site->api_select_some_fields_with_where(
            "
                    id
            ",
            "sma_categories",
            "parent_id = ".$category_id,
            "arr"
        );
        if (is_array($temp)) {
            if (count($temp) > 0) {
                for ($i=0;$i<count($temp);$i++) {
                    $condition_category .= " or t1.category_id = ".$temp[$i]['id'];
                }
            }
        }
        $condition_category .= ')';

        $condition_product = '';
        // if ($config_data['user_id'] > 0 && $this->api_shop_setting[0]['require_login'] == 1) {
        //     $temp = $this->site->api_select_some_fields_with_where(
        //         "
        //                 DISTINCT t1.product_id
        //         ",
        //         "sma_sale_items as t1 inner join sma_sales as t2 on t2.id = t1.sale_id",
        //         "t2.created_by = ".$config_data['user_id'],
        //         "arr"
        //     );            
        //     if (count($temp) > 0) {
        //         $condition_product = 'and (t1.id = '.$temp[0]['product_id'];
        //         for ($i=1;$i<count($temp);$i++) {
        //             $condition_product .= " or t1.id = ".$temp[$i]['product_id'];
        //         }
        //         $condition_product .= ')';
        //     }
        // }        
        
        $limit = '';
        if ($config_data['user_id'] <= 0 && $config_data['limit'] != '')
            $limit = " limit ".$config_data['limit'];
        
        $select_data = $this->site->api_select_some_fields_with_where(
            "
                    t1.*,
                    (SELECT SUM(c.quantity) FROM sma_sale_items c WHERE c.product_id = t1.id) as total_sale_quantity
            ",
            "sma_products as t1",
            $condition_category." ".$condition_product." and t1.hide != 1 and t1.slug != '' order by total_sale_quantity desc ".$limit,
            "obj"
        );
        
        if (count($select_data) > 0 && $config_data['user_id'] > 0) {
            
            $temp = $this->site->api_select_some_fields_with_where(
                "DISTINCT t1.product_id",
                "sma_sale_items as t1 inner join sma_sales as t2 on t2.id = t1.sale_id",
                "t2.created_by = ".$config_data['user_id'],
                "arr"
            );
            if (is_array($temp)) {
                if (count($temp) > 0) {
                    for ($i=0;$i<count($temp);$i++) {
                        for ($j=0;$j<count($select_data);$j++) {
                            if ($select_data[$j]->id == $temp[$i]['product_id']) {
                                $select_data[$j]->is_ordered = 1;
                                break;
                            }
                        }
                    }
                }
            }

            $temp = $this->site->api_select_some_fields_with_where(
                "t1.product_id",
                "sma_wishlist as t1 inner join sma_products as t2 on t2.id = t1.product_id",
                "t1.user_id = ".$config_data['user_id'],
                "arr"
            );
            if (is_array($temp)) {
                if (count($temp) > 0) {
                    for ($i=0;$i<count($temp);$i++) {
                        for ($j=0;$j<count($select_data);$j++) {
                            if ($select_data[$j]->id == $temp[$i]['product_id']) {
                                $select_data[$j]->is_favorite = 1;
                                break;
                            }
                        }
                    }
                }
            }
        }

        $select_data_2 = array();
        $k = 0;
        for ($i=0;$i<count($select_data);$i++) {
            $config_data_2 = array(
                'id' => $select_data[$i]->id,
            );
            $temp_hide = $this->api_get_product_display($config_data_2);
            if ($temp_hide['result'] != 'hide') {
                $select_data_2[$k] = $select_data[$i];
                $k++;
            }
        }

        $temp = array_slice($select_data_2, 0, $config_data['limit']);
        if (count($temp) > 0) {            
            return $temp;
        }
        return false;
    }
    
    public function getFeaturedProducts_v2($config_data)
    {
        $condition = '';
        $category_id = $config_data['category_id'];
        $user_id = $config_data['user_id'];
        $limit = $config_data['limit'];
        if ($limit != '')
            $limit = "limit ".$limit;

        if ($config_data['featured'] == 1)
            $condition .= " and t1.featured = 1";
        if ($config_data['promotion'] == 1)
            $condition .= " and t1.promotion = 1";

        if ($this->api_shop_setting[0]['air_base_url'] == base_url())
            $condition .= " and t3.add_ons like '%:air_display:{yes}:%' ";
        $select_data = $this->site->api_select_some_fields_with_where(
            "
                t1.*, (SELECT SUM(c.quantity) FROM sma_sale_items c WHERE c.product_id = t1.id) as total_sale_quantity                    
            ",
            "sma_products as t1",
            "t1.id > 0 ".$condition." and t1.hide != 1 order by total_sale_quantity desc ".$limit,
            "obj"
        );

        $k = 0;
        $k_2 = 0;
        $select_data_2 = array();                
        $select_data_3 = array();                
        if (is_array($select_data)) {
            if (count($select_data) > 0) {
                $temp = $this->site->api_select_some_fields_with_where(
                    "
                        DISTINCT t1.product_id
                ",
                    "sma_sale_items as t1 inner join sma_sales as t2 on t2.id = t1.sale_id",
                    "t2.created_by = ".$user_id,
                    "arr"
                );
                if (is_array($temp)) {
                    if (count($temp) > 0) {
                        for ($i=0;$i<count($temp);$i++) {
                            for ($j=0;$j<count($select_data);$j++) {
                                if ($select_data[$j]->id == $temp[$i]['product_id']) {
                                    $select_data[$j]->is_ordered = 1;
                                    break;
                                }
                            }
                        }
                    }
                }

                $temp = $this->site->api_select_some_fields_with_where(
                    "
                        t1.product_id
                ",
                    "sma_wishlist as t1 inner join sma_products as t2 on t2.id = t1.product_id",
                    "t1.user_id = ".$user_id,
                    "arr"
                );
                if (is_array($temp)) {
                    if (count($temp) > 0) {
                        for ($i=0;$i<count($temp);$i++) {
                            for ($j=0;$j<count($select_data);$j++) {
                                if ($select_data[$j]->id == $temp[$i]['product_id']) {
                                    $select_data[$j]->is_favorite = 1;
                                    break;
                                }
                            }
                        }
                    }
                }

                if ($select_data[0]->id > 0) {
                    for ($i_3=0;$i_3<count($select_data);$i_3++) {
                        $config_data_2 = array(
                            'id' => $select_data[$i_3]->id,
                        );
                        $temp_hide = $this->api_get_product_display($config_data_2);
                        if ($temp_hide['result'] == 'hide') {
                            unset($select_data[$i_3]);
                        }
                    }
                }


                for ($i_3=0;$i_3<count($select_data);$i_3++) {
                    if ($select_data[$i_3]->id > 0) {
                        $select_data_2[$k] = $select_data[$i_3];
                        $k++;
                    }
                    if ($config_data['promotion'] != 1) {
                        if ($k == $config_data['limit'] && $config_data['limit'] != '')
                            break;
                    }
                }           
                  

                if ($config_data['promotion'] == 1) {
                    for ($i_3=0;$i_3<count($select_data_2);$i_3++) {
                        if ($select_data_2[$i_3]->id > 0) {
                            $config_data_2 = array(
                                'id' => $select_data_2[$i_3]->id,
                                'customer_id' => $this->session->userdata('company_id'),
                            );
                            $temp = $this->site->api_calculate_product_price($config_data_2);  
                            if ($temp['promotion']['price'] <= 0) {
                                unset($select_data_2[$i_3]);
                            }
                        }
                    }
               
                    for ($i_3=0;$i_3<count($select_data_2);$i_3++) {
                        if ($select_data_2[$i_3]->id > 0) {
                            $select_data_3[$k_2] = $select_data_2[$i_3];
                            $k_2++;
                        }
                        if ($k_2 == $config_data['limit'] && $config_data['limit'] != '')
                            break;
                    }  
                    
                }
                else
                    $select_data_3 = $select_data_2;

                return $select_data_3;
            }
        }


        return false;
    }

    public function getInvoicePayments_v2($id)
    {
        if ($id != '') {
            $temp_field = $this->site->api_get_condition_add_ons_field("add_ons", "sma_add_ons", "table_name = 'sma_payments'");
            $temp = $this->site->api_select_some_fields_with_where(
                "*
                ".$temp_field,
                "sma_payments",
                "sale_id = ".$id." order by date desc",
                "arr"
            );
            return $temp;
        } else {
            return false;
        }
    }

    public function getOrderHistory($config_data)
    {
        if ($config_data['limit'] != '') {
            $order_by .= ' limit '.$config_data['offset_no'].', '.$config_data['limit'];
        }
        
        $temp = $this->site->api_select_some_fields_with_where(
            "company_id             
            ",
            "sma_users",
            "id = ".$config_data['user_id'],
            "arr"
        );
        $select_data = $this->site->api_select_some_fields_with_where(
            "distinct t2.product_id             
            ",
            "sma_sales as t1 inner join sma_sale_items as t2 on t1.id = t2.sale_id",
            "t1.customer_id = ".$temp[0]['company_id']." order by t1.id desc ".$order_by,
            "arr"
        );
        return $select_data;
    }

    public function apiGetWishlist($config_data)
    {
        if ($config_data['limit'] != '') {
            $order_by .= ' limit '.$config_data['offset_no'].', '.$config_data['limit'];
        }

        $search = $config_data['search'];
        if ($search != '') {
            if (is_numeric($config_data['search'])) {
                $condition .= ' and t2.id = '.$search;
            } else {
                $condition .= " and (t2.code = '".$search."' or t2.name like '%".$search."%')";
            }
        }

        $category = $config_data['category'];
        if ($category != '') {
            $condition .= ' and t2.category_id = '.$category;
        }
        
        $temp_field = $this->site->api_get_condition_add_ons_field("t1.add_ons", "sma_add_ons", "table_name = 'sma_wishlist'");
        $select_data = $this->site->api_select_some_fields_with_where(
            "t1.* 
            ".$temp_field,
            "sma_wishlist as t1 inner join sma_products as t2 on t1.product_id = t2.id",
            "t1.user_id = ".$config_data['user_id']." ".$condition." order by CAST(order_number AS UNSIGNED) asc ".$order_by,
            "arr"
        );

        return $select_data;
    }
    public function api_wishlist_generate_order_number($config_data)
    {
        $temp_field = $this->site->api_get_condition_add_ons_field("add_ons", "sma_add_ons", "table_name = 'sma_wishlist'");
        $select_data = $this->site->api_select_some_fields_with_where(
            "* 
            ".$temp_field,
            "sma_wishlist",
            "user_id = ".$config_data['user_id']." order by CAST(order_number AS UNSIGNED) asc",
            "arr"
        );

        $j = 1;
        if (is_array($select_data)) {
            for ($i=0;$i<count($select_data);$i++) {
                $config_data = array(
                'table_name' => 'sma_wishlist',
                'id_name' => 'id',
                'field_add_ons_name' => 'add_ons',
                'selected_id' => $select_data[$i]['id'],
                'add_ons_title' => 'order_number',
                'add_ons_value' => $j,
            );
                $this->site->api_update_add_ons_field($config_data);
                $j++;
            }
        }
    }
    public function api_get_product_display($config_data)
    {
        $config_data_2 = array(
            'table_name' => 'sma_products', 
            'select_table' => 'sma_products',
            'select_condition' => "id = ".$config_data['id']." and hide != 1 and (add_ons like '%:no_use:{}:%' or add_ons like '%:no_use:{no}:%') and (add_ons like '%:product_status:{}:%' or add_ons like '%:product_status:{unavailable_today}:%')",
        );
        $select_data = $this->site->api_select_data_v2($config_data_2);

        if (count($select_data) > 0) {
            $return['result'] = 'show';
            if ($select_data[0]['hide'] != 2) {
                $temp = $this->site->api_select_some_fields_with_where(
                    "id",
                    "sma_products",
                    "id = ".$select_data[0]['id']." and (add_ons like '%:display:{}:%' or add_ons like '%:display:{yes}:%')",
                    "arr"
                );
                if (is_array($temp)) {
                    if (count($temp) > 0) {
                        $return['result'] = 'show';
                    } else {
                        $return['result'] = 'hide';
                    }
                } 

                // if ($this->session->userdata('company_id') > 0) {
                //     if ($select_data[0]['hide_show_customer_type'] == 'show' && $select_data[0]['show_customer_list_selected'] != '') {
                //         $temp = $this->site->api_select_some_fields_with_where(
                //             "id",
                //             "sma_products",
                //             "id = ".$select_data[0]['id']." and getTranslate(add_ons,'show_customer_list_selected','".f_separate."','".v_separate."') like '%-".$this->session->userdata('company_id')."%'",
                //             "arr"
                //         );
                //         if (is_array($temp)) {
                //             if (count($temp) > 0) {
                //                 $return['result'] = 'show';
                //             } else {
                //                 $return['result'] = 'hide';
                //             }
                //         }
                //     } elseif ($select_data[0]['hide_show_customer_type'] != 'show' && $select_data[0]['hide_customer_list_selected'] != '') {
                        
                //         $temp = $this->site->api_select_some_fields_with_where(
                //             "id",
                //             "sma_products",
                //             "id = ".$select_data[0]['id']." and getTranslate(add_ons,'hide_customer_list_selected','".f_separate."','".v_separate."') not like '%-".$this->session->userdata('company_id')."%'",
                //             "arr"
                //         );
                //         if (is_array($temp)) {
                //             if (count($temp) > 0) {
                //                 $return['result'] = 'show';
                //             } else {
                //                 $return['result'] = 'hide';
                //             }
                //         }
                //     } else {
                //         $temp = $this->site->api_select_some_fields_with_where(
                //             "id",
                //             "sma_products",
                //             "id = ".$select_data[0]['id']." and getTranslate(add_ons,'display','".f_separate."','".v_separate."') != 'no'",
                //             "arr"
                //         );
                //         if (is_array($temp)) {
                //             if (count($temp) > 0) {
                //                 $return['result'] = 'show';
                //             } else {
                //                 $return['result'] = 'hide';
                //             }
                //         }
                //     }
                // }
                // else {
                //     $temp = $this->site->api_select_some_fields_with_where(
                //         "id",
                //         "sma_products",
                //         "id = ".$select_data[0]['id']." and getTranslate(add_ons,'display','".f_separate."','".v_separate."') != 'no'",
                //         "arr"
                //     );
                //     if (is_array($temp)) {
                //         if (count($temp) > 0) {
                //             $return['result'] = 'show';
                //         } else {
                //             $return['result'] = 'hide';
                //         }
                //     }                    
                // }
            }
        }
        else
            $return['result'] = 'hide';
        return $return;
    }

    public function api_get_orders($config_data)
    {
        $condition = '';
        if ($config_data['limit'] != '') {
            $order_by .= ' limit '.$config_data['offset_no'].', '.$config_data['limit'];
        }

        $search = $config_data['search'];

        $temp = $this->site->api_get_customer_order_condition_v2();
        $condition .= $temp;


        $temp_field = $this->site->api_get_condition_add_ons_field("add_ons", "sma_add_ons", "table_name = 'sma_sales'");
        $select_data = $this->site->api_select_some_fields_with_where(
            "* 
            ".$temp_field,
            "sma_sales",
            "id > 0 ".$condition." order by date desc ".$order_by,
            "arr"
        );

        return $select_data;
    }

    public function api_get_product($config_data) {
        $condition = '';
        if ($config_data['search'] != '') {
            $language_array = unserialize(multi_language);
            $temp_condition = '';
            $temp = $language_array[0][0];
            if (strtolower($config_data['lg']) != strtolower($language_array[0][1])) {                
                for ($i=0;$i<count($language_array);$i++) {
                    if (strtolower($config_data['lg']) == strtolower($language_array[$i][1])) {
                        $temp = $language_array[$i][0];
                        break;
                    }
                }
                $temp_condition .= " or getTranslate(t1.translate,'".$temp."','".f_separate."','".v_separate."') LIKE '%".$config_data['search']."%'";
            }

            $condition .= "
                and (t1.code LIKE '%".$config_data['search']."%' 
                or t1.name LIKE '%".$config_data['search']."%')
                ".$temp_condition."
            ";
        }
        
        if ($config_data['subcategory_slug'] != '') {
            $temp = $this->site->api_select_some_fields_with_where(
                "id",
                "sma_categories",
                "slug = '".$config_data['subcategory_slug']."'",
                "arr"
            );
            if ($temp[0]['id'] != '') {
                $condition .= " and t1.subcategory_id = ".$temp[0]['id'];
            }
        } elseif ($config_data['category_slug'] != '') {
            $temp = $this->site->api_select_some_fields_with_where(
                "id",
                "sma_categories",
                "slug = '".$config_data['category_slug']."'",
                "arr"
            );
            if ($temp[0]['id'] != '') {
                $condition .= " and t1.category_id = ".$temp[0]['id'];
            }
        }

        if ($config_data['brand_slug'] != '') {
            $temp = $this->site->api_select_some_fields_with_where(
                "id",
                "sma_brands",
                "slug = '".$config_data['brand_slug']."'",
                "arr"
            );
            if ($temp[0]['id'] != '') {
                $condition .= " and t1.brand = ".$temp[0]['id'];
            }
        }

        if ($this->api_shop_setting[0]['air_base_url'] == base_url())
            $condition .= " and t2.add_ons like '%:air_display:{yes}:%' ";

        if ($_GET['sort_by'] == 'featured')
            $condition .= " and t1.featured = 1 ";

        if ($_GET['sort_by'] == 'promotion-desc' || $_GET['promotion_id'] > 0)
            $condition .= " and t1.promotion = 1 ";
            
        $order_by = '';
        $temp = explode('-', $config_data['sort_by']);

        
        if ($config_data['sort_by'] != '') {
            $order_by .= 'order by t1.'.$temp[0];
            $order_by .= ' '.$temp[1];
        } 
        else {
            $order_by .= 'order by t2.ordering asc, t1.ordering asc';
        }

        //$order_by .= ' limit '.$config_data['offset_no'].', '.$config_data['limit'];

        if ($config_data['selected_id'] > 0) {
            $condition = ' and t1.'.$config_data['table_id']." = ".$config_data['selected_id'];
        }

        $temp_field = $this->site->api_get_condition_add_ons_field("t1.add_ons", "sma_add_ons", "table_name = '".$config_data['table_name']."'");
        if ($config_data['limit'] != '') {
            $select_data = $this->site->api_select_some_fields_with_where(
                "t1.* ".$temp_field,
                "sma_products as t1 inner join sma_categories as t2 on t1.category_id = t2.id",
                "t1.".$config_data['table_id']." > 0 ".$condition." and t1.hide != 1 ".$order_by,
                "arr"
            );
            
            if (is_array($select_data)) {
                if (count($select_data) > 0 && $this->api_shop_setting[0]['require_login'] == 1) {
                    $temp = $this->site->api_select_some_fields_with_where(
                        "DISTINCT t1.product_id",
                        "sma_sale_items as t1 inner join sma_sales as t2 on t2.id = t1.sale_id",
                        "t2.created_by = ".$config_data['user_id'],
                        "arr"
                    );
                    if (is_array($temp)) {
                        if (count($temp) > 0) {
                            for ($i=0;$i<count($temp);$i++) {
                                for ($j=0;$j<count($select_data);$j++) {
                                    if ($select_data[$j]['id'] == $temp[$i]['product_id']) {
                                        $select_data[$j]['is_ordered'] = 1;
                                        break;
                                    }
                                }
                            }
                        }
                    }
    
                    $temp = $this->site->api_select_some_fields_with_where(
                        "t1.product_id",
                        "sma_wishlist as t1 inner join sma_products as t2 on t2.id = t1.product_id",
                        "t1.user_id = ".$config_data['user_id'],
                        "arr"
                    );
                    if (is_array($temp)) {
                        if (count($temp) > 0) {
                            for ($i=0;$i<count($temp);$i++) {
                                for ($j=0;$j<count($select_data);$j++) {
                                    if ($select_data[$j]['id'] == $temp[$i]['product_id']) {
                                        $select_data[$j]['is_favorite'] = 1;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            $select_data = $this->site->api_select_some_fields_with_where(
                "t1.* ".$temp_field,
                "sma_products as t1",
                "t1.".$config_data['table_id']." > 0 ".$condition." and t1.hide != 1",
                "arr"
            );
        }

        $select_data_2 = array();
        $k = 0;
        for ($i=0;$i<count($select_data);$i++) {
            $config_data_2 = array(
                'id' => $select_data[$i]['id'],
            );
            $temp_hide = $this->api_get_product_display($config_data_2);
            if ($temp_hide['result'] != 'hide') {
                $select_data_2[$k] = $select_data[$i];
                $k++;
            }
        }

        if ($_GET['sort_by'] == 'promotion-desc' || $_GET['promotion_id'] > 0) {
            $k = 0;
            for ($i=0;$i<count($select_data_2);$i++) {
                if ($select_data_2[$i]['id'] > 0) {
                    $config_data_2 = array(
                        'id' => $select_data_2[$i]['id'],
                        'customer_id' => $this->session->userdata('company_id'),
                    );
                    $temp_3 = $this->site->api_calculate_product_price($config_data_2); 
                    if ($temp_3['promotion']['price'] > 0) {
                        if ($_GET['promotion_id'] > 0) {
                            if ($_GET['promotion_id'] == $select_data_2[$i]['promotion_id'] && $select_data_2[$i]['promotion_type'] == 'list') {
                                $select_data_3[$k] = $select_data_2[$i];
                                $k++; 
                            }
                        } 
                        else {
                            $select_data_3[$k] = $select_data_2[$i];
                            $k++;
                        }
                    }
       
                }
            }
            $select_data_2 = $select_data_3;
            // $this->sma->print_arrays($select_data_2);
        }      
        $return['select_data_total'] = $select_data_2;
        
        if ($config_data['limit'] != '') {
            $output = array_slice($select_data_2, $config_data['offset_no'], $config_data['limit'] + $config_data['offset_no']);
            $return['select_data'] = $output;
            //$order_by .= ' limit '.$config_data['offset_no'].', '.$config_data['limit'];
        }

        return $return;
    }


    public function api_get_consignment($config_data)
    {
        $condition = '';
        if ($config_data['limit'] != '') {
            $order_by .= ' limit '.$config_data['offset_no'].', '.$config_data['limit'];
        }

        $search = $config_data['search'];

        $temp = $this->site->api_get_customer_order_condition_v2();
        $condition .= $temp;

        $temp_field = $this->site->api_get_condition_add_ons_field("add_ons", "sma_add_ons", "table_name = 'sma_consignment'");
        $select_data = $this->site->api_select_some_fields_with_where(
            "* 
            ".$temp_field,
            "sma_consignment",
            "id > 0 ".$condition." order by date desc ".$order_by,
            "arr"
        );

        return $select_data;
    }

    public function addConsignment($data, $items, $customer, $address)
    {
        $cost = $this->site->costing($items);

        if (is_array($customer) && !empty($customer)) {
            $this->db->insert('companies', $customer);
            $data['customer_id'] = $this->db->insert_id();
        }

        if (is_array($address) && !empty($address)) {
            $address['company_id'] = $data['customer_id'];
            $this->db->insert('addresses', $address);
            $data['address_id'] = $this->db->insert_id();
        }

        $data['payment_status'] == 'partial';

        $data_2 = $data;
        $config_data = array(
            'type' => 'consignment',
            'date' => $data_2['date'],
            'update' => 1,
        );
        $temp = $this->site->api_calculate_reference_no($config_data);

        $data_2['reference_no'] = $temp['reference_no'];
        $data_2['due_date'] = '';
        $data_2['add_ons'] = 'initial_first_add_ons:{}:reminder_day:{}:';

        if ($data_2['grand_total'] > 0) {
            if ($this->db->insert('sma_consignment', $data_2)) {
                $temp_id = $this->db->insert_id();
                foreach ($items as $item) {
                    $item['sale_id'] = $temp_id;
                    $this->db->insert('sma_consignment_items', $item);
                }
                return $temp_id;
            }
        }

        return false;
    }

    public function api_display_import_date($product_import_date_list)
    {
        $select_value = array();
        $temp = $product_import_date_list;
        $temp = explode(",", $temp);
        sort($temp);

        $temp_display = '
        <div class="panel panel-default api_pannel_left">
        <div class="panel-heading text-bold">
        '.lang('List_Import_Date').'
        </div>

        <div class="panel-body" style="padding:0;">
            <table id="" class="table table-condensed table-striped api_margin_bottom_0" border="0">
                <thead>
                    <tr>
                        <th align="left" style="text-align: left!important;">
                            <i class="fa fa-calendar margin-right-sm api_padding_left_7"></i> 
                            <span class="api_padding_left_3">'.lang('Import_Date').'</span>
                        </th>
                        <th align="left" style="text-align: left!important;">
                            '.lang('Deadline').'
                        </td>
                    </tr>
                </thead>
                <tbody>
        ';

        for ($i=0;$i<count($temp);$i++) {
            if ($temp[$i] != '') {
                $temp2 = explode('<api>', $temp[$i]);

                $date1 = date_create($temp2[0]);
                $date2 = date_create($temp2[1]);

                $now = (string)(date("Y-m-d"));
                $date_now = date_create($now);

                if ($date_now <= $date2) {
                    if ($this->session->userdata('api_selected_import_date') == $temp[$i]) {
                        $temp_selected = set_radio('api_import_date', $temp[$i], true);
                    } else {
                        $temp_selected = set_radio('api_import_date', $temp[$i]);
                    }

                    $temp_display .= '
                            <tr>
                            <td align="center">
                                <div class="api_float_left">
                                    <input class="api_pointer" type="radio" name="api_import_date" value="'.$temp[$i].'" '.$temp_selected.' onclick="api_set_import_date(this.value);" style="width: 30px; display: block; margin-top: 3px;">
                                </div>
                                <div class="api_float_left">
                                    '.date_format($date1, 'jS, F Y').'
                                </div>
                            </td>
                            <td align="center">
                                <div class="api_float_left">
                                    '.date_format($date2, 'jS, F Y').'
                                </div>                        
                            </td>                    
                            </tr>        
                        ';
                }
            }
        }
        $temp_display .= '
                </tbody>
            </table>
        </div>
        </div>
        ';

        $temp_display_2 = '<div class="panel panel-default api_pannel_left"><div class="panel-heading text-bold">'.lang('List_Import_Date').'</div><div class="panel-body" style="padding:0;"><table id="" class="table table-condensed table-striped api_margin_bottom_0" border="0"><thead><tr><th align="left" style="text-align: left!important;"><i class="fa fa-calendar margin-right-sm api_padding_left_7"></i> <span class="api_padding_left_3">'.lang('Import_Date').'</span></th><th align="left" style="text-align: left!important;">'.lang('Deadline').'</td></tr></thead><tbody>';

        $k = 0;
        for ($i=0;$i<count($temp);$i++) {
            if ($temp[$i] != '') {
                $temp2 = explode('<api>', $temp[$i]);

                $date1 = date_create($temp2[0]);
                $date2 = date_create($temp2[1]);

                $now = (string)(date("Y-m-d"));
                $date_now = date_create($now);

                if ($date_now <= $date2) {
                    if ($this->session->userdata('api_selected_import_date') == $temp[$i]) {
                        $temp_selected = set_radio('api_import_date', $temp[$i], true);
                    } else {
                        $temp_selected = set_radio('api_import_date', $temp[$i]);
                    }

                    $temp_display_2 .= '<tr><td align="center"><div class="api_float_left"><input class="api_pointer" type="radio" name="api_import_date" value="'.$temp[$i].'" '.$temp_selected.' onclick="api_set_import_date(this.value);" style="width: 30px; display: block; margin-top: 3px;"></div><div class="api_float_left">'.date_format($date1, 'jS, F Y').'</div></td><td align="center"><div class="api_float_left">'.date_format($date2, 'jS, F Y').'</div></td></tr>';
                    $k++;
                }
            }
        }
        $temp_display_2 .= '</tbody></table></div></div>';

        $return['display'] = $temp_display;

        
        if ($k > 0) {
            $temp_title = lang('Please_select_an_import_date');
            $temp_script = '
                $("#api_swal3-question").show();
                $("#api_swal3-warning").hide();
                $("#api_swal3-info").hide();
                $("#api_swal3-success").hide();
                $(".api_swal3-spacer").hide();
                $("#api_modal_2_btn_ok").hide();
                $("#api_modal_2_btn_close").show();                      
            ';
            $temp_ok = '
                $("#api_modal_2_btn_ok").html("<a href=\''.base_url().'\'><button type=\'button\' role=\'button\' tabindex=\'0\' class=\'api_swal3-cancel api_swal3-styled api_link_box_none\' style=\'background-color:#00bcd4;\'>Ok</button>");               
            ';
        } else {
            $temp_title = lang('We_Are_Sorry');
            $temp_script = '
                $("#api_swal3-question").hide();
                $("#api_swal3-warning").show();
                $("#api_swal3-info").hide();
                $("#api_swal3-success").hide();
                $(".api_swal3-spacer").show();
                $("#api_modal_2_btn_ok").show();
                $("#api_modal_2_btn_close").hide();                
            ';

            $temp_ok = '
                $("#api_modal_2_btn_ok").html("<a href=\''.base_url().'logout\'><button type=\'button\' role=\'button\' tabindex=\'0\' class=\'api_swal3-cancel api_swal3-styled api_link_box_none\' style=\'background-color:#f44336;\'>Logout</button>");               
            ';
            
            $return['display'] = '';
            $return['display_2'] = lang('There is no next import date. Please come back later.');
            $temp_display_2 = lang('There is no next import date. Please come back later.');
        }
        
        $return['script'] = '
<script>
    '.$temp_script.'
    $("#api_modal_2_title").html("'.$temp_title.'");
    $("#api_modal_2_body").html(\''.$temp_display_2.'\');

    '.$temp_ok.'  

    $("#api_modal_2_btn_close").html("<button type=\'button\' role=\'button\' tabindex=\'0\' class=\'api_swal3-cancel api_swal3-styled\' style=\'background-color: rgb(170, 170, 170); cursor:normal !important;\'>Ok</button>");    

    $("#api_modal_2_trigger").click();    
</script>
        ';
        $return['script_2'] = '
<script>
function api_set_import_date(value){
    var postData = [];
    var result = $.ajax
    (
    	{
    		url: "'.base_url().'shop/api_set_import_date?value=" + value,
    		type: "GET",
    		secureuri:false,
    		dataType: "html",
    		data:postData,
    		async: false,
    		error: function (response, status, e)
    		{
    			alert(e);
    		}
    	}
    ).responseText;

    var array_data = String(result).split("api-ajax-request-multiple-result-split");
    $("#api_modal_2_btn_ok").show();
    $("#api_modal_2_btn_close").hide();
}
</script>     
        ';
        return $return;
    }

    public function api_order_insert($config_data) {

        $temp_add_date = date('Y-m-d H:i:s');
        if ($config_data['delivery_date'] != '') {
            $temp_2 = date('w');
            if ($temp_2 == 6) {
                $temp = date('Y-m-d');
                $temp = date('Y-m-d', strtotime($temp. ' + 2 days'));
            }
            else {
                $temp = new DateTime('tomorrow');
                $temp = $temp->format('Y-m-d');
            }                                                
            if ($config_data['delivery_date'] == 1)
                $config_data['delivery_date'] = date('Y-m-d').' 12:00:00';
            if ($config_data['delivery_date'] == 2) {
                $config_data['delivery_date'] = $temp.' 9:00:00';
                $temp_add_date =  $config_data['delivery_date'];
            }
            if ($config_data['delivery_date'] == 3) {
                $config_data['delivery_date'] = $temp.' 12:00:00';
                $temp_add_date =  $config_data['delivery_date'];
            }            
        }

        $customer = $this->site->getCompanyByID_v2($config_data['customer_id']);
        if ($customer->id > 0) {
            $temp = $this->site->api_select_some_fields_with_where("
                *     
                "
                ,"sma_companies"
                ,"id = ".$customer->id
                ,"arr"
            );
            if ($temp[0]['company'] != '' && $temp[0]['company'] != '-')
                $temp_customer_name = $temp[0]['company'];
            else
                $temp_customer_name = $temp[0]['name'];
        }                
        $config_data_2 = array(
            'id' => $customer->id,
            'default_warehouse' => $this->shop_settings->warehouse,
        );
        $temp_customer_warehouse = $this->site->api_get_customer_warehouse($config_data_2);

        $biller = $this->site->getCompanyByID($this->shop_settings->biller);
        $note = $this->db->escape_str($config_data['comment']);

        //-product-===============================================================
        $product_tax = 0; 
        $total = 0;
        $product_discount = 0;
        $total_discount = 0;            
        foreach ($this->cart->contents() as $item) {
            
            if ($product_details = $this->getProductForCart($item['product_id'])) {

                $config_data_2 = array(
                    'id' => $product_details->id,
                    'customer_id' => $customer->id,
                );
                $temp_product = $this->site->api_calculate_product_price($config_data_2);

                $subtotal = round($temp_product['price'] * $item['qty'],2, PHP_ROUND_HALF_UP);
                $subtotal = round($subtotal,2, PHP_ROUND_HALF_UP);
                $unit = $this->site->getUnitByID($product_details->unit);

                $config_data_2 = array(
                    'product_id' => $product_details->id,
                    'customer_id' => $customer->id,
                    'amount' => ($temp_product['price'] * $item['qty']),
                );                                    
                //$temp2 = $this->site->api_calculate_product_tax($config_data_2);

                $temp_item_tax = 0;
                if ($temp2['result'] > 0 && $customer->vat_no && $customer->vat_invoice_type != 'do') {
                    $temp_item_tax = (($temp_product['price'] * $item['qty']) * 0.2) * 0.03;
                    $product_tax += $temp_item_tax;
                }
                
                $product = [
                    'product_id' => $product_details->id,
                    'product_code' => $product_details->code,
                    'product_name' => $product_details->name,
                    'product_type' => $product_details->type,
                    'option_id' => null,
                    'net_unit_price' => $this->sma->formatDecimal($temp_product['price']),
                    'unit_price' => $this->sma->formatDecimal($temp_product['price']),
                    'quantity' => $item['qty'],
                    'product_unit_id' => $unit ? $unit->id : NULL,
                    'product_unit_code' => $unit ? $unit->code : NULL,
                    'unit_quantity' => $item['qty'],
                    'warehouse_id' => $temp_customer_warehouse['warehouse_id'],
                    'item_tax' => $temp_item_tax,
                    'tax_rate_id' => $product_details->tax_rate,
                    'subtotal' => $this->sma->formatDecimal($subtotal),
                    'serial_no' => NULL,
                    'real_unit_price' => $this->sma->formatDecimal($temp_product['price']),
                ];
                $total += $this->sma->formatDecimal(round($temp_product['price'] * $item['qty'],2, PHP_ROUND_HALF_UP), 4);
                $products[] = $product;
            } else {
                $return['result'] = 'product_not_found';
                return $return;
            }
        }
        //-product-===============================================================

        //-sale-===============================================================
        if ($customer->vat_no && $customer->vat_invoice_type != 'do') 
            $temp_order_tax = 2; 
        else 
            $temp_order_tax = 1;                           

        $order_tax = $this->site->calculateOrderTax($temp_order_tax, ($total + $product_tax));
        $total_tax = $this->sma->formatDecimal(($product_tax + $order_tax), 4);
        $grand_total = $this->sma->formatDecimal(($total + $total_tax + $this->shop_settings->shipping), 4);

        $temp_lang = get_cookie('shop_language', TRUE);
        $language_array = unserialize(multi_language);
        $temp_lang_2 = $language_array[0][0];
        if (strtolower($temp_lang) != strtolower($language_array[0][1])) {                
            for ($i=0;$i<count($language_array);$i++) {
                if (strtolower($temp_lang) == strtolower($language_array[$i][1])) {
                    $temp_lang_2 = $language_array[$i][0];
                    break;
                }
            }
        }

        $add_ons = 'initial_first_add_ons:{}:cs_reference_no:{}:delivery_date:{'.$config_data['delivery_date'].'}:product_import_date:{'.$config_data['product_import_date'].'}:language:{'.$temp_lang_2.'}:';

        if ($config_data['payment_method'] == 'payway_credit' || $config_data['payment_method'] == 'payway_aba_pay')
            $temp_payment_method = 'payway'; 
        else
            $temp_payment_method = $config_data['payment_method'];

        $data = [
            'date' => $temp_add_date,
            'customer_id' => $customer->id,
            'customer' => $temp_customer_name,
            'biller_id' => $biller->id,
            'biller' => ($biller->company && $biller->company != '-' ? $biller->company : $biller->name),
            'warehouse_id' => $temp_customer_warehouse['warehouse_id'],
            'note' => $note,
            'total' => $total,
            'product_tax' => $product_tax,
            'order_tax_id' => $temp_order_tax,
            'order_tax' => $order_tax,
            'total_tax' => $total_tax,
            'shipping' => $this->shop_settings->shipping,
            'grand_total' => $grand_total,
            'total_items' => $this->cart->total_items(),
            'sale_status' => 'pending',
            'payment_status' => 'pending',
            'created_by' => $this->session->userdata('user_id') ? $this->session->userdata('user_id') : NULL,
            'shop' => 1,
            'hash' => hash('sha256', microtime() . mt_rand()),
            'payment_method' => $temp_payment_method,
            'add_ons' => $add_ons,
        ];
        //-sale-===============================================================

        if ($config_data['insert'] == 1) {
            if ($data['grand_total'] > 0) {
                if ($sale_id = $this->addSale($data, $products, $customer, '')) {
                    if ($grand_total > 0)
                        $this->cart->destroy();

                    $return['result'] = $sale_id;
                }
            }
        }
        $return['grand_total'] = $grand_total;
            
        return $return;
    }

    public function api_order_line_notification($id) {
        if ($id > 0) {
            if (!$this->loggedIn) { redirect('login'); }
            if ($this->Staff) { admin_redirect('sales'); }

            $config_data = array(
                'table_name' => 'sma_sales',
                'select_table' => 'sma_sales',
                'translate' => '',
                'select_condition' => "id = ".$id." and created_by = ".$this->session->userdata('user_id'),
            );
            $select_data = $this->site->api_select_data_v2($config_data);
            if (count($select_data) > 0) {

                $temp_2 = $this->site->api_select_some_fields_with_where("
                    *
                    "
                    ,"sma_companies"
                    ,"id = ".$select_data[0]['customer_id']
                    ,"arr"
                );
                $temp_3 = array();
                if ($temp_2[0]['parent_id'] > 0) {
                    $temp_3 = $this->site->api_select_some_fields_with_where("
                        *
                        "
                        ,"sma_companies"
                        ,"id = ".$temp_2[0]['parent_id']
                        ,"arr"
                    );
                }

                $temp = '\nCompany: '.$select_data[0]['customer'];
                if (count($temp_3) > 0)
                    $temp .= '\nParent Company: '.$temp_3[0]['company'];

                $temp2 = '';
                if ($select_data[0]['delivery_date'] != '') {
                    $temp_3 = explode(' ', $select_data[0]['delivery_date']);
                    if ($temp_3[1] == '9:00:00')
                        $temp_4 = '9:00 AM to 12:00 AM';
                    else
                        $temp_4 = '12:00 PM to 7:00 PM';

                    $temp2 .= '\nDelivery Date: '.$temp_3[0].' '.$temp_4;
                    
                }
                $temp_item = $this->site->api_select_some_fields_with_where("*
                    "
                    ,"sma_sale_items"
                    ,"sale_id = ".$id." order by id asc"
                    ,"arr"
                );                

                if ($select_data[0]['payment_method'] == 'account_receivable')
                    $temp_method = 'account_payable';
                elseif ($select_data[0]['payment_method'] == 'cod')
                    $temp_method = 'cash on delivery';
                else
                    $temp_method = $select_data[0]['payment_method'];

                $temp_method = str_replace('_',' ',$temp_method);
                $temp_method = ucfirst($temp_method);
                if ($temp_method == 'Cash on delivery') $temp_method = 'COD';

                $temp3 = '\nPayment Method: '.$temp_method;

                $config_data = array(
                    'table_name' => 'sma_shop_settings',
                    'select_table' => 'sma_shop_settings',
                    'translate' => '',
                    'select_condition' => "shop_id = 1",
                );
                $this->api_shop_setting = $this->site->api_select_data_v2($config_data);                 
                if ($this->api_shop_setting[0]['product_import_date'] == 1 && base_url() == 'https://air.phsarjapan.com/')
                    $temp3 .= '\nImport Date: '.$select_data[0]['product_import_date'];

                $temp_display_2 = 'Order ID: '.$id.$temp.$temp2.$temp3.'\n\n';                   
                $temp_display = '';
                $j = 1;

                $temp_total_alcohol = 0;
                $temp_total_food = 0;                    
                for ($i=0;$i<count($temp_item);$i++) {
                    if ($temp_item[$i]['item_tax'] > 0)
                        $temp_total_alcohol += $temp_item[$i]['subtotal'];
                    else
                        $temp_total_food += $temp_item[$i]['subtotal'];
                    $temp_display .= $j.'. '.$temp_item[$i]['product_name'].', Qty: '.number_format($temp_item[$i]['quantity'],2).'\n';
                    $j++;
                }                    
                $temp_display_2 = $temp_display_2.$temp_display;
                if ($select_data[0]['order_tax'] > 0) {
                    $temp_display_2 .= '\nTotal: '.$this->sma->formatMoney($select_data[0]['total']).'\n';
                    if ($data['product_tax'] > 0) {
                        $temp_display_2 .= 'Food Total: '.$this->sma->formatMoney($temp_total_food).'\n';            
                        $temp_display_2 .= 'Alcohol Total: '.$this->sma->formatMoney($temp_total_alcohol).'\n';                                               
                        $temp_display_2 .= 'Public Lighting Tax (PLT): '.$this->sma->formatMoney($select_data[0]['product_tax']).'\n';
                        $temp_display_2 .= 'Subtotal: '.$this->sma->formatMoney($select_data[0]['total'] + $select_data[0]['product_tax']).'\n';                            
                    }
                    $temp_display_2 .= 'Vat 10%: '.$this->sma->formatMoney($select_data[0]['order_tax']).'';
                }
                $temp_display_2 .= '\nGrand Total: '.$this->sma->formatMoney($select_data[0]['grand_total']);

                if ($select_data[0]['note'] != '') {
                    $temp_note = preg_replace("/[\/\&%#\$]/", "", $select_data[0]['note']);
                    $temp_note = preg_replace("/[\"\']/", "", $temp_note);                        
                    $temp_display_2 .= '\n\n\nNote:\n'.$temp_note;
                }

                if (base_url() == 'https://order.daishintc.com/') {
                    $line_message = '
                        curl -v -X POST https://api.line.me/v2/bot/message/broadcast \
                        -H \'Content-Type:application/json\' \
                        -H \'Authorization: Bearer {FNtBe9JBGzmXbvr1dp5ODw1xdofgqtOC7KHfVosqKnRqA+9tal1O8tl3YjYZNJcamhDtZgvA0kJuHcf0LpA8q0vwKAfwzH8KVvVn5podwiWijuCl9NLeoUCp2Cyh6bDHpcuDDswGQml6UO5ICg1MRQdB04t89/1O/w1cDnyilFU=}\' \
                        -d \'{
                        "messages":[
                            {
                                "type":"text",
                                "text":"'.$temp_display_2.'"
                            }
                        ]
                        }\'
                    ';
                }
                if (base_url() == 'https://dev-invoice.camboinfo.com/' || base_url() == 'https://demo.phsarjapan.com/') {
                    $line_message = '
                        curl -v -X POST https://api.line.me/v2/bot/message/broadcast \
                        -H \'Content-Type:application/json\' \
                        -H \'Authorization: Bearer {UNTVh6YcE/XXoEOJ1lSrGjuP/ftpl1QsXvAggeidgD014fTA5KooVZ5FU9UFY7eZ0oS/ajspc+pBxzSEnBfJcnAmK+yE6nsgru8g58sjg7BzWV+mZawRtaQ7b1skK7TgefvFtxUCgD0etJD+jcFdLwdB04t89/1O/w1cDnyilFU=}\' \
                        -d \'{
                        "messages":[
                            {
                                "type":"text",
                                "text":"'.$temp_display_2.'"
                            }
                        ]
                        }\'
                    ';
                }
                if (base_url() == 'https://phsarjapan.com/' || base_url() == 'https://air.phsarjapan.com/') {
                    $line_message = '
                        curl -v -X POST https://api.line.me/v2/bot/message/broadcast \
                        -H \'Content-Type:application/json\' \
                        -H \'Authorization: Bearer {8gM7JhLa/RbLgM1xgY5dwCslVKkzARkRHEE6xIw+VQ2dYlg/1oGazuHkuJpR1hc/Xl1IbFRkx76kP3dE3aA1hKDsuc5w+SrKPtDvkW21CZPkw0IqBHuLSsZCwAFWvZ0QaIXoy96Ip8ivO+oWBTw6lAdB04t89/1O/w1cDnyilFU=}\' \
                        -d \'{
                            "messages":[
                                {
                                    "type":"text",
                                    "text":"'.$temp_display_2.'"
                                }
                            ]
                        }\'
                    ';
                }
                shell_exec($line_message);         

            }
        }        
    }

    public function order_received($id = null, $hash = null)
    {
        if ($inv = $this->getOrder(['id' => $id, 'hash' => $hash])) {
            $user = $inv->created_by ? $this->site->getUser($inv->created_by) : NULL;
            $customer = $this->site->getCompanyByID_v2($inv->customer_id);
            $biller = $this->site->getCompanyByID($inv->biller_id);
            $this->load->library('parser');

            $sale_id = $id;
            $config_data = array(
                'table_name' => 'sma_sales',
                'select_table' => 'sma_sales',
                'select_condition' => "id = ".$id,
            );
            $select_data = $this->site->api_select_data_v2($config_data);
            $temp = '';
            $temp_branch_post = $select_data[0]['company_branch'];

                $temp_2 = $this->site->api_select_some_fields_with_where("
                    *
                    "
                    ,"sma_companies"
                    ,"id = ".$select_data[0]['customer_id']
                    ,"arr"
                );
                $temp_3 = array();
                if ($temp_2[0]['parent_id'] > 0) {
                    $temp_3 = $this->site->api_select_some_fields_with_where("
                        *
                        "
                        ,"sma_companies"
                        ,"id = ".$temp_2[0]['parent_id']
                        ,"arr"
                    );
                }
                $temp = '<br>Company: '.$select_data[0]['customer'];
                    if (count($temp_3) > 0)
                        $temp .= '<br>Parent Company: '.$temp_3[0]['company'];


                    $temp2 = '';
                    if ($select_data[0]['delivery_date'] != '') {
                        $temp_3 = explode(' ', $select_data[0]['delivery_date']);
                        if ($temp_3[1] == '9:00:00')
                            $temp_4 = '9:00 AM to 12:00 AM';
                        else
                            $temp_4 = '12:00 PM to 7:00 PM';

                        $temp2 .= '<br>Delivery Date: '.$temp_3[0].' '.$temp_4;
                        
                    }
                    $temp_item = $this->site->api_select_some_fields_with_where("*
                        "
                        ,"sma_sale_items"
                        ,"sale_id = ".$sale_id." order by id asc"
                        ,"arr"
                    );
                    if ($data['payment_method'] == 'account_receivable')
                        $temp_method = 'account_payable';
                    elseif ($data['payment_method'] == 'cod')
                        $temp_method = 'cash on delivery';
                    else
                        $temp_method = $select_data[0]['payment_method'];

                    $temp_method = str_replace('_',' ',$temp_method);
                    $temp_method = ucfirst($temp_method);

                    $temp_method = preg_replace('/[^a-zA-Z0-9_. ()+]/', '-', $temp_method);
                    if ($temp_method == 'Cash on delivery' || $temp_method == 'Cod') $temp_method = 'COD';

                    $temp3 = '<br>Payment Method: '.$temp_method;

                    if ($this->api_shop_setting[0]['product_import_date'] == 1 && base_url() == 'https://air.phsarjapan.com/')
                        $temp3 .= '<br>Import Date: '.$select_data[0]['product_import_date'];

                    $customer->company = preg_replace('/[^a-zA-Z0-9_. ()+]/', '-', $customer->company);
                    $temp_display_2 = 'Order ID: '.$sale_id.$temp.$temp2.$temp3.'<br><br>';                   
                    $temp_display = '';
                    $j = 1;
                    $temp_total_alcohol = 0;
                    $temp_total_food = 0;
                    for ($i=0;$i<count($temp_item);$i++) {
                        if ($temp_item[$i]['item_tax'] > 0)
                            $temp_total_alcohol += $temp_item[$i]['subtotal'];
                        else
                            $temp_total_food += $temp_item[$i]['subtotal'];                        
                        $temp_item[$i]['product_name'] = preg_replace('/[^a-zA-Z0-9_. ()+]/', '-', $temp_item[$i]['product_name']);
                        $temp_display .= $j.'. '.$temp_item[$i]['product_name'].', Qty: '.number_format($temp_item[$i]['quantity'],2).'<br>';
                        $j++;
                    }                    
                    $temp_display_2 = $temp_display_2.$temp_display;
                    if ($select_data[0]['order_tax'] > 0) {
                        $temp_display_2 .= '<br>Total: '.$this->sma->formatMoney($select_data[0]['total']).'<br>';
                        if ($select_data[0]['product_tax'] > 0) {
                            $temp_display_2 .= 'Foot Total: '.$this->sma->formatMoney($temp_total_food).'<br>';
                            $temp_display_2 .= 'Alcohol Total: '.$this->sma->formatMoney($temp_total_alcohol).'<br>';                                                        
                            $temp_display_2 .= 'Public Lighting Tax (PLT): '.$this->sma->formatMoney($select_data[0]['product_tax']).'<br>';
                            $temp_display_2 .= 'Subtotal: '.$this->sma->formatMoney($select_data[0]['total'] + $select_data[0]['product_tax']).'<br>';                            
                        }                        
                        $temp_display_2 .= 'Vat 10: '.$this->sma->formatMoney($select_data[0]['order_tax']);
                    }
                    $temp_display_2 .= '<br>Grand Total: '.$this->sma->formatMoney($select_data[0]['grand_total']);

                    if ($select_data[0]['note'] != '') {
                        $temp_note = preg_replace("/[\/\&%#\$]/", "", $select_data[0]['note']);
                        $temp_note = preg_replace("/[\"\']/", "", $temp_note);
                        $temp_note = str_replace('\r', '', $temp_note);                        
                        $temp_note = str_replace('\n', '<br>', $temp_note);                        
                        $temp_display_2 .= '<br><br><br>Note:<br>'.$temp_note;
                    }
            
            $this->load->model('pay_model');
            $paypal = $this->pay_model->getPaypalSettings();
            $skrill = $this->pay_model->getSkrillSettings();
            $btn_code = '<div id="payment_buttons" class="text-center margin010">';
            if (!empty($this->shop_settings->bank_details)) {
                echo '<div style="width:100%;">'.$this->shop_settings->bank_details.'</div><hr class="divider or">';
            }
            if ($paypal->active == "1" && $inv->grand_total != "0.00") {
                if (trim(strtolower($customer->country)) == $biller->country) {
                    $paypal_fee = $paypal->fixed_charges + ($inv->grand_total * $paypal->extra_charges_my / 100);
                } else {
                    $paypal_fee = $paypal->fixed_charges + ($inv->grand_total * $paypal->extra_charges_other / 100);
                }
                //$btn_code .= '<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business=' . $paypal->account_email . '&item_name=' . $inv->reference_no . '&item_number=' . $inv->id . '&image_url=' . base_url() . 'assets/uploads/logos/' . $this->Settings->logo . '&amount=' . (($inv->grand_total - $inv->paid) + $paypal_fee) . '&no_shipping=1&no_note=1&currency_code=' . $this->default_currency->code . '&bn=BuyNow&rm=2&return=' . admin_url('sales/view/' . $inv->id) . '&cancel_return=' . admin_url('sales/view/' . $inv->id) . '&notify_url=' . admin_url('payments/paypalipn') . '&custom=' . $inv->reference_no . '__' . ($inv->grand_total - $inv->paid) . '__' . $paypal_fee . '"><img src="' . base_url('assets/images/btn-paypal.png') . '" alt="Pay by PayPal"></a> ';
            }
            if ($skrill->active == "1" && $inv->grand_total != "0.00") {
                if (trim(strtolower($customer->country)) == $biller->country) {
                    $skrill_fee = $skrill->fixed_charges + ($inv->grand_total * $skrill->extra_charges_my / 100);
                } else {
                    $skrill_fee = $skrill->fixed_charges + ($inv->grand_total * $skrill->extra_charges_other / 100);
                }
                //$btn_code .= ' <a href="https://www.moneybookers.com/app/payment.pl?method=get&pay_to_email=' . $skrill->account_email . '&language=EN&merchant_fields=item_name,item_number&item_name=' . $inv->reference_no . '&item_number=' . $inv->id . '&logo_url=' . base_url() . 'assets/uploads/logos/' . $this->Settings->logo . '&amount=' . (($inv->grand_total - $inv->paid) + $skrill_fee) . '&return_url=' . admin_url('sales/view/' . $inv->id) . '&cancel_url=' . admin_url('sales/view/' . $inv->id) . '&detail1_description=' . $inv->reference_no . '&detail1_text=Payment for the sale invoice ' . $inv->reference_no . ': ' . $inv->grand_total . '(+ fee: ' . $skrill_fee . ') = ' . $this->sma->formatMoney($inv->grand_total + $skrill_fee) . '&currency=' . $this->default_currency->code . '&status_url=' . admin_url('payments/skrillipn') . '"><img src="' . base_url('assets/images/btn-skrill.png') . '" alt="Pay by Skrill"></a>';
            }


            if (base_url() == 'https://order.daishintc.com/')
                $to = 'daishinorder@gmail.com';
            elseif (base_url() == 'https://phsarjapan.com/' || base_url() == 'https://air.phsarjapan.com/')
                $to = 'terashida1515@gmail.com';
            else
                $to = 'apipeapi@gmail.com';

            if (base_url() == 'https://order.daishintc.com/')
                $subject = 'Daishintc Order #'.$id;
            if (base_url() == 'https://dev-invoice.camboinfo.com/' || base_url() == 'https://demo.phsarjapan.com/')
                $subject = 'Daishintc Order #'.$id;             
            if (base_url() == 'https://phsarjapan.com/' || base_url() == 'https://air.phsarjapan.com/')
                $subject = 'Japan Shop Order #'.$id;



            $message = $temp_display_2;
                    
            $message = str_replace('&', 'and', $message);

            $attachment = '';
            $cc = '';
            $bcc = '';
            if (!is_int(strpos($_SERVER["HTTP_HOST"],"localhost")))
            if ($this->sma->send_email($to, $subject, $message, $this->Settings->default_email, null, $attachment, $cc, $bcc)) {
                
            }

            $parse_data = array(
                'order_id' => $inv->id,
                'contact_person' => $customer->name,
                'company' => $customer->company && $customer->company != '-' ? '('.$customer->company.')' : '',
                'order_link' => shop_url('orders/'.$id.'/'.($this->loggedIn ? '' : $data['hash'])),
                'site_link' => base_url(),
                'site_name' => $this->Settings->site_name,
                'logo' => '<img src="' . base_url() . 'assets/uploads/logos/' . $this->shop_settings->logo . '" />',
            );
            $msg = file_get_contents('./themes/' . $this->Settings->theme . '/admin/views/email_templates/sale.html');
            $message = $this->parser->parse_string($msg, $parse_data);

            //$attachment = $this->api_order_attachment($id);

            $sent = $error = $cc = $bcc = false;
            $cc = '';
            $bcc = '';
            $warehouse = $this->site->getWarehouseByID($inv->warehouse_id);

            
            if ($warehouse->email) {
                //$bcc .= ','.$warehouse->email;
            }
            try {                
                if (!is_int(strpos($_SERVER["HTTP_HOST"],"localhost"))) {
                    if ($this->sma->send_email(($user ? $user->email : $customer->email), $subject, $message, $this->Settings->default_email, null, $attachment, $cc, $bcc)) {
                        delete_files($attachment);
                        $sent = true;     
                        $this->session->set_userdata('api_order_send_mail','send');
                    }
                }
            } catch (Exception $e) {
                $error = $e->getMessage();
                $this->session->set_userdata('api_order_send_mail', $error);
            }
                        
            return ['sent' => $sent, 'error' => $error];
        }
    }

    function api_order_attachment($id) {
        if (!$this->loggedIn) { redirect('login'); }
        if ($this->Staff) { admin_redirect('sales'); }
        
        $condition = $this->site->api_get_customer_order_condition_v2();

        $config_data = array(
            'table_name' => 'sma_sales',
            'select_table' => 'sma_sales',
            'select_condition' => "id = ".$id." ".$condition,
        );
        $temp = $this->site->api_select_data_v2($config_data);
        if ($temp[0]['id'] <= 0) {
            $this->session->set_flashdata('error', lang('access_denied'));
            redirect('/');                    
        }
        

        $order = $this->getOrder(['id' => $id, 'hash' => $hash]);
        $this->data['inv'] = $temp;
        $this->data['rows'] = $this->shop_model->getOrderItems($id);
        $this->data['customer'] = $this->site->getCompanyByID($temp[0]['customer_id']);
        $this->data['biller'] = $this->site->getCompanyByID($temp[0]['biller_id']);
        $this->data['Settings'] = $this->Settings;
        $this->data['shop_settings'] = $this->shop_settings;
        $html = $this->load->view($this->Settings->theme.'/shop/views/pages/pdf_invoice', $this->data, TRUE);

        $name = lang("Order_Detail") . "_" . $temp[0]['id'] . ".pdf";

        return $this->sma->generate_pdf($html, $name, 'S', $this->data['biller']->invoice_footer);

    }
public function display_win_history($temp){
    $exp = explode("," , $temp[0]['lucky_draw_win_code']);
        $temp_display = '';
        $temp_display = '<div class="product_title">Win History</div>';
        $temp_display .= '<table class="all_product">';
        $temp_display .= '<tr>
                    <th>#</th>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Status</th>
                </tr>';
            $counter = 1;      
            for($k=count($exp)-1;$k>=0;$k--) {
            $exp_2 = explode("-" , $exp[$k]); //take the value that have exploded before for use
            if ($exp_2[0] != '') {
                $config_data = array(
                    'table_name' => 'sma_products',
                    'select_table' => 'sma_products',
                    'translate' => 'yes',
                    'select_condition' => "id = ".$exp_2[0]." order by id desc ",
                );
                $temp = $this->site->api_select_data_v2($config_data);
    
                $data_view = array (
                    'wrapper_class' => '',
                    'file_path' => 'assets/uploads/'.$temp[0]['image'],
                    'max_width' => 200,
                    'max_height' => 100,
                    'product_link' => 'true',
                    'image_class' => 'img-responsive',
                    'image_id' => '',   
                    'resize_type' => 'full',
                );
                $temp2 = $this->site->api_get_image($data_view);
                
    
                if($exp_2[1] == 1){
                    $message = "Received";
                } if($exp_2[1] == 0){
                    $message = "Not yet receive";
                }                    
    
                $temp_display .= '<tr>
                    <td>
                        '.$counter++.'
                    </td>
                    <td>
                        '.$temp2['image_table'].'
                    </td>
                    <td>
                        '.$temp[0]['title_en'].'
                    </td>
                    <td>
                        '.$message.'
                    </td>
                </tr>';
            }
        }                
            $temp_display .= '</table>';
            return $temp_display; 
    }
        
}
