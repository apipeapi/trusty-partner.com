<?php defined('BASEPATH') or exit('No direct script access allowed');

class Products_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAllProducts()
    {
        $q = $this->db->get('products');
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getCategoryProducts($category_id)
    {
        $q = $this->db->get_where('products', array('category_id' => $category_id));
        $this->db->order_by('name asc');
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getSubCategoryProducts($subcategory_id)
    {
        $q = $this->db->get_where('products', array('subcategory_id' => $subcategory_id));
        $this->db->order_by('name asc');

        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getProductOptions($pid)
    {
        $q = $this->db->order_by('id asc')->get_where('product_variants', array('product_id' => $pid));

        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getProductOptionsWithWH($pid)
    {
        $this->db->select($this->db->dbprefix('product_variants') . '.*, ' . $this->db->dbprefix('warehouses') . '.name as wh_name, ' . $this->db->dbprefix('warehouses') . '.id as warehouse_id, ' . $this->db->dbprefix('warehouses_products_variants') . '.quantity as wh_qty')
            ->join('warehouses_products_variants', 'warehouses_products_variants.option_id=product_variants.id', 'left')
            ->join('warehouses', 'warehouses.id=warehouses_products_variants.warehouse_id', 'left')
            ->group_by(array('' . $this->db->dbprefix('product_variants') . '.id', '' . $this->db->dbprefix('warehouses_products_variants') . '.warehouse_id'))
            ->order_by('product_variants.id asc');
        $q = $this->db->get_where('product_variants', array('product_variants.product_id' => $pid, 'warehouses_products_variants.quantity !=' => null));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getProductComboItems($pid)
    {
        $this->db->select($this->db->dbprefix('products') . '.id as id, ' . $this->db->dbprefix('products') . '.code as code, ' . $this->db->dbprefix('combo_items') . '.quantity as qty, ' . $this->db->dbprefix('products') . '.name as name, ' . $this->db->dbprefix('combo_items') . '.unit_price as price')->join('products', 'products.code=combo_items.item_code', 'left')->group_by('combo_items.id');
        $q = $this->db->get_where('combo_items', array('product_id' => $pid));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
        return false;
    }

    public function getProductByID($id)
    {
        $q = $this->db->get_where('products', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getProductWithCategory($id)
    {
        $this->db->select($this->db->dbprefix('products') . '.*, ' . $this->db->dbprefix('categories') . '.name as category')
        ->join('categories', 'categories.id=products.category_id', 'left');
        $q = $this->db->get_where('products', array('products.id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function has_purchase($product_id, $warehouse_id = null)
    {
        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }
        $q = $this->db->get_where('purchase_items', array('product_id' => $product_id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return true;
        }
        return false;
    }

    public function getProductDetails($id)
    {
        $this->db->select($this->db->dbprefix('products') . '.code, ' . $this->db->dbprefix('products') . '.name, ' . $this->db->dbprefix('categories') . '.code as category_code, cost, price, quantity, alert_quantity')
            ->join('categories', 'categories.id=products.category_id', 'left');
        $q = $this->db->get_where('products', array('products.id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getProductDetail($id)
    {
        $this->db->select($this->db->dbprefix('products') . '.*, ' . $this->db->dbprefix('tax_rates') . '.name as tax_rate_name, '.$this->db->dbprefix('tax_rates') . '.code as tax_rate_code, c.code as category_code, sc.code as subcategory_code', false)
            ->join('tax_rates', 'tax_rates.id=products.tax_rate', 'left')
            ->join('categories c', 'c.id=products.category_id', 'left')
            ->join('categories sc', 'sc.id=products.subcategory_id', 'left');
        $q = $this->db->get_where('products', array('products.id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getSubCategories($parent_id)
    {
        $this->db->select('id as id, name as text')
        ->where('parent_id', $parent_id)->order_by('name');
        $q = $this->db->get("categories");
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getProductByCategoryID($id)
    {
        $q = $this->db->get_where('products', array('category_id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return true;
        }
        return false;
    }

    public function getAllWarehousesWithPQ($product_id)
    {
        $this->db->select('' . $this->db->dbprefix('warehouses') . '.*, ' . $this->db->dbprefix('warehouses_products') . '.quantity, ' . $this->db->dbprefix('warehouses_products') . '.rack')
            ->join('warehouses_products', 'warehouses_products.warehouse_id=warehouses.id', 'left')
            ->where('warehouses_products.product_id', $product_id)
            ->group_by('warehouses.id');
        $q = $this->db->get('warehouses');
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getProductPhotos($id)
    {
        $q = $this->db->get_where("product_photos", array('product_id' => $id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getProductByCode($code)
    {
        $q = $this->db->get_where('products', array('code' => $code), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function addProduct($data, $items, $warehouse_qty, $product_attributes, $photos)
    {
        if ($this->db->insert('products', $data)) {
            $product_id = $this->db->insert_id();

            if ($items) {
                foreach ($items as $item) {
                    $item['product_id'] = $product_id;
                    $this->db->insert('combo_items', $item);
                }
            }

            $warehouses = $this->site->getAllWarehouses();
            if ($data['type'] != 'standard') {
                foreach ($warehouses as $warehouse) {
                    $this->db->insert('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse->id, 'quantity' => 0));
                }
            }

            $tax_rate = $this->site->getTaxRateByID($data['tax_rate']);

            if ($warehouse_qty && !empty($warehouse_qty)) {
                foreach ($warehouse_qty as $wh_qty) {
                    if (isset($wh_qty['quantity']) && ! empty($wh_qty['quantity'])) {
                        $this->db->insert('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $wh_qty['warehouse_id'], 'quantity' => $wh_qty['quantity'], 'rack' => $wh_qty['rack'], 'avg_cost' => $data['cost']));

                        if (!$product_attributes) {
                            $tax_rate_id = $tax_rate ? $tax_rate->id : null;
                            $tax = $tax_rate ? (($tax_rate->type == 1) ? $tax_rate->rate . "%" : $tax_rate->rate) : null;
                            $unit_cost = $data['cost'];
                            if ($tax_rate) {
                                if ($tax_rate->type == 1 && $tax_rate->rate != 0) {
                                    if ($data['tax_method'] == '0') {
                                        $pr_tax_val = ($data['cost'] * $tax_rate->rate) / (100 + $tax_rate->rate);
                                        $net_item_cost = $data['cost'] - $pr_tax_val;
                                        $item_tax = $pr_tax_val * $wh_qty['quantity'];
                                    } else {
                                        $net_item_cost = $data['cost'];
                                        $pr_tax_val = ($data['cost'] * $tax_rate->rate) / 100;
                                        $unit_cost = $data['cost'] + $pr_tax_val;
                                        $item_tax = $pr_tax_val * $wh_qty['quantity'];
                                    }
                                } else {
                                    $net_item_cost = $data['cost'];
                                    $item_tax = $tax_rate->rate;
                                }
                            } else {
                                $net_item_cost = $data['cost'];
                                $item_tax = 0;
                            }

                            $subtotal = (($net_item_cost * $wh_qty['quantity']) + $item_tax);

                            $item = array(
                                'product_id' => $product_id,
                                'product_code' => $data['code'],
                                'product_name' => $data['name'],
                                'net_unit_cost' => $net_item_cost,
                                'unit_cost' => $unit_cost,
                                'real_unit_cost' => $unit_cost,
                                'quantity' => $wh_qty['quantity'],
                                'quantity_balance' => $wh_qty['quantity'],
                                'quantity_received' => $wh_qty['quantity'],
                                'item_tax' => $item_tax,
                                'tax_rate_id' => $tax_rate_id,
                                'tax' => $tax,
                                'subtotal' => $subtotal,
                                'warehouse_id' => $wh_qty['warehouse_id'],
                                'date' => date('Y-m-d'),
                                'status' => 'received',
                            );
                            $this->db->insert('purchase_items', $item);
                            $this->site->syncProductQty($product_id, $wh_qty['warehouse_id']);
                        }
                    }
                }
            }

            if ($product_attributes) {
                foreach ($product_attributes as $pr_attr) {
                    $pr_attr_details = $this->getPrductVariantByPIDandName($product_id, $pr_attr['name']);

                    $pr_attr['product_id'] = $product_id;
                    $variant_warehouse_id = $pr_attr['warehouse_id'];
                    if ($variant_warehouse_id <= 0) {
                        $variant_warehouse_id = $this->Settings->default_warehouse;
                    }
                    unset($pr_attr['warehouse_id']);
                    if ($pr_attr_details) {
                        $option_id = $pr_attr_details->id;
                    } else {
                        $this->db->insert('product_variants', $pr_attr);
                        $option_id = $this->db->insert_id();
                    }
                    //if ($pr_attr['quantity'] != 0) {
                    $this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $product_id, 'warehouse_id' => $variant_warehouse_id, 'quantity' => $pr_attr['quantity']));

                    $tax_rate_id = $tax_rate ? $tax_rate->id : null;
                    $tax = $tax_rate ? (($tax_rate->type == 1) ? $tax_rate->rate . "%" : $tax_rate->rate) : null;
                    $unit_cost = $data['cost'];
                    if ($tax_rate) {
                        if ($tax_rate->type == 1 && $tax_rate->rate != 0) {
                            if ($data['tax_method'] == '0') {
                                $pr_tax_val = ($data['cost'] * $tax_rate->rate) / (100 + $tax_rate->rate);
                                $net_item_cost = $data['cost'] - $pr_tax_val;
                                $item_tax = $pr_tax_val * $pr_attr['quantity'];
                            } else {
                                $net_item_cost = $data['cost'];
                                $pr_tax_val = ($data['cost'] * $tax_rate->rate) / 100;
                                $unit_cost = $data['cost'] + $pr_tax_val;
                                $item_tax = $pr_tax_val * $pr_attr['quantity'];
                            }
                        } else {
                            $net_item_cost = $data['cost'];
                            $item_tax = $tax_rate->rate;
                        }
                    } else {
                        $net_item_cost = $data['cost'];
                        $item_tax = 0;
                    }

                    $subtotal = (($net_item_cost * $pr_attr['quantity']) + $item_tax);
                    $item = array(
                            'product_id' => $product_id,
                            'product_code' => $data['code'],
                            'product_name' => $data['name'],
                            'net_unit_cost' => $net_item_cost,
                            'unit_cost' => $unit_cost,
                            'quantity' => $pr_attr['quantity'],
                            'option_id' => $option_id,
                            'quantity_balance' => $pr_attr['quantity'],
                            'quantity_received' => $pr_attr['quantity'],
                            'item_tax' => $item_tax,
                            'tax_rate_id' => $tax_rate_id,
                            'tax' => $tax,
                            'subtotal' => $subtotal,
                            'warehouse_id' => $variant_warehouse_id,
                            'date' => date('Y-m-d'),
                            'status' => 'received',
                        );
                    $item['option_id'] = !empty($item['option_id']) && is_numeric($item['option_id']) ? $item['option_id'] : null;
                    $this->db->insert('purchase_items', $item);

                    //}

                    foreach ($warehouses as $warehouse) {
                        if (!$this->getWarehouseProductVariant($warehouse->id, $product_id, $option_id)) {
                            //  $this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $product_id, 'warehouse_id' => $warehouse->id, 'quantity' => 0));
                        }
                    }

                    $this->site->syncVariantQty($option_id, $variant_warehouse_id);
                }
            }

            if ($photos) {
                foreach ($photos as $photo) {
                    $this->db->insert('product_photos', array('product_id' => $product_id, 'photo' => $photo));
                }
            }

            $this->site->syncQuantity(null, null, null, $product_id);
            return true;
        }
        return false;
    }

    public function getPrductVariantByPIDandName($product_id, $name)
    {
        $q = $this->db->get_where('product_variants', array('product_id' => $product_id, 'name' => $name), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function addAjaxProduct($data)
    {
        if ($this->db->insert('products', $data)) {
            $product_id = $this->db->insert_id();
            return $this->getProductByID($product_id);
        }
        return false;
    }

    public function add_products($products = array())
    {
        if (!empty($products)) {
            foreach ($products as $product) {
                $variants = explode('|', $product['variants']);
                unset($product['variants']);
                if ($this->db->insert('products', $product)) {
                    $product_id = $this->db->insert_id();
                    foreach ($variants as $variant) {
                        if ($variant && trim($variant) != '') {
                            $vat = array('product_id' => $product_id, 'name' => trim($variant));
                            $this->db->insert('product_variants', $vat);
                        }
                    }

                    $config_data = array(
                        'product_id' => $product_id,
                        'category_id' => $product['category_id'],
                        'subcategory_id' => $product['subcategory_id'],
                    );
                    $this->site->api_update_product_display($config_data);


                    $temp2 = $this->site->api_select_some_fields_with_where(
                        "
                        warehouse
                        ",
                        "sma_shop_settings",
                        "shop_id = 1",
                        "arr"
                    );
                    $temp = array('product_id' => $product_id, 'warehouse_id' => $temp2[0]['warehouse'], 'avg_cost' => $product['cost']);
                    $this->db->insert('warehouses_products', $temp);
                }
            }
            return true;
        }
        return false;
    }

    public function getProductNames($term, $limit = 5)
    {
        $this->db->select('' . $this->db->dbprefix('products') . '.id, code, ' . $this->db->dbprefix('products') . '.name as name, ' . $this->db->dbprefix('products') . '.price as price, ' . $this->db->dbprefix('product_variants') . '.name as vname')
            ->where("type != 'combo' AND "
                . "(" . $this->db->dbprefix('products') . ".name LIKE '%" . $term . "%' OR code LIKE '%" . $term . "%' OR
                concat(" . $this->db->dbprefix('products') . ".name, ' (', code, ')') LIKE '%" . $term . "%')");
        $this->db->join('product_variants', 'product_variants.product_id=products.id', 'left')
            ->where('' . $this->db->dbprefix('product_variants') . '.name', null)
            ->group_by('products.id')->limit($limit);
        $q = $this->db->get('products');
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getQASuggestions($term, $limit = 20)
    {
        $this->db->select('' . $this->db->dbprefix('products') . '.id, code, ' . $this->db->dbprefix('products') . '.name as name, ' . $this->db->dbprefix('products') . '.quantity as quantity')
            ->where("type != 'combo' AND "
                . "(" . $this->db->dbprefix('products') . ".name LIKE '%" . $term . "%' OR code LIKE '%" . $term . "%' OR
                concat(" . $this->db->dbprefix('products') . ".name, ' (', code, ')') LIKE '%" . $term . "%')")
            ->limit($limit);
        $q = $this->db->get('products');
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getProductsForPrinting($term, $limit = 20)
    {
        $this->db->select('' . $this->db->dbprefix('products') . '.id, code, ' . $this->db->dbprefix('products') . '.name as name, ' . $this->db->dbprefix('products') . '.price as price')
            ->where("(" . $this->db->dbprefix('products') . ".name LIKE '%" . $term . "%' OR code LIKE '%" . $term . "%' OR
                concat(" . $this->db->dbprefix('products') . ".name, ' (', code, ')') LIKE '%" . $term . "%')")
            ->limit($limit);
        $q = $this->db->get('products');
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function updateProduct($id, $data, $items, $warehouse_qty, $product_attributes, $photos, $update_variants)
    {
        if ($this->db->update('products', $data, array('id' => $id))) {
            if ($items) {
                $this->db->delete('combo_items', array('product_id' => $id));
                foreach ($items as $item) {
                    $item['product_id'] = $id;
                    $this->db->insert('combo_items', $item);
                }
            }

            $tax_rate = $this->site->getTaxRateByID($data['tax_rate']);

            if ($warehouse_qty && !empty($warehouse_qty)) {
                foreach ($warehouse_qty as $wh_qty) {
                    $this->db->update('warehouses_products', array('rack' => $wh_qty['rack']), array('product_id' => $id, 'warehouse_id' => $wh_qty['warehouse_id']));
                }
            }

            if (!empty($update_variants)) {
                foreach ($update_variants as $variant) {
                    $vr = $this->getProductVariantByID($id, $variant['id']);
                    if ($vr) {
                        $this->db->update('product_variants', $variant, ['id' => $vr->id]);
                    } else {
                        $this->db->insert('product_variants', $variant);
                    }
                }
            }

            if ($photos) {
                foreach ($photos as $photo) {
                    $this->db->insert('product_photos', array('product_id' => $id, 'photo' => $photo));
                }
            }

            if ($product_attributes) {
                foreach ($product_attributes as $pr_attr) {
                    $pr_attr['product_id'] = $id;
                    $variant_warehouse_id = $pr_attr['warehouse_id'];
                    if ($variant_warehouse_id <= 0) {
                        $variant_warehouse_id = $this->Settings->default_warehouse;
                    }
                    unset($pr_attr['warehouse_id']);
                    $this->db->insert('product_variants', $pr_attr);
                    $option_id = $this->db->insert_id();

                    //if ($pr_attr['quantity'] != 0) {
                    $this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $id, 'warehouse_id' => $variant_warehouse_id, 'quantity' => $pr_attr['quantity']));

                    $tax_rate_id = $tax_rate ? $tax_rate->id : null;
                    $tax = $tax_rate ? (($tax_rate->type == 1) ? $tax_rate->rate . "%" : $tax_rate->rate) : null;
                    $unit_cost = $data['cost'];
                    if ($tax_rate) {
                        if ($tax_rate->type == 1 && $tax_rate->rate != 0) {
                            if ($data['tax_method'] == '0') {
                                $pr_tax_val = ($data['cost'] * $tax_rate->rate) / (100 + $tax_rate->rate);
                                $net_item_cost = $data['cost'] - $pr_tax_val;
                                $item_tax = $pr_tax_val * $pr_attr['quantity'];
                            } else {
                                $net_item_cost = $data['cost'];
                                $pr_tax_val = ($data['cost'] * $tax_rate->rate) / 100;
                                $unit_cost = $data['cost'] + $pr_tax_val;
                                $item_tax = $pr_tax_val * $pr_attr['quantity'];
                            }
                        } else {
                            $net_item_cost = $data['cost'];
                            $item_tax = $tax_rate->rate;
                        }
                    } else {
                        $net_item_cost = $data['cost'];
                        $item_tax = 0;
                    }

                    $subtotal = (($net_item_cost * $pr_attr['quantity']) + $item_tax);
                    $item = array(
                            'product_id' => $id,
                            'product_code' => $data['code'],
                            'product_name' => $data['name'],
                            'net_unit_cost' => $net_item_cost,
                            'unit_cost' => $unit_cost,
                            'quantity' => $pr_attr['quantity'],
                            'option_id' => $option_id,
                            'quantity_balance' => $pr_attr['quantity'],
                            'quantity_received' => $pr_attr['quantity'],
                            'item_tax' => $item_tax,
                            'tax_rate_id' => $tax_rate_id,
                            'tax' => $tax,
                            'subtotal' => $subtotal,
                            'warehouse_id' => $variant_warehouse_id,
                            'date' => date('Y-m-d'),
                            'status' => 'received',
                        );
                    $item['option_id'] = !empty($item['option_id']) && is_numeric($item['option_id']) ? $item['option_id'] : null;
                    //$this->db->insert('purchase_items', $item);

                    //}
                }
            }
            $config_data = array(
                'product_id' => $id
            );
            $this->site->api_update_product_quantity($config_data);

            return true;
        } else {
            return false;
        }
    }

    public function updateProductOptionQuantity($option_id, $warehouse_id, $quantity, $product_id)
    {
        if ($option = $this->getProductWarehouseOptionQty($option_id, $warehouse_id)) {
            if ($this->db->update('warehouses_products_variants', array('quantity' => $quantity), array('option_id' => $option_id, 'warehouse_id' => $warehouse_id))) {
                $this->site->syncVariantQty($option_id, $warehouse_id);
                return true;
            }
        } else {
            if ($this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $product_id, 'warehouse_id' => $warehouse_id, 'quantity' => $quantity))) {
                $this->site->syncVariantQty($option_id, $warehouse_id);
                return true;
            }
        }
        return false;
    }

    public function updatePrice($data = array())
    {
        if ($this->db->update_batch('products', $data, 'code')) {
            return true;
        }
        return false;
    }

    public function deleteProduct($id)
    {
        $temp = $this->site->api_select_some_fields_with_where(
            "
            image
            ",
            "sma_products",
            "id = ".$id,
            "arr"
        );
        if ($temp[0]['image'] != 'no_image.png') {
            $this->site->api_unlink('assets/uploads/'.$temp[0]['image']);
            $this->site->api_unlink('assets/uploads/thumbs/'.$temp[0]['image']);
        }

        $temp = $this->site->api_select_some_fields_with_where(
            "
            photo
            ",
            "sma_product_photos",
            "product_id = ".$id,
            "arr"
        );
        for ($i=0;$i<count($temp);$i++) {
            $this->site->api_unlink('assets/uploads/'.$temp[$i]['photo']);
            $this->site->api_unlink('assets/uploads/thumbs/'.$temp[$i]['photo']);
        }



        if ($this->db->delete('products', array('id' => $id)) && $this->db->delete('warehouses_products', array('product_id' => $id))) {
            $this->db->delete('warehouses_products_variants', array('product_id' => $id));
            $this->db->delete('product_variants', array('product_id' => $id));
            $this->db->delete('product_photos', array('product_id' => $id));
            $this->db->delete('product_prices', array('product_id' => $id));
            return true;
        }
        return false;
    }


    public function totalCategoryProducts($category_id)
    {
        $q = $this->db->get_where('products', array('category_id' => $category_id));
        return $q->num_rows();
    }

    public function getCategoryByCode($code)
    {
        $q = $this->db->get_where('categories', array('code' => $code), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getTaxRateByName($name)
    {
        $q = $this->db->get_where('tax_rates', array('name' => $name), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getAdjustmentByID($id)
    {
        $q = $this->db->get_where('adjustments', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }



    public function getAdjustmentItems($adjustment_id)
    {
        $this->db->select('adjustment_items.*, products.code as product_code, products.name as product_name, products.image, products.details as details, product_variants.name as variant')
            ->join('products', 'products.id=adjustment_items.product_id', 'left')
            ->join('product_variants', 'product_variants.id=adjustment_items.option_id', 'left')
            ->group_by('adjustment_items.id')
            ->order_by('id', 'asc');

        $this->db->where('adjustment_id', $adjustment_id);

        $q = $this->db->get('adjustment_items');
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function syncAdjustment($data = array())
    {
        if (! empty($data)) {
            $clause = array('product_id' => $data['product_id'], 'option_id' => $data['option_id'], 'warehouse_id' => $data['warehouse_id'], 'status' => 'received');
            $qty = $data['type'] == 'subtraction' ? 0 - $data['quantity'] : 0 + $data['quantity'];
            $this->site->setPurchaseItem($clause, $qty);

            $this->site->syncProductQty($data['product_id'], $data['warehouse_id']);
            if ($data['option_id']) {
                $this->site->syncVariantQty($data['option_id'], $data['warehouse_id'], $data['product_id']);
            }
        }
    }

    public function reverseAdjustment($id)
    {
        if ($products = $this->getAdjustmentItems($id)) {
            foreach ($products as $adjustment) {
                $clause = array('product_id' => $adjustment->product_id, 'warehouse_id' => $adjustment->warehouse_id, 'option_id' => $adjustment->option_id, 'status' => 'received');
                $qty = $adjustment->type == 'subtraction' ? (0+$adjustment->quantity) : (0-$adjustment->quantity);
                $this->site->setPurchaseItem($clause, $qty);
                $this->site->syncProductQty($adjustment->product_id, $adjustment->warehouse_id);
                if ($adjustment->option_id) {
                    $this->site->syncVariantQty($adjustment->option_id, $adjustment->warehouse_id, $adjustment->product_id);
                }
            }
        }
    }

    public function addAdjustment($data, $products)
    {
        $config_data = array(
            'type' => 'quantity_adjustment',
            'date' => $data['date'],
            'update' => 0,
        );
        $temp = $this->site->api_calculate_reference_no($config_data);
        $data['reference_no'] = $temp['reference_no'];

        if ($this->db->insert('adjustments', $data)) {
            $adjustment_id = $this->db->insert_id();
            foreach ($products as $product) {
                $product['adjustment_id'] = $adjustment_id;
                $this->db->insert('adjustment_items', $product);
                $adjustment_item_id = $this->db->insert_id();

                $config_data = array(
                    'table_name' => 'sma_products',
                    'select_table' => 'sma_products',
                    'translate' => '',
                    'select_condition' => "id = ".$product['product_id'],
                );
                $temp = $this->site->api_select_data_v2($config_data);
                if ($data['warehouse_id'] == 1) {
                    $temp_2 = $temp[0]['quantity'];
                } else {
                    $temp_2 = $temp[0]['quantity_'.$data['warehouse_id']];
                }
                $temp_3 = array(
                    'quantity_before' => $temp_2
                );
                $this->db->update('sma_adjustment_items', $temp_3, 'id = '.$adjustment_item_id);

                $config_data = array(
                    'product_id' => $product['product_id'],
                    'type' => $product['type'],
                    'quantity' => $product['quantity'],
                    'warehouse_id' => $product['warehouse_id'],
                    'option_id' => $product['option_id'],
                );
                $this->site->api_update_quantity_stock($config_data);

                $config_data = array(
                    'table_name' => 'sma_products',
                    'select_table' => 'sma_products',
                    'translate' => '',
                    'select_condition' => "id = ".$product['product_id'],
                );
                $temp = $this->site->api_select_data_v2($config_data);
                if ($data['warehouse_id'] == 1) {
                    $temp_2 = $temp[0]['quantity'];
                } else {
                    $temp_2 = $temp[0]['quantity_'.$data['warehouse_id']];
                }
                $temp_3 = array(
                    'quantity_after' => $temp_2
                );
                $this->db->update('sma_adjustment_items', $temp_3, 'id = '.$adjustment_item_id);
            }
            $config_data = array(
                'type' => 'quantity_adjustment',
                'date' => $data['date'],
                'update' => 1,
            );
            $temp = $this->site->api_calculate_reference_no($config_data);

            return true;
        }
        return false;
    }

    public function api_reverseAdjustment($config_data)
    {
        $temp = $this->site->api_select_some_fields_with_where(
            "
            *     
            ",
            "sma_adjustment_items",
            "adjustment_id = ".$config_data['adjustment_id'],
            "arr"
        );
        if (is_array($temp)) {
            if (count($temp) > 0) {
                for ($i=0;$i<count($temp);$i++) {
                    if ($temp[$i]['option_id'] <= 0) {
                        $temp_2 = $this->site->api_select_some_fields_with_where(
                            "
                    quantity     
                    ",
                            "sma_warehouses_products",
                            "product_id = ".$temp[$i]['product_id']." and warehouse_id = ".$temp[$i]['warehouse_id'],
                            "arr"
                        );
                    } else {
                        $temp_2 = $this->site->api_select_some_fields_with_where(
                            "
                    quantity     
                    ",
                            "sma_warehouses_products_variants",
                            "option_id = ".$temp[$i]['option_id']." and warehouse_id = ".$temp[$i]['warehouse_id'],
                            "arr"
                        );
                    }

                    $temp_3 = $temp_2[0]['quantity'];
                    if ($temp[$i]['type'] == 'addition') {
                        $temp_3 = $temp_2[0]['quantity'] - $temp[$i]['quantity'];
                    }
                    if ($temp[$i]['type'] == 'subtraction') {
                        $temp_3 = $temp_2[0]['quantity'] + $temp[$i]['quantity'];
                    }
                    $temp_4 = array(
                'quantity' => $temp_3
            );

                    if ($temp[$i]['option_id'] <= 0) {
                        $this->db->update('sma_products', $temp_4, 'id = '.$temp[$i]['product_id']);
                        $this->db->update('sma_warehouses_products', $temp_4, 'product_id = '.$temp[$i]['product_id'].' and warehouse_id = '.$temp[$i]['warehouse_id']);
                    } else {
                        $this->db->update('sma_product_variants', $temp_4, 'id = '.$temp[$i]['option_id']);
                        $this->db->update('sma_warehouses_products_variants', $temp_4, 'option_id = '.$temp[$i]['option_id'].' and warehouse_id = '.$temp[$i]['warehouse_id']);
                    }

                    $config_data_2 = array(
                'product_id' => $temp[$i]['product_id'],
                'warehouse_id' => $temp[$i]['warehouse_id']
            );
                    $this->site->api_update_product_quantity($config_data_2);
                }
            }
        }
    }

    public function updateAdjustment($id, $data, $products)
    {
        //$this->reverseAdjustment($id);
        $config_data = array(
            'adjustment_id' => $id,
        );
        $this->api_reverseAdjustment($config_data);

        if ($this->db->update('adjustments', $data, array('id' => $id)) &&
            $this->db->delete('adjustment_items', array('adjustment_id' => $id))) {
            foreach ($products as $product) {
                $product['adjustment_id'] = $id;
                $this->db->insert('adjustment_items', $product);

                $config_data = array(
                    'product_id' => $product['product_id'],
                    'type' => $product['type'],
                    'quantity' => $product['quantity'],
                    'warehouse_id' => $product['warehouse_id'],
                    'option_id' => $product['option_id'],
                );
                $this->site->api_update_quantity_stock($config_data);
                //$this->syncAdjustment($product);
            }
            return true;
        }
        return false;
    }

    public function deleteAdjustment($id)
    {
        //$this->reverseAdjustment($id);
        $temp = $this->site->api_select_some_fields_with_where(
            "
            *     
            ",
            "sma_adjustments",
            "id = ".$id,
            "arr"
        );
        $config_data = array(
            'adjustment_id' => $id,
        );
        $this->api_reverseAdjustment($config_data);
        if ($this->db->delete('adjustments', array('id' => $id)) &&
            $this->db->delete('adjustment_items', array('adjustment_id' => $id))) {
            return true;
        }
        return false;
    }

    public function getProductQuantity($product_id, $warehouse)
    {
        $q = $this->db->get_where('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row_array();
        }
        return false;
    }

    public function addQuantity($product_id, $warehouse_id, $quantity, $rack = null)
    {
        if ($this->getProductQuantity($product_id, $warehouse_id)) {
            if ($this->updateQuantity($product_id, $warehouse_id, $quantity, $rack)) {
                return true;
            }
        } else {
            if ($this->insertQuantity($product_id, $warehouse_id, $quantity, $rack)) {
                return true;
            }
        }

        return false;
    }

    public function insertQuantity($product_id, $warehouse_id, $quantity, $rack = null)
    {
        $product = $this->site->getProductByID($product_id);
        if ($this->db->insert('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse_id, 'quantity' => $quantity, 'rack' => $rack, 'avg_cost' => $product->cost))) {
            $this->site->syncProductQty($product_id, $warehouse_id);
            return true;
        }
        return false;
    }

    public function updateQuantity($product_id, $warehouse_id, $quantity, $rack = null)
    {
        $data = $rack ? array('quantity' => $quantity, 'rack' => $rack) : $data = array('quantity' => $quantity);
        if ($this->db->update('warehouses_products', $data, array('product_id' => $product_id, 'warehouse_id' => $warehouse_id))) {
            $this->site->syncProductQty($product_id, $warehouse_id);
            return true;
        }
        return false;
    }

    public function products_count($category_id, $subcategory_id = null)
    {
        if ($category_id) {
            $this->db->where('category_id', $category_id);
        }
        if ($subcategory_id) {
            $this->db->where('subcategory_id', $subcategory_id);
        }
        $this->db->from('products');
        return $this->db->count_all_results();
    }

    public function fetch_products($category_id, $limit, $start, $subcategory_id = null)
    {
        $this->db->limit($limit, $start);
        if ($category_id) {
            $this->db->where('category_id', $category_id);
        }
        if ($subcategory_id) {
            $this->db->where('subcategory_id', $subcategory_id);
        }
        $this->db->order_by("id", "asc");
        $query = $this->db->get("products");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getProductWarehouseOptionQty($option_id, $warehouse_id)
    {
        $q = $this->db->get_where('warehouses_products_variants', array('option_id' => $option_id, 'warehouse_id' => $warehouse_id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function syncVariantQty($option_id)
    {
        $wh_pr_vars = $this->getProductWarehouseOptions($option_id);
        $qty = 0;
        foreach ($wh_pr_vars as $row) {
            $qty += $row->quantity;
        }
        if ($this->db->update('product_variants', array('quantity' => $qty), array('id' => $option_id))) {
            return true;
        }
        return false;
    }

    public function getProductWarehouseOptions($option_id)
    {
        $q = $this->db->get_where('warehouses_products_variants', array('option_id' => $option_id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function setRack($data)
    {
        if ($this->db->update('warehouses_products', array('rack' => $data['rack']), array('product_id' => $data['product_id'], 'warehouse_id' => $data['warehouse_id']))) {
            return true;
        }
        return false;
    }

    public function getSoldQty($id)
    {
        $this->db->select("date_format(" . $this->db->dbprefix('sales') . ".date, '%Y-%M') month, SUM( " . $this->db->dbprefix('sale_items') . ".quantity ) as sold, SUM( " . $this->db->dbprefix('sale_items') . ".subtotal ) as amount")
            ->from('sales')
            ->join('sale_items', 'sales.id=sale_items.sale_id', 'left')
            ->group_by("date_format(" . $this->db->dbprefix('sales') . ".date, '%Y-%m')")
            ->where($this->db->dbprefix('sale_items') . '.product_id', $id)
            //->where('DATE(NOW()) - INTERVAL 1 MONTH')
            ->where('DATE_ADD(curdate(), INTERVAL 1 MONTH)')
            ->order_by("date_format(" . $this->db->dbprefix('sales') . ".date, '%Y-%m') desc")->limit(3);
        $q = $this->db->get();
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getPurchasedQty($id)
    {
        $this->db->select("date_format(" . $this->db->dbprefix('purchases') . ".date, '%Y-%M') month, SUM( " . $this->db->dbprefix('purchase_items') . ".quantity ) as purchased, SUM( " . $this->db->dbprefix('purchase_items') . ".subtotal ) as amount")
            ->from('purchases')
            ->join('purchase_items', 'purchases.id=purchase_items.purchase_id', 'left')
            ->group_by("date_format(" . $this->db->dbprefix('purchases') . ".date, '%Y-%m')")
            ->where($this->db->dbprefix('purchase_items') . '.product_id', $id)
            //->where('DATE(NOW()) - INTERVAL 1 MONTH')
            ->where('DATE_ADD(curdate(), INTERVAL 1 MONTH)')
            ->order_by("date_format(" . $this->db->dbprefix('purchases') . ".date, '%Y-%m') desc")->limit(3);
        $q = $this->db->get();
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getAllVariants()
    {
        $q = $this->db->get('variants');
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getWarehouseProductVariant($warehouse_id, $product_id, $option_id = null)
    {
        $q = $this->db->get_where('warehouses_products_variants', array('product_id' => $product_id, 'option_id' => $option_id, 'warehouse_id' => $warehouse_id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getPurchaseItems($purchase_id)
    {
        $q = $this->db->get_where('purchase_items', array('purchase_id' => $purchase_id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getTransferItems($transfer_id)
    {
        $q = $this->db->get_where('purchase_items', array('transfer_id' => $transfer_id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getUnitByCode($code)
    {
        $q = $this->db->get_where("units", array('code' => $code), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getBrandByName($name)
    {
        $q = $this->db->get_where('brands', array('name' => $name), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getStockCountProducts($warehouse_id, $type, $categories = null, $brands = null)
    {
        $this->db->select("{$this->db->dbprefix('products')}.id as id, {$this->db->dbprefix('products')}.code as code, {$this->db->dbprefix('products')}.name as name, {$this->db->dbprefix('warehouses_products')}.quantity as quantity")
        ->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
        ->where('warehouses_products.warehouse_id', $warehouse_id)
        ->where('products.type', 'standard')
        ->order_by('products.code', 'asc');
        if ($categories) {
            $r = 1;
            $this->db->group_start();
            foreach ($categories as $category) {
                if ($r == 1) {
                    $this->db->where('products.category_id', $category);
                } else {
                    $this->db->or_where('products.category_id', $category);
                }
                $r++;
            }
            $this->db->group_end();
        }
        if ($brands) {
            $r = 1;
            $this->db->group_start();
            foreach ($brands as $brand) {
                if ($r == 1) {
                    $this->db->where('products.brand', $brand);
                } else {
                    $this->db->or_where('products.brand', $brand);
                }
                $r++;
            }
            $this->db->group_end();
        }

        $q = $this->db->get('products');
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getStockCountProductVariants($warehouse_id, $product_id)
    {
        $this->db->select("{$this->db->dbprefix('product_variants')}.name, {$this->db->dbprefix('warehouses_products_variants')}.quantity as quantity")
            ->join('warehouses_products_variants', 'warehouses_products_variants.option_id=product_variants.id', 'left');
        $q = $this->db->get_where('product_variants', array('product_variants.product_id' => $product_id, 'warehouses_products_variants.warehouse_id' => $warehouse_id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function addStockCount($data)
    {
        if ($this->db->insert('stock_counts', $data)) {
            return true;
        }
        return false;
    }

    public function finalizeStockCount($id, $data, $products)
    {
        if ($this->db->update('stock_counts', $data, array('id' => $id))) {
            foreach ($products as $product) {
                $this->db->insert('stock_count_items', $product);
            }
            return true;
        }
        return false;
    }

    public function getStouckCountByID($id)
    {
        $q = $this->db->get_where("stock_counts", array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getStockCountItems($stock_count_id)
    {
        $q = $this->db->get_where("stock_count_items", array('stock_count_id' => $stock_count_id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return null;
    }

    public function getAdjustmentByCountID($count_id)
    {
        $q = $this->db->get_where('adjustments', array('count_id' => $count_id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getProductVariantID($product_id, $name)
    {
        $q = $this->db->get_where("product_variants", array('product_id' => $product_id, 'name' => $name), 1);
        if ($q != false && $q->num_rows() > 0) {
            $variant = $q->row();
            return $variant->id;
        }
        return false;
    }
    public function getProductVariantByID($product_id, $id)
    {
        $q = $this->db->get_where("product_variants", array('product_id' => $product_id, 'id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }


    public function filter_product($warehouse_id = "", $search="", $sort_by = "", $sort_order = "", $limit = "", $offset = "")
    {
        $search_arr =array(
            'search_name'	   => $search,
            'search_code'	   => $search,
            'search_brand'	   => $search,
            'search_category'  => $search,
            'search_cost'	   => $search,
            'search_price'	   => $search,
            'search_qty'	   => $search,
            'search_unit'	   => $search,
            'search_alert_qty' => $search,
            'warehouse_id'	   => $warehouse_id
        );

        $sql_query = $this->db
                ->select($this->db->dbprefix('products') . ".id as productid, {$this->db->dbprefix('products')}.image as image, {$this->db->dbprefix('products')}.code as code, {$this->db->dbprefix('products')}.name as name, {$this->db->dbprefix('brands')}.name as brand, {$this->db->dbprefix('categories')}.name as cname, cost as cost, price as price, COALESCE(quantity, 0) as quantity, {$this->db->dbprefix('units')}.code as unit, '' as rack, alert_quantity, hide, featured")
        ->join('categories', 'products.category_id=categories.id', 'left')
        ->join('units', 'products.unit=units.id', 'left')
        ->join('brands', 'products.brand=brands.id', 'left');
        if ($search) {
            $sql_query = $sql_query->order_by('name', 'DESC')
            ->where("({$this->db->dbprefix('products')}.name LIKE '%".$search_arr['search_name']."%' OR {$this->db->dbprefix('products')}.code LIKE '%".$search_arr['search_code']."%' OR {$this->db->dbprefix('brands')}.name LIKE '%".$search_arr['search_brand']."%' OR {$this->db->dbprefix('categories')}.name LIKE '%".$search_arr['search_category']."%' OR cost LIKE '%".$search_arr['search_cost']."%'  OR quantity LIKE '%".$search_arr['search_qty']."%'  OR alert_quantity LIKE '%".$search_arr['search_alert_qty']."%')");
        }
        if ($sort_by == 'category') {
            $sort_by = 'cname';
        }
        if ($sort_by == 'quantity_3') {
            $sort_by = "getTranslate(add_ons,'quantity_3','".f_separate."','".v_separate."')";
        }
        if ($sort_order) {
            $sql_query = $sql_query->order_by($sort_by, $sort_order);
        } else {
            $sql_query = $sql_query->order_by('productid', 'desc');
        }
        
        if ($limit !='') {
            return $sql_query->order_by('products.id', 'DESC')->get('products', $limit, $offset);
        }
    
        return $sql_query->order_by('products.id', 'DESC')->get('products');
    }

    public function api_get_product($config_data) {
        $condition = '';
        $config_data['search'] = trim($config_data['search']);

        if ($_GET['adjustment_track'] != '') {
            $temp = $this->site->api_select_some_fields_with_where(
                "product_id",
                "sma_adjustment_items",
                "adjustment_id = ".$_GET['adjustment_track'],
                "arr"
            );
            if (count($temp) > 0) {
                $condition = " and (t1.id = ".$temp[0]['product_id'];
                for ($i=1;$i<count($temp);$i++) {
                    $condition .= " or t1.id = ".$temp[$i]['product_id'];
                }
                $condition .= ')';
            }
        } elseif ($config_data['search'] != '') {
            $condition .= "
                and t1.name LIKE '%". $config_data['search'] ."%'
                OR t1.code LIKE '%". $config_data['search'] ."%'
            ";
            if (is_numeric($config_data['search'])) {
                $condition .= ' OR t1.'.$config_data['table_id']." = ".$config_data['search'];
            }
        }

        if ($this->input->get('featured') == 'yes')
            $condition .= " and t1.featured = 1";
        elseif ($this->input->get('featured') == 'no')
            $condition .= " and (t1.featured != 1 or t1.featured is null)";
        
        if ($this->input->get('promotion') == 'yes' || $this->input->get('promotion') == 'expired')
            $condition .= " and t1.promotion = 1";
        elseif ($this->input->get('promotion') == 'no')
            $condition .= " and (t1.promotion != 1 or t1.promotion is null)";

        if ($this->input->get('supplier') == 'yes')
            $condition .= " and (t1.supplier1 > 0 or t1.supplier2 > 0 or t1.supplier3 > 0 or t1.supplier4 > 0 or t1.supplier5 > 0)";
        elseif ($this->input->get('supplier') == 'no')
            $condition .= " and (t1.supplier1 <= 0 or t1.supplier1 is null) and (t1.supplier2 <= 0 or t1.supplier2 is null) and (t1.supplier3 <= 0 or t1.supplier3 is null) and (t1.supplier4 <= 0 or t1.supplier4 is null) and (t1.supplier5 <= 0 or t1.supplier5 is null)";

        if ($this->input->get('c_id') > 0) {
            $temp_2 = $this->input->get('c_id');
            $temp = $this->site->api_select_some_fields_with_where("
                parent_id
                "
                ,"sma_categories"

                ,"id = ".$temp_2
                ,"arr"
            );            
            if ($temp[0]['parent_id'] == 0)
                $condition .= "  and t1.category_id = ".$temp_2;
            else
                $condition .= "  and t1.subcategory_id = ".$temp_2;
        }
                
        $order_by = '';
        $order_by = 'order by t2.ordering asc, t1.ordering asc';
        if ($config_data['sort_by'] != '') {
            $temp = $config_data['warehouse'];

            if ($config_data['sort_by'] == 'category') {
                $order_by = "order by t2.name";
            } elseif ($config_data['sort_by'] == 'show') {
                $order_by = "order by show_customer_list_selected";
            } elseif ($config_data['sort_by'] == 'unit') {
                $order_by = "order by unit_name";
            } elseif ($config_data['sort_by'] == 'total_quantity') {
                $temp_2 = '';
                for ($i=0;$i<count($temp);$i++) {
                    $temp_2 .= ' + CAST(quantity_'.$temp[$i]['id'].' AS SIGNED)';
                }
                $order_by = "order by (t1.quantity ".$temp_2.")";
            } else {
                $order_by = 'order by t1.'.$config_data['sort_by'];
            }

            for ($i=0;$i<count($temp);$i++) {
                if ($config_data['sort_by'] == 'quantity_'.$temp[$i]['id']) {
                    $order_by = "order by CAST(quantity_".$temp[$i]['id']." AS SIGNED)";
                    break;
                }
            }

            $order_by .= ' '.$config_data['sort_order'];
        }


        if ($config_data['limit'] != '') {
            $order_by .= ' limit '.$config_data['offset_no'].', '.$config_data['limit'];
        }

        
        $temp_field = $this->site->api_get_condition_add_ons_field("t1.add_ons", "sma_add_ons", "table_name = '".$config_data['table_name']."'");

        $select_data = $this->site->api_select_some_fields_with_where(
            "t1.*, t2.ordering as category_ordering 
            ".$temp_field
            ,$config_data['table_name']." as t1 inner join sma_categories as t2 on t1.category_id = t2.id"
            ,"t1.id > 0 ".$condition." ".$order_by
            ,"arr"
        );


        for ($i=0;$i<count($select_data);$i++) {
            if ($select_data[$i]['promotion'] == 1) {                
                $config_data_2 = array(
                    'id' => $select_data[$i]['id'],
                    'customer_id' => $this->session->userdata('company_id'),
                );
                $temp = $this->site->api_calculate_product_price($config_data_2);
                if ($temp['promotion']['price'] > 0)
                    $select_data[$i]['promotion_status'] = 'yes';
                else
                    $select_data[$i]['promotion_status'] = 'expired';
            }
            else
                $select_data[$i]['promotion_status'] = 'no';

            if ($this->input->get('promotion') == 'yes' && $select_data[$i]['promotion_status'] != 'yes')
                unset($select_data[$i]);
            elseif ($this->input->get('promotion') == 'expired' && $select_data[$i]['promotion_status'] != 'expired')
                unset($select_data[$i]);
            elseif ($this->input->get('promotion') == 'no' && $select_data[$i]['promotion_status'] != 'no')
                unset($select_data[$i]);
        }

        $temp_select_data = array();
        $k = 0;
        for ($i=0;$i<count($select_data);$i++) {
            if ($select_data[$i]['id'] > 0) {    
                $temp_select_data[$k] = $select_data[$i];
                $k++;
            }
        }
        $select_data = array();
        $select_data = $temp_select_data;

        $select_data_2 = array();
        $select_data_3 = array();
        $select_data_4 = array();
        $k = 0;
        $j = 0;
        for ($i=0;$i<count($select_data);$i++) {
            if ($select_data[$i]['no_use'] != 'yes') {
                $select_data_2[$k] = $select_data[$i];
                $k++;
            }
            else {
                $select_data_3[$j] = $select_data[$i];
                $j++;                
            }
        }
        $k = 0;
        for ($i=0;$i<count($select_data_2);$i++) {
            $select_data_4[$k] = $select_data_2[$i];
            $k++;
        }
        for ($i=0;$i<count($select_data_3);$i++) {
            $select_data_4[$k] = $select_data_3[$i];
            $k++;
        }

        return $select_data_4;
    }

    public function api_get_adjustment($config_data)
    {
        if ($_GET['adjustment_track'] != '') {
            $temp = explode('-', $_GET['adjustment_track']);
            $condition = " and (t1.id = ".$temp[1];
            for ($i=2;$i<count($temp);$i++) {
                $condition .= " or t1.id = ".$temp[$i];
            }
            $condition .= ')';
        } elseif ($_GET['search'] != '') {
            $search = trim($this->input->get('search', true));
            $condition .= " and (t1.reference_no LIKE '%".$search."%')";
        }

        if (is_numeric($_GET['warehouse'])) {
            $condition .= " and t1.warehouse_id = ".$_GET['warehouse'];
        }

        if (is_numeric($_GET['created_by'])) {
            $condition .= " and t1.created_by = ".$_GET['created_by'];
        }

        if (is_numeric($_GET['product'])) {
            $condition .= " and t2.product_id = ".$_GET['product'];
        }

        $order_by = '';
        if ($_GET['sort_by'] != '') {
            if ($_GET['sort_by'] == 'date') {
                $order_by = "order by t1.date";
            } elseif ($_GET['sort_by'] == 'warehouse_name') {
                $order_by = "order by warehouse_name";
            } elseif ($_GET['sort_by'] == 'created_by_name') {
                $order_by = "order by created_by_name";
            } else {
                $order_by = 'order by t1.'.$_GET['sort_by'];
            }

            $order_by .= ' '.$_GET['sort_order'];
        } else {
            $order_by = 'order by t1.date Desc';
        }

        if ($config_data['limit'] != '') {
            $order_by .= ' limit '.$config_data['offset_no'].', '.$config_data['limit'];
        }

        $select_data = $this->site->api_select_some_fields_with_where(
            "distinct t1.id, t1.*, t3.name as warehouse_name
            , concat(t4.first_name,' ',t4.last_name) as created_by_name",
            $config_data['table_name']." as t1 inner join sma_adjustment_items as t2 on t1.id = t2.adjustment_id inner join sma_warehouses as t3 on t3.id = t1.warehouse_id inner join sma_users as t4 on t4.id = t1.created_by",
            "t1.".$config_data['table_id']." > 0 ".$condition." ".$order_by,
            "arr"
        );

        return $select_data;
    }

public function get_promotion($config_data) {
    $condition = '';
    if ($config_data['search'] != '') {
        $condition .= "
            and (name LIKE '%". $config_data['search'] ."%'
            OR type LIKE '%". $config_data['search'] ."%'
        ";
        if (is_numeric($config_data['search']))
            $condition .= ' OR '.$config_data['table_id']." = ".$config_data['search'];
        $condition .= ')';
    }

    $order_by = '';
    if ($config_data['sort_by'] != '') {

        $order_by .= 'order by '.$config_data['sort_by'];
            
        $order_by .= ' '.$config_data['sort_order'];                
    }
    else
        $order_by .= 'order by ordering asc';

    if ($limit != '') {
        $this->db->limit($limit, $offset);
    }

    if ($config_data['limit'] != '')
        $order_by .= ' limit '.$config_data['offset_no'].', '.$config_data['limit'];

    if ($config_data['selected_id'] > 0)
        $condition = ' and '.$config_data['table_id']." = ".$config_data['selected_id'];
    else {
        
    }

    $temp_field = $this->site->api_get_condition_add_ons_field("add_ons","sma_add_ons","table_name = '".$config_data['table_name']."'");
    $select_data = $this->site->api_select_some_fields_with_where("* 
        ".$temp_field
        ,$config_data['table_name']
        ,$config_data['table_id']." > 0 ".$condition." ".$order_by
        ,"arr"
    );  
                
    return $select_data;
}


}
