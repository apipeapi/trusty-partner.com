<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Restaurant_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

function restaurant_show_table($config_data_function){
    $return = array();
    $config_data = array(
        'table_name' => 'sma_restaurant',
        'select_table' => 'sma_restaurant',
        'translate' => '',
        'select_condition' => "id = ".$config_data_function['table_id'],
    );
    $temp_display_table = $this->site->api_select_data_v2($config_data);
    $return['display'] = '
        <table>
            <tr>
                <td valign="middle" align="center">
                    <div class="api_temp_1 temp_height">
    ';
    if ($_GET['table_id'] == $config_data_function['table_id'])
        $temp_2 = 'api_bg_'.$temp_display_table[0]['status'].'_status selected';
    else    
        $temp_2 = 'api_bg_'.$temp_display_table[0]['status'].'_status';
    

    if ($this->api_helper->is_mobile() == 1)
        $temp_function = 'api_ajax_load_menu_mobile';
    else
        $temp_function = 'api_ajax_load_menu';

    $return['display'] .= '
                    <a href="javascript:void(0);" onclick="
                        var postData = {
                            \'table_id\' : \''.$config_data_function['table_id'].'\',
                            \'ajax_url\' : \''.base_url().'admin/restaurant/'.$temp_function.'\',
                        };
                        '.$temp_function.'(postData);                              
                    ">
                        <div class="api_temp_table_1 '.$temp_2.' api_temp_table_id_'.$config_data_function['table_id'].'">
                            <table width="100%">
                                <tr>
                                    <td valign="middle" class="api_text_center" >
                                        <div class="api_title">'.$temp_display_table[0]['table_name'].'</div>
                                    </td>            
    ';
    $temp = str_replace('_', ' ', strtolower($temp_display_table[0]['status'])); 
    $return['display'] .= '     
                                </tr>
                                <tr>
                                    <td  valign="middle">                
                                        <div class="api_status">'.$temp.'</div>
                                    </td>
                                </tr>
                            </table>    
                        </div> 
                    </a>    
                    <div>
                </td> 
            </tr>
        </table>
    ';
    return $return;
}

function view_total_detail($config_data_function){
    $return = array();
    if($config_data_function > 0)
    $config_data = array(
        'table_name' => 'sma_restaurant',
        'select_table' => 'sma_restaurant',
        'translate' => '',
        'select_condition' => "id = ".$config_data_function['table_id'],
    );
    $temp_display_table = $this->site->api_select_data_v2($config_data);
    
    $return['display'] = '
            <table width="100%">
                <tr>
                    <td valign="middle" class="api_temp_footer_1">
                        <table>
                            <tr>
                                <td>1 items</td>
                            </tr>
                            <tr>
                                <td style="font-size: 20px">Total</td>
                                <td valign="middle" style="font-size: 20px">: $25</td>
                            </tr>
                        </table>
                    </td>
                
                    <td class="api_temp_footer_2" valign="middle">
                        <a href="javascript:void(0);" onclick="
                            var postData = {
                                \'table_id\' : \''.$config_data_function['table_id'].'\',
                                \'ajax_url\' : \''.base_url().'admin/restaurant/api_ajax_load_view_detail\',
                            };
                            api_ajax_load_view_detail(postData); 
                        " >                    
                        <table width="100%" border="0">
                        <tr>
                        <td valign="middle" align="center" height="70">
                            <div class="api_temp_height temp_icon">
                                <i class="fa fa-eye text-center" aria-hidden="true"></i>
                            </div>
                            <div class="padding05 text-center temp_title_icon">
                                View
                            </div>                        
                        </td>
                        </tr>
                        </table>
                        </a>
                        
                    </td>
                    
                    <td class="api_temp_footer_3" valign="middle">
                        <a href="javascript:void(0);" onclick="
                            var postData = {
                                \'table_id\' : \''.$config_data_function['table_id'].'\',
                                \'ajax_url\' : \''.base_url().'admin/restaurant/api_ajax_load_payment_detail\',
                            };
                            api_ajax_load_payment_detail(postData); 
                        " >                    
                        <table width="100%" border="0">
                        <tr>
                        <td valign="middle" align="center" height="70">
                            <div class="api_temp_height temp_icon">
                                <i class="fa fa-money text-center" aria-hidden="true"></i>
                            </div>
                            <div class="padding05 text-center temp_title_icon">
                                Payment
                            </div>                        
                        </td>
                        </tr>
                        </table>
                        </a> 
                    </td>
                </tr>
            </table>
    ';
    $return['display_mobile'] = ' 
                <table width="100%">
                    <tr>
                        <td valign="middle" class="api_temp_footer_1">
                            <table>
                                <tr>
                                    <td>1 items</td>
                                </tr>
                                <tr>
                                    <td style="font-size: 20px">Total</td>
                                    <td valign="middle" style="font-size: 20px">: $25</td>
                                </tr>
                            </table>
                        </td>
                    
                        <td class="api_temp_footer_2" valign="middle">
                            <a href="javascript:void(0);" onclick="
                                var postData = {
                                    \'table_id\' : \''.$config_data_function['table_id'].'\',
                                    \'ajax_url\' : \''.base_url().'admin/restaurant/api_ajax_load_view_detail_mobile\',
                                };
                                api_ajax_load_view_detail_mobile(postData); 
                            " >                  
                            <table width="100%" border="0">
                            <tr>
                            <td valign="middle" align="center" height="70">
                                <div class="api_temp_height temp_icon">
                                    <i class="fa fa-eye text-center" aria-hidden="true"></i>
                                </div>
                                <div class="padding05 text-center temp_title_icon">
                                    View
                                </div>                        
                            </td>
                            </tr>
                            </table>
                            </a>
                            
                        </td>
                        
                        <td class="api_temp_footer_3" valign="middle">
                            <a href="javascript:void(0);" onclick="
                                var postData = {
                                    \'table_id\' : \''.$config_data_function['table_id'].'\',
                                    \'ajax_url\' : \''.base_url().'admin/restaurant/api_ajax_load_payment_detail_mobile\',
                                };
                                api_ajax_load_payment_detail_mobile(postData); 
                            " >                   
                            <table width="100%" border="0">
                            <tr>
                            <td valign="middle" align="center" height="70">
                                <div class="api_temp_height temp_icon">
                                    <i class="fa fa-money text-center" aria-hidden="true"></i>
                                </div>
                                <div class="padding05 text-center temp_title_icon">
                                    Payment
                                </div>                        
                            </td>
                            </tr>
                            </table>
                            </a> 
                        </td>
                    </tr>
                </table>
    ';
   
    return $return;
}
function view_payment_detail($config_data_function){
    $return['display'] .= '
        <div class="api_temp_wrapper_payment">
            <div class="api_large"> 
                <i class="fa fa-money text-center" aria-hidden="true"></i>
                Payment : '.$config_data_function['table_name'].' 
            </div>
            <div class="api_clear_both"></div>
            <div class="col-md-12">
            <div class="col-md-4 api_temp_padding_top_bottom">
                <div class="form-group api_temp_cashier">
                    '.lang("Cashier",'cashier').'
                    <div class="controls"> 
    ';
                        $config_data = array(
                            'table_name' => 'sma_cashier',
                            'select_table' => 'sma_cashier',
                            'field_name' => 'translate',
                            'translate' => 'yes',
                            'select_condition' => "id > 0",
                        );
                        $select_data = $this->site->api_select_data_v2($config_data);
                        for ($j=0;$j<count($select_data);$j++) {        
                            if ($select_data[$j]['title_en'] != '')
                                $td[$select_data[$j]['id']] = $select_data[$j]['title_en'];
                        }
                        $config_data = array(
                            'table_name' => 'sma_sales',
                            'select_table' => 'sma_sales',
                            'select_condition' => "id > 0 order by id desc limit 1",
                        );
                        $select_sales = $this->site->api_select_data_v2($config_data);
    $return['display'] .= '
                        '.form_dropdown('cashier', $td, $select_sales[0]['cashier'], 'class="form-control api_select_v2 api_temp_height"  id="api_cashier"').'                  
                    </div>
                </div>
            </div>
            <div class="col-md-4 api_temp_padding_top_bottom">
                        <div class="form-group">
                            <div class="api_temp_input">
                                '.lang("Amount_USD", "amount_1").'
                            </div>
                                <input name="amount[]" type="hidden" id="amount_1" class="pa form-control kb-pad1_temp api_numberic_input" />
                                <input name="amount_v1[]" type="hidden" id="amount_1_v1"
                                class="pa form-control kb-pad1_temp api_numberic_input"/>
                                <input type="text" name="amount_v2[]" id="amount_1_v2"  class="pa form-control api_numberic_input api_temp_height" />
                            </div>
                        </div>
                    <input type="hidden" id="rate_currency" value="4100"/>
                    <div class="col-md-4 api_temp_padding_top_bottom">
                        <div class="form-group">
                            <div class="api_temp_input">
                                '.lang("Amount_KHR", "amount_khr").'
                            </div>
                                <input name="amount[]" type="hidden" id="amount_1" class="pa form-control kb-pad1_temp api_numberic_input" />
                                <input name="amount_v1[]" type="hidden" id="amount_1_v1"
                                class="pa form-control kb-pad1_temp api_numberic_input"/>
                                <input type="text" name="amount_v2[]" id="amount_1_v2"  class="pa form-control api_numberic_input api_temp_height"/>
                        </div>
                    </div>
            </div>

            <div class="row">  
                <div class="col-md-12">                             
                    <div class="col-md-4 api_temp_padding_top_bottom">
                        <div class="form-group">
                            <div class="api_temp_input">
                                '.lang("Customer_Pay_USD", "customer_pay_usd").'
                            </div>
                            <input name="customer_pay_usd" type="text" id="customer_pay_usd"
                            class="pa form-control api_numberic_input api_temp_height" />
                        </div>
                    </div>
                    <div class="col-md-4 api_temp_padding_top_bottom">
                        <div class="form-group">
                            <div class="api_temp_input">
                                '.lang("Customer_Pay_KHR", "customer_pay_khr").'
                            </div>
                            <input name="customer_pay_khr" type="text" id="customer_pay_khr"
                            class="pa form-control api_numberic_input api_temp_height" />
                        </div>
                    </div>
                    <div class="col-md-4 api_temp_label api_temp_padding_top_bottom" align="left">
                    <div class="form-group">
                        <div class="api_temp_lable_1">
                            '.lang("Shipping", "shipping").'
                        </div>    
                        <span>
                            <input type="radio" name="shipping_v2" value="yes" />  Yes
                            <span style="margin-left:10px">
                                <input type="radio" name="shipping_v2" value="no" checked />  No
                            </span>
                        </span>
                    </div>
                </div>
                </div>     
            </div>
            <div class="row">
            <div class="col-md-12">
                
                <div class="clearfir"></div>
                <div id="payments">
                    <div class="well-sm well_1 api_temp_modal">
                        <div class="payment">
                            <div class="row">
                                <div class="col-md-12 api_temp_padding_top_bottom">
                                    <div class="form-group">
                                        <div class="api_temp_input_1">  
                                            '.lang("paying_by", "paid_by_1").'
                                        </div> 
                                        <select name="paid_by[]" id="paid_by_1" class="form-control paid_by api_temp_height">
                                            <option value="ppp">'. lang("paypal_pro") .'</option>
                                            <option value="stripe">'. lang("stripe") .'</option>
                                            <option value="authorize">'. lang("authorize") .'</option>
                                        </select>   
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>     
            </div>    
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6  api_temp_group api_temp_padding_top_bottom">
                        <div class="form-group">
                            <div class="api_temp_lable" align="left">
                                '.lang("Change in USD", "Change in USD").'
                            </div> 
                            <fieldset class="scheduler-border api_temp_height_v2">
                                                          
                                    <div class="col-md-12">
                                            <div class="form-group">
                                                <table width="100%">
                                                    <tr>
                                                        <td valign="middle" style="font-weight:bold;border-bottom: 1px solid #fff;" >
                                                            Customer Change (USD) :
                                                        </td>
                                                        <td id="customer_change_usd"  style="width: 15%;font-weight: bold;font-size: 25px;border-bottom: 1px solid #fff;padding-left: 5px; color: red">
                                                            0.00
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-weight:bold;margin-top:5px;border-bottom: 1px solid #fff;" >
                                                            Customer Change (KHR) :
                                                        </td>
                                                        <td id="customer_change_khr" style="width: 15%;font-weight: bold;font-size: 25px;border-bottom: 1px solid #fff;padding-left: 5px;color: red">
                                                            0.00
                                                        </td>
                                                    </tr>
                                                </table>    
                                            </div>
                                    </div>
                            </fieldset> 
                        </div>  
                    </div>
                    <div class="col-md-6  api_temp_group api_temp_padding_top_bottom">  
                    <div class="form-group">
                        <div class="api_temp_lable" align="left">
                            '.lang("Change in KHR", "Change in KHR").'
                        </div>
                        <fieldset class="scheduler-border api_temp_height_v2">
                                <div class="col-md-12">
                                        <div class="form-group">
                                            <table style="border-bottom:none;width:100%">
                                                <td style="font-weight:bold;border-bottom:none;margin-top:5px" >
                                                    Customer Change (KHR) :
                                                </td>
                                                <td id="khmer_change" style="border-bottom:none;text-align:right;width: 15%;font-weight: bold;font-size: 25px;padding-left: 5px; color: red" >
                                                    0.00
                                                </td>
                                            </table>    
                                        </div>
                                </div>
                        </fieldset> 
                    </div>
                </div> 
                <div class="col-md-12 api_temp_padding_top_bottom">
                    <div class="form-group api_text">
                        <div class="api_temp_input_1">  
                            '.lang('payment_note', 'payment_note').'
                        </div>    
                        <textarea name="payment_note[]" id="payment_note_1" class="pa form-control kb-text payment_note" height="100px"></textarea>
                    </div>
                </div>
            </div> 
        </div>
    </div>  
    <div class="api_clear_both"></div>
    <div class="api_heigh_both"></div>
    ';

    if ($config_data_function['hide_btn_submit'] != 1)
        $return['display'] .= '
            <table width="100%" border="0">
                <tr>
                <td class="api_temp_submit">
                    <button class="btn btn-block btn-success api_temp_btn" id="submit-sale" >
                        '.lang('submit').'
                    </button>
                </td>
                </tr>
            </table>
        ';
    else 
        $return['display'] .= '<div class="api_height_50"></div>';

    $return['display'] .= '      
        </div>      
    ';
    return $return;
}

function view_table_selection($config_data_function){
    $return = array();

    $config_data = array(
        'table_name' => 'sma_restaurant',
        'select_table' => 'sma_restaurant',
        'translate' => '',
        'select_condition' => "id > 0 order by ordering asc",
    );
    $temp_select_data = $this->site->api_select_data_v2($config_data);
    for ($i=0;$i<count($temp_select_data);$i++) {
        $config_data = array(
            'table_id' => $temp_select_data[$i]['id'],
        );
        ${'temp_show_table_'.$temp_select_data[$i]['ordering']} = $this->restaurant_model->restaurant_show_table($config_data);
    }
    
    $return['display'] .= '
        <div class="api_temp_table_pc">
            <div class="api_temp_status">
                <table>
                    <tr>
                        <td valign="middle">
                            <div class="api_temp_border_1"></div>
                        </td>
                        <td valign="middle">
                            <div class="api_temp_border_2">Ready</div>
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle">
                            <div class="api_temp_border_3"></div>
                        </td>
                        <td valign="middle">
                            <div class="api_temp_border_4">Seated</div>
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle">
                            <div class="api_temp_border_5"></div>
                        </td>
                        <td valign="middle">
                            <div class="api_temp_border_6">Ordered</div>
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle">
                            <div class="api_temp_border_7"></div>
                        </td>
                        <td valign="middle">
                            <div class="api_temp_border_8">Served</div>
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle">
                            <div class="api_temp_border_9"></div>
                        </td>
                        <td valign="middle">
                            <div class="api_temp_border_10">Bill Printed</div>
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle">
                            <div class="api_temp_border_11"></div>
                        </td>
                        <td valign="middle">
                            <div class="api_temp_border_12">To Clean</div>
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle">
                            <div class="api_temp_border_13"></div>
                        </td>
                        <td valign="middle"> 
                            <div class="api_temp_border_14">Reserved</div>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="api_temp_status_1">
                <table>
                    <tr>
                        <td valign="middle" class="temp_height_mobile">
                            <div class="api_temp_2 api_ready">
                                <table>
                                    <tr>
                                        <td valign="middle">
                                            <div class="api_temp_border_1"></div>
                                        </td>
                                        <td valign="middle">
                                            <div class="api_temp_border_2">Ready</div>
                                        </td>
                                    </tr>
                                </table> 
                            </div>                
                            <div class="api_temp_2 api_seated">
                                <table>
                                    <tr>
                                        <td valign="middle">
                                            <div class="api_temp_border_3"></div>
                                        </td>
                                        <td valign="middle">
                                            <div class="api_temp_border_4">Seated</div>
                                        </td>
                                    </tr>
                                </table> 
                            </div>                
                            <div class="api_temp_2 api_ordered">
                                <table>
                                    <tr>
                                        <td valign="middle">
                                            <div class="api_temp_border_5"></div>
                                        </td>
                                        <td valign="middle">
                                            <div class="api_temp_border_6">Ordered</div>
                                        </td>
                                    </tr>
                                </table> 
                            </div>                
                            <div class="api_temp_2 api_served">
                                <table>
                                    <tr>
                                        <td valign="middle">
                                            <div class="api_temp_border_7"></div>
                                        </td>
                                        <td valign="middle">
                                            <div class="api_temp_border_8">Served</div>
                                        </td>
                                    </tr>
                                </table> 
                            </div>                
                            <div class="api_temp_2 api_bill">
                                <table>
                                    <tr>
                                        <td valign="middle">
                                            <div class="api_temp_border_9"></div>
                                        </td>
                                        <td valign="middle">
                                            <div class="api_temp_border_10">Bill Printed</div>
                                        </td>
                                    </tr>
                                </table> 
                            </div>                
                            <div class="api_temp_2 api_to_clean">
                                <table>
                                    <tr>
                                        <td valign="middle">
                                            <div class="api_temp_border_11"></div>
                                        </td>
                                        <td valign="middle">
                                            <div class="api_temp_border_12">To Clean</div>
                                        </td>
                                    </tr>
                                </table> 
                            </div>                
                            <div class="api_temp_2 api_reserved">
                                <table>
                                    <tr>
                                        <td valign="middle">
                                            <div class="api_temp_border_13"></div>
                                        </td>
                                        <td valign="middle"> 
                                            <div class="api_temp_border_14">Reserved</div>
                                        </td>
                                    </tr>
                                </table> 
                            </div>
                            <div class="api_clear_both"></div>
                        </td>
                    </tr>
                </table>                
            </div>
            <div align="center">   
                '.$temp_show_table_1['display'].'
                '.$temp_show_table_2['display'].'
                '.$temp_show_table_3['display'].'
            </div>
            <div class="api_height_20"></div>
            <div class="col-md-6 text-left">
                <table class="api_table_1">
                    <tr>
                        <td>'.$temp_show_table_4['display'].'</td>
                        <td>'.$temp_show_table_5['display'].'</td>
                    </tr>
                </table>   
            </div> 
            <div class="col-md-6 text-right"> 
                <table class="api_table_2">
                    <tr>  
                        <td>'.$temp_show_table_6['display'].'</td>
                        <td>'.$temp_show_table_7['display'].'</td>
                        <td>'.$temp_show_table_8['display'].'</td>
                    </tr>
                </table>
            </div> 
            <div class="api_height_v2"></div>
            <div class="col-md-6 text-left">
                <table class="api_table_1">
                    <tr>
                        <td>'.$temp_show_table_9['display'].'</td>
                        <td>'.$temp_show_table_10['display'].'</td>
                    </tr>
                </table>
            </div> 
            <div class="col-md-6 text-right"> 
                <table class="api_row_2 api_table_2">
                    <tr>  
                        <td>'.$temp_show_table_11['display'].'</td>
                        <td>'.$temp_show_table_12['display'].'</td>
                        <td>'.$temp_show_table_13['display'].'</td>
                    </tr>
                </table>
            </div> 
            <div class="api_height_v2"></div>
            <div class="col-md-6 text-left">
                <table class="api_table_1">
                    <tr>
                        <td>'.$temp_show_table_14['display'].'</td>
                    </tr>
                </table>
            </div> 
            <div class="col-md-6 text-right"> 
                <table class="api_row_3 api_table_2">
                    <tr>  
                        <td>'.$temp_show_table_15['display'].'</td>
                        <td>'.$temp_show_table_16['display'].'</td>
                        <td>'.$temp_show_table_17['display'].'</td>
                    </tr>
                </table>
            </div>
        </div>  
    ';  

    return $return;
}

function display_top_header_menu_icon($config_data_function){
    $return = array();
    $config_data = array(
        'table_name' => 'sma_restaurant',
        'select_table' => 'sma_restaurant',
        'translate' => '',
        'select_condition' => "id = ".$config_data_function['table_id'],
        
    );
    $temp_select_data = $this->site->api_select_data_v2($config_data);

    $return['display'] = '
        <div class="col-md-12 api_temp_top_header api_header_kh">
            <table width="100%" border="0">
            <tr>
                <td>
                    <div class="header-brand" href=""><span class="pos-logo-sm">'.lang('Restaurant').'</span></div>
                </td>
                <td valign="middle">
                    <div class="header-btn api_restaurant_top_menu_icons">
                        <ul class="api_kh api_temp_btn pull-right">                
                            <li class="dropdown">
                                <a class="btn bgrey  pos-tip api_btn_view_menu" title="View Menu" data-placement="bottom" href="javascript:void(0);" onclick="
                                    var postData = {
                                        \'table_id\' : \''.$config_data_function['table_id'].'\',
                                        \'ajax_url\' : \''.base_url().'admin/restaurant/api_ajax_load_menu\',
                                    };
                                    api_ajax_load_menu(postData); 
                                ">
                                    <i class="fa fa-list-alt" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a class="btn blightOrange pos-tip api_btn_view" title="View Detail" data-placement="bottom" href="javascript:void(0);" onclick="
                                    var postData = {
                                        \'table_id\' : \''.$config_data_function['table_id'].'\',
                                        \'ajax_url\' : \''.base_url().'admin/restaurant/api_ajax_load_view_detail\',
                                    };
                                    api_ajax_load_view_detail(postData); 
                                ">
                                    <i class="fa fa-eye text-center" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a class="btn bblue pos-tip api_btn_payment" title="View Detail Payment" data-placement="bottom" href="javascript:void(0);" onclick="
                                var postData = {
                                    \'table_id\' : \''.$config_data_function['table_id'].'\',
                                    \'ajax_url\' : \''.base_url().'admin/restaurant/api_ajax_load_payment_detail\',
                                };
                                api_ajax_load_payment_detail(postData); 
                            "> 
                                    <i class="fa fa-money text-center" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a class="btn bblue pos-tip api_btn_menu" title="Menu" data-placement="bottom" href="javascript:void(0);" onclick="click_menu();"  data-original-title="Menu">
                                    <i class="fa fa-list" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a class="btn bdarkGreen pos-tip api_btn_table" title="View Table" data-placement="bottom" ​​​href="javascript:void(0);" onclick="
                                    var postData = {
                                        \'table_id\' : \''.$config_data_function['table_id'].'\', 
                                        \'ajax_url\' : \''.base_url().'admin/restaurant/api_ajax_load_table\',
                                    };
                                    api_ajax_load_table(postData); 
                                ">
                                    <i class="fa fa-table api_table" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a class="btn bpink pos-tip api_btn_dashboard" title="Dashboard" data-placement="bottom" href="'.base_url().'admin/welcome" data-original-title="Dashboard">
                                    <i class="fa fa-dashboard"></i>
                                </a>
                            </li>
                            <li class="dropdown">
                                    <a class="btn bred tip pos-tip api_btn_logout" title="LogOut" href="'.base_url().'admin/logout">
                                        <i class="fa fa-sign-out"></i>
                                    </a>
                            </li>
                        </ul>    
                    </div>    
                </td>
            </tr>
            </table>
        </div>
        <div class="api_clear_both"></div>
    ';

    $return['display_mobile'] = '
        <div class="api_header_kh">
            <table width="100%" border="0">
                <tr>
                    <td valign="middle" height="100">
                        <div class="top_header_menu_icon" >
                        
                                <a class="pull-right api_padding_right_5" href="'.base_url().'admin/logout">
                                    <div class="api_temp_icon_top_menu_default">
                                        <i class="fa fa-sign-out api_temp_sign_out"></i>  
                                    </div>     
                                </a>
    ';

    if ($config_data_function['clicked'] == 'view_all_table')
        $temp_1 = 'selected';
    else
        $temp_1 = ''; 
    $return['display_mobile'] .= '     
                                <a  title="View Table" data-placement="bottom" ​​​href="javascript:void(0);" onclick="
                                    var postData = {
                                    \'table_id\' : \''.$config_data_function['table_id'].'\', 
                                    \'ajax_url\' : \''.base_url().'admin/restaurant/api_ajax_load_table_mobile\',
                                    };
                                    api_ajax_load_table_mobile(postData); 
                                    ">
                                    <div class="api_temp_icon_top_menu_default api_bg_view_all_table '.$temp_1.' ">
                                    <i class="fa fa-table api_temp_table" aria-hidden="true"></i> 
                                    </div>
                                </a>
    ';
    if ($config_data_function['clicked'] == 'view_payment')
        $temp_2 = 'selected';
    else
        $temp_2 = ''; 
    $return['display_mobile'] .= '
                                <a class="" title="View Detail Payment" data-placement="bottom" href="javascript:void(0);" onclick="
                                    var postData = {
                                    \'table_id\' : \''.$config_data_function['table_id'].'\',
                                    \'ajax_url\' : \''.base_url().'admin/restaurant/api_ajax_load_payment_detail_mobile\',
                                    };
                                    api_ajax_load_payment_detail_mobile(postData); 
                                    "> 
                                    <div class="api_temp_icon_top_menu_default api_bg_view_payment '.$temp_2.'">
                                        <i class="fa fa-money text-center api_temp_money" aria-hidden="true"></i>
                                    </div>    
                                </a>
    ';
    if ($config_data_function['clicked'] == 'view_detail')
        $temp_3 = 'selected';
    else
        $temp_3 = '';   

    $return['display_mobile'] .= '
                                <a class="" title="View Detail" data-placement="bottom" href="javascript:void(0);"     onclick="
                                    var postData = {
                                        \'table_id\' : \''.$config_data_function['table_id'].'\',
                                        \'ajax_url\' : \''.base_url().'admin/restaurant/api_ajax_load_view_detail_mobile\',
                                    };
                                    api_ajax_load_view_detail_mobile(postData); 
                                    ">
                                    <div class="api_temp_icon_top_menu_default api_bg_view_detail '.$temp_3.'">
                                        <i class="fa fa-eye text-center api_temp_eye" aria-hidden="true"></i>
                                    </div>    
                                </a>
    '; 
    if ($config_data_function['clicked'] == 'view_list_table')
        $temp_4 = 'selected';
    else
        $temp_4 = '';   
        
    $return['display_mobile'] .= '  
                                <a class="" title="View Menu"data-placement="bottom" href="javascript:void(0);" onclick="
                                    var postData = {
                                        \'table_id\' : \''.$config_data_function['table_id'].'\',
                                        \'ajax_url\' : \''.base_url().'admin/restaurant/api_ajax_load_menu_mobile\',
                                    };
                                    api_ajax_load_menu_mobile(postData); 
                                    ">
                                    <div class="api_temp_icon_top_menu_default api_bg_view_list_table '.$temp_4.'">
                                            <i class="fa fa-file-text-o api_temp_file_text" aria-hidden="true"></i>
                                    </div>
                                </a>
                                    <div class="api_temp_icon_top_menu_default">
                                        <i class="fa fa-search api_temp_search" aria-hidden="true"></i> 
                                    </div> 
    ';
    $config_data = array(
        'table_name' => 'sma_restaurant',
        'select_table' => 'sma_restaurant',
        'translate' => '',
        'select_condition' => "id = ".$config_data_function['table_id'],
    );
    $temp_display_table = $this->site->api_select_data_v2($config_data);
    $return['display_mobile']   .= '
                                    <div class="api_top_table_name">
                                        Table  '.$temp_display_table[0]['table_name'].'
                                    </div>
                                    
                                    <a ​​​href="javascript:void(0);" onclick="
                                        var postData = {
                                        \'table_id\' : \''.$config_data_function['table_id'].'\', 
                                        \'ajax_url\' : \''.base_url().'admin/restaurant/api_ajax_load_table_mobile\',
                                        };
                                        api_ajax_load_table_mobile(postData); 
                                        ">
                                        <i class="fa fa-arrow-left api_temp_arrow" aria-hidden="true"></i>
                                    </a> 
                                    
                            <div class="api_clear_both"></div>   
                        </div>
                    </td>
                </tr>
            </table>
        </div> 
    ';

    return $return;
}
function display_payment_submit($config_data_function){
    $return = array();
    $return['display'] = ' 

                <table width="100%">
                    <tr>
                        <td valign="middle" class="api_temp_footer_v2">
                        <button class="btn btn-block btn-success api_temp_btn" id="submit-sale" >'.lang('submit').'</button>
                        </td>
                    
                </table>
    ';   
    return $return;
}
function display_footer($config_data_function){
    $return = array();
    $return['display'] = ' 
        <table width="100%">
            <tr>
                <td valign="middle" class="api_temp_footer_1">
                    <table>
                        <tr>
                            <td>1 items</td>
                        </tr>
                        <tr>
                            <td style="font-size: 20px">Total</td>
                            <td valign="middle" style="font-size: 20px">: $25</td>
                        </tr>
                    </table>
                </td>
            
                <td class="api_temp_footer_2" valign="middle">
                    <a href="javascript:void(0);" onclick="
                        var postData = {
                            \'table_id\' : \''.$config_data_function['table_id'].'\',
                            \'ajax_url\' : \''.base_url().'admin/restaurant/api_ajax_load_view_detail_mobile\',
                        };
                        api_ajax_load_view_detail_mobile(postData); 
                    " >                  
                    <table width="100%" border="0">
                    <tr>
                    <td valign="middle" align="center" height="70">
                        <div class="api_temp_height temp_icon">
                            <i class="fa fa-eye text-center" aria-hidden="true"></i>
                        </div>
                        <div class="padding05 text-center temp_title_icon">
                            View
                        </div>                        
                    </td>
                    </tr>
                    </table>
                    </a>
                    
                </td>
                
                <td class="api_temp_footer_3" valign="middle">
                    <a href="javascript:void(0);" onclick="
                        var postData = {
                            \'table_id\' : \''.$config_data_function['table_id'].'\',
                            \'ajax_url\' : \''.base_url().'admin/restaurant/api_ajax_load_payment_detail_mobile\',
                        };
                        api_ajax_load_payment_detail_mobile(postData); 
                    " >                   
                    <table width="100%" border="0">
                    <tr>
                    <td valign="middle" align="center" height="70">
                        <div class="api_temp_height temp_icon">
                            <i class="fa fa-money text-center" aria-hidden="true"></i>
                        </div>
                        <div class="padding05 text-center temp_title_icon">
                            Payment
                        </div>                        
                    </td>
                    </tr>
                    </table>
                    </a> 
                </td>
            </tr>
        </table>
    ';
    return $return;
}
}

