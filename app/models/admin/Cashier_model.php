<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Cashier_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

	public function get($config_data) {
        $condition = '';
        if ($config_data['search'] != '') {
            $condition .= "
                and (CONVERT(getTranslate(translate,'".$config_data['lg']."','".f_separate."','".v_separate."') USING utf8)) LIKE '%". $config_data['search'] ."%'
                OR getTranslate(add_ons,'email','".f_separate."','".v_separate."') LIKE '%". $config_data['search'] ."%'
                OR getTranslate(add_ons,'phone','".f_separate."','".v_separate."') LIKE '%". $config_data['search'] ."%'
                OR getTranslate(add_ons,'company','".f_separate."','".v_separate."') LIKE '%". $config_data['search'] ."%'
            ";
        }
        
        $order_by = '';
        if ($config_data['sort_by'] != '') {
            if ($config_data['sort_by'] == 'title')
                $order_by .= "order by getTranslate(translate,'en','".f_separate."','".v_separate."')";
            elseif (is_int(strpos($config_data['sort_by'],"add_ons_"))) {
                $temp_name = str_replace('add_ons_','',$config_data['sort_by']);
                $order_by .= "order by getTranslate(add_ons,'".$temp_name."','".f_separate."','".v_separate."')";
            }
            else
                $order_by .= 'order by '.$config_data['sort_by'];
                
            $order_by .= ' '.$config_data['sort_order'];                
        }
        else
            $order_by .= 'order by '.$config_data['table_id'].' Desc';

        if ($limit != '') {
            $this->db->limit($limit, $offset);
        }

        if ($config_data['limit'] != '')
            $order_by .= ' limit '.$config_data['offset_no'].', '.$config_data['limit'];

        if ($config_data['selected_id'] > 0)
            $condition = ' and '.$config_data['table_id']." = ".$config_data['selected_id'];

        $temp_field = $this->site->api_get_condition_add_ons_field("add_ons","sma_add_ons","table_name = '".$config_data['table_name']."'");
        $select_data = $this->site->api_select_some_fields_with_where("*,
            getTranslate(translate,'en','".f_separate."','".v_separate."') as title 
            ".$temp_field
            ,$config_data['table_name']
            ,$config_data['table_id']." > 0 ".$condition." ".$order_by
            ,"arr"
        );  

        return $select_data;
    }
    public function insert($data = array())
    {
        if ($this->db->insert('sma_cashier', $data)) {
            return true;
        }
        return false;
    }	
}
