<?php defined('BASEPATH') or exit('No direct script access allowed');

class Site extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_total_qty_alerts()
    {
        $this->db->where('quantity < alert_quantity', null, false)->where('track_quantity', 1);
        return $this->db->count_all_results('products');
    }

    public function get_expiring_qty_alerts()
    {
        $date = date('Y-m-d', strtotime('+3 months'));
        $this->db->select('SUM(quantity_balance) as alert_num')
        ->where('expiry !=', null)->where('expiry !=', '0000-00-00')
        ->where('expiry <', $date);
        $q = $this->db->get('purchase_items');
        if ($q != false && $q->num_rows() > 0) {
            $res = $q->row();
            return (INT) $res->alert_num;
        }
        return false;
    }

    public function get_shop_sale_alerts()
    {
        $this->db->join('deliveries', 'deliveries.sale_id=sales.id', 'left')
        ->where('sales.shop', 1)->where('sales.sale_status', 'completed')->where('sales.payment_status', 'paid')
        ->group_start()->where('deliveries.status !=', 'delivered')->or_where('deliveries.status IS NULL', null)->group_end();
        return $this->db->count_all_results('sales');
    }

    public function get_shop_payment_alerts()
    {
        $this->db->where('shop', 1)->where('attachment !=', null)->where('payment_status !=', 'paid');
        return $this->db->count_all_results('sales');
    }

    public function get_setting()
    {
        $q = $this->db->get('settings');
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }
    public function getDateFormat($id)
    {
        $q = $this->db->get_where('date_format', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getAllCompanies($group_name)
    {
        $q = $this->db->get_where('companies', array('group_name' => $group_name));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
    public function getCustomers()
    {
        $this->db->from('companies');
        $this->db->order_by("name", "asc");
        $q = $this->db->get();

        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
    
    public function getCompanyByID($id)
    {
        $q = $this->db->get_where('companies', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
       
        return false;
    }
    public function getCompanyByID_v2($id)
    {
        if ($id != '') {
            $temp_field = $this->api_get_condition_add_ons_field("add_ons", "sma_add_ons", "table_name = 'sma_companies'");
            $temp = $this->api_select_some_fields_with_where(
                "*
                ".$temp_field,
                "sma_companies",
                "id = ".$id,
                "arr"
            );
            $q = $this->db->get_where('sma_companies', array('id' => $id), 1);
            $temp2 = $q->row();
            foreach ($temp[0] as $key => $value) {
                $temp2->{$key} = $value;
            }
            return $temp2;
        } else {
            return false;
        }
    }

    public function getCustomerGroupByID($id)
    {
        $q = $this->db->get_where('customer_groups', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    //get customer product discount by product id and customer id
    public function getCPDiscountByIDCID($pid, $cid)
    {
        $date = date('Y-m-d');
        $q = $this->db->get_where(
            'product_discount',
            array(
                            'product_id' => $pid,
                            'customer_id' => $cid,
                            'start_date <=' => $date,
                            'end_date >=' => $date
                        ),
            1
        );
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getUser($id = null)
    {
        if (!$id) {
            $id = $this->session->userdata('user_id');
        }
        $q = $this->db->get_where('users', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getProductByID($id)
    {
        $q = $this->db->get_where('products', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getProductByID_v2($id)
    {
        if ($id != '') {
            $temp_field = $this->api_get_condition_add_ons_field("add_ons", "sma_add_ons", "table_name = 'sma_products'");
            $temp = $this->api_select_some_fields_with_where(
                "*
                ".$temp_field,
                "sma_products",
                "id = ".$id,
                "arr"
            );
            $q = $this->db->get_where('sma_products', array('id' => $id), 1);
            $temp2 = $q->row();
            foreach ($temp[0] as $key => $value) {
                $temp2->{$key} = $value;
            }
            return $temp2;
        } else {
            return false;
        }
    }

    public function getAllCurrencies()
    {
        $q = $this->db->get('currencies');
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getCurrencyByCode($code)
    {
        $q = $this->db->get_where('currencies', array('code' => $code), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getAllTaxRates()
    {
        $q = $this->db->get('tax_rates');
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getTaxRateByID($id)
    {
        $q = $this->db->get_where('tax_rates', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getAllWarehouses()
    {
        $q = $this->db->get('warehouses');
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getWarehouseByID($id)
    {
        $q = $this->db->get_where('warehouses', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }


    public function getCategoryByID($id)
    {
        $q = $this->db->get_where('categories', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getGiftCardByID($id)
    {
        $q = $this->db->get_where('gift_cards', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getGiftCardByNO($no)
    {
        $q = $this->db->get_where('gift_cards', array('card_no' => $no), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function updateInvoiceStatus()
    {
        $date = date('Y-m-d');
        $q = $this->db->get_where('invoices', array('status' => 'unpaid'));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                if ($row->due_date < $date) {
                    $this->db->update('invoices', array('status' => 'due'), array('id' => $row->id));
                }
            }
            $this->db->update('settings', array('update' => $date), array('setting_id' => '1'));
            return true;
        }
    }

    public function modal_js()
    {
        $auth = base64_encode("camboinfo:32s#12dsd3");
        $context = stream_context_create([
            "http" => [
                "header" => "Authorization: Basic $auth"
            ]
        ]);
        return '<script type="text/javascript">' . file_get_contents($this->data['assets'] . 'js/modal.js', false, $context) . '</script>';
    }

    public function getReference($field)
    {
        $q = $this->db->get_where('order_ref', array('ref_id' => '1'), 1);
        if ($q != false && $q->num_rows() > 0) {
            $ref = $q->row();
            switch ($field) {
                case 'so':
                    $prefix = $this->Settings->sales_prefix;
                    break;
                case 'pos':
                    $prefix = isset($this->Settings->sales_prefix) ? $this->Settings->sales_prefix . '/POS' : '';
                    break;
                case 'qu':
                    $prefix = $this->Settings->quote_prefix;
                    break;
                case 'po':
                    $prefix = $this->Settings->purchase_prefix;
                    break;
                case 'to':
                    $prefix = $this->Settings->transfer_prefix;
                    break;
                case 'do':
                    $prefix = $this->Settings->delivery_prefix;
                    break;
                case 'pay':
                    $prefix = $this->Settings->payment_prefix;
                    break;
                case 'ppay':
                    $prefix = $this->Settings->ppayment_prefix;
                    break;
                case 'ex':
                    $prefix = $this->Settings->expense_prefix;
                    break;
                case 're':
                    $prefix = $this->Settings->return_prefix;
                    break;
                case 'rep':
                    $prefix = $this->Settings->returnp_prefix;
                    break;
                case 'qa':
                    $prefix = $this->Settings->qa_prefix;
                    break;
                default:
                    $prefix = '';
            }

            $ref_no = $prefix;

            if ($this->Settings->reference_format == 1) {
                $ref_no .= date('Y') . "/" . sprintf("%04s", $ref->{$field});
            } elseif ($this->Settings->reference_format == 2) {
                $ref_no .= date('Y') . "/" . date('m') . "/" . sprintf("%04s", $ref->{$field});
            } elseif ($this->Settings->reference_format == 3) {
                $ref_no .= sprintf("%04s", $ref->{$field});
            } elseif ($this->Settings->reference_format == 5) {
                $ref_no = (!empty($prefix)) ? $prefix . '-' : '';
                $ref_no .= date('Y') . ":" . sprintf("%06s", $ref->{$field});
            } else {
                $ref_no .= $this->getRandomReference();
            }

            return $ref_no;
        }
        return false;
    }
    /* 18-01-2019 */
    public function getSaleReference($is_vat = null, $is_field = false)
    {
        $q = $this->db->get_where('order_ref', array('ref_id' => '1'), 1);
        $ref_no = '';
        if ($q != false && $q->num_rows() > 0) {
            $ref = $q->row();
            if ($is_vat) {
                $field = 'so';
                $ref_no = "SL". "-" . date('Y') . ":" . sprintf("%06s", $ref->{$field});
            } else {
                $field = 'do';
                $ref_no = "DO". "-" . date('Y') . ":" . sprintf("%06s", $ref->{$field});
            }

            if ($is_field === true) {
                return $field;
            }

            return $ref_no;
        }
    }
    public function getSaleReference_v2($customer)
    {
        $q = $this->db->get_where('order_ref', array('ref_id' => '1'), 1);
        $ref_no = '';
        if ($q != false && $q->num_rows() > 0) {
            $ref = $q->row();
            if ($customer[0]['vat_no'] != '' && $customer[0]['vat_invoice_type'] != 'do') {
                $field = 'so';
                $ref_no = "SL". "-" . date('Y') . ":" . sprintf("%06s", $ref->{$field});
            } else {
                $field = 'do';
                $ref_no = "DO". "-" . date('Y') . ":" . sprintf("%06s", $ref->{$field});
            }
            $return['reference_no'] = $ref_no;
            $return['field'] = $field;

            return $return;
        }
    }

    public function getRandomReference($len = 12)
    {
        $result = '';
        for ($i = 0; $i < $len; $i++) {
            $result .= mt_rand(0, 9);
        }

        if ($this->getSaleByReference($result)) {
            $this->getRandomReference();
        }

        return $result;
    }

    public function getSaleByReference($ref)
    {
        $this->db->like('reference_no', $ref, 'both');
        $q = $this->db->get('sales', 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function updateReference($field)
    {
        $q = $this->db->get_where('order_ref', array('ref_id' => '1'), 1);
        if ($q != false && $q->num_rows() > 0) {
            $ref = $q->row();
            $this->db->update('order_ref', array($field => $ref->{$field} + 1), array('ref_id' => '1'));
            return true;
        }
        return false;
    }

    public function checkPermissions()
    {
        $q = $this->db->get_where('permissions', array('group_id' => $this->session->userdata('group_id')), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->result_array();
        }
        return false;
    }

    public function getNotifications()
    {
        $date = date('Y-m-d H:i:s', time());
        $this->db->where("from_date <=", $date);
        $this->db->where("till_date >=", $date);
        if (!$this->Owner) {
            if ($this->Supplier) {
                $this->db->where('scope', 4);
            } elseif ($this->Customer) {
                $this->db->where('scope', 1)->or_where('scope', 3);
            } elseif (!$this->Customer && !$this->Supplier) {
                $this->db->where('scope', 2)->or_where('scope', 3);
            }
        }
        $q = $this->db->get("notifications");
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getUpcomingEvents()
    {
        $dt = date('Y-m-d');
        $this->db->where('start >=', $dt)->order_by('start')->limit(5);
        if ($this->Settings->restrict_calendar) {
            $this->db->where('user_id', $this->session->userdata('user_id'));
        }

        $q = $this->db->get('calendar');

        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getUserGroup($user_id = false)
    {
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $group_id = $this->getUserGroupID($user_id);
        $q = $this->db->get_where('groups', array('id' => $group_id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getUserGroupID($user_id = false)
    {
        $user = $this->getUser($user_id);
        return $user->group_id;
    }

    public function getWarehouseProductsVariants($option_id, $warehouse_id = null)
    {
        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }
        $q = $this->db->get_where('warehouses_products_variants', array('option_id' => $option_id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getPurchasedItem($clause)
    {
        $orderby = empty($this->Settings->accounting_method) ? 'asc' : 'desc';
        $this->db->order_by('date', $orderby);
        $this->db->order_by('purchase_id', $orderby);
        if (!isset($clause['option_id']) || empty($clause['option_id'])) {
            $this->db->group_start()->where('option_id', null)->or_where('option_id', 0)->group_end();
        }
        $q = $this->db->get_where('purchase_items', $clause);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function setPurchaseItem($clause, $qty)
    {
        if ($product = $this->getProductByID($clause['product_id'])) {
            if ($pi = $this->getPurchasedItem($clause)) {
                $quantity_balance = $pi->quantity_balance+$qty;
                return $this->db->update('purchase_items', array('quantity_balance' => $quantity_balance), array('id' => $pi->id));
            } else {
                $unit = $this->getUnitByID($product->unit);
                $clause['product_unit_id'] = $product->unit;
                $clause['product_unit_code'] = $unit->code;
                $clause['product_code'] = $product->code;
                $clause['product_name'] = $product->name;
                $clause['purchase_id'] = $clause['transfer_id'] = $clause['item_tax'] = null;
                $clause['net_unit_cost'] = $clause['real_unit_cost'] = $clause['unit_cost'] = $product->cost;
                $clause['quantity_balance'] = $clause['quantity'] = $clause['unit_quantity'] = $clause['quantity_received'] = $qty;
                $clause['subtotal'] = ($product->cost * $qty);
                if (isset($product->tax_rate) && $product->tax_rate != 0) {
                    $tax_details = $this->site->getTaxRateByID($product->tax_rate);
                    $ctax = $this->calculateTax($product, $tax_details, $product->cost);
                    $item_tax = $clause['item_tax'] = $ctax['amount'];
                    $tax = $clause['tax'] = $ctax['tax'];
                    $clause['tax_rate_id'] = $tax_details->id;
                    if ($product->tax_method != 1) {
                        $clause['net_unit_cost'] = $product->cost - $item_tax;
                        $clause['unit_cost'] = $product->cost;
                    } else {
                        $clause['net_unit_cost'] = $product->cost;
                        $clause['unit_cost'] = $product->cost + $item_tax;
                    }
                    $pr_item_tax = $this->sma->formatDecimal($item_tax * $clause['unit_quantity'], 4);
                    if ($this->Settings->indian_gst && $gst_data = $this->gst->calculteIndianGST($pr_item_tax, ($this->Settings->state == $supplier_details->state), $tax_details)) {
                        $clause['gst'] = $gst_data['gst'];
                        $clause['cgst'] = $gst_data['cgst'];
                        $clause['sgst'] = $gst_data['sgst'];
                        $clause['igst'] = $gst_data['igst'];
                    }
                    $clause['subtotal'] = (($clause['net_unit_cost'] * $clause['unit_quantity']) + $pr_item_tax);
                }
                $clause['status'] = 'received';
                $clause['date'] = date('Y-m-d');
                $clause['option_id'] = !empty($clause['option_id']) && is_numeric($clause['option_id']) ? $clause['option_id'] : null;
                return $this->db->insert('purchase_items', $clause);
            }
        }
        return false;
    }

    public function syncVariantQty($variant_id, $warehouse_id, $product_id = null)
    {
        $balance_qty = $this->getBalanceVariantQuantity($variant_id);
        $wh_balance_qty = $this->getBalanceVariantQuantity($variant_id, $warehouse_id);
        if ($this->db->update('product_variants', array('quantity' => $balance_qty), array('id' => $variant_id))) {
            if ($this->getWarehouseProductsVariants($variant_id, $warehouse_id)) {
                $this->db->update('warehouses_products_variants', array('quantity' => $wh_balance_qty), array('option_id' => $variant_id, 'warehouse_id' => $warehouse_id));
            } else {
                if ($wh_balance_qty) {
                    $this->db->insert('warehouses_products_variants', array('quantity' => $wh_balance_qty, 'option_id' => $variant_id, 'warehouse_id' => $warehouse_id, 'product_id' => $product_id));
                }
            }
            return true;
        }
        return false;
    }

    public function getWarehouseProducts($product_id, $warehouse_id = null)
    {
        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }
        $q = $this->db->get_where('warehouses_products', array('product_id' => $product_id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function syncProductQty($product_id, $warehouse_id)
    {
        $balance_qty = $this->getBalanceQuantity($product_id);
        $wh_balance_qty = $this->getBalanceQuantity($product_id, $warehouse_id);
        if ($this->db->update('products', array('quantity' => $balance_qty), array('id' => $product_id))) {
            if ($this->getWarehouseProducts($product_id, $warehouse_id)) {
                $this->db->update('warehouses_products', array('quantity' => $wh_balance_qty), array('product_id' => $product_id, 'warehouse_id' => $warehouse_id));
            } else {
                if (! $wh_balance_qty) {
                    $wh_balance_qty = 0;
                }
                $product = $this->site->getProductByID($product_id);
                $this->db->insert('warehouses_products', array('quantity' => $wh_balance_qty, 'product_id' => $product_id, 'warehouse_id' => $warehouse_id, 'avg_cost' => $product->cost));
            }
            return true;
        }
        return false;
    }

    public function getSaleByID($id)
    {
        $q = $this->db->get_where('sales', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getSalePayments($sale_id)
    {
        $q = $this->db->get_where('payments', array('sale_id' => $sale_id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function syncSalePayments($id)
    {
        $sale = $this->getSaleByID($id);
        if ($payments = $this->getSalePayments($id)) {
            $paid = 0;
            $grand_total = $sale->grand_total+$sale->rounding;
            foreach ($payments as $payment) {
                $paid += $payment->amount;
            }

            $payment_status = $paid == 0 ? 'pending' : $sale->payment_status;
            if ($this->sma->formatDecimal($grand_total) == $this->sma->formatDecimal($paid)) {
                $payment_status = 'paid';
            } elseif ($sale->due_date <= date('Y-m-d') && !$sale->sale_id) {
                if ($sale->payment_status != 'partial') {
                    $payment_status = 'due';
                }
            } elseif ($paid != 0) {
                $payment_status = 'partial';
            }

            if ($this->db->update('sales', array('paid' => $paid, 'payment_status' => $payment_status), array('id' => $id))) {
                return true;
            }
        } else {
            if ($sale->payment_status != 'partial') {
                $payment_status = ($sale->due_date <= date('Y-m-d')) ? 'due' : 'pending';
            } else {
                $payment_status = $sale->payment_status;
            }
            if ($this->db->update('sales', array('paid' => 0, 'payment_status' => $payment_status), array('id' => $id))) {
                return true;
            }
        }

        return false;
    }

    public function getPurchaseByID($id)
    {
        $q = $this->db->get_where('purchases', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getPurchasePayments($purchase_id)
    {
        $q = $this->db->get_where('payments', array('purchase_id' => $purchase_id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function syncPurchasePayments($id)
    {
        $purchase = $this->getPurchaseByID($id);
        $paid = 0;
        if ($payments = $this->getPurchasePayments($id)) {
            foreach ($payments as $payment) {
                $paid += $payment->amount;
            }
        }

        $payment_status = $paid <= 0 ? 'pending' : $purchase->payment_status;
        if ($this->sma->formatDecimal($purchase->grand_total) > $this->sma->formatDecimal($paid) && $paid > 0) {
            $payment_status = 'partial';
        } elseif ($this->sma->formatDecimal($purchase->grand_total) <= $this->sma->formatDecimal($paid)) {
            $payment_status = 'paid';
        }

        if ($this->db->update('purchases', array('paid' => $paid), array('id' => $id))) {
            return true;
        }

        return false;
    }

    private function getBalanceQuantity($product_id, $warehouse_id = null)
    {
        $this->db->select('SUM(COALESCE(quantity_balance, 0)) as stock', false);
        $this->db->where('product_id', $product_id)->where('quantity_balance !=', 0);
        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }
        $this->db->group_start()->where('status', 'received')->or_where('status', 'partial')->group_end();
        $q = $this->db->get('purchase_items');
        if ($q != false && $q->num_rows() > 0) {
            $data = $q->row();
            return $data->stock;
        }
        return 0;
    }

    private function getBalanceVariantQuantity($variant_id, $warehouse_id = null)
    {
        $this->db->select('SUM(COALESCE(quantity_balance, 0)) as stock', false);
        $this->db->where('option_id', $variant_id)->where('quantity_balance !=', 0);
        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }
        $this->db->group_start()->where('status', 'received')->or_where('status', 'partial')->group_end();
        $q = $this->db->get('purchase_items');
        if ($q != false && $q->num_rows() > 0) {
            $data = $q->row();
            return $data->stock;
        }
        return 0;
    }

    public function calculateAVCost($product_id, $warehouse_id, $net_unit_price, $unit_price, $quantity, $product_name, $option_id, $item_quantity)
    {
        $product = $this->getProductByID($product_id);
        $real_item_qty = $quantity;
        $wp_details = $this->getWarehouseProduct($warehouse_id, $product_id);
        $con = $wp_details ? $wp_details->avg_cost : $product->cost;
        $tax_rate = $this->getTaxRateByID($product->tax_rate);
        $ctax = $this->calculateTax($product, $tax_rate, $con);
        if ($product->tax_method) {
            $avg_net_unit_cost = $con;
            $avg_unit_cost = ($con + $ctax['amount']);
        } else {
            $avg_unit_cost = $con;
            $avg_net_unit_cost = ($con - $ctax['amount']);
        }

        if ($pis = $this->getPurchasedItems($product_id, $warehouse_id, $option_id)) {
            $cost_row = array();
            $quantity = $item_quantity;
            $balance_qty = $quantity;
            foreach ($pis as $pi) {
                if (!empty($pi) && $pi->quantity > 0 && $balance_qty <= $quantity && $quantity != 0) {
                    if ($pi->quantity_balance >= $quantity && $quantity != 0) {
                        $balance_qty = $pi->quantity_balance - $quantity;
                        $cost_row = array('date' => date('Y-m-d'), 'product_id' => $product_id, 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => $pi->id, 'quantity' => $quantity, 'purchase_net_unit_cost' => $avg_net_unit_cost, 'purchase_unit_cost' => $avg_unit_cost, 'sale_net_unit_price' => $net_unit_price, 'sale_unit_price' => $unit_price, 'quantity_balance' => $balance_qty, 'inventory' => 1, 'option_id' => $option_id);
                        $quantity = 0;
                    } elseif ($quantity != 0) {
                        $quantity = $quantity - $pi->quantity_balance;
                        $balance_qty = $quantity;
                        $cost_row = array('date' => date('Y-m-d'), 'product_id' => $product_id, 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => $pi->id, 'quantity' => $pi->quantity_balance, 'purchase_net_unit_cost' => $avg_net_unit_cost, 'purchase_unit_cost' => $avg_unit_cost, 'sale_net_unit_price' => $net_unit_price, 'sale_unit_price' => $unit_price, 'quantity_balance' => 0, 'inventory' => 1, 'option_id' => $option_id);
                    }
                }
                if (empty($cost_row)) {
                    break;
                }
                $cost[] = $cost_row;
                if ($quantity == 0) {
                    break;
                }
            }
        }
        if ($quantity > 0 && !$this->Settings->overselling) {
            $this->session->set_flashdata('error', sprintf(lang("quantity_out_of_stock_for_%s"), ($pi->product_name ? $pi->product_name : $product_name)));
            redirect($_SERVER["HTTP_REFERER"]);
        } elseif ($quantity != 0) {
            $cost[] = array('date' => date('Y-m-d'), 'product_id' => $product_id, 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => null, 'quantity' => $real_item_qty, 'purchase_net_unit_cost' => $avg_net_unit_cost, 'purchase_unit_cost' => $avg_unit_cost, 'sale_net_unit_price' => $net_unit_price, 'sale_unit_price' => $unit_price, 'quantity_balance' => null, 'overselling' => 1, 'inventory' => 1);
            $cost[] = array('pi_overselling' => 1, 'product_id' => $product_id, 'quantity_balance' => (0 - $quantity), 'warehouse_id' => $warehouse_id, 'option_id' => $option_id);
        }
        return $cost;
    }

    public function calculateCost($product_id, $warehouse_id, $net_unit_price, $unit_price, $quantity, $product_name, $option_id, $item_quantity)
    {
        $pis = $this->getPurchasedItems($product_id, $warehouse_id, $option_id);
        $real_item_qty = $quantity;
        $quantity = $item_quantity;
        $balance_qty = $quantity;
        foreach ($pis as $pi) {
            $cost_row = null;
            if (!empty($pi) && $balance_qty <= $quantity && $quantity != 0) {
                $purchase_unit_cost = $pi->unit_cost ? $pi->unit_cost : ($pi->net_unit_cost + ($pi->item_tax / $pi->quantity));
                if ($pi->quantity_balance >= $quantity && $quantity != 0) {
                    $balance_qty = $pi->quantity_balance - $quantity;
                    $cost_row = array('date' => date('Y-m-d'), 'product_id' => $product_id, 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => $pi->id, 'quantity' => $quantity, 'purchase_net_unit_cost' => $pi->net_unit_cost, 'purchase_unit_cost' => $purchase_unit_cost, 'sale_net_unit_price' => $net_unit_price, 'sale_unit_price' => $unit_price, 'quantity_balance' => $balance_qty, 'inventory' => 1, 'option_id' => $option_id);
                    $quantity = 0;
                } elseif ($quantity != 0) {
                    $quantity = $quantity - $pi->quantity_balance;
                    $balance_qty = $quantity;
                    $cost_row = array('date' => date('Y-m-d'), 'product_id' => $product_id, 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => $pi->id, 'quantity' => $pi->quantity_balance, 'purchase_net_unit_cost' => $pi->net_unit_cost, 'purchase_unit_cost' => $purchase_unit_cost, 'sale_net_unit_price' => $net_unit_price, 'sale_unit_price' => $unit_price, 'quantity_balance' => 0, 'inventory' => 1, 'option_id' => $option_id);
                }
            }
            $cost[] = $cost_row;
            if ($quantity == 0) {
                break;
            }
        }
        if ($quantity > 0) {
            $this->session->set_flashdata('error', sprintf(lang("quantity_out_of_stock_for_%s"), (isset($pi->product_name) ? $pi->product_name : $product_name)));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        return $cost;
    }

    public function getPurchasedItems($product_id, $warehouse_id, $option_id = null)
    {
        $orderby = empty($this->Settings->accounting_method) ? 'asc' : 'desc';
        $this->db->select('id, quantity, quantity_balance, net_unit_cost, unit_cost, item_tax');
        $this->db->where('product_id', $product_id)->where('warehouse_id', $warehouse_id)->where('quantity_balance !=', 0);
        if (!isset($option_id) || empty($option_id)) {
            $this->db->group_start()->where('option_id', null)->or_where('option_id', 0)->group_end();
        } else {
            $this->db->where('option_id', $option_id);
        }
        $this->db->group_start()->where('status', 'received')->or_where('status', 'partial')->group_end();
        $this->db->group_by('id');
        $this->db->order_by('date', $orderby);
        $this->db->order_by('purchase_id', $orderby);
        $q = $this->db->get('purchase_items');
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getProductComboItems($pid, $warehouse_id = null)
    {
        $this->db->select('products.id as id, combo_items.item_code as code, combo_items.quantity as qty, products.name as name, products.type as type, combo_items.unit_price as unit_price, warehouses_products.quantity as quantity')
            ->join('products', 'products.code=combo_items.item_code', 'left')
            ->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
            ->group_by('combo_items.id');
        if ($warehouse_id) {
            $this->db->where('warehouses_products.warehouse_id', $warehouse_id);
        }
        $q = $this->db->get_where('combo_items', array('combo_items.product_id' => $pid));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
        return false;
    }

    public function item_costing($item, $pi = null)
    {
        $item_quantity = $pi ? $item['aquantity'] : $item['quantity'];
        if (!isset($item['option_id']) || empty($item['option_id']) || $item['option_id'] == 'null') {
            $item['option_id'] = null;
        }

        if ($this->Settings->accounting_method != 2 && !$this->Settings->overselling) {
            if ($this->getProductByID($item['product_id'])) {
                if ($item['product_type'] == 'standard') {
                    $unit = $this->getUnitByID($item['product_unit_id']);
                    $item['net_unit_price'] = $this->convertToBase($unit, $item['net_unit_price']);
                    $item['unit_price'] = $this->convertToBase($unit, $item['unit_price']);
                    $cost = $this->calculateCost($item['product_id'], $item['warehouse_id'], $item['net_unit_price'], $item['unit_price'], $item['quantity'], $item['product_name'], $item['option_id'], $item_quantity);
                } elseif ($item['product_type'] == 'combo') {
                    $combo_items = $this->getProductComboItems($item['product_id'], $item['warehouse_id']);
                    foreach ($combo_items as $combo_item) {
                        $pr = $this->getProductByCode($combo_item->code);
                        if ($pr->tax_rate) {
                            $pr_tax = $this->getTaxRateByID($pr->tax_rate);
                            if ($pr->tax_method) {
                                $item_tax = $this->sma->formatDecimal((($combo_item->unit_price) * $pr_tax->rate) / (100 + $pr_tax->rate));
                                $net_unit_price = $combo_item->unit_price - $item_tax;
                                $unit_price = $combo_item->unit_price;
                            } else {
                                $item_tax = $this->sma->formatDecimal((($combo_item->unit_price) * $pr_tax->rate) / 100);
                                $net_unit_price = $combo_item->unit_price;
                                $unit_price = $combo_item->unit_price + $item_tax;
                            }
                        } else {
                            $net_unit_price = $combo_item->unit_price;
                            $unit_price = $combo_item->unit_price;
                        }
                        if ($pr->type == 'standard') {
                            $cost[] = $this->calculateCost($pr->id, $item['warehouse_id'], $net_unit_price, $unit_price, ($combo_item->qty * $item['quantity']), $pr->name, null, $item_quantity);
                        } else {
                            $cost[] = array(array('date' => date('Y-m-d'), 'product_id' => $pr->id, 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => null, 'quantity' => ($combo_item->qty * $item['quantity']), 'purchase_net_unit_cost' => 0, 'purchase_unit_cost' => 0, 'sale_net_unit_price' => $combo_item->unit_price, 'sale_unit_price' => $combo_item->unit_price, 'quantity_balance' => null, 'inventory' => null));
                        }
                    }
                } else {
                    $cost = array(array('date' => date('Y-m-d'), 'product_id' => $item['product_id'], 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => null, 'quantity' => $item['quantity'], 'purchase_net_unit_cost' => 0, 'purchase_unit_cost' => 0, 'sale_net_unit_price' => $item['net_unit_price'], 'sale_unit_price' => $item['unit_price'], 'quantity_balance' => null, 'inventory' => null));
                }
            } elseif ($item['product_type'] == 'manual') {
                $cost = array(array('date' => date('Y-m-d'), 'product_id' => $item['product_id'], 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => null, 'quantity' => $item['quantity'], 'purchase_net_unit_cost' => 0, 'purchase_unit_cost' => 0, 'sale_net_unit_price' => $item['net_unit_price'], 'sale_unit_price' => $item['unit_price'], 'quantity_balance' => null, 'inventory' => null));
            }
        } else {
            if ($this->getProductByID($item['product_id'])) {
                if ($item['product_type'] == 'standard') {
                    $cost = $this->calculateAVCost($item['product_id'], $item['warehouse_id'], $item['net_unit_price'], $item['unit_price'], $item['quantity'], $item['product_name'], $item['option_id'], $item_quantity);
                } elseif ($item['product_type'] == 'combo') {
                    $combo_items = $this->getProductComboItems($item['product_id'], $item['warehouse_id']);
                    foreach ($combo_items as $combo_item) {
                        $pr = $this->getProductByCode($combo_item->code);
                        if ($pr->tax_rate) {
                            $pr_tax = $this->getTaxRateByID($pr->tax_rate);
                            if ($pr->tax_method) {
                                $item_tax = $this->sma->formatDecimal((($combo_item->unit_price) * $pr_tax->rate) / (100 + $pr_tax->rate));
                                $net_unit_price = $combo_item->unit_price - $item_tax;
                                $unit_price = $combo_item->unit_price;
                            } else {
                                $item_tax = $this->sma->formatDecimal((($combo_item->unit_price) * $pr_tax->rate) / 100);
                                $net_unit_price = $combo_item->unit_price;
                                $unit_price = $combo_item->unit_price + $item_tax;
                            }
                        } else {
                            $net_unit_price = $combo_item->unit_price;
                            $unit_price = $combo_item->unit_price;
                        }
                        $cost[] = $this->calculateAVCost($combo_item->id, $item['warehouse_id'], $net_unit_price, $unit_price, ($combo_item->qty * $item['quantity']), $item['product_name'], $item['option_id'], $item_quantity);
                    }
                } else {
                    $cost = array(array('date' => date('Y-m-d'), 'product_id' => $item['product_id'], 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => null, 'quantity' => $item['quantity'], 'purchase_net_unit_cost' => 0, 'purchase_unit_cost' => 0, 'sale_net_unit_price' => $item['net_unit_price'], 'sale_unit_price' => $item['unit_price'], 'quantity_balance' => null, 'inventory' => null));
                }
            } elseif ($item['product_type'] == 'manual') {
                $cost = array(array('date' => date('Y-m-d'), 'product_id' => $item['product_id'], 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => null, 'quantity' => $item['quantity'], 'purchase_net_unit_cost' => 0, 'purchase_unit_cost' => 0, 'sale_net_unit_price' => $item['net_unit_price'], 'sale_unit_price' => $item['unit_price'], 'quantity_balance' => null, 'inventory' => null));
            }
        }
        return $cost;
    }

    public function costing($items)
    {
        $citems = array();
        foreach ($items as $item) {
            $option = (isset($item['option_id']) && !empty($item['option_id']) && $item['option_id'] != 'null' && $item['option_id'] != 'false') ? $item['option_id'] : '';
            $pr = $this->getProductByID($item['product_id']);
            $item['option_id'] = $option;
            if ($pr && $pr->type == 'standard') {
                if (isset($citems['p' . $item['product_id'] . 'o' . $item['option_id']])) {
                    $citems['p' . $item['product_id'] . 'o' . $item['option_id']]['aquantity'] += $item['quantity'];
                } else {
                    $citems['p' . $item['product_id'] . 'o' . $item['option_id']] = $item;
                    $citems['p' . $item['product_id'] . 'o' . $item['option_id']]['aquantity'] = $item['quantity'];
                }
            } elseif ($pr && $pr->type == 'combo') {
                $wh = $this->Settings->overselling ? null : $item['warehouse_id'];
                $combo_items = $this->getProductComboItems($item['product_id'], $wh);
                foreach ($combo_items as $combo_item) {
                    if ($combo_item->type == 'standard') {
                        if (isset($citems['p' . $combo_item->id . 'o' . $item['option_id']])) {
                            $citems['p' . $combo_item->id . 'o' . $item['option_id']]['aquantity'] += ($combo_item->qty*$item['quantity']);
                        } else {
                            $cpr = $this->getProductByID($combo_item->id);
                            if ($cpr->tax_rate) {
                                $cpr_tax = $this->getTaxRateByID($cpr->tax_rate);
                                if ($cpr->tax_method) {
                                    $item_tax = $this->sma->formatDecimal((($combo_item->unit_price) * $cpr_tax->rate) / (100 + $cpr_tax->rate));
                                    $net_unit_price = $combo_item->unit_price - $item_tax;
                                    $unit_price = $combo_item->unit_price;
                                } else {
                                    $item_tax = $this->sma->formatDecimal((($combo_item->unit_price) * $cpr_tax->rate) / 100);
                                    $net_unit_price = $combo_item->unit_price;
                                    $unit_price = $combo_item->unit_price + $item_tax;
                                }
                            } else {
                                $net_unit_price = $combo_item->unit_price;
                                $unit_price = $combo_item->unit_price;
                            }
                            $cproduct = array('product_id' => $combo_item->id, 'product_name' => $cpr->name, 'product_type' => $combo_item->type, 'quantity' => ($combo_item->qty*$item['quantity']), 'net_unit_price' => $net_unit_price, 'unit_price' => $unit_price, 'warehouse_id' => $item['warehouse_id'], 'item_tax' => $item_tax, 'tax_rate_id' => $cpr->tax_rate, 'tax' => ($cpr_tax->type == 1 ? $cpr_tax->rate.'%' : $cpr_tax->rate), 'option_id' => null, 'product_unit_id' => $cpr->unit);
                            $citems['p' . $combo_item->id . 'o' . $item['option_id']] = $cproduct;
                            $citems['p' . $combo_item->id . 'o' . $item['option_id']]['aquantity'] = ($combo_item->qty*$item['quantity']);
                        }
                    }
                }
            }
        }
        // $this->sma->print_arrays($combo_items, $citems);
        $cost = array();
        foreach ($citems as $item) {
            $item['aquantity'] = $citems['p' . $item['product_id'] . 'o' . $item['option_id']]['aquantity'];
            $cost[] = $this->item_costing($item, true);
        }
        return $cost;
    }

    public function syncQuantity($sale_id = null, $purchase_id = null, $oitems = null, $product_id = null)
    {
        if ($sale_id) {
            $sale_items = $this->getAllSaleItems($sale_id);
            foreach ($sale_items as $item) {
                if ($item->product_type == 'standard') {
                    $this->syncProductQty($item->product_id, $item->warehouse_id);
                    if (isset($item->option_id) && !empty($item->option_id)) {
                        $this->syncVariantQty($item->option_id, $item->warehouse_id, $item->product_id);
                    }
                } elseif ($item->product_type == 'combo') {
                    $wh = $this->Settings->overselling ? null : $item->warehouse_id;
                    $combo_items = $this->getProductComboItems($item->product_id, $wh);
                    foreach ($combo_items as $combo_item) {
                        if ($combo_item->type == 'standard') {
                            $this->syncProductQty($combo_item->id, $item->warehouse_id);
                        }
                    }
                }
            }
        } elseif ($purchase_id) {
            $purchase_items = $this->getAllPurchaseItems($purchase_id);
            foreach ($purchase_items as $item) {
                $this->syncProductQty($item->product_id, $item->warehouse_id);
                if (isset($item->option_id) && !empty($item->option_id)) {
                    $this->syncVariantQty($item->option_id, $item->warehouse_id, $item->product_id);
                }
            }
        } elseif ($oitems) {
            foreach ($oitems as $item) {
                if (isset($item->product_type)) {
                    if ($item->product_type == 'standard') {
                        $this->syncProductQty($item->product_id, $item->warehouse_id);
                        if (isset($item->option_id) && !empty($item->option_id)) {
                            $this->syncVariantQty($item->option_id, $item->warehouse_id, $item->product_id);
                        }
                    } elseif ($item->product_type == 'combo') {
                        $combo_items = $this->getProductComboItems($item->product_id, $item->warehouse_id);
                        foreach ($combo_items as $combo_item) {
                            if ($combo_item->type == 'standard') {
                                $this->syncProductQty($combo_item->id, $item->warehouse_id);
                            }
                        }
                    }
                } else {
                    $this->syncProductQty($item->product_id, $item->warehouse_id);
                    if (isset($item->option_id) && !empty($item->option_id)) {
                        $this->syncVariantQty($item->option_id, $item->warehouse_id, $item->product_id);
                    }
                }
            }
        } elseif ($product_id) {
            $warehouses = $this->getAllWarehouses();
            foreach ($warehouses as $warehouse) {
                $this->syncProductQty($product_id, $warehouse->id);
                if ($product_variants = $this->getProductVariants($product_id)) {
                    foreach ($product_variants as $pv) {
                        $this->syncVariantQty($pv->id, $warehouse->id, $product_id);
                    }
                }
            }
        }
    }

    public function getProductVariants($product_id)
    {
        $q = $this->db->get_where('product_variants', array('product_id' => $product_id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getAllSaleItems($sale_id)
    {
        $q = $this->db->get_where('sale_items', array('sale_id' => $sale_id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getAllPurchaseItems($purchase_id)
    {
        $q = $this->db->get_where('purchase_items', array('purchase_id' => $purchase_id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function syncPurchaseItems($data = array())
    {
        if (!empty($data)) {
            foreach ($data as $items) {
                foreach ($items as $item) {
                    if (isset($item['pi_overselling'])) {
                        unset($item['pi_overselling']);
                        $option_id = (isset($item['option_id']) && !empty($item['option_id'])) ? $item['option_id'] : null;
                        $clause = array('purchase_id' => null, 'transfer_id' => null, 'product_id' => $item['product_id'], 'warehouse_id' => $item['warehouse_id'], 'option_id' => $option_id);
                        if ($pi = $this->getPurchasedItem($clause)) {
                            $quantity_balance = $pi->quantity_balance + $item['quantity_balance'];
                            $this->db->update('purchase_items', array('quantity_balance' => $quantity_balance), array('id' => $pi->id));
                        } else {
                            $clause['quantity'] = 0;
                            $clause['item_tax'] = 0;
                            $clause['quantity_balance'] = $item['quantity_balance'];
                            $clause['status'] = 'received';
                            $clause['option_id'] = !empty($clause['option_id']) && is_numeric($clause['option_id']) ? $clause['option_id'] : null;
                            $this->db->insert('purchase_items', $clause);
                        }
                    } else {
                        if ($item['inventory']) {
                            $this->db->update('purchase_items', array('quantity_balance' => $item['quantity_balance']), array('id' => $item['purchase_item_id']));
                        }
                    }
                }
            }
            return true;
        }
        return false;
    }

    public function getProductByCode($code)
    {
        $q = $this->db->get_where('products', array('code' => $code), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function check_customer_deposit($customer_id, $amount)
    {
        $customer = $this->getCompanyByID($customer_id);
        return $customer->deposit_amount >= $amount;
    }

    public function getWarehouseProduct($warehouse_id, $product_id)
    {
        $q = $this->db->get_where('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse_id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getAllBaseUnits()
    {
        $q = $this->db->get_where("units", array('base_unit' => null));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getUnitsByBUID($base_unit)
    {
        $this->db->where('id', $base_unit)->or_where('base_unit', $base_unit)
        ->group_by('id')->order_by('id asc');
        $q = $this->db->get("units");
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getUnitByID($id)
    {
        $q = $this->db->get_where("units", array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getPriceGroupByID($id)
    {
        $q = $this->db->get_where('price_groups', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getProductGroupPrice($product_id, $group_id)
    {
        $q = $this->db->get_where('product_prices', array('price_group_id' => $group_id, 'product_id' => $product_id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getAllBrands()
    {
        $q = $this->db->get("brands");
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getBrandByID($id)
    {
        $q = $this->db->get_where('brands', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function convertToBase($unit, $value)
    {
        switch ($unit->operator) {
            case '*':
                return $value / $unit->operation_value;
                break;
            case '/':
                return $value * $unit->operation_value;
                break;
            case '+':
                return $value - $unit->operation_value;
                break;
            case '-':
                return $value + $unit->operation_value;
                break;
            default:
                return $value;
        }
    }

    public function calculateTax($product_details = null, $tax_details, $custom_value = null, $c_on = null)
    {
        $value = $custom_value ? $custom_value : (($c_on == 'cost') ? $product_details->cost : $product_details->price);
        $tax_amount = 0;
        $tax = 0;
        if ($tax_details && $tax_details->type == 1 && $tax_details->rate != 0) {
            if ($product_details && $product_details->tax_method == 1) {
                $tax_amount = $this->sma->formatDecimal((($value) * $tax_details->rate) / 100, 4);
                $tax = $this->sma->formatDecimal($tax_details->rate, 0) . "%";
            } else {
                $tax_amount = $this->sma->formatDecimal((($value) * $tax_details->rate) / (100 + $tax_details->rate), 4);
                $tax = $this->sma->formatDecimal($tax_details->rate, 0) . "%";
            }
        } elseif ($tax_details && $tax_details->type == 2) {
            $tax_amount = $this->sma->formatDecimal($tax_details->rate);
            $tax = $this->sma->formatDecimal($tax_details->rate, 0);
        }

        $tax_id = $tax_details && isset($tax_details->id) ? $tax_details->id : null;
        return array('id' => $tax_id, 'tax' => $tax, 'amount' => $tax_amount);
    }

    public function getAddressByID($id)
    {
        return $this->db->get_where('addresses', ['id' => $id], 1)->row();
    }

    public function checkSlug($slug, $type = null)
    {
        if (!$type) {
            return $this->db->get_where('products', ['slug' => $slug], 1)->row();
        } elseif ($type == 'category') {
            return $this->db->get_where('categories', ['slug' => $slug], 1)->row();
        } elseif ($type == 'brand') {
            return $this->db->get_where('brands', ['slug' => $slug], 1)->row();
        }
        return false;
    }

    public function calculateDiscount($discount = null, $amount)
    {
        if ($discount && $this->Settings->product_discount) {
            $dpos = strpos($discount, '%');
            if ($dpos !== false) {
                $pds = explode("%", $discount);
                return $this->sma->formatDecimal(((($this->sma->formatDecimal($amount)) * (Float) ($pds[0])) / 100), 4);
            } else {
                return $this->sma->formatDecimal($discount, 4);
            }
        }
        return 0;
    }

    public function calculateOrderTax($order_tax_id = null, $amount)
    {
        if ($this->Settings->tax2 != 0 && $order_tax_id) {
            if ($order_tax_details = $this->site->getTaxRateByID($order_tax_id)) {
                if ($order_tax_details->type == 1) {
                    return $this->sma->formatDecimal((($amount * $order_tax_details->rate) / 100), 4);
                } else {
                    return $this->sma->formatDecimal($order_tax_details->rate, 4);
                }
            }
        }
        return 0;
    }

    public function getSmsSettings()
    {
        $q = $this->db->get('sms_settings');
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function api_auto_id($table, $field)
    {
        $temp = $this->api_select_some_fields_with_where("MAX(".$field.") as max", $table, $field." > 0", "arr");
        if (count($temp) > 0) {
            return $temp[0]['max'] + 1;
        } else {
            return 1;
        }
    }
    public function api_update_table($table_name, $field_array, $condition)
    {
        $this->db->set($field_array);
        $this->db->where($condition);
        $this->db->update($table_name);
    }
    public function api_insert_into($table, $data, $view_stm=false)
    {
        for ($i=0;$i<count($data);$i++) {
            if (is_numeric($data[$i]) && substr($data[$i], 0, 1)!="0") {
                $values[]=$data[$i];
            } else {
                $values[]="'".$data[$i]."'";
            }
        }
        $values=implode(",", $values);
        $sql=("INSERT INTO $table VALUES ($values)");
        $this->db->query($sql);
    }
    public function api_select_some_fields_with_where($field_names, $table_name, $condition, $type)
    {
        $temp = 'Select Field ==> '.$field_names.'<br/>'.'Select Table ==> '.$table_name.'<br/>'.'Condition ==> '.$condition.'<br/>';
        $sql = "SELECT $field_names FROM $table_name WHERE ".$condition;
        $query = $this->db->query($sql)or die($this->db->error().$temp);
        if ($type == 'arr') {
            return $query->result_array();
        } elseif ($type == 'obj') {
            return $query->result();
        }
        $query->free_result();
    }
    public function api_duplicate_record_update($config_data)
    {
        $sql = "CREATE TEMPORARY TABLE api_temp SELECT * FROM ".$config_data['duplicate_table']." WHERE ".$config_data['condition'];
        $this->db->query($sql);
        $sql = "UPDATE api_temp set ".$config_data['duplicate_id_col_name']." = NULL";
        $this->db->query($sql);
        if ($config_data['count_update'] > 0) {
            for ($i=1;$i<=$config_data['count_update'];$i++) {
                $sql = "UPDATE api_temp set ".$config_data['update_'.$i];
                $this->db->query($sql);
            }
        }
        $sql = "INSERT INTO ".$config_data['into_table']." SELECT * FROM api_temp";
        $this->db->query($sql);
        $sql = "DROP TEMPORARY TABLE IF EXISTS api_temp";
        $this->db->query($sql);
    }
    public function api_get_condition_add_ons_field($add_ons_field, $table, $condition)
    {
        $return = '';
        $temp = $this->api_select_some_fields_with_where("add_ons", $table, $condition, "arr");
        if (isset($temp[0]["add_ons"])) {
            $temp = $this->api_get_add_ons_field($temp[0]["add_ons"]);
            foreach (array_keys($temp) as $key) {
                $return .= ",getTranslate(".$add_ons_field.",'".$key."','".f_separate."','".v_separate."') as ".$key;
            }
        }
        return $return;
    }
    public function api_get_add_ons_field($add_on_value)
    {
        $return = array();
        $temp = html_entity_decode($add_on_value);
        $temp = str_replace('https:', '', $temp);
        $temp = str_replace('http:', '', $temp);
        $temp = explode(':', $temp);
        $j = 1;
        $k = 0;
        for ($i=0;$i<count($temp);$i++) {
            if (($j%2) != 0) {
                $temp_1[$k] = $temp[$i];
            } else {
                $temp_2[$k] = $temp[$i];
                $k++;
            }
            $j++;
        }
        for ($i=0;$i<count($temp_1);$i++) {
            if ($temp_1[$i] != '') {
                $temp_2[$i] = substr($temp_2[$i], 1);
                $temp_2[$i] = substr($temp_2[$i], 0, -1);
                $return[$temp_1[$i]] = $temp_2[$i];
            }
        }
        return $return;
    }
    public function api_update_add_ons_field($config_data)
    {
        $temp_field = $this->api_get_condition_add_ons_field($config_data['field_add_ons_name'], 'sma_add_ons', "table_name = '".$config_data['table_name']."'");
        $select_data = $this->api_select_some_fields_with_where(
            $config_data['id_name'].", getTranslate(".$config_data['field_add_ons_name'].",'".$config_data['add_ons_title']."','".f_separate."','".v_separate."') as api_temp_selected_add_on_value
            ".$temp_field,
            $config_data['table_name'],
            $config_data['id_name']." = ".$config_data['selected_id'],
            "arr"
        );
        foreach (array_keys($select_data[0]) as $key) {
            if ($key != $config_data['id_name'] && $key != $config_data['field_add_ons_name'] && $key != 'api_temp_selected_add_on_value') {
                if ($select_data[0][$key] == '' && $key != 'initial_first_add_ons') {
                    $temp_add_ons_value .= $key.':{}:';
                } else {
                    $temp_add_ons_value .= $key.':{'.$select_data[0][$key].'}:';
                }
            }
        }
        $select_data[0][$config_data['field_add_ons_name']] = str_replace($config_data['add_ons_title'].':{'.$select_data[0]['api_temp_selected_add_on_value'].'}:', $config_data['add_ons_title'].':{'.$config_data['add_ons_value'].'}:', $temp_add_ons_value);
        $this->api_update_table($config_data['table_name'], array($config_data['field_add_ons_name'] => $select_data[0][$config_data['field_add_ons_name']]), $config_data['id_name']." = ".$config_data['selected_id']);
    }
    public function api_update_translate_field($config_data)
    {
        $config_data_2 = array(
            'table_name' => $config_data['table_name'],
            'select_table' => $config_data['table_name'],
            'translate' => 'yes',
            'select_condition' => $config_data['id_name']." = ".$config_data['selected_id'],
        );
        $temp = $this->site->api_select_data_v2($config_data_2);

        $language_array = unserialize(multi_language);
        for ($i=0;$i<count($language_array);$i++) {                  
            if ($language_array[$i][0] != $config_data['add_ons_title'])  
                $temp_translate .= $language_array[$i][0].":{".$temp[0]['title_'.$language_array[$i][0]]."}:";
            else
                $temp_translate .= $config_data['add_ons_title'].":{".$config_data['add_ons_value']."}:";
        }

        $this->api_update_table($config_data['table_name'], array($config_data['field_add_ons_name'] => $temp_translate), $config_data['id_name']." = ".$config_data['selected_id']);
    }    
	public function api_select_data_v2($config_data)
    {
        if ($config_data['translate'] == 'yes') {
            $language_array = unserialize(multi_language);
            for ($i=0;$i<count($language_array);$i++) {                    
                $temp_translate .= ", getTranslate(translate,'".$language_array[$i][0]."','".f_separate."','".v_separate."') as title_".$language_array[$i][0];                
            }
        }
        $temp_field = $this->api_get_condition_add_ons_field("add_ons","sma_add_ons","table_name = '".$config_data['table_name']."'");
        $select_data = $this->api_select_some_fields_with_where("*
            ".$temp_translate."
            ".$temp_field
            ,$config_data['select_table']
            ,$config_data['select_condition']
            ,"arr"
        );
        return $select_data;        
    }

    public function api_calculate_product_promotion($select_data) {
        $return['price'] = 0;
        $return['duration'] = 0;
        date_default_timezone_set('Asia/Phnom_Penh');
        $now = date('Y-m-d h:i:s');
        if ($select_data[0]['promotion'] == 1) {
            if ($select_data[0]['promotion_type'] != 'list') {
                if ($select_data[0]['start_date'] <= $now && $select_data[0]['end_date'] >= $now && $select_data[0]['promo_price'] > 0) {                    
                    $return['price'] = $select_data[0]['promo_price'];
                    $return['rate'] = ($select_data[0]['promo_price'] * 100) / $select_data[0]['price'];             
                }
                $b_expired = 1;
                if ($select_data[0]['start_date'] <= $now && $select_data[0]['end_date'] >= $now)
                    $b_expired = 0;
                $return['is_expired'] = $b_expired;
                if ($return['price'] > 0) {
                    $dStart = date_create($now);
                    $dEnd  = date_create($select_data[0]['end_date']);
                    $dDiff = $dStart->diff($dEnd);   
                    if ($dDiff->days > 1)
                        $temp2 = 's';
                    else
                        $temp2 = '';
                        
                    if ($dDiff->days > 1)
                        $day_left = lang('days_left');
                    else
                        $day_left = lang('day_left');
                    $return['duration'] = date('l jS F Y', strtotime($select_data[0]['end_date'])).'<br>('.$dDiff->days.' '.lang('day'.$temp2.'_left').')';
                    $return['duration_end_date'] = date('l jS F Y', strtotime($select_data[0]['end_date']));
                    $return['duration_left'] = $dDiff->days.' '.$day_left;
                    
                    $return['min_qty'] = $select_data[0]['min_qty'];   
                    $temp = $select_data[0]['price'] - $select_data[0]['promo_price'];
                    $return['rate'] = ($temp * 100) / $select_data[0]['price'];
                    $return['rate'] = $return['rate'];
                }
                
            }
            else {
                if ($select_data[0]['promotion_id'] != '') {
                    $config_data = array(
                        'table_name' => 'sma_product_promotion',
                        'select_table' => 'sma_product_promotion',
                        'translate' => '',
                        'select_condition' => "id = ".$select_data[0]['promotion_id'],
                    );
                    $temp = $this->site->api_select_data_v2($config_data);

                    $b_expired = 1;
                    if ($temp[0]['status'] != 'disabled') {                        
                        if ($temp[0]['start_date'] <= $now && $temp[0]['end_date'] >= $now) {
                            if ($temp[0]['type_discount'] == 'percentage') {
                                $config_data_2 = array(
                                    'table_name' => 'sma_product_promotion_item',
                                    'select_table' => 'sma_product_promotion_item',
                                    'translate' => '',
                                    'description' => '',
                                    'select_condition' => "product_id = ".$select_data[0]['id']." and promotion_id = ".$select_data[0]['promotion_id'],
                                );
                                $temp_3 = $this->api_helper->api_select_data_v2($config_data_2);

                                if (count($temp_3) > 0) {
                                    $temp_2 = ($select_data[0]['price'] * $temp_3[0]['rate']) / 100;
                                    if ($temp_3[0]['rate'] > 0) {
                                        $return['price'] = $select_data[0]['price'] - $temp_2;    
                                        $return['rate'] = $temp_3[0]['rate'];       
                                    }
                                    else 
                                        $return['price'] = $select_data[0]['price'];
                                }
                                else {
                                    $temp_2 = ($select_data[0]['price'] * $temp[0]['rate']) / 100;     
                                    if ($temp[0]['rate'] > 0) {
                                        $return['price'] = $select_data[0]['price'] - $temp_2;    
                                        $return['rate'] = $temp_3[0]['rate'];       
                                    }
                                    else 
                                        $return['price'] = $select_data[0]['price'];
                                }

        
                            }
                            if ($temp[0]['type_discount'] == 'fixed_price' && $temp[0]['fixed_price'] < $select_data[0]['price']) {
                                $return['price'] = $temp[0]['fixed_price'];
                            }
                            $b_expired = 0;
                        }
                    }
                    if ($return['price'] > 0) {
                        $dStart = date_create($now);
                        $dEnd  = date_create($temp[0]['end_date']);
                        $dDiff = $dStart->diff($dEnd);   
                        if ($dDiff->days > 1)
                            $temp2 = 's';
                        else
                            $temp2 = '';
                        $return['duration'] = date('l jS F Y', strtotime($temp[0]['end_date'])).'<br>('.$dDiff->days.' '.lang('day'.$temp2.'_left').')';
                        $return['duration_end_date'] = date('l jS F Y', strtotime($temp[0]['end_date']));
                        $return['duration_left'] = $dDiff->days.' '.lang('day'.$temp2.'_left');
                        $return['min_qty'] = $temp[0]['min_qty'];
                    }
                    
                    $return['is_expired'] = $b_expired;
                }
                
            }
        }
        
        if ($return['min_qty'] <= 0) $return['min_qty'] = 1;
        return $return;
    }
    
    public function api_calculate_product_price($config_data)
    {
        $config_data_2 = array(
            'table_name' => 'sma_products',
            'select_table' => 'sma_products',
            'select_condition' => "id = ".$config_data['id'],
        );
        $select_product = $this->api_select_data_v2($config_data_2);
        
        $select_customer = array();
        $select_customer_group = array();
        $select_product_discount = array();
        if ($config_data['customer_id'] != '') {
            $config_data_2 = array(
                'table_name' => 'sma_companies',
                'select_table' => 'sma_companies',
                'select_condition' => "id = ".$config_data['customer_id'],
            );
            $select_customer = $this->api_select_data_v2($config_data_2);
            if ($select_customer[0]['customer_group_id'] != '') {
                $select_customer_group = $this->api_select_some_fields_with_where(
                    "
                    percent     
                    ",
                    "sma_customer_groups",
                    "id = ".$select_customer[0]['customer_group_id'],
                    "arr"
                );
            }
            $select_product_discount = $this->getCPDiscountByIDCID($config_data['id'], $config_data['customer_id']);
        }
        
        
        $warehouse = $this->getWarehouseByID($config_data['warehouse_id']);

        if ($config_data['option_id'] <= 0) {
            $temp = $this->api_select_some_fields_with_where(
                "
                id, price
                ",
                "sma_product_variants",
                "product_id = ".$config_data['id']." order by id asc limit 1",
                "arr"
            );
            if (is_array($temp)) {
                if (count($temp) > 0) {
                    $temp_option[0]['price'] = $temp[0]['price'];
                }
            }
        } else {
            $temp_option = $this->api_select_some_fields_with_where(
                "
                price
                ",
                "sma_product_variants",
                "id = ".$config_data['option_id'],
                "arr"
            );
        }
        $return['temp_price'] = $select_product[0]['price'];

        $return['price'] = $select_product[0]['price'];
        $return['price'] = $return['price'];
        $return['option'] = 'normal';
        $temp_promotion = $this->site->api_calculate_product_promotion($select_product);
        if ($temp_promotion['price'] > 0) {
            $return['price'] = $temp_promotion['price'];
            $return['option'] = 'promotion';
            $return['promotion'] = $temp_promotion;
            $return['promotion']['discount_price'] = $return['temp_price'] - $temp_promotion['price'];
            $return['promotion']['rate'] = $temp_promotion['rate'];            
        }
        elseif ($select_product_discount) {
            $return['price'] = $select_product_discount->special_price;
            $return['option'] = 'product_discount';
        }

        $return['price'] = $return['price'] + $temp_option[0]['price'];

        //customer_discount-============================================
        if ($select_customer[0]['discount_product'] > 0) {
            if (!$select_product_discount) {
                $return['price'] = $return['price'] - ($return['price'] * $select_customer[0]['discount_product']) / 100;
            }
            $return['option'] .= '_customer_discount';
        }
        //customer_discount-============================================
     
        //customer_group-============================================
        if ($select_customer_group[0]['percent'] > 0 && $return['option'] != 'product_discount') {
            $return['price'] = $return['price'] + ($return['price'] * $select_customer_group[0]['percent']) / 100;
        }
        if ($select_customer_group[0]['percent'] < 0 && $return['option'] != 'product_discount') {
            $return['price'] = $return['price'] - ($return['price'] * abs($select_customer_group[0]['percent'])) / 100;
        }
        //customer_group-============================================

        $return['price'] = $this->api_helper->round_up_second_decimal($return['price']);

        return $return;
    }
    public function api_update_product_display($config_data)
    {
        if ($config_data['category_id'] != '') {
            $config_data_2 = array(
                'table_name' => 'sma_categories',
                'select_table' => 'sma_categories',
                'select_condition' => "id = ".$config_data['category_id'],
            );
            $temp = $this->api_select_data_v2($config_data_2);
        }

        if ($config_data['subcategory_id'] != '') {
            $config_data_2 = array(
                'table_name' => 'sma_categories',
                'select_table' => 'sma_categories',
                'select_condition' => "id = ".$config_data['subcategory_id'],
            );
            $temp_2 = $this->api_select_data_v2($config_data_2);
        }

        if ($temp[0]['display'] == 'no' || $temp_2[0]['display'] == 'no') {
            $temp_3 = 'no';
        } else {
            $temp_3 = 'yes';
        }

        $config_data_2 = array(
            'table_name' => 'sma_products',
            'id_name' => 'id',
            'field_add_ons_name' => 'add_ons',
            'selected_id' => $config_data['product_id'],
            'add_ons_title' => 'display',
            'add_ons_value' => $temp_3,
        );
        $this->api_update_add_ons_field($config_data_2);
    }


    public function api_update_quantity_stock($config_data)
    {
        if ($config_data['option_id'] <= 0) {
            $temp_2 = $this->site->api_select_some_fields_with_where(
                "
                quantity
                ",
                "sma_warehouses_products",
                "product_id = ".$config_data['product_id']." and warehouse_id = ".$config_data['warehouse_id'],
                "arr"
            );
            if (is_array($temp_2)) {
                if (count($temp_2) > 0) {
                } else {
                    $temp_5 = array(
                    'product_id' => $config_data['product_id'],
                    'warehouse_id' => $config_data['warehouse_id'],
                    'quantity' => 0
                );
                    $this->db->insert('sma_warehouses_products', $temp_5);
                    $temp_2[0]['quantity'] = 0;
                }
            }
        } else {
            $temp_2 = $this->site->api_select_some_fields_with_where(
                "
                quantity
                ",
                "sma_warehouses_products_variants",
                "option_id = ".$config_data['option_id']." and product_id = ".$config_data['product_id']." and warehouse_id = ".$config_data['warehouse_id'],
                "arr"
            );
            if (is_array($temp_2)) {
                if (count($temp_2) > 0) {
                } else {
                    $temp_5 = array(
                    'option_id' => $config_data['option_id'],
                    'product_id' => $config_data['product_id'],
                    'warehouse_id' => $config_data['warehouse_id'],
                    'quantity' => 0
                );
                    $this->db->insert('sma_warehouses_products_variants', $temp_5);
                    $temp_2[0]['quantity'] = 0;
                }
            }
        }

        if (is_array($temp_2)) {
            if (count($temp_2) > 0) {
                $temp_3 = $temp_2[0]['quantity'];
                if ($config_data['type'] == 'addition') {
                    $temp_3 = $temp_2[0]['quantity'] + $config_data['quantity'];
                }
                if ($config_data['type'] == 'subtraction') {
                    $temp_3 = $temp_2[0]['quantity'] - $config_data['quantity'];
                }
                $temp = array(
                'quantity' => $temp_3
            );

                if ($config_data['option_id'] <= 0) {
                    $this->db->update('sma_warehouses_products', $temp, 'product_id = '.$config_data['product_id'].' and warehouse_id = '.$config_data['warehouse_id']);
                } else {
                    $this->db->update('sma_product_variants', $temp, 'id = '.$config_data['option_id']);
                    $this->db->update('sma_warehouses_products_variants', $temp, 'product_id = '.$config_data['product_id'].' and warehouse_id = '.$config_data['warehouse_id'].' and option_id = '.$config_data['option_id']);
                }
                $config_data_2 = array(
                'product_id' => $config_data['product_id'],
                'warehouse_id' => $config_data['warehouse_id'],
            );
                $this->api_update_product_quantity($config_data_2);
            }
        }
    }

    public function api_check_product_stock($config_data)
    {
        $temp = $this->site->api_select_some_fields_with_where(
            "
            quantity
            ",
            "sma_products",
            "id = ".$config_data['product_id'],
            "arr"
        );
        if ($config_data['compare_quantity'] > $temp[0]['quantity']) {
            $return['result'] = 0;
        } else {
            $return['result'] = 1;
        }
        return $return;
    }

    public function api_reverse_quantity($config_data)
    {
        $temp = $this->site->api_select_some_fields_with_where(
            "
            *     
            ",
            $config_data['table'],
            $config_data['table_id']." = ".$config_data['id']." and warehouse_id = ".$config_data['warehouse_id'],
            "arr"
        );
        if (is_array($temp)) {
            if (count($temp) > 0) {
                for ($i=0;$i<count($temp);$i++) {
                    $temp_2 = $this->site->api_select_some_fields_with_where(
                        "
                quantity     
                ",
                        "sma_warehouses_products",
                        "product_id = ".$temp[$i]['product_id']." and warehouse_id = ".$config_data['warehouse_id'],
                        "arr"
                    );

                    $temp_3 = $temp_2[0]['quantity'];
                    if ($temp[$i]['type'] == 'addition') {
                        $temp_3 = $temp_2[0]['quantity'] - $temp[$i]['quantity'];
                    }
                    if ($temp[$i]['type'] == 'subtraction') {
                        $temp_3 = $temp_2[0]['quantity'] + $temp[$i]['quantity'];
                    }
                    $temp_4 = array(
                'quantity' => $temp_3
            );
                    $this->db->update('sma_products', $temp_4, 'id = '.$temp[$i]['product_id']);
                    $this->db->update('sma_warehouses_products', $temp_4, 'product_id = '.$temp[$i]['product_id'].' and warehouse_id = '.$config_data['warehouse_id']);
                }
            }
        }
    }

    public function api_get_image($data_view) {
        if (!isset($data_view['image_icon_type'])) $data_view['image_icon_type'] = '';
    
        $temp = $this->api_img_resize_full($data_view);    
        if ($data_view['resize_type'] == 'size') {
            $temp_resize_header = '<div style="max-width:'.$data_view['resize_width'].'px; width:'.$data_view['resize_width'].'px; max-height:'.$data_view['resize_height'].'px; height:'.$data_view['resize_height'].'px; overflow:hidden;">';
            $temp_resize_footer = '</div>';
        }
        // if ($data_view['resize_type'] == 'fixed')
        //     $temp = $this->api_img_resize_fixed($data_view);
        $img = $temp['image'];
        
        if (!isset($temp['no_image'])) $temp['no_image'] = '';
        
        $return['image'] = $temp['image'];
        $return['no_image'] = $temp['no_image'];    
        $return['image_src'] = $temp['image_src'];
        $return['image_full_src'] = $temp['image_full_src'];
        if ($data_view['wrapper_class'] != '')         
            $return['image'] = '
            '.$temp_resize_header.'
                <div class="'.$data_view['wrapper_class'].'">
                    '.$img.'
                </div>
            '.$temp_resize_footer.'
            ';
        else        
            $return['image'] = $img;
    
    
        $return['image_table'] = '
            '.$temp_resize_header.'
            <table class="'.$data_view['table_class'].'" width="100%" cellpadding="0" cellspacing="0" border="0">
            <tr>
            <td valign="middle" align="center" height="'.$data_view['table_height'].'">
            '.$img.' 
            </td>
            </tr>
            </table>
            '.$temp_resize_footer.'
        ';
        $return['image_table_link'] = '
            '.$temp_resize_header.'
            <table class="'.$data_view['table_class'].'" width="100%" cellpadding="0" cellspacing="0" border="0">
            <tr>
            <td valign="middle" align="center" height="'.$data_view['table_height'].'">
                <a href="'.$return['image_full_src'].'" target="_blank">
                    '.$temp['image'].'
                </a>  
            </td>
            </tr>
            </table>
            '.$temp_resize_footer.'
        ';    
        $return['image_link'] = '
            '.$temp_resize_header.'
            <a href="'.$return['image_full_src'].'" target="_blank">
                '.$temp['image'].'
            </a>  
            '.$temp_resize_footer.'   
        ';
    
    
        if (!isset($page_link)) $page_link = '';
        
        $return['page_link'] = $page_link;
        return $return;
    }
    
    public function api_img_resize_full($config_data){
        if (!isset($config_data['image_width'])) $config_data['image_width'] = '';
        if (!isset($config_data['image_height'])) $config_data['image_height'] = '';
        if (!isset($config_data['image_title'])) $config_data['image_title'] = '';
    
        $file_path = $config_data['file_path'];
        $max_width = $config_data['max_width'];
        $max_height = $config_data['max_height'];
        $img_id = $config_data['image_id'];
        $img_class = $config_data['image_class'];
        $img_width = $config_data['image_width'];
        $img_height = $config_data['image_height'];
        $img_src = '';
        $temp_array['image_full_src'] = base_url().$config_data['file_path'];
        if (!is_file($file_path)) {  
            $temp_array['image_full_src'] = base_url().'assets/uploads/no_image.jpg';
            $file_path = 'assets/uploads/no_image.jpg';
        }
    
        if ($img_width <= 0 || $img_height <= 0) {
            if (is_file($file_path)) {            
                $data = GetImageSize($file_path);
                $img_width = $data[0];
                $img_height = $data[1];   
                if ($img_width <= 0)
                    $img_width = $max_width;
            }         
        }
    
    
        if (is_file($file_path)) {     
            if ($img_width > $max_width && $img_height <= $max_height) {
                $temp_height = ($img_height * $max_width) / $img_width;
                $temp_height = round($temp_height);
                if ($temp_height > $max_height) $temp_height = $max_height;
                $img_1 = '<img id="'.$img_id.'" class="'.$img_class.'" src="'.$temp_array['image_full_src'].'" width="'.$max_width.'" height="'.$temp_height.'" title="'.$config_data['image_title'].'"/>';
            }
            elseif ($img_width > $max_width && $img_height > $max_height) {
                if ($img_width > $img_height) {                     
                    $temp_height = ($img_height * $max_width) / $img_width;
                    $temp_height = round($temp_height);                                       
                    if ($temp_height > $max_height) $temp_height = $max_height;                                     
                    if ($temp_height < $max_height) {
                        if ($temp_height > $img_height) $temp_height = $img_height;
                        $img_1 = '<img id="'.$img_id.'" class="'.$img_class.'" src="'.$temp_array['image_full_src'].'" width="'.$max_width.'" height="'.$temp_height.'" title="'.$config_data['image_title'].'"/>';
                    }
                    else {
                        $temp_width = ($img_width * $max_height) / $img_height;
                        $temp_width = round($temp_width);
                        $img_1 = '<img id="'.$img_id.'" class="'.$img_class.'" src="'.$temp_array['image_full_src'].'" width="'.$temp_width.'" height="'.$temp_height.'" title="'.$config_data['image_title'].'"/>';                    
                    }                           
                }
                else {
                    $temp_width = ($img_width * $max_height) / $img_height;
                    $temp_width = round($temp_width);                         
                    if ($temp_width > $max_width) $temp_width = $max_width;                                     
                    if ($temp_width < $max_width) {
                        $img_1 = '<img id="'.$img_id.'" class="'.$img_class.'" src="'.$temp_array['image_full_src'].'" height="'.$max_height.'" width="'.$temp_width.'" title="'.$config_data['image_title'].'"/>';
                    }
                    else {
                        $temp_height = ($img_height * $max_width) / $img_width;
                        $temp_height = round($temp_height);                         
                        if ($temp_height > $max_height) $temp_height = $max_height;                                     
                        $img_1 = '<img id="'.$img_id.'" class="'.$img_class.'" src="'.$temp_array['image_full_src'].'" height="'.$temp_height.'" width="'.$temp_width.'" title="'.$config_data['image_title'].'"/>';                       
                    }
                }
            }
            elseif ($img_width <= $max_width && $img_height <= $max_height) {   
                $img_1 = '<img id="'.$img_id.'" class="'.$img_class.'" src="'.$temp_array['image_full_src'].'" width="'.$img_width.'" height="'.$img_height.'" title="'.$config_data['image_title'].'"/>';
            }
            elseif ($img_width <= $max_width && $img_height > $max_height) {
                $temp_width = ($img_width * $max_height) / $img_height;
                $temp_width = round($temp_width);                   
                if ($temp_width > $max_width) $temp_width = $max_width;                                     
                $img_1 = '<img id="'.$img_id.'" class="'.$img_class.'" src="'.$temp_array['image_full_src'].'" height="'.$max_height.'px" width="'.$temp_width.'" title="'.$config_data['image_title'].'"/>';
            }
        }
        else 
            $temp_array['no_image'] = 1;    
    
    
        $temp_array['image'] = $img_1;
        
        $temp_array['image_table'] = '
            <table class="'.$config_data['table_class'].'" width="100%" cellpadding="0" cellspacing="0" border="1">
            <tr>
            <td valign="middle" align="center" height="'.$max_height.'">
               '.$img_1.' 
            </td>
            </tr>
            </table>
        ';
        
        return $temp_array;
    }
    
    public function api_update_product_quantity($config_data)
    {
        if ($config_data['warehouse_id'] == '') {
            $config_data['warehouse_id'] = $this->Settings->default_warehouse;
        }

        $temp = $this->api_select_some_fields_with_where(
            "
            quantity,option_id
            ",
            "sma_warehouses_products_variants",
            "product_id = ".$config_data['product_id']." and warehouse_id = ".$config_data['warehouse_id'],
            "arr"
        );
        if (is_array($temp)) {
            if (count($temp) > 0) {
                for ($i=0;$i<count($temp);$i++) {
                    $temp2 = $this->api_select_some_fields_with_where(
                        "
                    product_id
                    ",
                        "sma_product_variants",
                        "product_id = ".$config_data['product_id']." and id = ".$temp[$i]['option_id'],
                        "arr"
                    );
                    if ($temp2[0]['product_id'] <= 0) {
                        $this->db->delete('sma_warehouses_products_variants', "option_id = ".$temp[$i]['option_id']." and product_id = ".$config_data['product_id']);
                    }
                }
            }
        }

        $temp = $this->api_select_some_fields_with_where(
            "
            quantity,option_id
            ",
            "sma_warehouses_products_variants",
            "product_id = ".$config_data['product_id']." and warehouse_id = ".$config_data['warehouse_id'],
            "arr"
        );
        if (is_array($temp)) {
            if (count($temp) > 0) {
                $temp_qty = 0;
                for ($i=0;$i<count($temp);$i++) {
                    $temp_qty = $temp_qty + ($temp[$i]['quantity']);
                }
                $temp2 = array(
                'quantity' => $temp_qty
            );
            } else {
                $temp = $this->api_select_some_fields_with_where(
                    "product_id,
                quantity
                ",
                    "sma_warehouses_products",
                    "product_id = ".$config_data['product_id']." and warehouse_id = ".$config_data['warehouse_id'],
                    "arr"
                );
                $temp2 = array(
                'quantity' => $temp[0]['quantity']
            );
            }
        }
        if ($config_data['warehouse_id'] == 1) {
            $this->db->update('sma_products', $temp2, 'id = '.$config_data['product_id']);
        } else {
            $config_data_2 = array(
                'table_name' => 'sma_products',
                'id_name' => 'id',
                'field_add_ons_name' => 'add_ons',
                'selected_id' => $config_data['product_id'],
                'add_ons_title' => 'quantity_'.$config_data['warehouse_id'],
                'add_ons_value' => $temp2['quantity'],
            );

            $this->api_update_add_ons_field($config_data_2);
        }
    }

    public function api_get_customer_order_condition()
    {
        if ($this->session->userdata('user_id') > 0) {
            $config_data = array(
                'table_name' => 'sma_users',
                'select_table' => 'sma_users',
                'translate' => '',
                'select_condition' => "id = ".$this->session->userdata('user_id'),
            );
            $temp_user = $this->site->api_select_data_v2($config_data);

            $temp_3 = "company_id = ".$this->session->userdata('company_id');
            if ($temp_user[0]['company_branch_type'] == 'purchaser' && $temp_user[0]['company_branch'] != '') {
                $condition .= " and getTranslate(add_ons,'company_branch','".f_separate."','".v_separate."') = '".$temp_user[0]['company_branch']."'";
                $temp_3 .= " and getTranslate(add_ons,'company_branch','".f_separate."','".v_separate."') = '".$temp_user[0]['company_branch']."'";
            }

            $config_data_2 = array(
                'table_name' => 'sma_users',
                'select_table' => 'sma_users',
                'translate' => '',
                'select_condition' => $temp_3." and group_id = 3",
            );
            $temp_2 = $this->site->api_select_data_v2($config_data_2);

            if ($temp_2[0]['id'] > 0) {
                $condition .= " and (created_by = ".$temp_2[0]['id'];
                for ($i=1;$i<count($temp_2);$i++) {
                    $condition .= " or created_by = ".$temp_2[$i]['id'];
                }
                $condition .= ")";
            }
        }
        return $condition;
    }

    public function api_get_customer_order_condition_v2()
    {
        if ($this->session->userdata('user_id') > 0) {
            $config_data = array(
                'table_name' => 'sma_users',
                'select_table' => 'sma_users',
                'translate' => '',
                'select_condition' => "id = ".$this->session->userdata('user_id'),
            );
            $temp_user = $this->site->api_select_data_v2($config_data);

            $temp = $this->site->api_select_some_fields_with_where(
                "
                *     
                ",
                "sma_companies",
                "id = ".$this->session->userdata('company_id'),
                "arr"
            );
            if ($temp[0]['parent_id'] > 0) {
                $condition = ' and customer_id = '.$temp[0]['id'];
            } else {
                $condition = " and (customer_id = ".$this->session->userdata('company_id');
                $temp = $this->site->api_select_some_fields_with_where(
                    "
                    *     
                    ",
                    "sma_companies",
                    "parent_id = ".$this->session->userdata('company_id'),
                    "arr"
                );
                for ($i=0;$i<count($temp);$i++) {
                    $condition .= ' or customer_id = '.$temp[$i]['id'];
                }
                $condition .= ')';
            }
        }
        return $condition;
    }
    
    public function api_unlink($file_path)
    {
        if (is_file($file_path)) {
            unlink($file_path);
        }
    }

    public function api_calculate_sl_reference_no_by_date($config_data)
    {
        $temp = date_create($config_data['date']);
        $temp_year = date_format($temp, 'Y');
        $temp_month = date_format($temp, 'm');
        $temp_day = date_format($temp, 'd');

        $temp = $this->site->api_select_some_fields_with_where(
            "
            *
            ",
            "sma_sales",
            "reference_no LIKE '%SL-".$temp_year."-".$temp_month."-".$temp_day.":%' order by date asc",
            "arr"
        );

        $temp2 = $this->site->api_select_some_fields_with_where(
            "
            *
            ",
            "sma_sale_summary",
            "reference_no LIKE '%SL-".$temp_year."-".$temp_month."-".$temp_day.":%' order by date asc",
            "arr"
        );
        $temp_m_1 = array();
        $k = 0;
        for ($i=0;$i<count($temp);$i++) {
            $j = $i + 1;
            $temp_m_1[$k]['type'] = 'sma_sales';
            $temp_m_1[$k]['id'] = $temp[$i]['id'];
            $temp_m_1[$k]['date'] = $temp[$i]['date'];
            $temp_m_1[$k]['reference_no'] =  $temp[$i]['reference_no'];
            $k++;
        }

        $temp_m_2 = array();
        $k = 0;
        for ($i=0;$i<count($temp2);$i++) {
            $j = $i + 1;
            $temp_m_2[$k]['type'] = 'sma_sale_summary';
            $temp_m_2[$k]['id'] = $temp2[$i]['id'];
            $temp_m_2[$k]['date'] = $temp2[$i]['date'];
            $temp_m_2[$k]['reference_no'] = $temp2[$i]['reference_no'];
            $k++;
        }

        $array_join_2 = array_merge($temp_m_1, $temp_m_2);

        $sort_by = 'reference_no';
        $array_join_3 = array();
        for ($i=0;$i<count($array_join_2);$i++) {
            foreach (array_keys($array_join_2[$i]) as $key) {
                if ($sort_by == $key) {
                    $array_join_3[$i][$key] = $array_join_2[$i][$key];
                }
            }
            foreach (array_keys($array_join_2[$i]) as $key) {
                if ($sort_by != $key) {
                    $array_join_3[$i][$key] = $array_join_2[$i][$key];
                }
            }
        }
        $result = $array_join_3;
        sort($result);

        $return = 0;
        if (count($result) > 0) {
            $temp6 = count($result) - 1;
            $temp5 = explode(':', $result[$temp6]['reference_no']);
            $return = intval($temp5[1]);
        }
        return $return + 1;
    }

    public function api_calculate_reference_no($config_data)
    {
        $temp = date_create($config_data['date']);
        $temp_year = date_format($temp, 'Y');
        $temp_month = date_format($temp, 'm');
        $temp_day = date_format($temp, 'd');

        $temp = $this->site->api_select_some_fields_with_where(
            "
            *
            ",
            "sma_reference_no",
            "date like '%".$temp_year."%' limit 1",
            "arr"
        );
        if (count($temp) <= 0) {
            $temp3 = array(
                'date' => date('Y-m-d')
            );
            $this->db->insert('sma_reference_no', $temp3);
            $temp = $this->site->api_select_some_fields_with_where(
                "
                *
                ",
                "sma_reference_no",
                "date like '%".date('Y')."%' limit 1",
                "arr"
            );
            $temp_year = date('Y');
        }

        if ($config_data['type'] == 'sale' || $config_data['type'] == 'sale_summary') {
            $config_data2 = array(
                'table_name' => 'sma_companies',
                'select_table' => 'sma_companies',
                'translate' => '',
                'select_condition' => "id = ".$config_data['customer_id'],
            );
            $temp4 = $this->api_select_data_v2($config_data2);

            if ($config_data['sale_reference_type'] == '') {
                if ($temp4[0]['vat_no'] != '' && $temp4[0]['vat_invoice_type'] != 'do') {
                    $temp_field = 'sale_sl';
                    $config_data4 = array(
                        'date' => $config_data['date']
                    );
                    $temp_number = $this->api_calculate_sl_reference_no_by_date($config_data4);
                    $return['reference_no'] = "SL"."-".$temp_year."-".$temp_month."-".$temp_day.":" . sprintf("%04s", $temp_number);
                } else {
                    if ($config_data['type'] != 'sale_summary') {
                        $temp_field = 'sale_do';
                        $return['reference_no'] = "DO"."-".$temp_year."-".$temp_month."-".$temp_day.":" . sprintf("%06s", $temp[0]['sale_do']);
                    } else {
                        $temp_field = 'sale_sl';
                        $config_data4 = array(
                            'date' => $config_data['date']
                        );
                        $temp_number = $this->api_calculate_sl_reference_no_by_date($config_data4);
                        $return['reference_no'] = "SL"."-".$temp_year."-".$temp_month."-".$temp_day.":" . sprintf("%04s", $temp_number);
                    }
                }
            } else {
                $temp_field = 'sale_sl';
                $config_data4 = array(
                    'date' => $config_data['date']
                );
                $temp_number = $this->api_calculate_sl_reference_no_by_date($config_data4);
                $return['reference_no'] = "SL"."-".$temp_year."-".$temp_month."-".$temp_day.":" . sprintf("%04s", $temp_number);
            }
        }
        if ($config_data['type'] == 'quantity_adjustment') {
            $temp_field = 'quantity_adjustment';
            $return['reference_no'] = "QA". "-" . date('Y') . ":" . sprintf("%06s", $temp[0]['quantity_adjustment']);
        }
        if ($config_data['type'] == 'consignment') {
            $temp_field = 'consignment';
            $return['reference_no'] = "CS". "-" . date('Y') . ":" . sprintf("%06s", $temp[0]['consignment']);
        }
        if ($config_data['type'] == 'purchase') {
            $temp_field = 'purchase';
            $return['reference_no'] = "PO". "-" . date('Y') . ":" . sprintf("%06s", $temp[0]['purchase']);
        }
        if ($config_data['type'] == 'expense') {
            $temp_field = 'expense';
            $return['reference_no'] = "EX". "-" . date('Y') . ":" . sprintf("%06s", $temp[0]['expense']);
        }
        if ($config_data['type'] == 'transfer') {
            $temp_field = 'transfer';
            $return['reference_no'] = "TO". "-" . date('Y') . ":" . sprintf("%06s", $temp[0]['transfer']);
        }
        if ($config_data['type'] == 'return') {
            $temp_field = 'return';
            $return['reference_no'] = "RE". "-" . date('Y') . ":" . sprintf("%06s", $temp[0]['return']);
        }
        if ($config_data['type'] == 'payment') {
            $temp_field = 'payment';
            $return['reference_no'] = "PAY". "-" . date('Y') . ":" . sprintf("%06s", $temp[0]['payment']);
        }

        if ($config_data['update'] == 1) {
            $temp5 = array(
                $temp_field => $temp[0][$temp_field] + 1,
            );
            $this->db->update('sma_reference_no', $temp5, 'id = '.$temp[0]['id']);
        }

        return $return;
    }

    public function api_number_format($value, $decimal)
    {
        $temp = floor($value);
        if ($temp != $value) {
            return number_format($value, $decimal);
        } else {
            return number_format($value, 0);
        }
    }

    public function api_get_option_category($config_data){
        $return = array();
        $condition = '';
        if ($config_data['strip_id'] != '')
            $condition .= " and id != ".$config_data['strip_id'];
        
        if ($config_data['condition_parent'] == '')
            $temp = "parent_id = 0";
        else 
            $temp = "id > 0 ".$config_data['condition_parent'];
        
        $config_data_2 = array(
            'table_name' => $config_data['table_name'],
            'select_table' => $config_data['table_name'],
            'translate' => $config_data['translate'],
            'select_condition' => $temp." ".$condition." ".$config_data['condition'],
        );
        $select_data = $this->site->api_select_data_v2($config_data_2);

        $_SESSION['api_temp'] = array();
        if ($config_data['none_label'] != '') {
            $temp = ':{api}:'.$config_data['none_label'];
            array_push($_SESSION['api_temp'], $temp);
        }

        $config_data_2 = $config_data;
        for ($i=0;$i<count($select_data);$i++) {
            $temp = $select_data[$i]['id'].':{api}:'.$select_data[$i][$config_data['field_name']];
            array_push($_SESSION['api_temp'], $temp);
            $config_data_2['level'] = 0;
            $config_data_2['parent_id'] = $select_data[$i]['id'];
            $this->api_get_option_category_sub($config_data_2);
        }
    }
    
    public function api_get_option_category_sub($config_data){
        $condition = '';
        if ($config_data['strip_id'] != '')
            $condition .= " and id != ".$config_data['strip_id'];

        $config_data_2 = array(
            'table_name' => $config_data['table_name'],
            'select_table' => $config_data['table_name'],
            'translate' => $config_data['translate'],
            'select_condition' => "parent_id = ".$config_data['parent_id']." ".$condition." ".$config_data['condition'],
        );
        $select_data = $this->site->api_select_data_v2($config_data_2);

        $config_data_2 = $config_data;
        for ($i=0;$i<count($select_data);$i++) {
            $temp = '';
            for ($i2=1;$i2<=$config_data['level'];$i2++) {
                if ($config_data['no_space'] != 1)
                    $temp .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
            }
            $temp = $temp.$config_data['space'];

            $temp = $select_data[$i]['id'].':{api}:'.$temp.$select_data[$i][$config_data['field_name']];
            array_push($_SESSION['api_temp'], $temp);

            $config_data_2['parent_id'] = $select_data[$i]['id'];
            
            $config_data_2['level'] = $config_data['level'] + 1;
            $this->api_get_option_category_sub($config_data_2);
        }
    }
    
    public function api_get_parent_category($config_data)
    {
        $select_data = $this->site->api_select_some_fields_with_where(
            "
            id,parent_id
            ",
            $config_data['table_name'],
            "id = ".$config_data['id'],
            "arr"
        );
        if ($select_data[0]['parent_id'] == 0) {
            return $select_data[0]['id'];
        } else {
            $config_data_2 = $config_data;
            $config_data_2['id'] = $select_data[0]['parent_id'];
            $this->api_get_parent_category($config_data_2);
        }
    }
    public function api_get_category_arraow($config_data)
    {
        $select_data = $this->api_select_some_fields_with_where(
            "
            *
            ",
            $config_data['table_name'],
            "id = ".$config_data['id'],
            "arr"
        );

        if ($select_data[0]['parent_id'] == 0) {
            array_push($_SESSION['api_temp'], $select_data[0][$config_data['field_name']]);
        } else {
            array_push($_SESSION['api_temp'], $select_data[0][$config_data['field_name']]);
            $config_data_2 = $config_data;
            $config_data_2['id'] = $select_data[0]['parent_id'];
            $this->api_get_category_arraow($config_data_2);
        }
    }
    public function api_get_category_arrow($config_data){
        if ($config_data['translate'] != 'yes') {
            $select_data = $this->api_select_some_fields_with_where("
                *
                "
                ,$config_data['table_name']
                ,"id = ".$config_data['id']
                ,"arr"
            );            
        }
        else {
            $temp = str_replace('title_','',$config_data['field_name']);
            $select_data = $this->api_select_some_fields_with_where("
                *, getTranslate(translate,'".$temp."','".f_separate."','".v_separate."') as ".$config_data['field_name']."
                "
                ,$config_data['table_name']
                ,"id = ".$config_data['id']
                ,"arr"
            );
        }

        if ($config_data['parent_id'] == '')
            $temp = 0;
        else 
            $temp = $config_data['parent_id'];

        if ($select_data[0]['parent_id'] == $temp) {
            array_push($_SESSION['api_temp'],$select_data[0][$config_data['field_name']]);
        }
        else {
            array_push($_SESSION['api_temp'], $select_data[0][$config_data['field_name']]);
            $config_data_2 = $config_data;
            $config_data_2['id'] = $select_data[0]['parent_id'];
            $this->api_get_category_arrow($config_data_2);
        }
    }
    public function api_get_condition_category($config_data)
    {
        $_SESSION['api_temp'] = array();
        $_SESSION['api_temp_2'] = array();
        array_push($_SESSION['api_temp'], $config_data['id']);          
        $this->api_get_condition_category_sub($config_data);

        if (count($_SESSION['api_temp']) > 0) {
            $condition .= " and (".$config_data['condition_id_name']." = ".$_SESSION['api_temp'][0];
            for ($i=0;$i<count($_SESSION['api_temp']);$i++) {
                $condition .= " or ".$config_data['condition_id_name']." = ".$_SESSION['api_temp'][$i];
            }
            $condition .= ")";
        } else {
            $condition .= " and ".$config_data['condition_id_name']." = ".$config_data['id'];
        }
        return $condition;
    }
    public function api_get_condition_category_sub($config_data)
    {
        $temp = $this->api_select_some_fields_with_where(
            "
            id
            ",
            $config_data['table_name'],
            "parent_id = ".$config_data['id'],
            "arr"
        );
        for ($i=0;$i<count($temp);$i++) {
            array_push($_SESSION['api_temp'], $temp[$i]['id']);
            $config_data_2 = $config_data;
            $config_data_2['id'] = $temp[$i]['id'];
            $this->api_get_condition_category_sub($config_data_2);
        }
    }

    public function api_update_sale_consignment_status($id)
    {
        $temp5 = $this->api_select_some_fields_with_where(
            "
            *
            ",
            "sma_consignment",
            "id = ".$id,
            "arr"
        );
        $temp = $this->api_select_some_fields_with_where(
            "
            *
            ",
            "sma_consignment_items",
            "sale_id = ".$id,
            "arr"
        );
        $temp2 = $this->api_select_some_fields_with_where(
            "
            *
            ",
            "sma_consignment_sale_items",
            "consignment_id = ".$id,
            "arr"
        );
        $temp4 = $this->api_select_some_fields_with_where(
            "
            *
            ",
            "sma_returns",
            "add_ons like '%:cs_reference_no:{".$temp5[0]['reference_no']."}:%'",
            "arr"
        );

        $b = 0;
        for ($i=0;$i<count($temp);$i++) {
            if ($temp[$i]['option_id'] > 0) {
                $condition = " and option_id = ".$temp[$i]['option_id'];
            } else {
                $condition = '';
            }
            $temp3 = 0;
            for ($i2=0;$i2<count($temp2);$i2++) {
                if ($temp[$i]['product_id'] == $temp2[$i2]['product_id'] && $temp[$i]['option_id'] == $temp2[$i2]['option_id']) {
                    $temp3 = $temp3 + $temp2[$i2]['quantity'];
                }
            }
            for ($i3=0;$i3<count($temp4);$i3++) {
                $temp6 = $this->api_select_some_fields_with_where(
                    "
                    *
                    ",
                    "sma_return_items",
                    "return_id = ".$temp4[$i3]['id']." and product_id = ".$temp[$i]['product_id']." ".$condition,
                    "arr"
                );
                for ($i4=0;$i4<count($temp6);$i4++) {
                    $temp3 = $temp3 + $temp6[$i4]['quantity'];
                }
            }
            if ($temp3 != $temp[$i]['quantity']) {
                $b = 1;
                break;
            }
        }

        if ($b == 0) {
            $temp = array(
                'sale_status' => 'completed'
            );
        } else {
            $temp = array(
                'sale_status' => 'pending'
            );
        }
        $this->db->update('sma_consignment', $temp, 'id = '.$id);
    }

    public function api_calculate_consignment_sale_wrapper($config_data)
    {
        $config_data_2 = array(
            'table_name' => 'sma_companies',
            'select_table' => 'sma_companies',
            'translate' => '',
            'select_condition' => "id = ".$config_data['customer_id'],
        );
        $temp_customer = $this->api_select_data_v2($config_data_2);
        
        if ($temp_customer[0]['sale_consignment_auto_insert'] == 'yes') {
            $config_data_2 = array(
                'table_name' => 'sma_consignment',
                'select_table' => 'sma_consignment',
                'translate' => '',
                'select_condition' => "sale_status != 'completed' and customer_id = ".$config_data['customer_id']." order by date asc",
            );
            $temp_purchase = $this->api_select_data_v2($config_data_2);
            $temp_sale_item = $this->api_select_some_fields_with_where(
                "
                *
                ",
                "sma_sale_items",
                "sale_id = ".$config_data['sale_id'],
                "arr"
            );

            for ($i=0;$i<count($temp_sale_item);$i++) {
                if ($temp_sale_item[$i]['option_id'] > 0) {
                    $condition = " and option_id = ".$temp_sale_item[$i]['option_id'];
                } else {
                    $condition = '';
                }

                $temp_qty = $temp_sale_item[$i]['quantity'];

                $config_data_2 = array(
                    'sale_id' => $temp_sale_item[$i]['sale_id'],
                    'product_id' => $temp_sale_item[$i]['product_id'],
                    'option_id' =>$temp_sale_item[$i]['option_id'],
                    'quantity' => $temp_qty,
                );
                $temp = $this->api_calculate_consignment_sale($config_data_2, $temp_purchase);

                while ($temp > 0) {
                    $config_data_2 = array(
                        'sale_id' => $temp_sale_item[$i]['sale_id'],
                        'product_id' => $temp_sale_item[$i]['product_id'],
                        'option_id' => $temp_sale_item[$i]['option_id'],
                        'quantity' => $temp,
                    );
                    $temp = $this->api_calculate_consignment_sale($config_data_2, $temp_purchase);
                }
            }
        }
    }

    public function api_calculate_consignment_sale($config_data, $temp_purchase)
    {
        $temp_remain_qty = 0;
        for ($i2=0;$i2<count($temp_purchase);$i2++) {
            $config_data_2 = array(
                'table_name' => 'sma_consignment_items',
                'select_table' => 'sma_consignment_items',
                'translate' => '',
                'select_condition' => "sale_id = ".$temp_purchase[$i2]['id'],
            );
            $temp_purchase_item = $this->api_select_data_v2($config_data_2);
            for ($i3=0;$i3<count($temp_purchase_item);$i3++) {
                if ($config_data['option_id'] > 0) {
                    $condition = " and option_id = ".$config_data['option_id'];
                } else {
                    $condition = '';
                }

                if ($temp_purchase_item[$i3]['product_id'] == $config_data['product_id']) {
                    $temp_5 = $this->api_select_some_fields_with_where(
                        "
                        quantity
                        ",
                        "sma_consignment_sale_items",
                        "consignment_id = ".$temp_purchase[$i2]['id']." and product_id = ".$config_data['product_id']." ".$condition,
                        "arr"
                    );
                    $temp_sold_quantity = 0;
                    for ($i5=0;$i5<count($temp_5);$i5++) {
                        $temp_sold_quantity = $temp_sold_quantity + $temp_5[$i5]['quantity'];
                    }

                    $temp_return = $this->api_select_some_fields_with_where(
                        "
                        id
                        ",
                        "sma_returns",
                        "add_ons like '%:cs_reference_no:{".$temp_purchase[$i2]['reference_no']."}:%'",
                        "arr"
                    );
                    $temp_5 = 0;
                    for ($i_return=0;$i_return<count($temp_return);$i_return++) {
                        $temp_return_item = $this->api_select_some_fields_with_where(
                            "
                            quantity
                            ",
                            "sma_return_items",
                            "return_id = '".$temp_return[$i_return]['id']."' and product_id = ".$config_data['product_id']." ".$condition,
                            "arr"
                        );
                        for ($i_return_item=0;$i_return_item<count($temp_return_item);$i_return_item++) {
                            $temp_5 = $temp_5 + $temp_return_item[$i_return_item]['quantity'];
                        }
                    }


                    $temp_sold_quantity = $temp_sold_quantity + $temp_5;
                    if ($temp_sold_quantity != $temp_purchase_item[$i3]['quantity']) {
                        $temp = $temp_sold_quantity + $config_data['quantity'];
                        if ($temp > $temp_purchase_item[$i3]['quantity']) {
                            $temp_remain_qty = $temp - $temp_purchase_item[$i3]['quantity'];
                            $temp = $temp_purchase_item[$i3]['quantity'];
                            $temp_set_quantity = $config_data['quantity'] - $temp_remain_qty;
                        } else {
                            $temp_set_quantity = $config_data['quantity'];
                        }

                        $config_data_2 = array(
                            'table_name' => 'sma_consignment_sale_items',
                            'select_table' => 'sma_consignment_sale_items',
                            'translate' => '',
                            'select_condition' => "sale_id = ".$config_data['sale_id']." and consignment_id = ".$temp_purchase[$i2]['id']." and product_id = ".$config_data['product_id']." ".$condition,
                        );
                        $temp_consignment_purchase_items = $this->api_select_data_v2($config_data_2);
                        if ($temp_consignment_purchase_items[0]['id'] > 0) {
                            $temp_4 = $temp_consignment_purchase_items[0]['quantity'] + $temp_set_quantity;
                            $config_data_2 = array(
                                'quantity' => $temp_4,
                                'date' => date('Y-m-d H:i:s'),
                            );
                            $this->db->update('sma_consignment_sale_items', $config_data_2, 'id = '.$temp_consignment_purchase_items[0]['id']);
                        } else {
                            $config_data_2 = array(
                                'sale_id' => $config_data['sale_id'],
                                'consignment_id' => $temp_purchase[$i2]['id'],
                                'product_id' => $config_data['product_id'],
                                'option_id' => $config_data['option_id'],
                                'quantity' => $temp_set_quantity,
                                'date' => date('Y-m-d H:i:s'),
                            );
                            $this->db->insert('sma_consignment_sale_items', $config_data_2);
                        }
                        $this->api_update_sale_consignment_status($temp_purchase[$i2]['id']);

                        return $temp_remain_qty;
                    }
                }
            }
        }
        return $temp_remain_qty;
    }

    public function api_get_consignment_sale_items($id)
    {
        $select_data = $this->site->api_select_some_fields_with_where(
            "
        distinct consignment_id",
            "sma_consignment_sale_items",
            "sale_id = ".$id,
            "arr"
        );
        return $select_data;
    }
    
    public function api_generate_password($length = 9, $add_dashes = false, $available_sets = 'luds')
    {
        $sets = array();
        if (strpos($available_sets, 'l') !== false) {
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        }
        if (strpos($available_sets, 'u') !== false) {
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        }
        if (strpos($available_sets, 'd') !== false) {
            $sets[] = '23456789';
        }
        if (strpos($available_sets, 's') !== false) {
            $sets[] = '!@#$%&*?';
        }
    
        $all = '';
        $password = '';
        foreach ($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
    
        $all = str_split($all);
        for ($i = 0; $i < $length - count($sets); $i++) {
            $password .= $all[array_rand($all)];
        }
    
        $password = str_shuffle($password);
    
        if (!$add_dashes) {
            return $password;
        }
    
        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while (strlen($password) > $dash_len) {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $dash_str .= $password;
        return $dash_str;
    }

    public function api_display_tax_rate($config_data)
    {
        $temp_tax_rate = $this->site->api_select_some_fields_with_where(
            "*",
            "sma_tax_rates",
            "id > 0 order by id asc",
            "arr"
        );
        if ($config_data['id'] == 0) {
            $config_data['id'] = 1;
        }
        $temp = '';
        for ($i=0;$i<count($temp_tax_rate);$i++) {
            if ($temp_tax_rate[$i]['id'] == $config_data['id']) {
                $temp = $temp_tax_rate[$i]['rate'];
            }
        }
        if ($temp != 0) {
            $temp = ' '.number_format($temp, 0).'%';
        } else {
            $temp = '';
        }

        return $temp;
    }

    public function api_kata_tip_display($promotion_products, $config_data, $api_view_array)
    {
        $temp_display .= '
            <ul class="slider" id="'.$config_data['id'].'">
        ';
        $r = 0;
        foreach (array_chunk($promotion_products, $config_data['col_num']) as $fps) {
            $temp_display .= '
                <li>
                <figure>
            ';
            $k = 1;
            foreach ($fps as $fp) {
                $config_data_2 = array(
                    'table_name' => 'sma_products',
                    'select_table' => 'sma_products',
                    'translate' => 'yes',
                    'select_condition' => "id = ".$fp->id,
                );
                $temp = $this->site->api_select_data_v2($config_data_2);
                $fp->name = $temp[0]['title_'.$this->api_shop_setting[0]['api_lang_key']];

                if (($k%2) != 0) {
                    $class_record = $api_view_array['col_class_odd'];
                } else {
                    $class_record = $api_view_array['col_class_even'];
                }
                    
                $temp_sold_out = array();
                $temp_sold_out[3] = '<option value="1">1</option>';
                $temp_sold_out[5] = '';
                $temp_sold_out[6] = 'api_link_box_none';
                $temp_sold_out[7] = '';

                if ($this->api_shop_setting[0]['out_of_stock'] == 1) {
                    if ($fp->quantity <= 0) {
                        $temp_sold_out[0] = 'api_opacity_6';
                        $temp_sold_out[1] = '
                            <div class="api_absolute_center">
                                <div class="api_sold_out_tag" style="visibility:hidden;">
                                    Sold Out
                                </div>
                            </div>      
                        ';
                        $temp_sold_out[2] = 'disabled';
                        $temp_sold_out[3] = '<option value="0">0</option>';
                        $temp_sold_out[4] = 'api_display_none';
                        $temp_sold_out[5] = $api_view_array['sorry'];
                        $temp_sold_out[6] = '';
                        $temp_sold_out[7] = 'background-color:#808080; border-color:#808080;';
                    }
                }
    
                if (is_file('assets/uploads/'.$fp->image)) {
                    $temp_img = $fp->image;
                } else {
                    $temp_img = 'no_image.png';
                }
    
                $data_view = array(
                    'wrapper_class' => '',
                    'file_path' => 'assets/uploads/thumbs/'.$temp_img,
                    'max_width' => 190,
                    'max_height' => 190,
                    'product_link' => 'true',
                    'image_class' => $temp_sold_out[6].' img-responsive '.$temp_sold_out[0],
                    'image_id' => '',
                    'resize_type' => 'full',
                    'table_class' => 'api_kata_tip_table_img',
                    'image_title' => $fp->name,
                );
                $temp = $this->site->api_get_image($data_view);
                $temp_display .= '
                    <div class="product-top product-bottom '.$config_data['col_class'].' api_padding_left_3 api_padding_right_0 api_padding_top_5 api_padding_bottom_5">
                        <div class="product product-image api_kata_tip_box" style="">
                            <a href="'.site_url('product/'.$fp->slug).'">
                                '.$temp['image_table'].'
                                '.$temp_sold_out[1].'
                            </a>
                            <a href="'.site_url('product/'.$fp->slug).'">
                                <div class="api_padding_left_10 api_padding_right_10 api_padding_bottom_0">
                                    <div class="api_title_line_2 api_title_truncate_line_2 text-red api_bold_im api_link_box_none">
                                        <span>
                                            '.$fp->name.'
                                        </span>
                                    </div>
                                </div>
                            </a>';
                if (!$shop_settings->hide_price) {
                    $temp_display .= '
                                <div class="product-price '.$temp_sold_out[4].'" style="'.$api_view_array['price_style'].' width: 100% !important;">
                                ';
                    $config_data_2 = array(
                                        'id' => $fp->id,
                                        'customer_id' => $this->session->userdata('company_id'),
                                    );
                    $temp = $this->site->api_calculate_product_price($config_data_2);
                                           
                    if ($temp['promotion']['price'] > 0) {
if ($fp->quantity < $temp['promotion']['min_qty']) {
    $temp_stock = lang('Out_of_Stock');
    $temp_class = 'danger';
}
else {
    $temp_stock = lang('In_Stock').' '.number_format($fp->quantity,0);
    $temp_class = 'success';
}
$temp_display .= '
<del class="col-md-12 api_padding_left_0 api_padding_right_0 text-red">
    '.$this->sma->convertMoney($temp['temp_price']).'
    <div class="api_float_right api_font_size_14 api_font_weight_normal api_display_none">                            
        <div class="label label-'.$temp_class.' api_border_r10">
            '.$temp_stock.'
        </div>
    </div>    
</del>
<div class="col-md-6 api_padding_left_0 api_padding_right_0 api_padding_bottom_5">
    '.$this->sma->convertMoney($temp['price']).'
</div>
<div class="col-md-6 api_padding_left_0 api_padding_right_0 pull-right api_padding_bottom_5">
    <div class="api_float_right api_font_size_14 api_font_weight_normal">                            
        <div class="label label-info api_border_r10">
            '.$temp['promotion']['duration_left'].'
        </div>
    </div>
</div>                                            
';
                    }
                    else {
                        $temp_display .= '
                            <del class="text-red api_visibility_hidden">
                                '.$this->sma->convertMoney($temp['price']).'
                            </del>
                            <div class="api_padding_bottom_5">
                                '.$this->sma->convertMoney($temp['price']).'
                            </div>
                        ';
                    }
                    $temp_display .= '
                                </div>
                                ';
                }
                if ($temp['promotion']['price'] > 0) {
                    $temp_qty = $temp['promotion']['min_qty'];
                }
                else
                    $temp_qty = 1;
                $temp_display .= '
                            '.$temp_sold_out[5].'
                            <div class="api_padding_left_5 api_padding_right_5" style="'.$api_view_array['select_qty_style'].'" style="width: 100% !important;">
                                <div class="form-group" style="margin-bottom:5px;">
                                    <div class="input-group">
                                        <span class="input-group-addon pointer btn-minus api_pause_carousel"><span class="fa fa-minus"></span></span>
                                        <input type="tel" name="quantity" id="api_kata_tip_display_product_quantity_'.$config_data['id'].'_'.$fp->id.'" onmouseout="sma_input_qty_mouse_out(\'api_kata_tip_display_product_quantity_'.$config_data['id'].'_'.$fp->id.'\','.$temp_qty.');" class="form-control text-center api_numberic_input quantity-input api_pause_carousel" value="'.$temp_qty.'" min_qty="'.$temp_qty.'" required="required">
                                        <span class="input-group-addon pointer btn-plus api_pause_carousel"><span class="fa fa-plus"></span></span>
                                    </div>
                                </div>        
                            </div>
                            <div class="clearfix"></div>
                            ';

                if ($fp->is_favorite == 1) {
                    $temp_is_favorite = 'fa-heart';
                } else {
                    $temp_is_favorite = 'fa-heart-o';
                }
                $temp_display .= '        
                            <div class="product-cart-button api_padding_left_5 api_padding_right_5 '.$api_view_array['btn_class'].'">
                                <div class="btn-group" role="group" aria-label="...">

                                    <button class="api_float_left btn btn-info add-to-wishlist" data-id="'.$fp->id.'"><i id="category_wishlist_heart_'.$fp->id.'" class="fa '.$temp_is_favorite.'"></i>
                                    </button>

                            ';
                    
                $temp_display_1 = '';
                if ($fp->is_ordered != 1) {
                    if ($this->session->userdata('user_id')) {
                        $temp_display_1 = 'api_display_none';
                        $temp_display .= '
                                        <button class="api_float_left btn btn-theme " '.$temp_sold_out[2].' data-toggle="modal" data-target="#api_modal_add_cart" onclick="api_add_cart_confirm(\'product_featured_'.$fp->id.'\');" style="width:100%; '.$temp_sold_out[7].'"><i class="fa fa-shopping-cart padding-right-md"></i> '.$api_view_array['label_btn'].'</button>
                                    ';
                    }
                }
                $temp_display .= '
                                    <button class="api_float_left btn btn-theme add-to-cart '.$temp_display_1.'" '.$temp_sold_out[2].' data-id="'.$fp->id.'" id="product_featured_'.$fp->id.'" style="width:100%; '.$temp_sold_out[7].'"><i class="fa fa-shopping-cart padding-right-md"></i> '.$api_view_array['label_btn'].'</button>

                                </div>
                            </div>
                            <div class="api_height_5 api_clear_both"></div>
                            ';

                $temp_display .= '
                        </div>
                ';
                $temp_display .= '
                    </div>
                ';
            }
            $temp_display .= '
                </li>
                </figure>
            ';
        }
        $temp_display .= '
            </ul>
        ';

        return $temp_display;
    }

    public function api_ebay_carousel_display($promotion_products, $config_data, $api_view_array)
    {
        $temp_display .= '
            <div class="hl-module hl-standard-carousel off-card hl-atf-module-js api_margin_bottom_15_im" id="'.$config_data['id'].'">
            <div>
                <div class="carousel hl-carousel hl-carousel__river hl-carousel__mobile carousel--slides carousel--peek"
                    data-ebayui="">
                    <div class="carousel__container" data-marko="{}">
                        <ul class="carousel__list">            
        ';
        $r = 0;
        foreach (array_chunk($promotion_products, 1) as $fps) {
                        
            $temp_display .= '
                <li class="carousel__snap-point hl-carousel__item hl-standard-carousel__item"
                style="width:calc(40% - 4.8px);margin-right:3px;padding-right:0px !important">
            ';
            $k = 1;
            foreach ($fps as $fp_2) {
                foreach ($fp_2 as $key => $value) {
                    $fp->{$key} = $value;
                }
                $config_data_2 = array(
                    'table_name' => 'sma_products',
                    'select_table' => 'sma_products',
                    'translate' => 'yes',
                    'select_condition' => "id = ".$fp->id,
                );
                $temp = $this->site->api_select_data_v2($config_data_2);
                $fp->name = $temp[0]['title_'.$this->api_shop_setting[0]['api_lang_key']];
                                
                if (($k%2) != 0) {
                    $class_record = $api_view_array['col_class_odd'];
                } else {
                    $class_record = $api_view_array['col_class_even'];
                }
                    
                $temp_sold_out = array();
                $temp_sold_out[3] = '<option value="1">1</option>';
                $temp_sold_out[5] = '';
                $temp_sold_out[6] = 'api_link_box_none';
                $temp_sold_out[7] = '';

                if ($this->api_shop_setting[0]['out_of_stock'] == 1) {
                    if ($fp->quantity <= 0) {
                        $temp_sold_out[0] = 'api_opacity_6';
                        $temp_sold_out[1] = '
                            <div class="api_absolute_center">
                                <div class="api_sold_out_tag" style="visibility:hidden;">
                                    Sold Out
                                </div>
                            </div>      
                        ';
                        $temp_sold_out[2] = 'disabled';
                        $temp_sold_out[3] = '<option value="0">0</option>';
                        $temp_sold_out[4] = 'api_display_none';
                        $temp_sold_out[5] = $api_view_array['sorry'];
                        $temp_sold_out[6] = '';
                        $temp_sold_out[7] = 'background-color:#808080; border-color:#808080;';
                    }
                }
    
                if (is_file('assets/uploads/'.$fp->image)) {
                    $temp_img = $fp->image;
                } else {
                    $temp_img = 'no_image.png';
                }
    
                $data_view = array(
                    'wrapper_class' => '',
                    'file_path' => 'assets/uploads/thumbs/'.$temp_img,
                    'max_width' => 130,
                    'max_height' => 130,
                    'product_link' => 'true',
                    'image_class' => $temp_sold_out[6].' api_padding_5 '.$temp_sold_out[0],
                    'image_id' => '',
                    'resize_type' => 'full',
                    'table_class' => 'api_kata_tip_table_img',
                    'image_title' => $fp->name,
                );
                $temp = $this->site->api_get_image($data_view);
                $temp_display .= '
                    <div class="product-top product-bottom '.$config_data['col_class'].' ">
                        <div class="product product-image api_ebay_box" style="">
                            <a href="'.site_url('product/'.$fp->slug).'">
                                    <table class="'.$data_view['table_class'].'" width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                    <td valign="middle" align="center" height="'.$data_view['table_height'].'">                            
                                        <div class="hl-image hl-image-js js-only hl-item__bg-image">
                                            <img class="'.$data_view['image_class'].'" src="'.$temp['image_full_src'].'" crossorigin="anonymous" alt="'.$fp->name.'" data-size="130" data-load-time="1588645568211" style="opacity: 1;">
                                        </div>         
                                    </td>
                                    </tr>
                                    </table>                                                      
                                '.$temp_sold_out[1].'
                            </a>
                            <a href="'.site_url('product/'.$fp->slug).'">
                                <div class="api_padding_left_5 api_padding_right_5 api_padding_bottom_0 '.$api_view_array['api_ebay_title_mobile'].'">
                                    <div class="'.$api_view_array['api_ebay_title_pc'].' text-red api_bold_im api_link_box_none" style="apipe" >
                                        <span>
                                            '.$fp->name.'
                                        </span>
                                    </div>
                                </div>
                            </a>                            
                ';

                if (!$shop_settings->hide_price) {
                    $temp_display .= '
                                <div class="product-price '.$temp_sold_out[4].'" style="'.$api_view_array['price_style'].' width: 100% !important;">
                                ';
                    $config_data_2 = array(
                                        'id' => $fp->id,
                                        'customer_id' => $this->session->userdata('company_id'),
                                    );
                    $temp = $this->site->api_calculate_product_price($config_data_2);
                    
                                 
                    if ($temp['promotion']['price'] > 0) {
if ($fp->quantity < $temp['promotion']['min_qty']) {
    $temp_stock = lang('Out_of_Stock');
    $temp_class = 'danger';
}
else {
    $temp_stock = lang('In_Stock').' '.number_format($fp->quantity,0);
    $temp_class = 'success';
}
$temp_display .= '
<del class="col-md-12 api_padding_left_0 api_padding_right_0 text-red">
    '.$this->sma->convertMoney($temp['temp_price']).'
    <div class="api_float_right api_font_weight_normal api_display_none">                            
        <div class="label label-'.$temp_class.' api_border_r10 api_font_size_10" style="height:16px;">
            '.$temp_stock.'
        </div>
    </div>    
</del>
<div class="api_float_left api_padding_left_0 api_padding_right_0 api_padding_bottom_5">
    '.$this->sma->convertMoney($temp['price']).'
</div>
<div class="api_float_right api_padding_left_0 api_padding_right_0 pull-right api_padding_bottom_5">
    <div class="api_float_right api_font_weight_normal">                            
        <div class="label label-info api_border_r10 api_font_size_10" style="height:16px;">
            '.$temp['promotion']['duration_left'].'
        </div>
    </div>
</div>                                            
';
                    }
                    else {
                        $temp_display .= '
                            <del class="text-red api_visibility_hidden">
                                '.$this->sma->convertMoney($temp['price']).'
                            </del>
                            <div class="api_padding_bottom_5">
                                '.$this->sma->convertMoney($temp['price']).'
                            </div>
                        ';
                    }
                    
                    $temp_display .= '
                                </div>
                                ';
                }
                if ($temp['promotion']['price'] > 0) {
                    $temp_qty = $temp['promotion']['min_qty'];
                }
                else
                    $temp_qty = 1;
                $temp_display .= '
                            '.$temp_sold_out[5].'
                            <div class="api_padding_left_5 api_padding_right_5" style="'.$api_view_array['select_qty_style'].'" style="width: 100% !important;">
                                <div class="form-group" style="margin-bottom:5px;">
                                    <div class="input-group">                
                                        <span class="input-group-addon pointer btn-minus api_pause_carousel"><span class="fa fa-minus"></span></span>
                                        <input type="tel" name="quantity" id="api_kata_tip_display_product_quantity_'.$config_data['id'].'_'.$fp->id.'" onmouseout="sma_input_qty_mouse_out(\'api_kata_tip_display_product_quantity_'.$config_data['id'].'_'.$fp->id.'\','.$temp_qty.');" class="form-control text-center api_numberic_input quantity-input api_pause_carousel" value="'.$temp_qty.'" min_qty="'.$temp_qty.'" required="required">
                                        <span class="input-group-addon pointer btn-plus api_pause_carousel"><span class="fa fa-plus"></span></span>
                                    </div>
                                </div>        
                            </div>
                            <div class="clearfix"></div>
                            ';

                if ($fp->is_favorite == 1) {
                    $temp_is_favorite = 'fa-heart';
                } else {
                    $temp_is_favorite = 'fa-heart-o';
                }
                $temp_display .= '        
                            <div class="product-cart-button api_padding_left_5 api_padding_right_5 '.$api_view_array['btn_class'].'">
                                <div class="btn-group" role="group" aria-label="...">

                                    <button class="api_float_left btn btn-info add-to-wishlist" data-id="'.$fp->id.'"><i id="category_wishlist_heart_'.$fp->id.'" class="fa '.$temp_is_favorite.'"></i>
                                    </button>

                            ';
                    
                $temp_display_1 = '';
                if ($fp->is_ordered != 1) {
                    if ($this->session->userdata('user_id')) {
                        $temp_display_1 = 'api_display_none';
                        $temp_display .= '
                                        <button class="api_float_left btn btn-theme " '.$temp_sold_out[2].' data-toggle="modal" data-target="#api_modal_add_cart" onclick="api_add_cart_confirm(\'product_featured_'.$fp->id.'\');" style="width:100%; '.$temp_sold_out[7].'"><i class="fa fa-shopping-cart padding-right-md"></i> '.$api_view_array['label_btn'].'</button>
                                    ';
                    }
                }
                $temp_display .= '
                                    <button class="api_float_left btn btn-theme add-to-cart '.$temp_display_1.'" '.$temp_sold_out[2].' data-id="'.$fp->id.'" id="product_featured_'.$fp->id.'" style="width:100%; '.$temp_sold_out[7].'"><i class="fa fa-shopping-cart padding-right-md"></i> '.$api_view_array['label_btn'].'</button>

                                </div>
                            </div>
                            <div class="api_height_5 api_clear_both"></div>
                            ';

                $temp_display .= '
                        </div>
                ';
                $temp_display .= '
                    </div>
                ';
            }
            $temp_display .= '
                </li>
            ';
        }
        $temp_display .= '
                    </ul>
                    <button class="carousel__control carousel__control--next" type="button" data-direction="1"
                        aria-describedby="carousel-status-s0-0-3_1-0-2[1]-4-match-media-0-ebay-carousel"
                        aria-label="Go to next slide - Daily Deals" data-marko="{}"><svg
                            class="icon icon--chevron-right-small" xmlns="http://www.w3.org/2000/svg"
                            focusable="false" aria-hidden="true">
                            <use xlink:href="#icon-chevron-right-small"></use>
                        </svg>
                    </button>
                </div>
            </div>
        </div>
    </div>
        ';

        return $temp_display;
    }
    public function build_slide($promotion_products, $config_data, $api_view_array)
    {
        $temp_display = '
        <a target-slide="'.$config_data['id'].'" class="click-'.$config_data['id'].' slick-prev slick-arrow control_prev_cat position-fixed"><span class="fa fa-chevron-left api_margin_top_5"></span></a>
        <a target-slide="'.$config_data['id'].'" class="click-'.$config_data['id'].' slick-next slick-arrow control_next_cat position-fixed"><span class="fa fa-chevron-right api_margin_top_5"></span></a>
        <div id="'.$config_data['id'].'"><ul>';
        
        foreach (array_chunk($promotion_products, $config_data['col_num']) as $fps) {

            foreach ($fps as $fp_2) {                
                foreach ($fp_2 as $key => $value) {
                    $fp->{$key} = $value;
                }
                $temp_sold_out = array();
                $temp_sold_out[3] = '<option value="1">1</option>';
                $temp_sold_out[5] = '';
                $temp_sold_out[6] = 'api_link_box_none';
                $temp_sold_out[7] = '';
                if ($this->api_shop_setting[0]['out_of_stock'] == 1) {
                    if ($fp->quantity <= 0) {
                        $temp_sold_out[0] = 'api_opacity_6';
                        $temp_sold_out[1] = '
                            <div class="api_absolute_center">
                                <div class="api_sold_out_tag" style="visibility:hidden;">
                                    Sold Out
                                </div>
                            </div>      
                        ';
                        $temp_sold_out[2] = 'disabled';
                        $temp_sold_out[3] = '<option value="0">0</option>';
                        $temp_sold_out[4] = 'api_display_none';
                        $temp_sold_out[5] = $api_view_array['sorry'];
                        $temp_sold_out[6] = '';
                        $temp_sold_out[7] = 'background-color:#808080; border-color:#808080;';
                    }
                }
    
                if (is_file('assets/uploads/'.$fp->image)) {
                    $temp_img = $fp->image;
                } else {
                    $temp_img = 'no_image.png';
                }
    
                $data_view = array(
                    'wrapper_class' => '',
                    'file_path' => 'assets/uploads/thumbs/'.$temp_img,
                    'max_width' => 190,
                    'max_height' => 190,
                    'product_link' => 'true',
                    'image_class' => $temp_sold_out[6].' img-responsive '.$temp_sold_out[0],
                    'image_id' => '',
                    'resize_type' => 'full',
                    'table_class' => 'api_kata_tip_table_img',
                    'image_title' => $fp->name,
                );
                $temp = $this->site->api_get_image($data_view);
                $temp_display .= '
                        <li class="product-top product-bottom" style="width:200px">
                        <div class="product product-image api_kata_tip_box" style="">
                            <a href="'.site_url('product/'.$fp->slug).'">
                                '.$temp['image_table'].''.$temp_sold_out[1].'
                            </a>
                            <a href="'.site_url('product/'.$fp->slug).'">
                                <div class="api_padding_left_10 api_padding_right_10 api_padding_bottom_0">
                                    <div class="api_title_line_2 api_title_truncate_line_2 text-red api_bold_im api_link_box_none">
                                        <span>
                                            '.$fp->{'title_'.$this->api_shop_setting[0]['api_lang_key']}.'
                                        </span>
                                    </div>
                                </div>
                            </a>';
                if (!$shop_settings->hide_price) {
                    $temp_display .= '
                                <div class="product-price '.$temp_sold_out[4].'" style="'.$api_view_array['price_style'].' width: 100% !important;">';
                    $config_data_2 = array(
                                        'id' => $fp->id,
                                        'customer_id' => $this->session->userdata('company_id'),
                                    );
                    $temp = $this->site->api_calculate_product_price($config_data_2);
                    if ($temp['promotion']['price'] > 0) {
if ($fp->quantity < $temp['promotion']['min_qty']) {
    $temp_stock = lang('Out_of_Stock');
    $temp_class = 'danger';
}
else {
    $temp_stock = lang('In_Stock').' '.number_format($fp->quantity,0);
    $temp_class = 'success';
}
$temp_display .= '
<del class="col-md-12 api_padding_left_0 api_padding_right_0 text-red">
    '.$this->sma->convertMoney($temp['temp_price']).'
    <div class="api_float_right api_font_size_14 api_font_weight_normal api_display_none">                            
        <div class="label label-'.$temp_class.' api_border_r10">
            '.$temp_stock.'
        </div>
    </div>    
</del>
<div class="col-md-6 api_padding_left_0 api_padding_right_0 api_padding_bottom_5">
    '.$this->sma->convertMoney($temp['price']).'
</div>
<div class="col-md-6 api_padding_left_0 api_padding_right_0 pull-right api_padding_bottom_5">
    <div class="api_float_right api_font_size_14 api_font_weight_normal">                            
        <div class="label label-info api_border_r10">
            '.$temp['promotion']['duration_left'].'
        </div>
    </div>
</div>                                            
';
                    } else {
                        $temp_display .= '
                                            <del class="text-red api_visibility_hidden">'.$this->sma->convertMoney($temp['price']).'</del>
                                            <div class="api_padding_bottom_5">'.$this->sma->convertMoney($temp['price']).'</div>';
                    }
                    $temp_display .= '</div>';
                }

                if ($temp['promotion']['price'] > 0) {
                    $temp_qty = $temp['promotion']['min_qty'];
                }
                else
                    $temp_qty = 1;                
                $temp_display .= $temp_sold_out[5].'
                            <div class="api_padding_left_5 api_padding_right_5" style="'.$api_view_array['select_qty_style'].'" style="width: 100% !important;">
                                <div class="form-group" style="margin-bottom:5px;">
                                    <div class="input-group">                
                                        <span class="input-group-addon pointer btn-minus api_pause_carousel"><span class="fa fa-minus"></span></span>
                                        <input type="tel" name="quantity" id="api_kata_tip_display_product_quantity_'.$config_data['id'].'_'.$fp->id.'" onmouseout="sma_input_qty_mouse_out(\'api_kata_tip_display_product_quantity_'.$config_data['id'].'_'.$fp->id.'\','.$temp_qty.');" class="form-control text-center api_numberic_input quantity-input api_pause_carousel" value="'.$temp_qty.'" min_qty="'.$temp_qty.'" required="required">
                                        <span class="input-group-addon pointer btn-plus api_pause_carousel"><span class="fa fa-plus"></span></span>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>';

                if ($fp->is_favorite == 1) {
                    $temp_is_favorite = 'fa-heart';
                } else {
                    $temp_is_favorite = 'fa-heart-o';
                }
                $temp_display .= '        
                            <div class="product-cart-button api_padding_left_5 api_padding_right_5 '.$api_view_array['btn_class'].'">
                                <div class="btn-group" role="group" aria-label="...">
                                <button class="api_float_left btn btn-info add-to-wishlist" data-id="'.$fp->id.'"><i id="category_wishlist_heart_'.$fp->id.'" class="fa '.$temp_is_favorite.'"></i></button>';
                $temp_display_1 = '';
                if ($fp->is_ordered != 1) {
                    if ($this->session->userdata('user_id')) {
                        $temp_display_1 = 'api_display_none';
                        $temp_display .= '<button class="api_float_left btn btn-theme " '.$temp_sold_out[2].' data-toggle="modal" data-target="#api_modal_add_cart" onclick="api_add_cart_confirm(\'product_featured_'.$fp->id.'\');" style="width:100%; '.$temp_sold_out[7].'"><i class="fa fa-shopping-cart padding-right-md"></i> '.$api_view_array['label_btn'].'</button>';
                    }
                }
                $temp_display .= '<button class="api_float_left btn btn-theme add-to-cart '.$temp_display_1.'" '.$temp_sold_out[2].' data-id="'.$fp->id.'" id="product_featured_'.$fp->id.'" style="width:100%; '.$temp_sold_out[7].'"><i class="fa fa-shopping-cart padding-right-md"></i> '.$api_view_array['label_btn'].'</button></div></div><div class="api_height_5 api_clear_both"></div>';
                $temp_display .= '</div></li>';
            }
        }
        $temp_display .= '</ul></div>';
        
        return $temp_display;
    }

    public function build_slide_v2($promotion_products, $config_data, $api_view_array)
    {
        $temp_display = '
        <a target-slide="'.$config_data['id'].'" class="click-'.$config_data['id'].' slick-prev slick-arrow control_prev_cat position-fixed"><span class="fa fa-chevron-left api_margin_top_5"></span></a>
        <a target-slide="'.$config_data['id'].'" class="click-'.$config_data['id'].' slick-next slick-arrow control_next_cat position-fixed"><span class="fa fa-chevron-right api_margin_top_5"></span></a>
        <div id="'.$config_data['id'].'"><ul>';
        
        foreach (array_chunk($promotion_products, $config_data['col_num']) as $fps) {

            foreach ($fps as $fp_2) {                
                foreach ($fp_2 as $key => $value) {
                    $fp->{$key} = $value;
                }
                $temp_sold_out = array();
                $temp_sold_out[3] = '<option value="1">1</option>';
                $temp_sold_out[5] = '';
                $temp_sold_out[6] = 'api_link_box_none';
                $temp_sold_out[7] = '';
                if ($this->api_shop_setting[0]['out_of_stock'] == 1) {
                    if ($fp->quantity <= 0) {
                        $temp_sold_out[0] = 'api_opacity_6';
                        $temp_sold_out[1] = '
                            <div class="api_absolute_center">
                                <div class="api_sold_out_tag" style="visibility:hidden;">
                                    Sold Out
                                </div>
                            </div>      
                        ';
                        $temp_sold_out[2] = 'disabled';
                        $temp_sold_out[3] = '<option value="0">0</option>';
                        $temp_sold_out[4] = 'api_display_none';
                        $temp_sold_out[5] = $api_view_array['sorry'];
                        $temp_sold_out[6] = '';
                        $temp_sold_out[7] = 'background-color:#808080; border-color:#808080;';
                    }
                }
    
                if (is_file('assets/uploads/'.$fp->image)) {
                    $temp_img = $fp->image;
                } else {
                    $temp_img = 'no_image.png';
                }
    
                $data_view = array(
                    'wrapper_class' => '',
                    'file_path' => 'assets/uploads/thumbs/'.$temp_img,
                    'max_width' => 190,
                    'max_height' => 190,
                    'product_link' => 'true',
                    'image_class' => $temp_sold_out[6].' img-responsive '.$temp_sold_out[0],
                    'image_id' => '',
                    'resize_type' => 'full',
                    'table_class' => 'api_kata_tip_table_img',
                    'image_title' => $fp->name,
                );
                $temp = $this->site->api_get_image($data_view);
                $temp_display .= '
                        <li class="product-top product-bottom" style="width:200px">
                        <div class="product product-image api_kata_tip_box" style="">
                            <a href="'.site_url('product/'.$fp->slug).'">
                                '.$temp['image_table'].''.$temp_sold_out[1].'
                            </a>
                            <a href="'.site_url('product/'.$fp->slug).'">
                                <div class="api_padding_left_10 api_padding_right_10 api_padding_bottom_0">
                                    <div class="api_title_line_2 api_title_truncate_line_2 text-red api_bold_im api_link_box_none">
                                        <span>
                                            '.$fp->{'title_'.$this->api_shop_setting[0]['api_lang_key']}.'
                                        </span>
                                    </div>
                                </div>
                            </a>';
                if (!$shop_settings->hide_price) {
                    $temp_display .= '
                                <div class="product-price '.$temp_sold_out[4].'" style="'.$api_view_array['price_style'].' width: 100% !important;">';
                    $config_data_2 = array(
                                        'id' => $fp->id,
                                        'customer_id' => $this->session->userdata('company_id'),
                                    );
                    $temp = $this->site->api_calculate_product_price($config_data_2);
                    if ($temp['promotion']['price'] > 0) {
                       if ($fp->quantity < $temp['promotion']['min_qty']) {
                            $temp_stock = lang('Out_of_Stock');
                            $temp_class = 'danger';
                        }
                        else {
                            $temp_stock = lang('In_Stock').' '.number_format($fp->quantity,0);
                            $temp_class = 'success';
                        }
                        $temp_display .= '
                            <del class="text-red">'.$this->sma->convertMoney($temp['price']).'</del>
                            <div class="api_padding_bottom_5">'.$this->sma->convertMoney($temp['price']).'<span class="label label-info api_border_r10" style="float: right;">'.$temp['promotion']['duration_left'].'</span></div>                                  
                                
                        ';
                    } else {
                        $temp_display .= ' 
                                            <del class="text-red api_visibility_hidden">'.$this->sma->convertMoney($temp['price']).'</del>
                                            <div class="api_padding_bottom_5">'.$this->sma->convertMoney($temp['price']).'</div>';
                    }
                    $temp_display .= '</div>';
                }

                if ($temp['promotion']['price'] > 0) {
                    $temp_qty = $temp['promotion']['min_qty'];
                }
                else
                    $temp_qty = 1;                
                $temp_display .= $temp_sold_out[5].'
                            <div class="api_padding_left_5 api_padding_right_5" style="'.$api_view_array['select_qty_style'].'" style="width: 100% !important;">
                                <div class="form-group" style="margin-bottom:5px;">
                                    <div class="input-group">                
                                        <span class="input-group-addon pointer btn-minus api_pause_carousel"><span class="fa fa-minus"></span></span>
                                        <input type="tel" name="quantity" id="api_kata_tip_display_product_quantity_'.$config_data['id'].'_'.$fp->id.'" onmouseout="sma_input_qty_mouse_out(\'api_kata_tip_display_product_quantity_'.$config_data['id'].'_'.$fp->id.'\','.$temp_qty.');" class="form-control text-center api_numberic_input quantity-input api_pause_carousel" value="'.$temp_qty.'" min_qty="'.$temp_qty.'" required="required">
                                        <span class="input-group-addon pointer btn-plus api_pause_carousel"><span class="fa fa-plus"></span></span>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>';

                if ($fp->is_favorite == 1) {
                    $temp_is_favorite = 'fa-heart';
                } else {
                    $temp_is_favorite = 'fa-heart-o';
                }
                $temp_display .= '        
                            <div class="product-cart-button api_padding_left_5 api_padding_right_5 '.$api_view_array['btn_class'].'">
                                <div class="btn-group" role="group" aria-label="...">
                                <button class="api_float_left btn btn-info add-to-wishlist" data-id="'.$fp->id.'"><i id="category_wishlist_heart_'.$fp->id.'" class="fa '.$temp_is_favorite.'"></i></button>';
                $temp_display_1 = '';
                if ($fp->is_ordered != 1) {
                    if ($this->session->userdata('user_id')) {
                        $temp_display_1 = 'api_display_none';
                        $temp_display .= '<button class="api_float_left btn btn-theme " '.$temp_sold_out[2].' data-toggle="modal" data-target="#api_modal_add_cart" onclick="api_add_cart_confirm(\'product_featured_'.$fp->id.'\');" style="width:100%; '.$temp_sold_out[7].'"><i class="fa fa-shopping-cart padding-right-md"></i> '.$api_view_array['label_btn'].'</button>';
                    }
                }
                $temp_display .= '<button class="api_float_left btn btn-theme add-to-cart '.$temp_display_1.'" '.$temp_sold_out[2].' data-id="'.$fp->id.'" id="product_featured_'.$fp->id.'" style="width:100%; '.$temp_sold_out[7].'"><i class="fa fa-shopping-cart padding-right-md"></i> '.$api_view_array['label_btn'].'</button></div></div><div class="api_height_5 api_clear_both"></div>';
                $temp_display .= '</div></li>';
            }
        }
        $temp_display .= '</ul></div>';
        
        return $temp_display;
    }

    public function f_strstr(string $find, string $in)
    {
        return preg_match("/$find:/", $in);
    }


    function api_calculate_sale_supplier($config_data) {
        $temp = $this->site->api_select_some_fields_with_where("
            product_id,id
            "
            ,"sma_sale_items"
            ,"sale_id = ".$config_data['id']." order by id asc"
            ,"arr"
        );
        for ($i2=0;$i2<count($temp);$i2++) {
            if ($temp[$i2]['product_id'] > 0) {
                $temp2 = $this->site->api_select_some_fields_with_where("
                    supplier1
                    "
                    ,"sma_products"
                    ,"id = ".$temp[$i2]['product_id']
                    ,"arr"
                );
                if ($temp2[0]['supplier1'] > 0) {
                    $return['suppliers'] .= '-'.$temp2[0]['supplier1'].'-';
                    $config_data_2 = array(
                        'table_name' => 'sma_sale_items',
                        'id_name' => 'id',
                        'field_add_ons_name' => 'add_ons',
                        'selected_id' => $temp[$i2]['id'],
                        'add_ons_title' => 'supplier',
                        'add_ons_value' => $temp2[0]['supplier1'],                    
                    );
                    $this->site->api_update_add_ons_field($config_data_2);                       
                }
            }
        }

        $config_data_2 = array(
            'table_name' => 'sma_sales',
            'id_name' => 'id',
            'field_add_ons_name' => 'add_ons',
            'selected_id' => $config_data['id'],
            'add_ons_title' => 'suppliers',
            'add_ons_value' => $return['suppliers'],                    
        );
        $this->site->api_update_add_ons_field($config_data_2);     


        return $return;
    }
    
    function api_get_customer_warehouse($config_data) {
        $return['warehouse_id'] = $config_data['default_warehouse'];
        if ($config_data['id'] > 0) {
            $temp = $this->site->api_select_some_fields_with_where("
                *     
                "
                ,"sma_companies"
                ,"id = ".$config_data['id']
                ,"arr"
            );
            if ($temp[0]['customer_group_id'] > 0) {
                $temp2 = $this->site->api_select_some_fields_with_where("
                    *     
                    "
                    ,"sma_customer_groups"
                    ,"id = ".$temp[0]['customer_group_id']
                    ,"arr"
                );
                if ($temp2[0]['warehouse'] > 0)
                    $return['warehouse_id'] = $temp2[0]['warehouse'];
            }
        }
        
        return $return;
    }

function api_get_consignment_quantity($config_data) {
    $temp_6 = $this->site->api_select_some_fields_with_where("*
        "
        ,"sma_consignment"
        ,"id = ".$config_data['consignment_id']
        ,"arr"
    );        
    $temp_3 = $this->site->api_select_some_fields_with_where("*
        "
        ,"sma_consignment_items"
        ,"sale_id = ".$config_data['consignment_id']
        ,"arr"
    );
    $temp_7 = $this->site->api_select_some_fields_with_where("*
        "
        ,"sma_sale_items"
        ,"sale_id = ".$config_data['sale_id']
        ,"arr"
    );        
    $k = 0;
    for ($i3=0;$i3<count($temp_3);$i3++) {    
        $temp = $this->site->api_select_some_fields_with_where("*
            "
            ,"sma_consignment_sale_items"
            ,"consignment_id = ".$config_data['consignment_id']." and product_id = ".$temp_3[$i3]['product_id']
            ,"arr"
        );
        $temp_qty = 0;
        for ($i5=0;$i5<count($temp);$i5++) {
            $temp_qty = $temp_qty + $temp[$i5]['quantity'];
        }

        $temp = $this->site->api_select_some_fields_with_where("id
            "
            ,"sma_returns"
            ,"add_ons LIKE '%:cs_reference_no:{".$temp_6[0]['reference_no']."}:%'"
            ,"arr"
        );
        $temp_retured_qty = 0;
        for ($i=0;$i<count($temp);$i++) {                       
            $temp_2 = $this->site->api_select_some_fields_with_where("quantity
                "
                ,"sma_return_items"
                ,"return_id = ".$temp[$i]['id']." and product_id = ".$temp_3[$i3]['product_id']
                ,"arr"
            );
            $temp_retured_qty = $temp_retured_qty + $temp_2[0]['quantity'];
        }                    

        $temp_consignment_qty = $temp_3[$i3]['quantity'] - $temp_qty - $temp_retured_qty;
        $return[$k]['product_id'] = $temp_3[$i3]['product_id'];
        $return[$k]['remain_qty'] = abs($temp_consignment_qty);
        for ($i7=0;$i7<count($temp_7);$i7++) {
            if ($temp_7[$i7]['product_id'] == $temp_3[$i3]['product_id'])
                $return[$k]['remain_qty_edit'] = $return[$k]['remain_qty'] + $temp_7[$i7]['quantity'];
        }
        $k++;
    }

    return $return;
}
function api_display_category($config_data) {
    $return['display'] = '';
    $config_data = array(
        'id' => $config_data['id'],
        'table_name' => $config_data['table_name'],
        'field_name' => $config_data['field_name'],
        'translate' => $config_data['translate'],
        'seperate' => $config_data['seperate'],
        'reverse' => $config_data['seperate'],
    );
    $_SESSION['api_temp'] = array();
    $this->api_get_category_arrow($config_data);    
    if ($config_data['reverse'] != 1) {
        for ($i2=0;$i2<count($_SESSION['api_temp']);$i2++) {
            $return['display'] .= $config_data['seperate'].$_SESSION['api_temp'][$i2];
        }
    }
    return $return;
}

function api_calculate_sale_payment($config_data) {  
    $temp_3 = $this->site->api_select_some_fields_with_where("
        grand_total
        "
        ,"sma_sales"
        ,"id = ".$config_data['sale_id']
        ,"arr"
    );      
    $temp = $this->site->api_select_some_fields_with_where("
        SUM(amount) as sum_amount
        "
        ,"sma_payments"
        ,"sale_id = ".$config_data['sale_id']
        ,"arr"
    );
    if ($temp[0]['sum_amount'] >= $temp_3[0]['grand_total']) {
        $temp_2 = array(
            'payment_status' => 'paid',
            'paid' => $temp[0]['sum_amount'],
        );    
    }
    else {
        if ($temp[0]['sum_amount'] == 0) {
            $temp_2 = array(
                'payment_status' => 'AR',
                'paid' => 0,
            );                
        }
        else {
            $temp_2 = array(
                'payment_status' => 'partial',
                'paid' => $temp[0]['sum_amount'],
            );     
        }
    }
    $this->db->update('sma_sales', $temp_2,"id = ".$config_data['sale_id']);
}

public function getAllCategories()
{
    $select_categories = $this->site->api_select_some_fields_with_where("
        *     
        "
        ,"sma_categories"
        ,"id > 0 and add_ons like '%:display:{yes}:%'"
        ,"obj"
    );
    return $select_categories;
}

public function getSubCategories($parent_id)
{
    $select_subcategories = $this->site->api_select_some_fields_with_where("
        *     
        "
        ,"sma_categories"
        ,"parent_id != 0 and add_ons like '%:display:{yes}:%'"
        ,"obj"
    );
    return $select_subcategories;
}

public function api_get_promotion_product($config_data) {
    if ($this->api_shop_setting[0]['air_base_url'] == base_url())
        $condition .= " and t2.add_ons like '%:air_display:{yes}:%' ";

    $language_array = unserialize(multi_language);
    for ($i=0;$i<count($language_array);$i++) {                    
        $temp_translate .= ", getTranslate(t1.translate,'".$language_array[$i][0]."','".f_separate."','".v_separate."') as title_".$language_array[$i][0];                
    }        
    $temp = $this->site->api_select_some_fields_with_where("
        t1.*".$temp_translate
        ,"sma_products as t1 inner join sma_categories as t2 on t1.category_id = t2.id"
        ,"t1.promotion = 1 ".$condition." order by t1.id desc"
        ,"obj"
    );
    $k = 0;
    $temp_2 = array();    
    if ($temp[0]->id > 0) {
        for ($i=0;$i<count($temp);$i++) {
            $config_data_2 = array(
                'id' => $temp[$i]->id,
            );
            $temp_hide = $this->site->api_get_product_display($config_data_2);
            if ($temp_hide['result'] != 'hide') {
                $temp_2[$k] = $temp[$i];
                $k++;
            }
        }
    }

    $k = 0;
    $temp_3 = array();  
    for ($i=0;$i<count($temp_2);$i++) {
        $config_data_2 = array(
            'id' => $temp_2[$i]->id,
            'customer_id' => $this->session->userdata('company_id'),
        );
        $temp_promo = $this->site->api_calculate_product_price($config_data_2);  
        if ($temp_promo['promotion']['price'] > 0) {
            $temp_3[$k] = $temp_2[$i];
            $k++;
        }             
    }        

    $temp_2 = array_slice($temp_3, 0, $config_data['limit']);
    $return['select_data'] = $temp_2;
    $return['select_data_total'] = $temp_3;

    return $return;
}

public function api_get_product_display($config_data)
    {
        $config_data_2 = array(
            'table_name' => 'sma_products',
            'select_table' => 'sma_products',
            'select_condition' => "id = ".$config_data['id']." and hide != 1 and add_ons like '%:no_use:{}:%' and add_ons like '%:product_status:{}:%'",
        );
        $select_data = $this->site->api_select_data_v2($config_data_2);
        if (count($select_data) > 0) {
            $return['result'] = 'show';
            if ($select_data[0]['hide'] != 2) {
                if ($this->session->userdata('company_id') > 0) {
                    if ($select_data[0]['hide_show_customer_type'] == 'show' && $select_data[0]['show_customer_list_selected'] != '') {
                        $temp = $this->site->api_select_some_fields_with_where(
                            "id",
                            "sma_products",
                            "id = ".$select_data[0]['id']." and getTranslate(add_ons,'show_customer_list_selected','".f_separate."','".v_separate."') like '%-".$this->session->userdata('company_id')."%'",
                            "arr"
                        );
                        if (is_array($temp)) {
                            if (count($temp) > 0) {
                                $return['result'] = 'show';
                            } else {
                                $return['result'] = 'hide';
                            }
                        }
                    } elseif ($select_data[0]['hide_show_customer_type'] != 'show' && $select_data[0]['hide_customer_list_selected'] != '') {
                        
                        $temp = $this->site->api_select_some_fields_with_where(
                            "id",
                            "sma_products",
                            "id = ".$select_data[0]['id']." and getTranslate(add_ons,'hide_customer_list_selected','".f_separate."','".v_separate."') not like '%-".$this->session->userdata('company_id')."%'",
                            "arr"
                        );
                        if (is_array($temp)) {
                            if (count($temp) > 0) {
                                $return['result'] = 'show';
                            } else {
                                $return['result'] = 'hide';
                            }
                        }
                    } else {
                        $temp = $this->site->api_select_some_fields_with_where(
                            "id",
                            "sma_products",
                            "id = ".$select_data[0]['id']." and getTranslate(add_ons,'display','".f_separate."','".v_separate."') != 'no'",
                            "arr"
                        );
                        if (is_array($temp)) {
                            if (count($temp) > 0) {
                                $return['result'] = 'show';
                            } else {
                                $return['result'] = 'hide';
                            }
                        }
                    }
                }
                else {
                    $temp = $this->site->api_select_some_fields_with_where(
                        "id",
                        "sma_products",
                        "id = ".$select_data[0]['id']." and getTranslate(add_ons,'display','".f_separate."','".v_separate."') != 'no'",
                        "arr"
                    );
                    if (is_array($temp)) {
                        if (count($temp) > 0) {
                            $return['result'] = 'show';
                        } else {
                            $return['result'] = 'hide';
                        }
                    }                    
                }
            }
        }
        else
            $return['result'] = 'hide';
        return $return;
    }

public function api_get_featured_product($config_data) {
    if ($this->api_shop_setting[0]['air_base_url'] == base_url())
        $condition .= " and t2.add_ons like '%:air_display:{yes}:%' ";
    $language_array = unserialize(multi_language);
    for ($i=0;$i<count($language_array);$i++) {                    
        $temp_translate .= ", getTranslate(t1.translate,'".$language_array[$i][0]."','".f_separate."','".v_separate."') as title_".$language_array[$i][0];                
    }  
    $temp = $this->site->api_select_some_fields_with_where("
        t1.*".$temp_translate
        ,"sma_products as t1 inner join sma_categories as t2 on t1.category_id = t2.id"
        ,"t1.featured = 1 ".$condition." order by t1.id desc"
        ,"obj"
    );
    $k = 0;
    $temp_2 = array();    
    if ($temp[0]->id > 0) {
        for ($i=0;$i<count($temp);$i++) {
            $config_data_2 = array(
                'id' => $temp[$i]->id,
            );
            $temp_hide = $this->site->api_get_product_display($config_data_2);
            if ($temp_hide['result'] != 'hide') {
                $temp_2[$k] = $temp[$i];
                $k++;
            }
        }
    }

    $temp = array_slice($temp_2, 0, $config_data['limit']);
    $return['select_data'] = $temp;
    $return['select_data_total'] = $temp_2;

    return $return;
}

public function getProductsByCategoryV2($config_data) {
    $category_id = $config_data['category_id'];
    $featured = $config_data['featured'];

    $condition_category = '(t1.category_id = '.$category_id;
    $temp = $this->site->api_select_some_fields_with_where(
        "
                id
        ",
        "sma_categories",
        "parent_id = ".$category_id,
        "arr"
    );
    if (is_array($temp)) {
        if (count($temp) > 0) {
            for ($i=0;$i<count($temp);$i++) {
                $condition_category .= " or t1.category_id = ".$temp[$i]['id'];
            }
        }
    }
    $condition_category .= ')';

    $condition_product = '';
    // if ($config_data['user_id'] > 0 && $this->api_shop_setting[0]['require_login'] == 1) {
    //     $temp = $this->site->api_select_some_fields_with_where(
    //         "
    //                 DISTINCT t1.product_id
    //         ",
    //         "sma_sale_items as t1 inner join sma_sales as t2 on t2.id = t1.sale_id",
    //         "t2.created_by = ".$config_data['user_id'],
    //         "arr"
    //     );            
    //     if (count($temp) > 0) {
    //         $condition_product = 'and (t1.id = '.$temp[0]['product_id'];
    //         for ($i=1;$i<count($temp);$i++) {
    //             $condition_product .= " or t1.id = ".$temp[$i]['product_id'];
    //         }
    //         $condition_product .= ')';
    //     }
    // }        
    
    $limit = '';
    if ($config_data['user_id'] <= 0 && $config_data['limit'] != '')
        $limit = " limit ".$config_data['limit'];

    $language_array = unserialize(multi_language);
    for ($i=0;$i<count($language_array);$i++) {                    
        $temp_translate .= ", getTranslate(t1.translate,'".$language_array[$i][0]."','".f_separate."','".v_separate."') as title_".$language_array[$i][0];                
    }  
    $select_data = $this->site->api_select_some_fields_with_where(
        "
                t1.* ".$temp_translate.",
                (SELECT SUM(c.quantity) FROM sma_sale_items c WHERE c.product_id = t1.id) as total_sale_quantity
        ",
        "sma_products as t1",
        $condition_category." ".$condition_product." and t1.hide != 1 and t1.slug != '' order by total_sale_quantity desc ".$limit,
        "obj"
    );
    
    if (count($select_data) > 0 && $config_data['user_id'] > 0) {
        
        $temp = $this->site->api_select_some_fields_with_where(
            "DISTINCT t1.product_id",
            "sma_sale_items as t1 inner join sma_sales as t2 on t2.id = t1.sale_id",
            "t2.created_by = ".$config_data['user_id'],
            "arr"
        );
        if (is_array($temp)) {
            if (count($temp) > 0) {
                for ($i=0;$i<count($temp);$i++) {
                    for ($j=0;$j<count($select_data);$j++) {
                        if ($select_data[$j]->id == $temp[$i]['product_id']) {
                            $select_data[$j]->is_ordered = 1;
                            break;
                        }
                    }
                }
            }
        }

        $temp = $this->site->api_select_some_fields_with_where(
            "t1.product_id",
            "sma_wishlist as t1 inner join sma_products as t2 on t2.id = t1.product_id",
            "t1.user_id = ".$config_data['user_id'],
            "arr"
        );
        if (is_array($temp)) {
            if (count($temp) > 0) {
                for ($i=0;$i<count($temp);$i++) {
                    for ($j=0;$j<count($select_data);$j++) {
                        if ($select_data[$j]->id == $temp[$i]['product_id']) {
                            $select_data[$j]->is_favorite = 1;
                            break;
                        }
                    }
                }
            }
        }
    }

    $select_data_2 = array();
    $k = 0;
    for ($i=0;$i<count($select_data);$i++) {
        $config_data_2 = array(
            'id' => $select_data[$i]->id,
        );
        $temp_hide = $this->api_get_product_display($config_data_2);
        if ($temp_hide['result'] != 'hide') {
            $select_data_2[$k] = $select_data[$i];
            $k++;
        }
    }

    $temp = array_slice($select_data_2, 0, $config_data['limit']);
    if (count($temp) > 0) {            
        return $temp;
    }
    return false;
}

public function api_payment_method($config_data) {
    $return = [];
    $config_data_2 = array(
        'table_name' => 'sma_payments',
        'select_table' => 'sma_payments',
        'translate' => '',
        'select_condition' => " id = ".$config_data['id'],
    );
    $payments = $this->site->api_select_data_v2($config_data_2);
    for ($i=0;$i<count($payments);$i++) {
    
        if ($payments[$i]['paid_by']=='cash') {
            $paid  = 'Cash';
            $amount = $payments[$i]['amount'];
        }
        else if ($payments[$i]['paid_by'] == 'aba_daiki') {
            $paid = 'ABA Daiki';
            $amount = $payments[$i]['amount'];
            $transaction_id = $payments[$i]['aba_daiki_transaction_id'];
            $text = 'Tran ID : ';
        }
        else if ($payments[$i]['paid_by'] == 'aba_qr') {
            $paid = 'ABA QR';
            $amount = $payments[$i]['amount'];
            $transaction_id = $payments[$i]['aba_qr_transfer_reference_number'];
            $text = 'Ref ID : ';
        }
        else if ($payments[$i]['paid_by'] == 'acode_daiki') {
            $paid = 'Acode Daiki';
            $amount = $payments[$i]['amount'];
            $transaction_id = $payments[$i]['acode_daiki_transaction_id'];
            $text = 'Tran ID : ';
        }
        else if ($payments[$i]['paid_by'] == 'AR') {
            $paid = 'AR';
            $amount = $payments[$i]['amount'];
        }
        else if ($payments[$i]['paid_by'] == 'CC') {
            $paid = 'Credit Card';
            $amount = $payments[$i]['amount'];
            $transaction_id = $payments[$i]['cc_no'];
            $name = $payments[$i]['cc_holder'];
        }
        else if ($payments[$i]['paid_by'] == 'Cheque') {
            $paid = 'Cheque';
            $amount = $payments[$i]['amount'];
            $transaction_id = $payments[$i]['cheque_no'];
            $text = 'Cheque No : ';
        }
        else if ($payments[$i]['paid_by'] == 'gift_card') {
            $paid = 'Gift Card';
        }
        else if ($payments[$i]['paid_by'] == 'payway') {
            $paid = 'Payway';
            $amount = $payments[$i]['amount'];
            $transaction_id = $payments[$i]['payway_transaction_id'];
            $text = 'Tran ID : ';
        }
        else if ($payments[$i]['paid_by'] == 'pipay') {
            $paid = 'Pipay';
            $amount = $payments[$i]['amount'];
            $transaction_id = $payments[$i]['pipay_transaction_id'];
            $text = 'Tran ID : ';
        }
        else if ($payments[$i]['paid_by'] == 'wing') {
            $paid = 'Wing';
            $amount = $payments[$i]['amount'];
            $transaction_id = $payments[$i]['wing_transaction_id'];
            $text = 'Tran ID : ';
        }
        else if ($payments[$i]['paid_by'] == 'wing_daiki') {
            $paid = 'Wing Daiki';
            $amount = $payments[$i]['amount'];
            $transaction_id = $payments[$i]['wing_daiki_transaction_id'];
            $text = 'Tran ID : ';
        }
        else if ($payments[$i]['paid_by'] == 'other') {
            $paid = 'Other';
            $amount = $payments[$i]['amount'];
        }
            //echo '<span class="api_text_transform_capitalize">'.lang($payments[$i]['paid_by']).'</span>';
    }
    $return['label'] = $paid;
    $return['transaction_id'] = $transaction_id;
    $return['amount'] = $amount;
    $return['text'] = $text;
    $return['name'] = $name;

    return $return;
}
public function api_calculate_customer_group_price($config_data_function) {
    $return = array();
//        $return['price'] = $config_data_function['price'];
    if ($config_data_function['customer_id'] > 0) {    
        $config_data = array(
            'table_name' => 'sma_companies',
            'select_table' => 'sma_companies',
            'select_condition' => "id = ".$config_data_function['customer_id'],
        );
        $select_customer = $this->api_select_data_v2($config_data);
        if ($select_customer[0]['customer_group_id'] > 0) {
            $select_customer_group = $this->api_select_some_fields_with_where("
                percent     
                "
                ,"sma_customer_groups"
                ,"id = ".$select_customer[0]['customer_group_id']
                ,"arr"
            );
        }

        if ($select_customer_group[0]['percent'] > 0) {
            // $return['price'] = $config_data_function['price'] + ($config_data_function['price'] * $select_customer_group[0]['percent']) / 100;

            // $return['original_price'] = $config_data_function['original_price'] + ($config_data_function['original_price'] * $select_customer_group[0]['percent']) / 100;                
        }
        if ($select_customer_group[0]['percent'] < 0) {
            // $return['price'] = $config_data_function['price'] - ($config_data_function['price'] * abs($select_customer_group[0]['percent'])) / 100;

            // $return['original_price'] = $config_data_function['original_price'] - ($config_data_function['original_price'] * abs($select_customer_group[0]['percent'])) / 100;                
        }
        $return['discount_rate'] = $select_customer_group[0]['percent'];
    }
    return $return;
}
public function api_calculate_product_price_v2($config_data_function) {
    $config_data = array(
        'table_name' => 'sma_products',
        'select_table' => 'sma_products',
        'select_condition' => "id = ".$config_data_function['id'],
    );
    $select_product = $this->api_select_data_v2($config_data);        

    //-customer_group-============================================
        if ($config_data_function['addition_price_rate'] > 0) {
            $temp = ($select_product[0]['price'] * $config_data_function['addition_price_rate']) / 100;
            $select_product[0]['price'] = $select_product[0]['price'] + $temp;
        }
    //-customer_group-============================================
    
    $return['temp_price'] = $select_product[0]['price'];
    $return['price'] = $select_product[0]['price'];
    $return['option'] = 'normal';
    
    //-promotion-============================================
        $temp_promotion = $this->api_calculate_product_promotion($select_product);
        if ($temp_promotion['price'] > 0) {
            $return['price'] = $temp_promotion['price'];
            $return['option'] = 'promotion';
            $return['promotion'] = $temp_promotion;
            $return['promotion']['discount_price'] = $return['temp_price'] - $temp_promotion['price'];
            $return['promotion']['rate'] = $this->api_helper->number_format($temp_promotion['rate'],2);
            
        }            
    //-promotion-============================================
    
    //-product_discount_customer-============================================
    if ($config_data_function['disable_product_discount_by_customer'] != 'yes') {
        $config_data = array(
            'return' => $return,
            'select_product' => $select_product,
        );
        $temp_product_discount = $this->api_calculate_product_discount($config_data);
        if ($temp_product_discount['promotion']['price'] > 0) {
            $return['price'] = $temp_product_discount['promotion']['price'];
            $return['option'] = 'product_discount';
            $return['promotion'] = $temp_product_discount['promotion'];
            $return['promotion']['discount_price'] = $return['temp_price'] - $temp_product_discount['promotion']['price'];
            $return['promotion']['rate'] = $this->api_helper->number_format($temp_product_discount['promotion']['rate'],2);
        }
        
    }
    //-product_discount_customer-============================================

    //-neating-============================================
    // $temp = $return['price'];        
    // $temp_decimal = $temp - floor($return['price']);
    // if ($temp_decimal > 0) {
    //     $temp2 = explode('.',$temp_decimal);
    //     $temp_decimal_array = str_split($temp2[1]);
    //     if ($temp_decimal_array[1] > 5) {
    //         $return['price'] = round($return['price'],1);
    //     }
    //     else {
    //         if ($temp_decimal_array[1] > 0) {
    //             $temp_decimal_array[1] = 5;                
    //             $return['price'] = floor($return['price']) + floatval('0.'.$temp_decimal_array[0].$temp_decimal_array[1]);
    //         }
    //     }
    // }
    //-neating-============================================
    
    return $return;
}
public function api_calculate_product_discount($config_data_function) {
    date_default_timezone_set('Asia/Phnom_Penh');
    $now = date('Y-m-d h:i:s');
    $return = $config_data_function['return'];
    $select_data = $config_data_function['select_product'];
    if ($this->session->userdata('company_id') > 0) {
        $now_date = date('Y-m-d');
        $temp_product_discount = $this->site->api_select_some_fields_with_where("
            start_date, end_date, special_price, discount_price  
            "
            ,"sma_product_discount"
            ,"customer_id = ".$this->session->userdata('company_id')." and product_id = ".$select_data[0]['id']." and start_date <= '".$now_date."' and end_date >= '".$now_date."' order by discount_price desc limit 1"
            ,"arr"
        );

        $b_expired = 1;
        if ($temp_product_discount[0]['start_date'] <= $now && $temp_product_discount[0]['end_date'] >= $now)
            $b_expired = 0;

        if ($b_expired == 0) {
            $temp_amount = $this->api_helper->get_amount_by_rate($temp_product_discount[0]['discount_price'],$select_data[0]['price']);
            $return['promotion']['price'] = $temp_amount['amount_remain'];
            $return['is_expired'] = $b_expired;
            if ($return['promotion']['price'] > 0) {
                $dStart = date_create($now);
                $dEnd  = date_create($temp_product_discount[0]['end_date']);
                $dDiff = $dStart->diff($dEnd);   
                if ($dDiff->days > 1)
                    $temp2 = 's';
                else
                    $temp2 = '';
                    
                if ($dDiff->days > 1)
                    $day_left = lang('days_left');
                else
                    $day_left = lang('day_left');
                $return['promotion']['duration'] = date('l jS F Y', strtotime($temp_product_discount[0]['end_date'])).'<br>('.$dDiff->days.' '.lang('day'.$temp2.'_left').')';
                $return['promotion']['duration_end_date'] = date('l jS F Y', strtotime($temp_product_discount[0]['end_date']));
                $return['promotion']['duration_left'] = $dDiff->days.' '.$day_left;
                $return['promotion']['min_qty'] = 1;   
                $return['promotion']['rate'] = $temp_product_discount[0]['discount_price'];                    
            }    
        }   
    }
    return $return;
}

}
