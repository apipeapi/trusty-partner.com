<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 

    $temp_url = '';
    if ($_GET['page'] != '') 
        $temp_url .= '?page='.$_GET['page']; 
    else 
        $temp_url .= '?page=1'; 

    if ($per_page != '') $temp_url .= '&per_page='.$per_page;
    if ($_GET['mode'] == 'consignment') $temp_url .= '&mode='.$_GET['mode'];

    foreach ($_GET as $name => $value) {
        if ($name != 'page' && $name != 'per_page' && $name != 'mode')
            if ($value != '') $temp_url .= '&'.$name.'='.$value;
    }

    echo admin_form_open('purchases'.$temp_url,'id="action-form" name="action-form" method="GET"');
    echo '<input type="hidden" name="mode" value="'.$_GET['mode'].'" />';
?>

<div class="box">
    <div class="box-header">
        <h2 class="blue">
            <i class="fa-fw fa fa-users"></i>
            <?php
                echo $page_title; 
            ?>
        </h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon fa fa-tasks tip" data-placement="left" title="<?=lang("actions")?>"></i>
                    </a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
<?php
                            if ($_GET['mode'] != '') $temp = '?mode='.$_GET['mode']; else $temp = '';
                            echo '
                                <li>
                                    <a href="'.admin_url('purchases/add').$temp.'">
                                        <i class="fa fa-plus-circle"></i> '.lang('add').'
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" onclick="api_bulk_actions(\'export_excel\');" data-action="export_excel">
                                        <i class="fa fa-file-excel-o"></i> '.lang('export_to_excel').'
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" onclick="api_bulk_actions(\'combine_to_pdf\');">
                                        <i class="fa fa-file-pdf-o"></i> '.lang('combine_to_pdf').'
                                    </a>
                                </li>
                            ';
                            echo '
                                <li class="divider"></li>
                            ';
?>
                                <li>
                                    <a href="#" class="bpo" title="<b><?=lang("delete_purchases")?></b>" data-content="<p><?=lang('r_u_sure')?></p>
                                    <button type='button' class='btn btn-danger' onclick='bulk_actions_delete();' data-action='delete'><?=lang('i_m_sure')?></a> <button class='btn bpo-close'><?=lang('no')?></button>" data-html="true" data-placement="left">
                                        <i class="fa fa-trash-o"></i> <?=lang('delete_purchases')?>
                                    </a>
                                </li>
                    </ul>
                </li>
                <li class="dropdown api_display_none">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-building-o tip" data-placement="left" title="<?=lang("warehouses")?>"></i></a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li><a href="<?=admin_url('purchases')?>"><i class="fa fa-building-o"></i> <?=lang('all_warehouses')?></a></li>
                        <li class="divider"></li>
                        <?php
                            for ($i=0;$i<count($api_warehouse);$i++) {
                                echo '<li ' . ($api_warehouse[$i]['id'] && $api_warehouse[$i]['id'] == $_GET['warehouse_id'] ? 'class="active"' : '') . '><a href="' . admin_url('purchases?warehouse_id=' . $api_warehouse[$i]['id']).'"><i class="fa fa-building"></i>' . $api_warehouse[$i]['name'] . '</a></li>';
                            }
                        ?>
                    </ul>
                </li>
            </ul>
        </div>

    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
<?php
    echo '
    <div style="background: #F9F9F9; margin: -20px -20px 20px -20px; padding: 10px; border-bottom: 1px solid #DBDEE0;">
    ';

    echo '    
        <div class="api_float_right">
            <ul style="list-style: none; margin:0px;">
            <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">                    
                    <button type="button" class="btn btn-success" style="margin:5px 0px 5px 5px !important;">
                        <li class="fa fa-pencil-square-o"></li> '.lang('Change_Payment').'
                    </button>
                </a>
                <ul class="dropdown-menu tasks-menus" role="menu" aria-labelledby="dLabel" style="">
                    <li>
                        <a href="javascript:void(0);" onclick="api_bulk_actions(\'change_payment_status_to_pending\');">
                            <i class="fa fa-edit"></i>'.lang('to_pending').'
                        </a>    
                    </li>

                    <li> 
                        <a href="javascript:void(0);" onclick="api_bulk_actions(\'change_payment_status_to_partial\');">
                            <i class="fa fa-edit"></i>'.lang('to_partial').'
                        </a>
                    </li>

                    <li>
                        <a href="javascript:void(0);" onclick="api_bulk_actions(\'change_payment_status_to_paid\');">
                            <i class="fa fa-edit"></i>'.lang('to_paid').'
                        </a>    
                    </li>
                </ul>
            </li>
            </ul>
        </div>
    ';

    echo '    
        <div class="api_float_right">
            <ul style="list-style: none; margin:0px;">
            <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle api_link_box_none" href="#">                    
                    <button type="button" class="btn btn-info" style="margin:5px 0px 5px 5px !important; background-color:#004f93  !important; border-color:#004f93  !important;">
                        <li class="fa fa-pencil-square-o"></li> '.lang('Change_Status').'
                    </button>
                </a>

                <ul class="dropdown-menu tasks-menus" role="menu" aria-labelledby="dLabel" style="">
                    <li>
                        <a href="javascript:void(0);" onclick="api_bulk_actions(\'change_status_to_received\');">
                            <i class="fa fa-edit"></i>'.lang('to_received').'
                        </a>    
                    </li>

                    <li> 
                        <a href="javascript:void(0);" onclick="api_bulk_actions(\'change_status_to_pending\');">
                            <i class="fa fa-edit"></i>'.lang('to_pending').'
                        </a>
                    </li>

                    <li>
                        <a href="javascript:void(0);" onclick="api_bulk_actions(\'change_status_to_ordered\');">
                            <i class="fa fa-edit"></i>'.lang('to_ordered').'
                        </a>    
                    </li>

                    <li>
                        <a href="javascript:void(0);" onclick="api_bulk_actions(\'change_status_to_on_the_way\');">
                            <i class="fa fa-edit"></i>'.lang('to_on_the_way').'
                        </a>    
                    </li>
                </ul>
            </li>
            </ul>
        </div>
    ';

    echo '
        <div class="api_clear_both"></div>
    </div>
    ';
?>
                
                <div class="table-responsive">
                        <div class="row">
                            <div class="col-md-6 text-left api_padding_top_5">
                                <div class="short-result">
                                     <span><?= lang('Show') ?></span>
                                         <select  id="" name="per_page" onchange="api_form_submit('<?php echo 'admin/purchases'.$temp_url; ?>')" style="width:80px;">
                                             <?php
                                                 $per_page_arr = array(
                                                     '10'=>'10',
                                                     '25'=>'25',
                                                     '50'=> '50',
                                                     '100'=>'100',
                                                     '200'=>'200',
                                                     '500'=>'500',
                                                     'All'=>$total_rows_sale
                                                 );
                                                
                                                 foreach($per_page_arr as $key =>$value){
                                                     $select = $value == $per_page?'selected':'';
                                                     echo '<option value="'.$value.'" '.$select.'>';
                                                             echo $key;
                                                     echo '</option>';
                                                 }

                                             ?>
                                         </select>
                                 </div>
                            </div>
                            <div class="col-md-6 text-right">
                                <div class="short-result">
                                    <div id="SLData_filter" class="show-data-search dataTables_filter">
                                        <label>
                                            Search
                                            <input class="input-xs" value="<?php echo $this->input->get('search');?>" type="text" name="search" placeholder="search"/>
                                            <input style="height: 34px;" class="btn btn-primary" type="submit" name="submit_search" value="<?= lang('Search') ?>">
                                        </label>                                        
                                    </div>
                                </div>    
                            </div>  

                        </div>     

<?php
    $row_headers = array(
        'checkbox'      => [
                            'label' => '<input class="checkbox checkth skip" onclick="api_select_check_all(this,\'val[]\'); api_setValueCheckbox(\'val[]\',\'check_value\',\'action-form\',\'-\')" type="checkbox" name="check"/>', 
                            'sort_fn' => false,
                            'style' => 'min-width:30px; width: 30px; text-align: center;'
                        ],
        'date'          => ['label' => lang("Date"), 'style' => 'min-width:80px; max-width:80px;'],
        'reference_no'  => ['label' => lang("reference_no"), 'style' => 'min-width:145px;'],
        'due_date'  => ['label' => lang("Due_Date"), 'style' => 'min-width:80px; max-width:80px;'],
        'supplier'      => ['label' => lang("Supplier"), 'style' => ''],
        'status'   => ['label' => lang("Purchase_Status"), 'style' => ''],
        'grand_total'   => ['label' => lang("grand_total"), 'style' => 'text-align:right;'],
        'payment_status'   => ['label' => lang("Payment_Status"), 'style' => ''],
        'action'        => [
                            'label' => lang("actions"), 
                            'style' => 'width:80px; max-width:80px; text-align:center;', 
                            'sort_fn' => false
                        ],
    );
?>

                    <table id="" class="table table-bordered table-hover table-striped" cellpadding="0" cellspacing="0" border="0">
                        <thead>
                        <tr class="primary">

<?php foreach ($row_headers as $key => $row) :?>
    <?php
   
    $th_class="pointer";
    $e_click = true;
    if(isset($row['sort_fn']) && $row['sort_fn'] == false) {
        $th_class="";
        $e_click = false;
    }
    $style = isset($row['style']) && $row['style'] != '' ? 'style="'.$row['style'].'"' : '';
    ?>
    <th <?= $style ?> >
        <?php if($e_click ) :?>
            <?php if ($sort_by == $key && $sort_order == "ASC") : ?>
                    <label class="font-normal <?=$th_class?>" onclick="formactionsubmit('<?= $key ?>', 'DESC')" id="fa_sort"  aria-hidden="true">
                    <?php echo $row['label']; ?>
                        <span class="fa fa-sort-down"></span>
                    </label>
            <?php elseif ($sort_by == $key and $sort_order=="DESC") : ?>    
                <label class="font-normal <?=$th_class?>" onclick="formactionsubmit('<?= $key ?>', 'ASC')" id="fa_sort_up"  aria-hidden="true">
                    <?php echo $row['label']; ?>
                    <span class="fa fa-sort-up"></span>
                </label>
            <?php else : ?> 
                <label class="font-normal <?=$th_class?>" onclick="formactionsubmit('<?= $key ?>', 'DESC')" id="fa_sort_down" aria-hidden="true">
                    <?php echo $row['label']; ?>
                    <span class="fa fa-sort"></span>
                </label>
            <?php endif; ?> 
        <?php else : ?>
            <label class="font-normal <?=$th_class?>" aria-hidden="true">
                <?php echo $row['label']; ?>
            </label>
        <?php endif; ?> 
    </th>
<?php endforeach; ?>

                            </th>
                        </tr>
                        </thead>
                        <tbody role="alert" aria-live="polite" aria-relevant="all">

<?php
for ($i=0;$i<count($select_data);$i++) {

    if ($select_data[$i]['consignment_status'] == 'pending')
        $temp_reference_no = '
            <br>
            <span class="label label-warning">
                '.lang('Under_Consignment').'
            </span>
        ';
    elseif ($select_data[$i]['consignment_status'] == 'completed') {
         $temp_reference_no = '
            <br>
            <span class="label label-success">
                '.lang('Consignment_Completed').'
            </span>
        ';       
    }

    if ($select_data[$i]['status'] == 'ordered')
        $temp = 'info';
    if ($select_data[$i]['status'] == 'pending')
        $temp = 'warning';
    if ($select_data[$i]['status'] == 'On the way')
        $temp = 'default';
    if ($select_data[$i]['status'] == 'received')
        $temp = 'success';
    $temp_status = '
        <span class="label label-'.$temp.'">
            '.$select_data[$i]['status'].'
        </span>
    ';                        

    if ($select_data[$i]['payment_status'] == 'partial')
        $temp = 'info';
    if ($select_data[$i]['payment_status'] == 'pending')
        $temp = 'warning';
    if ($select_data[$i]['payment_status'] == 'paid')
        $temp = 'success';
    $temp_payment_status = '
        <span class="label label-'.$temp.'">
            '.$select_data[$i]['payment_status'].'
        </span>
    ';  
    $temp_due_date = '';                      
    if (isset($select_data[$i]['due_date']) && $select_data[$i]['due_date'] != '0000-00-00')
        $temp_due_date = date('d/m/Y', strtotime($select_data[$i]['due_date'])).'<br>'.date('H:i:s', strtotime($select_data[$i]['due_date']));

    if ($_GET['mode'] != 'consignment')
        $temp = 'purchase_link';
    else
        $temp = 'consignment_purchase_link';

    echo '
    <tr id="'.$select_data[$i]['id'].'" class="'.$temp.'">
        <td class="api_td_width_auto"  style="text-align: center;">
            <div class="text-center">
                <input class="checkbox multi-select skip" type="checkbox" name="val[]" value="'.$select_data[$i]['id'].'" onclick="api_setValueCheckbox(\'val[]\',\'check_value\',\'action-form\',\'-\')"/>
            </div>
        </td>
        <td align="center" class="api_td_width_auto">
            '.date('d/m/Y', strtotime($select_data[$i]['date'])).'<br>
            '.date('H:i:s', strtotime($select_data[$i]['date'])).'
        </td>
        <td align="center" class="api_td_width_auto">
            '.$select_data[$i]['reference_no'].'
            '.$temp_reference_no.'
        </td>
        <td align="center" class="api_td_width_auto">
            '.$temp_due_date.'            
        </td>
        <td align="left" class="api_td_width_auto">
            '.$select_data[$i]['supplier'].'
        </td>
        <td align="center" class="api_td_width_auto">
            <div class="text-center">
                '.$temp_status.'
    ';
        $temp_display = '';        
        $temp = $this->site->api_select_some_fields_with_where("
            id
            "
            ,"sma_consignment_purchase_items"
            ,"purchase_id = ".$select_data[$i]['id']." limit 1"
            ,"arr"
        );
    
        if (count($temp) > 0) {
            $temp_display .= '
                <a class="api_link_box_none" href="'.base_url().'admin/purchases/consignment_purchase_sale_history/'.$select_data[$i]['id'].'" title="'.lang('Sales').'" data-toggle="modal" data-target="#myModal"> 
                    <span class="label label-info">S</span>
                </a>
            ';
        }

        $temp = $this->site->api_select_some_fields_with_where("*
            "
            ,"sma_returns"
            ,"add_ons like '%:purchase_id:{".$select_data[$i]['id']."}:%'"
            ,"arr"
        );
        if (count($temp) > 0) {
            $temp_display .= '
                <a class="api_link_box_none" href="'.base_url().'admin/returns?purchase_id='.$select_data[$i]['id'].'" target="_blank" title="'.lang('Returned_Items').'"> 
                    <span class="label label-danger">R</span>
                </a>
            ';
        }
    echo '
        <div class="api_padding_top_2">
            '.$temp_display.'
        </div>
    ';        
    echo '
            </div>
        </td>        
        <td align="right" class="api_td_width_auto">
            '.$this->sma->formatMoney($select_data[$i]['grand_total']).'
        </td>
        <td align="center" class="api_td_width_auto">
            <div class="text-center">
                '.$temp_payment_status.'
            </div>
        </td>        
    ';


    $delete_link = "
        <a href='#' class='po' 
            title='<b>".lang("Delete")."</b>' 
            data-content=\"
                <p>".lang('r_u_sure')."</p>
                <a class='btn btn-danger' href='".admin_url('purchases/delete/'.$select_data[$i]['id'])."'>
                    ".lang('i_m_sure')."
                </a> 
                <button class='btn po-close'>".lang('no')."</button>\" 
                data-html=\"true\" data-placement=\"left\">
            <i class='fa fa-trash-o api_width_20'></i>
            ".lang('Delete')."
        </a>
    ";

    if ($select_data[$i]['parent_id'] != 0)
        $temp = '';

    $temp_display = '
        <li>
            <a href="'.base_url().'admin/purchases/edit/'.$select_data[$i]['id'].'?mode='.$_GET['mode'].'">
                <i class="fa fa-edit api_width_20"></i> '.lang('Edit').'
            </a>
        </li>
    ';

    if ($_GET['mode'] == 'consignment')
        $temp_display .= '
            <li>
                <a href="'.base_url().'admin/purchases/consignment_purchase_sale_history/'.$select_data[$i]['id'].'" data-toggle="modal" data-target="#myModal">
                    <i class="fa fa-heart api_width_20"></i> '.lang('Sale_History').'
                </a>
            </li>
        ';

    $temp_display .= '
        <li>
            <a href="'.base_url().'admin/purchases/view/'.$select_data[$i]['id'].'">
                <i class="fa fa-file-text-o api_width_20"></i> '.lang('purchase_details').'
            </a>
        </li>
        <li>
            <a href="'.base_url().'admin/purchases/payments/'.$select_data[$i]['id'].'" data-toggle="modal" data-target="#myModal">
                <i class="fa fa-money api_width_20"></i> '.lang('view_payments').'
            </a>
        </li>    
        <li>
            <a href="'.base_url().'admin/purchases/add_payment/'.$select_data[$i]['id'].'" data-toggle="modal" data-target="#myModal">
                <i class="fa fa-money api_width_20"></i> '.lang('add_payment').'
            </a>
        </li>
    ';

    if ($_GET['mode'] == 'consignment')
        $temp_display .= '
            <li>
                <a href="'.base_url().'admin/returns/?purchase_id='.$select_data[$i]['id'].'">
                    <i class="fa fa-eye api_width_20"></i> '.lang('View_Returns').'
                </a>
            </li>
            <li>
                <a href="'.base_url().'admin/purchases/add_return/'.$select_data[$i]['id'].'" data-toggle="modal" data-target="#myModal">
                    <i class="fa fa-random api_width_20"></i> '.lang('Add_Return').'
                </a>
            </li>
        ';

    $temp_display .= '
        <li>
            <a href="'.base_url().'admin/purchases/pdf/'.$select_data[$i]['id'].'">
                <i class="fa fa-file-pdf-o api_width_20"></i> '.lang('download_pdf').'
            </a>
        </li>    
        <li>
            <a href="'.base_url().'admin/purchases/email/'.$select_data[$i]['id'].'" data-toggle="modal" data-target="#myModal">
                <i class="fa fa-envelope api_width_20"></i> '.lang('email_purchase').'
            </a>
        </li>  
        <li>
            <a href="'.base_url().'admin/products/print_barcodes?purchase='.$select_data[$i]['id'].'">
                <i class="fa fa-print api_width_20"></i> '.lang('print_barcodes').'
            </a>
        </li>
        <li>'.$delete_link.'</li>
    ';
    echo '
        <td align="center" class="api_td_width_auto">
            <div class="text-center">
                <div class="btn-group text-left">
                    <button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" 
                            data-toggle="dropdown">
                        '.lang('actions').'
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu pull-right" role="menu">
                        '.$temp_display.'
                    </ul>
                </div>
            </div>
        </td>
    ';

    echo '
    </tr>
    ';
}
if (count($select_data) <= 0) {
    echo '
    <tr>
    <td colspan="11">
        '.lang('No_record_found').'
    </td>
    </tr>
    ';
}
echo '
    </tbody>
';
?>                            
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th style="min-width:30px; width: 30px; text-align: center;">
                                <input class="checkbox checkft skip" type="checkbox" name="check" onclick="api_select_check_all(this,'val[]'); api_setValueCheckbox('val[]','check_value','action-form','-')"/>
                            </th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>                  
                            <th></th>
                            <th style="text-align: center"><?php echo lang('Actions'); ?></th>
                        </tr>
                        </tfoot>
                    </table>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel-heading-left">
                                <p class="data-total">
                                    <?php echo $show.' ';?>
                                </p>
                            </div>   
                            <div class="panel-heading-right">
                                <p>
                                   <?php echo $this->pagination->create_links(); //pagination links ?>
                                </p>
                            </div>  
                            <div class="panel-heading-clear-fix"></div>   
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" name="show_data" value="<?php echo $show_data;?>" id="show_data"/>
<input type="hidden" name="sort_by" value="<?php echo $sort_by;?>" id="sort_by"/>
<input type="hidden" name="sort_order" value="<?php echo $sort_order;?>" id="sort_order"/>
<?= form_close() ?>

<?php

echo admin_form_open('purchases/purchase_actions','id="api_form_action" name="api_form_action" method="post"');
echo '
    <div class="api_display_none">
            <input type="text" name="check_value" value="" id="check_value"/>
            <input type="text" name="api_action" value="" id="api_action"/>
            <button type="button" onclick="document.api_form_action.submit();" value="ok">ok</button>
    </div>
';
echo form_close();

?>


<script>
    function api_bulk_actions(action){
        $("#api_action").val(action);
        document.api_form_action.submit();
    } 
    function bulk_actions_delete(){
        $("#api_action").val('delete');
        document.api_form_action.submit();
    }
    function formactionsubmit(sort_by, sort_order) {
        document.getElementById("sort_by").value = sort_by;
        document.getElementById("sort_order").value = sort_order;
        document.forms[0].submit();
    }
    function api_form_submit(action){
        $("#action-form").attr('action', action);
        $('#form_action').val('');
        $("#action-form").submit();
    }

<?php
    $temp = $_GET['page'];
    if ($temp == '') $temp = 1;
    echo '
        $(".pagination .active").html(\'<a href="javascript:void(0);">'.$temp.'</a>\');
    ';
?>

</script>

<script type="text/javascript">
    $('body').on('click', '.consignment_purchase_link td:not(:first-child, :nth-last-child(4), :last-child)', function() {
        $('#myModal').modal({remote: site.base_url + 'purchases/modal_view_consignment/' + $(this).parent('.consignment_purchase_link').attr('id') + '?mode=consignment'});
        $('#myModal').modal('show');
    });    
</script>

<style>
    .purchase_link td:nth-child(5), .purchase_link td:nth-last-child(2){
        cursor: pointer;
    }
    .consignment_purchase_link td{
        cursor: pointer;
    }
    .consignment_purchase_link td:first-child, td:nth-last-child(4), td:nth-last-child(1){
        cursor: default;
    }


</style>
