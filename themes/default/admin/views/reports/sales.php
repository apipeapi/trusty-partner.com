<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
    $temp_url = '';
    if ($_GET['page'] != '') 
        $temp_url .= '?page='.$_GET['page']; 
    else 
        $temp_url .= '?page=1'; 
    if ($_GET['search'] != '') $temp_url .= '&sort_by='.$_GET['search'];
    if ($_GET['sort_by'] != '') $temp_url .= '&sort_by='.$_GET['sort_by'];
    if ($_GET['sort_order'] != '') $temp_url .= '&sort_order='.$_GET['sort_order'];
    if ($per_page != '') $temp_url .= '&per_page='.$per_page;
    if ($_GET['start_date'] != '') $temp_url .= '&start_date='.$_GET['start_date'];
    if ($_GET['end_date'] != '') $temp_url .= '&end_date='.$_GET['end_date'];

    echo admin_form_open('reports/sales'.$temp_url,'id="action-form" name="action-form" method="GET"');
?>

<div class="box">
    <div class="box-header">
        <h2 class="blue"><i
                class="fa-fw fa fa-barcode"></i><?= lang('Report_Sales') . ' (' . ($warehouse_id ? $warehouse->name : lang('all_warehouses')) . ')'.($supplier ? ' ('.lang('supplier').': '.($supplier->company && $supplier->company != '-' ? $supplier->company : $supplier->name).')' : ''); ?>
        </h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a class="api_link_box_none" onclick="document.api_form_action.submit();" title="<?= lang('Export_Excel') ?>">
                        <i class="icon fa fa-file-excel-o"></i>
                    </a>
                </li>
            </ul>
        </div>

    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?= lang('list_results'); ?></p>

                <div id="form">

                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <?= lang("start_date", "start_date"); ?>
                                <?php echo form_input('start_date', (isset($_GET['start_date']) ? $_GET['start_date'] : ''), 'class="form-control datetime" autocomplete="off" id="start_date"'); ?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <?= lang("end_date", "end_date"); ?>
                                <?php echo form_input('end_date', (isset($_GET['end_date']) ? $_GET['end_date'] : ''), 'class="form-control datetime" autocomplete="off" id="end_date"'); ?>
                            </div>
                        </div>

<?php
                        echo '
                            <div class="col-md-3">
                                <div class="form-group">
                                    '.lang("company", "company").'
                        ';
                        $config_data = array(
                            'none_label' => lang("All_Companies"),
                            'table_name' => 'sma_companies',
                            'space' => ':....',
                            'strip_id' => '',
                            'field_name' => 'company',
                            'condition' => ' and group_id = 3',
                        );                    
                        $this->site->api_get_option_category($config_data);
                        for ($i=0;$i<count($_SESSION['api_temp']);$i++) {                        
                            $temp = explode('<api>',$_SESSION['api_temp'][$i]);
                            $tr[$temp[0]] = $temp[1];
                        }
                        echo form_dropdown('customer', $tr, (isset($_GET['customer']) ? $_GET['customer'] : ''), 'data-placeholder="'.lang("customer").'" class="form-control"');
                        echo '
                                </div>
                            </div>
                        ';

                        echo '
                            <div class="col-md-3">
                                <div class="form-group">
                                    '.lang("Biller", "Biller").'
                        ';
                            $tr2[''] = lang("All_Biller");
                            for ($i=0;$i<count($api_biller);$i++) {
                                if ($api_biller[$i]['company'] != '')
                                    $tr2[$api_biller[$i]['id']] = $api_biller[$i]['company'];
                            }                        
                            echo form_dropdown('biller', $tr2, (isset($_GET['biller']) ? $_GET['biller'] : ''), 'class="form-control tip select" id="biller" style="width:100%;" ');

                        
                        echo '
                                </div>
                            </div>
                        ';

                    echo '
                    </div>
                    ';
                    echo '
                    <div class="row">
                    ';
                        
                        echo '
                            <div class="col-md-3">
                                <div class="form-group">
                                    '.lang("warehouse", "warehouse").'
                        ';
                        $tr3[''] = lang("All_Warehouses");
                        for ($i=0;$i<count($api_warehouse);$i++) {
                            if ($api_warehouse[$i]['name'] != '')
                                $tr3[$api_warehouse[$i]['id']] = $api_warehouse[$i]['name'];
                        }
                        echo form_dropdown('warehouse', $tr3, (isset($_GET['warehouse']) ? $_GET['warehouse'] : ''), ' class="form-control"');
                        echo '
                                </div>
                            </div>
                        ';                        

                        echo '
                            <div class="col-md-3">
                                <div class="form-group">
                                '.lang("created_by", "created_by").'
                        ';
                        $tr4[''] = lang("All_Users");
                        for ($i=0;$i<count($api_user);$i++) {
                            if ($api_user[$i]['fullname'] != '')
                                $tr4[$api_user[$i]['id']] = $api_user[$i]['fullname'];
                        }
                        echo form_dropdown('created_by', $tr4, (isset($_GET['created_by']) ? $_GET['created_by'] : ""), 'class="form-control" id="created_by"');
                        echo '
                                </div>
                            </div>
                        ';      

                        echo '
                            <div class="col-md-3">
                                <div class="form-group">
                                    '.lang("Supplier", "Supplier").'
                        ';
                            $tr2[''] = lang("All_Suppliers");
                            for ($i=0;$i<count($api_supplier);$i++) {
                                if ($api_supplier[$i]['company'] != '')
                                    $tr2[$api_supplier[$i]['id']] = $api_supplier[$i]['company'];
                            }                        
                            echo form_dropdown('api_supplier', $tr2, (isset($_GET['api_supplier']) ? $_GET['api_supplier'] : ''), 'class="form-control" id="api_supplier" style="width:100%;" ');

                        echo '
                                </div>
                            </div>
                        ';
                        
/*
                        echo '
                            <div class="col-md-3">
                                <div class="form-group">
                                '.lang("product", "product").'
                        ';
                        $tr5[''] = lang("All_Products");
                        for ($i=0;$i<count($api_product);$i++) {
                            if ($api_product[$i]['product_name'] != '')
                                $tr5[$api_product[$i]['id']] = $api_product[$i]['product_name'];
                        }
                        echo form_dropdown('product', $tr5, (isset($_GET['product']) ? $_GET['product'] : ""), 'class="form-control" id="product"');
                        echo '
                                </div>
                            </div>
                        ';
*/
                        
                        echo '
                            <div class="col-md-3">
                                <div class="form-group">
                                '.lang("Search", "Search").'
                                <input type="text" class="form-control" id="search" name="search" value="'.(isset($_GET['search']) ? $_GET['search'] : "").'" onkeydown="if (event.keyCode == 13) document.action-form.submit()" />
                        ';
                        echo '
                                </div>
                            </div>
                        ';

?>
                                              
                        <div class="col-md-12">
                            <div class="form-group pull-right api_padding_left_10">
                                <a href="<?php echo base_url().'admin/reports/sales'; ?>">
                                    <input type="button" name="reset" value="reset" class="btn btn-danger">
                                </a>
                            </div>
                            <div class="form-group pull-right">
                                <input type="button" name="submit_report" onclick="api_form_submit('<?php echo 'admin/reports/sales'.$temp_url; ?>')" value="Submit" class="btn btn-primary">
                            </div>
                        </div>  
                    </div>    

                </div>
                <!-- /form search -->
                <div class="clearfix"></div>

                <div class="table-responsive">
                    <div id="SLData_wrapper" class="dataTables_wrapper form-inline" role="grid">
                        <div class="row">
                            <div class="col-md-6 text-left">
                                <div class="short-result">
                                     <label>Show</label>
                                         <select  id="sel_id" name="per_page" onchange="api_form_submit('<?php echo 'admin/reports/sales'.$temp_url; ?>')" style="width:80px;">
                                             <?php
                                                 $per_page_arr = array(
                                                     '10'=>'10',
                                                     '25'=>'25',
                                                     '50'=> '50',
                                                     '100'=>'100',
                                                     '200'=>'200',
                                                     '500'=>'500',
                                                     '1000'=>'1000',                                                     
                                                     'All'=>$total_rows_sale
                                                 );
                                                
                                                 foreach($per_page_arr as $key =>$value){
                                                     $select = $value == $per_page?'selected':'';
                                                     echo '<option value="'.$value.'" '.$select.'>';
                                                             echo $key;
                                                     echo '</option>';
                                                 }

                                             ?>
                                         </select>
                                 </div>
                             </div>

                        </div>     
                    </div>   

                    <div class="clearfix api_height_15"></div>

<?php
    $row_headers = array(
        'checkbox'      => [
                            'label' => '<input class="checkbox checkth skip" id="api_check_all" onclick="api_select_check_all(this,\'val[]\'); api_setValueCheckbox(\'val[]\',\'check_value\',\'action-form\',\'-\'); api_calculate_sum_all(\'api_check_all\');" type="checkbox" name="check"/>', 
                            'sort_fn' => false,
                            'style' => 'min-width:30px; width: 30px; text-align: center;'
                        ],
        'date'          => ['label' => lang("date"), 'style' => ''],
        'reference_no'  => ['label' => lang("reference_no"), 'style' => ''],
        'customer'      => ['label' => lang("customer"), 'style' => ''],
        'parent_company'      => ['label' => lang("Parent_Company"), 'style' => '', 'sort_fn' => false],
        'biller'      => ['label' => lang("biller"), 'style' => ''],
        'api_supplier'   => ['label' => lang("Suppliers"), 'style' => '', 'sort_fn' => false],
        'product_qty'      => ['label' => lang("Product (Qty)"), 'style' => '', 'sort_fn' => false],
        'grand_total'   => ['label' => lang("grand_total"), 'style' => ''],
        'paid' => ['label' => lang("Paid"), 'style' => ''],
        'balance' => ['label' => lang("Balance"), 'style' => ''],
        'payment_status' => ['label' => lang("Payment_Status"), 'style' => ''],
    );
?>
                    
                    <table id="" class="table table-bordered table-condensed table-hover table-striped">
                        <thead>
                        <tr class="primary">
<?php foreach ($row_headers as $key => $row) :?>
    <?php
   
    $th_class="class='pointer api_pointer'";
    $e_click = true;
    $temp = 'api_pointer';
    if(isset($row['sort_fn']) && $row['sort_fn'] == false) {
        $th_class="";
        $temp = '';
        $e_click = false;
    }
    $style = isset($row['style']) && $row['style'] != '' ? 'style="'.$row['style'].'"' : '';
    ?>
    <th <?=$th_class?> <?= $style ?> >
        <?php if($e_click ) :?>
            <?php if ($sort_by == $key && $sort_order == "ASC") : ?>
                    <label class="font-normal <?php echo $temp; ?>" onclick="formactionsubmit('<?= $key ?>', 'DESC')" id="fa_sort"  aria-hidden="true">
                    <?php echo $row['label']; ?>
                        <span class="fa fa-sort-down"></span>
                    </label>
            <?php elseif ($sort_by == $key and $sort_order=="DESC") : ?>    
                <label class="font-normal <?php echo $temp; ?>" onclick="formactionsubmit('<?= $key ?>', 'ASC')" id="fa_sort_up"  aria-hidden="true">
                    <?php echo $row['label']; ?>
                    <span class="fa fa-sort-up"></span>
                </label>
            <?php else : ?> 
                <label class="font-normal <?php echo $temp; ?>" onclick="formactionsubmit('<?= $key ?>', 'DESC')" id="fa_sort_down" aria-hidden="true">
                    <?php echo $row['label']; ?>
                    <span class="fa fa-sort"></span>
                </label>
            <?php endif; ?> 
        <?php else : ?>
            <label class="font-normal <?php echo $temp; ?>" aria-hidden="true">
                <?php echo $row['label']; ?>
            </label>
        <?php endif; ?> 
    </th>
<?php endforeach; ?>

                        </tr>
                        </thead>
                        <tbody role="alert" aria-live="polite" aria-relevant="all">

<?php
for ($i=0;$i<count($select_data);$i++) {
    if ($select_data[$i]['payment_status'] == 'pending')
        $temp = '<span class="label label-warning">'.$select_data[$i]['payment_status'].'</span>';
    elseif ($select_data[$i]['payment_status'] == 'completed' || $select_data[$i]['payment_status'] == 'paid' || $select_data[$i]['payment_status'] == 'sent' || $select_data[$i]['payment_status'] == 'received')
        $temp = '<span class="label label-success">'.$select_data[$i]['payment_status'].'</span>';
    elseif ($select_data[$i]['payment_status'] == 'partial' || $select_data[$i]['payment_status'] == 'transferring' || $select_data[$i]['payment_status'] == 'ordered')
        $temp = '<span class="label label-info">'.$select_data[$i]['payment_status'].'</span>';
    elseif ($select_data[$i]['payment_status'] == 'due' || $select_data[$i]['payment_status'] == 'returned')
        $temp = '<span class="label label-danger">'.$select_data[$i]['payment_status'].'</span>';
    else
        $temp = '<span class="label label-default">'.$select_data[$i]['payment_status'].'</span>';

    $temp_ref = explode(':',$select_data[$i]['reference_no']);

    echo '
    <tr id="'.$select_data[$i]['id'].'" class="sale_report_link">
        <td style="min-width:30px; width: 30px; text-align: center;">
            <div class="text-center">
                <input class="checkbox multi-select skip" type="checkbox" name="val[]" value="'.$select_data[$i]['id'].'" onclick="api_setValueCheckbox(\'val[]\',\'check_value\',\'action-form\',\'-\')"/>
            </div>
        </td>
        <td class="api_td_width_auto" align="center">
            '.date('Y-m-d', strtotime($select_data[$i]['date'])).'<br>
            '.date('H:i:s', strtotime($select_data[$i]['date'])).'
        </td>
        <td class="api_td_width_auto" align="center">
            '.$temp_ref[0].':</br>
            '.$temp_ref[1].'
        </td>
    ';

    //------------------------------------
    echo '
        <td>'.$select_data[$i]['customer'].'</td>
        <td>
            '.$select_data[$i]['parent_company'].'
        </td>
        <td>'.$select_data[$i]['biller'].'</td>
        <td style="vertical-align: top !important;">'.$select_data[$i]['suppliers_name'].'</td>
    ';
    //----------------------------------------------------

    $temp_2 = $this->site->api_select_some_fields_with_where("
        id, product_name, quantity   
        "
        ,"sma_sale_items"
        ,"sale_id = ".$select_data[$i]['id']." order by product_name asc"
        ,"arr"
    );
    echo '
        <td>
    ';
    for ($i2=0;$i2<count($temp_2);$i2++) {
        echo $temp_2[$i2]['product_name'].' ('.number_format($temp_2[$i2]['quantity'],2).')<br><br>';
    }    
    echo '
        </td>
    ';


    echo '
        <td class="api_td_width_auto" align="right">'.$this->sma->formatMoney($select_data[$i]['grand_total']).'</td>
        <td class="api_td_width_auto" align="right">'.$this->sma->formatMoney($select_data[$i]['paid']).'</td>
        <td class="api_td_width_auto" align="right">'.$this->sma->formatMoney($select_data[$i]['balance']).'</td>
        <td class="api_td_width_auto" align="center">'.$temp.'</td>
    </tr>
    ';
}
if (count($select_data) <= 0) {
    echo '
    <tr>
    <td colspan="12">
        '.lang('No_record_found').'
    </td>
    </tr>
    ';
}
echo '
    </tbody>
';
?>                            
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th style="min-width:30px; width: 30px; text-align: center;">
                                <input class="checkbox checkft skip" type="checkbox" name="check" onclick="api_select_check_all(this,'val[]'); api_setValueCheckbox('val[]','check_value','action-form','-')"/>
                            </th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>                            
                        </tr>
                        </tfoot>
                    </table>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel-heading-left">
                                <p class="data-total">
                                    <?php echo $show.' ';?>
                                </p>
                            </div>   
                            <div class="panel-heading-right">
                                <p>
                                   <?php echo $this->pagination->create_links(); //pagination links ?>
                                </p>
                            </div>  
                            <div class="panel-heading-clear-fix"></div>   
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" name="show_data" value="<?php echo $show_data;?>" id="show_data"/>
<input type="hidden" name="sort_by" value="<?php echo $sort_by;?>" id="sort_by"/>
<input type="hidden" name="sort_order" value="<?php echo $sort_order;?>" id="sort_order"/>
<?= form_close() ?>

<?php

echo admin_form_open('reports/report_actions','id="api_form_action" name="api_form_action" method="post"');
echo '
    <div class="api_display_none">
            <input type="text" name="check_value" value="" id="check_value"/>
            <input type="text" name="action" value="export_excel" id="action"/>
            <button type="button" onclick="document.api_form_action.submit();" value="ok">ok</button>
    </div>
';
echo form_close();

?>


<style>    
    .modal-body h2{
        font-size: 20px;
        margin-top: 8px;
    }
    .modal-body p{
        font-size: 18px;
        margin-bottom: 17px;
    }
    #tbl-update_to_due {
        position: relative;
        padding: 0px 15px 15px 15px !important;
        margin-top:15px;
    }
    #tbl-update_to_paid {
        position: relative;
        padding: 0px 15px 15px 15px !important;
        margin-top:15px;
    }
    #tbl-update_to_partail {
        position: relative;
        padding: 0px 15px 15px 15px !important;
        margin-top:15px;
    }
    h4.modal-title{
        font-size: 18px;
    }
    .panel-heading-left {
        float: left;
    }
    .panel-heading-right {
        float: right;
    }
    .pointer{
        cursor:pointer;
    }
    .font-normal{
        font-style: normal;
    }
    
    .pagination>li>a, .pagination>li>span {
        position: relative;
        float: left;
        padding: 5px 10px;
        margin-left: -1px;
        line-height: 1.42857143;
        color: #428bca;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
        font-size: 13px;
    }
    .pagination {
        display: inline-block;
        padding-left: 0;
        margin: -5px 0;
        border-radius: 4px;
    }
    .txt-f{
        color:#333 !important
    }
    #loading{
        display:none !important;
        visibility : hidden;
    }
</style>

<script>
     
    function formactionsubmit(sort_by, sort_order) {
        document.getElementById("sort_by").value = sort_by;
        document.getElementById("sort_order").value = sort_order;
        document.forms[0].submit();
    }

function api_form_submit(action){
    $("#action-form").attr('action', action);
    //$("#action-form").attr('method', 'post');
    $('#form_action').val('');
    $("#action-form").submit();
}     
<?php
    $temp = $_GET['page'];
    if ($temp == '') $temp = 1;
    echo '
        $(".pagination .active").html(\'<a href="javascript:void(0);">'.$temp.'</a>\');
    ';
?>
</script>

