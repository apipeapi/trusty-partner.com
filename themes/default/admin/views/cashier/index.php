<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php 

if ($Owner) {
    $spage = $show_data != '' ? $show_data : 1;
    echo admin_form_open('cashier?page='.$spage, 'id="action-form" name="action-form"');
}
?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-building"></i><?= lang('cashier'); ?></h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon fa fa-tasks tip" data-placement="left" title="<?= lang("actions") ?>"></i>
                    </a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?= admin_url('cashier/edit') ?>" data-toggle="modal" data-target="#myModal" id="add">
                                <i class="fa fa-plus-circle"></i> <?= lang('add').' '.lang('cashier') ?>
                            </a>
                        </li>                    
                        <li class="divider"></li>
                        <li>
                            <a href="#" class="bpo" title="<b><?= $this->lang->line("delete") ?></b>"
                                data-content="<p><?= lang('r_u_sure') ?></p><button type='button' class='btn btn-danger' onclick='bulk_actions_delete();' data-action='delete'><?= lang('i_m_sure') ?></a> <button class='btn bpo-close'><?= lang('no') ?></button>"
                                data-html="true" data-placement="left">
                                <i class="fa fa-trash-o"></i> <?= lang('delete') ?>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <p class="introtext"><?= lang('list_results'); ?></p>

                <div class="table-responsive">
                        <div class="row">
                            <div class="sale-header">

                                <div class="col-md-6 text-left">
                                    <div class="short-result">
                                        <label>
                                            Show
                                        </label>
                                        <select id="sel_id" name="per_page" onchange="document.action-form.submit()">
                                            <?php
                                                $per_page_arr = array(
                                                    '10' => '10',
                                                    '25' => '25',
                                                    '50' => '50',
                                                    '100' => '100',
                                                    '200' => '200',
                                                    'All' => $total_rows_sale
                                                );
                                            
                                                foreach($per_page_arr as $key =>$value){
                                                    $select = $value == $per_page?'selected':'';
                                                    echo '<option value="'.$value.'" '.$select.'>';
                                                            echo $key;
                                                    echo '</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6 text-right">
                                    <div class="short-result">
                                        <div id="SLData_filter" class="show-data-search dataTables_filter">
                                            <label>
                                                Search
                                                <input class="input-xs" value="<?php echo $this->input->post('search');?>" type="text" name="search" id="mysearch" placeholder="search"/>
                                                <input style="height: 34px;" class="btn btn-primary" type="submit" name="submit_search" value="Submit">
                                            </label>
                                            <div id="result"></div>
                                        </div>
                                    </div>    
                                </div>	
                                <div class="clearfix"></div>
                             </div>
                        </div>     
                    </div> 
                
                    <table id="SLData cellpadding="0" cellspacing="0" border="0"
                           class="table table-bordered table-hover table-striped">
                        <thead>
<?php
    $row_headers = array(
        'checkbox'      => [
                            'label' => '<input class="checkbox checkft" type="checkbox" name="check"/>', 
                            'sort_fn' => false,
                            'style' => 'min-width:30px; width: 30px; text-align: center;'
                        ],
        'title'          => ['label' => lang("fullname"), 'style' => 'width:auto;'],
        'add_ons_email'  => ['label' => lang("email_address"), 'style' => 'width:auto;'],
        'add_ons_phone'  => ['label' => lang("phone"), 'style' => 'width:140px;'],
        'add_ons_company'  => ['label' => lang("company"), 'style' => 'width:auto;'],
        'action'        => [
                            'label' => lang("actions"), 
                            'style' => 'width:80px; text-align:center;', 
                            'sort_fn' => false
                        ],
    );
?>

                        
                        <tr class="">
                                <?php foreach ($row_headers as $key => $row) :?>
                                    <?php
                                   
                                    $th_class="class='pointer'";
                                    $e_click = true;
                                    if(isset($row['sort_fn']) && $row['sort_fn'] == false) {
                                        $th_class="";
                                        $e_click = false;
                                    }
                                    $style = isset($row['style']) && $row['style'] != '' ? 'style="'.$row['style'].'"' : '';
                                    ?>
                                    <th <?= $style ?> >
                                        <?php if($e_click ) :?>
                                            <?php if ($sort_by == $key && $sort_order == "ASC") : ?>
                                                    <label class="font-normal" onclick="formactionsubmit('<?= $key ?>', 'DESC')" id="fa_sort"  aria-hidden="true">
                                                    <?php echo '<span class="pointer">'.$row['label'].'</span>'; ?>
                                                        <span class="fa fa-sort-down"></span>
                                                    </label>
                                            <?php elseif ($sort_by == $key and $sort_order=="DESC") : ?>	
                                                <label class="font-normal" onclick="formactionsubmit('<?= $key ?>', 'ASC')" id="fa_sort_up"  aria-hidden="true">
                                                    <?php echo '<span class="pointer">'.$row['label'].'</span>'; ?>
                                                    <span class="fa fa-sort-up"></span>
                                                </label>
                                            <?php else : ?>	
                                                <label class="font-normal" onclick="formactionsubmit('<?= $key ?>', 'DESC')" id="fa_sort_down" aria-hidden="true">
                                                    <?php echo '<span class="pointer">'.$row['label'].'</span>'; ?>
                                                    <span class="fa fa-sort"></span>
                                                </label>
                                            <?php endif; ?>	
                                        <?php else : ?>
                                            <label class="font-normal" aria-hidden="true">
                                                <?php echo $row['label']; ?>
                                            </label>
                                        <?php endif; ?>	
                                    </th>
                                <?php endforeach; ?>

                        </tr>
                        </thead>
                        <tbody>
<?php
for ($i=0;$i<count($select_data);$i++) {
    echo '
        <tr id="'.$select_data[$i]['id'].'" class="api_table_link_delivery_person">
        <td style="min-width:30px; width: 30px; text-align: center;">
            <div class="text-center">
                <input class="checkbox multi-select" type="checkbox" 
                    name="val[]" value="'.$select_data[$i]['id'].'" />
            </div>
        </td>
        <td>
            '.$select_data[$i]['title'].'
        </td>
        <td>
            '.$select_data[$i]['email'].'
        </td>
        <td>
            '.$select_data[$i]['phone'].'
        </td>
        <td>
            '.$select_data[$i]['company'].'
        </td>
        <td align="center">
            <a class="tip" title="" href="'.admin_url('cashier/edit/'.$select_data[$i]['id']).'" data-toggle="modal" data-target="#myModal" data-original-title="Edit"><i class="fa fa-edit"></i>
            </a>
            <a href="#" class="tip po" title="<b>'.$this->lang->line("delete").'</b>"
                data-content="<p>'.lang('r_u_sure').'</p> <a href=\''.admin_url('cashier/delete/'.$select_data[$i]['id']).'\'><button type=\'button\' class=\'btn btn-danger\' data-action=\'delete\'>'.lang('i_m_sure').'</button></a> <button class=\'btn po-close\'>'.lang('no').'</button>"
                data-html="true" data-placement="left">
                <i class="fa fa-trash-o"></i>
            </a>
        </td>
        </tr>
    ';    
}
if (count($select_data) <= 0)
    echo '
    <tr>
    <td colspan="5">
        '.lang('no_record_found').'
    </td>
    </tr>
    ';
?>           



                        </tbody>
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th style="min-width:30px; width: 30px; text-align: center;">
                                <input class="checkbox checkft" type="checkbox" name="check"/>
                            </th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th style="width:100px; text-align: center;"><?= lang("actions"); ?></th>
                        </tr>
                        </tfoot>
                    </table>
                    <div class="row">
                        <div class="col-md-6 text-left">
                            <div class="dataTables_info" id="EXPData_info">
                                    <?php echo $show;?>
                            </div>
                        </div>
                        <div class="col-md-6 text-right">
                            <div class="dataTables_paginate paging_bootstrap">
                               <?php echo $this->pagination->create_links(); //pagination links ?>
                            </div>
                        </div>
                    </div>

                    
                </div>
            </div>
        </div>
    </div>
</div>

<?php 
if ($Owner) { 
?>
    <div style="display: none;">
        <input type="hidden" name="show_data" value="<?php echo $show_data;?>" id="show_data"/>
        <input type="hidden" name="sort_by" value="<?php echo $sort_by;?>" id="sort_by"/>
        <input type="hidden" name="sort_order" value="<?php echo $sort_order;?>" id="sort_order"/>    
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php 
} 
?>

<script>
function formactionsubmit(sort_by, sort_order) {
    document.getElementById("sort_by").value = sort_by;
    document.getElementById("sort_order").value = sort_order;
    document.forms[0].submit();
}

function bulk_actions_delete(){
    $("#action-form").attr('action', 'admin/cashier/bulk_actions');
    $('#form_action').val('delete');
    $("#action-form").submit();
}       
</script>

