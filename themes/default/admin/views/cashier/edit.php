<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
<?php
    if ($select_data[0]['id'] != '') {
        $temp_title = lang('edit').' '.lang('cashier');
    }
    else {
        $temp_title = lang('add').' '.lang('cashier');
    }
?>            
            <h4 class="modal-title" id="myModalLabel"><?php echo $temp_title; ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => $form_name, 'name' => $form_name);
        echo admin_form_open_multipart("cashier/edit/".$select_data[0]['id'], $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="row">
				<div class="col-md-12">
					<div class="form-group">
                        <div class="form-group">
                            <?= lang("fullname", "fullname").' *'; ?>
                            <?php echo form_input('fullname', $select_data[0]['title'], 'class="form-control" id="fullname" required="required"'); ?>
                        </div>
					</div>
				</div>
                <div class="col-md-12 ">
                    <div class="form-group">
                        <?php echo lang('email', 'email'); ?>
                        <div class="controls">
                            <input type="email" id="add_ons_email" name="add_ons_email" value="<?= $select_data[0]['email']; ?>" class="form-control" />
                        </div>
                    </div>
                </div>                
				<div class="col-md-12 ">
					<div class="form-group">
                        <div class="form-group">
                            <?= lang("phone", "phone"); ?>
                            <?php echo form_input('add_ons_phone', $select_data[0]['phone'], 'class="form-control" id="add_ons_phone" '); ?>
                        </div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
                        <div class="form-group">
                            <?= lang("company", "company"); ?>
                            <?php echo form_input('add_ons_company', $select_data[0]['company'], 'class="form-control" id="add_ons_company"'); ?>
                        </div>
					</div>
				</div>

                <div class="col-md-12">
                    <div class="form-group">
                        <?= lang("address", "address"); ?>
                        <?php echo form_textarea('add_ons_address', $select_data[0]['address'], 'class="form-control" id="add_ons_address" style="height: 100px;"'); ?>
                    </div>
                </div>                
            </div>


        </div>
        <div class="modal-footer">
            <?php echo form_submit('submit', $temp_title, 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
