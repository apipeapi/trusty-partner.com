<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script>
    $(document).ready(function () {
        oTable = $('#NTTable').dataTable({
            "aaSorting": [[1, "asc"], [2, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= admin_url('notifications/getNotifications') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [null, {"mRender": fld}, {"mRender": fld}, {"mRender": fld}, {"bSortable": false}]
        });
    });
</script>

<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-info-circle"></i><?= lang('notifications'); ?></h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown"><a href="<?= admin_url('notifications/add'); ?>" data-toggle="modal"
                                        data-target="#myModal"><i class="icon fa fa-plus"></i></a></li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <p class="introtext"><?= lang('list_results'); ?></p>

                <div class="table-responsive">
                    <table id="NTTable" cellpadding="0" cellspacing="0" border="0"
                           class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th><?php echo $this->lang->line("notification"); ?></th>
                            <th style="width: 140px;"><?php echo $this->lang->line("submitted_at"); ?></th>
                            <th style="width: 140px;"><?php echo $this->lang->line("from"); ?></th>
                            <th style="width: 140px;"><?php echo $this->lang->line("till"); ?></th>
                            <th style="width:80px;"><?php echo $this->lang->line("actions"); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="5" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                        </tr>

                        </tbody>
                    </table>
                </div>
                <!--<p><a href="<?php echo admin_url('notifications/add'); ?>" class="btn btn-primary" data-toggle="modal" data-target="#myModal"><?php echo $this->lang->line("add_notification"); ?></a></p>-->
            </div>
        </div>
    </div>
</div>

<?php

//need sma_product_discount_inv2016, sma_companies_inv2016
//add temp_email to sma_product_discount_inv2016

// $temp = $this->site->api_select_some_fields_with_where("
//     *
//     "
//     ,"sma_product_discount_inv2016"
//     ,"id > 0"
//     ,"arr"
// );
// $temp2 = $this->site->api_select_some_fields_with_where("
//     *
//     "
//     ,"sma_products"
//     ,"id > 0"
//     ,"arr"
// );
// for ($i=0;$i<count($temp);$i++) {
//     $b = 0;
//     for ($i2=0;$i2<count($temp2);$i2++) {
//         if ($temp[$i]['product_code'] == $temp2[$i2]['code']) {
//             $temp3 = array(
//                 'product_id' => $temp2[$i2]['id']
//             );            
//             $this->db->update('sma_product_discount_inv2016', $temp3,'id = '.$temp[$i]['id']);
//         }        
//     }
// }

// $temp = $this->site->api_select_some_fields_with_where("
//     *
//     "
//     ,"sma_product_discount_inv2016"
//     ,"id > 0"
//     ,"arr"
// );
// $temp2 = $this->site->api_select_some_fields_with_where("
//     *
//     "
//     ,"sma_companies_inv2016"
//     ,"id > 0"
//     ,"arr"
// );
// for ($i=0;$i<count($temp);$i++) {
//     $b = 0;
//     for ($i2=0;$i2<count($temp2);$i2++) {
//         if ($temp[$i]['customer_id'] == $temp2[$i2]['id']) {
//             $temp3 = array(
//                 'temp_email' => $temp2[$i2]['email']
//             );            
//             $this->db->update('sma_product_discount_inv2016', $temp3,'id = '.$temp[$i]['id']);
//         }        
//     }
// }


// $temp = $this->site->api_select_some_fields_with_where("
//     *
//     "
//     ,"sma_product_discount_inv2016"
//     ,"id > 0"
//     ,"arr"
// );
// $temp2 = $this->site->api_select_some_fields_with_where("
//     *
//     "
//     ,"sma_companies"
//     ,"id > 0"
//     ,"arr"
// );
// for ($i=0;$i<count($temp);$i++) {
//     $b = 0;
//     for ($i2=0;$i2<count($temp2);$i2++) {
//         if ($temp[$i]['temp_email'] == $temp2[$i2]['email']) {
//             $temp3 = array(
//                 'customer_id' => $temp2[$i2]['id']
//             );            
//             $this->db->update('sma_product_discount_inv2016', $temp3,'id = '.$temp[$i]['id']);
//         }        
//     }
// }



// $date = date('Y-m-d');
// $temp2 = $this->site->api_select_some_fields_with_where("
//     *     
//     "
//     ,"sma_product_discount_inv2016"
//     ,"id > 0 and start_date <= '".$date."' and end_date >= '".$date."'"
//     ,"arr"
// );
// for ($i=0;$i<count($temp2);$i++) {
//     $temp3 = array();
//     foreach ($temp2[$i] as $key => $value) {
//         if ($key != 'id' && $key != 'temp_email')
//             $temp3[$key] = $value;
//     }
//     $this->db->insert('sma_product_discount', $temp3);
// }


// $config_data = array(
//     'table_name' => 'sma_categories',
//     'select_table' => 'sma_categories',
//     'translate' => 'yes',
//     'select_condition' => "id = 56",
// );
// $temp = $this->site->api_select_data_v2($config_data);
// for ($i=0;$i<count($temp);$i++) {
//     $config_data = array(
//         'table_name' => 'sma_categories',
//         'id_name' => 'id',
//         'field_add_ons_name' => 'translate',
//         'selected_id' => $temp[$i]['id'],
//         'add_ons_title' => 'ch',
//         'add_ons_value' => $temp[$i]['title_en'],                    
//     );
//     $this->site->api_update_translate_field($config_data);       
// }



// $this->db->delete('sma_sales', "id > 0");
// $this->db->delete('sma_sale_items', "id > 0");

// $this->db->delete('sma_returns', "id > 0");
// $this->db->delete('sma_return_items', "id > 0");

// $this->db->delete('sma_adjustments', "id > 0");
// $this->db->delete('sma_adjustment_items', "id > 0");

// $this->db->delete('sma_purchases', "id > 0");
// $this->db->delete('sma_purchase_items', "id > 0");


//$this->db->delete('sma_expenses', "id > 0");

// $this->db->delete('sma_product_variants', "id > 0");
// $this->db->delete('sma_products', "id > 0");
// $this->db->delete('sma_warehouses_products', "id > 0");
// $this->db->delete('sma_products', "id > 0");
// $this->db->delete('sma_warehouses_products_variants', "id > 0");
// $this->db->delete('sma_wishlist', "id > 0");
// $this->db->delete('sma_variants', "id > 0");
// $this->db->delete('sma_product_discount', "id > 0");

//$this->db->delete('sma_categories', "id != 60");


//$this->db->delete('sma_companies', "id != 1 and id != 3 and id != 691");
//$this->db->delete('sma_users', "id != 37 and id != 4");

?>


