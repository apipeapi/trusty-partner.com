<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?= lang('add_currency'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open("system_settings/add_currency", $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="form-group">
                <?= lang("date", "date"); ?>
                <?php echo form_input('date', (isset($_POST['date']) ? $_POST['date'] : date("d/m/Y H:i")), 'class="form-control datetime" autocomplete="off"'); ?>
            </div>

            <div class="form-group">
                <?= lang('currency_code', 'code'); ?>
                <?= form_input('code', set_value('code'), 'class="form-control tip" id="code" required="required"'); ?>
            </div>

            <div class="form-group">
                <?= lang('currency_name', 'name'); ?>
                <?= form_input('name', set_value('name'), 'class="form-control tip" id="name" required="required"'); ?>
            </div>

            <div class="form-group">
                <?= lang('symbol', 'symbol'); ?>
                <?= form_input('symbol', set_value('symbol'), 'class="form-control tip" id="symbol" required="required"'); ?>
            </div>

            <div class="form-group">
                <?= lang('exchange_rate', 'rate'); ?>
                <?= form_input('rate', set_value('rate'), 'class="form-control tip" id="rate"  required="required"'); ?>
            </div>

            <div class="form-group">
                <input type="checkbox" value="1" name="auto_update" id="auto_update">
                <label class="padding-left-10" for="auto_update"><?= lang("auto_update_rate"); ?></label>
            </div>

<?php
    $temp_1 = set_radio('add_ons_yesterday_kh_rate', '', true); 
    $temp_2 = set_radio('add_ons_yesterday_kh_rate', '1');

    echo '
    <div class="form-group">
        '.lang("Verified", "Verified").'
            <div class="">
                <div class="api_height_10"></div>
                <input type="radio" id="add_ons_yesterday_kh_rate" name="add_ons_yesterday_kh_rate" value="" '.$temp_1.' />
                Yes
                <span class="api_padding_left_10">
                    <input type="radio" id="add_ons_yesterday_kh_rate" name="add_ons_yesterday_kh_rate" value="1" '.$temp_2.' />
                </span>
                No
            </div>
    </div>                
    ';
?>            
        </div>
        <div class="modal-footer">
            <?= form_submit('add_currency', lang('add_currency'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?= form_close(); ?>
</div>
<?= $modal_js ?>
