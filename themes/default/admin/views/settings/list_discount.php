<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php
	$v="";
	if ($this->input->post('start_date')) {
		$v .= "&start_date=" .$this->input->post('start_date');
	}
	if ($this->input->post('end_date')) {
		$v .= "&end_date=" .$this->input->post('end_date');
	}
?>
<script>
    $(document).ready(function () {
        oTable = $('#CGData').dataTable({
            //"aaSorting": [[0, "desc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= admin_url('system_settings/getlist_discount/?1'.$v) ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [{
				"bSortable": false, 
				"mRender": checkbox}, 
				{"bSortable": false},
				{"bSortable": false},
				{"bSortable": false},
				{"bSortable": false},
				{"bSortable": false, "mRender": currencyFormat},  
                {"bSortable": false, "mRender": currencyFormat}, 
				{"bSortable": false}, 
				{"bSortable": false}, 
				{"bSortable": false, "mRender": status_ex},
				{"bSortable": false}]
        });
    });
</script>
<?= admin_form_open('system_settings/list_discount_actions', 'id="action-form"') ?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-building"></i><?= $page_title ?></h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-tasks tip" data-placement="left" title="<?= lang("actions") ?>"></i></a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li><a href="<?php echo admin_url('system_settings/add_product_discount'); ?>" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> <?= lang('add_product_discount') ?></a></li>
                        <li class="divider"></li>
                        <li><a href="#" id="delete" data-action="delete"><i class="fa fa-trash-o"></i> <?= lang('delete_product_discount') ?></a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
	
	<div style="display: none;">
		<input type="hidden" name="form_action" value="" id="form_action"/>
		<?= form_submit('submit', 'submit', 'id="action-form-submit"') ?>
		</div>
	<?= form_close() ?>

    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?php echo $this->lang->line("list_results_product_discount"); ?></p>
				<div class="form">
					<?php echo admin_form_open("system_settings/list_discount"); ?>
					<div class="col-sm-3">
						<?= lang("from_date","from_date") ?>
						<div class="form-group">					
							<?= form_input('start_date', set_value('start_date'), 'class="form-control tip date" id="start_date"'); ?>
						</div>
					</div>	
					<div class="col-sm-3">
						<?= lang("end_date","end_date") ?>
						<div class="form-group">					
							<?= form_input('end_date', set_value('end_date'), 'class="form-control tip date" id="end_date"'); ?>
						</div>
					</div>					
					<div class="col-sm-3">
						<?= lang("&nbsp;","&nbsp;") ?>
						<div class="form-group">
							<button type="submit" class="btn btn-warning" >
								<?= lang("search") ?>
								<i class="fa fa-search"></i>
							</button>
						</div>
					</div>
					<?php echo form_close(); ?>
				</div>
				
                <div class="table-responsive">
                    <table id="CGData" class="table table-bordered table-hover table-striped reports-table">
                        <thead>
                        <tr>
                            <th style="min-width:30px; width: 30px; text-align: center;">
                                <input class="checkbox checkth" type="checkbox" name="check"/>
                            </th>
                            <th><?= lang("customer_name"); ?></th>
                            <th><?= lang("sale_person"); ?></th>
                            <th class="width-100"><?= lang("product_code"); ?></th>
                            <th><?= lang("product_name"); ?></th>
                            <th><?= lang("normal_price"); ?></th>
                            <th><?= lang("special_price"); ?></th>
                            <th class="width-100"><?= lang("start_date"); ?></th>
                            <th class="width-100"><?= lang("end_date"); ?></th>
                            <th class="width-100"><?= lang("status_product_discount"); ?></th>
                            <th style="width:65px;"><?php echo $this->lang->line("actions"); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="10" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                        </tr>

                        </tbody>
                    </table>
                </div>

            </div>

        </div>
    </div>
</div>


<script language="javascript">
    $(document).ready(function () {

        $('#delete').click(function (e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });

        $('#excel').click(function (e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });

        $('#pdf').click(function (e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });

    });
</script>

