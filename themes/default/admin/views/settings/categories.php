<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<?php

if ($Owner) {
    $spage = $show_data != '' ? $show_data : 1;
    
    echo admin_form_open('system_settings/categories?page='.$spage, 'id="action-form" name="action-form"');
}

?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-folder-open"></i><?= lang('categories'); ?></h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon fa fa-tasks tip" data-placement="left" title="<?= lang("actions") ?>"></i>
                    </a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?php echo admin_url('system_settings/add_category'); ?>" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-plus"></i> <?= lang('add_category') ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo admin_url('system_settings/import_categories'); ?>" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-plus"></i> <?= lang('import_categories') ?>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);" onclick="api_bulk_actions('export_excel');">
                                <i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#" class="bpo" title="<b><?=lang("delete_categories")?></b>" data-content="<p><?=lang('r_u_sure')?></p>
                            <button type='button' class='btn btn-danger' onclick='bulk_actions_delete();' data-action='delete'><?=lang('i_m_sure')?></a> <button class='btn bpo-close'><?=lang('no')?></button>" data-html="true" data-placement="left">
                                <i class="fa fa-trash-o"></i> <?=lang('delete_categories')?>
                            </a>
                        </li>                        
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <p class="introtext"><?= lang('list_results'); ?></p>

                <div class="table-responsive">
                        <div class="row">
                            <div class="sale-header">
                                <div class="col-md-12 api_padding_0">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label" for="parent_id">
                                            <?php echo lang("parent_category"); ?></label>
                                            <div class="api_height_5"></div>
                                            <div class="controls"> 
                                                <?php

                                            $tr[''] = lang("Please_select_a_parent_category");
                                            if (is_array($categories)) {
                                                if (count($categories) > 0) {
                                                    for ($i=0;$i<count($categories);$i++) {
                                                        $tr[$categories[$i]['id']] = $categories[$i]['name'];
                                                    }
                                                }
                                            }
                                            echo form_dropdown('parent_id', $tr, $_POST['parent_id'], 'data-placeholder="'.lang("Please_select_a_parent_category").'" class="form-control" onchange="admin_categories_parent_category_change(this.value);" ');

                                                ?>
                                            </div>
                                        </div>
                                    </div>                                    
                                    <div class="api_clear_both api_height_15"></div>
                                </div>

                                <div class="col-md-6 text-left">
                                    <div class="short-result">
                                        <label>
                                            Show
                                        </label>
                                        <select id="sel_id" name="per_page" onchange="$('#action-form').attr('action','<?php echo base_url(); ?>admin/system_settings/categories?page=<?php echo $spage; ?>' + '&per_page=' + document.getElementById('sel_id').value); document.action-form.submit()">
                                            <?php
                                                $per_page_arr = array(
                                                    '10' => '10',
                                                    '25' => '25',
                                                    '50' => '50',
                                                    '100' => '100',
                                                    '200' => '200',
                                                    'All' => $total_rows_sale
                                                );
                                            
                                                foreach ($per_page_arr as $key =>$value) {
                                                    $select = $value == $per_page?'selected':'';
                                                    echo '<option value="'.$value.'" '.$select.'>';
                                                    echo $key;
                                                    echo '</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6 text-right">
                                    <div class="short-result">
                                        <div id="SLData_filter" class="show-data-search dataTables_filter">
                                            <label>
                                                Search
                                                <input class="input-xs" value="<?php echo $this->input->post('search');?>" type="text" name="search" id="mysearch" placeholder="search"/>
                                                <input style="height: 34px;" class="btn btn-primary" type="submit" name="submit_search" value="Submit">
                                            </label>
                                            <div id="result"></div>
                                        </div>
                                    </div>    
                                </div>  
                                <div class="clearfix"></div>
                             </div>
                        </div>     
                    </div> 
                
                    <table cellpadding="0" cellspacing="0" border="0"
                           class="table table-bordered table-hover table-striped">
                        <thead>
<?php
    $row_headers = array(
        'checkbox'      => [
            'label' => '<input class="checkbox checkft" type="checkbox" name="check"/>',
            'sort_fn' => false,
            'style' => 'min-width:30px; max-width: 30px; text-align: center;'
        ],
        'image'          => [
            'sort_fn' => false,
            'label' => lang("image"), 'style' => 'min-width:80px; width: 80px; text-align: center;'
        ],
        'code'  => [
            'label' => lang("code"), 'style' => 'min-width:100px; width: 100px;'
        ],
        'name'      => [
            'label' => lang("category_name"), 'style' => 'text-align:left;'
        ],
        'slug'      => [
            'label' => lang("slug"), 'style' => 'text-align:left;'
        ],
        'parent_id'      => [
            'label' => lang("parent_category"), 'style' => 'text-align:left;'
        ],
        'order'      => [
            'sort_fn' => false,
            'label' => lang("Ordering"), 'style' => 'text-align:center; width:80px;'
        ],
        'display'      => [
            'label' => lang("Display"), 'style' => 'width:100px;'
        ],
        'action'        => [
            'label' => lang("actions"),
            'style' => 'width:80px; text-align:center;',
            'sort_fn' => false
        ],
    );
?>

                        
                        <tr class="">
                                <?php foreach ($row_headers as $key => $row) :?>
                                    <?php
                                   
                                    $th_class="class='pointer'";
                                    $e_click = true;
                                    if (isset($row['sort_fn']) && $row['sort_fn'] == false) {
                                        $th_class="";
                                        $e_click = false;
                                    }
                                    $style = isset($row['style']) && $row['style'] != '' ? 'style="'.$row['style'].'"' : '';
                                    ?>
                                    <th <?= $style ?> >
                                        <?php if ($e_click) :?>
                                            <?php if ($sort_by == $key && $sort_order == "ASC") : ?>
                                                    <label class="font-normal" onclick="formactionsubmit('<?= $key ?>', 'DESC')" id="fa_sort"  aria-hidden="true">
                                                    <?php echo '<span class="pointer">'.$row['label'].'</span>'; ?>
                                                        <span class="fa fa-sort-down"></span>
                                                    </label>
                                            <?php elseif ($sort_by == $key and $sort_order=="DESC") : ?>    
                                                <label class="font-normal" onclick="formactionsubmit('<?= $key ?>', 'ASC')" id="fa_sort_up"  aria-hidden="true">
                                                    <?php echo '<span class="pointer">'.$row['label'].'</span>'; ?>
                                                    <span class="fa fa-sort-up"></span>
                                                </label>
                                            <?php else : ?> 
                                                <label class="font-normal" onclick="formactionsubmit('<?= $key ?>', 'DESC')" id="fa_sort_down" aria-hidden="true">
                                                    <?php echo '<span class="pointer">'.$row['label'].'</span>'; ?>
                                                    <span class="fa fa-sort"></span>
                                                </label>
                                            <?php endif; ?> 
                                        <?php else : ?>
                                            <label class="font-normal" aria-hidden="true">
                                                <?php echo $row['label']; ?>
                                            </label>
                                        <?php endif; ?> 
                                    </th>
                                <?php endforeach; ?>

                        </tr>
                        </thead>
                        <tbody>
<?php

if (is_array($select_data)) {
    if (count($select_data) > 0) {
        for ($i=0;$i<count($select_data);$i++) {
            if ($select_data[$i]['display'] == 'no') {
        $temp_display = '
            <a href="javascript:void(0);" onclick="
    var postData = {
        \'ajax_file\' : \''.admin_url('system_settings/category_update_display').'\',
        \'field_id\' : \''.$select_data[$i]['id'].'\',
        \'element_id\' : \'api_page_admin_category_display_'.$select_data[$i]['id'].'\',
    };
    api_ajax_category_update_display(postData);  
            ">
                <span class="label label-danger">
                <i class="fa fa-times"></i> No</span>
            </a>
        ';
    }
    else {
        $temp_display = '
            <a href="javascript:void(0);" onclick="
    var postData = {
        \'ajax_file\' : \''.admin_url('system_settings/category_update_display').'\',
        \'field_id\' : \''.$select_data[$i]['id'].'\',
        \'element_id\' : \'api_page_admin_category_display_'.$select_data[$i]['id'].'\',
    };
    api_ajax_category_update_display(postData);  
            ">
                <span class="label label-success">
                <i class="fa fa-check"></i> Yes</span>
            </a>        
        ';
    }


            if ($select_data[$i]['parent_id'] > 0)
                $temp_display2 = '';


            echo '
        <tr id="'.$select_data[$i]['id'].'" class="api_table_link_categories">
        <td style="min-width:30px; max-width: 30px; text-align: center;">
            <div class="text-center">
                <input class="checkbox multi-select" type="checkbox" 
                    name="val[]" value="'.$select_data[$i]['id'].'" />
            </div>
        </td>
        <td align="center">
            ';
            if (is_file('assets/uploads/'.$select_data[$i]['image']) && $select_data[$i]['image'] != 'no_image.png')
            echo '
                <a href="'.site_url('assets/uploads/'.$select_data[$i]['image']).'" data-toggle="lightbox">
                    <img src="'.site_url('assets/uploads/thumbs/'.$select_data[$i]['image']).'" alt="" style="width:30px; height:30px;">
                </a>
            ';            
    echo '
        </td>
        <td align="center">
            '.$select_data[$i]['code'].'
        </td>
        <td>
            '.$select_data[$i]['name'].'
        </td>
        <td>
            '.$select_data[$i]['slug'].'
        </td>
        <td>
        '.$select_data[$i]['parent_id_name'].'
        </td>
        <td align="center">
            <input type="text" class="form-control" value="'.$select_data[$i]['ordering'].'" style="width:50px; text-align:center;" onchange="
            var postData = {
                \'table_name\' : \'sma_categories\', 
                \'field_id\' : '.$select_data[$i]['id'].',                
                \'field_value\' : this.value,
                \'redirect\' : \''.base_url().'admin/system_settings/categories\',
            };
            api_ajax_update_ordering(postData);            
            "/>  
        </td>
    ';
        

        echo '<td align="center">
            <div id="api_page_admin_category_display_'.$select_data[$i]['id'].'">
                '.$temp_display.'
            </div>
        </td>
        <td align="center">
            <a href="'.admin_url('products/print_barcodes/?category='.$select_data[$i]['id']).'" title="" class="tip" data-original-title="Print Barcodes"><i class="fa fa-print"></i>
            </a> 
            <a href="'.admin_url('system_settings/edit_category/'.$select_data[$i]['id']).'" data-toggle="modal" data-target="#myModal" class="tip" title="" data-original-title="Edit Category"><i class="fa fa-edit"></i>
            </a> 

            <a href="#" class="tip po" title="<b>'.$this->lang->line("delete").'</b>"
                data-content="<p>'.lang('r_u_sure').'</p> <a href=\''.admin_url('system_settings/delete_category/'.$select_data[$i]['id']).'\'><button type=\'button\' class=\'btn btn-danger\' data-action=\'delete\'>'.lang('i_m_sure').'</button></a> <button class=\'btn po-close\'>'.lang('no').'</button>"
                data-html="true" data-placement="left">
                <i class="fa fa-trash-o"></i>
            </a>
        </td>
        </tr>
    ';
        }
    }
}
if (count($select_data) <= 0) {
    echo '
    <tr>
    <td colspan="8">
        '.lang('no_record_found').'
    </td>
    </tr>
    ';
}
?>           



                        </tbody>
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th style="min-width:30px; width: 30px; text-align: center;">
                                <input class="checkbox checkft" type="checkbox" name="check"/>
                            </th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            
                            <th style="width:100px; text-align: center;"><?= lang("actions"); ?></th>
                        </tr>
                        </tfoot>
                    </table>
                    <div class="row">
                        <div class="col-md-6 text-left">
                            <div class="dataTables_info" id="EXPData_info">
                                    <?php echo $show;?>
                            </div>
                        </div>
                        <div class="col-md-6 text-right">
                            <div class="dataTables_paginate paging_bootstrap">
                               <?php echo $this->pagination->create_links(); //pagination links?>
                            </div>
                        </div>
                    </div>

                    
                </div>
            </div>
        </div>
    </div>
</div>

<?php
if ($Owner) {
    ?>
    <div style="display: none;">
        <input type="hidden" name="show_data" value="<?php echo $show_data; ?>" id="show_data"/>
        <input type="hidden" name="sort_by" value="<?php echo $sort_by; ?>" id="sort_by"/>
        <input type="hidden" name="sort_order" value="<?php echo $sort_order; ?>" id="sort_order"/>    
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php
}
?>

<script>
function formactionsubmit(sort_by, sort_order) {
    document.getElementById("sort_by").value = sort_by;
    document.getElementById("sort_order").value = sort_order;
    document.forms[0].submit();
}

function api_bulk_actions(action){
    $("#action-form").attr('action', 'admin/system_settings/category_actions');
    $('#form_action').val(action);
    $("#action-form").submit();
}  
function bulk_actions_delete(){
    $("#action-form").attr('action', 'admin/system_settings/category_actions');
    $('#form_action').val('delete');
    $("#action-form").submit();
}
function admin_categories_parent_category_change(value){
    $("#action-form").attr('action', 'admin/system_settings/categories?page=<?= $spage; ?>&parent_id=' + value);
    $("#action-form").submit();
}

function api_ajax_category_update_display(postData){
    var result = $.ajax
    (
        {
            url: postData['ajax_file'],
            type: 'GET',
            secureuri:false,
            dataType: 'html',
            data:postData,
            async: false,
            error: function (response, status, e)
            {
                alert(e);
            }
        }
    ).responseText;
// var myWindow = window.open("", "MsgWindow", "width=700, height=400");
// myWindow.document.write(result);     
    var array_data = String(result).split("api-ajax-request-multiple-result-split");
    var b = array_data[0];
    var result_text = array_data[1];
    $("#" + postData['element_id']).html(array_data[2]);
    // console.log(array_data[2]);
}


//air_dispaly

function api_ajax_category_update_air(postData){
    var result = $.ajax
    (
    	{
            url: postData['ajax_file'],
    		type: 'GET',
    		secureuri:false,
    		dataType: 'html',
    		data:postData,
    		async: false,
    		error: function (response, status, e)
    		{
    			alert(e);
    		}
    	}
    ).responseText;
// var myWindow = window.open("", "MsgWindow", "width=700, height=400");
// myWindow.document.write(result);     
    var array_data = String(result).split("api-ajax-request-multiple-result-split");
    var result_text = array_data[2];
    document.getElementById(postData['element_id']).innerHTML =  array_data[2];
}
</script>

