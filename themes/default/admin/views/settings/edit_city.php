<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo $api_list_view['page_title']; ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open_multipart($api_list_view['controller']."/edit_".$api_list_view['object_name']."/".$select_data[0]['id'], $attrib); ?>
        <div class="modal-body">
            <p><?= lang('update_info'); ?></p>

            <?php
                $language_array = unserialize(multi_language);
                for ($i=0;$i<count($language_array);$i++) {     
                    if ($i == 0) $temp = 'required="required"'; else $temp = '';
                    echo '
                        <div class="form-group">
                            <label class="control-label" for="translate_'.$language_array[$i][0].'">'.lang($language_array[$i][1]).'</label>
                            '.form_input('translate_'.$language_array[$i][0], $select_data[0]['title_'.$language_array[$i][0]], 'class="form-control" '.$temp).'
                        </div>
                    ';                       
                }
            ?> 

            <div class="form-group">
                <?= lang("parent_category", "parent_category") ?>
                <?php
                    $config_data = array(
                        'none_label' => lang("Select a City"),
                        'table_name' => 'sma_city',
                        'space' => ' &rarr; ',
                        'strip_id' => '',        
                        'field_name' => 'title_en',
                        'condition' => 'order by title_en asc',
                        'translate' => 'yes',
                        'no_space' => 1,
                    );                        
                    $this->site->api_get_option_category($config_data);
                    $temp_option = $_SESSION['api_temp'];
                    for ($i=0;$i<count($temp_option);$i++) {                        
                        $temp = explode(':{api}:',$temp_option[$i]);
                        $temp_10 = '';
                        if ($temp[0] != '') {
                            $config_data_2 = array(
                                'id' => $temp[0],
                                'table_name' => 'sma_city',
                                'field_name' => 'title_en',
                                'translate' => 'yes',
                            );
                            $_SESSION['api_temp'] = array();
                            $this->site->api_get_category_arrow($config_data_2);          
                            for ($i2 = count($_SESSION['api_temp']) - 1; ; $i2--) {
                                if ($i2 == 0) {
                                    break;
                                }
                                $temp_arrow = '';
                                if ($i2 > 1)
                                    $temp_arrow = ' &rarr; ';
                                $temp_10 .= $_SESSION['api_temp'][$i2].$temp_arrow;
                            }   
                        }
                        $tr_city[$temp[0]] = $temp_10.$temp[1];
                    }
                    echo form_dropdown('parent_id', $tr_city, (isset($_POST['parent_id']) ? $_POST['parent_id'] : $select_data[0]['parent_id']), 'class="form-control select" style="width:100%"');
                ?>
            </div>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('edit', $api_list_view['page_title'], 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script>
    function api_delivery_fee() {
        var delivery_fee = api_remove_special(document.getElementById('delivery_fee').value);
        delivery_fee = parseFloat(delivery_fee).toFixed(2);
        if (delivery_fee == 'NaN')
            document.getElementById('delivery_fee').value = 0;
        else 
            document.getElementById('delivery_fee').value = parseFloat(delivery_fee).toFixed(2);
    }
    function api_is_number(text) {
        text = text + '';
        if(text) {
        var reg = new RegExp('[0-9]+$');
        return reg.test(text);
        }
        return false;
    }
    function api_remove_special(text) {
        text = text + '';
        if(text) {
        var lower = text.toLowerCase();
        var upper = text.toUpperCase();
        var result = "";
        for(var i=0; i<lower.length; ++i) {
            if(api_is_number(text[i]) || text[i] == '%' || text[i] == '.')
            result += text[i];
            else
            result += '';
        }
        return result;
        }
        return '';
    } 
</script>