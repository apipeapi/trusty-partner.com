<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_product_discount'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open("system_settings/edit_product_discount/".$id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
			
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label for="name"><?php echo $this->lang->line("customer"); ?></label>
						<div class="controls"> 
						<?php 
							$customer = array();
							foreach ($customers as $row) {
								$customer[$row->id] = $row->name;
							}
							echo form_dropdown('slcustomerId', $customer,$product_discount->customer_id, 'id="slcustomerId"  required="required" class="form-control input-tip select" style="width:100%;"');
						?> 
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="name"><?php echo $this->lang->line("start_date"); ?></label>
						<div class="controls"> 
						<?php 
							echo form_input('start_date',$this->sma->hrsd($product_discount->start_date), 'class="form-control tip date" id="start_date" required="required"'); 
						?> 
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="name"><?php echo $this->lang->line("end_date"); ?></label>
						<div class="controls"> 
						<?php 
							echo form_input('end_date',$this->sma->hrsd($product_discount->end_date), 'class="form-control tip date" id="end_date" required="required"'); 
						?> 
						</div>
					</div>
				</div>
				<div class="col-md-12">
<?php
    $temp_1 = set_radio('temp_type', 'name', true); 
    $temp_2 = set_radio('temp_type', 'code');
    echo '
    <div class="form-group">
            <div class="">
                <div class="api_height_10"></div>
                <input class="skip" type="radio" id="temp_type" name="temp_type" value="name" '.$temp_1.' onclick="$(\'#temp_product_name\').show(); $(\'#temp_product_code\').hide();"/>
                '.lang("Product_Name").'
                <span class="api_padding_left_10">
                    <input class="skip" type="radio" id="temp_type" name="temp_type" value="code" '.$temp_2.' onclick="$(\'#temp_product_name\').hide(); $(\'#temp_product_code\').show();"/>
                </span>
                '.lang("Product_Code").'
            </div>
    </div>                
    ';
?>

					<div class="form-group" id="temp_product_name">
						<label for="name"><?php echo $this->lang->line("product_name"); ?></label>
						<div class="controls"> 
						<?php 
							$product = array();
							foreach ($products as $row) {
								$product[$row->id] = $row->name;
							}
							echo form_dropdown('slproductId', $product,$product_discount->product_id, 'id="slproductId"  required="required" class="form-control input-tip select" style="width:100%;"');
						?> 
						</div>
					</div>
                    <div class="form-group" id="temp_product_code" style="display: none;">
                        <label for="product_code"><?php echo lang("Barcode"); ?></label>
                        <div class="controls"> 
                        <?php 
                            $product = array();
                            foreach ($products as $row) {
                                $product[$row->id] = $row->code;
                            }
                            echo form_dropdown('slproductId_2', $product, $product_discount->product_id, 'id="slproductId_2" class="form-control input-tip select" style="width:100%;"');
                        ?> 
                        </div>
                    </div>

					<div class="form-group all">
                       <?= lang("the_normal_price", "the_normal_price") ?>
                        <span id="auto_normal_price"><?= $this->sma->formatMoney($product_discount->normal_price) ?></span>
                        <input type="hidden" name="normal_price" id="normal_price" value="<?=$product_discount->normal_price ?>">
                    </div>
				</div>
				<div class="col-md-6">
                    <div class="form-group all">
                       <?= lang("the_discount_rate", "discount_price") ?><br>
                        <div class="col-md-3">
                            <input style="margin-left:-14px;" type="text" name="discount_price" class="form-control" id="discountPrice" value="<?= $product_discount->discount_price ?>">
                        </div>
                        <div class="col-md-1">
                            <span style="margin-left: -35px;font-weight: bold">%</span>
                        </div>
                        
                    </div>
                    <br>
                     <div class="form-group all">
                       <?= lang("the_special_price", "the_special_price") ?>
                        <input  type="text" name="the_special_price" class="form-control" id="the_special_price" value="<?= number_format($product_discount->special_price,2) ?>">
                        
                    </div>
                </div>
				<div class="col-md-12">
                    <div class="form-group all">
                        <?= lang("note", "note") ?>
                        <textarea class="form-control" cols="50" id="note" name="note" rows="10"></textarea>
                    </div>
                </div>
			</div>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('edit_product_discount', lang('edit_product_discount'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>

<?= $modal_js ?>
<script>
    $(document).ready(function () {
		
       <?php if($this->input->get('customer_name')) { ?>
           
        if (!localStorage.getItem('slitems')) {
            localStorage.setItem('slcustomer', <?=$this->input->get('customer_name');?>);
        }
        <?php } ?>
		
        function getNormalPrice(product_id, customer_id){
			$.ajax({
                type: 'get',
                url: '<?= admin_url('system_settings/suggestions'); ?>',
                dataType: "json",
                data: {
                    customer_id: customer_id,
                    product_id: product_id
                },
                success: function (data) {
                    $("#auto_normal_price").html(data.price);
                    $("#normal_price").val(data.price);
                    var price = data.price;
                    var discounts = $('#discountPrice').val();
                    fndiscount(price,discounts);
                }
            });
        }
     
        $("#slcustomerId").change(function() {
            var product_id = $("#slproductId").val();
            var customer_id = $(this).val();
            getNormalPrice(product_id,customer_id);
            $("#slproductId_2").val($(this).val());
        });
        
        $("#slproductId").change(function() {
            var customer_id = $("#slcustomerId").val();
            var product_id = $(this).val();
            getNormalPrice(product_id,customer_id);
            $("#slproductId").val($(this).val());
        });
 
        function fndiscount(price,discount){
            $.ajax({
                type: 'get',
                url: '<?= admin_url('system_settings/discount'); ?>',
                data: {
                    price: price,
                    discount: discount
                },
                success: function (data) {
                    $("#the_special_price").val(data);
                }
            });
        }
            
        $("#discountPrice").change(function() {
            var price = $("#auto_normal_price").html();
            var discount = $(this).val();
            fndiscount(price,discount);
        });
    });
    
</script>