<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_category'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open_multipart("system_settings/edit_category/".$category->id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('update_info'); ?></p>

            <div class="form-group">
                <?= lang('category_code', 'code'); ?>
                <?= form_input('code', set_value('code', $category->code), 'class="form-control" id="code" required="required"'); ?>
            </div>

            <?php
                $language_array = unserialize(multi_language);
                for ($i=0;$i<count($language_array);$i++) {     
                    if ($i == 0) $temp = 'required="required"'; else $temp = '';
                    echo '
                        <div class="form-group">
                            <label class="control-label" for="translate_'.$language_array[$i][0].'">'.lang($language_array[$i][1]).'</label>
                            '.form_input('translate_'.$language_array[$i][0], $select_data[0]['title_'.$language_array[$i][0]], 'class="form-control" '.$temp).'
                        </div>
                    ';                       
                }
            ?> 


            <div class="form-group all">
                <?= lang('slug', 'slug'); ?>
                <?= form_input('slug', set_value('slug', $category->slug), 'class="form-control tip" id="slug" required="required"'); ?>
            </div>

            <div class="form-group">
                <?= lang("parent_category", "parent") ?>
                <?php
                $cat[''] = lang('select').' '.lang('parent_category');
                foreach ($categories as $pcat) {
                    $cat[$pcat->id] = $pcat->name;
                }
                echo form_dropdown('parent', $cat, (isset($_POST['parent']) ? $_POST['parent'] : $category->parent_id), 'class="form-control select" id="parent" style="width:100%"')
                ?>
            </div>

            <?php
                if ($select_data[0]['display'] == 'yes') {
                    $temp_1 = set_radio('add_ons_display', 'yes', TRUE);
                    $temp_2 = set_radio('add_ons_display', 'no');
                }
                else {
                    $temp_1 = set_radio('add_ons_display', 'yes');
                    $temp_2 = set_radio('add_ons_display', 'no', TRUE);                    
                }

                echo '
                <div class="form-group">
                    '.lang("Display", "Display").'
                    <div class="">            
                        <div class="api_height_10"></div>
                        <input type="radio" name="add_ons_display" value="yes" '.$temp_1.' />
                        Yes
                        <span class="api_padding_left_10">
                            <input type="radio" name="add_ons_display" value="no" '.$temp_2.' />
                        </span>
                        No
                    </div>
                </div>                
                ';
            ?> 

            <div class="form-group">
                <?= lang("category_image", "image") ?>
                <input id="image" type="file" data-browse-label="<?= lang('browse'); ?>" name="userfile" data-show-upload="false" data-show-preview="false" class="form-control file">
            </div>

            <div class="form-group all">
                <?= lang('description', 'description'); ?>
                <?= form_textarea('add_ons_description', $select_data[0]['description'], 'class="form-control tip" id="description" '); ?>
            </div>


            


        </div>
        <div class="modal-footer">
            <?php echo form_submit('edit_category', lang('edit_category'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>
<script>
    $(document).ready(function() {
        $('.gen_slug').change(function(e) {
            getSlug($(this).val(), 'category');
        });
    });
</script>
