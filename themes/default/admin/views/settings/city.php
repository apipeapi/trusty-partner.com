<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php 
    $temp_url = '';
    if ($_GET['page'] != '') 
        $temp_url .= '?page='.$_GET['page']; 
    else 
        $temp_url .= '?page=1'; 

    if ($per_page != '') $temp_url .= '&per_page='.$per_page;

    foreach ($_GET as $name => $value) {
        if ($name != 'page' && $name != 'per_page' && $name != 'mode')
            if ($value != '') $temp_url .= '&'.$name.'='.$value;
    }

    $api_list_view_current_page = $_GET['page'];
    if ($api_list_view_current_page == '') $api_list_view_current_page = 1;

    echo admin_form_open($api_list_view['controller'].'/'.$api_list_view['object_name'].$temp_url,'id="'.$api_list_view['form_name'].'" name="'.$api_list_view['form_name'].'" method="GET"');
?>



<div class="box">
    <div class="box-header">
        <h2 class="blue">
            <i class="fa-fw fa fa-users"></i>
            <?php
                echo $api_list_view['page_title']; 
            ?>
        </h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon fa fa-tasks tip" data-placement="left" title="<?=lang("actions")?>"></i>
                    </a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
<?php
//-action_list-============================
    echo '
        <li>
            <a href="'.admin_url($api_list_view['controller'].'/edit_'.$api_list_view['object_name']).'" data-toggle="modal" data-target="#myModal">
                <i class="fa fa-plus-circle"></i> '.lang('Add').' '.$api_list_view['object_title'].'
            </a>
        </li>
    ';
    echo '
        <li class="divider"></li>
        <li>
            <a href="#" class="bpo" title="<b>'.lang("Delete").' '.$api_list_view['object_title'].'</b>" data-content="<p'.lang('r_u_sure').'</p>
            <button type=\'button\' class=\'btn btn-danger\' onclick=\'api_list_view_action_delete();\' data-action=\'delete\'>'.lang('i_m_sure').'</a> <button class=\'btn bpo-close\'>
            '.lang('no').'</button>" data-html="true" data-placement="left">
                <i class="fa fa-trash-o"></i> '.lang("Delete").'
            </a>
        </li>
    ';
//-action_list-============================
?>
                    </ul>
                </li>
            </ul>
        </div>

    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
<?php            
//-top_buttons-================================================

//-top_buttons-================================================
?>
                <div class="table-responsive">
<?php
    echo '
        <div class="col-md-12 api_padding_0">
    ';

//-search_fields-================================================
echo '
<div style="background: #fff; margin: -20px -20px 20px -20px; padding: 10px; border-bottom: 1px solid #DBDEE0;">
';

echo '
    <div class="col-md-6 " >
        <div class="form-group">
            '.lang("Parent_Name", "Parent_Name").'
';

            $config_data = array(
                'none_label' => lang("All_Parent_Cities"),
                'table_name' => 'sma_city',
                'space' => ' &rarr; ',
                'strip_id' => '',        
                'field_name' => 'title_en',
                'condition' => 'order by title_en asc',
                'translate' => 'yes',
                'no_space' => 1,
            );                        
            $this->site->api_get_option_category($config_data);
            $temp_option = $_SESSION['api_temp'];
            for ($i=0;$i<count($temp_option);$i++) {                        
                $temp = explode(':{api}:',$temp_option[$i]);
                $temp_10 = '';
                if ($temp[0] != '') {
                    $config_data_2 = array(
                        'id' => $temp[0],
                        'table_name' => 'sma_city',
                        'field_name' => 'title_en',
                        'translate' => 'yes',
                    );
                    $_SESSION['api_temp'] = array();
                    $this->site->api_get_category_arrow($config_data_2);          
                    for ($i2 = count($_SESSION['api_temp']) - 1; ; $i2--) {
                        if ($i2 == 0) {
                            break;
                        }
                        $temp_arrow = '';
                        if ($i2 > 1)
                            $temp_arrow = ' &rarr; ';
                        $temp_10 .= $_SESSION['api_temp'][$i2].$temp_arrow;
                    }   
                }
                $tr_city[$temp[0]] = $temp_10.$temp[1];
            }
            echo form_dropdown('parent_id', $tr_city, $_GET['parent_id'], 'class="form-control" onchange="document.'.$api_list_view['form_name'].'.submit();"');
echo '
        </div>
    </div>
';

echo '
    <div class="api_clear_both"></div>
</div>
';
//-search_fields-================================================

    echo '
        </div>
    ';
//-list_view_searh-================================================
    echo '
        <div class="row">
            <div class="col-md-6 text-left api_padding_top_5">
                <div class="short-result">
                        <span>'.lang('Show').'</span>
                            <select name="per_page" onchange="$(\'#api_action\').val(\'\'); $(\'#'.$api_list_view['form_name'].'\').submit();" style="width:80px;">
    ';
                                $per_page_arr = array(
                                    '10'=>'10',
                                    '25'=>'25',
                                    '50'=> '50',
                                    '100'=>'100',
                                    '200'=>'200',
                                    '500'=>'500',
                                    'All'=>$total_rows_sale
                                );
                            
                                foreach($per_page_arr as $key =>$value){
                                    $select = $value == $per_page?'selected':'';
                                    echo '<option value="'.$value.'" '.$select.'>';
                                            echo $key;
                                    echo '</option>';
                                }
    echo '
                            </select>
                    </div>
            </div>
    ';
    echo '
        <div class="col-md-6 text-right api_padding_top_5">

            <div class="form-group pull-right">
                <input type="button" name="submit_form" onclick="$(\'#api_action\').val(\'\'); $(\'#'.$api_list_view['form_name'].'\').submit();" value="Search" class="btn btn-primary">
            </div>

            <div class="form-group pull-right">
                <input type="text" class="form-control" id="search" name="search" value="'.(isset($_GET['search']) ? $_GET['search'] : "").'" onkeydown="if (event.keyCode == 13) document.'.$api_list_view['form_name'].'.submit()" />
            </div>


        </div>
    ';
                                    
    // echo '
    //     <div class="col-md-6 text-right">
    //         <div class="short-result">
    //             <div id="SLData_filter" class="show-data-search dataTables_filter">
    //                 <label>
    //                     Search
    //                     <input class="input-xs" value="'.$this->input->get('search').'" type="text" name="search" placeholder="Search"/>
    //                     <input style="height: 34px;" class="btn btn-primary" type="submit" name="submit_search" value="'.lang('Search').'">
    //                 </label>                                        
    //             </div>
    //         </div>    
    //     </div>
    // ';
    echo ' 
        </div>
    ';
//-list_view_searh-================================================
?>

<?php
    $row_headers = array(
        'checkbox' => [
            'label' => '<input class="checkbox checkth skip" onclick="api_select_check_all(this,\'val[]\'); api_setValueCheckbox(\'val[]\',\'check_value\',\''.$api_list_view['form_name'].'\',\'-\')" type="checkbox" name="check"/>', 
            'sort_fn' => false,
            'style' => 'min-width:30px; width: 30px; text-align: center;'
        ],
        'title_en' => [
            'label' => lang("English_Name"),
            'style' => 'width:300px'
        ],
        'title_kh' => [
            'label' => lang("Khmer_Name"),
            'style' => 'width:300px'
        ],        
        'parent_id'      => [
            'label' => lang("Parent_Name"), 'style' => 'text-align:center;'
        ],
        'action' => [
            'label' => lang("actions"),
            'style' => 'width:80px; text-align:center;',
            'sort_fn' => false
        ],
    );
?>
<table id="api_list_view" class="api_list_view table table-bordered table-hover table-striped" cellpadding="0" cellspacing="0" border="0">
<thead>
<tr class="primary">
<?php foreach ($row_headers as $key => $row) :?>
    <?php
    $th_class="pointer";
    $e_click = true;
    if(isset($row['sort_fn']) && $row['sort_fn'] == false) {
        $th_class="";
        $e_click = false;
    }
    $style = isset($row['style']) && $row['style'] != '' ? 'style="'.$row['style'].'"' : '';
    ?>
    <th <?= $style ?> >
        <?php if($e_click ) :?>
            <?php if ($sort_by == $key && $sort_order == "ASC") : ?>
                    <label class="font-normal <?=$th_class?>" onclick="formactionsubmit('<?= $key ?>', 'DESC')" id="fa_sort"  aria-hidden="true">
                    <?php echo $row['label']; ?>
                        <span class="fa fa-sort-down"></span>
                    </label>
            <?php elseif ($sort_by == $key and $sort_order=="DESC") : ?>    
                <label class="font-normal <?=$th_class?>" onclick="formactionsubmit('<?= $key ?>', 'ASC')" id="fa_sort_up"  aria-hidden="true">
                    <?php echo $row['label']; ?>
                    <span class="fa fa-sort-up"></span>
                </label>
            <?php else : ?> 
                <label class="font-normal <?=$th_class?>" onclick="formactionsubmit('<?= $key ?>', 'DESC')" id="fa_sort_down" aria-hidden="true">
                    <?php echo $row['label']; ?>
                    <span class="fa fa-sort"></span>
                </label>
            <?php endif; ?> 
        <?php else : ?>
            <label class="font-normal <?=$th_class?>" aria-hidden="true">
                <?php echo $row['label']; ?>
            </label>
        <?php endif; ?> 
    </th>
<?php endforeach; ?>
</tr>
</thead>
<tbody role="alert" aria-live="polite" aria-relevant="all">

<?php
//-view_data-======================================================================
for ($i=0;$i<count($select_data);$i++) {
    $temp7 = '
        <div class="text-center">
            <input class="checkbox multi-select skip" type="checkbox" name="val[]" value="'.$select_data[$i]['id'].'" id="api_list_check_'.$select_data[$i]['id'].'" onclick="api_setValueCheckbox(\'val[]\',\'check_value\',\''.$api_list_view['form_name'].'\',\'-\');"/>
        </div>        
    ';
    $temp6 = '
        <a class="tip api_margin_right_5" title="Edit" href="'.admin_url($api_list_view['controller'].'/edit_'.$api_list_view['object_name'].'/'.$select_data[$i]['id']).'" data-toggle="modal" data-target="#myModal"><i class="fa fa-edit"></i>
        </a>
        <a href="#" class="tip bpo" title="<b>'.$this->lang->line("delete").'</b>"
            data-content="<p>'.lang('r_u_sure').'</p> <a href=\''.admin_url($api_list_view['controller'].'/delete_'.$api_list_view['object_name'].'/'.$select_data[$i]['id']).'\'><button type=\'button\' class=\'btn btn-danger\' data-action=\'delete\'>'.lang('i_m_sure').'</button></a> <button class=\'btn po-close\'>'.lang('no').'</button>"
            data-html="true" data-placement="left">
            <i class="fa fa-trash-o"></i>
        </a>        
    ';

    $temp_10 = '';
    $config_data = array(
        'id' => $select_data[$i]['id'],
        'table_name' => 'sma_city',
        'field_name' => 'title_en',
        'translate' => 'yes',
    );
    $_SESSION['api_temp'] = array();
    $this->site->api_get_category_arrow($config_data);          
    for ($i2 = count($_SESSION['api_temp']) - 1; ; $i2--) {
        if ($i2 == 0) {
            break;
        }
        $temp_arrow = '';
        if ($i2 > 1)
            $temp_arrow = ' <span class="fa fa-long-arrow-right api_padding_left_10 api_padding_right_10"></span> ';
        $temp_10 .= $_SESSION['api_temp'][$i2].$temp_arrow;
    }        
    
        
    echo '
        <tr id="'.$select_data[$i]['id'].'" class="api_table_link_company_branch" >
        <td style="min-width:30px; width: 30px; text-align: center; '.$temp_disabled_background.'">
            '.$temp7.'
        </td>
        <td align="left" style="'.$temp_disabled_background.'">
            '.$select_data[$i]['title_en'].'
        </td>        
        <td align="left" style="'.$temp_disabled_background.'">
            '.$select_data[$i]['title_kh'].'
        </td>                
        <td align="left" style="'.$temp_disabled_background.'">
            '.$temp_10.'
        </td>
        <td align="center" style="'.$temp_disabled_background.'">
            '.$temp6.'
        </td>
        </tr>
    ';    
}
if (count($select_data) <= 0) {
    echo '
    <tr>
    <td colspan="'.count($row_headers).'">
        '.lang('no_record_found').'
    </td>
    </tr>
    ';
}
echo '
    </tbody>
';
//-view_data-======================================================================
?>                            
<tfoot class="dtFilter">
<tr class="active">
    <th style="min-width:30px; width: 30px; text-align: center;">
        <input class="checkbox checkft skip" type="checkbox" name="check" onclick="api_select_check_all(this,'val[]'); api_setValueCheckbox('val[]','check_value','<?php echo $api_list_view['form_name']; ?>','-')"/>
    </th>
    <?php
    for ($i=1;$i<count($row_headers) - 1;$i++) {
        echo '<th></th>';
    }
    ?>                           
    <th style="text-align: center">
        <?php echo lang('Actions'); ?>
    </th>
</tr>
</tfoot>
</table>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel-heading-left">
                                <p class="data-total">
                                    <?php echo $show.' ';?>
                                </p>
                            </div>   
                            <div class="panel-heading-right">
                                <p>
                                   <?php echo $this->pagination->create_links(); ?>
                                </p>
                            </div>  
                            <div class="panel-heading-clear-fix"></div>   
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" name="show_data" value="<?php echo $show_data;?>" id="show_data"/>
<input type="hidden" name="sort_by" value="<?php echo $sort_by;?>" id="sort_by"/>
<input type="hidden" name="sort_order" value="<?php echo $sort_order;?>" id="sort_order"/>
<?= form_close() ?>

<?php
echo admin_form_open($api_list_view['form_bulk_action'],'id="api_form_action" name="api_form_action" method="post"');
echo '
    <div class="api_display_none">
        <input type="text" name="check_value" value="" id="check_value"/>
        <input type="text" name="api_action" value="" id="api_action"/>
        <button type="button" onclick="document.api_form_action.submit();" value="ok">ok</button>
    </div>
';
echo form_close();
?>

<script>
    $(".pagination .active").html('<a href="javascript:void(0);"><?php echo $api_list_view_current_page; ?></a>');
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/api/plugin/list_view/stock_manager/script.js"></script>