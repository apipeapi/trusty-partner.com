<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_translation'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open_multipart("shop_settings/edit_translation/" . $select_data[0]['id'], $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info').$select_data[0]['id']; ?></p>
            
        <div class="form-group" >
            <label class="control-label">Keyword</label> 
            <input type="text" name="keyword" value="<?php echo $select_data[0]['keyword']; ?>" class="form-control">
        </div>

<?php
$language_array = unserialize(multi_language);
for ($i=0;$i<count($language_array);$i++) {     
    if ($i == 0) $temp = 'required="required"'; else $temp = '';
    echo '
        <div class="form-group">
            <label class="control-label" for="translate_'.$language_array[$i][0].'">'.lang($language_array[$i][1]).'</label>
            '.form_input('translate_'.$language_array[$i][0], $select_data[0]['title_'.$language_array[$i][0]], 'class="form-control" '.$temp).'
        </div>
    ';                       
}
?>  
        </div>
        <div class="modal-footer">
            <?php echo form_submit('edit_translation', lang('Edit_Translation'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>