<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('delivery_person'); ?></h4>
        </div>
        <?php 
            $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'name' => 'frm_set_delivering');
        
            if ($sales == '') {
                echo admin_form_open_multipart("consignment/set_delivering/".$select_data[0]['id'], $attrib);
            }
            else {
                $select_data = array();
                echo admin_form_open_multipart("consignment/set_delivering_bulk/".$sales, $attrib);
                echo '<input type="hidden" id="sales" name="sales" value="'.$sales.'" />';
            }
        ?>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <?= lang("delivery_person", "delivery_person"); ?>
                        <?php
                            $tr[""] = "";
                            $i = 0;
                            foreach ($delivery_person as $value) {
                                $tr[$delivery_person[$i]['id']] = $delivery_person[$i]['title'];
                                $i++;
                            }
                            echo form_dropdown('add_ons_delivery_person', $tr, $select_data[0]['delivery_person'], 'id="add_ons_delivery_person" data-placeholder="'.lang("select").' '.lang("delivery_person").'" required="required" class="form-control input-tip select" style="width:100%;"');
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <?php 
                echo form_submit('button', lang('submit'), 'class="btn btn-primary" onclick="if(document.frm_set_delivering.add_ons_delivery_person.value != \'\') document.frm_set_delivering.submit();"'); 
            ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>

