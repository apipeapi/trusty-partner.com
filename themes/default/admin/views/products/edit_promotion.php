<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php
    if ($select_data[0]['id'] != '') {
        $temp_1 = lang('Edit').' '.$api_list_view['object_title'];
        $temp_2 = '/'.$select_data[0]['id'];
        $temp_3 = lang('Update');
        $temp_4 = '';
        $temp_5 = '';
        $temp_6 = 'api_display_none';
    }
    else {
        $temp_1 = lang('Add').' '.$api_list_view['object_title'];
        $temp_2 = '';
        $temp_3 = lang('Add');
        $temp_4 = '';
        $temp_5 = '';
        $temp_6 = '';
    }

?>

<div class="modal-dialog">
    <div class="modal-content" style=" width: 1000px; margin-left: -34%;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo $temp_1; ?></h4>
        </div>
        <?php         

        $attrib = array('data-toggle' => 'validator', 'role' => 'form',  'id' => $form_name, 'name' => $form_name);
        echo admin_form_open_multipart($api_list_view['controller']."/edit_".$api_list_view['object_name'].$temp_2, $attrib); ?>
        
        <div class="modal-body">
            <div class="row">
                
                          
<?php
echo '
    <div class="col-md-12">
        <div class="form-group">
            '.lang("name", "name").' *
            <input type="text" class="form-control" name="name" required="required" value="'.$select_data[0]['name'].'"  />
        </div>
    </div>
';

if ($select_data[0]['start_date'] == '')
    $select_data[0]['start_date'] = date('Y-m-d');
echo '
<div class="col-md-6">
    <div class="form-group">
        '.lang("start_date", "start_date").'
        '.form_input('start_date', (isset($_POST['start_date']) ? $_POST['start_date'] : $this->sma->hrld($select_data[0]['start_date'])), 'class="form-control datetime" id="'.$form_name.'_start_date" autocomplete="off"').'
    </div>
</div>
';

if ($select_data[0]['end_date'] == '')
    $select_data[0]['end_date'] = date('Y-m-d');    
echo '
<div class="col-md-6">
    <div class="form-group">
        '.lang("end_date", "end_date").'
        '.form_input('end_date', (isset($_POST['end_date']) ? $_POST['end_date'] : $this->sma->hrld($select_data[0]['end_date'])), 'class="form-control datetime" id="'.$form_name.'_end_date" autocomplete="off"').'
    </div>
</div>
';


echo '
<div class="col-md-6 api_display_none">
    <div class="form-group">
        <label class="control-label">
            '.lang("Discount_Type").'
        </label>
        <div class="controls"> 
';
            $type_discount['percentage'] = lang('Percentage');
            $type_discount['fixed_price'] = lang('Fixed Price');
            echo form_dropdown('type_discount', $type_discount, $select_data[0]['type_discount'], 'class="form-control" id="'.$form_name.'_type_discount" onchange="'.$form_name.'_type_discount_change();";');
echo '
        </div>
    </div>    
</div>
';

if ($select_data[0]['min_qty'] == '')
    $select_data[0]['min_qty'] = 0;
echo '
<div class="col-md-3">
    <div class="form-group">
        '.lang("Minimum_Qty_Order", "Minimum_Qty_Order").'
        '.form_input('min_qty', (isset($_POST['min_qty']) ? $_POST['min_qty'] : $select_data[0]['min_qty']), 'class="form-control api_numberic_input" autocomplete="off"').'
    </div>
</div>
';

echo '
    <div class="col-md-3">
        <div class="form-group">
            '.lang("Discount_Rate", "Discount_Rate").'
            <div class="controls"> 
';
            for ($i=0;$i<=100;$i++) {
                $tr_rate[$i] = $i.'%';
            }
            echo form_dropdown('rate', $tr_rate, (isset($_POST['rate']) ? $_POST['rate'] : ($select_data[0]['rate'] ? $select_data[0]['rate'] : 0)), 'class="form-control" id="'.$form_name.'_rate"');
echo '
            </div>
        </div> 
    </div>
';

echo '
<div class="col-md-3">
    <div class="form-group">
        '.lang("Ordering", "Ordering").'
';
for ($i=0;$i<=100;$i++) {
    $tr_ordering[$i] = $i;
}
$ordering = $select_data[0]['ordering'];
// if($select_data[0]['ordering'] != 0) {
//     $ordering = $select_data[0]['ordering'];
// } else {
//     $config_data = array(
//         'table_name' => 'sma_product_promotion',
//         'select_table' => 'sma_product_promotion',
//         'select_condition' => "id > 0 order by ordering desc limit 1",
//     );
//     $temp = $this->site->api_select_data_v2($config_data);
//     $ordering = $temp[0]['ordering'] + 1;
// }
echo form_dropdown('ordering', $tr_ordering, $ordering, 'class="form-control api_numberic_input" ');         
echo '
    </div>
</div>
';

if ($select_data[0]['status'] == '' || $select_data[0]['status'] == 'enabled') {
    $temp_1 = set_radio('status', 'enabled', TRUE); 
    $temp_2 = set_radio('status', 'disabled');
}
else {
    $temp_1 = set_radio('status', 'enabled');
    $temp_2 = set_radio('status', 'disabled', TRUE);                                
}
echo '
<div class="form-group col-md-3">
    '.lang("Status", "Status").'
    <div class="">
        <div class="api_height_10"></div>
        <input type="radio" id="" name="status" value="enabled" '.$temp_1.' />
        '.lang('Enabled').'
        <span class="api_padding_left_10">
            <input type="radio" id="" name="status" value="disabled" '.$temp_2.' />
        </span>
        '.lang('Disabled').'
    </div>
</div>                
';


echo '
    <div class="col-md-6 api_display_none">
        <div class="form-group all">
            '.lang("Fixed_Price", "Fixed_Price").'
            <div class="input-group ">
                '.form_input('fixed_price', (isset($_POST['fixed_price']) ? $_POST['fixed_price'] : ($select_data[0]['fixed_price'] ? $select_data[0]['fixed_price'] : 0)), 'class="form-control api_numberic_input" id="'.$form_name.'_fixed_price"').'
                <span class="input-group-addon" style="padding: 1px 10px;">
                    <i class="fa fa-usd"></i>
                </span>                            
            </div>
        </div>
    </div>
';

echo '
    <div class="col-md-12">
        <div class="form-group all">'.lang("Promotion_Image", "image").'

        <input id="image" type="file" data-browse-label="'.lang('browse').'" name="userfile" data-show-upload="false" data-show-preview="false"  class="form-control file">
        </div>
    </div>
';



echo '
    <div class="col-md-12">
        <div class="form-group all">
';


if ($select_data[0]['id'] > 0) {
echo '
    <div class="col-md-11 api_padding_0">
        <div class="form-group">
            '.lang("Add_Product", "Add_Product").'
';

            $config_data = array(
                'table_name' => 'sma_products',
                'select_table' => 'sma_products',
                'translate' => '',
                'description' => '',
                'select_condition' => "id > 0 order by name asc",
            );
            $temp = $this->api_helper->api_select_data_v2($config_data);

            $tr[''] = lang("Select_a_product");
            for ($i=0;$i<count($temp);$i++) {
                $tr[$temp[$i]['id']] = $temp[$i]['name'].' ('.$temp[$i]['code'].')';
            }                            
            echo form_dropdown('temp_add_product', $tr, '', 'class="form-control" id="temp_add_product" ');

echo '   
        </div>
    </div>
    <div class="col-md-1 api_padding_0">
        <div class="api_height_30"></div>
        <button type="button" class="btn btn-info" style="width:100%;" onclick="
        var postData = {
            \'product_id\' : $(\'#temp_add_product\').val(),
            \'promotion_id\' : \''.$select_data[0]['id'].'\',
        };      
        api_temp_add_product(postData);        
        ">Add</button>
    </div>
';    
        echo '
            <div class="table-responsive ">
                <table class="table table-bordered table-hover table-striped print-table order-table">
                    <thead>

                        <tr>
                            <th align="center" width="30">'.lang("no.").'</th>
                            <th width="50" >'.lang("image").'</th>
                            <th>'.lang("product_name").'</th>
                            <th width="120" align="right">'.lang("Price").'</th>
                            <th width="130" align="right">'.lang("Discount_Rate").'</th>
                            <th width="120" align="right">'.lang("Price_After_Discount").'</th>
                            <th width="50" align="center">'.lang("Action").'</th>
                        </tr>

                    </thead>
                    <tbody id="api_temp_tbody_promotion"> ';
                    
$config_data_2 = array(
    'table_name' => 'sma_product_promotion_item',
    'select_table' => 'sma_product_promotion_item',
    'translate' => '',
    'select_condition' => "id > 0 and promotion_id = ".$select_data[0]['id']." order by id desc ",
);
$select_list = $this->site->api_select_data_v2($config_data_2);                  
$row = 1;
for($i=0;$i<count($select_list);$i++){
    $config_data_2 = array(
        'table_name' => 'sma_products',
        'select_table' => 'sma_products',
        'translate' => '',
        'select_condition' => "id = ".$select_list[$i]['product_id'],
    );
    $select_data_product = $this->site->api_select_data_v2($config_data_2);    

    $data_view = array (
        'wrapper_class' => '',
        'file_path' => 'assets/uploads/thumbs/'.$select_data_product[0]['image'],
        'max_width' => 50,
        'max_height' => 50,
        'product_link' => '',
        'image_class' => 'img-responsive',
        'image_id' => '',   
        'resize_type' => 'full',       
    );
    $temp_image = $this->site->api_get_image($data_view);                            

    $element_html .= '
        <tr class="api_temp_tr_promote_'.$select_list[$i]['id'].'">
            <td align="center">'.$row.'</td>
            <td align="center" valign="middle">
                '.$temp_image['image'].'
            </td>
            <td>
                '.$select_data_product[0]['name'].' ('.$select_data_product[0]['code'].')
            </td>
            <td align="right">
                '.$this->sma->formatMoney($this->api_helper->round_up_second_decimal($select_data_product[0]['price'])).'
            </td>
            <td align="right">
                <select name="temp_rate_'.$select_list[$i]['id'].'" style="width:80px; height:30px;" onchange="
                var postData = {
                    \'field_value\' : this.value,
                    \'field_id\' : \''.$select_list[$i]['id'].'\',
                    \'element_id\' : \'temp_price_after_discount_'.$select_list[$i]['id'].'\',
                };
                api_ajax_rate_change(postData);
                ">
        ';
                    for ($j=0;$j<=100;$j++) {
                        if ($j == $select_list[$i]['rate'])
                            $temp_selected = 'selected';
                        else
                            $temp_selected = '';

                        $element_html .= '
                            <option value="'.$j.'" '.$temp_selected.'>
                                '.$j.'%
                            </option>
                        ';
                    }
        $element_html .= '
                </select>
            </td>
        ';

        $config_data_2 = array(
            'id' => $select_data_product[0]['id'],
            'customer_id' => '',
        );
        $temp_price = $this->site->api_calculate_product_price($config_data_2);   

        $element_html .= '
            <td align="right">
                <span id="temp_price_after_discount_'.$select_list[$i]['id'].'">'.$this->sma->formatMoney($temp_price['price']).'</span>
            </td>
            <td align="center">
                <a href="javascript:void(0);" onclick="
                var postData = {
                    \'id\' : \''.$select_list[$i]['id'].'\',
                    \'product_id\' : \''.$select_data_product[0]['id'].'\',
                    \'element_id\' : \'api_temp_tr_promote_'.$select_list[$i]['id'].'\',
                };                                            
                api_temp_promotion_remove(postData);
                " title="Remove">
                    <li class="fa fa-trash-o"></li>
                </a>
            </td>
        </tr>
        ';
    $row++;
}
echo $element_html;                   
                    echo ' 
                    </tbody>
                 </table>
                 </div>
                 ';
}
echo '

        </div>
    </div>
';
?>

            </div>

        </div>
        <div class="modal-footer">
            <?php
                echo form_submit('Add', $temp_3, 'class="btn btn-primary"'); 
            ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>
<script src="<?php echo base_url(); ?>assets/api/js/public.js"></script>

<script>
<?php
echo '
function '.$form_name.'_type_discount_change() {
    if (document.getElementById("'.$form_name.'_type_discount").value == "percentage") {
        $("#'.$form_name.'_rate").removeAttr("disabled");
        $("#'.$form_name.'_fixed_price").attr("disabled","disabled");
    }
    if (document.getElementById("'.$form_name.'_type_discount").value == "fixed_price") {
        $("#'.$form_name.'_rate").attr("disabled","disabled");
        $("#'.$form_name.'_fixed_price").removeAttr("disabled");
    }
}
'.$form_name.'_type_discount_change();
';
?>

function api_temp_promotion_remove(postData){    
    var result = $.ajax
    (
    	{
    		url: '<?php echo base_url(); ?>admin/products/ajax_promotion_remove',
    		type: 'GET',
    		secureuri:false,
    		dataType: 'html',
    		data:postData,
    		async: false,
    		error: function (response, status, e)
    		{
    			alert(e);
    		}
    	}
    ).responseText;
//var myWindow = window.open("", "MsgWindow", "width=700, height=400");
//myWindow.document.write(result);     
    var array_data = String(result).split("api-ajax-request-multiple-result-split");
    var result_text = array_data[1];
    $('.' + postData['element_id']).hide();
}

function api_temp_add_product(postData){    
    if (postData['product_id'] != '') {
        var result = $.ajax
        (
            {
                url: '<?php echo base_url(); ?>admin/products/ajax_promotion_add',
                type: 'GET',
                secureuri:false,
                dataType: 'html',
                data:postData,
                async: false,
                error: function (response, status, e)
                {
                    alert(e);
                }
            }
        ).responseText;
    //var myWindow = window.open("", "MsgWindow", "width=700, height=400");
    //myWindow.document.write(result);     
        var array_data = String(result).split("api-ajax-request-multiple-result-split");
        var result_text = array_data[1];
        //console.log(array_data);
        if (result_text !='')
            document.getElementById('api_temp_tbody_promotion').innerHTML = result_text;
        else if (array_data[2] != '')
            alert(array_data[2]);
    }
}
function api_ajax_rate_change(postData) {
    var result = $.ajax
    (
    	{
    		url: '<?php echo base_url(); ?>admin/products/api_ajax_rate_change',
    		type: 'GET',
    		secureuri:false,
    		dataType: 'html',
    		data:postData,
    		async: false,
    		error: function (response, status, e)
    		{
    			alert(e);
    		}
    	}
    ).responseText;
// var myWindow = window.open("", "MsgWindow", "width=700, height=400");
// myWindow.document.write(result);     
    var array_data = String(result).split("api-ajax-request-multiple-result-split");
    var result_text = array_data[1];
    document.getElementById(postData['element_id']).innerHTML = result_text;
}

</script>









