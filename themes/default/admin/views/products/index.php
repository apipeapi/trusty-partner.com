<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 

    $temp_url = '';
    if ($_GET['page'] != '') 
        $temp_url .= '?page='.$_GET['page']; 
    else 
        $temp_url .= '?page=1'; 
    if ($_GET['search'] != '') $temp_url .= '&sort_by='.$_GET['search'];
    if ($this->input->get('featured') != '') $temp_url .= '&featured='.$this->input->get('featured');
    if ($this->input->get('promotion') != '') $temp_url .= '&promotion='.$this->input->get('promotion');    
    if ($this->input->get('c_id') != '') $temp_url .= '&c_id='.$this->input->get('c_id');
    if ($_GET['sort_by'] != '') $temp_url .= '&sort_by='.$_GET['sort_by'];
    if ($_GET['sort_order'] != '') $temp_url .= '&sort_order='.$_GET['sort_order'];
    if ($per_page != '') $temp_url .= '&per_page='.$per_page;

    echo admin_form_open('products'.$temp_url,'id="action-form" name="action-form" method="GET"');
?>

<div class="box">
    <div class="box-header">
        <h2 class="blue"><i
                class="fa-fw fa fa-barcode"></i><?= lang('products') . ' (' . ($warehouse_id ? $warehouse->name : lang('all_warehouses')) . ')'.($supplier ? ' ('.lang('supplier').': '.($supplier->company && $supplier->company != '-' ? $supplier->company : $supplier->name).')' : ''); ?>
        </h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon fa fa-tasks tip" data-placement="left" title="<?= lang("actions") ?>"></i>
                    </a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?= admin_url('products/add') ?>">
                                <i class="fa fa-plus-circle"></i> <?= lang('add_product') ?>
                            </a>
                        </li>
                        <?php if(!$warehouse_id) { ?>
                        <li>
                            <a href="<?= admin_url('products/update_price') ?>" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-file-excel-o"></i> <?= lang('update_price') ?>
                            </a>
                        </li>
                        <?php } ?>
                        <li>
                            <a href="#" id="labelProducts" onclick="api_bulk_actions('labels');" data-action="labels">
                                <i class="fa fa-print"></i> <?= lang('print_barcode_label') ?>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" onclick="api_bulk_actions('export_excel');" >
                                <i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" onclick="api_bulk_actions('set_no_use');" >
                                <i class="fa fa-minus-square-o"></i> <?= lang('Set_No_More_Use') ?>
                            </a>
                        </li>             
                        <li>
                            <a href="javascript:void(0)" onclick="api_bulk_actions('unset_no_use');" >
                                <i class="fa fa-plus-square-o"></i> <?= lang('Unset_No_More_Use') ?>
                            </a>
                        </li>                                           
                        <li class="divider"></li>
                        <li>
                            <a href="#" class="bpo" title="<b><?= $this->lang->line("delete_products") ?></b>"
                                data-content="<p><?= lang('r_u_sure') ?></p><button type='button' class='btn btn-danger' onclick='bulk_actions_delete();' ><?= lang('i_m_sure') ?></a> <button class='btn bpo-close'><?= lang('no') ?></button>"
                                data-html="true" data-placement="left">
                            <i class="fa fa-trash-o"></i> <?= lang('delete_products') ?>
                             </a>
                         </li>
                    </ul>
                </li>
                <?php if ($warehouses == 'api_display_none') { ?>
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-building-o tip" data-placement="left" title="<?= lang("warehouses") ?>"></i></a>
                        <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                            <li><a href="<?= admin_url('products') ?>"><i class="fa fa-building-o"></i> <?= lang('all_warehouses') ?></a></li>
                            <li class="divider"></li>
                            <?php
                            foreach ($warehouses as $warehouse) {
                                echo '<li><a href="' . admin_url('products/' . $warehouse->id) . '"><i class="fa fa-building"></i>' . $warehouse->name . '</a></li>';
                            }
                            ?>
                        </ul>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?= lang('list_results'); ?></p>
<?php
//-search_fields-================================================
echo '
<div style="background: #fff; margin: -20px -20px 20px -20px; padding: 10px; border-bottom: 1px solid #DBDEE0;">
';

echo '
    <div class="col-md-6" >
        <div class="form-group">
            '.lang("Category", "Category").'
';
            $config_data = array(
                'none_label' => lang("All_Categories"),
                'table_name' => 'sma_categories',
                'space' => ' &rarr; ',
                'strip_id' => '',        
                'field_name' => 'title_en',
                'condition' => 'order by title_en asc',
                'translate' => 'yes',
                'no_space' => 1,
                'type' => 'arrow',
            );                        
            $this->site->api_get_option_category($config_data);
            $temp_option = $_SESSION['api_temp'];
            for ($i=0;$i<count($temp_option);$i++) {                        
                $temp = explode(':{api}:',$temp_option[$i]);
                $temp_10 = '';
                if ($temp[0] != '') {
                    $config_data_2 = array(
                        'id' => $temp[0],
                        'table_name' => 'sma_categories',
                        'field_name' => 'title_en',
                        'translate' => 'yes',
                    );
                    $_SESSION['api_temp'] = array();
                    $this->site->api_get_category_arrow($config_data_2);          
                    for ($i2 = count($_SESSION['api_temp']) - 1; ; $i2--) {
                        if ($i2 == 0) {
                            break;
                        }
                        $temp_arrow = '';
                        if ($i2 > 1)
                            $temp_arrow = ' &rarr; ';
                        $temp_10 .= $_SESSION['api_temp'][$i2].$temp_arrow;
                    }   
                }
                $tr_category[$temp[0]] = $temp_10.$temp[1];
            }
            echo form_dropdown('c_id', $tr_category, $_GET['c_id'], 'class="form-control"');
echo '
        </div>
    </div>
';


echo '
    <div class="col-md-2" >
        <div class="form-group">
            <label class="control-label">
                '.lang("Featured").'
            </label>
            <div class="controls"> 
';
                $tr_featured[''] = lang("Any");
                $tr_featured['yes'] = 'Yes';
                $tr_featured['no'] = 'No';
                echo form_dropdown('featured', $tr_featured, $_GET['featured'], 'class="form-control"');
echo '
            </div>
        </div>
    </div>    
';

echo '
    <div class="col-md-2" >
        <div class="form-group">
            <label class="control-label">
                '.lang("Promotion").'
            </label>
            <div class="controls"> 
';
                $tr_promotion[''] = lang("Any");
                $tr_promotion['yes'] = 'Yes';
                $tr_promotion['expired'] = 'Yes (Expired)';
                $tr_promotion['no'] = 'No';
                echo form_dropdown('promotion', $tr_promotion, $_GET['promotion'], 'class="form-control"');
echo '
            </div>
        </div>
    </div>    
';
echo '
    <div class="col-md-2" >
        <div class="form-group">
            <label class="control-label">
                '.lang("Supplier").'
            </label>
            <div class="controls"> 
';
                $tr_supplier[''] = lang("Any");
                $tr_supplier['yes'] = 'Yes';
                $tr_supplier['no'] = 'No';
                echo form_dropdown('supplier', $tr_supplier, $_GET['supplier'], 'class="form-control"');
echo '
            </div>
        </div>
    </div>    
';

echo '
    <div class="api_clear_both"></div>
</div>
';
//-search_fields-================================================
?>

                <div class="table-responsive">
					<div id="SLData_wrapper" class="dataTables_wrapper form-inline" role="grid">
                        <div class="row">
                            <div class="col-md-6 text-left">
                                <div class="short-result">
                                     <label>Show</label>
                                         <select  id="sel_id" name="per_page" style="width: 80px;" onchange="this.form.submit()">
                                             <?php
                                                 $per_page_arr = array(
                                                     '10'=>'10',
                                                     '25'=>'25',
                                                     '50'=> '50',
                                                     '100'=>'100',
                                                     '200'=>'200',
                                                     '500'=>'500',
                                                     '1000'=>'1000',
                                                     'All'=>$total_rows_sale
                                                 );
                                                
                                                 foreach($per_page_arr as $key =>$value){
                                                     $select = $value == $per_page?'selected':'';
                                                     echo '<option value="'.$value.'" '.$select.'>';
                                                             echo $key;
                                                     echo '</option>';
                                                 }

                                             ?>
                                         </select>
                                 </div>
                             </div>

                             <div class="col-md-6 text-right">
                                 <div class="short-result">
                                     <div id="SLData_filter" class="show-data-search dataTables_filter">
                                         <label>Search
                                         <input class="input-xs" value="<?php echo  $this->input->get('search');?>" type="text" name="search" id="mysearch" placeholder="search"/>
                                         <input style="height: 34px;" class="btn btn-primary" type="submit" name="submit_search" value="Submit">
                                         </label>

                                         <div id="result"></div>

                                     </div><br>
                                 </div>    
                             </div>	
                             <div class="clearfix"></div>
                        </div>     
                    </div>   
					
                    <table id="" class="table table-bordered table-condensed table-hover table-striped">
                        <thead>
                        <tr class="primary">
                            <th style="min-width:30px; width: 30px; text-align: center;">
                                <input class="checkbox checkth skip" type="checkbox" name="check" onclick="api_select_check_all(this,'val[]'); api_setValueCheckbox('val[]','check_value','action-form','-')"/>
                            </th>
                            <th style="min-width:40px; width: 40px; text-align: center;"><?php echo $this->lang->line("image"); ?></th>
							<th class="pointer" style="min-width:100px !important; max-width: 100px !important; text-align: center;"> 
                                <?php if($sort_by == "code" and $sort_order=="ASC"){?>
                                    <i class="font-normal" onclick="formactionsubmit('code', 'DESC')" id="fa_sort"  aria-hidden="true"><?php echo lang("Code").' &<br>'.lang("barcode"); ?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort-down"></span></i>
                                <?php }elseif($sort_by == "code" and $sort_order=="DESC"){?>	
                                    <i class="font-normal" onclick="formactionsubmit('code', 'ASC')" id="fa_sort_up"  aria-hidden="true"><?php echo lang("Code").' &<br>'.lang("barcode"); ?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort-up"></span></i>
                                <?php } else{?>	
                                    <i class="font-normal" onclick="formactionsubmit('code', 'DESC')" id="fa_sort_down" aria-hidden="true"><?php echo lang("Code").' &<br>'.lang("barcode"); ?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort"></span></i>
                                <?php } ?>	
                            </th>
							<th class="pointer"> 
                                <?php if($sort_by == "name" and $sort_order=="ASC"){?>
                                    <i class="font-normal" onclick="formactionsubmit('name', 'DESC')" id="fa_sort"  aria-hidden="true"><?php echo $this->lang->line("name"); ?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort-down"></span></i>
                                <?php }elseif($sort_by == "name" and $sort_order=="DESC"){?>	
                                    <i class="font-normal" onclick="formactionsubmit('name', 'ASC')" id="fa_sort_up"  aria-hidden="true"><?php echo $this->lang->line("name"); ?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort-up"></span></i>
                                <?php } else{?>	
                                    <i class="font-normal" onclick="formactionsubmit('name', 'DESC')" id="fa_sort_down" aria-hidden="true"><?php echo $this->lang->line("name"); ?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort"></span></i>
                                <?php } ?>	
                            </th>
							<th class=""> 
                                <?php echo $this->lang->line("category"); ?>
                            </th>
                            
                            <?php
                            if ($Owner || $Admin) {
								?>
								<th class="pointer"> 
									<?php if($sort_by == "cost" and $sort_order=="ASC"){?>
										<i class="font-normal" onclick="formactionsubmit('cost', 'DESC')" id="fa_sort"  aria-hidden="true"><?php echo $this->lang->line("cost"); ?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort-down"></span></i>
									<?php }elseif($sort_by == "cost" and $sort_order=="DESC"){?>	
										<i class="font-normal" onclick="formactionsubmit('cost', 'ASC')" id="fa_sort_up"  aria-hidden="true"><?php echo $this->lang->line("cost"); ?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort-up"></span></i>
									<?php } else{?>	
										<i class="font-normal" onclick="formactionsubmit('cost', 'DESC')" id="fa_sort_down" aria-hidden="true"><?php echo $this->lang->line("cost"); ?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort"></span></i>
									<?php } ?>	
								</th>
								<th class="pointer"> 
									<?php if($sort_by == "price" and $sort_order=="ASC"){?>
										<i class="font-normal" onclick="formactionsubmit('price', 'DESC')" id="fa_sort"  aria-hidden="true"><?php echo $this->lang->line("price"); ?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort-down"></span></i>
									<?php }elseif($sort_by == "price" and $sort_order=="DESC"){?>	
										<i class="font-normal" onclick="formactionsubmit('price', 'ASC')" id="fa_sort_up"  aria-hidden="true"><?php echo $this->lang->line("price"); ?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort-up"></span></i>
									<?php } else{?>	
										<i class="font-normal" onclick="formactionsubmit('price', 'DESC')" id="fa_sort_down" aria-hidden="true"><?php echo $this->lang->line("price"); ?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort"></span></i>
									<?php } ?>	
								</th>
								<?php
                                
                            } else {
                                if ($this->session->userdata('show_cost')) {
                                    ?>
									<th class="pointer"> 
										<?php if($sort_by == "cost" and $sort_order=="ASC"){?>
											<i class="font-normal" onclick="formactionsubmit('cost', 'DESC')" id="fa_sort"  aria-hidden="true"><?php echo $this->lang->line("cost"); ?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort-down"></span></i>
										<?php }elseif($sort_by == "cost" and $sort_order=="DESC"){?>	
											<i class="font-normal" onclick="formactionsubmit('cost', 'ASC')" id="fa_sort_up"  aria-hidden="true"><?php echo $this->lang->line("cost"); ?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort-up"></span></i>
										<?php } else{?>	
											<i class="font-normal" onclick="formactionsubmit('cost', 'DESC')" id="fa_sort_down" aria-hidden="true"><?php echo $this->lang->line("cost"); ?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort"></span></i>
										<?php } ?>	
									</th>
								<?php
                                }
                                if ($this->session->userdata('show_price')) {
                                    ?>
									<th class="pointer"> 
										<?php if($sort_by == "price" and $sort_order=="ASC"){?>
											<i class="font-normal" onclick="formactionsubmit('price', 'DESC')" id="fa_sort"  aria-hidden="true"><?php echo $this->lang->line("price"); ?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort-down"></span></i>
										<?php }elseif($sort_by == "price" and $sort_order=="DESC"){?>	
											<i class="font-normal" onclick="formactionsubmit('price', 'ASC')" id="fa_sort_up"  aria-hidden="true"><?php echo $this->lang->line("price"); ?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort-up"></span></i>
										<?php } else{?>	
											<i class="font-normal" onclick="formactionsubmit('price', 'DESC')" id="fa_sort_down" aria-hidden="true"><?php echo $this->lang->line("price"); ?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort"></span></i>
										<?php } ?>	
									</th>
								<?php
                                }
                            }
                            ?>
                            <th class="pointer api_display_none"> 
                                <?php if($sort_by == "total_quantity" and $sort_order=="ASC"){?>
                                    <i class="font-normal" onclick="formactionsubmit('total_quantity', 'DESC')" id="fa_sort"  aria-hidden="true"><?php echo lang("Total"); ?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort-down"></span></i>
                                <?php }elseif($sort_by == "total_quantity" and $sort_order=="DESC"){?>    
                                    <i class="font-normal" onclick="formactionsubmit('total_quantity', 'ASC')" id="fa_sort_up"  aria-hidden="true"><?php echo lang("Total"); ?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort-up"></span></i>
                                <?php } else{?> 
                                    <i class="font-normal" onclick="formactionsubmit('total_quantity', 'DESC')" id="fa_sort_down" aria-hidden="true"><?php echo lang("Total"); ?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort"></span></i>
                                <?php } ?>  
                            </th>
<?php
    for ($i2=0;$i2<count($api_warehouse);$i2++) {
        if ($i2 == 0)
            $temp = '';
        else
            $temp = '_'.$api_warehouse[$i2]['id'];
        echo '
            <th class="pointer api_display_none"> 
        ';        
            if ($sort_by == 'quantity'.$temp and $sort_order == "ASC")
                echo '
                    <i class="font-normal" onclick="formactionsubmit(\'quantity'.$temp.'\', \'DESC\')" id="fa_sort"  aria-hidden="true">'.$api_warehouse[$i2]['code'].'&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort-down"></span></i>
                ';
            elseif ($sort_by == 'quantity'.$temp and $sort_order == "DESC")
                echo '
                    <i class="font-normal" onclick="formactionsubmit(\'quantity'.$temp.'\', \'ASC\')" id="fa_sort_up"  aria-hidden="true">'.$api_warehouse[$i2]['code'].'&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort-up"></span></i>
                ';
            else
                echo '                
                    <i class="font-normal" onclick="formactionsubmit(\'quantity'.$temp.'\', \'DESC\')" id="fa_sort_down" aria-hidden="true">'.$api_warehouse[$i2]['code'].'&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort"></span></i>
                ';
        echo '                
            </th>
        ';
    }
?>
							<th class=""> 
                                <?php echo lang("Ordering"); ?>
							</th>
							<th class=""> 
                                <?php echo $this->lang->line("unit"); ?>
							</th>
                            <th class="pointer"> 
                                <?php if($sort_by == "hide" and $sort_order=="ASC"){?>
                                    <i class="font-normal" onclick="formactionsubmit('hide', 'DESC')" id="fa_sort"  aria-hidden="true"><?php echo lang("shop"); ?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort-down"></span></i>
                                <?php }elseif($sort_by == "hide" and $sort_order=="DESC"){?>  
                                    <i class="font-normal" onclick="formactionsubmit('hide', 'ASC')" id="fa_sort_up"  aria-hidden="true"><?php echo lang("shop"); ?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort-up"></span></i>
                                <?php } else{?> 
                                    <i class="font-normal" onclick="formactionsubmit('hide', 'DESC')" id="fa_sort_down" aria-hidden="true"><?php echo lang("shop"); ?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort"></span></i>
                                <?php } ?>  
                            </th>                            
                       

                            <th class=""> 
                                <?php echo lang("Featured"); ?>
                            </th>
                            <th class=""> 
                                <?php echo lang("Promotion"); ?>
                            </th> 
                            <th class=""> 
                                <?php echo lang("Supplier"); ?>
                            </th>                            
                            <th style="min-width:65px; text-align:center;"><?= lang("actions") ?></th>
                        </tr>
                        </thead>
                        <tbody role="alert" aria-live="polite" aria-relevant="all">

                            <?php
for ($i=0;$i<count($select_data);$i++) {
    if ($select_data[$i]['no_use'] == 'yes') {
        $temp_bg = 'api_td_disabled';
        $temp_no_use = '<label class="label label-danger">'.lang('No_more_use').'</label>';
    }
    else {
        $temp_bg = '';
        $temp_no_use = '';
    }
									$detail_link = anchor('admin/products/view/'.$select_data[$i]['id'], '<i class="fa fa-file-text-o"></i> ' . lang('product_details'));
									$single_barcode = anchor('admin/products/print_barcodes/'.$select_data[$i]['id'], '<i class="fa fa-print"></i> ' . lang('print_barcode_label'));
									$duplicate_product = anchor('admin/products/add/'.$select_data[$i]['id'], '<i class="fa fa-plus-square"></i> ' . lang('duplicate_product'));$edit_product = anchor('admin/products/edit/'.$select_data[$i]['id'], '<i class="fa fa-edit"></i>' . lang('edit_product'));
									$delete_link = "<a href='#' class='tip po' title='<b>" . $this->lang->line("delete_product") . "</b>' data-content=\"<p>"
									. lang('r_u_sure') . "</p><a class='btn btn-danger po-delete1' id='a__$1' href='" . admin_url('products/delete/$1') . "'>"
									. lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i> "
									. lang('delete_product') . "</a>";
								
									
									?>
								<tr id='<?php echo $select_data[$i]['id']; ?>' class="product_link">
									<td class="<?php echo $temp_bg; ?>" style="min-width:30px; width: 30px; text-align: center;">
<?php                                        
                                        echo '
										<div class="text-center">
											<input class="checkbox multi-select skip" type="checkbox" name="val[]" value="'.$select_data[$i]['id'].'" onclick="api_setValueCheckbox(\'val[]\',\'check_value\',\'action-form\',\'-\')"/>
										</div>
                                        ';
?>
									</td>

									<td class="<?php echo $temp_bg; ?>">
										<div class="text-center">
<?php
    if (is_file('assets/uploads/'.$select_data[$i]['image']) && $select_data[$i]['image'] != 'no_image.png')
        echo '
            <a href="'.site_url('assets/uploads/'.$select_data[$i]['image']).'" data-toggle="lightbox">
                <img src="'.site_url('assets/uploads/thumbs/'.$select_data[$i]['image']).'" alt="" style="width:30px; height:30px;">
            </a>
        ';
/*        
    else
        echo '
            <a href="'.site_url('assets/uploads/no_image.png').'" data-toggle="lightbox">
                <img src="'.site_url('assets/uploads/thumbs/no_image.png').'" alt="" style="width:30px; height:30px;">
            </a>
        ';
*/
?>
                                        </div>
									</td>
									
									<td class="<?php echo $temp_bg; ?>">
                                        <?php 
                                            echo '
                                                '.$select_data[$i]['code'].' <br>
                                                '.$select_data[$i]['barcode'].' '.$temp_no_use.'
                                            ';
                                        ?>
                                    </td>
									<td class="<?php echo $temp_bg; ?>"><?= $select_data[$i]['name'] ?></td>
									<td class="<?php echo $temp_bg; ?>">
                                        <?php
if ($select_data[$i]['category_id'] > 0) {
    $temp = $this->site->api_select_some_fields_with_where("
        code     
        "
        ,"sma_categories"
        ,"id = ".$select_data[$i]['category_id']." and parent_id = 0"
        ,"arr"
    );                                    
    if (count($temp) > 0)
        echo $temp[0]['code'];
    else
        echo '<span class="api_color_red">'.lang('None').'</span>';

}
if ($select_data[$i]['subcategory_id'] > 0) {
    $temp = $this->site->api_select_some_fields_with_where("
        code     
        "
        ,"sma_categories"
        ,"id = ".$select_data[$i]['subcategory_id']." and parent_id > 0"
        ,"arr"
    );
    if (count($temp) > 0)
        echo '<b>&nbsp; | &nbsp;</b>'.$temp[0]['code'];
    else
        echo '<b>&nbsp; | &nbsp;</b><span class="api_color_red">'.lang('None').'</span>';    
}
                                        ?>    
                                    </td>
									<?php
									if ($Owner || $Admin) {
										echo '<td class="'.$temp_bg.'">' . number_format($select_data[$i]['cost'],2) . '</td>';
										echo '<td class="'.$temp_bg.'">' . number_format($select_data[$i]['price'],2) . '</td>';
									} else {
										if ($this->session->userdata('show_cost')) {
											echo '<td class="'.$temp_bg.'">' . number_format($select_data[$i]['cost'],2) . '</td>';
										}
										if ($this->session->userdata('show_price')) {
											echo '<td class="'.$temp_bg.'">' . number_format($select_data[$i]['price'],2) . '</td>';
										}
									}
									?>
<?php
    $temp = '';    
    $temp_total = 0;
    for ($i2=0;$i2<count($api_warehouse);$i2++) {
        if ($select_data[$i]['quantity_'.$api_warehouse[$i2]['id']] == '')
            $select_data[$i]['quantity_'.$api_warehouse[$i2]['id']] = 0;
        if ($select_data[$i]['quantity_'.$api_warehouse[$i2]['id']] == '')
            $select_data[$i]['quantity_'.$api_warehouse[$i2]['id']] = 0;

        if ($api_warehouse[$i2]['id'] == 1) {
            $temp .= '
                <td class="'.$temp_bg.'" align="right">
                    '.$this->site->api_number_format($select_data[$i]['quantity'],2).'
                </td>
            ';
            $temp_total = $temp_total + $select_data[$i]['quantity'];
        }
        else {
            $temp .= '
                <td class="'.$temp_bg.'" align="right">
                    '.$this->site->api_number_format($select_data[$i]['quantity_'.$api_warehouse[$i2]['id']],2).'
                </td>
            ';
            $temp_total = $temp_total + $select_data[$i]['quantity_'.$api_warehouse[$i2]['id']];
        }
    }
    // echo '
    //     <td class="'.$temp_bg.'" align="right">
    //         '.$this->site->api_number_format($temp_total,2).'
    //     </td>
    // ';
    // echo $temp;
    echo '
        <td class="'.$temp_bg.'" align="center">
            <input type="text" class="form-control" value="'.$select_data[$i]['ordering'].'" style="width:50px; text-align:center;" onchange="
            var postData = {
                \'category_id\' : '.$select_data[$i]['category_id'].', 
                \'table_name\' : \'sma_products\', 
                \'field_id\' : '.$select_data[$i]['id'].',                
                \'field_value\' : this.value,
                \'redirect\' : \''.base_url().'admin/products\',
            };
            api_ajax_update_ordering(postData);
            ";   
        </td>
    ';
?>
									<td class="<?php echo $temp_bg; ?>">
                                        <?= $select_data[$i]['unit_name'] ?>
                                    </td>
                                    <td class="<?php echo $temp_bg; ?>" align="center">
<?php
    if ($select_data[$i]['hide'] == 0)
        $temp = '<span class="label label-default"> '.lang('None').'</span>';
    if ($select_data[$i]['hide'] == 1)
        $temp = '<span class="label label-danger"><i class="fa fa-check"></i> '.lang('Hide').'</span>';
    if ($select_data[$i]['hide'] == 2)
        $temp = '<span class="label label-success"><i class="fa fa-check"></i> '.lang('Show').'</span>';

    $temp_display = '
        <a href="javascript:void(0);" onclick="
var postData = {
    \'ajax_file\' : \''.admin_url('products/ajax_update_hide_show').'\',
    \'field_id\' : \''.$select_data[$i]['id'].'\',
    \'element_id\' : \'api_page_admin_product_hide_'.$select_data[$i]['id'].'\',
};
ajax_update_hide_show(postData);
        ">
            '.$temp.'
        </a>
    ';

    echo '
        <div id="api_page_admin_product_hide_'.$select_data[$i]['id'].'">
            '.$temp_display.'
        </div>
    ';
?>
                                        
                                    </td>                                    

<?php
    $temp4 = '';
    $temp5 = '';
    $temp2 = explode('-',$select_data[$i]['show_customer_list_selected']);
    for ($i2=0;$i2<count($temp2);$i2++) {
        if ($temp2[$i2] != '') {
            $temp3 = $this->site->api_select_some_fields_with_where("
                name
                "
                ,"sma_companies"
                ,"id = ".$temp2[$i2]
                ,"arr"
            );
            if (is_array($temp3)) if (count($temp3) > 0) {
                if (count($temp2) > 0)
                    $temp4 = $temp3[0]['name'];
                if (count($temp2) > 1)
                    $temp4 = $temp3[0]['name'].', etc..';
            }
            break;                
        }
    }
    if ($temp4 != '') 
        $temp6 = '
            <span class="label label-success" title="'.$temp4.'">
            <i class="fa fa-check"></i> Yes</span>
        ';
    else
        $temp6 = '
            <span class="label label-danger">
            <i class="fa fa-check"></i> No</span>
        ';
 


    if ($select_data[$i]['featured'] == 1) {
        $temp_display = '
            <a href="javascript:void(0);" onclick="
    var postData = {
        \'ajax_file\' : \''.admin_url('products/ajax_update_featured').'\',
        \'field_id\' : \''.$select_data[$i]['id'].'\',
        \'element_id\' : \'api_page_admin_product_featured_'.$select_data[$i]['id'].'\',
    };
    ajax_update_featured(postData);
            ">
                <span class="label label-success">
                <i class="fa fa-check"></i> Yes</span>
            </a>
        ';
    }
    else {
        $temp_display = '
            <a href="javascript:void(0);" onclick="
    var postData = {
        \'ajax_file\' : \''.admin_url('products/ajax_update_featured').'\',
        \'field_id\' : \''.$select_data[$i]['id'].'\',
        \'element_id\' : \'api_page_admin_product_featured_'.$select_data[$i]['id'].'\',
    };
    ajax_update_featured(postData);
            ">
                <span class="label label-danger">
                <i class="fa fa-check"></i> No</span>
            </a>
        ';
    }
    echo '
        <td class="'.$temp_bg.'" align="center">
            <div id="api_page_admin_product_featured_'.$select_data[$i]['id'].'">
                '.$temp_display.'
            </div>
        </td>                                    
    ';

    if ($select_data[$i]['promotion_status'] == 'yes')
        $temp_display = '
            <span class="label label-success">
            <i class="fa fa-check"></i> Yes</span>
        ';
    if ($select_data[$i]['promotion_status'] == 'expired')
        $temp_display = '
            <span class="label label-default">
            '.lang('Expired').'</span>
        ';
    if ($select_data[$i]['promotion_status'] == 'no')
        $temp_display = '
            <span class="label label-danger">
            <i class="fa fa-check"></i> No</span>
        ';

    echo '
        <td class="'.$temp_bg.'" align="center">
            '.$temp_display.'
        </td>                                    
    ';
    
    
    if ($select_data[$i]['supplier1'] > 0 || $select_data[$i]['supplier2'] > 0 || $select_data[$i]['supplier3'] > 0 || $select_data[$i]['supplier4'] > 0 || $select_data[$i]['supplier5'] > 0) {
        $temp_display = '
            <span class="label label-success">Yes</span>
        ';
    } else {
        $temp_display = '
            <span class="label label-danger">No</span>
        ';
    }
    echo '
        <td align="center">
            '.$temp_display.'
        </td>                                    
    ';

?>

									<td class="<?php echo $temp_bg; ?>" style="width:80px; text-align:center;">
										<div class="text-center">
											<div class="btn-group text-left">
												<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">
												 <?php echo $this->lang->line("actions"); ?><span class="caret"></span></button>
												<ul class="dropdown-menu pull-right" role="menu">
                                                    <li><?php echo $edit_product; ?></li>
													<li><?php echo $duplicate_product; ?></li>
													<li><?php echo $single_barcode; ?></li>
													<li>
														<a href="<?= site_url('assets/uploads/'.$select_data[$i]['image'])?>" data-type="image" data-toggle="lightbox"><i class="fa fa-file-photo-o"></i><?=lang('view_image')?></a>
													</li>
													<li class="divider"></li>
													<li>
														<a href="#" class="bpo" title="<b><?= $this->lang->line("delete_products") ?></b>"
														data-content="<p><?= lang('r_u_sure') ?></p><a href='<?=admin_url('products/delete/'.$select_data[$i]['id'])?>' class='btn btn-danger' id='<?=$select_data[$i]['id']?>' ><?= lang('i_m_sure') ?></a></a> <button class='btn bpo-close'><?= lang('no') ?></button>"
														data-html="true" data-placement="left">
														<i class="fa fa-trash-o"></i> <?= lang('delete_products') ?>
														</a>
													</li>	
												</ul>
											</div>
										</div>
									</td>                                    
								</tr>	
									<?php
}
							?>
<?php
if (count($select_data) <= 0) {
    $temp = 14 + count($api_warehouse);
    echo '
    <tr>
    <td colspan="'.$temp.'">
        '.lang('No_record_found').'
    </td>
    </tr>
    ';
}
?>                            
                        </tbody>

                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th style="min-width:30px; width: 30px; text-align: center;">
                                <input class="checkbox checkft skip" type="checkbox" name="check" onclick="api_select_check_all(this,'val[]'); api_setValueCheckbox('val[]','check_value','action-form','-')"/>
                            </th>
                            <th style="min-width:40px; width: 40px; text-align: center;"><?php echo $this->lang->line("image"); ?></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <?php
                            if ($Owner || $Admin) {
                                echo '<th></th>';
                                echo '<th></th>';
                            } else {
                                if ($this->session->userdata('show_cost')) {
                                    echo '<th></th>';
                                }
                                if ($this->session->userdata('show_price')) {
                                    echo '<th></th>';
                                }
                            }
                            echo '
                            <th></th>
                            ';
                            for ($i2=0;$i2<count($api_warehouse);$i2++) {
                                echo '<th></th>';
                            }
                            ?>
                            <th></th>
                            <th></th>
                            <th style="width:65px; text-align:center;"><?= lang("actions") ?></th>
                        </tr>
                        </tfoot>
                    </table>
					<div class="row">
                        <div class="col-lg-12">
                            <div class="panel-heading-left">
                                <p class="data-total">
                                    <?php echo $show;?>
                                </p>
                            </div>   
                            <div class="panel-heading-right">
                                <p>
                                   <?php echo $this->pagination->create_links(); //pagination links ?>
                                </p>
                            </div>  
                            <div class="panel-heading-clear-fix"></div>   
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" name="warehouse_id" value="<?php echo $warehouse_id;?>" id="warehouse_id"/>
<input type="hidden" name="show_data" value="<?php echo $show_data;?>" id="show_data"/>
<input type="hidden" name="sort_by" value="<?php echo $sort_by;?>" id="sort_by"/>
<input type="hidden" name="sort_order" value="<?php echo $sort_order;?>" id="sort_order"/>

<?php
echo form_close();

echo admin_form_open('products/product_actions','id="api_form_action" name="api_form_action" method="post"');
echo '
    <div class="api_display_none">
            <input type="text" name="check_value" value="" id="check_value"/>
            <input type="text" name="api_action" value="" id="api_action"/>
            <button type="button" onclick="document.api_form_action.submit();" value="ok">ok</button>
    </div>
';
echo form_close();
?>

<style>    
    .modal-body h2{
        font-size: 20px;
        margin-top: 8px;
    }
    .modal-body p{
        font-size: 18px;
        margin-bottom: 17px;
    }
    #tbl-update_to_due {
        position: relative;
        padding: 0px 15px 15px 15px !important;
        margin-top:15px;
    }
    #tbl-update_to_paid {
        position: relative;
        padding: 0px 15px 15px 15px !important;
        margin-top:15px;
    }
    #tbl-update_to_partail {
        position: relative;
        padding: 0px 15px 15px 15px !important;
        margin-top:15px;
    }
    h4.modal-title{
        font-size: 18px;
    }
    .panel-heading-left {
        float: left;
    }
    .panel-heading-right {
        float: right;
    }
    .pointer{
        cursor:pointer;
    }
    .font-normal{
        font-style: normal;
    }
    
    .pagination>li>a, .pagination>li>span {
        position: relative;
        float: left;
        padding: 5px 10px;
        margin-left: -1px;
        line-height: 1.42857143;
        color: #428bca;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
        font-size: 13px;
    }
    .pagination {
        display: inline-block;
        padding-left: 0;
        margin: -5px 0;
        border-radius: 4px;
    }
    .txt-f{
        color:#333 !important
    }
    #loading{
        display:none !important;
        visibility : hidden;
    }
</style>

<script>
     
    function formactionsubmit(sort_by, sort_order) {
        document.getElementById("sort_by").value = sort_by;
        document.getElementById("sort_order").value = sort_order;
        document.forms[0].submit();

    }

function api_bulk_actions(action){
    $("#api_action").val(action);
    $("#api_form_action").submit();
} 
function bulk_actions_delete(){
    $("#api_action").val('delete');
    document.api_form_action.submit();
} 

<?php
    $temp = $_GET['page'];
    if ($temp == '') $temp = 1;
    echo '
        $(".pagination .active").html(\'<a href="javascript:void(0);">'.$temp.'</a>\');
    ';
?>

function ajax_update_hide(postData){
    var result = $.ajax
    (
        {
            url: postData['ajax_file'],
            type: 'GET',
            secureuri:false,
            dataType: 'html',
            data:postData,
            async: false,
            error: function (response, status, e)
            {
                alert(e);
            }
        }
    ).responseText;
    var array_data = String(result).split("api-ajax-request-multiple-result-split");
    var b = array_data[0];
    var result_text = array_data[1];
    $("#" + postData['element_id']).html(array_data[2]);
}

function ajax_update_featured(postData){
    var result = $.ajax
    (
        {
            url: postData['ajax_file'],
            type: 'GET',
            secureuri:false,
            dataType: 'html',
            data:postData,
            async: false,
            error: function (response, status, e)
            {
                alert(e);
            }
        }
    ).responseText;
    var array_data = String(result).split("api-ajax-request-multiple-result-split");
    var b = array_data[0];
    var result_text = array_data[1];
    $("#" + postData['element_id']).html(array_data[2]);
}
function ajax_update_hide_show(postData){
    var result = $.ajax
    (
        {
            url: postData['ajax_file'],
            type: 'GET',
            secureuri:false,
            dataType: 'html',
            data:postData,
            async: false,
            error: function (response, status, e)
            {
                alert(e);
            }
        }
    ).responseText;
    var array_data = String(result).split("api-ajax-request-multiple-result-split");
    var b = array_data[0];
    var result_text = array_data[1];
    $("#" + postData['element_id']).html(array_data[2]);
}

</script>
<style type="text/css">
    .product_link td:nth-child(3),.product_link td:nth-child(6),.product_link td:nth-child(7),.product_link td:nth-child(8),.product_link td:nth-child(9),.product_link td:nth-child(10),.product_link td:nth-child(11),.product_link td:nth-child(12) , .product_link td:nth-child(13), .product_link td:nth-child(14), .product_link td:nth-child(15){
        cursor: default;
    }
</style>

