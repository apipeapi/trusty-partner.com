<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php 
    $temp_url = '';
    if ($_GET['page'] != '') 
        $temp_url .= '?page='.$_GET['page']; 
    else 
        $temp_url .= '?page=1'; 

    if ($per_page != '') $temp_url .= '&per_page='.$per_page;

    foreach ($_GET as $name => $value) {
        if ($name != 'page' && $name != 'per_page' && $name != 'mode')
            if ($value != '') $temp_url .= '&'.$name.'='.$value;
    }

    $api_list_view_current_page = $_GET['page'];
    if ($api_list_view_current_page == '') $api_list_view_current_page = 1;

    echo admin_form_open($api_list_view['controller'].'/'.$api_list_view['object_name'].$temp_url,'id="'.$api_list_view['form_name'].'" name="'.$api_list_view['form_name'].'" method="GET"');
?>



<div class="box">
    <div class="box-header">
        <h2 class="blue">
            <i class="fa-fw fa fa-users"></i>
            <?php
                echo $api_list_view['page_title']; 
            ?>
        </h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon fa fa-tasks tip" data-placement="left" title="<?=lang("actions")?>"></i>
                    </a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
<?php
//-action_list-============================
    echo '
        <li>
            <a href="'.admin_url($api_list_view['controller'].'/edit_'.$api_list_view['object_name']).'" data-toggle="modal" data-target="#myModal">
                <i class="fa fa-plus-circle"></i> '.lang('Add').' '.$api_list_view['object_title'].'
            </a>
        </li>
    ';      
    echo '
        <li class="divider"></li>
        <li>
            <a href="#" class="bpo" title="<b>'.lang("Delete").' '.$api_list_view['object_title'].'</b>" data-content="<p'.lang('r_u_sure').'</p>
            <button type=\'button\' class=\'btn btn-danger\' onclick=\'bulk_actions_delete();\' data-action=\'delete\'>'.lang('i_m_sure').'</a> <button class=\'btn bpo-close\'>`'.lang('no').'</button>" data-html="true" data-placement="left">
                <i class="fa fa-trash-o"></i> '.lang("Delete").'
            </a>
        </li>
    ';
//-action_list-============================
?>
                    </ul>
                </li>
            </ul>
        </div>

    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
<?php            
//-top_buttons-================================================
    echo '<p class="introtext">'.lang('list_results').'</p>';


//-top_buttons-================================================
?>
                <div class="table-responsive">
<?php
    // echo '
    //     <div class="col-md-12 api_padding_0">
    // ';

    // echo '
    //     <div class="col-md-2 api_padding_0 pull-right">
    //         <div class="form-group">
    //         '.lang("Search", "Search").'
    //         <input type="text" class="form-control" id="search" name="search" value="'.(isset($_GET['search']) ? $_GET['search'] : "").'" onkeydown="if (event.keyCode == 13) document.'.$api_list_view['form_name'].'.submit()" />
    //         </div>
    //     </div>
    // ';
    // echo '
    //     </div>
    // ';
//-list_view_searh-================================================
    echo '
        <div class="row">
            <div class="col-md-6 text-left api_padding_top_5">
                <div class="short-result">
                        <span>'.lang('Show').'</span>
                            <select name="per_page" onchange="$(\'#api_action\').val(\'\'); $(\'#'.$api_list_view['form_name'].'\').submit();" style="width:80px;">
    ';
                                $per_page_arr = array(
                                    '10'=>'10',
                                    '25'=>'25',
                                    '50'=> '50',
                                    '100'=>'100',
                                    '200'=>'200',
                                    '500'=>'500',
                                    'All'=>$total_rows_sale
                                );
                            
                                foreach($per_page_arr as $key =>$value){
                                    $select = $value == $per_page?'selected':'';
                                    echo '<option value="'.$value.'" '.$select.'>';
                                            echo $key;
                                    echo '</option>';
                                }
    echo '
                            </select>
                    </div>
            </div>
    ';
    // echo '
    //     <div class="col-md-6 text-right api_padding_top_5">
    //         <div class="form-group pull-right api_padding_left_10">
    //             <a href="'.base_url().'admin/'.$api_list_view['controller'].'/'.$api_list_view['object_name'].'">
    //                 <input type="button" name="reset" value="reset" class="btn btn-danger">
    //             </a>
    //         </div>
    //         <div class="form-group pull-right">
    //             <input type="button" name="submit_form" onclick="$(\'#api_action\').val(\'\'); $(\'#'.$api_list_view['form_name'].'\').submit();" value="Submit" class="btn btn-primary">
    //         </div>
    //     </div>
    // ';
                                    
    echo '
        <div class="col-md-6 text-right">
            <div class="short-result">
                <div id="SLData_filter" class="show-data-search dataTables_filter">
                    <label>
                        Search
                        <input class="input-xs" value="'.$this->input->get('search').'" type="text" name="search" placeholder="Search"/>
                        <input style="height: 34px;" class="btn btn-primary" type="submit" name="submit_search" value="'.lang('Search').'">
                    </label>                                        
                </div>
            </div>    
        </div>
    ';
    echo ' 
        </div>
    ';
//-list_view_searh-================================================
?>

<?php
    $row_headers = array(
        'checkbox' => [
            'label' => '<input class="checkbox checkth skip" onclick="api_select_check_all(this,\'val[]\'); api_setValueCheckbox(\'val[]\',\'check_value\',\''.$api_list_view['form_name'].'\',\'-\')" type="checkbox" name="check"/>', 
            'sort_fn' => false,
            'style' => 'min-width:30px; width: 30px; text-align: center;'
        ],
        'name' => [
            'label' => lang("Name"),
        ],
        'image'          => [
            'sort_fn' => false,
            'label' => lang("image"), 'style' => 'min-width:80px; width: 80px; text-align: center;'
        ], 
        'Duration' => [
            'label' => lang("Duration"),
            'sort_fn' => false
        ],
        'type' => [
            'label' => lang("Discount_Type"),
            'style' => 'width:150px',
            'sort_fn' => false
        ],
        'discount' => [
            'label' => lang("Discount"),
            'style' => 'width:150px',
            'sort_fn' => false
        ],                
        'min_qty' => [
            'label' => lang("Min_Qty_Order"),
            'style' => 'width:150px'
        ],
        'ordering' => [
            'label' => lang("Ordering"),
            'style' => 'width:50px',
        ],
        'status' => [
            'label' => lang("Status"),
            'style' => 'width:150px'
        ],
        'action' => [
            'label' => lang("actions"),
            'style' => 'width:80px; text-align:center;',
            'sort_fn' => false
        ],
    );
?>
<table id="api_list_view" class="api_list_view table table-bordered table-hover table-striped" cellpadding="0" cellspacing="0" border="0">
<thead>
<tr class="primary">
<?php foreach ($row_headers as $key => $row) :?>
    <?php
    $th_class="pointer";
    $e_click = true;
    if(isset($row['sort_fn']) && $row['sort_fn'] == false) {
        $th_class="";
        $e_click = false;
    }
    $style = isset($row['style']) && $row['style'] != '' ? 'style="'.$row['style'].'"' : '';
    ?>
    <th <?= $style ?> >
        <?php if($e_click ) :?>
            <?php if ($sort_by == $key && $sort_order == "ASC") : ?>
                    <label class="font-normal <?=$th_class?>" onclick="formactionsubmit('<?= $key ?>', 'DESC')" id="fa_sort"  aria-hidden="true">
                    <?php echo $row['label']; ?>
                        <span class="fa fa-sort-down"></span>
                    </label>
            <?php elseif ($sort_by == $key and $sort_order=="DESC") : ?>    
                <label class="font-normal <?=$th_class?>" onclick="formactionsubmit('<?= $key ?>', 'ASC')" id="fa_sort_up"  aria-hidden="true">
                    <?php echo $row['label']; ?>
                    <span class="fa fa-sort-up"></span>
                </label>
            <?php else : ?> 
                <label class="font-normal <?=$th_class?>" onclick="formactionsubmit('<?= $key ?>', 'DESC')" id="fa_sort_down" aria-hidden="true">
                    <?php echo $row['label']; ?>
                    <span class="fa fa-sort"></span>
                </label>
            <?php endif; ?> 
        <?php else : ?>
            <label class="font-normal <?=$th_class?>" aria-hidden="true">
                <?php echo $row['label']; ?>
            </label>
        <?php endif; ?> 
    </th>
<?php endforeach; ?>
</tr>
</thead>
<tbody role="alert" aria-live="polite" aria-relevant="all">

<?php
//-view_data-======================================================================
for ($i=0;$i<count($select_data);$i++) {

    $temp7 = '
        <div class="text-center">
            <input class="checkbox multi-select skip" type="checkbox" name="val[]" value="'.$select_data[$i]['id'].'" id="api_list_check_'.$select_data[$i]['id'].'" onclick="api_setValueCheckbox(\'val[]\',\'check_value\',\''.$api_list_view['form_name'].'\',\'-\');"/>
        </div>        
    ';

    $temp6 = '
        <a class="tip api_margin_right_5" title="Edit" href="'.admin_url($api_list_view['controller'].'/edit_'.$api_list_view['object_name'].'/'.$select_data[$i]['id']).'" data-toggle="modal" data-target="#myModal"><i class="fa fa-edit"></i>
        </a>
        <a href="#" class="tip po" title="" data-content="<p>Are you sure?</p> <a href=\''.base_url().'admin/'.$api_list_view['controller'].'/delete_'.$api_list_view['object_name'].'/'.$select_data[$i]['id'].'\'><button type=\'button\' class=\'btn btn-danger\' data-action=\'delete\'>Yes I\'m sure</button></a> <button class=\'btn po-close\'>No</button>" data-html="true" data-placement="left" data-original-title="<b>Delete</b>">
        <i class="fa fa-trash-o"></i>
    </a>
    ';

    if ($select_data[$i]['type_discount'] == 'percentage') {
        $temp_2 = lang('Percentage');
        $temp_3 = $select_data[$i]['rate'].'%';
    }
    if ($select_data[$i]['type_discount'] == 'fixed_price') {
        $temp_2 = lang('Fixed_Price');        
        $temp_3 = $this->sma->formatMoney($select_data[$i]['fixed_price']);    
    }
    if ($select_data[$i]['status'] == '' || $select_data[$i]['status'] == 'enabled')
        $temp_4 = '
            <div class="text-center" id="api_ajax_update_display_status_'.$select_data[$i]['id'].'">
                <a href="javascript:void(0);" onclick="
        var postData = {
            \'ajax_file\' : \''.admin_url('welcome/api_ajax_update_display').'\',
            \'table_name\' : \'sma_product_promotion\',
            \'field_id\' : \'id\',
            \'field_value\' : \''.$select_data[$i]['id'].'\',
            \'display_type\' : \'enabled_disabled\',
            \'update_field_name\' : \'status\',
            \'element_id\' : \'api_ajax_update_display_status_'.$select_data[$i]['id'].'\',
        };
        api_ajax_update_display(postData);  
                ">
                    <span class="label label-success">
                    <i class="fa fa-check"></i> Enabled</span>
                </a>
            </div>
        ';
    if ($select_data[$i]['status'] == 'disabled')
        $temp_4 = '
            <div class="text-center" id="api_ajax_update_display_status_'.$select_data[$i]['id'].'">
                <a href="javascript:void(0);" onclick="
        var postData = {
            \'ajax_file\' : \''.admin_url('welcome/api_ajax_update_display').'\',
            \'table_name\' : \'sma_product_promotion\',
            \'field_id\' : \'id\',
            \'field_value\' : \''.$select_data[$i]['id'].'\',
            \'display_type\' : \'enabled_disabled\',
            \'update_field_name\' : \'status\',
            \'element_id\' : \'api_ajax_update_display_status_'.$select_data[$i]['id'].'\',
        };
        api_ajax_update_display(postData);  
                ">
                    <span class="label label-danger">
                    <i class="fa fa-times"></i> Disabled</span>
                </a>
            </div>
        ';


    echo '
        <tr id="'.$select_data[$i]['id'].'" class="api_table_link_company_branch" >
        <td style="min-width:30px; width: 30px; text-align: center; '.$temp_disabled_background.'">
            '.$temp7.'
        </td>
        <td align="center" style="'.$temp_disabled_background.'">
            '.$select_data[$i]['name'].'
        </td>           
        <td align="center">
            ';
            $data_view = array (
                'wrapper_class' => '',
                'file_path' => 'assets/uploads/thumbs/'.$select_data[$i]['image'],
                'max_width' => 100,
                'max_height' => 70,
                'product_link' => '',
                'image_class' => 'img-responsive',
                'image_id' => '',   
                'resize_type' => 'full',       
            );
            $temp_image = $this->site->api_get_image($data_view);      

            echo '
                <a href="'.site_url('assets/uploads/'.$select_data[$i]['image']).'" data-toggle="lightbox">
                    '.$temp_image['image'].'
                </a>
            ';  
        
            echo '
            
        </td> 
        <td align="center" style="'.$temp_disabled_background.'">
            '.date('Y-m-d', strtotime($select_data[$i]['start_date'])).'
            &nbsp;&nbsp;<span class="fa fa-long-arrow-right"></span>&nbsp;&nbsp;
            '.date('Y-m-d', strtotime($select_data[$i]['end_date'])).'
        </td>        
        <td align="center" style="'.$temp_disabled_background.'">
            '.$temp_2.'
        </td>
        <td align="center" style="'.$temp_disabled_background.'">
            '.$temp_3.'
        </td>
        <td align="center" style="'.$temp_disabled_background.'">
            '.$select_data[$i]['min_qty'].'
        </td>                
        <td align="center" style="'.$temp_disabled_background.'">
            <input type="text" class="form-control" value="'.$select_data[$i]['ordering'].'" style="width:50px; text-align:center;" onchange="
            var postData = {
                \'table_name\' : \'sma_product_promotion\', 
                \'field_id\' : '.$select_data[$i]['id'].',                
                \'field_value\' : this.value,
                \'redirect\' : \''.base_url().'admin/products/promotion\',
            };
            api_ajax_update_ordering(postData);            
            "/>        
        </td>                 
        <td style="'.$temp_disabled_background.'">
            '.$temp_4.'
        </td>   
        <td align="center" style="'.$temp_disabled_background.'">
            '.$temp6.'
        </td>
        
        </tr>
    ';    
}
if (count($select_data) <= 0) {
    echo '
    <tr>
    <td colspan="'.count($row_headers).'">
        '.lang('no_record_found').'
    </td>
    </tr>
    ';
}
echo '
    </tbody>
';
//-view_data-======================================================================
?>                            
<tfoot class="dtFilter">
<tr class="active">
    <th style="min-width:30px; width: 30px; text-align: center;">
        <input class="checkbox checkft skip" type="checkbox" name="check" onclick="api_select_check_all(this,'val[]'); api_setValueCheckbox('val[]','check_value','<?php echo $api_list_view['form_name']; ?>','-')"/>
    </th>
    <?php
    for ($i=1;$i<count($row_headers) - 1;$i++) {
        echo '<th></th>';
    }
    ?>                           
    <th style="text-align: center">
        <?php echo lang('Actions'); ?>
    </th>
</tr>
</tfoot>
</table>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel-heading-left">
                                <p class="data-total">
                                    <?php echo $show.' ';?>
                                </p>
                            </div>   
                            <div class="panel-heading-right">
                                <p>
                                   <?php echo $this->pagination->create_links(); ?>
                                </p>
                            </div>  
                            <div class="panel-heading-clear-fix"></div>   
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" name="show_data" value="<?php echo $show_data;?>" id="show_data"/>
<input type="hidden" name="sort_by" value="<?php echo $sort_by;?>" id="sort_by"/>
<input type="hidden" name="sort_order" value="<?php echo $sort_order;?>" id="sort_order"/>
<?= form_close() ?>

<?php
echo admin_form_open($api_list_view['form_bulk_action'],'id="api_form_action" name="api_form_action" method="post"');
echo '
    <div class="api_display_none">
        <input type="text" name="check_value" value="" id="check_value"/>
        <input type="text" name="api_action" value="" id="api_action"/>
        <button type="button" onclick="document.api_form_action.submit();" value="ok">ok</button>
    </div>
';
echo form_close();
?>

<script>
    $(".pagination .active").html('<a href="javascript:void(0);"><?php echo $api_list_view_current_page; ?></a>');

    function formactionsubmit(sort_by, sort_order) {
        document.getElementById("sort_by").value = sort_by;
        document.getElementById("sort_order").value = sort_order;
        document.forms[0].submit();

    }    
function api_bulk_actions(action){
    $("#api_action").val(action);
    $("#api_form_action").submit();
} 
function bulk_actions_delete(){
    $("#api_action").val('delete');
    document.api_form_action.submit();
}     
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/api/plugin/list_view/stock_manager/script.js"></script>