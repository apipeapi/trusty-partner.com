<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('Add Products') . " (" . $select_data[0]['company'] . ")"; ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open("customers/favorite_items/" . $select_data[0]['id'], $attrib); ?>
        <div class="modal-body">            

            <div class="row">
                <div class="col-sm-12">

                    <?php
                    
                    echo '
                        <div class="form-group">
                            <label class="control-label" for="products_id">
                                '.lang("Products").'
                            </label>
                            <div class="controls"> 
                    ';
                                $tr2[''] = lang("None");
                                for($i=0;$i<count($explode_products_id);$i++) {
                                    if($explode_products_id[$i] > 0) {
                                        $condition =  $condition.' and id != '.$explode_products_id[$i];
                                    }
                                }
                                $config_data = array(
                                    'table_name' => 'sma_products',
                                    'select_table' => 'sma_products',
                                    'select_condition' => "id > 0 ".$condition." order by name asc",
                                );
                                $select_products = $this->site->api_select_data_v2($config_data);
                                for ($i=0;$i<count($select_products);$i++) {
                                    if ($select_products[$i]['temp_ignore'] != 1) {
                                        if ($select_products[$i]['name'] != '')
                                            $tr2[$select_products[$i]['id']] = $select_products[$i]['name'];
                                    }
                                }
                                echo form_dropdown('products_id', $tr2, '', 'data-placeholder="'.lang("None").'" id="products_id" class="form-control" required="required"');
                    echo '
                            </div>
                        </div>    
                    ';    
                    ?>


                </div>
            </div>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('add', lang('Add'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>

