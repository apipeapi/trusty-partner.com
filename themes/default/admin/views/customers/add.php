<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg" >
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_customer'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'add-customer-form');
        echo admin_form_open_multipart("customers/add", $attrib); ?> 
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label"
							   for="sale_person"><?php echo lang("sale_person"); ?></label>

						<div class="controls"> 
							<?php
                                //$sp[''] = lang('Select_a_sale_person');
								foreach ($sales_person as $sale_person) {
									$sp[$sale_person->id] = $sale_person->first_name;
								}
								echo form_dropdown('sale_person_id', $sp, 0, 'class="form-control tip select" id="sale_person_id" style="width:100%;" required="required"');
							?>
							
						</div>
					</div>
				</div>
			    <div class="col-md-6 api_display_none">
					<div class="form-group">
                        <label class="control-label" for="">
                            <?php echo lang("Customer_Group"); ?> *
                        </label>
                        <?php
                        foreach ($customer_groups as $customer_group) {
                            $temp_4 = '';
                            if ($customer_group->percent >= 0)
                                $temp_4 = '+';
                            $temp_3 = ' (Price: '.$temp_4.$customer_group->percent.'%)';                           
                            $cgs[$customer_group->id] = $customer_group->name.$temp_3;
                        }
                        echo form_dropdown('customer_group', $cgs, $Settings->customer_group, 'class="form-control select" id="customer_group" style="width:100%;" required="required"');
                        ?>
                    </div>
				</div>
                <div class="col-md-6">
                    <div class="form-group">
                    <label class="control-label" for="payment_category">
						<?php echo lang("payment_category"); ?></label>
                        <div class="controls"> 
							<?php
								$payment_cate = array(
                                    'cash on delivery' => lang('cash_on_delivery'), 
                                    'weekly' => lang('weekly'), 
                                    '2 weekly' => lang('2weekly'), 
                                    'monthly' => lang('monthly'),
                                    '2 month' => lang('2month'));
								echo form_dropdown('payment_category', $payment_cate, isset($this->Settings->payment_category)?$this->Settings->payment_category:0, 'class="form-control tip select" id="payment_category" style="width:100%;" required="required" onchange="payment_due_date(this.value);"');
                            ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <?=lang("paying_by", "paid_by_1");?>
                        <select name="add_ons_customer_paid_by" id="add_ons_customer_paid_by" class="form-control paid_by">
                            <?= $this->sma->paid_opts(); ?>
                        </select>
					</div>
                    <div class="form-group api_display_none" id="hide_payment_due_date">
                        <?= lang("Payment due date", "payment_due_date");?>
                        <?php 
                            if($customer->payment_due_date == '')
                                echo form_input('add_ons_payment_due_date', '', 'class="form-control datetime" autocomplete="off"');
                            else 
                                echo form_input('add_ons_payment_due_date', $this->sma->hrld($customer->payment_due_date), 'class="form-control datetime" autocomplete="off" ');
                        ?>
                    </div>  
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("warehouse", "warehouse"); ?>
						<?php
						$wh[''] = lang('select').' '.lang('warehouse');
						foreach ($warehouses as $warehouse) {
							$wh[$warehouse->id] = $warehouse->name;
						}
						echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : ''), 'id="warehouse" class="form-control select" style="width:100%;" ');
						?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="price_group"><?php echo $this->lang->line("price_group"); ?></label>
                        <?php
                        $pgs[''] = lang('select').' '.lang('price_group');
                        foreach ($price_groups as $price_group) {
                            $pgs[$price_group->id] = $price_group->name;
                        }
                        echo form_dropdown('price_group', $pgs, $Settings->price_group, 'class="form-control select" id="price_group" style="width:100%;"');
                        ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("vat_no", "vat_no"); ?>
                        <?php echo form_input('vat_no', '', 'class="form-control" id="vat_no" onchange="api_vat_no_change(this.value);"'); ?>
                    </div>
                </div>
                <div class="col-md-6 api_display_none">
                    <div class="form-group">
						<label class="control-label" for="add_ons_discount_product"><?php echo $this->lang->line("discount"); ?></label>
                        <?php
                        for ($i=0;$i<=100;$i++) {
                            $temp[$i] = $i.'%';
                            
                        }                        
                        echo form_dropdown('add_ons_discount_product', $temp, 0, 'class="form-control select" id="add_ons_discount_product" style="width:100%;"');
                        ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group company">
                        <?= lang("company", "company"); ?>
                        <?php echo form_input('company', '', 'class="form-control tip" id="company" data-bv-notempty="true"'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group company_kh">
                        <?= lang("company_kh", "company_kh"); ?>
                        <?php echo form_input('company_kh', '', 'class="form-control tip" id="company_kh" '); ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group person">
                        <?= lang("name", "name"); ?>
                        <?php echo form_input('name', '', 'class="form-control tip" id="name" data-bv-notempty="true"'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group person">
                        <?= lang("name_kh", "name_kh"); ?>
                        <?php echo form_input('name_kh', '', 'class="form-control tip" id="name_kh" '); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("address", "address"); ?>
                        <?php echo form_input('address', '', 'class="form-control" id="address" required="required"'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("address_kh", "address_kh"); ?>
                        <?php echo form_input('address_kh', '', 'class="form-control" id="address_kh" '); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("postal_code", "postal_code"); ?>
                        <?php echo form_input('postal_code', $customer->postal_code, 'class="form-control" id="postal_code"'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                    <?php
                        $temp_1 = set_radio('add_ons_vat_invoice_type', '', TRUE);
                        $temp_2 = set_radio('add_ons_vat_invoice_type', 'do');                                
                            echo '
                                <div class="form-group" id="add_ons_vat_invoice_type_wrapper">
                                    '.lang("VAT_Invoice_Type", "VAT_Invoice_Type").'
                                        <div class="">
                                            <div class="api_height_10"></div>
                                            <input type="radio" id="add_ons_vat_invoice_type" name="add_ons_vat_invoice_type" value="" '.$temp_1.' />
                                            SL
                                            <span class="api_padding_left_10">
                                                <input type="radio" id="add_ons_vat_invoice_type" name="add_ons_vat_invoice_type" value="do" '.$temp_2.' />
                                            </span>
                                            DO
                                        </div>
                                </div>                
                            ';
                    ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <?php
                        $temp_1 = set_radio('add_ons_sale_consignment_auto_insert', 'yes'); 
                        $temp_2 = set_radio('add_ons_sale_consignment_auto_insert', '', TRUE);
                        echo '
                        <div class="form-group col-md-8" style="margin-left: -15px;">
                            '.lang("Auto_Add_Sale_Consignment", "Auto_Add_Sale_Consignment").'
                                <div class="">
                                    <div class="api_height_10"></div>
                                    <input type="radio" id="add_ons_sale_consignment_auto_insert" name="add_ons_sale_consignment_auto_insert" value="yes" '.$temp_1.' />
                                    Yes
                                    <span class="api_padding_left_10">
                                        <input type="radio" id="add_ons_sale_consignment_auto_insert" name="add_ons_sale_consignment_auto_insert" value="" '.$temp_2.' />
                                    </span>
                                    No
                                </div>
                        </div>                
                        ';
                    ?>                        
                </div>
                <div class="col-md-6">
                    <?php
                        $temp_1 = set_radio('add_ons_closed', 'yes'); 
                        $temp_2 = set_radio('add_ons_closed', '', TRUE);

                        echo '
                        <div class="form-group">
                            '.lang("Closed", "Closed").'
                                <div class="">
                                    <div class="api_height_10"></div>
                                    <input type="radio" id="add_ons_closed" name="add_ons_closed" value="yes" '.$temp_1.' />
                                    Yes
                                    <span class="api_padding_left_10">
                                        <input type="radio" id="add_ons_closed" name="add_ons_closed" value="" '.$temp_2.' />
                                    </span>
                                    No
                                </div>
                        </div>                
                        ';
                    ?>
                </div>
                <div class="col-md-2">
                    
                </div>
            </div>
            
            

            <div class="row">
                <div class="col-md-12">
                <fieldset class="scheduler-border " style="width: 100%;background: white;">
                            <legend class="scheduler-border" style="font-size: 14px !important;">Customer Information</legend>     
                            <style>
                                .form_customer_information{
                                    padding-left: 0px;
                                    padding-right: 0px;
                                }
                            </style>                  
                                <div class="col-md-12 form_customer_information">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><?= lang("Facebook", "facebook");?></label> 
                                            <input type="text" class="form-control" name="add_ons_facebook" value="<?= $customer->facebook;?>" />
                                        </div>
                                        <div class="form-group">
                                            <label><?= lang("Telegram", "telegram");?></label> 
                                            <input type="text" class="form-control" name="add_ons_telegram" value="<?= $customer->telegram;?>" />
                                        </div> 
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><?= lang("Instagram", "instargram");?></label> 
                                            <input type="text" class="form-control" name="add_ons_instagram" value="<?= $customer->instagram;?>" />
                                        </div>   
                                        <div class="form-group">
                                            <label><?= lang("Google", "google");?></label> 
                                            <input type="text" class="form-control" name="add_ons_google" value="<?= $customer->google;?>" />
                                        </div> 
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?= lang("phone", "phone"); ?>
                                            <input type="tel" name="phone" class="form-control" required="required" id="phone"/>
                                        </div>
                                    </div>
                                        <div class="col-md-6">
                                        <div class="form-group">
                                            <?= lang("email_address", "email_address"); ?>
                                            <input type="email" name="email" class="form-control" required="required" id="email_address"/>
                                        </div>
                                    </div>
                                </div>
                    </fieldset> 
                </div>
            </div>


                        
                    <div class="form-group api_display_none">
                        <?= lang("City", "city"); ?>
                        <?php 
                            if($customer->city == '')
                                echo form_input('city', '-', 'class="form-control" id="city" required="required"'); 
                            else 
                                echo form_input('city', $customer->city, 'class="form-control" id="city" required="required"'); 
                        ?>
                    </div>
					<div class="form-group api_display_none">
                        <?= lang("city_kh", "city_kh"); ?>
                        <?php 
                            if($customer->city_kh == '')   
                                echo form_input('city_kh', '-', 'class="form-control" id="city_kh"'); 
                            else 
                                echo form_input('city_kh', $customer->city_kh, 'class="form-control" id="city_kh"'); 
                        ?>
                    </div>
                    <div class="form-group api_display_none">
                        <?= lang("state", "state"); ?>
                        <?php
                        if ($Settings->indian_gst) {
                            $states = $this->gst->getIndianStates(true);
                            echo form_dropdown('state', $states, '', 'class="form-control select" id="state" required="required"');
                        } else {
                            echo form_input('state', '', 'class="form-control" id="state"');
                        }
                        ?>
                    </div>
					<div class="form-group api_display_none">
                        <?= lang("state_kh", "state_kh"); ?>
                        <?php echo form_input('state_kh', '', 'class="form-control" id="state_kh" '); ?>
                    </div>

                <!-- </div>  -->
                
                <div class="col-md-6">
                    <div class="form-group api_display_none">
                        <?= lang('award_points', 'award_points'); ?>
                        <?= form_input('award_points', set_value('award_points', $customer->award_points), 'class="form-control tip" id="award_points" '); ?>
                    </div> 
                    <fieldset class="scheduler-border api_display_none" style="width: 100%;background: white;">
                            <legend class="scheduler-border" style="font-size: 14px !important;">Customer Information</legend>     
                            <style>
                                .form_customer_information{
                                    padding-left: 0px;
                                    padding-right: 0px;
                                }
                            </style>                  
                                <div class="col-md-12 form_customer_information">
                                    <div class="form-group">
                                        <label><?= lang("National ID", "national_id");?></label> 
                                        <input type="text" class="form-control" name="add_ons_national_id" value="<?= $customer->national_id;?>" />
                                    </div>  
                                </div>
                                <div class="col-md-12 form_customer_information">
                                    <div class="form-group">
                                        <label><?= lang("Passport ID", "passport_id");?></label> 
                                        <input type="text" class="form-control" name="add_ons_passport_id" value="<?= $customer->passport_id;?>" />
                                    </div>  
                                </div>
                                <div class="col-md-12 form_customer_information">
                                    <div class="form-group">
                                        <?= lang("Opening date", "opening_date"); ?>
                                        <?php 
                                            if($customer->opening_date == '')
                                                echo form_input('add_ons_opening_date', '', 'class="form-control datetime" autocomplete="off"');
                                            else 
                                                echo form_input('add_ons_opening_date', $this->sma->hrld($customer->opening_date), 'class="form-control datetime" autocomplete="off"');
                                        ?>
                                        
                                    </div>  
                                </div>
                                <div class="col-md-12 form_customer_information">
                                    <div class="form-group">
                                        <label><?= lang("Birthday", "birthday");?></label> 
                                        <?php 
                                            if($customer->birth_day == '')
                                                echo form_input('add_ons_birth_day', '', 'class="form-control datetime" autocomplete="off"');
                                            else 
                                                echo form_input('add_ons_birth_day', $this->sma->hrld($customer->birth_day),  'class="form-control datetime" autocomplete="off"');
                                        ?>
                                    </div>  
                                </div>               
                    </fieldset>
                    
                        <?php
                        /*
                            $temp_1 = set_radio('add_ons_favorite', 'yes'); 
                            $temp_2 = set_radio('add_ons_favorite', '', TRUE);

                            echo '
                            <div class="form-group">
                                '.lang("Favorite", "Favorite").'
                                    <div class="">
                                        <div class="api_height_10"></div>
                                        <input type="radio" id="add_ons_favorite" name="add_ons_favorite" value="yes" '.$temp_1.' />
                                        Yes
                                        <span class="api_padding_left_10">
                                            <input type="radio" id="add_ons_favorite" name="add_ons_favorite" value="" '.$temp_2.' />
                                        </span>
                                        No
                                    </div>
                            </div>                
                            ';
                        */
                        ?>

                    <div class="api_display_none">                        
                        <div class="form-group">
                            <?= lang("postal_code", "postal_code"); ?>
                            <?php echo form_input('postal_code', '', 'class="form-control" id="postal_code"'); ?>
                        </div>
                        <div class="form-group">
                            <?= lang("country", "country"); ?>
                            <?php echo form_input('country', '', 'class="form-control" id="country"'); ?>
                        </div> 
    					<div class="form-group">
                            <?= lang("country_kh", "country_kh"); ?>
                            <?php echo form_input('country_kh', '', 'class="form-control" id="country_kh"'); ?>
                        </div>                    
                        <div class="form-group">
                            <?= lang("ccf1", "cf1"); ?>
                            <?php echo form_input('cf1', '', 'class="form-control" id="cf1"'); ?>
                        </div>
                        <div class="form-group">
                            <?= lang("ccf2", "cf2"); ?>
                            <?php echo form_input('cf2', '', 'class="form-control" id="cf2"'); ?>

                        </div>
                        <div class="form-group">
                            <?= lang("ccf3", "cf3"); ?>
                            <?php echo form_input('cf3', '', 'class="form-control" id="cf3"'); ?>
                        </div>
                        <div class="form-group">
                            <?= lang("ccf4", "cf4"); ?>
                            <?php echo form_input('cf4', '', 'class="form-control" id="cf4"'); ?>

                        </div>
                        <div class="form-group">
                            <?= lang("ccf5", "cf5"); ?>
                            <?php echo form_input('cf5', '', 'class="form-control" id="cf5"'); ?>

                        </div>
                        <div class="form-group">
                            <?= lang("ccf6", "cf6"); ?>
                            <?php echo form_input('cf6', '', 'class="form-control" id="cf6"'); ?>
                        </div>
                    </div>
                </div>

         <!-- </div>   end row  -->
            

        </div>
        <div class="modal-footer">
            <?php echo form_submit('add_customer', lang('add_customer'), 'class="btn btn-primary"'); ?>
            <?php echo form_button('close', lang('Close'), 'class="btn btn-danger" data-dismiss="modal"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>


<script type="text/javascript">
    $(document).ready(function (e) {
        $('#add-customer-form').bootstrapValidator({
            feedbackIcons: {
                valid: 'fa fa-check',
                invalid: 'fa fa-times',
                validating: 'fa fa-refresh'
            }, excluded: [':disabled']
        });
        $('select.select').select2({minimumResultsForSearch: 7});
        fields = $('.modal-content').find('.form-control');
        $.each(fields, function () {
            var id = $(this).attr('id');
            var iname = $(this).attr('name');
            var iid = '#' + id;
            if (!!$(this).attr('data-bv-notempty') || !!$(this).attr('required')) {
                $("label[for='" + id + "']").append(' *');
                $(document).on('change', iid, function () {
                    $('form[data-toggle="validator"]').bootstrapValidator('revalidateField', iname);
                });
            }
        });
    });
    function api_vat_no_change(value){
        if (value != '')
            $("#add_ons_vat_invoice_type_wrapper").removeClass("api_display_none");
        else
            $("#add_ons_vat_invoice_type_wrapper").addClass("api_display_none");
    }

    var value = '';
    function payment_due_date(value) {
        if(value == 'cash on delivery')
            $("#hide_payment_due_date").hide();
        else 
            $("#hide_payment_due_date").show();  
    }
    payment_due_date(value);
</script>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
