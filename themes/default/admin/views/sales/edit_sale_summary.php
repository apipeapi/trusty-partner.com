<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php
    if ($select_data[0]['id'] != '') {
        $temp_1 = lang('Edit_Sale_Summary');
        $temp_2 = '/'.$select_data[0]['id'];
        $temp_3 = lang('Update');
        $temp_4 = '';
        $temp_5 = '';
    }
    else {
        $temp_1 = lang('Add_Sale_Summary');
        $temp_2 = '';
        $temp_3 = lang('Add');
        $temp_4 = '';
        $temp_5 = '';
    }
?>

<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo $temp_1; ?></h4>
        </div>
        <?php 
        $attrib = array('data-toggle' => 'validator', 'role' => 'form',  'id' => $form_name, 'name' => $form_name);
        echo admin_form_open_multipart("sales/edit_sale_summary".$temp_2, $attrib); ?>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label" for="customer">
            			<?php echo lang("Customer_VAT_Invoice_Type_DO"); ?></label>
                        <div class="controls"> 
            				<?php
                            $tr[''] = lang("Please_select_a_customer");
                            foreach ($company as $temp) {
                                if ($temp->company != '')
                                    $tr[$temp->id] = $temp->company.' &nbsp;&nbsp;&nbsp;&nbsp;('.$temp->name.')';
                            }
                            echo form_dropdown('customer', $tr, $select_data[0]['customer_id'], 'data-placeholder="'.lang("Please_select_a_customer").'" id="customer" class="form-control" required="required" onchange="api_temp_customer_change(this.value);"');                            
                            ?>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 api_display_none">
                    <div class="form-group">
                        <label class="control-label" for="add_ons_company_branch">
                        <?php echo lang("Company_Branch"); ?></label>
                        <div class="controls" id="company_branch_wrapper"> 
                            <?php
                            $tr2[''] = lang('Please_select_a_company_branch');
                            echo form_dropdown('add_ons_company_branch', $tr2, '', 'data-placeholder="'.lang('Please_select_company_branch').'" id="add_ons_company_branch" class="form-control" disabled="diabled" ');                            
                            ?>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <?= lang("date", "date"); ?>
                        <?php echo form_input('date', (isset($select_data[0]['date']) ? date('d/m/Y H:i', strtotime($select_data[0]['date'])) : date('d/m/Y H:i')), 'class="form-control input-tip datetime" id="date" autocomplete="off"'); ?>  
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <?= lang("start_date", "start_date"); ?>
                        <?php echo form_input('start_date', (isset($select_data[0]['start_date']) ? date('d/m/Y', strtotime($select_data[0]['start_date'])) : ''), 'class="form-control date" id="start_date" required="required" autocomplete="off"'); ?>
                        <?php echo form_input('temp_start_date', (isset($select_data[0]['start_date']) ? date('d/m/Y', strtotime($select_data[0]['start_date'])) : ''), 'class="form-control date api_display_none" autocomplete="off"'); ?>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <?= lang("end_date", "end_date"); ?>
                        <?php echo form_input('end_date', (isset($select_data[0]['end_date']) ? date('d/m/Y', strtotime($select_data[0]['end_date'])) : ''), 'class="form-control date" id="end_date" required="required" autocomplete="off"'); ?>
                        <?php echo form_input('temp_end_date', (isset($select_data[0]['end_date']) ? date('d/m/Y', strtotime($select_data[0]['end_date'])) : ''), 'class="form-control date api_display_none" autocomplete="off"'); ?>
                    </div>
                </div>
                
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label" for="khnmer_Currency">
            			<?php echo lang("Khmer_Currency_Rate"); ?></label>
                        <div class="controls"> 
            				<?php

                            $tr = array();
                            $tr[''] = lang('Please_select_khmer_currency_rate');
                            for ($i=0;$i<count($select_currency);$i++) {
                                $tr[$select_currency[$i]['id']] = date('Y-m-d', strtotime($select_currency[$i]['date'])).' &nbsp;&nbsp;&nbsp;&nbsp; '.number_format($select_currency[$i]['rate'],0).' KHR';

                            }
                            echo form_dropdown('add_ons_kh_currency_rate', $tr, $select_data[0]['kh_currency_rate'], 'data-placeholder="'.lang('Please_select_khmer_currency_rate').'" id="khnmer_Currency" class="form-control" required="required"');                            
                            ?>
                        </div>
                    </div>
                </div> 
                          
<?php
    if ($select_data[0]['rebate'] == 'yes' || $select_data[0]['rebate'] == '') {
        $temp_1 = set_radio('add_ons_rebate', 'yes', TRUE);
        $temp_2 = set_radio('add_ons_rebate', 'no');
    }
    else {
        $temp_1 = set_radio('add_ons_rebate', 'yes');
        $temp_2 = set_radio('add_ons_rebate', 'no',true);        
    }
    echo '
    <div class="form-group col-md-6">
        '.lang("Rebate", "add_ons_rebate").'
        <div class="">            
            <div class="api_height_10"></div>
            <input type="radio" id="add_ons_rebate" name="add_ons_rebate" value="yes" '.$temp_1.' />
            Yes
            <span class="api_padding_left_10">
                <input type="radio" id="add_ons_rebate" name="add_ons_rebate" value="no" '.$temp_2.' />
            </span>
            No
        </div>
    </div>                
    ';
?>

            </div>

        </div>
        <div class="modal-footer">
            <?php
                echo form_submit('Add', $temp_3, 'class="btn btn-primary"'); 
            ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>
<script>
function api_submit2() {
    if (document.getElementById('start_date').value != '' && document.getElementById('end_date').value != '' && document.getElementById('customer').value != '')    
        $('#edit_sale_summary').submit();       
    else
        $('#edit_sale_summary').submit();       
}

</script>

<script type="text/javascript">

function api_temp_customer_change(id){
    var postData = [];
    postData['id'] = id;    
    postData['company_branch'] = '<?php echo $select_data[0]['company_branch']; ?>';
    var result = $.ajax
    (
        {
            url: '<?php echo base_url(); ?>admin/sales/api_temp_customer_change?id=' + postData['id'] + '&company_branch=' + postData['company_branch'],
            type: 'GET',
            secureuri:false,
            dataType: 'html',
            data:postData,
            async: false,
            error: function (response, status, e)
            {
                alert(e);
            }
        }
    ).responseText;
//var myWindow = window.open("", "MsgWindow", "width=700, height=400");
//myWindow.document.write(result);     
    var array_data = String(result).split("api-ajax-request-multiple-result-split");
    var result_text = array_data[1];

    $('#company_branch_wrapper').html(result_text);
}
api_temp_customer_change($("#customer").val());
</script>

