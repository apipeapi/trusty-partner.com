<?php defined('BASEPATH') OR exit('No direct script access allowed');
$allow_discount = ($Owner || $Admin || $this->session->userdata('allow_discount') || $inv->order_discount_id);

if ($consignment_id != '') {
    $config_data = array(
        'consignment_id' => $consignment_id,
        'sale_id' => $inv->id,
    );
    $temp_consigment_qty = $this->site->api_get_consignment_quantity($config_data);
    //print_r($temp_consigment_qty);
}

?>
<script type="text/javascript">
    var consignment_id = '<?php echo $consignment_id; ?>';
    var consignmemt_qty = []; 

<?php
    echo '
        var consignmemt_qty = [
    ';
    for ($i=0;$i<count($temp_consigment_qty);$i++) {
        echo ' 
            {
                "product_id" : "'.$temp_consigment_qty[$i]['product_id'].'",
                "remain_qty" : "'.$temp_consigment_qty[$i]['remain_qty'].'",
                "remain_qty_edit" : "'.$temp_consigment_qty[$i]['remain_qty_edit'].'"
            }, 
        ';
    }
    echo '
        ];
    ';    
?>

    var count = 1, an = 1, product_variant = 0, DT = <?= $Settings->default_tax_rate ?>,
        product_tax = 0, invoice_tax = 0, total_discount = 0, total = 0, allow_discount = <?= $allow_discount ? 1 : 0; ?>,
        tax_rates = <?php echo json_encode($tax_rates); ?>;
    //var audio_success = new Audio('<?= $assets ?>sounds/sound2.mp3');
    //var audio_error = new Audio('<?= $assets ?>sounds/sound3.mp3');
    $(document).ready(function () {
        <?php if ($inv) { 
        
            $temp_note = str_replace('\r','\n',$this->sma->decode_html($inv->note));
            $temp_note = str_replace("'","\'",$this->sma->decode_html($temp_note));
            $temp_note = preg_replace( "/\r|\n/", "", $temp_note );
            $temp_staff_note = str_replace('\r','\n',$this->sma->decode_html($inv->staff_note));            
            $temp_staff_note = str_replace("'","\'",$this->sma->decode_html($inv->staff_note));           
            
        ?>
        localStorage.setItem('slcustomer', '<?= $inv->customer_id ?>');
        localStorage.setItem('slbiller', '<?= $inv->biller_id ?>');
        localStorage.setItem('slwarehouse', '<?= $inv->warehouse_id ?>');
        localStorage.setItem('slpayment_term', '<?= $inv->payment_term ?>');
        localStorage.setItem('slnote', '<?= $temp_note; ?>');
        localStorage.setItem('slinnote', '<?= $temp_staff_note; ?>');
        localStorage.setItem('sldiscount', '<?= $inv->order_discount_id ?>');
        localStorage.setItem('sltax2', '<?= $inv->order_tax_id ?>');
        localStorage.setItem('slshipping', '<?= $inv->shipping ?>');
        localStorage.setItem('slitems', JSON.stringify(<?= $inv_items; ?>));
        <?php } ?>
        $(document).on('change', '#slbiller', function (e) {
            localStorage.setItem('slbiller', $(this).val());
        });
        if (slbiller = localStorage.getItem('slbiller')) {
            $('#slbiller').val(slbiller);
        }
        $("#add_item").autocomplete({
            source: function (request, response) {
                if (!$('#slcustomer').val()) {
                    $('#add_item').val('').removeClass('ui-autocomplete-loading');
                    bootbox.alert('<?=lang('select_above');?>');
                    $('#add_item').focus();
                    return false;
                }
                $.ajax({
                    type: 'get',
                    url: '<?= admin_url('sales/suggestions'); ?>',
                    dataType: "json",
                    data: {
                        term: request.term,
                        warehouse_id: $("#slwarehouse").val(),
                        customer_id: $("#slcustomer").val(),
                        mode: '<?php echo $_GET['mode']; ?>',
                    },
                    success: function (data) {
                        $(this).removeClass('ui-autocomplete-loading');
                        response(data);                        
                    }
                });
            },
            minLength: 1,
            autoFocus: false,
            delay: 250,
            response: function (event, ui) {
                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).val('');
                }
                else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                    $(this).removeClass('ui-autocomplete-loading');
                }
                else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).val('');

                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {
                    var row = add_invoice_item(ui.item);
                    if (row)
                        $(this).val('');
                } else {
                    bootbox.alert('<?= lang('no_match_found') ?>');
                }
            }
        });

        $('#reset').click(function (e) {
            $(window).unbind('beforeunload');
        });
        $('#edit_sale').click(function () {
            $(window).unbind('beforeunload');
            $('form.edit-so-form').submit();
        });
    });
</script>


<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-plus"></i><?= lang('edit_sale'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <p class="introtext"><?php echo lang('enter_info'); ?></p>
                <?php
                $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'class' => 'edit-so-form');
                echo admin_form_open_multipart("sales/edit/" . $inv->id, $attrib);
                echo '
                    <input type="hidden" name="consignment_id" value="'.$consignment_id.'" />
                '; 

                if ($_GET['mode'] == 'sample')
                    $temp = 1;
                else
                    $temp = 0;
                echo '
                    <input type="hidden" name="sample" value="'.$temp.'" />
                ';                
                ?>


                <div class="row">
                    <div class="col-lg-12">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("date", "sldate"); ?>
                                    <?php echo form_input('date', (isset($_POST['date']) ? $_POST['date'] : $this->sma->hrld($inv->date)), 'class="form-control input-tip datetime" id="sldate" autocomplete="off"'); ?>
                                </div>
                            </div>
                            
                            <?php
                                /*
                                if ($inv->reference_no != '') {
                                    if ($inv->date > $inv->generate_date)
                                        $inv->generate_date = $inv->date;
                                    echo '
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                '.lang("Generate_Date", "Generate_Date").'
                                                '.form_input('generate_date', (isset($_POST['generate_date']) ? $_POST['generate_date'] : $this->sma->hrld($inv->generate_date)), 'class="form-control input-tip datetime" autocomplete="off"').'
                                            </div>
                                        </div>
                                    ';
                                }
                                */
                            ?>

                        <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("biller", "slbiller"); ?>
                                    <?php
                                    $bl[""] = "";
                                    foreach ($billers as $biller) {
                                        $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                    }
                                    echo form_dropdown('biller', $bl, (isset($_POST['biller']) ? $_POST['biller'] : $inv->biller_id), 'id="slbiller" data-placeholder="' . lang("select") . ' ' . lang("biller") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                    ?>
                                </div>
                            </div>
                        <?php } else {
                            $biller_input = array(
                                'type' => 'hidden',
                                'name' => 'biller',
                                'id' => 'slbiller',
                                'value' => $this->session->userdata('biller_id'),
                            );
                            echo form_input($biller_input);
                        } ?>
                        <?php if ($_GET['mode'] == 'sample') { ?>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("Sample_Sale_Person", "add_ons_sample_sale_person"); ?>
                                <?php
                                    $sp[''] = lang('Select_a_sale_person');
                                    foreach ($sales_person as $sale_person) {
                                        $sp[$sale_person->id] = $sale_person->first_name;
                                    }
                                    echo form_dropdown('add_ons_sample_sale_person', $sp, $inv->sample_sale_person, 'class="form-control tip select" id="add_ons_sample_sale_person" style="width:100%;" ');
                                ?>
                            </div>
                        </div>
                        <?php } ?>

                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <div class="panel panel-warning">
                                <div
                                    class="panel-heading"><?= lang('please_select_these_before_adding_product') ?></div>
                                <div class="panel-body" style="padding: 5px;">

                                    <?php if ($Owner || $Admin || !$this->session->userdata('warehouse_id')) { ?>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("warehouse", "slwarehouse"); ?>
                                                <?php
                                                $wh[''] = '';
                                                foreach ($warehouses as $warehouse) {
                                                    $wh[$warehouse->id] = $warehouse->name;
                                                }
                                                echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : $inv->warehouse_id), 'id="slwarehouse" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("warehouse") . '" required="required" style="width:100%;" ');
                                                ?>
                                            </div>
                                        </div>
                                    <?php } else {
                                        $warehouse_input = array(
                                            'type' => 'hidden',
                                            'name' => 'warehouse',
                                            'id' => 'slwarehouse',
                                            'value' => $this->session->userdata('warehouse_id'),
                                        );
                                        echo form_input($warehouse_input);
                                    } ?>

                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <?= lang("customer", "slcustomer"); ?>
                                            <div class="input-group">
                                                <?php
                                                echo form_input('customer', (isset($_POST['customer']) ? $_POST['customer'] : ""), 'id="slcustomer" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" required="required" class="form-control input-tip" style="width:100%;" 
                                                    onchange="
                                                        var postData = {
                                                            \'id\' : this.value,
                                                        };                                              
                                                        api_get_company_branch(postData);
                                                    "
                                                ');

                                                
                                                    if ($inv->cs_reference_no == '') {
                                                        echo '
                                                            <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                                                <a href="#" id="removeReadonly">
                                                                    <i class="fa fa-unlock" id="unLock"></i>
                                                                </a>
                                                            </div>
                                                        ';
                                                    }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
            <div class="col-md-12 api_padding_0 api_display_none">
			    <div class="col-md-4">
					<div class="form-group">
                        <label class="control-label" for="company_branch">
						<?php echo lang("company_branch"); ?></label>
                        <div class="controls" id="company_branch_wrapper"> 
							<?php
                                $temp = $this->site->api_select_some_fields_with_where("*
                                    "
                                    ,"sma_companies"
                                    ,"id = ".$inv->customer_id
                                    ,"arr"
                                );
                                $temp2 = $this->site->api_select_some_fields_with_where("id,
                                    getTranslate(translate,'en','".f_separate."','".v_separate."') as title,
                                    getTranslate(add_ons,'branch_number','".f_separate."','".v_separate."') as branch_number,
                                    getTranslate(add_ons,'address','".f_separate."','".v_separate."') as address
                                    "
                                    ,"sma_company_branch"
                                    ,"parent_id = '".$temp[0]['id']."' order by title asc"
                                    ,"arr"
                                );                                
                                for ($i=0;$i<count($temp2);$i++) {
                                    $temp_value = $temp2[$i]['id'].'<api>'.$temp2[$i]['branch_number'];
                                    $tr[$temp_value] = $temp2[$i]['title'];
                                }
                                if (count($temp2) <= 0)
                                    $tr[''] = lang("none");                                 
                                echo form_dropdown('company_branch', $tr, $inv->temp_company_branch_id.'<api>'.$inv->temp_company_branch_branch_number, 'data-placeholder="'.lang("none").'" class="form-control" id="company_branch" onchange="var temp = this.value.split(\'<api>\'); $(\'#branch_number\').val(temp[1]); $(\'#company_branch_id\').val(temp[0]);"');

                            ?>
                        </div>
                    </div>
				</div>                                    
			    <div class="col-md-4 api_display_none">
					<div class="form-group">
                        <label class="control-label" for="branch_number">
						<?php echo lang("branch_number"); ?></label>
                        <div class="controls"> 
							<?php
                                echo form_input('branch_number', $inv->temp_company_branch_branch_number, 'class="form-control input-tip" readonly="readonly" id="branch_number"');
                                $config_data = array(
                                    'type' => 'hidden',
                                    'name' => 'company_branch_id',
                                    'id' => 'company_branch_id',
                                    'value' => $inv->temp_company_branch_id,
                                );
                                echo form_input($config_data);                          
                            ?>
                        </div>
                    </div>
				</div>
			    <div class="col-md-4">
					<div class="form-group">
                        <label class="control-label" for="po_number">
						<?php echo lang("po_number"); ?></label>
                        <div class="controls"> 
							<?php
                                echo form_input('po_number', $inv->po_number, 'class="form-control input-tip" id="po_number"');
                            ?>
                        </div>
                    </div>
				</div>                                    
                                
			</div>                                                                        
                                </div>
                            </div>

                        </div>

                        <?php
                            if ($inv->cs_reference_no != '') $temp = 'display: none;'; else $temp = ''; 
                        ?>
                        <div class="col-md-12" id="sticker" style="<?= $temp; ?>">
                            <div class="well well-sm">
                                <div class="form-group" style="margin-bottom:0;">
                                    <div class="input-group wide-tip">
                                        <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                            <i class="fa fa-2x fa-barcode addIcon"></i></a></div>
                                        <?php echo form_input('add_item', '', 'class="form-control input-lg" id="add_item" placeholder="' . lang("add_product_to_order") . '"'); ?>
                                        <?php if ($Owner || $Admin || $GP['products-add']) { ?>
                                        <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                            <a href="#" id="addManually">
                                                <i class="fa fa-2x fa-plus-circle addIcon" id="addIcon"></i>
                                            </a>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="control-group table-group">
                                <label class="table-label"><?= lang("order_items"); ?> *</label>

                                <div class="controls table-controls">
                                    <table id="slTable"
                                           class="table items table-striped table-bordered table-condensed table-hover sortable_table">
                                        <thead>
                                        <tr>
<th class="col-md-3">
    <?= lang('product') . ' (' . lang('code') .' - '.lang('name') . ')'; ?>
</th>
<th class="col-md-2">
    <?= lang("Original Price"); ?>
</th>
<th class="col-md-2">
    <?= lang("Discounted Price"); ?>
</th>
<th class="col-md-1">
    <?= lang("quantity"); ?>
</th>
<th class="col-md-2">
    <?= lang("Discount"); ?>
</th>
<th class="col-md-2"><?= lang("subtotal"); ?> (<span
        class="currency"><?= $default_currency->code ?></span>)
</th>
<th style="width: 30px !important; text-align: center;"><i
        class="fa fa-trash-o"
        style="opacity:0.5; filter:alpha(opacity=50);"></i></th>
                                        </tr>
                                        </thead>
                                        <tbody></tbody>
                                        <tfoot></tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>

<?php

$b = 0;
if (is_int(strpos($inv->reference_no,"SL")))
    $b = 1;
else {
    $config_data = array(
        'type' => 'sale',
        'date' => $inv->date,
        'customer_id' => $inv->customer_id,
        'update' => 0,
    );
    $temp = $this->site->api_calculate_reference_no($config_data);
    if (is_int(strpos($temp['reference_no'],"SL")))
        $b = 1;

}

$temp_tax_rate = $this->site->api_select_some_fields_with_where("
*     
"
,"sma_tax_rates"
,"id > 0 order by id asc"
,"arr"
);      
if ($b == 1) {                  
    $tr3 = array();
    for ($i=0;$i<count($temp_tax_rate);$i++) {
        $tr3[$temp_tax_rate[$i]['id']] = $temp_tax_rate[$i]['name'];
    }
}
else {
    $tr3 = array();
    $tr3[1] = $temp_tax_rate[0]['name'];
}

    echo '
        <div class="col-md-4">
            <div class="form-group">
                '.lang("VAT", "VAT").'
    ';
    echo form_dropdown('order_tax_id', $tr3, (isset($_POST['order_tax_id']) ? $_POST['order_tax_id'] : $inv->order_tax_id), ' class="form-control"');
    echo '
            </div>
        </div>
    ';

?>

                        <?php if ($allow_discount) { ?>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("order_discount", "sldiscount"); ?>
                                <?php echo form_input('order_discount', '', 'class="form-control input-tip" id="sldiscount" '.($allow_discount ? '' : 'readonly="true"')); ?>
                            </div>
                        </div>
                        <?php } ?>

                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("shipping", "slshipping"); ?>
                                <?php echo form_input('shipping', '', 'class="form-control input-tip" id="slshipping"'); ?>

                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("delivery_date", "delivery_date"); ?>
                                <?php
                                    if ($inv->delivery_date != '') $temp = $this->sma->hrld($inv->delivery_date); else $temp = '';
                                    echo form_input('delivery_date', (isset($_POST['delivery_date']) ? $_POST['delivery_date'] : $temp), 'class="form-control input-tip datetime" autocomplete="off" id="delivery_date"'); 
                                ?>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("delivery_person", "delivery_person"); ?>
                                <?php
                                    $tr[""] = "";
                                    $i = 0;
                                    foreach ($delivery_person as $value) {
                                        $tr[$delivery_person[$i]['id']] = $delivery_person[$i]['title'];
                                        $i++;
                                    }
                                    echo form_dropdown('add_ons_delivery_person', $tr, $inv->delivery_person, 'id="add_ons_delivery_person" data-placeholder="'.lang("select").' '.lang("delivery_person").'" class="form-control input-tip select" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                        
                        <div class="col-sm-4">
                            <div class="form-group">
                                <?= lang("payment_term", "slpayment_term"); ?>
                                <?php echo form_input('payment_term', '', 'class="form-control tip" data-trigger="focus" data-placement="top" title="' . lang('payment_term_tip') . '" id="slpayment_term"'); ?>

                            </div>
                        </div>


                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("Language", "Language"); ?>
                                <?php
                                    $i = 0;
                                    $language_array = unserialize(multi_language);
                                    for ($i=0;$i<count($language_array);$i++) {       
                                        $tr_lang[$language_array[$i][0]] = $language_array[$i][2];            
                                    }                                    
                                    echo form_dropdown('add_ons_language', $tr_lang, $inv->language, ' class="form-control input-tip select" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                                                
                        <?php 
                            if ($inv->reference_no != '') {                                
                        ?>
                        <div class="col-sm-4 api_display_none">
                            <div class="form-group">
                                <?= lang("kh_currency_rate", "kh_currency_rate"); ?>
                                <?php echo form_input('kh_currency_rate', (isset($_POST['kh_currency_rate']) ? $_POST['kh_currency_rate'] : $inv->kh_currency_rate), 'class="form-control tip" data-trigger="focus" data-placement="top" id="kh_currency_rate"'); ?>

                            </div>
                        </div>
                        <?php } ?>

                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("document", "document") ?>
                                <input id="document" type="file" data-browse-label="<?= lang('browse'); ?>" name="document" data-show-upload="false"
                                       data-show-preview="false" class="form-control file">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("Cashier", "cashier") ?>
                                <?php
                                    $config_data = array(
                                        'table_name' => 'sma_cashier',
                                        'select_table' => 'sma_cashier',
                                        'field_name' => 'translate',
                                        'translate' => 'yes',
                                        'select_condition' => "id > 0",
                                    );
                                    $select_cashier_name = $this->site->api_select_data_v2($config_data);
                                    for($k=0;$k<count($select_cashier_name);$k++) {
                                        if ($select_cashier_name[$k]['title_en'] != '')
                                                $td[$select_cashier_name[$k]['id']] = $select_cashier_name[$k]['title_en'];
                                        $config_data = array(
                                            'table_name' => 'sma_sales',
                                            'select_table' => 'sma_sales',
                                            'select_condition' => "id = ".$inv->id,
                                        );
                                        $select_sales = $this->site->api_select_data_v2($config_data);
                                    }
                                    
                                        //echo form_dropdown('cashier', $td, $select_sales[0]['cashier'], 'class="form-control"');
                                        if ($select_sales[0]['cashier'] == '')
                                            echo form_dropdown('cashier',$td, 219, 'class="form-control"');
                                        else 
                                            echo form_dropdown('cashier', $td, $select_sales[0]['cashier'], 'class="form-control"');
                                ?>
                            </div>
                        </div>

                        <?= form_hidden('payment_status', $inv->payment_status); ?>
                        <div class="clearfix"></div>

                        <input type="hidden" name="total_items" value="" id="total_items" required="required"/>
                        
                        <div class="row" id="bt">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= lang("sale_note", "slnote"); ?>
                                        <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : $inv->note), 'class="form-control" id="slnote" style="margin-top: 10px; height: 100px;"'); ?>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= lang("staff_note", "slinnote"); ?>
                                        <?php echo form_textarea('staff_note', (isset($_POST['staff_note']) ? $_POST['staff_note'] : $inv->staff_note), 'class="form-control" id="slinnote" style="margin-top: 10px; height: 100px;"'); ?>

                                    </div>
                                </div>


                            </div>

                        </div>
                        <div class="col-md-12">
                            <div
                                class="form-group">
                                <?php echo form_submit('', lang("submit"), 'id="" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?>
                                <button type="button" class="btn btn-danger" id="reset"><?= lang('reset') ?></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="bottom-total" class="well well-sm" style="margin-bottom: 0;">
                    <table class="table table-bordered table-condensed totals" style="margin-bottom:0;">
                        <tr class="warning">
                            <td><?= lang('items') ?> <span class="totals_val pull-right" id="titems">0</span></td>
                            <td><?= lang('total') ?> <span class="totals_val pull-right" id="total">0.00</span></td>
                            <?php if ($allow_discount) { ?>
                            <td><?= lang('order_discount') ?> <span class="totals_val pull-right" id="tds">0.00</span></td>
                            <?php } ?>
                            <?php if ($Settings->tax2) { ?>
                                <td><?= lang('order_tax') ?> <span class="totals_val pull-right" id="ttax2">0.00</span></td>
                            <?php } ?>
                            <td><?= lang('shipping') ?> <span class="totals_val pull-right" id="tship">0.00</span></td>
                            <td><?= lang('grand_total') ?> <span class="totals_val pull-right" id="gtotal">0.00</span></td>
                        </tr>
                    </table>
                </div>

                <?php echo form_close(); ?>

            </div>

        </div>
    </div>
</div>

<div class="modal" id="prModal" tabindex="-1" role="dialog" aria-labelledby="prModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                            class="fa fa-2x">&times;</i></span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="prModalLabel"></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <form class="form-horizontal" role="form">
                    <?php if ($Settings->tax1) { ?>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"><?= lang('product_tax') ?></label>
                            <div class="col-sm-8">
                                <?php
                                $tr_tax[""] = "";
                                foreach ($tax_rates as $tax) {
                                    $tr_tax[$tax->id] = $tax->name;
                                }
                                echo form_dropdown('ptax', $tr_tax, "", 'id="ptax" class="form-control pos-input-tip" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if ($Settings->product_serial) { ?>
                        <div class="form-group">
                            <label for="pserial" class="col-sm-4 control-label"><?= lang('serial_no') ?></label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="pserial">
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="pquantity" class="col-sm-4 control-label"><?= lang('quantity') ?></label>

                        <div class="col-sm-8">
                        <?php
                            if ($consignment_id == '')
                                echo '
                                    <input type="text" class="form-control" id="pquantity">
                                ';
                            else
                                echo '
                                    <input type="text" class="form-control" id="pquantity" readonly="readonly">
                                ';
                        ?>                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="punit" class="col-sm-4 control-label"><?= lang('product_unit') ?></label>
                        <div class="col-sm-8">
                            <div id="punits-div"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="poption" class="col-sm-4 control-label"><?= lang('product_option') ?></label>
                        <div class="col-sm-8">
                            <div id="poptions-div"></div>
                        </div>
                    </div>
                    <?php if ($Settings->product_discount) { ?>
                        <div class="form-group">
                            <label for="pdiscount"
                                   class="col-sm-4 control-label"><?= lang('product_discount') ?></label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="pdiscount" <?= $allow_discount ? '' : 'readonly="true"'; ?>>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="pprice" class="col-sm-4 control-label"><?= lang('unit_price') ?></label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="pprice" <?= ($Owner || $Admin || $GP['edit_price']) ? '' : 'readonly'; ?>>
                        </div>
                    </div>
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th style="width:25%;"><?= lang('net_unit_price'); ?></th>
                            <th style="width:25%;"><span id="net_price"></span></th>
                            <th style="width:25%;"><?= lang('product_tax'); ?></th>
                            <th style="width:25%;"><span id="pro_tax"></span></th>
                        </tr>
                    </table>
                    <input type="hidden" id="punit_price" value=""/>
                    <input type="hidden" id="old_tax" value=""/>
                    <input type="hidden" id="old_qty" value=""/>
                    <input type="hidden" id="old_price" value=""/>
                    <input type="hidden" id="row_id" value=""/>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="editItem"><?= lang('submit') ?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="mModal" tabindex="-1" role="dialog" aria-labelledby="mModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                            class="fa fa-2x">&times;</i></span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="mModalLabel"><?= lang('add_product_manually') ?></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="mcode" class="col-sm-4 control-label"><?= lang('product_code') ?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="mcode">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mname" class="col-sm-4 control-label"><?= lang('product_name') ?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="mname">
                        </div>
                    </div>
                    <?php if ($Settings->tax1) { ?>
                        <div class="form-group">
                            <label for="mtax" class="col-sm-4 control-label"><?= lang('product_tax') ?> *</label>

                            <div class="col-sm-8">
                                <?php
                                $tr_tax[""] = "";
                                foreach ($tax_rates as $tax) {
                                    $tr_tax[$tax->id] = $tax->name;
                                }
                                echo form_dropdown('mtax', $tr_tax, "", 'id="mtax" class="form-control input-tip select" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="mquantity" class="col-sm-4 control-label"><?= lang('quantity') ?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="mquantity">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="munit" class="col-sm-4 control-label"><?= lang('unit') ?> *</label>

                        <div class="col-sm-8">
                            <?php
                            $uts[""] = "";
                            foreach ($units as $unit) {
                                $uts[$unit->id] = $unit->name;
                            }
                            echo form_dropdown('munit', $uts, "", 'id="munit" class="form-control input-tip select" style="width:100%;"');
                            ?>
                        </div>
                    </div>
                    <?php if ($Settings->product_serial) { ?>
                        <div class="form-group">
                            <label for="mserial" class="col-sm-4 control-label"><?= lang('product_serial') ?></label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="mserial">
                            </div>
                        </div>
                    <?php } ?>
                    <?php if ($Settings->product_discount) { ?>
                        <div class="form-group">
                            <label for="mdiscount" class="col-sm-4 control-label">
                                <?= lang('product_discount') ?>
                            </label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="mdiscount" <?= $allow_discount ? '' : 'readonly="true"'; ?>>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="mprice" class="col-sm-4 control-label"><?= lang('unit_price') ?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="mprice">
                        </div>
                    </div>
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th style="width:25%;"><?= lang('net_unit_price'); ?></th>
                            <th style="width:25%;"><span id="mnet_price"></span></th>
                            <th style="width:25%;"><?= lang('product_tax'); ?></th>
                            <th style="width:25%;"><span id="mpro_tax"></span></th>
                        </tr>
                    </table>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="addItemManually"><?= lang('submit') ?></button>
            </div>
        </div>
    </div>
</div>

<script>
    function api_get_company_branch(postData) {
        $('#branch_number').val('');
        $('#company_branch_id').val('');        
        var result = $.ajax
        (
        	{
        		url:site.base_url + "sales/getSelectCompanyBranch",
        		type: 'GET',
        		secureuri:false,
        		dataType: 'html',
        		data:postData,
        		async: false,
        		error: function (response, status, e)
        		{
        			alert(e);
        		}
        	}
        ).responseText;    
    	var array_data = String(result).split("api-ajax-request-multiple-result-split");
        $('#company_branch_wrapper').html(array_data[0]);        
        var temp = $('#company_branch').val().split('<api>'); $('#branch_number').val(temp[1]); $('#company_branch_id').val(temp[0]);        
    }
    var temp = $('#company_branch').val().split('<api>'); $('#branch_number').val(temp[1]); $('#company_branch_id').val(temp[0]);
        
</script>






