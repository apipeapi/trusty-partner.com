<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php 
    $temp_url = '';
    if ($_GET['page'] != '') 
        $temp_url .= '?page='.$_GET['page']; 
    else 
        $temp_url .= '?page=1'; 

    if ($per_page != '') $temp_url .= '&per_page='.$per_page;

    foreach ($_GET as $name => $value) {
        if ($name != 'page' && $name != 'per_page' && $name != 'mode')
            if ($value != '') $temp_url .= '&'.$name.'='.$value;
    }
    echo admin_form_open('sales/sale_summary'.$temp_url,'id="action-form" name="action-form" method="GET"');

?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-pencil-square-o"></i><?= lang('Sale_Summary'); ?></h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon fa fa-tasks tip" data-placement="left" title="<?= lang("actions") ?>"></i>
                    </a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?php echo base_url().'admin/sales/edit_sale_summary'; ?>" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-plus-circle"></i> <?=lang('Add_Tax_Invoice_Summary')?>
                            </a>
                        </li>    
                        <li>
                            <a href="<?php echo base_url().'admin/sales/export_no_vat_summary/vat'; ?>" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-file-excel-o"></i> <?=lang('Export_To_Vat_Summary')?>
                            </a>
                        </li>    
                        <li>
                            <a href="<?php echo base_url().'admin/sales/export_no_vat_summary'; ?>" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-file-excel-o"></i> <?=lang('Export_To_No_Vat_Summary')?>
                            </a>
                        </li>    
                        <li class="divider api_display_none"></li>
                        <li class="api_display_none">
                            <a href="#" class="bpo" title="<b><?= $this->lang->line("delete") ?></b>"
                                data-content="<p><?= lang('r_u_sure') ?></p><button type='button' class='btn btn-danger' onclick='bulk_actions_delete();' data-action='delete'><?= lang('i_m_sure') ?></a> <button class='btn bpo-close'><?= lang('no') ?></button>"
                                data-html="true" data-placement="left">
                                <i class="fa fa-trash-o"></i> <?= lang('delete') ?>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
            <?php                
    echo '
    <div style="background: #F9F9F9; margin: -20px -20px 20px -20px; padding: 10px; border-bottom: 1px solid #DBDEE0;">    
        <div class="api_float_right">
            <ul style="list-style: none; margin:0px;">
            <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">                    
                    <button type="button" class="btn btn-success" style="margin:5px 0px 5px 5px !important;">
                        <li class="fa fa-pencil-square-o"></li> '.lang('Change_Payment').'
                    </button>
                </a>

                <ul class="dropdown-menu tasks-menus" role="menu" aria-labelledby="dLabel" style="">                
                    <li> 
                        <a href="javascript:void(0);" onclick="api_bulk_actions(\'change_payment_status_to_pending\');">
                            <i class="fa fa-edit"></i>'.lang('to_pending').'
                        </a>
                    </li>

                    <li>
                        <a href="javascript:void(0);" onclick="api_bulk_actions(\'change_payment_status_to_paid\');">
                            <i class="fa fa-edit"></i>'.lang('to_paid').'
                        </a>    
                    </li>
                </ul>
            </li>
            </ul>    
        </div>    
        <div class="api_float_right">
            <a href="'.base_url().'admin/sales/edit_sale_summary" data-toggle="modal" data-target="#myModal">
                <button type="button" class="btn btn-info api_margin_0 ">
                    <li class="fa fa-plus-circle"></li> &nbsp;'.lang('Add_Tax_Invoice_Summary').'
                </button>
            </a>

            <a href="'.base_url().'admin/sales/export_no_vat_summary/vat" data-toggle="modal" data-target="#myModal">
                <button type="button" class="btn btn-info api_link_box_none" style="background-color:#8800a5 !important; border-color:#8800a5 !important; margin-left:5px;">
                    <li class="fa fa-file-excel-o"></li> &nbsp;'.lang('Export_To_Vat_Summary').'
                </button>
            </a>

            <a href="'.base_url().'admin/sales/export_no_vat_summary" data-toggle="modal" data-target="#myModal">    
            <button type="button" class="btn btn-info api_margin_5_im api_link_box_none" style="background-color:#004f93  !important; border-color:#004f93  !important" >
                <li class="fa fa-file-excel-o"></li> &nbsp;'.lang('Export_To_No_Vat_Summary').'
            </button>
            </a>

        </div>

    ';


    echo '
        <div class="api_clear_both"></div>
    </div>
    ';
?>


                <div class="table-responsive">
                        <div class="row">
                            <div class="sale-header">
                                <div class="col-md-12 api_padding_0">
                                    <?php
                                        echo '
                                            <div class="col-md-3 pull-right">
                                                <div class="form-group">
                                                '.lang("Search", "Search").'
                                                <input type="text" class="form-control" id="search" name="search" value="'.(isset($_GET['search']) ? $_GET['search'] : "").'" onkeydown="if (event.keyCode == 13) document.action-form.submit()" />
                                        ';
                                        echo '
                                                </div>
                                            </div>
                                        ';
                                    ?>
                                    <div class="col-md-3 pull-right">
                                		<div class="form-group">
                                            <label class="control-label" for="customer">
                                			<?php echo lang("Customer_VAT_Invoice_Type_DO"); ?></label>
                                            <div class="api_height_5"></div>
                                            <div class="controls"> 
                                				<?php
                                            $tr[''] = lang("Please_select_a_customer");
                                            foreach ($company as $temp) {
                                                if ($temp->company != '')
                                                    $tr[$temp->id] = $temp->company;
                                            }
                                            echo form_dropdown('customer_id', $tr, $_GET['customer_id'], 'data-placeholder="'.lang("Please_select_a_customer").'" class="form-control" ');                            
                                                ?>
                                            </div>
                                        </div>
                                    </div>                                    
                                    
                                </div>
         
                                <div class="col-md-12">
                                    <div class="form-group pull-right api_padding_left_10">
                                        <a href="<?php echo base_url().'admin/sales/sale_summary'; ?>">
                                            <input type="button" name="reset" value="reset" class="btn btn-danger">
                                        </a>
                                    </div>
                                    <div class="form-group pull-right">
                                        <input type="button" name="submit_form" onclick="api_form_submit('<?php echo 'admin/sales/sale_summary'.$temp_url; ?>')" value="Submit" class="btn btn-primary">
                                    </div>
                                </div>  
  


                                <div class="col-md-6 text-left">
                                    <div class="short-result">
                                        <label>
                                            Show
                                        </label>
                                        <select id="sel_id" name="per_page" onchange="api_form_submit('<?php echo 'admin/sales/sale_summary'.$temp_url; ?>')" style="width:80px;">
                                            <?php
                                                $per_page_arr = array(
                                                    '10' => '10',
                                                    '25' => '25',
                                                    '50' => '50',
                                                    '100' => '100',
                                                    '200' => '200',
                                                    'All' => $total_rows_sale
                                                );
                                            
                                                foreach($per_page_arr as $key =>$value){
                                                    $select = $value == $per_page?'selected':'';
                                                    echo '<option value="'.$value.'" '.$select.'>';
                                                            echo $key;
                                                    echo '</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                             </div>
                        </div>     
                    </div> 
                
                    <table cellpadding="0" cellspacing="0" border="0"
                           class="table table-bordered table-hover table-striped">
                        <thead>
<?php
    $row_headers = array(
        'checkbox'      => [
            'label' => '<input class="checkbox checkth skip" id="api_check_all" onclick="api_select_check_all(this,\'val[]\'); api_setValueCheckbox(\'val[]\',\'check_value\',\'action-form\',\'-\');" type="checkbox" name="check"/>', 
            'sort_fn' => false,
            'style' => 'min-width:30px; width: 30px; text-align: center;'
        ],
        'date'          => ['label' => lang("date"), 'style' => 'width:100px'],
        'start_date'          => ['label' => lang("Period"), 'style' => 'width:220px'],
        'reference_no'  => ['label' => lang("reference_no"), 'style' => 'width:150px'],
        'customer'  => ['label' => lang("customer"), 'style' => ''],
        'currency'      => ['label' => lang("Currency"), 'style' => 'width:100px', 'sort_fn' => false],
        'rebate'      => ['label' => lang("Rebate"), 'style' => 'width:100px'],
        'grand_total'      => ['label' => lang("Grand_Total"), 'style' => 'min-width:150px;', 'sort_fn' => false],
        'payment_status'      => ['label' => lang("Payment_Status"), 'style' => ''],
        'action'        => [
                            'label' => lang("actions"), 
                            'style' => 'width:80px; text-align:center;', 
                            'sort_fn' => false
                        ],
    );
?>

                        
                        <tr class="">
                                <?php foreach ($row_headers as $key => $row) :?>
                                    <?php
                                   
                                    $th_class="class='pointer'";
                                    $e_click = true;
                                    if(isset($row['sort_fn']) && $row['sort_fn'] == false) {
                                        $th_class="";
                                        $e_click = false;
                                    }
                                    $style = isset($row['style']) && $row['style'] != '' ? 'style="'.$row['style'].'"' : '';
                                    ?>
                                    <th <?= $style ?> >
                                        <?php if($e_click ) :?>
                                            <?php if ($sort_by == $key && $sort_order == "ASC") : ?>
                                                    <label class="font-normal" onclick="formactionsubmit('<?= $key ?>', 'DESC')" id="fa_sort"  aria-hidden="true">
                                                    <?php echo '<span class="pointer">'.$row['label'].'</span>'; ?>
                                                        <span class="fa fa-sort-down"></span>
                                                    </label>
                                            <?php elseif ($sort_by == $key and $sort_order=="DESC") : ?>	
                                                <label class="font-normal" onclick="formactionsubmit('<?= $key ?>', 'ASC')" id="fa_sort_up"  aria-hidden="true">
                                                    <?php echo '<span class="pointer">'.$row['label'].'</span>'; ?>
                                                    <span class="fa fa-sort-up"></span>
                                                </label>
                                            <?php else : ?>	
                                                <label class="font-normal" onclick="formactionsubmit('<?= $key ?>', 'DESC')" id="fa_sort_down" aria-hidden="true">
                                                    <?php echo '<span class="pointer">'.$row['label'].'</span>'; ?>
                                                    <span class="fa fa-sort"></span>
                                                </label>
                                            <?php endif; ?>	
                                        <?php else : ?>
                                            <label class="font-normal" aria-hidden="true">
                                                <?php echo $row['label']; ?>
                                            </label>
                                        <?php endif; ?>	
                                    </th>
                                <?php endforeach; ?>

                        </tr>
                        </thead>
                        <tbody>
<?php

for ($i=0;$i<count($select_data);$i++) {
    if ($select_data[$i]['rebate'] == '' || $select_data[$i]['rebate'] == 'yes')
        $temp_rebate = '
            <a href="javascript:void(0);" onclick="
    var postData = {
        \'ajax_file\' : \''.admin_url('sales/ajax_update_rebate').'\',
        \'field_id\' : \''.$select_data[$i]['id'].'\',
        \'element_id\' : \'api_rebate_'.$select_data[$i]['id'].'\',
    };
    ajax_update_rebate(postData);
            ">
                <span class="label label-success">
                <i class="fa fa-check"></i> Yes</span>
            </a>
        ';
    else
        $temp_rebate = '
            <a href="javascript:void(0);" onclick="
    var postData = {
        \'ajax_file\' : \''.admin_url('sales/ajax_update_rebate').'\',
        \'field_id\' : \''.$select_data[$i]['id'].'\',
        \'element_id\' : \'api_rebate_'.$select_data[$i]['id'].'\',
    };
    ajax_update_rebate(postData);
            ">
                <span class="label label-danger">
                <i class="fa fa-check"></i> No</span>
            </a>
        ';
    if ($select_data[$i]['kh_currency_rate'] > 0) {
        $temp = $this->site->api_select_some_fields_with_where("
            *     
            "
            ,"sma_currencies"
            ,"id = ".$select_data[$i]['kh_currency_rate']
            ,"arr"
        );
        $temp2 = $this->sales_model->api_calculate_sale_summary($select_data[$i]['id']);
    }

    if ($select_data[$i]['disabled'] == 1) {
        $temp7 = '';
        $temp_disabled_background = 'background-color:#dfdfdf !important';
        $temp3 = $this->sma->formatMoney(0);
        $temp4 = $this->sma->formatMoney(0);
        $temp5 = '';
        $temp6 = '';
        $temp_sales = '';
    }
    else {
        $temp7 = '
            <div class="text-center">
                <input class="checkbox multi-select skip" type="checkbox" name="val[]" value="'.$select_data[$i]['id'].'" id="api_list_check_'.$select_data[$i]['id'].'" onclick="api_setValueCheckbox(\'val[]\',\'check_value\',\'action-form\',\'-\');"/>
            </div>        
        ';
        $temp_disabled_background = '';
        if ($temp2['rebate'] <= 0)
            $temp3 = '
                <div id="api_rebate_'.$select_data[$i]['id'].'">
                    '.$temp_rebate.'
                </div>        
            ';
        else
            $temp3 = '
                <div id="api_rebate_'.$select_data[$i]['id'].'">
                    '.$temp_rebate.'
                </div>        
                <div class="api_height_5"></div>
                '.$this->sma->formatMoney($temp2['rebate']).'
            ';
        $temp4 = $this->sma->formatMoney($temp2['grand_total']);
        $temp5 = '';
        if ($select_data[$i]['payment_status'] == 'pending')
            $temp5 = '<span class="label label-warning">'.$select_data[$i]['payment_status'].'</span>';
        if ($select_data[$i]['payment_status'] == 'paid')
            $temp5 = '<span class="label label-success">'.$select_data[$i]['payment_status'].'</span>';
    
        $temp_sales = '';
        if ($temp2['sales'] != '')
            $temp_sales = '
                <br>
                <a class="api_link_box_none" href="'.base_url().'admin/sales?adjustment_sale_track='.$temp2['sales'].'" title="'.lang('View_Sales').'" target="_blank"> 
                    <span class="label label-info">S</span>
                </a>
            ';
        $temp6 = '
            <a class="tip api_margin_right_5" title="Export" href="'.admin_url('sales/export_sale_summary/'.$select_data[$i]['id']).'" ><i class="fa fa-file-excel-o"></i>
            </a>
            <a class="tip api_margin_right_5" title="Edit" href="'.admin_url('sales/edit_sale_summary/'.$select_data[$i]['id']).'" data-toggle="modal" data-target="#myModal"><i class="fa fa-edit"></i>
            </a>
            <a class="api_display_none" href="#" class="tip po" title="<b>'.$this->lang->line("delete").'</b>"
                data-content="<p>'.lang('r_u_sure').'</p> <a href=\''.admin_url('sales/sale_summary_delete/'.$select_data[$i]['id']).'\'><button type=\'button\' class=\'btn btn-danger\' data-action=\'delete\'>'.lang('i_m_sure').'</button></a> <button class=\'btn po-close\'>'.lang('no').'</button>"
                data-html="true" data-placement="left">
                <i class="fa fa-trash-o"></i>
            </a>        
        ';
    }

    
    echo '
        <tr id="'.$select_data[$i]['id'].'" class="api_table_link_company_branch" >
        <td style="min-width:30px; width: 30px; text-align: center; '.$temp_disabled_background.'">
            '.$temp7.'
        </td>
        <td align="center" style="'.$temp_disabled_background.'">
            '.date('Y-m-d', strtotime($select_data[$i]['date'])).'<br>
            '.date('H:i:s', strtotime($select_data[$i]['date'])).'
        </td>        
        <td align="center" style="'.$temp_disabled_background.'">
            '.date('Y-m-d', strtotime($select_data[$i]['start_date'])).' <li class="fa fa-long-arrow-right api_padding_left_10 api_padding_right_10"></li> '.date('Y-m-d', strtotime($select_data[$i]['end_date'])).' 
        </td>
        <td align="center" style="'.$temp_disabled_background.'">
            '.$select_data[$i]['reference_no'].'
        </td>
        <td style="'.$temp_disabled_background.'">
            '.$select_data[$i]['customer'].'
        </td>
        <td align="right" style="'.$temp_disabled_background.'">
            '.$this->site->api_number_format($temp[0]['rate'],0).' KHR
        </td>        
        <td align="center" style="'.$temp_disabled_background.'">
            '.$temp3.'
        </td>
        <td align="right" style="'.$temp_disabled_background.'">
            '.$temp4.'
        </td>
        <td align="center" style="'.$temp_disabled_background.'">
            '.$temp5.'
            '.$temp_sales.'
        </td>          
        <td align="center" style="'.$temp_disabled_background.'">
            '.$temp6.'
        </td>
        </tr>
    ';    
}
if (count($select_data) <= 0)
    echo '
    <tr>
    <td colspan="10">
        '.lang('no_record_found').'
    </td>
    </tr>
    ';
?>           



                        </tbody>
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th style="min-width:30px; width: 30px; text-align: center;">
                                <input class="checkbox checkft skip" type="checkbox" name="check" onclick="api_select_check_all(this,'val[]'); api_setValueCheckbox('val[]','check_value','action-form','-')"/>
                            </th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>                            
                            <th style="width:100px; text-align: center;"><?= lang("actions"); ?></th>
                        </tr>
                        </tfoot>
                    </table>
                    <div class="row">
                        <div class="col-md-6 text-left">
                            <div class="dataTables_info" id="EXPData_info">
                                    <?php echo $show;?>
                            </div>
                        </div>
                        <div class="col-md-6 text-right">
                            <div class="dataTables_paginate paging_bootstrap">
                               <?php echo $this->pagination->create_links(); //pagination links ?>
                            </div>
                        </div>
                    </div>

                    
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" name="show_data" value="<?php echo $show_data;?>" id="show_data"/>
<input type="hidden" name="sort_by" value="<?php echo $sort_by;?>" id="sort_by"/>
<input type="hidden" name="sort_order" value="<?php echo $sort_order;?>" id="sort_order"/>
<?= form_close() ?>

<?php
echo admin_form_open('sales/sale_summary_actions','id="api_form_action" name="api_form_action" method="post"');
echo '
    <div class="api_display_none">
            <input type="text" name="check_value" value="" id="check_value"/>
            <input type="text" name="api_action" value="" id="api_action"/>
            <button type="button" onclick="document.api_form_action.submit();" value="ok">ok</button>
    </div>
';
echo form_close();
?>

<script>
function formactionsubmit(sort_by, sort_order) {
    document.getElementById("sort_by").value = sort_by;
    document.getElementById("sort_order").value = sort_order;
    document.forms[0].submit();
}

function bulk_actions_delete(){
    $("#api_action").val('delete');
    document.api_form_action.submit();
}
function ajax_update_rebate(postData){
    var result = $.ajax
    (
        {
            url: postData['ajax_file'],
            type: 'GET',
            secureuri:false,
            dataType: 'html',
            data:postData,
            async: false,
            error: function (response, status, e)
            {
                alert(e);
            }
        }
    ).responseText;
    var array_data = String(result).split("api-ajax-request-multiple-result-split");
    var b = array_data[0];
    var result_text = array_data[1];
    $("#" + postData['element_id']).html(array_data[2]);
}

<?php
    $temp = $_GET['page'];
    if ($temp == '') $temp = 1;
    echo '
        $(".pagination .active").html(\'<a href="javascript:void(0);">'.$temp.'</a>\');
    ';
?>

function api_bulk_actions(action){
    $("#api_action").val(action);
    document.api_form_action.submit();
}
function api_form_submit(action){
        $("#action-form").attr('action', action);
        $('#form_action').val('');
        $("#action-form").submit();
    }      
</script>

