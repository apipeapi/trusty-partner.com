<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="window.print();">
                <i class="fa fa-print"></i> <?= lang('print'); ?>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?= lang('view_payments').' ('.lang('sale').' '.lang('reference').': '.$inv->reference_no.')'; ?></h4>
        </div>
        <div class="modal-body">
            <div class="table-responsive">
                <table id="CompTable" cellpadding="0" cellspacing="0" border="0"
                       class="table table-bordered table-hover table-striped">
                    <thead>
                    <tr>
                        <th style="width:30%;"><?= $this->lang->line("date"); ?></th>
                        <th style="width:30%;"><?= $this->lang->line("reference_no"); ?></th>
                        <th style="width:15%;"><?= $this->lang->line("amount"); ?></th>
                        <th style="width:15%;"><?= $this->lang->line("paid_by"); ?></th>
                        <th style="width:10%;"><?= $this->lang->line("actions"); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (count($payments) > 0) {
                        for ($i=0;$i<count($payments);$i++) {	 ?>
                            <tr class="row<?= $payments[$i]['id']; ?>">
                                <td><?= $this->sma->hrld($payments[$i]['date']); ?></td>
                                <td><?= $payments[$i]['reference_no']; ?></td>
                                <td><?= $this->sma->formatMoney($payments[$i]['amount']) . ' ' . (($payments[$i]['attachment']) ? '<a href="' . admin_url('welcome/download/' . $payments[$i]['attachment']) . '"><i class="fa fa-chain"></i></a>' : ''); ?></td>
                                <td class="text-center">
                                    <?php
                                        if ($payments[$i]['paid_by']=='aba_qr')
                                            echo lang('ABA QRcode');
                                        else if ($payments[$i]['paid_by'] == 'AR')
                                            echo lang('AR');                                  
                                        else
                                            echo '<span class="api_text_transform_capitalize">'.lang($payments[$i]['paid_by']).'</span>';

                                        if ($payments[$i]['paid_by'] == 'paypal' && $payments[$i]['transaction'] != '')
                                            echo '
                                                <div style="height:5px;"></div>
                                                <div class="label label-default" style="font-weight:normal; font-size:13px;">
                                                    '.$payments[$i]['transaction_id'].'
                                                </div>
                                            '; 
                                        if ($payments[$i]['paid_by'] == 'aba' || $payments[$i]['paid_by'] == 'acleda')
                                            echo '
                                                <div style="height:5px;"></div>
                                                <div class="label label-default" style="font-weight:normal; font-size:13px;">
                                                    '.$payments[$i]['transfer_reference_number'].'
                                                </div>
                                            ';
                                        if ($payments[$i]['paid_by'] == 'aba_qr')
                                            echo '
                                                <div style="height:5px;"></div>
                                                <div class="label label-default" style="font-weight:normal; font-size:13px;">
                                                    '.$payments[$i]['aba_qr_transfer_reference_number'].'
                                                </div>
                                            ';
                                        if ($payments[$i]['paid_by'] == 'pipay')
                                            echo '
                                                <div style="height:5px;"></div>
                                                <div class="label label-default" style="font-weight:normal; font-size:13px;">
                                                    '.$payments[$i]['pipay_transaction_id'].'
                                                </div>
                                            '; 
                                        if ($payments[$i]['paid_by'] == 'wing')
                                            echo '
                                                <div style="height:5px;"></div>
                                                <div class="label label-default" style="font-weight:normal; font-size:13px;">
                                                    '.$payments[$i]['wing_transaction_id'].'
                                                </div>
                                            '; 
                                        if ($payments[$i]['paid_by'] == 'payway')
                                            echo '
                                                <div style="height:5px;"></div>
                                                <div class="label label-default" style="font-weight:normal; font-size:13px;">
                                                    '.$payments[$i]['payway_transaction_id'].'
                                                </div>
                                            ';
                                        if ($payments[$i]['paid_by'] == 'aba_daiki')
                                            echo '
                                                <div style="height:5px;"></div>
                                                <div class="label label-default" style="font-weight:normal; font-size:13px;">
                                                    '.$payments[$i]['aba_daiki_transaction_id'].'
                                                </div>
                                            ';         
                                        if ($payments[$i]['paid_by'] == 'wing_daiki')
                                            echo '
                                                <div style="height:5px;"></div>
                                                <div class="label label-default" style="font-weight:normal; font-size:13px;">
                                                    '.$payments[$i]['wing_daiki_transaction_id'].'
                                                </div>
                                            ';          
                                        if ($payments[$i]['paid_by'] == 'acode_daiki')
                                            echo '
                                                <div style="height:5px;"></div>
                                                <div class="label label-default" style="font-weight:normal; font-size:13px;">
                                                    '.$payments[$i]['acode_daiki_transaction_id'].'
                                                </div>
                                            ';                                                                                                                            
                                    ?>
                                </td>
                                <td>
                                    <div class="text-center">
                                        <a href="<?= admin_url('sales/payment_note/' . $payments[$i]['id']) ?>"
                                           data-toggle="modal" data-target="#myModal2"><i class="fa fa-file-text-o"></i></a>
                                        <?php if ($payments[$i]['paid_by'] != 'gift_card') { ?>
                                            <a href="<?= admin_url('sales/email_payment/' . $payments[$i]['id']) ?>" class="email_payment"><i class="fa fa-envelope"></i></a>
                                            <a href="<?= admin_url('sales/edit_payment/' . $payments[$i]['id']) ?>"
                                               data-toggle="modal" data-target="#myModal2"><i
                                                    class="fa fa-edit"></i></a>
                                            <a href="#" class="po"
                                               title="<b><?= $this->lang->line("delete_payment") ?></b>"
                                               data-content="<p><?= lang('r_u_sure') ?></p><a class='btn btn-danger' id='<?= $payments[$i]['id'] ?>' href='<?= admin_url('sales/delete_payment/' . $payments[$i]['id']) ?>'><?= lang('i_m_sure') ?></a> <button class='btn po-close'><?= lang('no') ?></button>"
                                               rel="popover"><i class="fa fa-trash-o"></i></a>
                                        <?php } ?>
                                    </div>
                                </td>
                            </tr>
                        <?php }
                    } else {
                        echo "<tr><td colspan='5'>" . lang('no_data_available') . "</td></tr>";
                    } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" charset="UTF-8">
    $(document).ready(function () {
        $(document).on('click', '.po-delete', function () {
            var id = $(this).attr('id');
            $(this).closest('tr').remove();
        });
        $(document).on('click', '.email_payment', function (e) {
            e.preventDefault();
            var link = $(this).attr('href');
            $.get(link, function(data) {
                bootbox.alert(data.msg);
            });
            return false;
        });
    });
</script>
