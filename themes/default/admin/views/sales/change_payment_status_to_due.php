<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php
if (count($select_data) > 0)
    echo '
        <div class="modal-dialog modal-lg no-modal-header">
    ';
else
    echo '
        <div class="modal-dialog modal-sm no-modal-header">
    ';

?>
 
    <div class="modal-content">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <h4 class="modal-title"><?php echo lang('Update_Payment_Status_To_Due'); ?></h4>
        </div>   

        <div class="modal-body">
<?php

    if (count($select_data) > 0) {
        echo '
            <div class="api_font_size_18">
                '.lang('Some customers below cannot update the payment status To Due.<br>Because these customers will be pay by weekly or monthly.').'
            </div>
            <div class="api_clear_both api_height_15"></div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped print-table order-table">
                    <thead>
                    <tr>
                        <th>'.lang('Reference_No').'</th>
                        <th>'.lang("Customer_Name").'</th>
                        <th>'.lang("Payment_Category").'</th>
                    </tr>
                    </thead>
                    <tbody>
        ';
        for ($i=0;$i<count($select_data);$i++) {
            echo '
                <tr>
                <td class="api_color_red">
                    '.$select_data[$i]['reference_no'].'
                </td>
                <td>
                    '.$select_data[$i]['customer'].'
                </td>
                <td align="center">
                    '.$select_data[$i]['payment_category'].'
                </td>                
                </tr>
            ';

        }
        echo '

                    </tbody>
                </table>
            </div>
        ';
    }

echo '
<h2>Are you sure you want to update?</h2>    
';
?>
        </div>

<?php
    echo '
        <div class="modal-footer"> 
            <div class="api_float_right">
                <div class="btn btn-success" onclick="api_bulk_actions(\'change_payment_status_to_due\');">
                    '.lang("Yes_I'm_sure").'
                </div>
                <div class="btn btn-danger" data-dismiss="modal">
                    '.lang('No').'
                </div>
            </div>
        </div>
    ';
?>

    </div>
</div>




