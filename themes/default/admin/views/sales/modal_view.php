<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg no-modal-header">
    <div class="modal-content">
        <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <?php
            
            if ($inv->mode != 'sample') {
                // echo '
                //     <a href="javascript:void(0);" onclick="window.open(\''.base_url().'admin/sales/pdf/'.$inv->id.'?print=1\', \'_blank\'); window.location = \''.base_url().'admin/sales\'">          
                //         <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;">
                //             <i class="fa fa-print"></i> '.lang('Print_PDF').'
                //         </button>
                //     </a>
                // ';
                echo '
                    <a href="'.base_url().'admin/sales/pdf/'.$inv->id.'" >          
                        <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;">
                            <i class="fa fa-print"></i> '.lang('Print_PDF').'
                        </button>
                    </a>
                ';                
            }
            ?>
            <?php
                if ($logo) {
                    if ($inv->order_tax_id == 2) {
                        $temp2 = '<img src="'.base_url().'assets/uploads/logos/'.$biller->logo.'">';
                    } else {
                        $temp2 = '<img src="'.base_url().'assets/uploads/logos/delivery-logo.png">';
                    }
                    echo '
                        <div class="text-center" style="margin-bottom:20px;">
                            '.$temp2.'
                        </div>
                    ';
                }
            ?>
            <div class="well well-sm">
                <div class="row bold">
                    <div class="col-xs-6">
                    <p class="bold">
                        <?= lang("date"); ?>: 
                        <?php

                        /*
                        if ($inv->date > $inv->generate_date)
                            echo $this->sma->hrld($inv->date);
                        else
                            echo $this->sma->hrld($inv->generate_date);
                        */

                        echo $this->sma->hrld($inv->date);

                        ?><br>
                        <?= lang("ref"); ?>: <?= $inv->reference_no; ?><br>
                        <?php if (!empty($inv->return_sale_ref)) {
                            echo lang("return_ref").': '.$inv->return_sale_ref;
                            if ($inv->return_id) {
                                echo ' <a data-target="#myModal2" data-toggle="modal" href="'.admin_url('sales/modal_view/'.$inv->return_id).'"><i class="fa fa-external-link no-print"></i></a><br>';
                            } else {
                                echo '<br>';
                            }
                        } ?>
                        <?= lang("sale_status"); ?>: <?= lang($inv->sale_status); ?><br>
                        <?= lang("payment_status"); ?>: <?= lang($inv->payment_status); ?>
                    </p>
                    </div>
                    <div class="col-xs-6 text-right order_barcodes">
                        <?php
                            if ($inv->reference_no != '') {
                                echo '
                                <img src="'.admin_url('misc/barcode/'.$this->sma->base64url_encode($inv->reference_no).'/code128/74/0/1').'" alt="'.$inv->reference_no.'" class="bcimg" />
                            ';
                            }
                        ?>                      
                        
                        <?= $this->sma->qrcode('link', urlencode(admin_url('sales/view/' . $inv->id)), 2); ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="row">

                <?php if ($Settings->invoice_view == 1) { ?>
                    <div class="col-xs-12 text-center">
                        <h1><?= lang('tax_invoice'); ?></h1>
                    </div>
                <?php } ?>
				
				<div class="col-xs-6">
                    <?php echo $this->lang->line("from"); ?>:
                    <h2 style="margin-top:10px;"><?= $biller->company != '-' ? $biller->company : $biller->name; ?></h2>
                    <?= $biller->company ? "" : "Attn: " . $biller->name ?>

                    <?php
                    //echo $biller->address . "<br>" . $biller->city . " " . $biller->postal_code . " " . $biller->state . "<br>" . $biller->country;
                    $config_data = array(
                        'id' => $biller->city_id,
                        'table_name' => 'sma_city',
                        'field_name' => 'title_en',
                        'translate' => 'yes',
                        'seperate' => ', ',
                        'reverse' => '',
                    );
                    $temp_10 = $this->site->api_display_category($config_data);
                    echo $biller->address.$temp_10['display'].' '.$biller->postal_code.'<br>';
                    
                    echo "<p>";
                    if ($customer->vat_no && $customer->vat_invoice_type != 'do') {
                        if ($biller->vat_no != "-" && $biller->vat_no != "") {
                            echo "<br>" . lang("vat_no") . ": " . $biller->vat_no;
                        }
                    }
                    if ($biller->gst_no != "-" && $biller->gst_no != "") {
                        echo "<br>" . lang("gst_no") . ": " . $biller->gst_no;
                    }
                    if ($biller->cf1 != "-" && $biller->cf1 != "") {
                        echo "<br>" . lang("bcf1") . ": " . $biller->cf1;
                    }
                    if ($biller->cf2 != "-" && $biller->cf2 != "") {
                        echo "<br>" . lang("bcf2") . ": " . $biller->cf2;
                    }
                    if ($biller->cf3 != "-" && $biller->cf3 != "") {
                        echo "<br>" . lang("bcf3") . ": " . $biller->cf3;
                    }
                    if ($biller->cf4 != "-" && $biller->cf4 != "") {
                        echo "<br>" . lang("bcf4") . ": " . $biller->cf4;
                    }
                    if ($biller->cf5 != "-" && $biller->cf5 != "") {
                        echo "<br>" . lang("bcf5") . ": " . $biller->cf5;
                    }
                    if ($biller->cf6 != "-" && $biller->cf6 != "") {
                        echo "<br>" . lang("bcf6") . ": " . $biller->cf6;
                    }

                    echo "</p>";
                    // echo lang("tel") . " : " . $biller->phone . "<br>" . lang("email") . " : " . $biller->email;
                    ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $this->lang->line("to"); ?>:<br/>
                    <h2 style="margin-top:10px;">
                        <?php
                            echo $inv->customer;
                        ?>
                    </h2>

                    <?php
                        if ($inv->parent_name != '') {
                            echo '
                                '.lang('Parent_Company').' : '.$inv->parent_name.'<br>
                            ';
                        }
                    ?>

                    <?=
                        $customer->company && $customer->company != '-' ? "" : "Attn: " . $customer->name
                    ?>
                    <?php
                    if($inv->address_id == 0) {
                        echo $customer->address.'<br>'; 
                    } else { 
                        $config_data = array(
                            'table_name' => 'sma_addresses',
                            'select_table' => 'sma_addresses',
                            'translate' => '',
                            'description' => '',
                            'select_condition' => "id = ".$inv->address_id,
                        );
                        $temp_address = $this->api_helper->api_select_data_v2($config_data);
                        echo $temp_address[0]['delivery_address'];
                    }
                    ?>
                </div>

            </div>
            <div class="row" style="margin-bottom: 15px;"> 
                <div class="col-xs-6"><?php echo lang("tel") . " : " . $biller->phone . "<br>" . lang("email") . " : " . $biller->email; ?>
                </div>
                <div class="col-xs-6">
                    <?php 
                        if($inv->address_id == 0) {
                            echo lang("tel") . " : " . $customer->phone . "<br>" . lang("email") . " : " . $customer->email; 
                        } else {
                            echo '<div>'.lang("tel").' : '.$temp_address[0]['phone'].'</div>';
                            echo '<div>'.lang("email").' : '.$customer->email.'</div>';
                        }
                        ?>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped print-table order-table">

                    <thead>

                    <tr>
                        <th><?= lang("no."); ?></th>
                        <th><?= lang("description"); ?></th>
                        <th><?= lang("Original_Price"); ?></th>
                        <th><?= lang("Discounted_Price"); ?></th>
                        <th><?= lang("quantity"); ?></th>
                        <th><?= lang("Discount"); ?></th>
                        <th><?= lang("subtotal"); ?></th>
                    </tr>

                    </thead>

                    <tbody>

                    <?php $r = 1;
                    if ($rows) :
                        foreach ($rows as $row):
                        ?>
                            <tr>
                                <td style="text-align:center; width:40px; vertical-align:middle;"><?= $r; ?></td>
                                <td style="vertical-align:middle;">
                                    <?= $row->product_code.' - '.$row->product_name . ($row->variant ? ' (' . $row->variant . ')' : ''); ?>
                                    <?= $row->second_name ? '<br>' . $row->second_name : ''; ?>
                                    <?= $row->details ? '<br>' . $row->details : ''; ?>
                                    <?php
                                        if ($row->serial_no != '' && $row->serial_no != 'null') {
                                            echo '<br>'.$row->serial_no;
                                        }
                                    ?>
                                </td>
<td style="text-align:right; width:100px;">
    <?php
        if ($row->discount > 0)
            echo $this->sma->formatMoney($row->net_unit_price); 
        else
            echo $this->sma->formatMoney($row->unit_price); 
    ?>
</td>   
<td style="text-align:right; width:100px;">
<?php
        if ($row->discount > 0)
            echo $this->sma->formatMoney($row->unit_price); 
?>
</td>                              
<td style="width: 80px; text-align:center; vertical-align:middle;">
    <?= $this->sma->formatQuantity($row->quantity); ?>
</td>
<td style="text-align:right; width:150px; vertical-align:middle;">
    <?php
        $config_data = array(
            'table_name' => 'sma_sale_items',
            'select_table' => 'sma_sale_items',
            'translate' => '',
            'description' => '',
            'select_condition' => "id = ".$row->id,
        );
        $temp = $this->api_helper->api_select_data_v2($config_data);

        if (intval($temp[0]['discount_rate']) > 0) {
            echo $this->sma->formatMoney($row->discount).' ('.number_format($temp[0]['discount_rate'],2).'%)';
        }
    ?>
</td>                             
<td style="text-align:right; width:120px;">
    <?= $this->sma->formatMoney($row->subtotal); ?>
</td>
                            </tr>
                            <?php
                            $r++;
                        endforeach;
                    endif;
                    if ($return_rows) {
                        echo '<tr class="warning"><td colspan="100%" class="no-border"><strong>'.lang('returned_items').'</strong></td></tr>';
                        foreach ($return_rows as $row):
                        ?>
                            <tr class="warning">
                                <td style="text-align:center; width:40px; vertical-align:middle;"><?= $r; ?></td>
                                <td style="vertical-align:middle;">
                                    <?= $row->product_code.' - '.$row->product_name . ($row->variant ? ' (' . $row->variant . ')' : ''); ?>
                                    <?= $row->second_name ? '<br>' . $row->second_name : ''; ?>
                                    <?= $row->details ? '<br>' . $row->details : ''; ?>
                                    <?= $row->serial_no ? '<br>' . $row->serial_no : ''; ?>
                                </td>
                                <?php if ($Settings->indian_gst) { ?>
                                <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $row->hsn_code; ?></td>
                                <?php } ?>
                                <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $this->sma->formatQuantity($row->quantity).' '.$row->product_unit_code; ?></td>
                                <td style="text-align:right; width:100px;"><?= $this->sma->formatMoney($row->unit_price); ?></td>
                                <?php
                                if ($Settings->tax1 && $inv->product_tax > 0) {
                                    echo '<td style="width: 100px; text-align:right; vertical-align:middle;">' . ($row->item_tax != 0 ? '<small>('.($Settings->indian_gst ? $row->tax : $row->tax_code).')</small>' : '') . ' ' . $this->sma->formatMoney($row->item_tax) . '</td>';
                                }
                        if ($Settings->product_discount && $inv->product_discount != 0) {
                            echo '<td style="width: 100px; text-align:right; vertical-align:middle;">' . ($row->discount != 0 ? '<small>(' . $row->discount . ')</small> ' : '') . $this->sma->formatMoney($row->item_discount) . '</td>';
                        } ?>
                                <td style="text-align:right; width:120px;"><?= $this->sma->formatMoney($row->subtotal); ?></td>
                            </tr>
                            <?php
                            $r++;
                        endforeach;
                    }
                    ?>
                    </tbody>
                    <tfoot>
                    <?php
                    $col = 6;
                    if ($Settings->product_discount && $inv->product_discount != 0) {
                        $col++;
                    }
                    if ($Settings->tax1 && $inv->product_tax > 0) {
                        $col++;
                    }
                    if ($Settings->product_discount && $inv->product_discount != 0 && $Settings->tax1 && $inv->product_tax > 0) {
                        $tcol = $col - 2;
                    } elseif ($Settings->product_discount && $inv->product_discount != 0) {
                        $tcol = $col - 1;
                    } elseif ($Settings->tax1 && $inv->product_tax > 0) {
                        $tcol = $col - 1;
                    } else {
                        $tcol = $col;
                    }
                    ?>
                    <?php if ($inv->grand_total != $inv->total) { ?>
                        <tr>
                            <td colspan="<?= $tcol; ?>"
                                style="text-align:right; padding-right:10px;"><?= lang("total"); ?>
                                (<?= $default_currency->code; ?>)
                            </td>
                            <?php
                            if ($Settings->tax1 && $inv->product_tax > 0) {
                                echo '<td style="text-align:right;">' . $this->sma->formatMoney($return_sale ? ($inv->product_tax+$return_sale->product_tax) : $inv->product_tax) . '</td>';
                            }
                            if ($Settings->product_discount && $inv->product_discount != 0) {
                                echo '<td style="text-align:right;">' . $this->sma->formatMoney($return_sale ? ($inv->product_discount+$return_sale->product_discount) : $inv->product_discount) . '</td>';
                            }
                            ?>
                            <td style="text-align:right; padding-right:10px;"><?= $this->sma->formatMoney($return_sale ? (($inv->total + $inv->product_tax)+($return_sale->total + $return_sale->product_tax)) : ($inv->total + $inv->product_tax)); ?></td>
                        </tr>
                    <?php } ?>
                    <?php
                    if ($return_sale) {
                        echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("return_total") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($return_sale->grand_total) . '</td></tr>';
                    }
                    if ($inv->surcharge != 0) {
                        echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("return_surcharge") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($inv->surcharge) . '</td></tr>';
                    }
                    ?>

                    <?php if ($Settings->indian_gst) {
                        if ($inv->cgst > 0) {
                            $cgst = $return_sale ? $inv->cgst + $return_sale->cgst : $inv->cgst;
                            echo '<tr><td colspan="' . $col . '" class="text-right">' . lang('cgst') . ' (' . $default_currency->code . ')</td><td class="text-right">' . ($Settings->format_gst ? $this->sma->formatMoney($cgst) : $cgst) . '</td></tr>';
                        }
                        if ($inv->sgst > 0) {
                            $sgst = $return_sale ? $inv->sgst + $return_sale->sgst : $inv->sgst;
                            echo '<tr><td colspan="' . $col . '" class="text-right">' . lang('sgst') . ' (' . $default_currency->code . ')</td><td class="text-right">' . ($Settings->format_gst ? $this->sma->formatMoney($sgst) : $sgst) . '</td></tr>';
                        }
                        if ($inv->igst > 0) {
                            $igst = $return_sale ? $inv->igst + $return_sale->igst : $inv->igst;
                            echo '<tr><td colspan="' . $col . '" class="text-right">' . lang('igst') . ' (' . $default_currency->code . ')</td><td class="text-right">' . ($Settings->format_gst ? $this->sma->formatMoney($igst) : $igst) . '</td></tr>';
                        }
                    } ?>

                    <?php if ($inv->order_discount != 0) {
                        echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("order_discount") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">'.($inv->order_discount_id ? '<small>('.$inv->order_discount_id.')</small> ' : '') . $this->sma->formatMoney($return_sale ? ($inv->order_discount+$return_sale->order_discount) : $inv->order_discount) . '</td></tr>';
                    }
                    ?>
                    <?php 
                    // if ($inv->service_charge != 0) {
                    //     echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("Service Charge") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;"><small>(5%) </small> '.$this->sma->formatMoney($inv->service_charge).'</td></tr>';
                    // }
                    ?>
                    <?php if ($inv->shipping != 0) {
                        echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("shipping") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($inv->shipping) . '</td></tr>';
                    }
                    ?>

                    <?php if ($Settings->tax2 && $inv->order_tax != 0) {
                        $config_data = array(
                            'id' => $inv->order_tax_id
                        );
                        $temp = $this->site->api_display_tax_rate($config_data);
                        echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;">'.lang("Vat").$temp.' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;"><small>(10%)</small> ' . $this->sma->formatMoney($return_sale ? ($inv->order_tax+$return_sale->order_tax) : $inv->order_tax) . '</td></tr>';
                    }
                    ?>
                    <tr>
                        <td colspan="<?= $col; ?>"
                            style="text-align:right; font-weight:bold;"><?= lang("total_amount").' (include VAT 10%)'; ?>
                            (<?= $default_currency->code; ?>)
                        </td>
                        <td style="text-align:right; padding-right:10px; font-weight:bold;"><?= $this->sma->formatMoney($return_sale ? ($inv->grand_total+$return_sale->grand_total) : $inv->grand_total); ?></td>
                    </tr>
                    </tfoot>
                </table>
            </div>

            <?= $Settings->invoice_view > 0 ? $this->gst->summary($rows, $return_rows, ($return_sale ? $inv->product_tax+$return_sale->product_tax : $inv->product_tax)) : ''; ?>

            <div class="row">
                <div class="col-xs-12">
                    <?php
                        if ($inv->note || $inv->note != "") { ?>
                            <div class="well well-sm">
                                <p class="bold"><?= lang("note"); ?>:</p>
                                <div><?= $this->sma->decode_html($inv->note); ?></div>
                            </div>
                        <?php
                        }
                        if ($inv->staff_note || $inv->staff_note != "") { ?>
                            <div class="well well-sm staff_note">
                                <p class="bold"><?= lang("staff_note"); ?>:</p>
                                <div><?= $this->sma->decode_html($inv->staff_note); ?></div>
                            </div>
                        <?php } ?>
                </div>

                <?php if ($customer->award_points != 0 && $Settings->each_spent > 0) { ?>
                <div class="col-xs-5 pull-left">
                    <div class="well well-sm">
                        <?=
                        '<p>'.lang('this_sale').': '.floor(($inv->grand_total/$Settings->each_spent)*$Settings->ca_point)
                        .'<br>'.
                        lang('total').' '.lang('award_points').': '. $customer->award_points . '</p>';?>
                    </div>
                </div>
                <?php } ?>

                <div class="col-xs-5 pull-right">
                    <div class="well well-sm">
                        <p>
                            <?= lang("created_by"); ?>: <?= $inv->created_by ? $created_by->first_name . ' ' . $created_by->last_name : $customer->name; ?> <br>
                            <?= lang("date"); ?>: <?= $this->sma->hrld($inv->date); ?>
                        </p>
                        <?php if ($inv->updated_by) { ?>
                        <p>
                            <?= lang("updated_by"); ?>: <?= $updated_by->first_name . ' ' . $updated_by->last_name;; ?><br>
                            <?= lang("update_at"); ?>: <?= $this->sma->hrld($inv->updated_at); ?>
                        </p>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php if (!$Supplier || !$Customer) { ?>
                <div class="buttons">
                    <div class="btn-group btn-group-justified">
<?php
    if ($inv->mode != 'sample') {
        ?>
                        <div class="btn-group">
                            <a href="<?= admin_url('sales/add_payment/' . $inv->id) ?>" class="tip btn btn-success" title="<?= lang('add_payment') ?>" data-toggle="modal" data-target="#myModal2">
                                <i class="fa fa-dollar"></i>
                                <span class="hidden-sm hidden-xs"><?= lang('payment') ?></span>
                            </a>
                        </div>
                        <div class="btn-group">
                            <a href="<?= admin_url('sales/add_delivery/' . $inv->id) ?>" class="tip btn btn-info" title="<?= lang('Set_Delivery') ?>" data-toggle="modal" data-target="#myModal2">
                                <i class="fa fa-truck"></i>
                                <span class="hidden-sm hidden-xs"><?= lang('Set_Delivery') ?></span>
                            </a>
                        </div>
                        <div class="btn-group">
                            <a href="<?= admin_url('sales/return_sale_v2/' . $inv->id) ?>" class="tip btn btn-danger" title="<?= lang('return_sale') ?>">
                                <i class="fa fa-minus-circle"></i>
                                <span class="hidden-sm hidden-xs"><?= lang('return_sale') ?></span>
                            </a>
                        </div>
                        <?php if ($inv->attachment) { ?>
                            <div class="btn-group">
                                <a href="<?= admin_url('welcome/download/' . $inv->attachment) ?>" class="tip btn btn-primary" title="<?= lang('attachment') ?>">
                                    <i class="fa fa-chain"></i>
                                    <span class="hidden-sm hidden-xs"><?= lang('attachment') ?></span>
                                </a>
                            </div>
                        <?php } ?>
                        <div class="btn-group">
                            <a href="<?= admin_url('sales/email/' . $inv->id) ?>" data-toggle="modal" data-target="#myModal2" class="tip btn btn-primary" title="<?= lang('email') ?>">
                                <i class="fa fa-envelope-o"></i>
                                <span class="hidden-sm hidden-xs"><?= lang('email') ?></span>
                            </a>
                        </div>

<?php
            echo '
            <div class="btn-group">
                <a href="'.base_url().'admin/sales/pdf/'.$inv->id.'" class="tip btn btn-warning api_link_box_none" style="background-color:#8800a5 !important; border-color:#8800a5 !important;" title="'.lang('Print_PDF').'">          
                    <i class="fa fa-download"></i>
                    <span class="hidden-sm hidden-xs">'.lang('Print_PDF').'</span>
                </a>
            </div>
            '; ?>

                        <?php if (! $inv->sale_id) { ?>
                        <div class="btn-group">
                            <a href="<?= admin_url('sales/edit/' . $inv->id) ?>" class="tip btn btn-warning sledit" title="<?= lang('edit') ?>">
                                <i class="fa fa-edit"></i>
                                <span class="hidden-sm hidden-xs"><?= lang('edit') ?></span>
                            </a>
                        </div>
                        <div class="btn-group">
                            <a href="#" class="tip btn btn-danger bpo" title="<b><?= $this->lang->line("delete_sale") ?></b>"
                                data-content="<div style='width:150px;'><p><?= lang('r_u_sure') ?></p><a class='btn btn-danger' href='<?= admin_url('sales/delete/' . $inv->id) ?>'><?= lang('i_m_sure') ?></a> <button class='btn bpo-close'><?= lang('no') ?></button></div>"
                                data-html="true" data-placement="top">
                                <i class="fa fa-trash-o"></i>
                                <span class="hidden-sm hidden-xs"><?= lang('delete') ?></span>
                            </a>
                        </div>
<?php
    } ?>

                        <?php
    } ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready( function() {
        $('.tip').tooltip();
    });
</script>
