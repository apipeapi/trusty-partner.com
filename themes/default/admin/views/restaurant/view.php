<?php
include 'assets/api/component/api_cart_express/api_cart_express.php';
include 'themes/default/admin/views/restaurant/header.php';

$l = $this->api_shop_setting[0]['api_lang_key'];
$temp_page_name = 'restaurant';
echo '
<link href="'.base_url().'/assets/api/page/'.$temp_page_name.'/css/style.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
<link href="'.base_url().'/assets/api/page/'.$temp_page_name.'/css/style_1024.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
<link href="'.base_url().'/assets/api/page/'.$temp_page_name.'/css/style_768.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
<link href="'.base_url().'/assets/api/page/'.$temp_page_name.'/css/style_600.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
<link href="'.base_url().'/assets/api/page/'.$temp_page_name.'/css/style_320.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
<link href="'.base_url().'/assets/api/page/'.$temp_page_name.'/css/style_414.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
';

$api_cart_express = new api_cart_express;
$config_data = array();
$temp = $api_cart_express->css($config_data);
echo $temp['display'];

$config_data = array(
    'table_name' => 'sma_restaurant',
    'select_table' => 'sma_restaurant',
    'translate' => '',
    'select_condition' => "id > 0 order by ordering asc limit 1",
);
$temp = $this->site->api_select_data_v2($config_data);
if ($_GET['table_id'] == '') $_GET['table_id'] = $temp[0]['id'];

//-top_header-----------------------
    $config_data_function['table_id'] = $_GET['table_id'];
    $temps = $this->restaurant_model->display_top_header_menu_icon($config_data_function); 
    if ($this->api_helper->is_mobile() != 1) 
        $temp_display_top_header = $temps['display'];
    else
        $temp_display_top_header_mobile = $temps['display_mobile'];

//-top_header-----------------------

//-menu----------------------- 
    $api_cart_express = new api_cart_express;  
    $config_data = array(
        'table_id' =>  $_GET['table_id'], 
    );
    $config_data_2 = $api_cart_express->initial_data_restaurant($config_data);    
    $config_data_2['table_id'] = $_GET['table_id'];
    $temp = $api_cart_express->view_product_list($config_data_2);
    $temp_display_menu = $temp['display'];
//-menu-----------------------

//-total_footer-----------------------
    $config_data_function['table_id'] = $_GET['table_id'];
    $temp_footer = $this->restaurant_model->view_total_detail($config_data_function);
    if ($this->api_helper->is_mobile() != 1) 
        $temp_display_total_footer = $temp_footer['display'];
    else
        $temp_display_total_footer_mobile = $temp_footer['display_mobile'];

//-total_footer-----------------------

//-table_selection-----------------------
    $temp = $this->restaurant_model->view_table_selection($config_data_function);
    $temp_display_table_selection = $temp['display'];
//-table_selection-----------------------

//-layout_pc-----------------------
    if ($this->api_helper->is_mobile() != 1) {
        echo $temp_display_top_header;
        $temp_display = '
            <table class="hidden-xs" width="100%" border="0">
            <tr>
            <td valign="top" width="50%">
                <div class="api_temp_wrapper_menu">
                    '.$temp_display_menu.'              
                </div>
            </td>
            <td valign="top" width="50%" align="center">
                <div class="api_temp_wrapper_table">
                    '.$temp_display_table_selection.'
                </div>
            </td>
            </tr>
            <tr>
            <td valign="bottom" width="50%" class="api_temp_footer_total">
                <div class="api_temp_wrapper_total">
                    '.$temp_display_total_footer.'
                </div>
            </td>
            <td valign="bottom" width="50%" class="api_temp_status_1">
                <div class="api_temp_wrapper_status_1">
                    '.$temp_display_table_status.'
                </div>
            </td>
            </tr>    
            </table>
        ';
    }
//-layout_pc-----------------------

//-layout_mobile-----------------------
    if ($this->api_helper->is_mobile() == 1) {
        $temp_display = '
            '.$temp_display_top_header_mobile.'
            <div class="api_temp_wrapper_mobile">
                '.$temp_display_menu.'
            </div>
            <div class="api_temp_footer_total_mobile">
                '.$temp_display_total_footer_mobile.'
            </div>            
        ';
    }
//-layout_mobile-----------------------

$config_data = array(
    'wrapper_class' => 'api_page_restaurant',
    'custom_html' => '',
    'display' => $temp_display,
    'display_class' => $this->api_web[0]['panel_left_class'].' api_panel_left',
    'panel' => '',
    'panel_class' => $this->api_web[0]['panel_right_class'].' api_panel_right',
    'type' => '',
);    
$temp = $this->api_display->template_display($config_data);
echo $temp['display'];

echo '    
<script  src="'.base_url().'/assets/api/page/'.$temp_page_name.'/js/script.js?v='.$this->api_shop_setting[0]['version'].'"></script>
';

include 'themes/default/admin/views/restaurant/footer.php';






