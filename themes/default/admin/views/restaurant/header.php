<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
<?php 
    $l = $this->api_shop_setting[0]['api_lang_key'];
    $temp_page_name = 'restaurant';
    echo '
    <title>'.lang('Restaurent') . " | " . $Settings->site_name.'</title>
    <meta http-equiv="pragma" content="no-cache"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="shortcut icon" href="'.base_url().'assets/uploads/logos/'.$this->api_shop_setting[0]['icon'].'">
    <link rel="apple-touch-icon" href="'.base_url().'assets/uploads/logos/'.$this->api_shop_setting[0]['apple_touch_icon'].'">    
    <script type="text/javascript" src="'.$assets.'js/jquery-2.0.3.min.js"></script>   
    <link rel="stylesheet" href=" '.base_url().'assets/api/page/'.$temp_page_name.'/css/bootstrap.css" type="text/css"/>
    <link rel="stylesheet" href=" '.base_url().'assets/api/page/'.$temp_page_name.'/css/bootstrap-responsive.css" type="text/css"/>
    <link rel="stylesheet" href="'.$assets.'styles/theme.css" type="text/css"/>
    <link href="'.$assets.'styles/style.css" rel="stylesheet"/>
    <link href="https://camboinfo.com/api/css/public.css?v='.$this->api_shop_setting[0]['version'].'" rel="stylesheet">
';

?>
</head>
<body>
<div id="loader-wrapper">
    <div id="loader"></div>
</div>
