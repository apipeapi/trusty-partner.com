<?php
$temp_display .= '
    <div class="api_temp_status">
        <table>
            <tr>
                <td valign="middle">
                    <div class="api_temp_border_1"></div>
                </td>
                <td valign="middle">
                    <div class="api_temp_border_2">Ready</div>
                </td>
            </tr>
            <tr>
                <td valign="middle">
                    <div class="api_temp_border_3"></div>
                </td>
                <td valign="middle">
                    <div class="api_temp_border_4">Seated</div>
                </td>
            </tr>
            <tr>
                <td valign="middle">
                    <div class="api_temp_border_5"></div>
                </td>
                <td valign="middle">
                    <div class="api_temp_border_6">Ordered</div>
                </td>
            </tr>
            <tr>
                <td valign="middle">
                    <div class="api_temp_border_7"></div>
                </td>
                <td valign="middle">
                    <div class="api_temp_border_8">Served</div>
                </td>
            </tr>
            <tr>
                <td valign="middle">
                    <div class="api_temp_border_9"></div>
                </td>
                <td valign="middle">
                    <div class="api_temp_border_10">Bill Printed</div>
                </td>
            </tr>
            <tr>
                <td valign="middle">
                    <div class="api_temp_border_11"></div>
                </td>
                <td valign="middle">
                    <div class="api_temp_border_12">To Clean</div>
                </td>
            </tr>
            <tr>
                <td valign="middle">
                    <div class="api_temp_border_13"></div>
                </td>
                <td valign="middle"> 
                    <div class="api_temp_border_14">Reserved</div>
                </td>
            </tr>
            
        </table>
    </div>
';

$temp_display .= '
    <div  class="api_temp_status_1">
        <div class="api_temp_2 api_ready">
            <table>
                <tr>
                    <td valign="middle">
                        <div class="api_temp_border_1"></div>
                    </td>
                    <td valign="middle">
                        <div class="api_temp_border_2">Ready</div>
                    </td>
                </tr>
            </table> 
        </div>                
        <div class="api_temp_2 api_seated">
            <table>
                <tr>
                    <td valign="middle">
                        <div class="api_temp_border_3"></div>
                    </td>
                    <td valign="middle">
                        <div class="api_temp_border_4">Seated</div>
                    </td>
                </tr>
            </table> 
        </div>                
        <div class="api_temp_2 api_ordered">
            <table>
                <tr>
                    <td valign="middle">
                        <div class="api_temp_border_5"></div>
                    </td>
                    <td valign="middle">
                        <div class="api_temp_border_6">Ordered</div>
                    </td>
                </tr>
            </table> 
        </div>                
        <div class="api_temp_2 api_served">
            <table>
                <tr>
                    <td valign="middle">
                        <div class="api_temp_border_7"></div>
                    </td>
                    <td valign="middle">
                        <div class="api_temp_border_8">Served</div>
                    </td>
                </tr>
            </table> 
        </div>                
        <div class="api_temp_2 api_bill">
            <table>
                <tr>
                    <td valign="middle">
                        <div class="api_temp_border_9"></div>
                    </td>
                    <td valign="middle">
                        <div class="api_temp_border_10">Bill Printed</div>
                    </td>
                </tr>
            </table> 
        </div>                
        <div class="api_temp_2 api_to_clean">
            <table>
                <tr>
                    <td valign="middle">
                        <div class="api_temp_border_11"></div>
                    </td>
                    <td valign="middle">
                        <div class="api_temp_border_12">To Clean</div>
                    </td>
                </tr>
            </table> 
        </div>                
        <div class="api_temp_2 api_reserved">
            <table>
                <tr>
                    <td valign="middle">
                        <div class="api_temp_border_13"></div>
                    </td>
                    <td valign="middle"> 
                        <div class="api_temp_border_14">Reserved</div>
                    </td>
                </tr>
            </table> 
        </div>
        <div class="api_clear_both"></div>                
    </div>
';
?>




