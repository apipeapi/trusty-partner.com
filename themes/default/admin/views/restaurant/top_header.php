<?php
for ($k=0;$k<count($table_id);$k++) 
if ($table_id > 0) 
    $config_data = array(
        'table_name' => 'sma_restaurant',
        'select_table' => 'sma_restaurant',
        'translate' => '',
        'select_condition' => "id = ".$table_id,
);
$temp_display_table = $this->site->api_select_data_v2($config_data);

// $this->sma->print_arrays($temp_display_table);
$temp_display_top_header .= '
    <div class="col-md-12 api_temp_top_header api_header_kh">
        <table width="100%" border="0">
        <tr>
            <td>
                <div class="header-brand" href=""><span class="pos-logo-sm">'.lang('Restaurant').'</span></div>
            </td>
            <td valign="middle">
                <div class="header-btn api_restaurant_top_menu_icons">
                    <ul class="api_kh api_temp_btn pull-right">                
                        <li class="dropdown">
                            <a class="btn bgrey  pos-tip api_btn_view_menu" title="View Menu" data-placement="bottom" href="javascript:void(0);" onclick="
                                var postData = {
                                    \'table_id\' : \''.$temp_display_table[$k]['id'].'\',
                                    \'ajax_url\' : \''.base_url().'admin/restaurant/api_ajax_load_menu\',
                                };
                                api_ajax_load_menu(postData); 
                            ">
                                <i class="fa fa-list-alt" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a class="btn blightOrange pos-tip api_btn_view" title="View Detail" data-placement="bottom" href="javascript:void(0);" onclick="
                                var postData = {
                                    \'table_id\' : \''.$temp_display_table[$k]['id'].'\',
                                    \'ajax_url\' : \''.base_url().'admin/restaurant/api_ajax_load_view_detail\',
                                };
                                api_ajax_load_view_detail(postData); 
                            ">
                                <i class="fa fa-eye text-center" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a class="btn bblue pos-tip api_btn_payment" title="View Detail Payment" data-placement="bottom" href="javascript:void(0);" onclick="
                            var postData = {
                                \'table_id\' : \''.$temp_display_table[$k]['id'].'\',
                                \'ajax_url\' : \''.base_url().'admin/restaurant/api_ajax_load_payment_detail\',
                            };
                            api_ajax_load_payment_detail(postData); 
                        "> 
                                <i class="fa fa-money text-center" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a class="btn bblue pos-tip api_btn_menu" title="Menu" data-placement="bottom" href="javascript:void(0);" onclick="click_menu();"  data-original-title="Menu">
                                <i class="fa fa-list" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a class="btn bdarkGreen pos-tip api_btn_table" title="View Table" data-placement="bottom" ​​​href="javascript:void(0);" onclick="
                                var postData = {
                                    \'table_id\' : \''.$temp_display_table[$k]['id'].'\', 
                                    \'ajax_url\' : \''.base_url().'admin/restaurant/api_ajax_load_table\',
                                };
                                api_ajax_load_table(postData); 
                            ">
                                <i class="fa fa-table api_table" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a class="btn bpink pos-tip api_btn_dashboard" title="Dashboard" data-placement="bottom" href="'.base_url().'admin/welcome" data-original-title="Dashboard">
                                <i class="fa fa-dashboard"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                                <a class="btn bred tip pos-tip api_btn_logout" title="LogOut" href="'.base_url().'admin/logout">
                                    <i class="fa fa-sign-out"></i>
                                </a>
                        </li>
                    </ul>    
                </div>    
            </td>
        </tr>
        </table>
    </div>
    <div class="api_clear_both"></div>

';
$temp_display_top_header_mobile .= '
    <div class="api_header_kh">
        <table width="100%" border="0">
            <tr>
                <td valign="middle" height="100">
                    <div class="top_header_menu_icon" >
                        <a class="pull-right" href="'.base_url().'admin/logout">
                            <div class="api_temp_icon_top_menu_default">
                                <i class="fa fa-sign-out api_temp_sign_out"></i>
                            </div>    
                        </a>
                        <a  class="pull-right" title="View Table" data-placement="bottom" ​​​href="javascript:void(0);" onclick="
                            var postData = {
                                \'table_id\' : \''.$temp_display_table[$k]['id'].'\', 
                                \'ajax_url\' : \''.base_url().'admin/restaurant/api_ajax_load_table_mobile\',
                            };
                            api_ajax_load_table_mobile(postData); 
                        ">
                            <div class="api_temp_icon_top_menu_default">
                                <i class="fa fa-table api_tepm_table" aria-hidden="true"></i>
                            </div>    
                        </a>
                        <a class="pull-right" title="View Detail Payment" data-placement="bottom" href="javascript:void(0);" onclick="
                            var postData = {
                                \'table_id\' : \''.$temp_display_table[$k]['id'].'\',
                                \'ajax_url\' : \''.base_url().'admin/restaurant/api_ajax_load_payment_detail_mobile\',
                            };
                            api_ajax_load_payment_detail_mobile(postData); 
                        "> 
                            <div class="api_temp_icon_top_menu_default">
                                <i class="fa fa-money text-center api_temp_money" aria-hidden="true"></i>
                            </div>
                        </a>
                        <a class="pull-right" title="View Detail" data-placement="bottom" href="javascript:void(0);"     onclick="
                                var postData = {
                                    \'table_id\' : \''.$temp_display_table[$k]['id'].'\',
                                    \'ajax_url\' : \''.base_url().'admin/restaurant/api_ajax_load_view_detail_mobile\',
                                };
                                api_ajax_load_view_detail_mobile(postData); 
                            ">
                            <div class="api_temp_icon_top_menu_default">
                                <i class="fa fa-eye text-center api_temp_eye" aria-hidden="true"></i>
                            </div>
                        </a>
                        <a class="pull-right" title="View Menu"data-placement="bottom" href="javascript:void(0);" onclick="
                                var postData = {
                                    \'table_id\' : \''.$temp_display_table[$k]['id'].'\',
                                    \'ajax_url\' : \''.base_url().'admin/restaurant/api_ajax_load_menu_mobile\',
                                };
                                api_ajax_load_menu_mobile(postData); 
                            ">
                            <div class="api_temp_icon_top_menu_default">
                                <i class="fa fa-file-text-o api_temp_file_text" aria-hidden="true"></i>
                            </div>
                        </a>
                            <div class="api_temp_icon_top_menu_default">
                                <i class="fa fa-search api_temp_search" aria-hidden="true"></i>
                            </div> 
                            <div class="api_temp_icon_top_menu_default">
                                <i class="fa fa-arrow-left api_temp_search" aria-hidden="true"></i>
                            </div> 
                            <div class="api_top_table_name">
                                
                            </div>
                        <div class="api_clear_both"></div>   
                    </div>
                </td>
            </tr>
        </table>
    </div>
';
?>






