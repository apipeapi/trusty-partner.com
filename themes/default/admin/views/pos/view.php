<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php if ($modal) { ?>
<div class="modal-dialog no-modal-header" role="document"><div class="modal-content"><div class="modal-body">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
    <?php
} else {
    ?><!doctype html>
    <html>
    <head>
        <meta charset="utf-8">
        <title><?=$page_title . " " . lang("no") . " " . $inv->id;?></title>
        <base href="<?=base_url()?>"/>
        <meta http-equiv="cache-control" content="max-age=0"/>
        <meta http-equiv="cache-control" content="no-cache"/>
        <meta http-equiv="expires" content="0"/>
        <meta http-equiv="pragma" content="no-cache"/>
        <link rel="shortcut icon" href="<?=$assets?>images/icon.png"/>
        <link rel="stylesheet" href="<?=$assets?>styles/theme.css" type="text/css"/>
        <style type="text/css" media="all">
            body { color: #000; }
            #wrapper { max-width: 480px; margin: 0 auto; padding-top: 20px; }
            .btn { border-radius: 0; margin-bottom: 5px; }
            .bootbox .modal-footer { border-top: 0; text-align: center; }
            h3 { margin: 5px 0; }
            .order_barcodes img { float: none !important; margin-top: 5px; }
            @media print {
                .no-print { display: none; }
                #wrapper { max-width: 480px; width: 100%; min-width: 250px; margin: 0 auto; }
                .no-border { border: none !important; }
                .border-bottom { border-bottom: 1px solid #ddd !important; }
                table tfoot { display: table-row-group; }
            }
        </style>
    </head>

    <body>
        <?php
    } ?>
    <div id="wrapper">
        <div id="receiptData">
            <div class="no-print">
                <?php
               /* if ($message) {
                    ?>
                    <div class="alert alert-success">
                        <button data-dismiss="alert" class="close" type="button">×</button>
                        <?=is_array($message) ? print_r($message, true) : $message;?>
                    </div>
                    <?php
                }*/ ?>
            </div>
            <div id="receipt-data">
                <div class="text-center">
                    <?= !empty($biller->logo) ? '<img class="api_margin_left_20" src="'.base_url('assets/uploads/logos/'.$biller->logo).'" alt="">' : ''; ?>
                    
                    <img class="api_margin_left_20" src="<?=base_url()?>assets/uploads/logos/logo.jpg" alt="">
                    <div style="margin-top: -10px">
                        <?php 
                            $config_data = array(
                                'table_name' => 'sma_companies',
                                'select_table' => 'sma_companies',
                                'translate' => '',
                                'select_condition' => "id = ".$inv->biller_id,
                            );
                            $temp = $this->site->api_select_data_v2($config_data);
                            for($i=0;$i<count($temp);$i++) {
                                echo '<br>'.$temp[$i]['address'];
                                $config_data = array(
                                    'id' => $temp[$i]['city_id'],
                                    'table_name' => 'sma_city',
                                    'field_name' => 'title_en',
                                    'translate' => 'yes',
                                    'seperate' => ', ',
                                    'reverse' => '',
                                );
                                $temp_10 = $this->site->api_display_category($config_data);
                                echo $biller->address.$temp_10['display'].' '.$biller->postal_code.'<br>';
                                echo 'Tel : '.$temp[$i]['phone'];
                            } 
                        ?>
                    </div>


                    <h3 style="text-transform:uppercase;"><?=$biller->company != '-' ? $biller->company : $biller->name;?></h3>
                    <?php
                    // echo "<p>" . $biller->address . " " . $biller->city . " " . $biller->postal_code . " " . $biller->state . " " . $biller->country;

                    // comment or remove these extra info if you don't need
                    if (!empty($biller->cf1) && $biller->cf1 != "-") {
                        echo "<br>" . lang("bcf1") . ": " . $biller->cf1;
                    }
                    if (!empty($biller->cf2) && $biller->cf2 != "-") {
                        echo "<br>" . lang("bcf2") . ": " . $biller->cf2;
                    }
                    if (!empty($biller->cf3) && $biller->cf3 != "-") {
                        echo "<br>" . lang("bcf3") . ": " . $biller->cf3;
                    }
                    if (!empty($biller->cf4) && $biller->cf4 != "-") {
                        echo "<br>" . lang("bcf4") . ": " . $biller->cf4;
                    }
                    if (!empty($biller->cf5) && $biller->cf5 != "-") {
                        echo "<br>" . lang("bcf5") . ": " . $biller->cf5;
                    }
                    if (!empty($biller->cf6) && $biller->cf6 != "-") {
                        echo "<br>" . lang("bcf6") . ": " . $biller->cf6;
                    }
                    // end of the customer fields

                    echo "<br><br>";
                    echo '</p>';
                    ?>
                </div>
                <?php
                if ($Settings->invoice_view == 1 || $Settings->indian_gst) {
                    ?>
                    <div class="col-sm-12 text-center">
                        <h4 style="font-weight:bold;"><?=lang('tax_invoice');?></h4>
                    </div>
                    <?php
                }
                echo '<table width="100%" style="margin-top: -40px;margin-left: 5px;">';
                    echo '<tbody>';
                    echo '<tr>';
                    echo '<td valign="top"><span style="font-weight:bold">'.lang("Sale No/Ref") . ' : </span><br>'.$inv->reference_no.'</td>';
                        // echo '<td valign="top"width="20%"><span style="font-weight:bold">'.lang("customer") .' </span><span style="font-weight:bold;">: </span><br>'.$inv->customer.'</td>'; 
                        echo '<td valign="top"width="20%"><span style="font-weight:bold">'.lang("customer") .' </span><span style="font-weight:bold;">: </span><br>';
                            if($inv->address_id == 0 || $inv->address_id == '') {
                                echo $inv->customer;
                            } else {
                                $config_data = array(
                                    'table_name' => 'sma_addresses',
                                    'select_table' => 'sma_addresses',
                                    'translate' => '',
                                    'select_condition' => "id = ".$inv->address_id ,
                                );
                                $select_address = $this->site->api_select_data_v2($config_data);
                                if($select_address[0]['first_name'] != '' || $select_address[0]['last_name'] != '') 
                                    echo $select_address[0]['first_name'].' '.$select_address[0]['last_name']; 
                                else  
                                    echo $inv->customer;
                            }
                        echo '</td>';
                    echo '</tr>';
                    echo '<tr>';
                    
                   
                    // $config_data = array(
                    //     'table_name' => 'sma_users',
                    //     'select_table' => 'sma_users',
                    //     'translate' => '',
                    //     'select_condition' => "id = ".$inv->created_by,
                    // );
                    // $temp_user = $this->site->api_select_data_v2($config_data);
                    // for($i=0;$i<count($temp_user);$i++) {
                    //     echo '<td style="width:23%" valign="top"><span style="font-weight:bold;">Cashier : </span><br>'.$temp_user[$i]['first_name'].'<span> '.$temp_user[$i]['last_name'].'</span></td>';
                    // }
                    // if ($inv->cashier <= 0)
                    //     $inv->cashier = 211;
                    if ($inv->cashier <= 0) {
                        echo '<td style="width:23%" valign="top"><span style="font-weight:bold;">Cashier :  </span><span>Admin</span></td>';
                        
                    } else {
                        $config_data = array(
                            'table_name' => 'sma_cashier',
                            'select_table' => 'sma_cashier',
                            'field_name' => 'translate',
                            'translate' => 'yes',
                            'select_condition' => "id = ".$inv->cashier,
                        );
                        $select_cashier = $this->site->api_select_data_v2($config_data);
                        for($i=0;$i<count($select_cashier);$i++) {
                            echo '<td style="width:23%" valign="top"><span style="font-weight:bold;">Cashier : </span><span>'.$select_cashier[$i]['title_en'].'</span></td>';
                        }
                    }
                    
                        $date = date_create($inv->date);
                        echo '<td valign="top"><span style="font-weight:bold">' .lang("date") . '</span> <span style="font-weight:bold"> :</span> <br>' .date_format($date, 'd/m/Y'). '</td>';
                    echo '</tr>';
                    echo '</tbody>';
                echo '</table>'; 
                ?>
                

                <div style="clear:both;"></div>
                <table class="table table-condensedss" style="margin-top: 7px;">
                    <thead style="border-top: 2px solid white;background: #f9f9f9;border-bottom:2px solid;">
                        <th style="border-bottom: 1px solid #D8D6D6;">Description</th>
                        <th class="text-right" style="border-bottom: 1px solid #D8D6D6;">Qty</th>
                        <th class="text-right" style="border-bottom: 1px solid #D8D6D6;">Price</th>
                        <th class="text-right" style="border-bottom: 1px solid #D8D6D6;">Disc</th>
                        <th class="text-right" style="border-bottom: 1px solid #D8D6D6;">Amount</th>
                    </thead>
                    <tbody>
                    <?php
                            $r = 1; $category = 0;
                            $tax_summary = array();
                            foreach ($rows as $row) {
                                if ($pos_settings->item_order == 1 && $category != $row->category_id) {
                                    $category = $row->category_id;
                                    echo '<tr><td colspan="100%" class="no-border"><strong>'.$row->category_name.'</strong></td></tr>';
                                }
                                $subtotal_v2 = ($row->unit_quantity * $row->real_unit_price) - $row->item_discount;
                                echo '
                                        <tr style="font-size:13px">
                                            
                                            <td class="no-border">#
                                                ' . $r . ': &nbsp;' .$row->product_name.'
                                            </td>
                                            <td class="no-border text-right ">' . $this->sma->formatQuantity($row->unit_quantity) . '</td>
                                            <td class="no-border text-right">
                                                '.$this->sma->formatMoney($row->real_unit_price).'
                                            </td>';
                                               
                                            $disc = ($row->item_discount * 100) / ($row->real_unit_price * $row->unit_quantity);
                                            
                                            // if ($row->discount <= 0)
                                            //     echo '<td class="no-border text-right">0%</td>';
                                            // else
                                            //     echo '<td class="no-border text-right">('.$row->discount.') '.$this->sma->api_convert_number($disc,2).'%</td>';
                                           
                                            if (($row->discount) == (floatval($row->discount).'%')) {
                                                echo '<td class="no-border text-right" >'.($row->discount).'</td>';
                                            } else {
                                                if ($row->discount !=0) {
                                                    echo '<td class="no-border text-right" >'.$this->sma->api_convert_number($disc,2).'%<br>('.$this->sma->formatMoney($row->discount).')</td>';
                                                } else 
                                                     echo '<td class="no-border text-right">0%</td>';
                                            }
                                    
                                            echo '<td class="no-border text-right">
                                                ' . $this->sma->formatMoney($subtotal_v2) . '
                                            </td>

                                            
                                        </tr>';
                                if (!empty($row->second_name)) {
                                    echo '<tr><td colspan="2" class="no-border">'.$row->second_name.'</td></tr>';
                                }
                                $r++;
                                $a = $subtotal_v2;
                                $total_before_discount += $a;
                            } 
                            
                            if ($return_rows) {
                                echo '<tr class="warning"><td colspan="100%" class="no-border"><strong>'.lang('returned_items').'</strong></td></tr>';
                                foreach ($return_rows as $row) {
                                    if ($pos_settings->item_order == 1 && $category != $row->category_id) {
                                        $category = $row->category_id;
                                        echo '<tr><td colspan="100%" class="no-border"><strong>'.$row->category_name.'</strong></td></tr>';
                                    }
                                    echo '<tr><td colspan="2" class="no-border">#' . $r . ': &nbsp;&nbsp;' . product_name($row->product_name, ($printer ? $printer->char_per_line : null)) . ($row->variant ? ' (' . $row->variant . ')' : '') . '<span class="pull-right">' . ($row->tax_code ? '*'.$row->tax_code : '') . '</span></td></tr>';
                                    echo '<tr><td class="no-border border-bottom">' . $this->sma->formatQuantity($row->unit_quantity) . ' x '.$this->sma->formatMoney($row->unit_price).($row->item_tax != 0 ? ' - '.lang('tax').' <small>('.($Settings->indian_gst ? $row->tax : $row->tax_code).')</small> '.$this->sma->formatMoney($row->item_tax).' ('.lang('hsn_code').': '.$row->hsn_code.')' : '').'</td><td class="no-border border-bottom text-right">' . $this->sma->formatMoney($row->subtotal) . '</td></tr>';
                                    // echo '<tr><td class="no-border border-bottom">' . $this->sma->formatQuantity($row->quantity) . ' x ';
                                    // if ($row->item_discount != 0) {
                                    //     echo '<del>' . $this->sma->formatMoney($row->net_unit_price + ($row->item_discount / $row->quantity) + ($row->item_tax / $row->quantity)) . '</del> ';
                                    // }
                                    // echo $this->sma->formatMoney($row->net_unit_price + ($row->item_tax / $row->quantity)) . '</td><td class="no-border border-bottom text-right">' . $this->sma->formatMoney($row->subtotal) . '</td></tr>';
                                    $r++;
                                }
                            }
                        ?>
                    </tbody>
                </table>
                    <table class="table table-condensed" style="margin-top: -20px;border-top: 2px solid;">
                    <tfoot>
                        <tr>
                            <th><?=lang("សរុប​ / Sub Total");?></th><th></th><th></th><th></th>
                            <!-- <th class="text-right"><?=$this->sma->formatMoney($return_sale ? (($inv->total + $inv->product_tax)+($return_sale->total + $return_sale->product_tax)) : ($inv->total + $inv->product_tax));?></th> -->
                            <?php 
                                $sub_total = ($inv->total_discount + $inv->grand_total) - $inv->shipping;
                            ?>
                            <th class="text-right"><?= $this->sma->formatMoney($inv->total);?></th>
                        </tr>
                        <?php 
                        
                            // if ($inv->product_discount !=0 ) {
                            //    echo ' <tr>
                            //                 <th>'.lang("Discount").'</th><th></th><th></th><th></th>
                            //                 <th class="text-right">'.$this->sma->formatMoney($inv->product_discount).'</th>
                            //         </tr>';
                            // }
                        ?>
                        <?php
                        if ($inv->service_charge != 0) {
                            echo '<tr><th>ថ្លៃ​សេវាកម្ម / ' . lang("Service Charge") . '</th><th></th><th></th><th></th><th class="text-right"><small>(5%)</small> ' . $this->sma->formatMoney($inv->service_charge) . '</th></tr>';
                        }

                        if ($inv->order_tax != 0) {
                            echo '<tr><th>ពន្ធ / ' . lang("tax") . '</th><th></th><th></th><th></th><th class="text-right"><small>(10%)</small> ' . $this->sma->formatMoney($return_sale ? ($inv->order_tax+$return_sale->order_tax) : $inv->order_tax) . '</th></tr>';
                        }

                        // if ($inv->total_discount != 0) {
                        //     $order_disc = (($this->sma->formatDecimal($inv->total_discount) * 100) / $sub_total);
                            
                        //     echo '<tr><th>' . lang("Discount") . '</th><th></th><th></th>
                        //     <th></th>
                        //     <th class="text-right">('.$this->sma->api_convert_number($order_disc,2).'%) ' . $this->sma->formatMoney($inv->order_discount).'</th></tr>';
                        // } else {
                        //     echo '<tr><th>' . lang("Discount") . '</th><th></th><th></th>
                        //     <th></th>
                        //     <th class="text-right">$0.00</th></tr>';
                        // }
                        echo '<tr>';
                        if ($inv->order_discount != 0) {
                            if (($inv->order_discount_id) == (floatval($inv->order_discount_id).'%')) {
                                echo '<th>' . lang("បញ្ចុះតម្លៃ / Discount") . '</th><th></th><th></th><th></th>
                                <th class="text-right"><span style="margin-left: -237px;">('.($inv->order_discount_id).') </span>' . $this->sma->formatMoney($inv->order_discount).'</th>';
                            } else {
                                $disc_percent = ($this->sma->formatDecimal($inv->order_discount) * 100) / $this->sma->formatDecimal($inv->total);
                                echo '<th>' . lang("បញ្ចុះតម្លៃ / Discount") . '</th><th></th><th></th><th></th>
                                <th class="text-right"><span style="margin-left: -237px;">('.$this->sma->api_convert_number($disc_percent,2).'%) </span>' . $this->sma->formatMoney($inv->order_discount).'</th>';
                            }
                        } 
                        
                        echo '</tr>';

                        if ($inv->shipping != 0) {
                            echo '<tr><th>' . lang("ដឹកជញ្ជូន / Shipping") . '</th><th></th><th></th><th></th><th class="text-right">' . $this->sma->formatMoney($inv->shipping) . '</th></tr>';
                        }

                        if ($return_sale) {
                            if ($return_sale->surcharge != 0) {
                                echo '<tr><th>' . lang("order_discount") . '</th><th></th><th></th><th></th><th class="text-right">' . $this->sma->formatMoney($return_sale->surcharge) . '</th></tr>';
                            }
                        }

                        if ($Settings->indian_gst) {
                            if ($inv->cgst > 0) {
                                $cgst = $return_sale ? $inv->cgst + $return_sale->cgst : $inv->cgst;
                                echo '<tr><td>' . lang('cgst') .'</td><td class="text-right">' . ( $Settings->format_gst ? $this->sma->formatMoney($cgst) : $cgst) . '</td></tr>';
                            }
                            if ($inv->sgst > 0) {
                                $sgst = $return_sale ? $inv->sgst + $return_sale->sgst : $inv->sgst;
                                echo '<tr><td>' . lang('sgst') .'</td><td class="text-right">' . ( $Settings->format_gst ? $this->sma->formatMoney($sgst) : $sgst) . '</td></tr>';
                            }
                            if ($inv->igst > 0) {
                                $igst = $return_sale ? $inv->igst + $return_sale->igst : $inv->igst;
                                echo '<tr><td>' . lang('igst') .'</td><td class="text-right">' . ( $Settings->format_gst ? $this->sma->formatMoney($igst) : $igst) . '</td></tr>';
                            }
                        }

                        if ($pos_settings->rounding || $inv->rounding != 0) {
                            ?>
                            <tr>
                                <th><?=lang("rounding");?></th>
                                <th class="text-right"><?= $this->sma->formatMoney($inv->rounding);?></th>
                            </tr>
                            <tr>
                                <th><?=lang("grand_total");?></th>
                                <th class="text-right"><?=$this->sma->formatMoney($return_sale ? (($inv->grand_total + $inv->rounding)+$return_sale->grand_total) : ($inv->grand_total + $inv->rounding));?></th>
                            </tr>
                            <?php
                        } else {
                            ?>
                            <tr>
                                <th><?=lang("សរុបរួម / Grand Total Dollar");?></th><th></th><th></th><th></th>
                                <!-- <th class="text-right"><?=$this->sma->formatMoney($return_sale ? ($inv->grand_total+$return_sale->grand_total) : $inv->grand_total);?></th> -->
                                <th class="text-right"><?=$this->sma->formatMoney($inv->grand_total);?></th>
                            </tr>
                            <tr>
                                <th><?=lang("សរុបរួម / Grand Total Riel");?></th><th></th><th></th><th></th>
                                <th class="text-right"><?=$this->sma->formatMoneyRiel($inv->grand_total * 4100);?></th>
                            </tr>
                            <?php
                        }
                        if ($inv->paid < ($inv->grand_total + $inv->rounding)) {
                            ?>
                            <tr>
                                <th><?=lang("ប្រាក់ដែលបានបង់ / Paid Amount");?></th></th><th></th><th></th><th></th>
                                <th class="text-right"><?=$this->sma->formatMoney($return_sale ? ($inv->paid+$return_sale->paid) : $inv->paid);?></th>
                            </tr>
                            <tr>
                                <th><?=lang("ប្រាក់ដែលខ្វះ / Due Amount");?></th></th><th></th><th></th><th></th>
                                <!-- <th class="text-right"><?=$this->sma->formatMoney(($return_sale ? (($inv->grand_total + $inv->rounding)+$return_sale->grand_total) : ($inv->grand_total + $inv->rounding)) - ($return_sale ? ($inv->paid+$return_sale->paid) : $inv->paid));?></th> -->
                                <th class="text-right"><?= $this->sma->formatMoney($inv->grand_total - ($return_sale ? ($inv->paid+$return_sale->paid) : $inv->paid));?></th>
                            </tr>
                            <?php
                        } ?>
                    </tfoot>
                </table>
                <?php
                if ($payments) {
                    echo '<table class="table table-striped table-condensed"><tbody>';
                        for($i=0;$i<count($payments);$i++) {
                            $config_data = array(
                                'id' => $payments[$i]->id,
                            );
                            $temp = $this->site->api_payment_method($config_data);
                            if ($payments[$i]->paid_by == 'cash') {
                                $config_data = array(
                                    'table_name' => 'sma_sales',
                                    'select_table' => 'sma_sales',
                                    'translate' => '',
                                    'select_condition' => "id = ".$inv->id." order by id desc ",
                                );
                                $temp_sale = $this->site->api_select_data_v2($config_data);
                                for ($i=0;$i<count($temp_sale);$i++) {
                                    $cash_recive = $temp_sale[$i]['pos_customer_pay_usd'] + ($temp_sale[$i]['pos_customer_pay_khr'] / 4100);
                                    echo '<tr>';
                                        echo '<th>'.lang("បង់ប្រាក់ដោយ / Paid by").' : '.$temp['label'].'</th>';
                                        echo '<td></td>';
                                    echo '</tr>';
                                        if ($temp_sale[$i]['pos_customer_pay_usd'] <= 0 && $temp_sale[$i]['pos_customer_pay_khr'] <= 0 ){
                                            echo '';
                                        } else {
                                            echo '<tr style="font-weight: bold;background:#f9f9f9">';
                                                echo '<th>'.lang('ប្រាក់ដែលបានទទួល / Cash Received').'</th>';
                                                echo '<td class="text-right">'.$this->sma->formatMoney($cash_recive).'</td>'; 
                                            echo '</tr>'; 
                                        }
        
                                        if ($temp_sale[$i]['pos_customer_change_usd'] <= 0) {
                                            echo '';
                                        } else {
                                                echo '<tr style="background:#f9f9f9;font-weight: bold;">';
                                                    echo '<th>'.lang('ប្រាក់អាប់ជាដុល្លា​រ / Change as Dollar').'</th>';
                                                    echo '<td class="text-right">'.$this->sma->formatMoney($temp_sale[$i]['pos_customer_change_usd']).'</td>'; 
                                                echo '</tr>';
                                        } if ($temp_sale[$i]['pos_customer_change_khr'] <= 0)
                                            echo '';
                                        else {
                                            if ($temp_sale[$i]['pos_customer_change_usd'] <= 0) {
                                                echo '<tr style="background:#f9f9f9;font-weight: bold;">';
                                                    echo '<th>'.lang('ប្រាក់អាប់ជារៀល /​ Change as Riel').'</th>';
                                                    echo '<td class="text-right">R '.($temp_sale[$i]['pos_customer_change_khr']).'</td>'; 
                                                echo '</tr>';
                                            }  else {
                                                echo '<tr style="background:#f9f9f9;font-weight: bold;">';
                                                    echo '<th>'.lang('ប្រាក់អាប់ជារៀល / Change as Riel').'</th>';
                                                    echo '<td class="text-right">R '.($temp_sale[$i]['pos_customer_change_khr']).'</td>'; 
                                                echo '</tr>';
                                            }
                                        }
                                }       
                            } else {
                                if ($payments[$i]->paid_by == 'CC') {
                                    echo '<tr>';
                                        echo '<th>'.lang("បង់ប្រាក់ដោយ / Paid by").' : '.$temp['label'].'</th>';
                                        echo '<th>No : xxxx xxxx xxxx '.substr($temp['transaction_id'],4);echo '</th>';
                                        echo '<th class="text-right">Name : '.$temp['name'].'</th>';
                                    echo '</tr>';
                                        echo '<tr style="background: #f9f9f9">';
                                        echo '<th>'.lang("ទឹកប្រាក់ / Amount").' : </th>';
                                        echo '<td></td>';
                                        echo '<th class="text-right">'.$this->sma->formatMoney($temp['amount']).'</th>';
                                    echo '<tr>';
                                    echo '<tr style="background: #f9f9f9; font-weight: bold;">';
                                        echo '<th>'.lang('ប្រាក់ដែលបានទទួល / Cash Received').'</th>';
                                        echo '<th></th>';
                                        echo '<td class="text-right">'.$this->sma->formatMoney($temp['amount']).'</td>'; 
                                    echo '</tr>'; 
                                } else {
                                    if ($temp['amount'] !=0) {
                                        echo '<tr style="font-weight: bold;">';
                                            echo '<td>'.lang("បង់ប្រាក់ដោយ / Paid by").' : <span >'.$temp['label'].'</span></td>';
                                            echo '<td class="text-right">'.$temp['text'].$temp['transaction_id'];echo '</td>';
                                        echo '</tr>';
                                        echo '<tr style="background: #f9f9f9; font-weight: bold;">';
                                            echo '<td>'.lang("ទឹកប្រាក់ / Amount").' : </td>';
                                            echo '<td class="text-right">'.$this->sma->formatMoney($temp['amount']).'</td>';
                                        echo '<tr>';
                                        echo '<tr style="background: #f9f9f9; font-weight: bold;">';
                                            echo '<th>'.lang('ប្រាក់ដែលបានទទួល / Cash Received').'</th>';
                                            echo '<td class="text-right">'.$this->sma->formatMoney($temp['amount']).'</td>'; 
                                        echo '</tr>'; 
                                    } else {
                                        echo '<tr style="font-weight: bold;">';
                                            echo '<td>'.lang("បង់ប្រាក់ដោយ / Paid by").' : <span>'.$temp['label'].'</span></td>';
                                            echo '<td class="text-right">'.$temp['text'].$temp['transaction_id'];echo '</td>';
                                        echo '</tr>';
                                    }
                                }
                                
                            }
                        }                    
                    echo '</tbody></table>';
                }

                if ($return_payments) {
                    echo '<strong>'.lang('return_payments').'</strong><table class="table table-striped table-condensed"><tbody>';
                    foreach ($return_payments as $payment) {
                        $payment->amount = (0-$payment->amount);
                        echo '<tr>';
                        if (($payment->paid_by == 'cash' || $payment->paid_by == 'deposit') && $payment->pos_paid) {
                            echo '<td>' . lang("paid_by") . ': ' . lang($payment->paid_by) . '</td>';
                            echo '<td colspan="2">' . lang("amount") . ': ' . $this->sma->formatMoney($payment->pos_paid == 0 ? $payment->amount : $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                            echo '<td>' . lang("change") . ': ' . ($payment->pos_balance > 0 ? $this->sma->formatMoney($payment->pos_balance) : 0) . '</td>';
                        } elseif (($payment->paid_by == 'CC' || $payment->paid_by == 'ppp' || $payment->paid_by == 'stripe') && $payment->cc_no) {
                            echo '<td>' . lang("paid_by") . ': ' . lang($payment->paid_by) . '</td>';
                            echo '<td>' . lang("amount") . ': ' . $this->sma->formatMoney($payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                            echo '<td>' . lang("no") . ': ' . 'xxxx xxxx xxxx ' . substr($payment->cc_no, -4) . '</td>';
                            echo '<td>' . lang("name") . ': ' . $payment->cc_holder . '</td>';
                        } elseif ($payment->paid_by == 'Cheque' && $payment->cheque_no) {
                            echo '<td>' . lang("paid_by") . ': ' . lang($payment->paid_by) . '</td>';
                            echo '<td colspan="2">' . lang("amount") . ': ' . $this->sma->formatMoney($payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                            echo '<td>' . lang("cheque_no") . ': ' . $payment->cheque_no . '</td>';
                        } elseif ($payment->paid_by == 'gift_card' && $payment->pos_paid) {
                            echo '<td>' . lang("paid_by") . ': ' . lang($payment->paid_by) . '</td>';
                            echo '<td>' . lang("no") . ': ' . $payment->cc_no . '</td>';
                            echo '<td>' . lang("amount") . ': ' . $this->sma->formatMoney($payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                            echo '<td>' . lang("balance") . ': ' . $this->sma->formatMoney($this->sma->getCardBalance($payment->cc_no)) . '</td>';
                        } elseif ($payment->paid_by == 'other' && $payment->amount) {
                            echo '<td colspan="2">' . lang("paid_by") . ': ' . lang($payment->paid_by) . '</td>';
                            echo '<td colspan="2">' . lang("amount") . ': ' . $this->sma->formatMoney($payment->pos_paid == 0 ? $payment->amount : $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                            echo $payment->note ? '</tr><td colspan="4">' . lang("payment_note") . ': ' . $payment->note . '</td>' : '';
                        }
                        echo '</tr>';
                    }
                    echo '</tbody></table>';
                }
                ?>

                <?= $Settings->invoice_view > 0 ? $this->gst->summary($rows, $return_rows, ($return_sale ? $inv->product_tax+$return_sale->product_tax : $inv->product_tax)) : ''; ?>
                <div class="text-center">Exchange Rate 1$ : R 4,100</div>
                <?= $customer->award_points != 0 && $Settings->each_spent > 0 ? '<p class="text-center">'.lang('this_sale').': '.floor(($inv->grand_total/$Settings->each_spent)*$Settings->ca_point)
                .'<br>'.
                lang('total').' '.lang('award_points').': '. $customer->award_points . '</p>' : ''; ?>
                <?= $inv->note ? '<p class="text-center">' . $this->sma->decode_html($inv->note) . '</p>' : ''; ?>
                <?= $inv->staff_note ? '<p class="no-print"><strong>' . lang('staff_note') . ':</strong> ' . $this->sma->decode_html($inv->staff_note) . '</p>' : ''; ?>
                <?= $biller->invoice_footer ? '<p class="text-center">'.$this->sma->decode_html($biller->invoice_footer).'</p>' : ''; ?>
            </div>
            
            <div class="order_barcodes text-center">
                Please scan and register <?php echo $this->Settings->site_name; ?> LINE@ account!!
                <br>
                <img src="<?php echo base_url().'assets/uploads/logos/line_logo.png'; ?>" width="40" class="bcimg " />
                <img src="<?php echo base_url().'assets/uploads/logos/line_qr_code.jpg'; ?>" width="100" class="bcimg" />
            </div>
            <div style="clear:both;"></div>
        </div>

        <div id="buttons" style="padding-top:10px; text-transform:uppercase;" class="no-print">
            <hr>
           <?php
        /*    if ($message) {
                ?>
                <div class="alert alert-success">
                    <button data-dismiss="alert" class="close" type="button">×</button>
                    <?=is_array($message) ? print_r($message, true) : $message;?>
                </div>
                <?php
            } */
            ?>
            
            <?php
            if ($modal) {
                ?>
                <div class="btn-group btn-group-justified" role="group" aria-label="...">
                    <div class="btn-group" role="group">
                        <?php
                        if ($pos->remote_printing == 1) {
                            echo '<button onclick="window.print();" class="btn btn-block btn-primary">'.lang("print").'</button>';
                        } else {
                            echo '<button onclick="return printReceipt()" class="btn btn-block btn-primary">'.lang("print").'</button>';
                        }

                        ?>
                    </div>
                    <div class="btn-group" role="group">
                        <a class="btn btn-block btn-success" href="#" id="email"><?= lang("email"); ?></a>
                    </div>
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('close'); ?></button>
                    </div>
                </div>
                <?php
            } else {
                ?>
                <span class="pull-right col-xs-12">
                    <?php
                    if ($pos->remote_printing == 1) {
                        echo '<button onclick="window.print();" class="btn btn-block btn-primary">'.lang("print").'</button>';
                    } else {
                        echo '<button onclick="return printReceipt()" class="btn btn-block btn-primary">'.lang("print").'</button>';
                        echo '<button onclick="return openCashDrawer()" class="btn btn-block btn-default">'.lang("open_cash_drawer").'</button>';
                    }
                    ?>
                </span>
                <span class="pull-left col-xs-12"><a class="btn btn-block btn-success" href="#" id="email"><?= lang("email"); ?></a></span>
                <span class="col-xs-12">
                    <a class="btn btn-block btn-warning" href="<?= admin_url('pos'); ?>"><?= lang("back_to_pos"); ?></a>
                </span>
                <?php
            }
            if ($pos->remote_printing == 1) {
                ?>
                <div style="clear:both;"></div>
                <div class="col-xs-12" style="background:#F5F5F5; padding:10px;">
                    <p style="font-weight:bold;">
                        Please don't forget to disble the header and footer in browser print settings.
                    </p>
                    <p style="text-transform: capitalize;">
                        <strong>FF:</strong> File &gt; Print Setup &gt; Margin &amp; Header/Footer Make all --blank--
                    </p>
                    <p style="text-transform: capitalize;">
                        <strong>chrome:</strong> Menu &gt; Print &gt; Disable Header/Footer in Option &amp; Set Margins to None
                    </p>
                </div>
                <?php
            } ?>
            <div style="clear:both;"></div>
        </div>
    </div>

    <?php
    if( ! $modal) {
        ?>
        <script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
        <?php
    }
    ?>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#email').click(function () {
                bootbox.prompt({
                    title: "<?= lang("email_address"); ?>",
                    inputType: 'email',
                    value: "<?= $customer->email; ?>",
                    callback: function (email) {
                        if (email != null) {
                            $.ajax({
                                type: "post",
                                url: "<?= admin_url('pos/email_receipt') ?>",
                                data: {<?= $this->security->get_csrf_token_name(); ?>: "<?= $this->security->get_csrf_hash(); ?>", email: email, id: <?= $inv->id; ?>},
                                dataType: "json",
                                success: function (data) {
                                    bootbox.alert({message: data.msg, size: 'small'});
                                },
                                error: function () {
                                    bootbox.alert({message: '<?= lang('ajax_request_failed'); ?>', size: 'small'});
                                    return false;
                                }
                            });
                        }
                    }
                });
                return false;
            });
        });

        <?php
        if ($pos_settings->remote_printing == 1) {
            ?>
            $(window).load(function () {
                window.print();
                return false;
            });
            <?php
        }
        ?>

    </script>
    <?php /* include FCPATH.'themes'.DIRECTORY_SEPARATOR.$Settings->theme.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'pos'.DIRECTORY_SEPARATOR.'remote_printing.php'; */ ?>
    <?php include 'remote_printing.php'; ?>
    <?php
    if($modal) {
        ?>
    </div>
</div>
</div>
<?php
} else {
    ?>
</body>
</html>
<?php
}

?>

