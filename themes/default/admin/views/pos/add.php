<?php defined('BASEPATH') or exit('No direct script access allowed'); session_start(); ?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?=lang('pos_module') . " | " . $Settings->site_name;?></title>
    <script type="text/javascript">if(parent.frames.length !== 0){top.location = '<?=admin_url('pos')?>';}</script>
    <base href="<?=base_url()?>"/>
    <meta http-equiv="cache-control" content="max-age=0"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <meta http-equiv="pragma" content="no-cache"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="shortcut icon" href="<?= base_url().'assets/uploads/logos/'.$this->api_shop_setting[0]['icon']; ?>">   
    <link rel="stylesheet" href="<?=$assets?>styles/theme.css" type="text/css"/>
    <link rel="stylesheet" href="<?=$assets?>styles/style.css" type="text/css"/>
    <link rel="stylesheet" href="<?=$assets?>pos/css/posajax.css?v=1000" type="text/css"/>
    <link rel="stylesheet" href="<?=$assets?>pos/css/posajax_600.css?v=1" type="text/css"/>
    <link rel="stylesheet" href="<?=$assets?>pos/css/posajax_768.css" type="text/css"/>
    <link rel="stylesheet" href="<?=$assets?>pos/css/print.css" type="text/css" media="print"/>
    <script type="text/javascript" src="<?=$assets?>js/jquery-2.0.3.min.js"></script>
    <script type="text/javascript" src="<?=$assets?>js/jquery-migrate-1.2.1.min.js"></script>
    <script  src="<?php echo base_url().'assets/api/js/public.js'; ?>"></script>
    <link rel="stylesheet" href="<?php echo base_url().'assets/api/css/public.css'; ?>" type="text/css"/>
    <!--[if lt IE 9]>
    <script src="assets/js/jquery.js"></script>
    <![endif]-->
    <?php if ($Settings->user_rtl) {?>
        <link href="<?=$assets?>styles/helpers/bootstrap-rtl.min.css" rel="stylesheet"/>
        <link href="<?=$assets?>styles/style-rtl.css" rel="stylesheet"/>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.pull-right, .pull-left').addClass('flip');
            });
        </script>
    <?php }
    ?>
    <style>
        .pos_style{
            font-size:20px;
        }
        /* #select2-drop {
            left: 381.766px !important;
            width: 127px !important;
        } */
    </style>
</head>
<body>

<noscript>
    <div class="global-site-notice noscript">
        <div class="notice-inner">
            <p><strong>JavaScript seems to be disabled in your browser.</strong><br>You must have JavaScript enabled in
                your browser to utilize the functionality of this website.</p>
        </div>
    </div>
</noscript>

<div id="wrapper">
    <header id="header" class="navbar hidden-xs">
        <div class="container">
            <a class="navbar-brand" href="<?=admin_url()?>"><span class="logo"><span class="pos-logo-lg"><?=$Settings->site_name?></span><span class="pos-logo-sm"><?=lang('pos')?></span></span></a>

            <div class="header-nav">
                <ul class="nav navbar-nav pull-right">
                    <li class="dropdown">
                        <a class="btn account dropdown-toggle" data-toggle="dropdown" href="#">
                            <img alt="" src="<?=$this->session->userdata('avatar') ? base_url() . 'assets/uploads/avatars/thumbs/' . $this->session->userdata('avatar') : $assets . 'images/' . $this->session->userdata('gender') . '.png';?>" class="mini_avatar img-rounded">

                            <div class="user">
                                <span><?=lang('welcome')?>! <?=$this->session->userdata('username');?></span>
                            </div>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="<?=admin_url('auth/profile/' . $this->session->userdata('user_id'));?>">
                                    <i class="fa fa-user"></i> <?=lang('profile');?>
                                </a>
                            </li>
                            <li>
                                <a href="<?=admin_url('auth/profile/' . $this->session->userdata('user_id') . '/#cpassword');?>">
                                    <i class="fa fa-key"></i> <?=lang('change_password');?>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<?=admin_url('auth/logout');?>">
                                    <i class="fa fa-sign-out"></i> <?=lang('logout');?>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>

                <ul class="nav navbar-nav pull-right">
                    <li class="dropdown">
                        <a class="btn bblue pos-tip" title="<?=lang('dashboard')?>" data-placement="bottom" href="<?=admin_url('welcome')?>">
                            <i class="fa fa-dashboard"></i>
                        </a>
                    </li>
                    <?php if ($Owner) {?>
                        <li class="dropdown hidden-sm hidden-xs">
                            <a class="btn pos-tip" title="<?=lang('settings')?>" data-placement="bottom" href="<?=admin_url('pos/settings')?>">
                                <i class="fa fa-cogs"></i>
                            </a>
                        </li>
                    <?php }
                    ?>
                    <li class="dropdown hidden-xs">
                        <a class="btn pos-tip" title="<?=lang('calculator')?>" data-placement="bottom" href="#" data-toggle="dropdown">
                            <i class="fa fa-calculator"></i>
                        </a>
                        <ul class="dropdown-menu pull-right calc">
                            <li class="dropdown-content">
                                <span id="inlineCalc"></span>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown hidden-sm hidden-xs">
                        <a class="btn pos-tip" title="<?=lang('shortcuts')?>" data-placement="bottom" href="#" data-toggle="modal" data-target="#sckModal">
                            <i class="fa fa-key"></i>
                        </a>
                    </li>
                    <li class="dropdown hidden-xs">
                        <a class="btn pos-tip" title="<?=lang('view_bill_screen')?>" data-placement="bottom" href="<?=admin_url('pos/view_bill')?>" target="_blank">
                            <i class="fa fa-laptop"></i>
                        </a>
                    </li>
                    <li class="dropdown hidden-xs">
                        <a class="btn blightOrange pos-tip" id="opened_bills" title="<span><?=lang('suspended_sales')?></span>" data-placement="bottom" data-html="true" href="<?=admin_url('pos/opened_bills')?>" data-toggle="ajax">
                            <i class="fa fa-th"></i>
                        </a>
                    </li>
                    <li class="dropdown hidden-xs">
                        <a class="btn bdarkGreen pos-tip" id="register_details" title="<span><?=lang('register_details')?></span>" data-placement="bottom" data-html="true" href="<?=admin_url('pos/register_details')?>" data-toggle="modal" data-target="#myModal">
                            <i class="fa fa-check-circle"></i>
                        </a>
                    </li>
                    <li class="dropdown hidden-xs">
                        <a class="btn borange pos-tip" id="close_register" title="<span><?=lang('close_register')?></span>" data-placement="bottom" data-html="true" data-backdrop="static" href="<?=admin_url('pos/close_register')?>" data-toggle="modal" data-target="#myModal">
                            <i class="fa fa-times-circle"></i>
                        </a>
                    </li>
                    <li class="dropdown hidden-xs">
                        <a class="btn borange pos-tip" id="add_expense" title="<span><?=lang('add_expense')?></span>" data-placement="bottom" data-html="true" href="<?=admin_url('purchases/add_expense')?>" data-toggle="modal" data-target="#myModal">
                            <i class="fa fa-dollar"></i>
                        </a>
                    </li>
                    <?php if ($Owner) {?>
                        <li class="dropdown hidden-xs">
                            <a class="btn bdarkGreen pos-tip" id="today_profit" title="<span><?=lang('today_profit')?></span>" data-placement="bottom" data-html="true" href="<?=admin_url('reports/profit')?>" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-hourglass-half"></i>
                            </a>
                        </li>
                    <?php }
                    ?>
                    <?php if ($Owner || $Admin) {?>
                        <li class="dropdown hidden-xs">
                            <a class="btn bdarkGreen pos-tip" id="today_sale" title="<span><?=lang('today_sale')?></span>" data-placement="bottom" data-html="true" href="<?=admin_url('pos/today_sale')?>" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-heart"></i>
                            </a>
                        </li>
                        <li class="dropdown hidden-xs">
                            <a class="btn bblue pos-tip" title="<?=lang('list_open_registers')?>" data-placement="bottom" href="<?=admin_url('pos/registers')?>">
                                <i class="fa fa-list"></i>
                            </a>
                        </li>
                    <?php }
                    ?>
                    <li class="dropdown hidden-xs">
                        <a class="btn bred pos-tip" title="<?=lang('clear_ls')?>" data-placement="bottom" id="clearLS" href="#">
                            <i class="fa fa-eraser"></i>
                        </a>
                    </li>
                </ul>

                <ul class="nav navbar-nav pull-right">
                    <li class="dropdown hidden-xs hidden-sm">
                        <a class="btn bblack" style="cursor: default;"><span id="display_time"></span></a>
                    </li>
                </ul>
            </div>
        </div>
    </header>
<!-- mobile -->
    <header id="header" class="navbar visible-xs">
        <div class="container">
            <table width="100%">
                <tr>
                    <td>
                        <a class="navbar-brand" href="<?=admin_url()?>"><span class="logo"><span class="pos-logo-lg"><?=$Settings->site_name?></span><span class="pos-logo-sm"><?=lang('pos')?></span></span></a>
                    </td>
                    <td>
                        <div class="header-nav">
                            <ul class="nav navbar-nav pull-right"> 
                                <li class="dropdown">
                                    <a class="btn bblue pos-tip" title="<?=lang('dashboard')?>" data-placement="bottom" href="<?=admin_url('welcome')?>">
                                        <i class="fa fa-dashboard"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </td>
                    <td>
                        <div class="header-nav">
                            <ul class="nav navbar-nav pull-right">
                                <li class="dropdown">
                                    <a class="btn account dropdown-toggle" data-toggle="dropdown" href="#">
                                        <img alt="" src="<?=$this->session->userdata('avatar') ? base_url() . 'assets/uploads/avatars/thumbs/' . $this->session->userdata('avatar') : $assets . 'images/' . $this->session->userdata('gender') . '.png';?>" class="mini_avatar img-rounded">

                                        <div class="user">
                                            <span><?=lang('welcome')?>! <?=$this->session->userdata('username');?></span>
                                        </div>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a href="<?=admin_url('auth/profile/' . $this->session->userdata('user_id'));?>">
                                                <i class="fa fa-user"></i> <?=lang('profile');?>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?=admin_url('auth/profile/' . $this->session->userdata('user_id') . '/#cpassword');?>">
                                                <i class="fa fa-key"></i> <?=lang('change_password');?>
                                            </a>
                                        </li>
                                        <li class="divider"></li>
                                        <li>
                                            <a href="<?=admin_url('auth/logout');?>">
                                                <i class="fa fa-sign-out"></i> <?=lang('logout');?>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </td>
                </tr>
            </table>
            
    </header>

    <div id="content" class="">
        <div class="c1">
            <div class="pos">
                <?php
                    if ($error) {
                        echo "<div class=\"alert alert-danger\"><button type=\"button\" class=\"close fa-2x\" data-dismiss=\"alert\">&times;</button>" . $error . "</div>";
                    }
                ?>
                <?php
                    if ($message) {
                        echo "<div class=\"alert alert-success\"><button type=\"button\" class=\"close fa-2x\" data-dismiss=\"alert\">&times;</button>" . $message . "</div>";
                    }
                ?>
                <div id="pos" class="visible">
                    <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'pos-sale-form', 'name' => 'pos-sale-form');
                    echo admin_form_open("pos", $attrib);?>
                    <div id="leftdiv">
                        <div id="printhead">
                            <h4 style="text-transform:uppercase;"><?php echo $Settings->site_name; ?></h4>
                            <?php
                                echo "<h5 style=\"text-transform:uppercase;\">" . $this->lang->line('order_list') . "</h5>";
                                echo $this->lang->line("date") . " " . $this->sma->hrld(date('Y-m-d H:i:s'));
                            ?>
                        </div>
                        <div id="left-top">
                            <div
                                style="position: absolute; <?=$Settings->user_rtl ? 'right:-9999px;' : 'left:-9999px;';?>"><?php echo form_input('test', '', 'id="test" class="kb-pad"'); ?></div>
                            <div class="form-group">
                                <div class="input-group ">
                                <?php
    
                                    for ($i=0;$i<count($companies);$i++) {
                                        if ($companies[$i]['id'] == 1) {
                                            $tr[$companies[$i]['id']] = $companies[$i]['company'].' [Name: '.$companies[$i]['name'].']';
                                            break;
                                        }
                                    }
                                    for ($i=0;$i<count($companies);$i++) {
                                        if ($companies[$i]['id'] != 1) {
                                            $tr[$companies[$i]['id']] = $companies[$i]['company'].' [Name: '.$companies[$i]['name'].']';
                                        }
                                    }
                                    echo form_dropdown('customer2', $tr, 1, 'data-placeholder="'.lang("Please_select_a_customer").'" id="" class="form-control" required="required" onchange="document.getElementById(\'poscustomer\').value = this.value; get_customer_paid_by(this.value);" ');

                                    echo '<input id="poscustomer" name="customer" type="text" value="" required="required" style="display:none !important;"/>';

/*
                                    echo form_input('customer', (isset($_POST['customer']) ? $_POST['customer'] : ""), 'id="poscustomer" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("customer") . '" required="required" class="form-control pos-input-tip" style="width:100%;"');
                                    echo '
                                    <div class="input-group-addon no-print" style="padding: 2px 8px; border-left: 0;">
                                        <a href="#" id="toogle-customer-read-attr" class="external">
                                            <i class="fa fa-pencil" id="addIcon" style="font-size: 1.2em;"></i>
                                        </a>
                                    </div>
                                    ';
*/

                                ?>

                                    <div class="input-group-addon no-print" style="padding: 2px 7px; border-left: 0;">
                                        <a href="#" id="view-customer" class="external" data-toggle="modal" data-target="#myModal">
                                            <i class="fa fa-eye" id="addIcon" style="font-size: 1.2em;"></i>
                                        </a>
                                    </div>
                                <?php if ($Owner || $Admin || $GP['customers-add']) { ?>
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        <a href="<?=admin_url('customers/add');?>" id="add-customer" class="external" data-toggle="modal" data-target="#myModal">
                                            <i class="fa fa-plus-circle" id="addIcon" style="font-size: 1.5em;"></i>
                                        </a>
                                    </div>
                                <?php } ?>
                                </div>
                                <div style="clear:both;"></div>
                            </div>
                            <div class="no-print">
                                <?php if ($Owner || $Admin || !$this->session->userdata('warehouse_id')) {
                                    ?>
                                    <div class="form-group">
                                        <?php
                                            $wh[''] = '';
                                    foreach ($warehouses as $warehouse) {
                                        $wh[$warehouse->id] = $warehouse->name;
                                    }
                                    echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : $Settings->default_warehouse), 'id="poswarehouse" class="form-control pos-input-tip api_display_none" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("warehouse") . '" required="required" style="width:100%;" '); ?>
                                    </div>
                                <?php
                                } else {
                                    $warehouse_input = array(
                                            'type' => 'hidden',
                                            'name' => 'warehouse',
                                            'id' => 'poswarehouse',
                                            'value' => $this->session->userdata('warehouse_id'),
                                        );

                                    echo form_input($warehouse_input);
                                }
                                ?>
                                <div class="form-group" id="ui">
                                    <?php if ($Owner || $Admin || $GP['products-add']) { ?>
                                    <div class="input-group">
                                    <?php } ?>
                                    
                                    <?php echo form_input('add_item', '', 'class="form-control pos-tip" id="add_item" data-placement="top" data-trigger="focus" placeholder="' . $this->lang->line("search_product_by_name_code") . '" title="' . $this->lang->line("au_pr_name_tip") . '"'); ?>
                                    <?php if ($Owner || $Admin || $GP['products-add']) { ?>
                                        <div class="input-group-addon" style="padding: 2px 8px;">
                                            <a href="#" id="addManually">
                                                <i class="fa fa-plus-circle" id="addIcon" style="font-size: 1.5em;"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <div style="clear:both;"></div>
                                </div>
                            </div>
                        </div>
                        <div id="print" >
                            <div id="left-middle2" class="hii" >
                                <div id="product-list" >
                                    <?php if($this->api_helper->is_mobile() != 1) {?>
                                    <table class="table items table-striped table-bordered table-condensed table-hover sortable_table"
                                           id="posTable" style="margin-bottom: 0;">
                                        <thead>
                                        <tr>
                                            <th width="40%"><?=lang("product");?></th>
                                            <th width="12%"><?=lang("price");?></th>
                                            <th width="14%"><?=lang("qty");?></th>
                                            <th width="21%"><?=lang("Original Total");?></th>
                                            <th width="15%"><?=lang("Discount");?></th>
                                            <th width="20%"><?=lang("subtotal");?></th>
                                            <th style="width: 5%; text-align: center;">
                                                <i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                    <?php } else{?>
                                    <table class="table items table-striped table-bordered table-condensed table-hover sortable_table"
                                    id="posTable" style="margin-bottom: 0;" width="100%">
                                           <!-- <?php echo $this->api_helper->is_mobile(); ?> -->
                                        <thead>
                                            <tr>
                                                <th width="60%"><?=lang("Product Details");?></th>
                                                <th width="30%"><?=lang("Subtotal");?></th>
                                                <th width="10%"><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                    <?php } ?>
                                    <div style="clear:both;"></div>
                                </div>
                            </div>
                           
                                <table class="table table-bordered table-condensed table-hover sortable_table"
                                           id="posTable" style="margin-bottom: 0;">
                                        <thead>
                                            <tr>
                                                <th ><?=lang("items");?></th>
                                                <th ><?=lang("qty");?></th>
                                                <th ><?=lang("Total Before Discount");?></th>
                                                <th ><?=lang("Total Discount");?></th>
                                                <th ><?=lang("Total After Discount");?></th>
                                                
                                            </tr>
                                            <tr class="bold pos_style">
                                                <td style="text-align: center;"><span id="titems">0</span></td>
                                                <td width="11%" style="text-align: center;"><span id="qty">0</span></td>
                                                <td style="text-align: center; display:none"><span id="total">0.00</span></td>
                                                <td style="text-align: center;"><span id="total_before_discount" class="pull-right">0.00</span></td>
                                                <td style="text-align: center;"><span id="discount" class="pull-right">0.00</span></td>
                                                <td style="text-align: center;"><span id="total_after_discount" class="pull-right">0.00</span></td>
                                            </tr>
                                        </thead>
                                    </table>
                            

                            <div style="clear:both;"></div>
                            <div id="left-bottom">
                                <table id="totalTable"
                                       style="width:100%; float:right; padding:5px; color:#000; background: #FFF;">
                            <!-- New version -->
                                    <tr>
                                    <td style="padding: 5px 10px; font-weight:bold; " colspan="2"><?=lang('Order Discount');?>
                                            <?php if ($Owner || $Admin || $this->session->userdata('allow_discount')) { ?>
                                            <a href="#" id="ppdiscount">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <?php } ?>
                                        </td>
                                        <td class="text-right" style="padding:5px 10px 5px 10px; font-size: 14px; font-weight:bold;" colspan="2">
                                            <span id="tds" class="pos_style">0.00</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 5px 10px; font-weight:bold;" colspan="2">
                                            <?=lang('total_payable');?>
                                            <a href="#" id="pshipping">
                                                <i class="fa fa-plus-square api_display_none"></i>
                                            </a>
                                            <span id="tship"></span>
                                        </td>
                                    <td><div id="total_khr" style="font-weight:bold;" class="pos_style">(KHR) 0</div></td>
                                        <td class="text-right" style="padding:5px 10px 5px 10px; font-size: 14px;font-weight:bold;" colspan="2">
                                            <span id="gtotal" class="pos_style api_display_none">0.00</span>
                                            <span id="total_payable" class="pos_style">(USD) 0.00</span>
                                            <span id="hide_total_payable" hidden></span>
                                        </td>
                                    </tr>
                                </table>

                                <div class="clearfix"></div>
                                <div id="botbuttons" class="col-xs-12 text-center">
                                    <input type="hidden" name="biller" id="biller" value="<?= ($Owner || $Admin || !$this->session->userdata('biller_id')) ? $pos_settings->default_biller : $this->session->userdata('biller_id')?>"/>
                                    <div class="row">
                                        <div class="col-xs-2" style="padding: 0;">
                                            <div class="btn-group-vertical btn-block">
                                                <button type="button" class="btn btn-warning btn-block btn-flat"
                                                id="suspend">
                                                    <?=lang('suspend'); ?>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="col-xs-2" style="padding: 0;">
                                            <div class="btn-group-vertical btn-block">
                                                <button type="button" class="btn btn-danger btn-block btn-flat"
                                                id="reset">
                                                    <?= lang('cancel'); ?>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="col-xs-2" style="padding: 0;">
                                            <div class="btn-group-vertical btn-block">
                                                <button type="button" class="btn btn-info btn-block" id="print_order">
                                                    <?=lang('order');?>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="col-xs-2" style="padding: 0;">
                                            <div class="btn-group-vertical btn-block">
                                                <button type="button" class="btn btn-primary btn-block" id="print_bill">
                                                    <?=lang('bill');?>
                                                </button>
                                            </div>
                                        </div>
                                        
                                        <div class="col-xs-4" style="padding: 0;">
                                            <button type="button" class="btn btn-success btn-block" onclick="get_amount_usd(); $('#paid_by_1').val(document.getElementById('add_ons_poscustomer_id').value).trigger('change');$('#paid_by_1').select2('open'); $('#paid_by_1').select2('close'); " id="payment">
                                                <i class="fa fa-money" style="margin-right: 5px;"></i><?=lang('payment');?>
                                            </button>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div style="clear:both; height:5px;"></div>
                                <div id="num">
                                    <div id="icon"></div>
                                </div>
                                <span id="hidesuspend"></span>
                                <input type="hidden" name="pos_note" value="" id="pos_note">
                                <input type="hidden" name="staff_note" value="" id="staff_note">

                                <div id="payment-con">
                                    <?php for ($i = 1; $i <= 5; $i++) {?>
                                        <input type="hidden" name="amount[]" id="amount_val_<?=$i?>" value=""/>
                                        <input type="hidden" name="balance_amount[]" id="balance_amount_<?=$i?>" value=""/>
                                        <input type="hidden" name="paid_by[]" id="paid_by_val_<?=$i?>" value="cash"/>
                                        <input type="hidden" name="cc_no[]" id="cc_no_val_<?=$i?>" value=""/>
                                        <input type="hidden" name="paying_gift_card_no[]" id="paying_gift_card_no_val_<?=$i?>" value=""/>
                                        <input type="hidden" name="cc_holder[]" id="cc_holder_val_<?=$i?>" value=""/>
                                        <input type="hidden" name="cheque_no[]" id="cheque_no_val_<?=$i?>" value=""/>
                                        <input type="hidden" name="cc_month[]" id="cc_month_val_<?=$i?>" value=""/>
                                        <input type="hidden" name="cc_year[]" id="cc_year_val_<?=$i?>" value=""/>
                                        <input type="hidden" name="cc_type[]" id="cc_type_val_<?=$i?>" value=""/>
                                        <input type="hidden" name="cc_cvv2[]" id="cc_cvv2_val_<?=$i?>" value=""/>
                                        <input type="hidden" name="payment_note[]" id="payment_note_val_<?=$i?>" value=""/>



                                    <?php } ?>
                                </div>
                                <input name="order_tax" type="hidden" value="<?=$suspend_sale ? $suspend_sale->order_tax_id : ($old_sale ? $old_sale->order_tax_id : $Settings->default_tax_rate2);?>" id="postax2">
                                <input name="discount" type="hidden" value="<?=$suspend_sale ? $suspend_sale->order_discount_id : ($old_sale ? $old_sale->order_discount_id : '');?>" id="posdiscount">
                                <input name="shipping" type="hidden" value="<?=$suspend_sale ? $suspend_sale->shipping : ($old_sale ? $old_sale->shipping :  '0');?>" id="posshipping">
                                <input type="hidden" name="rpaidby" id="rpaidby" value="cash" style="display: none;"/>
                                <input type="hidden" name="total_items" id="total_items" value="0" style="display: none;"/>

<input type="hidden" name="add_ons_aba_qr_transfer_reference_number" id="add_ons_aba_qr_transfer_reference_number_form" value=""/>
<input type="hidden" name="add_ons_wing_transaction_id" id="add_ons_wing_transaction_id_form" value=""/>
<input type="hidden" name="add_ons_wing_daiki_transaction_id" id="add_ons_wing_daiki_transaction_id_form" value=""/>
<input type="hidden" name="add_ons_pipay_transaction_id" id="add_ons_pipay_transaction_id_form" value=""/>
<input type="hidden" name="add_ons_aba_daiki_transaction_id" id="add_ons_aba_daiki_transaction_id_form" value=""/>
<input type="hidden" name="add_ons_payway_transaction_id" id="add_ons_payway_transaction_id_form" value=""/>
<input type="hidden" name="add_ons_acode_daiki_transaction_id" id="add_ons_acode_daiki_transaction_id_form" value=""/>

<input type="hidden" name="add_ons_customer_account_number" id="add_ons_customer_account_number_form" value=""/>
<input type="hidden" name="add_ons_company_account_number" id="add_ons_company_account_number_form" value=""/>
<input type="hidden" name="add_ons_transfer_amount" id="add_ons_transfer_amount_form" value=""/>
<input type="hidden" name="add_ons_transfer_reference_number" id="add_ons_transfer_reference_number_form" value=""/>    
<input type="hidden" name="temp_customer_pay_usd" id="temp_customer_pay_usd" value=""/>
<input type="hidden" name="temp_customer_pay_khr" id="temp_customer_pay_khr" value=""/>
<input type="hidden" name="shipping_price_v2" id="shipping_price_v2" value=""/>
<input type="hidden" name="add_ons_pos_customer_pay_usd" id="add_ons_customer_pay_usd_v2" value=""/>
<input type="hidden" name="add_ons_pos_customer_pay_khr" id="add_ons_customer_pay_khr_v2" value=""/>
<input type="hidden" name="add_ons_pos_customer_change_usd" id="add_ons_customer_change_usd_v2" value=""/>
<input type="hidden" name="add_ons_pos_customer_change_khr" id="add_ons_customer_change_khr_v2" value=""/>
<input type="hidden" name="add_ons_cashier" id="add_ons_cashier" value=""/>
<input type="hidden" name="add_ons_poscustomer_id" id="add_ons_poscustomer_id" value=""/>
    
<?php                      

?>
                                <input type="submit" id="submit_sale" value="Submit Sale" style="display: none;"/>
                            </div>
                        </div>

                    </div>
                   
                    <div id="cp">
                        <div id="cpinner" style="max-width: 48%;">
                            <div class="quick-menu" >
                                <div id="proContainer" >
                                    <div id="ajaxproducts">
                                        <div id="item-list" style="min-height:763px;">
                                            <?php echo $products; ?>
                                        </div>
                                        <div class="btn-group btn-group-justified pos-grid-nav">
                                            <div class="btn-group">
                                                <button style="z-index:10002;" class="btn btn-primary pos-tip" title="<?=lang('previous')?>" type="button" id="previous">
                                                    <i class="fa fa-chevron-left"></i>
                                                </button>
                                            </div>
                                            <?php if ($Owner || $Admin || $GP['sales-add_gift_card']) {?>
                                            <div class="btn-group">
                                                <button style="z-index:10003;" class="btn btn-primary pos-tip" type="button" id="sellGiftCard" title="<?=lang('sell_gift_card')?>">
                                                    <i class="fa fa-credit-card" id="addIcon"></i> <?=lang('sell_gift_card')?>
                                                </button>
                                            </div>
                                            <?php }
                                            ?>
                                            <div class="btn-group">
                                                <button style="z-index:10004;" class="btn btn-primary pos-tip" title="<?=lang('next')?>" type="button" id="next">
                                                    <i class="fa fa-chevron-right"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="clear:both;"></div>
                                </div>
                            </div>
                        </div>
                        <div style="clear:both;"></div>
                    </div>
                    <div style="clear:both;"></div>
                </div>
                <div style="clear:both;"></div>
            </div>
        </div>
    </div>
</div>

<div class="rotate btn-cat-con api_display_none">
    <button type="button" id="open-nobarcode" class="btn btn-success api_display_none" onclick="
            var postData = {
                'ajax_file' : '\.admin_url()\.pos/display_category_nobarcode',
                'element_id' : 'item-list',
            };
            ajax_display_category_nobarcode(postData);">
            <?= lang('No Barcode'); ?>
    </button>
    <button type="button" id="open-special" class="btn btn-danger api_display_none" onclick="
            var postData = {
                'ajax_file' : '\.admin_url()\.pos/display_category_special',
                'element_id' : 'item-list',
            };
            ajax_display_category_special(postData);">
            <?= lang('Special'); ?>
    </button>
    <button type="button" id="open-brands" class="btn btn-info open-brands api_display_none"><?= lang('brands'); ?></button>
    <button type="button" id="open-subcategory" class="btn btn-warning open-subcategory"><?= lang('subcategories'); ?></button>
    <button type="button" id="open-category" class="btn btn-primary open-category"><?= lang('categories'); ?></button>
</div>
<div id="brands-slider">
    <div id="brands-list">
        <?php
            // for ($i = 1; $i <= 40; $i++) {
            foreach ($brands as $brand) {
                echo "<button id=\"brand-" . $brand->id . "\" type=\"button\" value='" . $brand->id . "' class=\"btn-prni brand\" ><img src=\"assets/uploads/thumbs/" . ($brand->image ? $brand->image : 'no_image.png') . "\" class='img-rounded img-thumbnail' /><span>" . $brand->name . "</span></button>";
            }
            // }
        ?>
    </div>
</div>
<div id="category-slider">
    <!--<button type="button" class="close open-category"><i class="fa fa-2x">&times;</i></button>-->
    <div id="category-list">
        <?php
            //for ($i = 1; $i <= 40; $i++) {
            foreach ($categories as $category) {
                echo "<button id=\"category-" . $category->id . "\" type=\"button\" value='" . $category->id . "' class=\"btn-prni category\" ><img src=\"assets/uploads/thumbs/" . ($category->image ? $category->image : 'no_image.png') . "\" class='img-rounded img-thumbnail' /><span>" . $category->name . "</span></button>";
            }
            //}
        ?>
    </div>
</div>
<div id="subcategory-slider">
    <!--<button type="button" class="close open-category"><i class="fa fa-2x">&times;</i></button>-->
    <div id="subcategory-list">
        <?php
            if (!empty($subcategories)) {
                foreach ($subcategories as $category) {
                    echo "<button id=\"subcategory-" . $category->id . "\" type=\"button\" value='" . $category->id . "' class=\"btn-prni subcategory\" ><img src=\"assets/uploads/thumbs/" . ($category->image ? $category->image : 'no_image.png') . "\" class='img-rounded img-thumbnail' /><span>" . $category->name . "</span></button>";
                }
            }
        ?>
    </div> 
</div>
<div class="modal fade in" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="payModalLabel"
     aria-hidden="true" style="top:30px">
    <div class="modal-dialog modal-lg api_temp_modal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                            class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span></button>
                <h4 class="modal-title" id="payModalLabel"><?=lang('finalize_sale');?></h4>
            </div>
            <div class="modal-body" id="payment_content">
               
                    <div class="col-md-12 col-sm-9 api_clear_both">
                        <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                            <div class="form-group api_display_none">
                                <?=lang("biller", "biller");?>
                                <?php
                                    foreach ($billers as $biller) {
                                        $btest = ($biller->company && $biller->company != '-' ? $biller->company : $biller->name);
                                        $bl[$biller->id] = $btest;
                                        $posbillers[] = array('logo' => $biller->logo, 'company' => $btest);
                                        if ($biller->id == $pos_settings->default_biller) {
                                            $posbiller = array('logo' => $biller->logo, 'company' => $btest);
                                        }
                                    }
                                    echo form_dropdown('biller', $bl, (isset($_POST['biller']) ? $_POST['biller'] : $pos_settings->default_biller), 'class="form-control" id="posbiller" required="required"');
                                ?>
                            </div>
                        <?php } else {
                                    $biller_input = array(
                                    'type' => 'hidden',
                                    'name' => 'biller',
                                    'id' => 'posbiller',
                                    'value' => $this->session->userdata('biller_id'),
                                );

                                    echo form_input($biller_input);

                                    foreach ($billers as $biller) {
                                        $btest = ($biller->company && $biller->company != '-' ? $biller->company : $biller->name);
                                        $posbillers[] = array('logo' => $biller->logo, 'company' => $btest);
                                        if ($biller->id == $this->session->userdata('biller_id')) {
                                            $posbiller = array('logo' => $biller->logo, 'company' => $btest);
                                        }
                                    }
                                }
                        ?>
                        
                        <div class="form-group api_display_none">
                            <div class="row">
                                <div class="col-sm-6">
                                    <?=form_textarea('sale_note', '', 'id="sale_note" class="form-control kb-text skip" style="height: 100px;" placeholder="' . lang('sale_note') . '" maxlength="250"');?>
                                </div>
                                <div class="col-sm-6">
                                    <?=form_textarea('staffnote', '', 'id="staffnote" class="form-control kb-text skip" style="height: 100px;" placeholder="' . lang('staff_note') . '" maxlength="250"');?>
                                </div>
                            </div>
                        </div>

                <!-- Select Cashier Name -->
                <div class="api_clear_both"style="height:15px;"></div>
                    <div class="col-md-12">
                        <div class="form-group api_temp_cashier">
                            <?php echo lang("Cashier",'cashier'); ?>
                            <div class="controls"> 
                                <?php
                                    $config_data = array(
                                        'table_name' => 'sma_cashier',
                                        'select_table' => 'sma_cashier',
                                        'field_name' => 'translate',
                                        'translate' => 'yes',
                                        'select_condition' => "id > 0",
                                    );
                                    $select_data = $this->site->api_select_data_v2($config_data);
                                        for ($i=0;$i<count($select_data);$i++) {
                                            if ($select_data[$i]['title_en'] != '')
                                                $td[$select_data[$i]['id']] = $select_data[$i]['title_en'];
                                        }
                                        $config_data = array(
                                            'table_name' => 'sma_sales',
                                            'select_table' => 'sma_sales',
                                            'select_condition' => "id > 0 order by id desc limit 1",
                                        );
                                        $select_sales = $this->site->api_select_data_v2($config_data);
                                        echo form_dropdown('cashier', $td, $select_sales[0]['cashier'], 'class="form-control api_select_v2"  id="api_cashier"');

                                ?>
                                
                            </div>
                        </div>
                    </div>
               
                        <div class="clearfir"></div>
                        <div id="payments">
                            <div class="well well-sm well_1 api_temp_modal" style="margin: -6px;">
                                <div class="payment">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <?=lang("Shipping", "shipping");?>
                                                <span style="margin-left:10px">
                                                    <input type="radio" name="shipping_v2" value="yes" onclick="shipping_price(this.value); $('#shipping_price').focus();"  />  Yes
                                                    <span style="margin-left:10px">
                                                        <input type="radio" name="shipping_v2" value="no" onclick="shipping_price(this.value); $('#shipping_price').focus();" checked />  No
                                                    </span>
                                                </span>
                                                
                                            </div>
                                            <div class="form-group" id="hidden_shipping_price">
                                                <?=lang("Shipping Price", "shipping_price");?>
                                                <input type="text" name="shipping_price" id="shipping_price" class="form-control api_numberic_input">
                                                <input type="hidden" name="shipping_price_hidden" id="shipping_price_hidden" class="form-control api_numberic_input">
                                            </div>
                                        </div>
                                    </div>
                                   
                                    <div class="row">                               
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <?=lang("Amount_USD", "amount_1");?>
                                                    <input name="amount[]" type="hidden" id="amount_1"
                                                       class="pa form-control kb-pad1_temp api_numberic_input" />
                                                
                                                       <input name="amount_v1[]" type="hidden" id="amount_1_v1"
                                                       class="pa form-control kb-pad1_temp api_numberic_input" readonly="readonly"/>
                                                       <input type="text" name="amount_v2[]" id="amount_1_v2"  class="pa form-control api_numberic_input" readonly="readonly" />
                                            </div>
                                        </div>
                                        
                                        <input type="hidden" id="rate_currency" value="4100"/>
                                        
        
                                        <div class="col-md-3">
                                            <div class="form-group">   
                                                <?=lang("Amount_KHR", "amount_khr");?>
                                                <input name="amount_khr" type="hidden" id="amount_khr"
                                                       class="pa form-control api_numberic_input" readonly="readonly" />
                                                <input name="amount_khr_v2" type="text" id="amount_khr_v2"
                                                       class="pa form-control api_numberic_input" readonly="readonly" />
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <?=lang("Customer_Pay_USD", "customer_pay_usd");?>
                                                <input name="customer_pay_usd" type="text" id="customer_pay_usd"
                                                       class="pa form-control api_numberic_input" />
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <?=lang("Customer_Pay_KHR", "customer_pay_khr");?>
                                                <input name="customer_pay_khr" type="text" id="customer_pay_khr"
                                                       class="pa form-control api_numberic_input" />
                                            </div>
                                        </div>
                                    </div>
                                  
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <?=lang("paying_by", "paid_by_1");?>
                                                <select name="paid_by[]" id="paid_by_1" class="form-control paid_by">
                                                    <?= $this->sma->paid_opts(); ?>
                                                    <?=$pos_settings->paypal_pro ? '<option value="ppp">' . lang("paypal_pro") . '</option>' : '';?>
                                                    <?=$pos_settings->stripe ? '<option value="stripe">' . lang("stripe") . '</option>' : '';?>
                                                    <?=$pos_settings->authorize ? '<option value="authorize">' . lang("authorize") . '</option>' : '';?>
                                                </select>
                                            </div>
                                            <script>
                                                   document.getElementById('paid_by_1').value = document.getElementById('add_ons_poscustomer_id').value; 
                                                   
                                            </script>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <?php
                                                    $array_field = array('customer_account_number','company_account_number','transfer_amount','transfer_reference_number');
                                                    $form_aba_qrcode = '<div id="aba_qr_bank_in_form">';
                                                    foreach ($array_field as $item) {
                                                        while ($item === 'transfer_reference_number') {
                                                            $form_aba_qrcode .= '
                                                                <div class="'.$temp_padding.'">                
                                                                <div class="form-group">
                                                
                                                                '.lang('Transfer_Reference_Number','Transfer_Reference_Number').'
                                                                    '.form_input("add_ons_aba_qr_".$item, '', 'class="form-control" id="aba_qr_'.$item.'"').'
                                                                </div>
                                                                </div>
                                                            ';
                                                            break;
                                                        }
                                                    }
                                                    $form_aba_qrcode .= '</div>';
                                                    echo $form_aba_qrcode;

                                                    $form_milti = ['aba_daiki','wing','pipay','payway','wing_daiki','acode_daiki'];
                                                    $generate_form = '';
                                                    foreach ($form_milti as $trans) {
                                                        $generate_form .= '<div id="api_'.$trans.'_form">';
                                                        $generate_form .= '   
                                                            <div class="'.$temp_padding.'">                                 
                                                            <div class="form-group">
                                                                '.lang(ucfirst($trans).'_Transaction_ID', ucfirst($trans).'_Transaction_Id').'
                                                                '.form_input('add_ons_'.$trans.'_transaction_id', '', 'class="form-control" id="'.$trans.'_transaction_id" ').'
                                                            </div>
                                                            </div>
                                                        ';
                                                        $generate_form .= '</div>';
                                                    }
                                                    echo $generate_form;
                                                ?>
                                                <?php echo form_close(); ?>
                                                <div class="form-group gc_1" style="display: none;">
                                                    <?=lang("gift_card_no", "gift_card_no_1");?>
                                                    <input name="paying_gift_card_no[]" type="text" id="gift_card_no_1"
                                                        class="pa form-control kb-pad gift_card_no"/>
                                                    <div id="gc_details_1"></div>
                                                </div>
                                                <div class="pcc_1" style="display:none;">
                                                    <div class="form-group">
                                                        <input type="text" id="swipe_1" class="form-control swipe"
                                                            placeholder="<?=lang('swipe')?>"/>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <input name="cc_no[]" type="text" id="pcc_no_1"
                                                                    class="form-control"
                                                                    placeholder="<?=lang('cc_no')?>"/>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">

                                                                <input name="cc_holer[]" type="text" id="pcc_holder_1"
                                                                    class="form-control"
                                                                    placeholder="<?=lang('cc_holder')?>"/>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <select name="cc_type[]" id="pcc_type_1"
                                                                        class="form-control pcc_type"
                                                                        placeholder="<?=lang('card_type')?>">
                                                                    <option value="Visa"><?=lang("Visa");?></option>
                                                                    <option
                                                                        value="MasterCard"><?=lang("MasterCard");?></option>
                                                                    <option value="Amex"><?=lang("Amex");?></option>
                                                                    <option
                                                                        value="Discover"><?=lang("Discover");?></option>
                                                                </select>
                                                                <!-- <input type="text" id="pcc_type_1" class="form-control" placeholder="<?=lang('card_type')?>" />-->
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input name="cc_month[]" type="text" id="pcc_month_1"
                                                                    class="form-control"
                                                                    placeholder="<?=lang('month')?>"/>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">

                                                                <input name="cc_year" type="text" id="pcc_year_1"
                                                                    class="form-control"
                                                                    placeholder="<?=lang('year')?>"/>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">

                                                                <input name="cc_cvv2" type="text" id="pcc_cvv2_1"
                                                                    class="form-control"
                                                                    placeholder="<?=lang('cvv2')?>"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="pcheque_1" style="display:none;">
                                                    <div class="form-group"><?=lang("cheque_no", "cheque_no_1");?>
                                                        <input name="cheque_no[]" type="text" id="cheque_no_1"
                                                            class="form-control cheque_no"/>
                                                    </div>
                                                </div>

                                                <div>
                                                                                                    
                                                </div>
                                                
                                                <div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <?=lang('payment_note', 'payment_note');?>
                                                <textarea name="payment_note[]" id="payment_note_1"
                                                          class="pa form-control kb-text payment_note"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <fieldset class="scheduler-border" style="margin-left: 14px; height: 122px !important;background: white;">
                                                    <legend class="scheduler-border" style="font-size: 14px !important; ">Change in USD</legend>                       
                                                        <div class="col-md-12" style="margin-top: -14px; !important;">
                                                                <div class="form-group">
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td style="font-weight:bold;text-align:right;border-bottom: 1px solid #fff;" >
                                                                                Customer Change (USD)
                                                                            </td>
                                                                            <td valign="middle"  style="border-bottom: 1px solid #fff;">:</td>
                                                                            <td id="customer_change_usd"  style="text-align:right;width: 15%;font-weight: bold;font-size: 25px;border-bottom: 1px solid #fff;padding-left: 5px;">
                                                                                0.00
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="font-weight:bold;text-align: right;margin-top:5px;border-bottom: 1px solid #fff;" >
                                                                                Customer Change (KHR)
                                                                            </td>
                                                                            <td valign="middle"  style="border-bottom: 1px solid #fff;">:</td>
                                                                            <td id="customer_change_khr" style="text-align:right;width: 15%;font-weight: bold;font-size: 25px;border-bottom: 1px solid #fff;padding-left: 5px;" >
                                                                                0.00
                                                                            </td>
                                                                        </tr>
                                                                    </table>    
                                                                </div>
                                                        </div>
                                                </fieldset> 
                                            </div>  
                                        </div>
                                        <div class="col-md-6">  
                                            <div class="form-group">
                                                <fieldset class="scheduler-border" style="background: white; height: 120px;">
                                                    <legend class="scheduler-border" style="font-size: 14px;!important;">Change in KHR</legend>        
                                                        <div class="col-md-12" style="margin-top: -14px; !important;">
                                                                <div class="form-group">
                                                                    <table style="border-bottom:none;width:100%">
                                                                        <td style="font-weight:bold;border-bottom:none;text-align: right;margin-top:5px" >
                                                                            Customer Change (KHR)
                                                                        </td>
                                                                        <td varlign="middle"  style="border-bottom: 1px solid #fff;">:</td>
                                                                        <td id="khmer_change" style="border-bottom:none;text-align:right;width: 15%;font-weight: bold;font-size: 25px;padding-left: 5px" >
                                                                            0.00
                                                                        </td>
                                                                    </table>    
                                                                </div>
                                                        </div>
                                                </fieldset> 
                                            </div>
                                        </div>  
                                    </div>

                                       <button class="btn btn-success pull-right api_display_none" onclick="customer_pay_money();">Calculate</button>
                                        <div class="api_clear_both"></div>
                                </div>
                                <div id="multi-payment"></div>
                        <button type="button" class="btn btn-primary col-md-12 addButton" style="display:none;"><i
                                class="fa fa-plus"></i> <?=lang('add_more_payments')?></button>
                        <div style="clear:both; height:15px;"></div>
                        <div class="font16">
                            <table width="100%" class="table table-bordered table-condensed table-striped" style="margin-bottom: 0;">
                                <tbody>
                                <tr>
                                    <td width="25%"><?=lang("total_items");?></td>
                                    <td width="25%" class="text-right"><span id="item_count">0.00</span></td>
                                    <td width="25%"><?=lang("total_payable");?></td>
                                    <td width="25%" class="text-right"><span id="twt">0.00</span></td>
                                </tr>
                                <tr style="display:none">
                                    <td><?=lang("total_paying");?></td>
                                    <td class="text-right"><span id="total_paying">0.00</span></td>
                                    <td><?=lang("balance");?></td>
                                    <td class="text-right"><span id="balance">0.00</span></td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                            </div>
                        </div>

                        
                    <div class="col-md-2 col-sm-3 text-center api_display_none">
                        <span style="font-size: 1.2em; font-weight: bold;"><?=lang('quick_cash');?></span>

                        <div class="btn-group btn-group-vertical">
                            <button type="button" class="btn btn-lg btn-info quick-cash" id="quick-payable">0.00
                            </button>
                            <?php
                                foreach (lang('quick_cash_notes') as $cash_note_amount) {
                                    echo '<button type="button" class="btn btn-lg btn-warning quick-cash" onclick="amount_khr(); customer_pay_money();">' . $cash_note_amount . '</button>';
                                }
                            ?>
                            <button type="button" class="btn btn-lg btn-danger"
                                    id="clear-cash-notes"><?=lang('clear');?></button>
                        </div>
                    </div>
            
            </div>
            <div class="api_clear_both"style="height:15px;"></div>
            <div class="modal-footer">
                <button class="btn btn-block btn-lg btn-primary" id="submit-sale" ><?=lang('submit');?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="cmModal" tabindex="-1" role="dialog" aria-labelledby="cmModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">
                    <i class="fa fa-2x">&times;</i></span>
                    <span class="sr-only"><?=lang('close');?></span>
                </button>
                <h4 class="modal-title" id="cmModalLabel"></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <div class="form-group">
                    <?= lang('comment', 'icomment'); ?>
                    <?= form_textarea('comment', '', 'class="form-control" id="icomment" style="height:80px;"'); ?>
                </div>
                <div class="form-group">
                    <?= lang('ordered', 'iordered'); ?>
                    <?php
                    $opts = array(0 => lang('no'), 1 => lang('yes'));
                    ?>
                    <?= form_dropdown('ordered', $opts, '', 'class="form-control" id="iordered" style="width:100%;"'); ?>
                </div>
                <input type="hidden" id="irow_id" value=""/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="editComment"><?=lang('submit')?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="prModal" tabindex="-1" role="dialog" aria-labelledby="prModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                            class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span></button>
                <h4 class="modal-title" id="prModalLabel"></h4>
                <p id="pro_discount_error" style="font-weight:bold;"></p>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <form class="form-horizontal" role="form">
                    <?php if ($Settings->tax1) {
                        ?>
                        <div class="form-group api_display_none">
                            <label class="col-sm-4 control-label"><?=lang('product_tax')?></label>
                            <div class="col-sm-8">
                                <?php
                                    $tr[""] = "";
                        foreach ($tax_rates as $tax) {
                            $tr[$tax->id] = $tax->name;
                        }
                        echo form_dropdown('ptax', $tr, "", 'id="ptax" class="form-control pos-input-tip" style="width:100%;"'); ?>
                            </div>
                        </div>
                    <?php
                    } ?>
                    <?php if ($Settings->product_serial) { ?>
                        <div class="form-group">
                            <label for="pserial" class="col-sm-4 control-label"><?=lang('serial_no')?></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control kb-text" id="pserial">
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group api_display_none">
                        <label for="pquantity" class="col-sm-4 control-label"><?=lang('quantity')?></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control api_numberic_input " id="pquantity"  onclick="this.select();">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="punit" class="col-sm-4 control-label"><?= lang('product_unit') ?></label>
                        <div class="col-sm-8">
                            <div id="punits-div"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="poption" class="col-sm-4 control-label"><?=lang('product_option')?></label>
                        <div class="col-sm-8">
                            <div id="poptions-div"></div>
                        </div>
                    </div>
                    <?php if ($Settings->product_discount && ($Owner || $Admin || $this->session->userdata('allow_discount'))) { ?>
                        <div class="form-group">
                            <label for="pdiscount" class="col-sm-4 control-label"><?=lang('product_discount')?></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="pdiscount" onclick="this.select();" >
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="pprice" class="col-sm-4 control-label"><?=lang('unit_price')?></label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control api_numberic_input" id="pprice" <?= ($Owner || $Admin || $GP['edit_price']) ? '' : 'readonly'; ?> onclick="this.select();">
                            
                        </div>
                    </div>
                    <input type="hidden" id="vi_id">
    
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th style="width:25%;"><?=lang('net_unit_price');?></th>
                            <th style="width:25%"><span id="net_price"></span></th>
                            <th style="width:25%; display:none"><?=lang('product_tax');?></th>
                            <th style="width:25%; display:none"><span id="pro_tax"></span></th>
                        </tr>
                    </table>
                    <input type="hidden" id="punit_price" value=""/>
                    <input type="hidden" id="old_tax" value=""/>
                    <input type="hidden" id="old_qty" value=""/>
                    <input type="hidden" id="old_price" value=""/>
                    <input type="hidden" id="row_id" value=""/>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="editItem"><?=lang('submit')?></button>
                
            </div>
        </div>
    </div>
</div>

<div class="modal fade in" id="gcModal" tabindex="-1" role="dialog" aria-labelledby="mModalLabel" aria-hidden="true" style="top: -10px; !important;">
    <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                        class="fa fa-2x">&times;</i></button>
                <h4 class="modal-title" id="myModalLabel"><?=lang('sell_gift_card');?></h4>
            </div>
            <div class="modal-body">
                <p><?=lang('enter_info');?></p>

                <div class="alert alert-danger gcerror-con" style="display: none;">
                    <button data-dismiss="alert" class="close" type="button">×</button>
                    <span id="gcerror"></span>
                </div>
                <div class="form-group">
                    <?=lang("card_no", "gccard_no");?> *
                    <div class="input-group">
                        <?php echo form_input('gccard_no', '', 'class="form-control" id="gccard_no"'); ?>
                        <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                            <a href="#" id="genNo"><i class="fa fa-cogs"></i></a>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="gcname" value="<?=lang('gift_card')?>" id="gcname"/>

                <div class="form-group">
                    <?=lang("value", "gcvalue");?> *
                    <?php echo form_input('gcvalue', '', 'class="form-control" id="gcvalue"'); ?>
                </div>
                <div class="form-group">
                    <?=lang("price", "gcprice");?> *
                    <?php echo form_input('gcprice', '', 'class="form-control" id="gcprice"'); ?>
                </div>
                <div class="form-group">
                    <?=lang("customer", "gccustomer");?>
                    <?php echo form_input('gccustomer', '', 'class="form-control" id="gccustomer"'); ?>
                </div>
                <div class="form-group">
                    <?=lang("expiry_date", "gcexpiry");?>
                    <?php echo form_input('gcexpiry', $this->sma->hrsd(date("Y-m-d", strtotime("+2 year"))), 'class="form-control date" id="gcexpiry"'); ?>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" id="addGiftCard" class="btn btn-primary"><?=lang('sell_gift_card')?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade in" id="mModal" tabindex="-1" role="dialog" aria-labelledby="mModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                            class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span></button>
                <h4 class="modal-title" id="mModalLabel"><?=lang('add_product_manually')?></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="mcode" class="col-sm-4 control-label"><?=lang('product_code')?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control kb-text" id="mcode">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mname" class="col-sm-4 control-label"><?=lang('product_name')?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control kb-text" id="mname">
                        </div>
                    </div>
                    <?php if ($Settings->tax1) {
                        ?>
                        <div class="form-group">
                            <label for="mtax" class="col-sm-4 control-label"><?=lang('product_tax')?> *</label>

                            <div class="col-sm-8">
                                <?php
                                    $tr[""] = "";
                        foreach ($tax_rates as $tax) {
                            $tr[$tax->id] = $tax->name;
                        }
                        echo form_dropdown('mtax', $tr, "", 'id="mtax" class="form-control pos-input-tip" style="width:100%;"'); ?>
                            </div>
                        </div>
                    <?php
                    }
                    ?>
                    <div class="form-group">
                        <label for="mquantity" class="col-sm-4 control-label"><?=lang('quantity')?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control kb-pad" id="mquantity">
                        </div>
                    </div>
                    <?php if ($Settings->product_discount && ($Owner || $Admin || $this->session->userdata('allow_discount'))) {?>
                        <div class="form-group">
                            <label for="mdiscount"
                                   class="col-sm-4 control-label"><?=lang('product_discount')?></label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control kb-pad" id="mdiscount">
                            </div>
                        </div>
                    <?php }
                    ?>
                    <div class="form-group">
                        <label for="mprice" class="col-sm-4 control-label"><?=lang('unit_price')?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control kb-pad" id="mprice">
                        </div>
                    </div>
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th style="width:25%;"><?=lang('net_unit_price');?></th>
                            <th style="width:25%;"><span id="mnet_price"></span></th>
                            <th style="width:25%;"><?=lang('product_tax');?></th>
                            <th style="width:25%;"><span id="mpro_tax"></span></th>
                        </tr>
                    </table>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="addItemManually"><?=lang('submit')?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade in" id="sckModal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">
                <i class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span>
                </button>
                <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="window.print();">
                    <i class="fa fa-print"></i> <?= lang('print'); ?>
                </button>
                <h4 class="modal-title" id="mModalLabel"><?=lang('shortcut_keys')?></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <table class="table table-bordered table-striped table-condensed table-hover"
                       style="margin-bottom: 0px;">
                    <thead>
                    <tr>
                        <th><?=lang('shortcut_keys')?></th>
                        <th><?=lang('actions')?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><?=$pos_settings->focus_add_item?></td>
                        <td><?=lang('focus_add_item')?></td>
                    </tr>
                    <tr>
                        <td><?=$pos_settings->add_manual_product?></td>
                        <td><?=lang('add_manual_product')?></td>
                    </tr>
                    <tr>
                        <td><?=$pos_settings->customer_selection?></td>
                        <td><?=lang('customer_selection')?></td>
                    </tr>
                    <tr>
                        <td><?=$pos_settings->add_customer?></td>
                        <td><?=lang('add_customer')?></td>
                    </tr>
                    <tr>
                        <td><?=$pos_settings->toggle_category_slider?></td>
                        <td><?=lang('toggle_category_slider')?></td>
                    </tr>
                    <tr>
                        <td><?=$pos_settings->toggle_subcategory_slider?></td>
                        <td><?=lang('toggle_subcategory_slider')?></td>
                    </tr>
                    <tr>
                        <td><?=$pos_settings->cancel_sale?></td>
                        <td><?=lang('cancel_sale')?></td>
                    </tr>
                    <tr>
                        <td><?=$pos_settings->suspend_sale?></td>
                        <td><?=lang('suspend_sale')?></td>
                    </tr>
                    <tr>
                        <td><?=$pos_settings->print_items_list?></td>
                        <td><?=lang('print_items_list')?></td>
                    </tr>
                    <tr>
                        <td><?=$pos_settings->finalize_sale?></td>
                        <td><?=lang('finalize_sale')?></td>
                    </tr>
                    <tr>
                        <td><?=$pos_settings->today_sale?></td>
                        <td><?=lang('today_sale')?></td>
                    </tr>
                    <tr>
                        <td><?=$pos_settings->open_hold_bills?></td>
                        <td><?=lang('open_hold_bills')?></td>
                    </tr>
                    <tr>
                        <td><?=$pos_settings->close_register?></td>
                        <td><?=lang('close_register')?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade in" id="dsModal" tabindex="-1" role="dialog" aria-labelledby="dsModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="fa fa-2x">&times;</i>
                </button>
                <h4 class="modal-title" id="dsModalLabel"><?=lang('edit_order_discount');?></h4>
                <div id="order_discount_error" style="font-weight: bold;color:red;"></div>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <?=lang("order_discount", "order_discount_input");?>
                    <?php echo form_input('order_discount_input', '', 'class="form-control" onclick="this.select();" id="order_discount_input"'); ?>
                    
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" id="updateOrderDiscount" class="btn btn-primary"><?=lang('update')?></button>
            </div>

<!-- New version -->
            
        </div>
    </div>
</div>

<div class="modal fade in" id="sModal" tabindex="-1" role="dialog" aria-labelledby="sModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="fa fa-2x">&times;</i>
                </button>
                <h4 class="modal-title" id="sModalLabel"><?=lang('shipping');?></h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <?=lang("shipping", "shipping_input");?>
                    <?php echo form_input('shipping_input', '', 'class="form-control" id="shipping_input"'); ?>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" id="updateShipping" class="btn btn-primary"><?=lang('update')?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade in" id="txModal" tabindex="-1" role="dialog" aria-labelledby="txModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-sm" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                        class="fa fa-2x">&times;</i></button>
                <h4 class="modal-title" id="txModalLabel"><?=lang('edit_order_tax');?></h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <?=lang("order_tax", "order_tax_input");?>
<?php
    $tr[""] = "";
    foreach ($tax_rates as $tax) {
        $tr[$tax->id] = $tax->name;
    }
    echo form_dropdown('order_tax_input', $tr, "", 'id="order_tax_input" class="form-control pos-input-tip" style="width:100%;"');
?>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" id="updateOrderTax" class="btn btn-primary"><?=lang('update')?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade in" id="susModal" tabindex="-1" role="dialog" aria-labelledby="susModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                        class="fa fa-2x">&times;</i></button>
                <h4 class="modal-title" id="susModalLabel"><?=lang('suspend_sale');?></h4>
            </div>
            <div class="modal-body">
                <p><?=lang('type_reference_note');?></p>

                <div class="form-group">
                    <?=lang("reference_note", "reference_note");?>
                    <?= form_input('reference_note', (!empty($reference_note) ? $reference_note : ''), 'class="form-control kb-text" id="reference_note"'); ?>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" id="suspend_sale" class="btn btn-primary"><?=lang('submit')?></button>
            </div>
        </div>
    </div>
</div>
<div id="order_tbl"><span id="order_span"></span>
    <table id="order-table" class="prT table table-striped" style="margin-bottom:0;" width="100%"></table>
</div>
<div id="bill_tbl"><span id="bill_span"></span>
    <table id="bill-table" width="100%" class="prT table table-striped" style="margin-bottom:0;"></table>
    <table id="bill-total-table" class="prT table" style="margin-bottom:0;" width="100%"></table>
    <span id="bill_footer"></span>
</div>
<div class="modal fade in" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true"></div>
<div class="modal fade in" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2"
     aria-hidden="true"></div>
<div id="modal-loading" style="display: none;">
    <div class="blackbg"></div>
    <div class="loader"></div>
</div>
<?php unset($Settings->setting_id, $Settings->smtp_user, $Settings->smtp_pass, $Settings->smtp_port, $Settings->update, $Settings->reg_ver, $Settings->allow_reg, $Settings->default_email, $Settings->mmode, $Settings->timezone, $Settings->restrict_calendar, $Settings->restrict_user, $Settings->auto_reg, $Settings->reg_notification, $Settings->protocol, $Settings->mailpath, $Settings->smtp_crypto, $Settings->corn, $Settings->customer_group, $Settings->envato_username, $Settings->purchase_code);?>
<script type="text/javascript">
var site = <?=json_encode(array('url' => base_url(), 'base_url' => admin_url('/'), 'assets' => $assets, 'settings' => $Settings, 'dateFormats' => $dateFormats))?>, pos_settings = <?=json_encode($pos_settings);?>;
var lang = {
    unexpected_value: '<?=lang('unexpected_value');?>',
    select_above: '<?=lang('select_above');?>',
    r_u_sure: '<?=lang('r_u_sure');?>',
    bill: '<?=lang('bill');?>',
    order: '<?=lang('order');?>',
    total: '<?=lang('total');?>',
    items: '<?=lang('items');?>',
    discount: '<?=lang('discount');?>',
    order_tax: '<?=lang('order_tax');?>',
    grand_total: '<?=lang('grand_total');?>',
    total_payable: '<?=lang('total_payable');?>',
    rounding: '<?=lang('rounding');?>',
    merchant_copy: '<?=lang('merchant_copy');?>'
};
</script>

<script type="text/javascript">
    var product_variant = 0, shipping = 0, p_page = 0, per_page = 0, tcp = "<?=$tcp?>", pro_limit = <?= $pos_settings->pro_limit; ?>,
        brand_id = 0, obrand_id = 0, cat_id = "<?=$pos_settings->default_category?>", ocat_id = "<?=$pos_settings->default_category?>", sub_cat_id = 0, osub_cat_id,
        count = 1, an = 1, DT = <?=$Settings->default_tax_rate?>,
        product_tax = 0, invoice_tax = 0, product_discount = 0, order_discount = 0, total_discount = 0, total = 0, total_paid = 0, grand_total = 0,
        KB = <?=$pos_settings->keyboard?>, tax_rates =<?php echo json_encode($tax_rates); ?>;
    var protect_delete = <?php if (!$Owner && !$Admin) {
    echo $pos_settings->pin_code ? '1' : '0';
} else {
    echo '0';
} ?>, billers = <?= json_encode($posbillers); ?>, biller = <?= json_encode($posbiller); ?>;
    var username = '<?=$this->session->userdata('username');?>', order_data = '', bill_data = '';

    function widthFunctions(e) {
        var wh = $(window).height(),
            lth = $('#left-top').height(),
            lbh = $('#left-bottom').height();
            // $('#item-list').css("height", wh - 140);
            // $('#item-list').css("min-height", 515);
            // $('#left-middle').css("height", wh - lth - lbh - 102);
            // $('#left-middle').css("height", wh - lth - lbh - 163);
            // $('#left-middle').css("min-height", 278); // 278
            // $('#product-list').css("height", wh - lth - lbh - 107);
            // $('#product-list').css("min-height", 90);
    }
    $(window).bind("resize", widthFunctions);
    $(document).ready(function () {
        $('#view-customer').click(function(){
            $('#myModal').modal({remote: site.base_url + 'customers/view/' + $("input[name=customer]").val()});
            $('#myModal').modal('show');
        });
        $('textarea').keydown(function (e) {
            if (e.which == 13) {
               var s = $(this).val();
               $(this).val(s+'\n').focus();
               e.preventDefault();
               return false;
            }
        });
        <?php if ($sid) { ?>
        localStorage.setItem('positems', JSON.stringify(<?=$items;?>));
        <?php } ?>

        <?php if ($oid) { ?>
        localStorage.setItem('positems', JSON.stringify(<?=$items;?>));
        <?php } ?>

<?php if ($this->session->userdata('remove_posls')) {?>
        if (localStorage.getItem('positems')) {
            localStorage.removeItem('positems');
        }
        if (localStorage.getItem('posdiscount')) {
            localStorage.removeItem('posdiscount');
        }
        if (localStorage.getItem('postax2')) {
            localStorage.removeItem('postax2');
        }
        if (localStorage.getItem('posshipping')) {
            localStorage.removeItem('posshipping');
        }
        if (localStorage.getItem('poswarehouse')) {
            localStorage.removeItem('poswarehouse');
        }
        if (localStorage.getItem('posnote')) {
            localStorage.removeItem('posnote');
        }
        if (localStorage.getItem('poscustomer')) {
            localStorage.removeItem('poscustomer');
        }
        if (localStorage.getItem('posbiller')) {
            localStorage.removeItem('posbiller');
        }
        if (localStorage.getItem('poscurrency')) {
            localStorage.removeItem('poscurrency');
        }
        if (localStorage.getItem('posnote')) {
            localStorage.removeItem('posnote');
        }
        if (localStorage.getItem('staffnote')) {
            localStorage.removeItem('staffnote');
        }
        <?php $this->sma->unset_data('remove_posls');}
        ?>
        widthFunctions();
        <?php if ($suspend_sale) {?>
        localStorage.setItem('postax2', '<?=$suspend_sale->order_tax_id;?>');
        localStorage.setItem('posdiscount', '<?=$suspend_sale->order_discount_id;?>');
        localStorage.setItem('poswarehouse', '<?=$suspend_sale->warehouse_id;?>');
        localStorage.setItem('poscustomer', '<?=$suspend_sale->customer_id;?>');
        localStorage.setItem('posbiller', '<?=$suspend_sale->biller_id;?>');
        localStorage.setItem('posshipping', '<?=$suspend_sale->shipping;?>');
        <?php }
        ?>
        <?php if ($old_sale) {?>
        localStorage.setItem('postax2', '<?=$old_sale->order_tax_id;?>');
        localStorage.setItem('posdiscount', '<?=$old_sale->order_discount_id;?>');
        localStorage.setItem('poswarehouse', '<?=$old_sale->warehouse_id;?>');
        localStorage.setItem('poscustomer', '<?=$old_sale->customer_id;?>');
        localStorage.setItem('posbiller', '<?=$old_sale->biller_id;?>');
        localStorage.setItem('posshipping', '<?=$old_sale->shipping;?>');
        <?php }
        ?>
<?php if ($this->input->get('customer')) {?>
        if (!localStorage.getItem('positems')) {
            localStorage.setItem('poscustomer', <?=$this->input->get('customer');?>);
        } else if (!localStorage.getItem('poscustomer')) {
            localStorage.setItem('poscustomer', <?=$customer->id;?>);
        }
        <?php } else {?>
        if (!localStorage.getItem('poscustomer')) {
            localStorage.setItem('poscustomer', <?=$customer->id;?>);
        }
        <?php }
        ?>
        if (!localStorage.getItem('postax2')) {
            localStorage.setItem('postax2', <?=$Settings->default_tax_rate2;?>);
        }
        $('.select').select2({minimumResultsForSearch: 7});
        // var customers = [{
        //     id: <?=$customer->id;?>,
        //     text: '<?=$customer->company == '-' ? $customer->name : $customer->company;?>'
        // }];
        $('#poscustomer').val(localStorage.getItem('poscustomer')).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: "<?=admin_url('customers/getCustomer')?>/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'No Match Found'}]};
                    }
                }
            }
        });
        if (KB) {
            display_keyboards();

            var result = false, sct = '';
            $('#poscustomer').on('select2-opening', function () {
                sct = '';
                $('.select2-input').addClass('kb-text');
                display_keyboards();
                $('.select2-input').bind('change.keyboard', function (e, keyboard, el) {
                    if (el && el.value != '' && el.value.length > 0 && sct != el.value) {
                        sct = el.value;
                    }
                    if(!el && sct.length > 0) {
                        $('.select2-input').addClass('select2-active');
                        setTimeout(function() {
                            $.ajax({
                                type: "get",
                                async: false,
                                url: "<?=admin_url('customers/suggestions')?>/?term=" + sct,
                                dataType: "json",
                                success: function (res) {
                                    if (res.results != null) {
                                        $('#poscustomer').select2({data: res}).select2('open');
                                        $('.select2-input').removeClass('select2-active');
                                    } else {
                                        // bootbox.alert('no_match_found');
                                        $('#poscustomer').select2('close');
                                        $('#test').click();
                                    }
                                }
                            });
                        }, 500);
                    }
                });
            });

            $('#poscustomer').on('select2-close', function () {
                $('.select2-input').removeClass('kb-text');
                $('#test').click();
                $('select, .select').select2('destroy');
                $('select, .select').select2({minimumResultsForSearch: 7});
            });
            $(document).bind('click', '#test', function () {
                var kb = $('#test').keyboard().getkeyboard();
                kb.close();
            });

        }

        $(document).on('change', '#posbiller', function () {
            var sb = $(this).val();
            $.each(billers, function () {
                if(this.id == sb) {
                    biller = this;
                }
            });
            $('#biller').val(sb);
        });

        <?php for ($i = 1; $i <= 5; $i++) {?>
        $('#paymentModal').on('change', '#amount_<?=$i?>', function (e) {
            $('#amount_val_<?=$i?>').val($(this).val());
        });
        $('#paymentModal').on('blur', '#amount_<?=$i?>', function (e) {
            $('#amount_val_<?=$i?>').val($(this).val());
        });
        $('#paymentModal').on('select2-close', '#paid_by_<?=$i?>', function (e) {
            $('#paid_by_val_<?=$i?>').val($(this).val());
        });
        $('#paymentModal').on('change', '#pcc_no_<?=$i?>', function (e) {
            $('#cc_no_val_<?=$i?>').val($(this).val());
        });
        $('#paymentModal').on('change', '#pcc_holder_<?=$i?>', function (e) {
            $('#cc_holder_val_<?=$i?>').val($(this).val());
        });
        $('#paymentModal').on('change', '#gift_card_no_<?=$i?>', function (e) {
            $('#paying_gift_card_no_val_<?=$i?>').val($(this).val());
        });
        $('#paymentModal').on('change', '#pcc_month_<?=$i?>', function (e) {
            $('#cc_month_val_<?=$i?>').val($(this).val());
        });
        $('#paymentModal').on('change', '#pcc_year_<?=$i?>', function (e) {
            $('#cc_year_val_<?=$i?>').val($(this).val());
        });
        $('#paymentModal').on('change', '#pcc_type_<?=$i?>', function (e) {
            $('#cc_type_val_<?=$i?>').val($(this).val());
        });
        $('#paymentModal').on('change', '#pcc_cvv2_<?=$i?>', function (e) {
            $('#cc_cvv2_val_<?=$i?>').val($(this).val());
        });
        $('#paymentModal').on('change', '#cheque_no_<?=$i?>', function (e) {
            $('#cheque_no_val_<?=$i?>').val($(this).val());
        });
        $('#paymentModal').on('change', '#payment_note_<?=$i?>', function (e) {
            $('#payment_note_val_<?=$i?>').val($(this).val());
        });
        <?php } ?>
        // aba qr code
        $('#paymentModal').on('change', '#aba_qr_transfer_reference_number', function (e) {
            $('#aba_qr_transfer_reference_number_val').val($(this).val());
        });
        // aba
        $('#paymentModal').on('change', '#transfer_reference_number', function (e) {
            $('#transfer_reference_number_val').val($(this).val());
        });
        $('#paymentModal').on('change', '#customer_account_number', function (e) {
            $('#customer_account_number_val').val($(this).val());
        });
        $('#paymentModal').on('change', '#company_account_number', function (e) {
            $('#company_account_number_val').val($(this).val());
        });
        $('#paymentModal').on('change', '#transfer_amount', function (e) {
            $('#transfer_amount_val').val($(this).val());
        });
        // wing
        $('#paymentModal').on('change', '#wing_transaction_id', function (e) {
            $('#wing_transaction_id_val').val($(this).val());
        });
        // pipay
        $('#paymentModal').on('change', '#pipay_transaction_id', function (e) {
            $('#pipay_transaction_id_val').val($(this).val());
        });
        
        $('#payment').click(function () {
            <?php if ($sid) {?>
            suspend = $('<span></span>');
            suspend.html('<input type="hidden" name="delete_id" value="<?php echo $sid; ?>" />');
            suspend.appendTo("#hidesuspend");
            <?php }
            ?>
            //var twt = formatDecimal((total + invoice_tax) - order_discount + shipping);
            var twt = document.getElementById('hide_total_payable').innerHTML;
            var am = document.getElementById('amount_1_v1').value;
            var sh = document.getElementById('shipping_price_v2').value;
            var sum = parseFloat(am) + parseFloat(sh);
            document.getElementById('amount_1_v2').value = sum;
            var cashier = $('#api_cashier').val();
            if (count == 1) {
                bootbox.alert('<?=lang('x_total');?>');
                return false;
            }
            gtotal = formatDecimal(twt);
            <?php if ($pos_settings->rounding) {?>
                round_total = roundNumber(gtotal, <?=$pos_settings->rounding?>);
                var rounding = formatDecimal(0 - (gtotal - round_total));
                $('#twt').text(formatMoney(round_total) + ' (' + formatMoney(rounding) + ')');
                $('#quick-payable').text(round_total);
                <?php } else {?>
                $('#twt').text(formatMoney(gtotal));
                $('#quick-payable').text(gtotal);
            <?php }
            ?>
            $('#item_count').text(count - 1);
            $('#paymentModal').appendTo("body").modal('show');
            $('#amount_1').focus();
            $('#amount_1_v1').val((document.getElementById('hide_total_payable').innerHTML));
            $('#amount_1_v2').val(formatMoney(document.getElementById('hide_total_payable').innerHTML));
            //$("#shipping_price").keyup();
            $("#shipping_price").change();
            $('#add_ons_cashier').val(cashier);

        });
        $('#paymentModal').on('show.bs.modal', function(e) {
            $('#submit-sale').text('<?=lang('submit');?>').attr('disabled', false);
        });
        $('#paymentModal').on('shown.bs.modal', function(e) {
            $('#amount_1').focus().val(0);
            $('#quick-payable').click();
        });
        var pi = 'amount_1', pa = 2;
        $(document).on('click', '.quick-cash', function () {
            if ($('#quick-payable').find('span.badge').length) {
                $('#clear-cash-notes').click();
            }
            var $quick_cash = $(this);
            var amt = $quick_cash.contents().filter(function () {
                return this.nodeType == 3;
            }).text();
            var th = ',';
            var $pi = $('#' + pi);
            amt = formatDecimal(amt.split(th).join("")) * 1 + $pi.val() * 1;
            $pi.val(formatDecimal(amt)).focus();
            var note_count = $quick_cash.find('span');
            if (note_count.length == 0) {
                $quick_cash.append('<span class="badge">1</span>');
            } else {
                note_count.text(parseInt(note_count.text()) + 1);
            }
        });
        $(document).on('click', '#quick-payable', function () {
            $('#clear-cash-notes').click();
            $(this).append('<span class="badge">1</span>');
            $('#amount_1').val(grand_total);
        });
        $(document).on('click', '#clear-cash-notes', function () {
            $('.quick-cash').find('.badge').remove();
            $('#' + pi).val('0').focus();
        });

        $(document).on('change', '.gift_card_no', function () {
            var cn = $(this).val() ? $(this).val() : '';
            var payid = $(this).attr('id'),
                id = payid.substr(payid.length - 1);
            if (cn != '') {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "sales/validate_gift_card/" + cn,
                    dataType: "json",
                    success: function (data) {
                        if (data === false) {
                            $('#gift_card_no_' + id).parent('.form-group').addClass('has-error');
                            bootbox.alert('<?=lang('incorrect_gift_card')?>');
                        } else if (data.customer_id !== null && data.customer_id !== $('#poscustomer').val()) {
                            $('#gift_card_no_' + id).parent('.form-group').addClass('has-error');
                            bootbox.alert('<?=lang('gift_card_not_for_customer')?>');
                        } else {
                            $('#gc_details_' + id).html('<small>Card No: ' + data.card_no + '<br>Value: ' + data.value + ' - Balance: ' + data.balance + '</small>');
                            $('#gift_card_no_' + id).parent('.form-group').removeClass('has-error');
                            //calculateTotals();
                            $('#amount_' + id).val(gtotal >= data.balance ? data.balance : gtotal).focus();
                        }
                    }
                });
            }
        });

        $(document).on('click', '.addButton', function () {
            if (pa <= 5) {
                $('#paid_by_1, #pcc_type_1').select2('destroy');
                var phtml = $('#payments').html(),
                    update_html = phtml.replace(/_1/g, '_' + pa);
                pi = 'amount_' + pa;
                $('#multi-payment').append('<button type="button" class="close close-payment" style="margin: -10px 0px 0 0;"><i class="fa fa-2x">&times;</i></button>' + update_html);
                $('#paid_by_1, #pcc_type_1, #paid_by_' + pa + ', #pcc_type_' + pa).select2({minimumResultsForSearch: 7});
                read_card();
                pa++;
            } else {
                bootbox.alert('<?=lang('max_reached')?>');
                return false;
            }
            if (KB) { display_keyboards(); }
            $('#paymentModal').css('overflow-y', 'scroll');
        });

        $(document).on('click', '.close-payment', function () {
            $(this).next().remove();
            $(this).remove();
            pa--;
        });

        $(document).on('focus', '.amount', function () {
            pi = $(this).attr('id');
            calculateTotals();
        }).on('blur', '.amount', function () {
            calculateTotals();
        });

        function calculateTotals() {
            var total_paying = 0;
            var ia = $(".amount");
            $.each(ia, function (i) {
                var this_amount = formatCNum($(this).val() ? $(this).val() : 0);
                total_paying += parseFloat(this_amount);
            });            
            $('#total_paying').text(formatMoney(total_paying));
            <?php if ($pos_settings->rounding) {?>
            $('#balance').text(formatMoney(total_paying - round_total));
            $('#balance_' + pi).val(formatDecimal(total_paying - round_total));
            total_paid = total_paying;
            grand_total = round_total;
            <?php } else {?>
            $('#balance').text(formatMoney(total_paying - gtotal));
            $('#balance_' + pi).val(formatDecimal(total_paying - gtotal));
            total_paid = total_paying;
            grand_total = gtotal;
            <?php }
            ?>
        }

        $("#add_item").autocomplete({
            source: function (request, response) {
                if (!$('#poscustomer').val()) {
                    $('#add_item').val('').removeClass('ui-autocomplete-loading');
                    bootbox.alert('<?=lang('select_above');?>');
                    //response('');
                    $('#add_item').focus();
                    return false;
                }
                $.ajax({
                    type: 'get',
                    url: '<?=admin_url('sales/suggestions');?>',
                    dataType: "json",
                    data: {
                        term: request.term,
                        warehouse_id: $("#poswarehouse").val(),
                        customer_id: $("#poscustomer").val()
                    },
                    success: function (data) {
                        $(this).removeClass('ui-autocomplete-loading');
                        response(data);
                    }
                });
            },
            minLength: 1,
            autoFocus: false,
            delay: 250,
            response: function (event, ui) {
                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    bootbox.alert('<?=lang('no_match_found')?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).val('');
                }
                else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                }
                else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    bootbox.alert('<?=lang('no_match_found')?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).val('');

                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {
                    var row = add_invoice_item(ui.item);
                    if (row)
                        $(this).val('');
                } else {
                    bootbox.alert('<?=lang('no_match_found')?>');
                }
            }
        });

        <?php if ($pos_settings->tooltips) {
                echo '$(".pos-tip").tooltip();';
            }
        ?>
        // $('#posTable').stickyTableHeaders({fixedOffset: $('#product-list')});
        $('#posTable').stickyTableHeaders({scrollableArea: $('#product-list')});
        //$('#product-list, #category-list, #subcategory-list, #brands-list').perfectScrollbar({suppressScrollX: true});
        $('select, .select').select2({minimumResultsForSearch: 7});

        $(document).on('click', '.product', function (e) {
            $('#modal-loading').show();
            code = $(this).val(),
                wh = $('#poswarehouse').val(),
                cu = $('#poscustomer').val();
            $.ajax({
                type: "get",
                url: "<?=admin_url('pos/getProductDataByCode')?>",
                data: {code: code, warehouse_id: wh, customer_id: cu},
                dataType: "json",
                success: function (data) {
                    e.preventDefault();
                    if (data !== null) {
                        add_invoice_item(data);
                        $('#modal-loading').hide();
                    } else {
                        bootbox.alert('<?=lang('no_match_found')?>');
                        $('#modal-loading').hide();
                    }
                }
            });
        });

        $(document).on('click', '.category', function () {
            if (cat_id != $(this).val()) {
                $('#open-category').click();
                $('#modal-loading').show();
                cat_id = $(this).val();
                $.ajax({
                    type: "get",
                    url: "<?=admin_url('pos/ajaxcategorydata');?>",
                    data: {category_id: cat_id},
                    dataType: "json",
                    success: function (data) {
                        $('#item-list').empty();
                        var newPrs = $('<div></div>');
                        newPrs.html(data.products);
                        newPrs.appendTo("#item-list");
                        $('#subcategory-list').empty();
                        var newScs = $('<div></div>');
                        newScs.html(data.subcategories);
                        newScs.appendTo("#subcategory-list");
                        tcp = data.tcp;
                        nav_pointer();
                    }
                }).done(function () {
                    p_page = 'n';
                    $('#category-' + cat_id).addClass('active');
                    $('#category-' + ocat_id).removeClass('active');
                    ocat_id = cat_id;
                    $('#modal-loading').hide();
                    nav_pointer();
                });
            }
        });
        $('#category-' + cat_id).addClass('active');

        $(document).on('click', '.brand', function () {
            if (brand_id != $(this).val()) {
                $('#open-brands').click();
                $('#modal-loading').show();
                brand_id = $(this).val();
                $.ajax({
                    type: "get",
                    url: "<?=admin_url('pos/ajaxbranddata');?>",
                    data: {brand_id: brand_id},
                    dataType: "json",
                    success: function (data) {
                        $('#item-list').empty();
                        var newPrs = $('<div></div>');
                        newPrs.html(data.products);
                        newPrs.appendTo("#item-list");
                        tcp = data.tcp;
                        nav_pointer();
                    }
                }).done(function () {
                    p_page = 'n';
                    $('#brand-' + brand_id).addClass('active');
                    $('#brand-' + obrand_id).removeClass('active');
                    obrand_id = brand_id;
                    $('#category-' + cat_id).removeClass('active');
                    $('#subcategory-' + sub_cat_id).removeClass('active');
                    cat_id = 0; sub_cat_id = 0;
                    $('#modal-loading').hide();
                    nav_pointer();
                });
            }
        });

        $(document).on('click', '.subcategory', function () {
            if (sub_cat_id != $(this).val()) {
                $('#open-subcategory').click();
                $('#modal-loading').show();
                sub_cat_id = $(this).val();
                $.ajax({
                    type: "get",
                    url: "<?=admin_url('pos/ajaxproducts');?>",
                    data: {category_id: cat_id, subcategory_id: sub_cat_id, per_page: p_page != 0 ? p_page : 'n' },
                    dataType: "html",
                    success: function (data) {
                        $('#item-list').empty();
                        var newPrs = $('<div></div>');
                        newPrs.html(data);
                        newPrs.appendTo("#item-list");
                    }
                }).done(function () {
                    p_page = 'n';
                    $('#subcategory-' + sub_cat_id).addClass('active');
                    $('#subcategory-' + osub_cat_id).removeClass('active');
                    $('#modal-loading').hide();
                });
            }
        });

        $('#next').click(function () {
            if (p_page == 'n') {
                p_page = 0
            }
            p_page = p_page + pro_limit;
            if (tcp >= pro_limit && p_page < tcp) {
                $('#modal-loading').show();
                $.ajax({
                    type: "get",
                    url: "<?=admin_url('pos/ajaxproducts');?>",
                    data: {category_id: cat_id, subcategory_id: sub_cat_id, per_page: p_page != 0 ? p_page : 'n'},
                    dataType: "html",
                    success: function (data) {
                        $('#item-list').empty();
                        var newPrs = $('<div></div>');
                        newPrs.html(data);
                        newPrs.appendTo("#item-list");
                        nav_pointer();
                    }
                }).done(function () {
                    $('#modal-loading').hide();
                });
            } else {
                p_page = p_page - pro_limit;
            }
        });

        $('#previous').click(function () {
            if (p_page == 'n') {
                p_page = 0;
            }
            if (p_page != 0) {
                $('#modal-loading').show();
                p_page = p_page - pro_limit;
                if (p_page == 0) {
                    p_page = 'n'
                }
                $.ajax({
                    type: "get",
                    url: "<?=admin_url('pos/ajaxproducts');?>",
                    data: {category_id: cat_id, subcategory_id: sub_cat_id, per_page: p_page != 0 ? p_page : 'n'},
                    dataType: "html",
                    success: function (data) {
                        $('#item-list').empty();
                        var newPrs = $('<div></div>');
                        newPrs.html(data);
                        newPrs.appendTo("#item-list");
                        nav_pointer();
                    }

                }).done(function () {
                    $('#modal-loading').hide();
                });
            }
        });
        $(document).on('change', '.paid_by', function () {
            var p_val = $(this).val();
            localStorage.setItem('paid_by', p_val);
            check_paid(p_val)
        });
        check_paid('');
        function check_paid(p_val){
            $('#rpaidby').val(p_val);
            var array_hide = ['.pcheque_1','.pcc_1','.pcash_1','.gc_1','#ppp-stripe','#aba_qr_bank_in_form','#api_bank_in_form','#api_pipay_form','#api_wing_form','#api_aba_daiki_form','#api_payway_form','#api_wing_daiki_form','#api_acode_daiki_form']
            for(var i=0;i<array_hide.length;i++){
                $(array_hide[i]).hide('fast');
            }
            if (p_val == 'cash') {
                $('.pcash_1').show();
                $('#amount_1').focus();
            }else if (p_val == 'gift_card') {
                $('.gc_1').show();
                $('#gift_card_no').focus();
            } else if (p_val == 'CC' || p_val == 'stripe' || p_val == 'ppp' || p_val == 'authorize') {
                if (p_val == 'CC') {
                    $('#ppp-stripe').hide();
                } else {
                    $('#ppp-stripe').show();
                }
                $('.pcc_1').show();
                $('#pcc_no_1').focus();
                $('#swipe_1').focus();
            } else if (p_val == 'Cheque') {
                $('.pcheque_1').show();
                $('#cheque_no_1').focus();
            } else if (p_val == 'aba'){
                $('#api_bank_in_form').show('fast');
                $('#transfer_amount').val($('#amount_1').val())
            } else if (p_val == 'aba_qr'){
                $('#aba_qr_bank_in_form').show('fast');
                $('#aba_qr_transfer_amount').val($('#amount_1').val())
                $('#aba_qr_transfer_reference_number').focus();
            } else if (p_val == 'acleda'){
                $('#api_bank_in_form').show('fast');
            } else if (p_val == 'pipay'){
                $('#api_pipay_form').show('fast');
                $('#pipay_transaction_id').focus();
            } else if (p_val == 'wing'){
                $('#api_wing_form').show('fast');
                $('#wing_transaction_id').focus();
            } else if (p_val == 'aba_daiki'){
                $('#api_aba_daiki_form').show('fast');
                $('#aba_daiki_transaction_id').focus();
            } 
            else if (p_val == 'payway') {
                $('#api_payway_form').show('fast');
                $('#payway_transaction_id').focus();
            }
            else if (p_val == 'wing_daiki') {
                $('#api_wing_daiki_form').show('fast');
                $('#wing_daiki_transaction_id').focus();
            }
            else if (p_val == 'acode_daiki') {
                $('#api_acode_daiki_form').show('fast');
                $('#acode_daiki_transaction_id').focus();
            }

        }

        $(document).on('click', '#submit-sale', function () {
            $('#temp_customer_pay_usd').val(document.getElementById('customer_pay_usd').value);
            $('#temp_customer_pay_khr').val(document.getElementById('customer_pay_khr').value);
            
            $("#amount_1").val(document.getElementById('hide_total_payable').innerHTML);
            total_paid = $("#amount_1").val();
            //total_paid = 5;
            var cashier_name = $('#api_cashier').val();
            $('#add_ons_cashier').val(cashier_name);

            if (total_paid == 0 || total_paid < grand_total) {
                bootbox.confirm("<?= lang('paid_l_t_payable');?>", function (res) {                    
                    if (res == true) {
                        $('#pos_note').val(localStorage.getItem('posnote'));
                        $('#staff_note').val(localStorage.getItem('staffnote'));
                        $('#submit-sale').text('<?=lang('loading');?>').attr('disabled', true);
                        $('#pos-sale-form').submit();
                    }
                });
                return false;
            } else {

                temp = $('#aba_qr_transfer_reference_number').val();
                $('#add_ons_aba_qr_transfer_reference_number_form').val(temp);

                temp = $('#wing_transaction_id').val();
                $('#add_ons_wing_transaction_id_form').val(temp);

                temp = $('#pipay_transaction_id').val();
                $('#add_ons_pipay_transaction_id_form').val(temp);

                temp = $('#aba_daiki_transaction_id').val();
                $('#add_ons_aba_daiki_transaction_id_form').val(temp);

                temp = $('#customer_account_number').val();
                $('#add_ons_customer_account_number_form').val(temp);

                temp = $('#company_account_number').val();
                $('#add_ons_company_account_number_form').val(temp);

                temp = $('#transfer_amount').val();
                $('#add_ons_transfer_amount_form').val(temp);

                temp = $('#transfer_reference_number').val();
                $('#add_ons_transfer_reference_number_form').val(temp);

                temp = $('#payway_transaction_id').val();
                $('#add_ons_payway_transaction_id_form').val(temp);

                temp = $('#wing_daiki_transaction_id').val();
                $('#add_ons_wing_daiki_transaction_id_form').val(temp);

                temp = $('#acode_daiki_transaction_id').val();
                $('#add_ons_acode_daiki_transaction_id_form').val(temp);

                $('#pos_note').val(localStorage.getItem('posnote'));
                $('#staff_note').val(localStorage.getItem('staffnote'));
                $(this).text('<?=lang('loading');?>').attr('disabled', true);
                $('#pos-sale-form').submit();
            }
        });
        $('#suspend').click(function () {
            if (count <= 1) {
                bootbox.alert('<?=lang('x_suspend');?>');
                return false;
            } else {
                $('#susModal').modal();
            }
        });
        $('#suspend_sale').click(function () {
            ref = $('#reference_note').val();
            if (!ref || ref == '') {
                bootbox.alert('<?=lang('type_reference_note');?>');
                return false;
            } else {
                suspend = $('<span></span>');
                <?php if ($sid) {?>
                suspend.html('<input type="hidden" name="delete_id" value="<?php echo $sid; ?>" /><input type="hidden" name="suspend" value="yes" /><input type="hidden" name="suspend_note" value="' + ref + '" />');
                <?php } else {?>
                suspend.html('<input type="hidden" name="suspend" value="yes" /><input type="hidden" name="suspend_note" value="' + ref + '" />');
                <?php }
                ?>
                suspend.appendTo("#hidesuspend");
                $('#total_items').val(count - 1);
                $('#pos-sale-form').submit();

            }
        });
    });

    $(document).ready(function () {
        $('#print_order').click(function () {
            if (count == 1) {
                bootbox.alert('<?=lang('x_total');?>');
                return false;
            }
            <?php if ($pos_settings->remote_printing != 1) { ?>
                printOrder();
            <?php } else { ?>
                Popup($('#order_tbl').html());
            <?php } ?>
        });
        $('#print_bill').click(function () {
            if (count == 1) {
                bootbox.alert('<?=lang('x_total');?>');
                return false;
            }
            <?php if ($pos_settings->remote_printing != 1) { ?>
                printBill();
            <?php } else { ?>
                Popup($('#bill_tbl').html());
            <?php } ?>
        });
    });

    $(function () {
        $(".alert").effect("shake");
        setTimeout(function () {
            $(".alert").hide('blind', {}, 500)
        }, 15000);
        <?php if ($pos_settings->display_time) {?>
        var now = new moment();
        $('#display_time').text(now.format((site.dateFormats.js_sdate).toUpperCase() + " HH:mm"));
        setInterval(function () {
            var now = new moment();
            $('#display_time').text(now.format((site.dateFormats.js_sdate).toUpperCase() + " HH:mm"));
        }, 1000);
        <?php }
        ?>
    });
    <?php if ($pos_settings->remote_printing == 1) { ?>
    function Popup(data) {
        var mywindow = window.open('', 'sma_pos_print', 'height=500,width=300');
        mywindow.document.write('<html><head><title>Print</title>');
        mywindow.document.write('<link rel="stylesheet" href="<?=$assets?>styles/helpers/bootstrap.min.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');
        mywindow.print();
        mywindow.close();
        return true;
    }
    <?php }
    ?>
</script>
<?php
    $s2_lang_file = read_file('./assets/config_dumps/s2_lang.js');
    foreach (lang('select2_lang') as $s2_key => $s2_line) {
        $s2_data[$s2_key] = str_replace(array('{', '}'), array('"+', '+"'), $s2_line);
    }
    $s2_file_date = $this->parser->parse_string($s2_lang_file, $s2_data, true);
?>
<script type="text/javascript" src="<?=$assets?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?=$assets?>js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?=$assets?>js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="<?=$assets?>js/select2.min.js"></script>
<script type="text/javascript" src="<?=$assets?>js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?=$assets?>js/custom.js"></script>
<script type="text/javascript" src="<?=$assets?>js/jquery.calculator.min.js"></script>
<script type="text/javascript" src="<?=$assets?>js/bootstrapValidator.min.js"></script>
<script type="text/javascript" src="<?=$assets?>pos/js/plugins.min.js"></script>
<script type="text/javascript" src="<?=$assets?>pos/js/parse-track-data.js"></script>
<script type="text/javascript" src="<?=$assets?>pos/js/pos.ajax.js"></script>
<?php
if (! $pos_settings->remote_printing) {
    ?>
    <script type="text/javascript">
        var order_printers = <?= json_encode($order_printers); ?>;
        function printOrder() {
            $.each(order_printers, function() {
                var socket_data = { 'printer': this,
                'logo': (biller && biller.logo ? biller.logo : ''),
                'text': order_data };
                $.get('<?= admin_url('pos/p/order'); ?>', {data: JSON.stringify(socket_data)});
            });
            return false;
        }

        function printBill() {
            var socket_data = {
                'printer': <?= json_encode($printer); ?>,
                'logo': (biller && biller.logo ? biller.logo : ''),
                'text': bill_data
            };
            $.get('<?= admin_url('pos/p'); ?>', {data: JSON.stringify(socket_data)});
            return false;
        }
    </script>
    <?php
} elseif ($pos_settings->remote_printing == 2) {
        ?>
    <script src="<?= $assets ?>js/socket.io.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        socket = io.connect('http://localhost:6440', {'reconnection': false});

        function printBill() {
            if (socket.connected) {
                var socket_data = {'printer': <?= json_encode($printer); ?>, 'text': bill_data};
                socket.emit('print-now', socket_data);
                return false;
            } else {
                bootbox.alert('<?= lang('pos_print_error'); ?>');
                return false;
            }
        }

        var order_printers = <?= json_encode($order_printers); ?>;
        function printOrder() {
            if (socket.connected) {
                $.each(order_printers, function() {
                    var socket_data = {'printer': this, 'text': order_data};
                    socket.emit('print-now', socket_data);
                });
                return false;
            } else {
                bootbox.alert('<?= lang('pos_print_error'); ?>');
                return false;
            }
        }
    </script>
    <?php
    } elseif ($pos_settings->remote_printing == 3) {
        ?>
    <script type="text/javascript">
        try {
            socket = new WebSocket('ws://127.0.0.1:6441');
            socket.onopen = function () {
                console.log('Connected');
                return;
            };
            socket.onclose = function () {
                console.log('Not Connected');
                return;
            };
        } catch (e) {
            console.log(e);
        }

        var order_printers = <?= $pos_settings->local_printers ? "''" : json_encode($order_printers); ?>;
        function printOrder() {
            if (socket.readyState == 1) {

                if (order_printers == '') {

                    var socket_data = { 'printer': false, 'order': true,
                    'logo': (biller && biller.logo ? site.url+'assets/uploads/logos/'+biller.logo : ''),
                    'text': order_data };
                    socket.send(JSON.stringify({type: 'print-receipt', data: socket_data}));

                } else {

                $.each(order_printers, function() {
                    var socket_data = { 'printer': this,
                    'logo': (biller && biller.logo ? site.url+'assets/uploads/logos/'+biller.logo : ''),
                    'text': order_data };
                    socket.send(JSON.stringify({type: 'print-receipt', data: socket_data}));
                });

            }
                return false;
            } else {
                bootbox.alert('<?= lang('pos_print_error'); ?>');
                return false;
            }
        }

        function printBill() {
            if (socket.readyState == 1) {
                var socket_data = {
                    'printer': <?= $pos_settings->local_printers ? "''" : json_encode($printer); ?>,
                    'logo': (biller && biller.logo ? site.url+'assets/uploads/logos/'+biller.logo : ''),
                    'text': bill_data
                };
                socket.send(JSON.stringify({type: 'print-receipt', data: socket_data}));
                return false;
            } else {
                bootbox.alert('<?= lang('pos_print_error'); ?>');
                return false;
            }
        }
    </script>
    <?php
    }
?>
<script type="text/javascript">
$('.sortable_table tbody').sortable({
    containerSelector: 'tr'
});
</script>
<script type="text/javascript" charset="UTF-8"><?=$s2_file_date?></script>
<div id="ajaxCall"><i class="fa fa-spinner fa-pulse"></i></div>
<?php
if (isset($print) && !empty($print)) {
    /* include FCPATH.'themes'.DIRECTORY_SEPARATOR.$Settings->theme.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'pos'.DIRECTORY_SEPARATOR.'remote_printing.php'; */
    include 'remote_printing.php';
}
?>
<script>
    if (site.url == 'https://order.daishintc.com/') {
        $('link[href="'+site.assets+'styles/red.css"]').attr('disabled', 'disabled');
        $('link[href="'+site.assets+'styles/red.css"]').remove();
        $('<link>')
        .appendTo('head')
        .attr({type: 'text/css', rel: 'stylesheet'})
        .attr('href', site.assets+'styles/red.css');
    }
    else if (site.url == 'https://dev-invoice.camboinfo.com/') {
        $('link[href="'+site.assets+'styles/red.css"]').attr('disabled', 'disabled');
        $('link[href="'+site.assets+'styles/red.css"]').remove();
        $('<link>')
        .appendTo('head')
        .attr({type: 'text/css', rel: 'stylesheet'})
        .attr('href', ''+site.assets+'styles/blue.css');
    }        
    else if (site.url == 'https://phsarjapan.com/' || site.url == 'https://air.phsarjapan.com/') {
        $('link[href="'+site.assets+'styles/light.css"]').attr('disabled', 'disabled');
        $('link[href="'+site.assets+'styles/light.css"]').remove();
        $('<link>')
        .appendTo('head')
        .attr({type: 'text/css', rel: 'stylesheet'})
        .attr('href', ''+site.assets+'styles/pink.css');     
    }
    else {
        $('link[href="'+site.assets+'styles/light.css"]').attr('disabled', 'disabled');
        $('link[href="'+site.assets+'styles/blue.css"]').attr('disabled', 'disabled');
        $('link[href="'+site.assets+'styles/green.css"]').attr('disabled', 'disabled');
        $('link[href="'+site.assets+'styles/light.css"]').remove();
        $('link[href="'+site.assets+'styles/blue.css"]').remove();
        $('link[href="'+site.assets+'styles/green.css"]').remove();
        $('link[href="'+site.assets+'styles/red.css"]').remove();                        
    }
</script>

<script>
    function get_amount_usd() {
        var temp = 0;
        var temp1 = document.getElementById('rate_currency').value;
        var temp2 = document.getElementById('total_khr').innerHTML;
        var total_khr = temp2.replace('(KHR)','');
        var total_payable = document.getElementById('hide_total_payable').innerHTML;
        
        // var temp3 = document.getElementById('tds').innerHTML;
        // var temp4 = temp3.split(' ');
        // var temp5 = parseFloat(temp4[1]).toFixed(2);
        // temp1 = parseFloat(temp1).toFixed(0);
        // temp2 = parseFloat(temp2).toFixed(2);
        
        // document.getElementById('amount_1').value = tt;
        // temp2 = temp2 - temp5;
        // var temp3 = temp1 * temp2;
        //temp3 = parseFloat(temp3).toFixed(0);
        // temp3 = temp3.toFixed(0).replace(/./g, function(c, i, a) {
        //     return i && c !== "." && ((a.length - i) % 3 === 0) ? "," + c : c;
        // });;

        document.getElementById('amount_khr').value = total_khr;  
        document.getElementById('amount_khr_v2').value = total_khr;   
        //document.getElementById('amount_1_v2').value = total_payable;
        
        <?php
            $config_data = array(
                'table_name' => 'sma_companies',
                'select_table' => 'sma_companies',
                'translate' => '',
                'select_condition' => "id = 1 ",
            );
            $select_paid_by = $this->site->api_select_data_v2($config_data);
        ?>
        if(document.getElementById('poscustomer').value == '')
            document.getElementById('add_ons_poscustomer_id').value = '<?php echo $select_paid_by[0]['customer_paid_by'];?>';
    }
    get_amount_usd();
    
    function amount_usd() {
        var get_total_payable = document.getElementById('hide_total_payable').innerHTML;
        get_total_payable = parseFloat(get_total_payable).toFixed(2);
    
        if(document.getElementById('amount_1').value == '') {
            document.getElementById('amount_1').value = get_total_payable;
        }

        var temp1 = document.getElementById('rate_currency').value;
        var temp2 = document.getElementById('amount_1').value;
        
        temp1 = parseFloat(temp1).toFixed(0);
        temp2 = parseFloat(temp2).toFixed(2);
        var temp3 = temp1 * temp2;
        
        document.getElementById('amount_khr').value = temp3;   
        
    }

    function amount_khr() {
        var get_khr = document.getElementById('total_khr').innerHTML;
        var replace = get_khr.replace('(KHR)','');
        var replace_2 = replace.replace(',','');
        if (document.getElementById('amount_khr').value == '')
            document.getElementById('amount_khr').value = replace_2;

        var temp1 = document.getElementById('rate_currency').value;
        var temp2 = document.getElementById('amount_khr').value;

        temp1 = parseFloat(temp1).toFixed(0);
        temp2 = parseFloat(temp2).toFixed(0);
        var temp3 = temp2 / temp1;
        total = temp3.toFixed(2);

        document.getElementById('amount_1').value = total; 
    }

function customer_pay_money() {
    if (document.getElementById('amount_1_v2').value <= 0)
        document.getElementById('amount_1_v2').value = 0;
        
    var amount_USD_v2 = document.getElementById('amount_1_v2').value;
    var customer_pay_usd_v2 = document.getElementById('customer_pay_usd').value;
    var customer_pay_khr_v2 = document.getElementById('customer_pay_khr').value;
    var temp1_v2 = document.getElementById('amount_1_v2').value;
    
    // input customer pay money to 
    document.getElementById('add_ons_customer_pay_usd_v2').value = customer_pay_usd_v2;
    document.getElementById('add_ons_customer_pay_khr_v2').value = customer_pay_khr_v2;
    
    if (customer_pay_usd_v2 == '')
        customer_pay_usd_v2 = 0;
    if (customer_pay_khr_v2 == '')
        customer_pay_khr_v2 = 0;
 
    // calculate : khr / rate = USD
    var get_rate_currency_v2 = 4100;
    var convert_khr_to_usd_v2 = parseFloat(customer_pay_khr_v2) / parseFloat(get_rate_currency_v2);     
    
    /*  calculator customer pay KHR with customer pay USD  */   
    var sub_total_v2 = parseFloat(convert_khr_to_usd_v2) + parseFloat(customer_pay_usd_v2); // sum : (khr / rate) + cus_pay_USD
    //document.getElementById('add_ons_customer_pay_khr_v2').value = sub_total_v2;

    if(sub_total_v2 >= amount_USD_v2)  {
        /*  calculator amount USD with customer pay USD  */   
        var total_v2 = parseFloat(sub_total_v2) - parseFloat(amount_USD_v2);  
        var temp1_v2 = '';
        document.getElementById('customer_change_usd').style.color = "black"; 
        document.getElementById('customer_change_khr').style.color = "black"; 
        document.getElementById('khmer_change').style.color = "black"; 
    }
    else {
        var total_v2 = parseFloat(amount_USD_v2) - parseFloat(sub_total_v2);  
        var temp1_v2 = '-';
        document.getElementById('customer_change_usd').style.color = "red"; 
        document.getElementById('customer_change_khr').style.color = "red"; 
        document.getElementById('khmer_change').style.color = "red"; 
    }   
    
    var num_v2 = total_v2;
    integr_v2 = Math.floor(num_v2);
    decimal_v2 = num_v2 - integr_v2; 
    
    temp6_v2 = decimal_v2 * parseFloat(get_rate_currency_v2);
    temp6_v2 = parseFloat(temp6_v2).toFixed(0);
    temp7_v2 = temp6_v2.replace(/./g, function(c, i, a) {
        return i && c !== "." && ((a.length - i) % 3 === 0) ? "," + c : c;
    });;
    
    document.getElementById('customer_change_usd').innerHTML = temp1_v2 + parseFloat(integr_v2).toFixed(2);      
    document.getElementById('customer_change_khr').innerHTML = temp1_v2 + temp7_v2;
    
    document.getElementById('add_ons_customer_change_usd_v2').value = temp1_v2 + integr_v2;  
    document.getElementById('add_ons_customer_change_khr_v2').value = temp1_v2 + temp7_v2;
    
    
    var temp8_v2 = document.getElementById('customer_change_usd').innerHTML;
    var temp9_v2 = document.getElementById('customer_change_khr').innerHTML;
    temp9_v2 = temp9_v2.replace(",","");
    var temp10_v2 = temp8_v2 * parseFloat(get_rate_currency_v2);
    temp10_v2 = parseInt(temp10_v2);
    temp9_v2 = parseInt(temp9_v2);
    var temp11_v2 = temp10_v2 + temp9_v2;
    temp11_v2 = parseFloat(temp11_v2).toFixed(0);
    document.getElementById('khmer_change').innerHTML = formatNumber(temp11_v2,0);
    if(document.getElementById('customer_change_usd').innerHTML == '-0.00'){
        document.getElementById('customer_change_usd').style.color = 'black';
        document.getElementById('customer_change_usd').innerHTML = '0.00';
    }
    if(document.getElementById('customer_change_khr').innerHTML == '-0'){
        document.getElementById('customer_change_khr').style.color = 'black';
        document.getElementById('customer_change_khr').innerHTML = '0';
    }

    

}

// $('#shipping_price').keyup(function() {
//     $('#shipping_price_v2').val(document.getElementById('shipping_price').value);
//     var amount_v2 = document.getElementById('amount_1_v1').value;
//     var get_khr = document.getElementById('total_khr').innerHTML;
//     var replace_khr = get_khr.replace('(KHR)','');
//     var get_total_payable = document.getElementById('hide_total_payable').innerHTML;
//         get_total_payable = parseFloat(get_total_payable).toFixed(2);
//     var shipping = document.getElementById('shipping_price').value;
    
//     var sum = '';
//     if (shipping == '') {
//         document.getElementById('shipping_price').value = 0;
//         document.getElementById('twt').innerHTML = formatMoney(get_total_payable);
//         document.getElementById('amount_1_v2').value = get_total_payable;
//         document.getElementById('amount_khr_v2').value = replace_khr;
//     } else {
//         sum = parseFloat(amount_v2) + parseFloat(shipping);
//         document.getElementById('amount_1_v2').value = sum;
//         document.getElementById('twt').innerHTML = formatMoney(sum);
//         var khr = sum * 4100;
//         document.getElementById('amount_khr_v2').value = khr;
//     }
//     customer_pay_money();
// });

$('#shipping_price').change(function() {
    // $('#shipping_price_v2').val(document.getElementById('shipping_price').value);
    var amount_v2 = document.getElementById('amount_1_v1').value;
    var get_khr = document.getElementById('total_khr').innerHTML;
    var replace_khr = get_khr.replace('(KHR)','');
    var get_total_payable = document.getElementById('hide_total_payable').innerHTML;
        get_total_payable = parseFloat(get_total_payable).toFixed(2);
    var shipping = document.getElementById('shipping_price').value;
    shipping = formatDecimal(shipping);
    $('#shipping_price_v2').val(shipping);

    var sh = parseFloat($('#shipping_price').val());
    sh = parseFloat(sh).toFixed(2);
    var tt_payable = document.getElementById('hide_total_payable').innerHTML;
    tt_payable = parseFloat(tt_payable).toFixed(2);
    if (document.getElementById('amount_1_v2').value == 'NaN')
    document.getElementById('amount_1_v2').value = 1;
    if (sh == 'NaN') {
        sh = '';
    }
    $('#shipping_price').val(sh);
    document.getElementById('shipping_price_hidden').value = shipping;
    
    var sum = '';
    if (document.getElementById('shipping_price_hidden').value == '') {
        document.getElementById('shipping_price_hidden').value = 0;
        document.getElementById('twt').innerHTML = formatMoney(get_total_payable);
        document.getElementById('amount_1_v2').value = get_total_payable;
        document.getElementById('amount_khr_v2').value = replace_khr;
    } else {
        sum = parseFloat(amount_v2) + parseFloat(shipping);
        sum = parseFloat(sum).toFixed(2);
        document.getElementById('amount_1_v2').value = (sum);
        document.getElementById('twt').innerHTML = formatMoney(sum);
        var khr = sum * 4100;
        document.getElementById('amount_khr_v2').value = khr;
    }
    
    
    customer_pay_money();
});


$("#amount_1").keyup(function() {
    amount_usd();
    customer_pay_money();
})
$("#amount_khr").keyup(function() {
    amount_khr();
    customer_pay_money();
})
$("#customer_pay_usd").change(function() {
    amount_usd();
    customer_pay_money();
    var paid = parseFloat($('#customer_pay_usd').val());
    var am = document.getElementById('amount_1_v2').value;
    var n = am;
    var change_khr = am * 4100;
    int = Math.floor(n);
    decimal = n - int;
    var khr = decimal * 4100;
    paid = parseFloat(paid).toFixed(2);
    if (paid == 'NaN'){
        paid = '';
        $('#customer_change_usd').text('-'+parseFloat(int).toFixed(2));
        $('#customer_change_khr').text('-'+formatNumber(khr,0));
        $('#khmer_change').text('-'+formatNumber(change_khr,0));
    }
    $('#customer_pay_usd').val(paid);
    
})
$("#customer_pay_khr").change(function() {
    amount_khr();
    customer_pay_money();
    var khr = ($('#customer_pay_khr').val());
    var am = document.getElementById('amount_1_v2').value;
    khr = parseFloat(khr).toFixed(0);
    int = Math.floor(am);
    decimal = am - int;
    var change_khr = decimal * 4100;
    var change_khr_2 = am * 4100;
    if (khr == 'NaN') {
        khr = '';
        $('#customer_change_usd').text('-'+parseFloat(int).toFixed(2));
        $('#customer_change_khr').text('-'+formatNumber(change_khr,0));
        $('#khmer_change').text('-'+formatNumber(change_khr_2,0));
    }
    $('#customer_pay_khr').val(khr);
})


</script>

<script>
function ajax_display_category_special(postData){
    var result = $.ajax
    (
     {
      url: '<?php echo base_url(); ?>admin/pos/display_category_special',
      type: 'GET',
      secureuri:false,
      dataType: 'html',
      data:postData,
      async: false,
      error: function (response, status, e)
      {
       alert(e);
      }
     }
    ).responseText;
    //var myWindow = window.open("", "MsgWindow", "width=700, height=400");
    //myWindow.document.write(result);     
    var array_data = String(result).split("api-ajax-request-multiple-result-split");
    var result_text = array_data[1];
    document.getElementById(postData['element_id']).innerHTML = result_text;
}
function ajax_display_category_nobarcode(postData){
    var result = $.ajax
    (
     {
      url: '<?php echo base_url(); ?>admin/pos/display_category_nobarcode',
      type: 'GET',
      secureuri:false,
      dataType: 'html',
      data:postData,
      async: false,
      error: function (response, status, e)
      {
       alert(e);
      }
     }
    ).responseText;
    // var myWindow = window.open("", "MsgWindow", "width=700, height=400");
    // myWindow.document.write(result);     
    var array_data = String(result).split("api-ajax-request-multiple-result-split");
    var result_text = array_data[1];
    document.getElementById(postData['element_id']).innerHTML = result_text;
}


var value = 'no';
function shipping_price(value) {
    if (value == 'no') {
        var tt_payable = document.getElementById('hide_total_payable').innerHTML;
        $('#hidden_shipping_price').hide();
        var a = 0;
        document.getElementById('shipping_price').value = a;
        document.getElementById('shipping_price_hidden').value = a;
        $('#shipping_price_v2').val(document.getElementById('shipping_price').value);
        document.getElementById('amount_1_v2').value = tt_payable;
        document.getElementById('twt').innerHTML = formatMoney(tt_payable);
        document.getElementById('amount_khr_v2').value = tt_payable * 4100;
        customer_pay_money();
    }
    else {
        $('#hidden_shipping_price').show();
        var b = 0;
        document.getElementById('shipping_price').value = b;
        $('#shipping_price_v2').val(document.getElementById('shipping_price').value);
    }
}
shipping_price(value);

function get_customer_paid_by(value){
    var result = $.ajax
    (
     {
      url: "<?=admin_url('pos/get_customer_paid_by')?>",
      type: 'GET',
      secureuri:false,
      dataType: 'html',
      data:value,
      async: false,
      error: function (response, status, e)
      {
       alert(e);
      }
     }
    ).responseText;
    // var myWindow = window.open("", "MsgWindow", "width=700, height=400");
    // myWindow.document.write(result);     
    var array_data = String(result).split("api-ajax-request-multiple-result-split");
    var result_text = array_data[1];
    document.getElementById('add_ons_poscustomer_id').value = result_text;
}

</script>

<style>


#product-list {
    height:380px;
    width:100%;
    overflow-y: scroll;
}
#pos #product-list {
    position:inherit !important;
    height:550px;
}
::-webkit-scrollbar{width:10px;background-color:white}
::-webkit-scrollbar-track{-webkit-box-shadow:inset 0 0 1px #22649d;border-radius:1px}
::-webkit-scrollbar-thumb{border-radius:1px;-webkit-box-shadow:inset 0 0 1px #22649d;background-color:#22649d}

</style>
</body>
</html>
