<style>
    .btn-default{
        cursor: pointer;
        font-weight: bold;
        font-size: 13px;
        
    }
</style>
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-users"></i><?= lang('create_user'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?php echo lang('create_user'); ?></p>

                <?php $attrib = array('class' => 'form-horizontal', 'data-toggle' => 'validator', 'role' => 'form');
                echo admin_form_open("auth/create_user", $attrib);
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-5">
                            <div class="form-group">
                                <?php echo lang('first_name', 'first_name'); ?>
                                <div class="controls">
                                    <?php echo form_input('first_name', '', 'class="form-control" id="first_name" required="required" pattern=".{3,10}"'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <?php echo lang('last_name', 'last_name'); ?>
                                <div class="controls">
                                    <?php echo form_input('last_name', '', 'class="form-control" id="last_name" required="required"'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?= lang('gender', 'gender'); ?>
                                <?php
                                $ge[''] = array('male' => lang('male'), 'female' => lang('female'));
                                echo form_dropdown('gender', $ge, (isset($_POST['gender']) ? $_POST['gender'] : ''), 'class="tip form-control" id="gender" data-placeholder="' . lang("select") . ' ' . lang("gender") . '" required="required"');
                                ?>
                            </div>

                            <div class="form-group">
                                <?php echo lang('company', 'company_id'); ?>
                                <div class="controls">
                                    <?php
                                    $tr[''] = lang("Please_select_a_company");
                                    if (is_array($customers)) if (count($customers) > 0)
                                    for ($i=0;$i<count($customers);$i++) {
                                        $tr[$customers[$i]['id']] = $customers[$i]['company'];
                                    }                            
                                    echo form_dropdown('company_id', $tr, '', 'data-placeholder="'.lang("Please_select_a_company").'" id="company_id" class="form-control" 
                                        onchange="
                                            var postData = {
                                               \'user_id\' : \''.$user->id.'\',
                                                \'company_id\' : this.value,
                                            };                                              
                                            api_get_company_branch(postData);
                                        " required="required" 
                                    ');
                                    ?>
                                </div>
                            </div>

                            <?php
    $tip = '<span style=\'font-weight:normal;\'><strong class=\'api_font_size_16\'>Owner</strong> can see all branches order information.<br><br><strong class=\'api_font_size_16\'>Purchaser</strong> can see all branches or only selected branch order information.</strong></span>';

    echo '
    <div class="form-group api_display_none">
        <div class="panel panel-info api_margin_bottom_0">
            <div class="panel-heading api_padding_left_10 api_padding_right_10 api_padding_top_5 api_padding_bottom_5">
                '.lang("Company_User_Type","Company_User_Type").'
                <span class="tip" title="" data-placement="bottom" data-html="true" href="#" data-original-title="'.$tip.'">
                    <i class="fa fa-question-circle"></i>
                </span>
            </div>
            <div class="panel-body" style="padding: 5px;">
                <div id="company_user_type_wrapper">
                </div>
            </div>
        </div>            
    </div>
    ';
?>

                            <div class="form-group">
                                <?php echo lang('phone', 'phone'); ?>
                                <div class="controls">
                                    <?php echo form_input('phone', '', 'class="form-control" id="phone" required="required"'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <?php echo lang('email', 'email'); ?>
                                <div class="controls">
                                    <input type="email" id="email" name="email" class="form-control"
                                        required="required" />
                                    <?php /* echo form_input('email', '', 'class="form-control" id="email" required="required"'); */ ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('username', 'username'); ?>
                                <div class="controls">
                                    <input type="text" id="username" name="username" class="form-control"
                                        required="required" pattern=".{4,20}" />
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('password', 'password'); ?>
                                <div class="controls">
                                    <?php echo form_password('password', '', 'class="form-control tip" id="password" required="required" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" data-bv-regexp-message="'.lang('pasword_hint').'"'); ?>
                                    <span class="help-block"><?= lang('pasword_hint') ?></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <?php echo lang('confirm_password', 'confirm_password'); ?>
                                <div class="controls">
                                    <?php echo form_password('confirm_password', '', 'class="form-control" id="confirm_password" required="required" data-bv-identical="true" data-bv-identical-field="password" data-bv-identical-message="' . lang('pw_not_same') . '"'); ?>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-5 col-md-offset-1">

                            <div class="form-group">
                                <?= lang('status', 'status'); ?>
                                <?php
                                $opt = array(1 => lang('active'), 0 => lang('inactive'));
                                echo form_dropdown('status', $opt, (isset($_POST['status']) ? $_POST['status'] : ''), 'id="status" required="required" class="form-control select" style="width:100%;"');
                                ?>
                            </div>
                            <div class="form-group">
                                <?= lang("group", "group"); ?>
                                <?php
                                foreach ($groups as $group) {                                    
                                    $gp[$group['id']] = $group['name'];
                                }
                                echo form_dropdown('group', $gp, (isset($_POST['group']) ? $_POST['group'] : ''), 'id="group" required="required" class="form-control select" style="width:100%;"');
                                ?>
                            </div>

                            <div class="clearfix"></div>
                            <div class="no">
                                <div class="form-group">
                                    <?= lang("biller", "biller"); ?>
                                    <?php
                                    $bl[""] = lang('select').' '.lang('biller');
                                    foreach ($billers as $biller) {
                                        $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                    }
                                    echo form_dropdown('biller', $bl, (isset($_POST['biller']) ? $_POST['biller'] : ''), 'id="biller" class="form-control select" style="width:100%;"');
                                    ?>
                                </div>

                                <div class="form-group">
                                    <?= lang("warehouse", "warehouse"); ?>
                                    <?php
                                    $wh[''] = lang('select').' '.lang('warehouse');
                                    foreach ($warehouses as $warehouse) {
                                        $wh[$warehouse->id] = $warehouse->name;
                                    }
                                    echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : ''), 'id="warehouse" class="form-control select" style="width:100%;" ');
                                    ?>
                                </div>

                                <div class="form-group">
                                    <?= lang("view_right", "view_right"); ?>
                                    <?php
                                    $vropts = array(1 => lang('all_records'), 0 => lang('own_records'));
                                    echo form_dropdown('view_right', $vropts, (isset($_POST['view_right']) ? $_POST['view_right'] : 1), 'id="view_right" class="form-control select" style="width:100%;"');
                                    ?>
                                </div>
                                <div class="form-group">
                                    <?= lang("edit_right", "edit_right"); ?>
                                    <?php
                                    $opts = array(1 => lang('yes'), 0 => lang('no'));
                                    echo form_dropdown('edit_right', $opts, (isset($_POST['edit_right']) ? $_POST['edit_right'] : 0), 'id="edit_right" class="form-control select" style="width:100%;"');
                                    ?>
                                </div>
                                <div class="form-group">
                                    <?= lang("allow_discount", "allow_discount"); ?>
                                    <?= form_dropdown('allow_discount', $opts, (isset($_POST['allow_discount']) ? $_POST['allow_discount'] : 0), 'id="allow_discount" class="form-control select" style="width:100%;"'); ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-8">
                                    <label class="checkbox" for="notify">
                                        <input type="checkbox" name="notify" value="1" id="notify" checked="checked" />
                                        <?= lang('notify_user_by_email') ?>
                                    </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <!-- luckydrawfile -->

                            <div class="row" style="margin-top: 20px;">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?= lang("Lucky Draw Code", "lucky_draw_code"); ?>
                                        <input type="text" name="add_ons_lucky_draw_code" class="form-control"
                                            id="add_ons_lucky_draw_code" value="" />
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group" >
                                        <button type="button" class="btn btn-default" onclick="generate_lucky_draw_code();">Generate Code</button>
                                    </div>
                                </div>  
                                <div class="col-md-12 ">
                                    <div class="form-group">
                                        <?= lang("Lucky Draw Status", "lucky_draw_status"); ?>
                                        <div class="controls">
                                        <?php 
                                            $stt[''] = array('playable' => lang('Playable'),'win' => lang('Win'), 'lost' => lang('Lost'));
                                            echo form_dropdown('add_ons_lucky_draw_status', $stt,(isset($_POST['lucky_draw_status']) ? $_POST['lucky_draw_status'] : ''), 'class="tip form-control" id="add_ons_lucky_draw_status"');
                                        ?>
                                        
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?= lang("Lucky Draw Win Code", "lucky_draw_win_code"); ?>
                                        <input type="text" name="add_ons_lucky_draw_win_code" class="form-control"
                                            id="add_ons_lucky_draw_win_code"
                                            value="<?php echo $user->lucky_draw_win_code; ?>" />

                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?= lang("Lucky Draw Win Product", "lucky_draw_win_product"); ?>
                                        <input type="text" name="add_ons_lucky_draw_win_product" class="form-control"
                                            id="add_ons_lucky_draw_win_product"
                                            value="<?php echo $user->lucky_draw_win_product; ?>" />

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <p><?php echo form_submit('add_user', lang('add_user'), 'class="btn btn-primary"'); ?></p>

                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
    $('.no').slideUp();
    $('#group').change(function(event) {
        var group = $(this).val();
        if (group == 1 || group == 2 || group == 3) {
            $('.no').slideUp();
        } else {
            $('.no').slideDown();
        }
    });
});
</script>

<script>
<?php 
    echo '
        function api_get_company_branch(postData) {
            var result = $.ajax
            (
                {
                    url:"'.base_url().'" + "admin/auth/getSelectCompanyBranch",
                    type: "GET",
                    secureuri:false,
                    dataType: "html",
                    data:postData,
                    async: false,
                    error: function (response, status, e)
                    {
                        alert(e);
                    }
                }
            ).responseText;    
            var array_data = String(result).split("api-ajax-request-multiple-result-split");
            $("#company_user_type_wrapper").html(array_data[0]);        
        }
        var postData = {
            "user_id" : "'.$user->id.'",
            "company_id" : "'.$user->company_id.'",
        };        
        api_get_company_branch(postData);
    ';
?>

function generate_lucky_draw_code(){
    // var userID = '<?php //echo $this->session->userdata('user_id') ?>';
    var random = Math.floor(Math.random() * 999999);
    // alert(userID + " " + random);
    $('#api_modal_title').html('Generated Code');
    $('#api_modal_body').html(random);
    $('#api_modal_footer').html('<button type="button" class="btn btn-info" data-dismiss="modal" onclick="use_this_code_click()">'+ 'Use This Code'+'</button> <button type="button" class="btn btn-danger" data-dismiss="modal">'+'Close'+'</button>');
    $('#api_modal_trigger').click();
}
//generate code
function use_this_code_click(){
    $('#add_ons_lucky_draw_code').val($('#api_modal_body').html());
}
</script>

<button type="button" id="api_modal_trigger" class="api_display_none" data-toggle="modal" data-target="#api_modal">Open Modal</button>
<!-- Modal -->
<div id="api_modal" class="modal fade" role="dialog" style="margin-top:100px; display: none;">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="api_modal_title">
        </h4>
      </div>
      <div class="modal-body" id="api_modal_body">
      </div>
      <div class="modal-footer" id="api_modal_footer">
      </div>
    </div>
  </div>
</div>