<style>
  .all_product {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
  margin: auto;
  text-align: center;

}
.product_title{
    width: 770px;
    padding: 14px;
    margin: auto;
    margin-top: 50px;
    color: red;
    font-weight: bold;
    font-size: 24px;
    margin-left: 335px;
}
.all_product td, .all_product th {
  border: 1px solid #ddd;
  padding: 8px;
}

.all_product tr:nth-child(even){background-color: #f2f2f2;}

.all_product tr:hover {background-color: #ddd;}

.all_product th {
  /* padding-top: 12px;
  padding-bottom: 12px;
  text-align: left; */
  background-color: #ec1d23;
  color: white;
  text-align: center;
}
.btn-default{
    cursor: pointer;
    font-weight: bold;
    font-size: 13px;
}
</style>
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">

    <div class="col-sm-2">
        <div class="row">
            <div class="col-sm-12 text-center">
                <div style="max-width:200px; margin: 0 auto;">
                    <?=
                    $user->avatar ? '<img alt="" src="' . base_url() . 'assets/uploads/avatars/thumbs/' . $user->avatar . '" class="avatar">' :
                        '<img alt="" src="' . base_url() . 'assets/images/' . $user->gender . '.png" class="avatar">';
                    ?>
                </div>
                <h4><?= lang('login_email'); ?></h4>

                <p><i class="fa fa-envelope"></i> <?= $user->email; ?></p>
            </div>
        </div>
    </div>

    <div class="col-sm-10">

        <ul id="myTab" class="nav nav-tabs">
            <li class=""><a href="#edit" class="tab-grey"><?= lang('edit') ?></a></li>
            <li class=""><a href="#cpassword" class="tab-grey"><?= lang('change_password') ?></a></li>
            <li class=""><a href="#avatar" class="tab-grey"><?= lang('avatar') ?></a></li>
        </ul>

        <div class="tab-content">
            <div id="edit" class="tab-pane fade in">

                <div class="box">
                    <div class="box-header">
                        <h2 class="blue"><i class="fa-fw fa fa-edit nb"></i><?= lang('edit_profile'); ?></h2>
                    </div>
                    <div class="box-content">
                        <div class="row">
                            <div class="col-lg-12">

                                <?php $attrib = array('class' => 'form-horizontal', 'data-toggle' => 'validator', 'role' => 'form');
                                echo admin_form_open('auth/edit_user/' . $user->id, $attrib);
                                ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <?php echo lang('first_name', 'first_name'); ?>
                                                <div class="controls">
                                                    <?php echo form_input('first_name', $user->first_name, 'class="form-control" id="first_name" required="required"'); ?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <?php echo lang('last_name', 'last_name'); ?>

                                                <div class="controls">
                                                    <?php echo form_input('last_name', $user->last_name, 'class="form-control" id="last_name" required="required"'); ?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <?php echo lang('company', 'company_id'); ?>
                                                <div class="controls">
                                                    <?php
                                                    $tr[''] = lang("Please_select_a_company");
                                                    if (is_array($customers)) if (count($customers) > 0)
                                                    for ($i=0;$i<count($customers);$i++) {
                                                        $tr[$customers[$i]['id']] = $customers[$i]['company'];
                                                    }                            
                                                    echo form_dropdown('company_id', $tr, $user->company_id, 'data-placeholder="'.lang("Please_select_a_company").'" id="company_id" class="form-control" 
                                                        onchange="
                                                            var postData = {
                                                                \'user_id\' : \''.$user->id.'\',
                                                                \'company_id\' : this.value,
                                                            };                                              
                                                            api_get_company_branch(postData);
                                                        " required="required" 
                                                    ');
                                                    ?>
                                                </div>
                                            </div>

                                            <?php
    $tip = '<span style=\'font-weight:normal;\'><strong class=\'api_font_size_16\'>Owner</strong> can see all branches order information.<br><br><strong class=\'api_font_size_16\'>Purchaser</strong> can see all branches or only selected branch order information.</strong></span>';

    echo '
    <div class="form-group api_display_none">
        <div class="panel panel-info api_margin_bottom_0">
            <div class="panel-heading api_padding_left_10 api_padding_right_10 api_padding_top_5 api_padding_bottom_5">
                '.lang("Company_User_Type","Company_User_Type").'
                <span class="tip" title="" data-placement="bottom" data-html="true" href="#" data-original-title="'.$tip.'">
                    <i class="fa fa-question-circle"></i>
                </span>
            </div>
            <div class="panel-body" style="padding: 5px;">
                <div id="company_user_type_wrapper">
                </div>
            </div>
        </div>            
    </div>
    ';
?>

                                            <div class="form-group">

                                                <?php echo lang('phone', 'phone'); ?>
                                                <div class="controls">
                                                    <input type="tel" name="phone" class="form-control" id="phone"
                                                        required="required" value="<?= $user->phone ?>" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <?= lang('gender', 'gender'); ?>
                                                <div class="controls"> <?php
                                                    $ge[''] = array('male' => lang('male'), 'female' => lang('female'));
                                                    echo form_dropdown('gender', $ge, (isset($_POST['gender']) ? $_POST['gender'] : $user->gender), 'class="tip form-control" id="gender" required="required"');
                                                    ?>
                                                </div>
                                            </div>
                                        
                                            <div class="form-group">
                                                <?= lang('award_points', 'award_points'); ?>
                                                <?= form_input('award_points', set_value('award_points', $user->award_points), 'class="form-control tip" id="award_points"  '); ?>
                                            </div>
                                            

                                            
                                            <div class="form-group">
                                                <?php echo lang('username', 'username'); ?>
                                                <input type="text" name="username" class="form-control" id="username"
                                                    value="<?= $user->username ?>" required="required" />
                                            </div>
                                            <div class="form-group">
                                                <?php echo lang('email', 'email'); ?>

                                                <input type="email" name="email" class="form-control" id="email"
                                                    value="<?= $user->email ?>" required="required" />
                                            </div>
                                            <div class="row">
                                                <div class="panel panel-warning">
                                                    <div class="panel-heading">
                                                        <?= lang('if_you_need_to_rest_password_for_user') ?></div>
                                                    <div class="panel-body" style="padding: 5px;">
                                                        <div class="col-md-12">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <?php echo lang('password', 'password'); ?>
                                                                    <?php echo form_input($password,'','autocomplete="new-password"'); ?>
                                                                </div>

                                                                <div class="form-group">
                                                                    <?php echo lang('confirm_password', 'password_confirm'); ?>
                                                                    <?php echo form_input($password_confirm); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            

                                        </div>
                                        <div class="col-md-6 col-md-offset-1">
                                            <?php 
                                                if ($Owner) 
                                                    $temp_display = '';
                                                else
                                                    $temp_display = 'api_display_none';
                                            ?>


                                            <div class="row <?php echo $temp_display; ?>">

                                                <div class="panel panel-warning">
                                                    <div class="panel-heading"><?= lang('user_options') ?></div>
                                                    <div class="panel-body" style="padding: 5px;">
                                                        <div class="col-md-12">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <?= lang('status', 'status'); ?>
                                                                    <?php
                                                                            $opt = array(1 => lang('active'), 0 => lang('inactive'));
                                                                            echo form_dropdown('status', $opt, (isset($_POST['status']) ? $_POST['status'] : $user->active), 'id="status" required="required" class="form-control input-tip select" style="width:100%;"');
                                                                            ?>
                                                                </div>

                                                                <?php 
                                                                        //if (!$this->ion_auth->in_group('customer', $id) && !$this->ion_auth->in_group('supplier', $id)) { ?>
                                                                <div class="form-group">
                                                                    <?= lang("group", "group"); ?>
                                                                    <?php
                                                                            $gp[""] = "";
                                                                            foreach ($groups as $group) {
                                                                                //if ($group['name'] != 'customer' && $group['name'] != 'supplier') {
                                                                                    $gp[$group['id']] = $group['name'];
                                                                                //}
                                                                            }
                                                                            echo form_dropdown('group', $gp, (isset($_POST['group']) ? $_POST['group'] : $user->group_id), 'id="group" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("group") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                                                            
                                                                            ?>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                                <div class="no">
                                                                    <div class="form-group">
                                                                        <?= lang("biller", "biller"); ?>
                                                                        <?php
                                                                                $bl[""] = lang('select').' '.lang('biller');
                                                                                foreach ($billers as $biller) {
                                                                                    $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                                                                }
                                                                                echo form_dropdown('biller', $bl, (isset($_POST['biller']) ? $_POST['biller'] : $user->biller_id), 'id="biller" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("biller") . '" class="form-control select" style="width:100%;"');
                                                                                ?>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <?= lang("warehouse", "warehouse"); ?>
                                                                        <?php
                                                                                $wh[''] = lang('select').' '.lang('warehouse');
                                                                                foreach ($warehouses as $warehouse) {
                                                                                    $wh[$warehouse->id] = $warehouse->name;
                                                                                }
                                                                                echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : $user->warehouse_id), 'id="warehouse" class="form-control select" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("warehouse") . '" style="width:100%;" ');
                                                                                ?>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <?= lang("view_right", "view_right"); ?>
                                                                        <?php
                                                                                $vropts = array(1 => lang('all_records'), 0 => lang('own_records'));
                                                                                echo form_dropdown('view_right', $vropts, (isset($_POST['view_right']) ? $_POST['view_right'] : $user->view_right), 'id="view_right" class="form-control select" style="width:100%;"');
                                                                                ?>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <?= lang("edit_right", "edit_right"); ?>
                                                                        <?php
                                                                                $opts = array(1 => lang('yes'), 0 => lang('no'));
                                                                                echo form_dropdown('edit_right', $opts, (isset($_POST['edit_right']) ? $_POST['edit_right'] : $user->edit_right), 'id="edit_right" class="form-control select" style="width:100%;"');
                                                                                ?>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <?= lang("allow_discount", "allow_discount"); ?>
                                                                        <?= form_dropdown('allow_discount', $opts, (isset($_POST['allow_discount']) ? $_POST['allow_discount'] : $user->allow_discount), 'id="allow_discount" class="form-control select" style="width:100%;"'); ?>
                                                                    </div>
                                                                    <?php // } ?>
                                                                </div>

                                                                <div class="form-group">
                                                                    <?php
                                                                        if ($user->allow_same_email != 'yes') {
                                                                            $temp_1 = set_radio('add_ons_allow_same_email', 'yes'); 
                                                                            $temp_2 = set_radio('add_ons_allow_same_email', '', TRUE);
                                                                        }
                                                                        else {
                                                                            $temp_1 = set_radio('add_ons_allow_same_email', 'yes', TRUE);
                                                                            $temp_2 = set_radio('add_ons_allow_same_email', '');                                
                                                                        }
                                                                        echo '
                                                                        <div class="form-group col-md-12">
                                                                            '.lang("Allow_Same_Email", "allow_same_email").'
                                                                                <div class="">
                                                                                    <div class="api_height_10"></div>
                                                                                    <input type="radio" id="add_ons_allow_same_email" name="add_ons_allow_same_email" value="yes" '.$temp_1.' />
                                                                                    Yes
                                                                                    <span class="api_padding_left_10">
                                                                                        <input type="radio" id="add_ons_allow_same_email" name="add_ons_allow_same_email" value="" '.$temp_2.' />
                                                                                    </span>
                                                                                    No
                                                                                </div>
                                                                        </div>                
                                                                        ';
                                                                    ?>

                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <?= lang("Lucky Draw Code", "lucky_draw_code"); ?>
                                                        <input type="text" name="add_ons_lucky_draw_code"
                                                            class="form-control" id="add_ons_lucky_draw_code"
                                                            value="<?php echo $user->lucky_draw_code; ?>" />
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group" >
                                                        <button type="button" class="btn btn-default" onclick="generate_lucky_draw_code();">Generate Code</button>
                                                       
                                                    </div>
                                                </div>  

                                                <div class="col-md-12 ">
                                                    <div class="form-group">
                                                        <?= lang("Lucky Draw Status", "lucky_draw_status"); ?>
                                                        <div class="controls"> <?php
                                                        $stt[''] = array('playable' => lang('Playable'),'win' => lang('Win'), 'lost' => lang('Lost'));
                                                        echo form_dropdown('add_ons_lucky_draw_status', $stt, (isset($_POST['add_ons_lucky_draw_status']) ? $_POST['add_ons_lucky_draw_status'] : $user->lucky_draw_status), 'class="tip form-control"');
                                                        ?>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group" style="display:none">
                                                        <?= lang("Lucky Draw Win Code", "lucky_draw_win_code"); ?>
                                                        <input type="text" name="add_ons_lucky_draw_win_code"
                                                            class="form-control" id="add_ons_lucky_draw_win_code"
                                                            value="<?php echo $user->lucky_draw_win_code; ?>" />

                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group" style="display:none">
                                                        <?= lang("Lucky Draw Win Product", "lucky_draw_win_product"); ?>
                                                        <input type="text" name="add_ons_lucky_draw_win_product"
                                                            class="form-control" id="add_ons_lucky_draw_win_product"
                                                            value="<?php echo $user->lucky_draw_win_product; ?>" />

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="panel panel-warning">
                                                    <div class="panel-heading"><?= lang('Win_Lucky_Draw_Product') ?>
                                                    </div>
                                                    <div class="panel-body" style="padding: 5px;">
                                                        <div class="col-md-12">
                                                            <div id="display_all_product">
                                                                <?php
            
                                                                //explode , and -(cut comma and minus)$lucky_draw_code[0]['lucky_draw_win_code']
                                                                $exp = explode("," , $user->lucky_draw_win_code);
                                                                
                                                                $temp_display = '';
                                                                $temp_display .= '<table class="all_product">
                                                                                    <tr>
                                                                                        <th>#</th>
                                                                                        <th>Image</th>
                                                                                        <th>Product Name</th>
                                                                                        <th>Status</th>
                                                                                    </tr>';
                                                                    $counter = 1;                
                                                                    for($k=0;$k<count($exp);$k++) {
                                                                    $exp_2 = explode("-" , $exp[$k]); //take the value that have exploded before for use
                                                                    if ($exp_2[0] != '') {
                                                                        $config_data = array(
                                                                            'table_name' => 'sma_products',
                                                                            'select_table' => 'sma_products',
                                                                            'translate' => 'yes',
                                                                            'select_condition' => "id = ".$exp_2[0]." order by id desc ",
                                                                        );
                                                                        $temp = $this->site->api_select_data_v2($config_data);

                                                                        $data_view = array (
                                                                            'wrapper_class' => '',
                                                                            'file_path' => 'assets/uploads/'.$temp[0]['image'],
                                                                            'max_width' => 200,
                                                                            'max_height' => 100,
                                                                            'product_link' => 'true',
                                                                            'image_class' => 'img-responsive',
                                                                            'image_id' => '',   
                                                                            'resize_type' => 'full',
                                                                        );
                                                                        $temp2 = $this->site->api_get_image($data_view);
                                                                        

                                                                        if($exp_2[1] == 1){
                                                                            $message = "Received";
                                                                        } if($exp_2[1] == 0){
                                                                            $message = "Not yet receive";
                                                                        }                    

                                                                        $temp_display .= '<tr>
                                                                            <td>
                                                                                '.$counter++.'
                                                                            </td>
                                                                            <td>
                                                                                '.$temp2['image_table'].'
                                                                            </td>
                                                                            <td>
                                                                                '.$temp[0]['title_en'].'
                                                                            </td>
                                                                            <td>';
                                                                            
                                                                                if ($exp_2[1] != 1) {
                                                                                    $api_temp_1 = set_radio('status_product', '1'); 
                                                                                    $api_temp_2 = set_radio('status_product', '0', TRUE);
                                                                                }
                                                                                else {
                                                                                    $api_temp_1 = set_radio('status_product', '1', TRUE);
                                                                                    $api_temp_2 = set_radio('status_product', '0');                                
                                                                                }
                                                                                $temp_display .= '<div class="form-group">
                                                                                    <div class="form-group col-md-12">
                                                                                            <div style="float: right">
                                                                                                <input type="radio" class="skip" name="status_'.$k.'" id="status_1_'.$k.'" onclick="api_change_lucky_status('.$exp_2[0].','.$k.',1);" value="1" '.$api_temp_1.' />
                                                                                                Yes
                                                                                                <br>
                                                                                                <span class="api_padding_left_10">
                                                                                                    <input style="margin-left: -15px" type="radio" class="skip" name="status_'.$k.'" id="status_0_'.$k.'"  onclick="api_change_lucky_status('.$exp_2[0].','.$k.',0);" value="0" '.$api_temp_2.' />
                                                                                                </span>
                                                                                                No
                                                                                            </div>
                                                                                    </div>                
                                                                                </div>

                                                                            </td>
                                                                        </tr>';
                                                                    }
                                                                    // $this->sma->print_arrays($temp);
                                                                }
                                                                $temp_display .= '</table>';

                                                                echo $temp_display;

                                                                
                                                            ?>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            
                                            <?php echo form_hidden('id', $id); ?>
                                            <?php echo form_hidden($csrf); ?>
                                        </div>


                                    </div>
                                </div>
                                <p><?php echo form_submit('update', lang('update'), 'class="btn btn-primary"'); ?></p>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="cpassword" class="tab-pane fade">
                <div class="box">
                    <div class="box-header">
                        <h2 class="blue"><i class="fa-fw fa fa-key nb"></i><?= lang('change_password'); ?></h2>
                    </div>
                    <div class="box-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <?php echo admin_form_open("auth/change_password?id=".$this->uri->segment(4), 'id="change-password-form"'); ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <?php echo lang('old_password', 'curr_password'); ?> <br />
                                                <?php echo form_password('old_password', '', 'class="form-control" id="curr_password" required="required"'); ?>
                                            </div>

                                            <div class="form-group">
                                                <label
                                                    for="new_password"><?php echo sprintf(lang('new_password'), $min_password_length); ?></label>
                                                <br />
                                                <?php echo form_password('new_password', '', 'class="form-control" id="new_password" required="required" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" data-bv-regexp-message="'.lang('pasword_hint').'"'); ?>
                                                <span class="help-block"><?= lang('pasword_hint') ?></span>
                                            </div>

                                            <div class="form-group">
                                                <?php echo lang('confirm_password', 'new_password_confirm'); ?> <br />
                                                <?php echo form_password('new_password_confirm', '', 'class="form-control" id="new_password_confirm" required="required" data-bv-identical="true" data-bv-identical-field="new_password" data-bv-identical-message="' . lang('pw_not_same') . '"'); ?>

                                            </div>
                                            <?php echo form_input($user_id); ?>
                                            <p><?php echo form_submit('change_password', lang('change_password'), 'class="btn btn-primary"'); ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="avatar" class="tab-pane fade">
                <div class="box">
                    <div class="box-header">
                        <h2 class="blue"><i class="fa-fw fa fa-file-picture-o nb"></i><?= lang('change_avatar'); ?></h2>
                    </div>
                    <div class="box-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="col-md-5">
                                    <div style="position: relative;">
                                        <?php if ($user->avatar) { ?>
                                        <img alt="" src="<?= base_url() ?>assets/uploads/avatars/<?= $user->avatar ?>"
                                            class="profile-image img-thumbnail">
                                        <a href="#" class="btn btn-danger btn-xs po" style="position: absolute; top: 0;"
                                            title="<?= lang('delete_avatar') ?>"
                                            data-content="<p><?= lang('r_u_sure') ?></p><a class='btn btn-block btn-danger po-delete' href='<?= admin_url('auth/delete_avatar/' . $id . '/' . $user->avatar) ?>'> <?= lang('i_m_sure') ?></a> <button class='btn btn-block po-close'> <?= lang('no') ?></button>"
                                            data-html="true" rel="popover"><i class="fa fa-trash-o"></i></a><br>
                                        <br><?php } ?>
                                    </div>
                                    <?php echo admin_form_open_multipart("auth/update_avatar"); ?>
                                    <div class="form-group">
                                        <?= lang("change_avatar", "change_avatar"); ?>
                                        <input type="file" data-browse-label="<?= lang('browse'); ?>" name="avatar"
                                            id="product_image" required="required" data-show-upload="false"
                                            data-show-preview="false" accept="image/*" class="form-control file" />
                                    </div>
                                    <div class="form-group">
                                        <?php echo form_hidden('id', $id); ?>
                                        <?php echo form_hidden($csrf); ?>
                                        <?php echo form_submit('update_avatar', lang('update_avatar'), 'class="btn btn-primary"'); ?>
                                        <?php echo form_close(); ?>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
    $(document).ready(function() {
        $('#change-password-form').bootstrapValidator({
            message: 'Please enter/select a value',
            submitButtons: 'input[type="submit"]'
        });
    });
    </script>
    <?php if ($Owner && $id != $this->session->userdata('user_id')) { ?>
    <script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        $('#group').change(function(event) {
            var group = $(this).val();
            if (group == 1 || group == 2 || group == 3) {
                $('.no').slideUp();
            } else {
                $('.no').slideDown();
            }
        });
        var group = <?=$user->group_id?>;
        if (group == 1 || group == 2 || group == 3) {
            $('.no').slideUp();
        } else {
            $('.no').slideDown();
        }
    });
    $('#group').change();
    </script>
    <?php } ?>

    <script>
    <?php 
    echo '
        function api_get_company_branch(postData) {
            var result = $.ajax
            (
                {
                    url:"'.base_url().'" + "admin/auth/getSelectCompanyBranch",
                    type: "GET",
                    secureuri:false,
                    dataType: "html",
                    data:postData,
                    async: false,
                    error: function (response, status, e)
                    {
                        alert(e);
                    }
                }
            ).responseText;    
            var array_data = String(result).split("api-ajax-request-multiple-result-split");
            $("#company_user_type_wrapper").html(array_data[0]);        
        }
        var postData = {
            "user_id" : "'.$user->id.'",
            "company_id" : "'.$user->company_id.'",
        };        
        api_get_company_branch(postData);
    ';
?>

function api_change_lucky_status(p_id, index, value){
    var temp = $("#add_ons_lucky_draw_win_code").val();
    var temp = temp.split(",");
    var temp_2 = "";
    for (var i = 0; i < temp.length; i++) {
        if (i != index)
            temp_2 = temp_2 + temp[i] + ',';
        else
            temp_2 = temp_2 + p_id + '-' + value + ',';
    }
    temp_2 = temp_2.slice(0, -1);
    $("#add_ons_lucky_draw_win_code").val(temp_2);
}

//alert user id with code random
function generate_lucky_draw_code(){
    var userID = '<?php echo $this->uri->segment(4); ?>';
    var random = Math.floor(Math.random() * 999999);
    // alert(userID + " " + random);
    $('#api_modal_title').html('Generated Code');
    $('#api_modal_body').html(userID + "-" +random);
    $('#api_modal_footer').html('<button type="button" class="btn btn-info" data-dismiss="modal" onclick="use_this_code_click()">'+ 'Use This Code'+'</button> <button type="button" class="btn btn-danger" data-dismiss="modal">'+'Close'+'</button>');
    $('#api_modal_trigger').click();
}
//generate code
function use_this_code_click(){
    // document.getElementById('add_ons_lucky_draw_code').value = $('#api_modal_body').html();
    // document.getElementById('add_ons_lucky_draw_code').value = document.getElementById('api_modal_body').innerHTML;
    $('#add_ons_lucky_draw_code').val($('#api_modal_body').html());
}
</script>



<button type="button" id="api_modal_trigger" class="api_display_none" data-toggle="modal" data-target="#api_modal">Open Modal</button>
<!-- Modal -->
<div id="api_modal" class="modal fade" role="dialog" style="margin-top:100px; display: none;">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="api_modal_title">
        </h4>
      </div>
      <div class="modal-body" id="api_modal_body">
      </div>
      <div class="modal-footer" id="api_modal_footer">
      </div>
    </div>
  </div>
</div>