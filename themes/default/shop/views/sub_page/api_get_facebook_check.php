
<script>

  window.fbAsyncInit = function() {
    FB.init({
      appId      : '<?php echo $this->api_shop_setting[0]['facebook_app_id']; ?>',
      cookie     : true,                     // Enable cookies to allow the server to access the session.
      xfbml      : true,                     // Parse social plugins on this webpage.
      version    : 'v6.0'           // Use this Graph API version for this call.
    });
  };
  
  (function(d, s, id) {                      // Load the SDK asynchronously
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

function checkLoginState() {               // Called when a person is finished with the Login Button.
  FB.getLoginStatus(function(response) {   // See the onlogin handler
    if (response.status === 'connected') {
      FB.api('/me?fields=id,name,email', function(response) {
          var postData = {
              'id' : response.id,
              'name' : response.name,
              'email' : response.email,
          };
          if (response.id != <?php echo $this->session->userdata('api_facebook_login_id'); ?>) 
            window.location = "<?php echo base_url().'logout'; ?>";
      });
    }
    else {                                 // Not logged into your webpage or we are unable to tell.
      window.location = "<?php echo base_url().'logout'; ?>";
    }    
  });
}


$('#api_facebook_login_button').click();
</script>


<fb:login-button class="api_display_none" id="api_facebook_login_button" scope="public_profile,email" onlogin="checkLoginState();">
</fb:login-button>

<!--
<fb:login-button class="" id="api_facebook_login_button" scope="public_profile,email" onlogin="checkLoginState();">
</fb:login-button>

<div class="fb-login-button" id="api_facebook_login_button" data-width="" data-size="large" data-button-type="continue_with" data-layout="default" data-auto-logout-link="false" data-use-continue-as="false" scope="public_profile,email" onlogin="checkLoginState();"></div>
-->




