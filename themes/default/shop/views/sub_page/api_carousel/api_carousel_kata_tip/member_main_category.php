<?php

$temp_display = '';
$temp_script = '';
for ($i=0;$i<count($main_categories);$i++) {

        if (${'products_'.$main_categories[$i]['id']}) {
            $config_data = array(
                'id' => 'slider'.$main_categories[$i]['id'],
                'col_num' => 4,
                'col_class' => 'col-md-3',
            );
            
            $temp_pc = $this->site->build_slide(${'products_'.$main_categories[$i]['id']}, $config_data, $api_view_array);
            
            if (count(${'products_'.$main_categories[$i]['id']})>0) {
                // $temp_pc = $this->site->api_kata_tip_display(${'products_'.$main_categories[$i]['id']},$config_data,$api_view_array);
                $temp_display .= '
                <section id="sliderwrapper_'.$main_categories[$i]['id'].'" class="page-contents api_padding_bottom_0_im hidden-sm hidden-xs">
                    <div class="container">
                        <div class="col-md-12">                        
                            <h3 class="margin-top-no text-size-lg">';
                if (count(${'products_'.$main_categories[$i]['id']}) > 0) {
                    $temp_display .= '
                        <a style="color: #dc9b01"class="api_link_underline" href="'.base_url().'category/'.$main_categories[$i]['slug'].'">
                            '.$main_categories[$i]['title_'.$this->api_shop_setting[0]['api_lang_key']].'
                        </a>
                    ';
                }
                $temp_display .= ' <span class="count_item"></span>
                            </h3>
                        </div>    
                        <div class="col-md-12 api_padding_left_10 api_padding_right_5"> 
                            '.$temp_pc.'
                        </div>
                    </div>
                </section>';
            }
        }

    // $temp_script .= 'slide_gen("'.$config_data['id'].'");';
    // $temp_script .= '
//     $("#api_slider_promotion_'.$main_categories[$i]['id'].'").slick({
//         autoplay:false,
//         arrows: true,
//         slidesToShow: 1
//     });
// ';
}

echo $temp_display;

?>

<?php
echo '
    <script>
    $(function(){
        '.$temp_script.'
    });
    </script>
';
?>