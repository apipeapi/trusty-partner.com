<?php

echo '
<link rel="stylesheet" href="'.base_url().'themes/default/shop/views/sub_page/api_carousel/api_carousel_kata_tip/reset.css">
<link rel="stylesheet" href="'.base_url().'themes/default/shop/views/sub_page/api_carousel/api_carousel_kata_tip/slick.css">
<script src="'.base_url().'themes/default/shop/views/sub_page/api_carousel/api_carousel_kata_tip/slick.js"></script>
';

if ($this->session->userdata('api_mobile') == 1) {
    $api_view_array['col_class'] = 'col-md-4 col-sm-6 col-xs-6';
    $api_view_array['col_class_odd'] = 'api_padding_right_7';
    $api_view_array['col_class_even'] = 'api_padding_left_7';
    $api_view_array['product_class'] = 'api_padding_0';
    $api_view_array['title_class'] = 'api_padding_5';
    $api_view_array['title_style'] = 'min-height:50px; max-height: 50px !important; overflow:hidden;';
    $api_view_array['quantity_class'] = 'api_display_none';
    $api_view_array['price_class_break'] = '<div class="api_clear_both"></div>';
    $api_view_array['price_style'] = 'float: left !important; padding-bottom:0px; padding-left:5px !important; padding-right:5px !important; text-align: left !important; width: 100% !important;';
    $api_view_array['select_qty_style'] = 'width: 100% !important; padding-bottom:0px; padding-left:5px !important; padding-right:5px !important;';
    $api_view_array['label_qty_class'] = 'api_display_none';
    $api_view_array['select_style'] = 'width:100% !important;';
    $api_view_array['btn_class'] = 'api_padding_right_5 api_padding_left_5';
    $api_view_array['btn_break'] = '<div class="api_clear_both api_height_5"></div>';
    $api_view_array['label_btn'] = lang('Add');
    $api_view_array['carousel_col'] = 2;
    $api_view_array['carousel_col_row'] = 2;
    $api_view_array['carousel_col_row_2'] = 2;
    $api_view_array['select_option'] = '';
    
    $api_view_array['sorry'] = '
        <div class="product-price " style=" width: 100% !important;">
            <div class="text-red api_padding_bottom_0" style="font-size:14px;">
                Sorry for inconvenient
            </div>
        </div>   
    ';
} else {
    $api_view_array['title_style'] = 'min-height:40px; max-height: 40px !important; overflow:hidden;';
    $api_view_array['col_class'] = 'col-md-4 col-sm-6';
    $api_view_array['label_btn'] = lang('add_to_cart');
    $api_view_array['carousel_col'] = 3;
    $api_view_array['carousel_col_row'] = 3;
    $api_view_array['carousel_col_row_2'] = 3;
    $api_view_array['carousel_script'] = '';
    $api_view_array['select_option'] = '<option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option>';
    // $api_view_array['sorry'] = '
    //     <div class="product-price " style=" width: 100% !important; height:55px; text-align:center;">
    //         <div class="text-red api_padding_bottom_5" style="font-size:14px; text-align:left;">
    //             Sorry for inconvenient
    //         </div>
    //     </div>';
    $api_view_array['sorry'] = '  
        <div class="product-price " style=" width: 100% !important;">
            <del class="text-red api_visibility_hidden">$1.50</del>
            <div class="api_padding_bottom_5">Sorry for inconvenient</div>
        </div>
    ';
}


$temp_display = '';
$temp_script = '';
//Promotion===============================================================================

if ($this->api_shop_setting[0]['front_promotion'] == 1) {
    if (is_array($promotion_products)) {
        $config_data = array(
            'id' => 'slider1',
            'col_num' => 4,
            'col_class' => 'col-md-3',
        );
        
        // $temp_pc = $this->site->api_kata_tip_display($promotion_products, $config_data, $api_view_array);
        $temp_pc = $this->site->build_slide($promotion_products, $config_data, $api_view_array);
        if (count($promotion_products)>0) {
            $temp_display .= '
                <section id="api_slider_promotion_wrapper" class="page-contents api_padding_bottom_0_im">
                    <div class="container">
                        <div class="col-md-12">
                        <a class="api_link_underline" href="'.base_url().'shop/products?sort_by=promotion-desc">
                            <img width="25" style="float:left; margin:-10px 10px 0px 0px" class="img-responsive" src="'.base_url('assets/images/hot-item.png').'">
                            <h3 class="margin-top-no text-size-lg" style="float:left">
                            <span class="">'.lang('HOT_ITEM').'</span> <span class="count_item">( '.count($promotion_products_total).' '.lang('products').' )</span> 
                            </h3>
                        </a>
                        <div class="api_clear_both"></div>
                        </div>
                        <div class="col-md-12 api_padding_left_10 api_padding_right_5"> 
                            '.$temp_pc.'         
                        </div>
                    </div>
                </section>
            ';
        }
        // $temp_script .= 'slide_gen("slider1");';
        // $temp_script .= '
        //     $("#slider1").slick({
        //         autoplay:false,
        //         arrows: true,
        //         slidesToShow: 1
        //     });
        // ';
    }
}
//Promotion===============================================================================


//Featured===============================================================================
if (is_array($featured_products)) {
    $new_array_featured_products = [];

    $config_data = array(
            'id' => 'slider2',
            'col_num' => 4,
            'col_class' => 'col-md-3',
        );
    $temp_pc = $this->site->build_slide_v2($featured_products, $config_data, $api_view_array);
// $this->sma->print_arrays($featured_products);
    if (count($featured_products)>0) {
        $temp_display .= '
                <section id="api_slider_featured_wrapper" class="page-contents api_padding_bottom_0_im">
                    <div class="container">
                        <div class="col-md-12">
                        <a class="api_link_underline" href="'.base_url().'shop/products?sort_by=featured">
                            <img width="25" style="float:left; margin:-2px 10px 0 0" class="img-responsive image_round_action" src="'.base_url('assets/images/star_1.png').'">
                            <h3 class="margin-top-no text-size-lg">
                                <span class="">'.lang('Popular_And_New').'</span> <span class="count_item">( '.count($featured_products_total).' '.lang('products').' )</span>
                            </h3>
                        </a>
                        </div>
                        <div class="col-md-12 api_padding_left_10 api_padding_right_5">
                            '.$temp_pc.'
                        </div>
                    </div>
                </section>
            ';
    }
    // $temp_script .= 'slide_gen("slider2");';
    // $temp_script .= '
    //         $("#slider1").slick({
    //             autoplay:false,
    //             arrows: true,
    //             slidesToShow: 1
    //         });
    //     ';
}
//Featured===============================================================================

echo $temp_display;

?>

<style>
.slider {
  width: 100%;
  margin: 0px 0px 0px 0px;
}
.slider .slick-list {
  padding: 0 30% 0 0;
}
.slider li {
  margin: 0px;
}
.slider img {
  max-height:190px;
}
.api_kata_tip_table_img{
    height:190px;
}
.api_kata_tip_box {
    background-color: #333 !important;
    /* border: 1px solid #d9bb72 !important; */
    margin:0px !important;
    padding:0px !important;
    height:auto !important;
}
.api_kata_tip_box .product-price {
    font-size: 1.2em;
    font-weight: 700;
    line-height: 1.2em;
    padding-top:5px;
    padding-bottom:5px;
    padding-left:10px;
    padding-right:10px;
    text-align: left;
}
.api_kata_tip_box .product-cart-button .btn-group .btn-theme {
    width: calc(100% - 40px) !important;
}
.api_kata_tip_box .btn-group, .btn-group-vertical {
    display: block;
}

.api_kata_tip_box_mobile{
    width:123px;
    float:left;
}
.api_kata_tip_box_mobile .fa-shopping-cart{
    padding-right:0px !important;
}

.api_kata_tip_box_mobile .input-group-addon {
    padding: 6px 7px !important;
}
.api_kata_tip_box_mobile .btn {
    padding: 6px 6px !important;
}
.api_kata_tip_box_mobile .product-cart-button .btn-group .btn-theme {
    width: calc(100% - 27px) !important;
}
.api_kata_tip_box .api_absolute_center{
        top:30%;
}
.slick-prev {
    background-color: #fff;
    height: 70px;
    width: 35px;
    z-index: 1000;
    display: none;
    border: 1px solid rgba(0,0,0,0.3);
    cursor: pointer;
    top: 30%;
    position: absolute;
    opacity: .9;
    transition: background-color .1s ease-out,border .5s ease-out;
}
.slick-next {
    background-color: #fff;
    height: 70px;
    width: 35px;
    z-index: 1000;
    display: none;
    right:-5px;
    border: 1px solid rgba(0,0,0,0.3);
    cursor: pointer;
    top: 30%;
    position: absolute;
    opacity: .9;
    transition: background-color .1s ease-out,border .5s ease-out;
}
.image_round_action{
    -webkit-animation:spin 4s linear infinite;
    -moz-animation:spin 4s linear infinite;
    animation:spin 4s linear infinite;
}
@-moz-keyframes spin { 100% { -moz-transform: rotate(360deg); } }
@-webkit-keyframes spin { 100% { -webkit-transform: rotate(360deg); } }
@keyframes spin { 100% { -webkit-transform: rotate(360deg); transform:rotate(360deg); } }
div[id^="slider"] ul{
    overflow: auto;
    white-space: nowrap;
}
div[id^="slider"] ul::-webkit-scrollbar{
    width: 0;
    height: 0;
    background-color: transparent;
}
li.product-top.product-bottom {
    display: inline-block;
    margin: 0px auto 0 0px;
}
</style>
<?php
echo '
<script>
    $(function(){
        '.$temp_script.'
    });
</script>
';
?>

