<?php
//Slide===============================================================================
    $slider = json_decode($this->api_shop_setting[0]['slider']);
    if ($this->api_shop_setting[0]['front_slider'] != 1) {
        $slider = array();
    }

?>

<?php if (!empty($slider)) { ?>
<section class="slider-container mobile hidden-xs">
    <div class="container-fluid">
        <div class="row">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-pause="hover">
                <ol class="carousel-indicators margin-bottom-sm">
                    <?php
                    $sr = 0;
                    foreach ($slider as $slide) {
                        if (is_file('assets/uploads/'.$slide->image)) {
                            echo '<li data-target="#carousel-example-generic" data-slide-to="'.$sr.'" class="'.($sr == 0 ? 'active' : '').'"></li> ';
                            $sr++;
                        }
                    }
                    ?>
                </ol>

                <div class="carousel-inner" role="listbox">
                    <?php
                    $sr = 0;
                    foreach ($slider as $slide) {
                        if (!empty($slide->image)) {
                            echo '<div class="item'.($sr == 0 ? ' active' : '').'">';
                            if (!empty($slide->link)) {
                                echo '<a href="'.$slide->link.'">';
                            }
                            echo '<img src="'.base_url('assets/uploads/'.$slide->image).'" alt="" style="margin-top: -2px;">';
                            if (!empty($slide->caption)) {
                                echo '<div class="carousel-caption">'.$slide->caption.'</div>';
                            }
                            if (!empty($slide->link)) {
                                echo '</a>';
                            }
                            echo '</div>';
                        }
                        $sr++;
                    }
                    ?>
                </div>
<!--
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="fa fa-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only"><?//= lang('prev'); ?></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="fa fa-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only"><?//= lang('next'); ?></span>
                </a>
-->
            </div>
        </div>
    </div>
</section>
<section class="slider-container mobile visible-xs">
    <div class="container-fluid">
        <div class="row">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-pause="hover">
                <ol class="carousel-indicators margin-bottom-sm">
                    <?php
                    $sr = 0;
                    foreach ($slider as $slide) {
                        if (is_file('assets/images/'.$slide->image)) {
                            echo '<li data-target="#carousel-example-generic" data-slide-to="'.$sr.'" class="'.($sr == 0 ? 'active' : '').'"></li> ';
                            $sr++;
                        }
                    }
                    ?>
                </ol>

                <div class="carousel-inner" role="listbox">
                    <?php
                    $sr = 0;
                    foreach ($slider as $slide) {
                        if (!empty($slide->image)) {
                            echo '<div class="item'.($sr == 0 ? ' active' : '').'">';
                            if (!empty($slide->link)) {
                                echo '<a href="'.$slide->link.'">';
                            }
                            echo '<img src="'.base_url('assets/images/'.$slide->image).'" alt="" style="margin-top: -2px;">';
                            if (!empty($slide->caption)) {
                                echo '<div class="carousel-caption">'.$slide->caption.'</div>';
                            }
                            if (!empty($slide->link)) {
                                echo '</a>';
                            }
                            echo '</div>';
                        }
                        $sr++;
                    }
                    ?>
                </div>
<!--
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="fa fa-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only"><?//= lang('prev'); ?></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="fa fa-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only"><?//= lang('next'); ?></span>
                </a>
-->
            </div>
        </div>
    </div>
</section>
<style>
    @media only screen and (max-width:425px){
        .slider-container.mobile{
            margin-bottom:-20px;
        }
    }
</style>
<?php } ?>
<?php
//Slide===============================================================================
?>   