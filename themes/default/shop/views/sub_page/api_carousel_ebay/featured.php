<?php

echo '
    <link rel="stylesheet" href="'.base_url().'themes/default/shop/views/sub_page/api_carousel_ebay/style.css">
';

if ($this->session->userdata('api_mobile') == 1) {
    $api_view_array['col_class'] = 'col-md-4 col-sm-6 col-xs-6';
    $api_view_array['col_class_odd'] = 'api_padding_right_7';
    $api_view_array['col_class_even'] = 'api_padding_left_7';
    $api_view_array['product_class'] = 'api_padding_0';
    $api_view_array['title_class'] = 'api_padding_5';
    $api_view_array['title_style'] = 'min-height:50px; max-height: 50px !important; overflow:hidden;';
    $api_view_array['quantity_class'] = 'api_display_none';
    $api_view_array['price_class_break'] = '<div class="api_clear_both"></div>';
    $api_view_array['price_style'] = 'float: left !important; padding-bottom:0px; padding-left:5px !important; padding-right:5px !important; text-align: left !important; width: 100% !important;';
    $api_view_array['select_qty_style'] = 'width: 100% !important; padding-bottom:0px; padding-left:5px !important; padding-right:5px !important;';
    $api_view_array['label_qty_class'] = 'api_display_none';
    $api_view_array['select_style'] = 'width:100% !important;';
    $api_view_array['btn_class'] = 'api_padding_right_5 api_padding_left_5';
    $api_view_array['btn_break'] = '<div class="api_clear_both api_height_5"></div>';
    $api_view_array['label_btn'] = lang('Add');
    $api_view_array['carousel_col'] = 2;
    $api_view_array['carousel_col_row'] = 2;
    $api_view_array['carousel_col_row_2'] = 2;
    $api_view_array['select_option'] = '';
    
    $api_view_array['sorry'] = '
        <div class="product-price " style=" width: 100% !important; padding-left:5px;">
            <div class="text-red api_padding_bottom_0" style="font-size:12px;">
                Sorry for inconvenient
            </div>
        </div>   
    ';
    $api_view_array['api_ebay_title_mobile'] = 'api_ebay_title_mobile';
    $api_view_array['api_ebay_title_pc'] = '';
} else {
    $api_view_array['title_style'] = 'min-height:40px; max-height: 40px !important; overflow:hidden;';
    $api_view_array['col_class'] = 'col-md-4 col-sm-6';
    $api_view_array['label_btn'] = lang('add_to_cart');
    $api_view_array['carousel_col'] = 3;
    $api_view_array['carousel_col_row'] = 3;
    $api_view_array['carousel_col_row_2'] = 3;
    $api_view_array['carousel_script'] = '';
    $api_view_array['select_option'] = '<option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option>';
    $api_view_array['sorry'] = '
        <div class="product-price " style=" width: 100% !important; height:55px; text-align:center;">
            <div class="text-red api_padding_bottom_5" style="font-size:14px;">
                Sorry for inconvenient
            </div>
        </div>    
    ';
    $api_view_array['api_ebay_title_mobile'] = '';
    $api_view_array['api_ebay_title_pc'] = 'api_title_line_2 api_title_truncate_line_2';
}

$temp_display = '';


//Promotion===============================================================================
if ($this->api_shop_setting[0]['front_promotion'] == 1) {    
    if (is_array($promotion_products)) {
        $config_data = array(
            'id' => 'api_ebay_carousel_promotion',
            'col_class' => 'api_ebay_box_mobile',
        );
        $temp_mobile = $this->site->api_ebay_carousel_display($promotion_products, $config_data, $api_view_array);
        if (count($promotion_products)>0) {
        $temp_display .= '
            <div class="col-md-12 api_padding_left_0 api_padding_right_10">
                <a class="api_link_underline" href="'.base_url().'shop/products?sort_by=promotion-desc" style="color:#fc747a !important;">
                    <h3 class="margin-top-no text-size-lg api_margin_bottom_0">                
                        <img width="25" style="float:left; margin:-10px 10px 0px 0px" class="img-responsive" src="'.base_url('assets/images/hot-item.png').'">
                        '.lang('HOT_ITEM').' <span class="count_item">('.count($promotion_products_total).') <span class="api_screen_hide_768"> '.lang('products').' </span></span>
                    </h3>
                </a>
            </div>
            <div class="col-md-12 api_padding_0">
                <div class="main-content" role="main" tabindex="-1" id="mobile_list'.$config_data['id'].'">
                    '.$temp_mobile.'
                </div>
            </div>
        ';
        }
        $register_script .= "count_products('count_pro_mobile_list".$config_data['id']."');";
    }
}
//Promotion===============================================================================


//Featured===============================================================================
if (is_array($featured_products)) {
    $config_data = array(
        'id' => 'api_ebay_carousel_featured',
        'col_class' => 'api_ebay_box_mobile',
    );
    $temp_mobile = $this->site->api_ebay_carousel_display($featured_products, $config_data, $api_view_array);
    if (count($featured_products)>0) {
    $temp_display .= '
        <div class="col-md-12 api_padding_left_0 api_padding_right_10">
            <a class="api_link_underline" href="'.base_url().'shop/products?sort_by=featured" style="color:#fc747a !important;">
                <h3 class="margin-top-no text-size-lg api_margin_bottom_0">
                    <img width="25" style="float:left; margin:-2px 10px 0px 0px" class="img-responsive image_round_action" src="'.base_url('assets/images/star_1.png').'">
                    '.lang('Popular_And_New').' <span class="count_item">('.count($featured_products_total).')<span class="api_screen_hide_768"> '.lang('products').' </span></span>
                </h3>
            </a>
         
        </div>
        <div class="col-md-12 api_padding_0">
            <div class="main-content" role="main" tabindex="-1" id="mobile_list'.$config_data['id'].'">
                '.$temp_mobile.'
            </div>
        </div>
    ';
    }
    $register_script .= "count_products('count_pro_mobile_list".$config_data['id']."');";
}
//Featured===============================================================================


//Categories===============================================================================
if ($api_view_array['show_main_category'] == 1) {
    for ($i=0;$i<count($main_categories);$i++) {
        if (${'products_'.$main_categories[$i]['id']}) {
            $config_data = array(
                'id' => 'api_ebay_carousel_'.$main_categories[$i]['id'],
                'col_class' => 'api_ebay_box_mobile',
            );
            $temp_mobile = $this->site->api_ebay_carousel_display(${'products_'.$main_categories[$i]['id']}, $config_data, $api_view_array);
            $temp_display .= '
                <div class="col-md-12 api_padding_left_0 api_padding_right_10">
                    <h3 class="margin-top-no text-size-lg api_margin_bottom_0">
            ';
            $product_count = count(${'products_'.$main_categories[$i]['id']});
            if ($product_count > 0) {
                $temp_display .= '
                    <a class="api_link_underline" href="'.base_url().'category/'.$main_categories[$i]['slug'].'" style="color:#fc747a !important;">
                        '.$main_categories[$i]['title_'.$this->api_shop_setting[0]['api_lang_key']].'
                    </a>
                ';             
            }
            $temp_display .= '
                    </h3>
                </div>
                <div class="col-md-12 api_padding_0">
                    <div class="main-content" role="main" tabindex="-1" id="mobile_list'.$main_categories[$i]['id'].'">
                        '.$temp_mobile.'
                    </div>
                </div>
            ';
        }
    }
}
//Categories===============================================================================


echo '
    <section class="page-contents api_padding_bottom_0_im">
        <div class="container">
            '.$temp_display.'
        </div>
    </section>
';
$temp_display = '';
echo '
    <script src="'.base_url().'themes/default/shop/views/sub_page/api_carousel_ebay/inception.js"></script>
    <script src="'.base_url().'themes/default/shop/views/sub_page/api_carousel_ebay/highlnfe.js"></script>
    <Style>
    .carousel__list::-webkit-scrollbar{
        height:3px;
        background-color:#f8f8f8;
    }
    .ghir .carousel:not(.carousel__autoplay) .carousel__list::-webkit-scrollbar-thumb {
        border-color:inherit;
        border-radius:4px;
        border-right-style:inset;
        border-right-width:calc(100vw + 100vh);
        border:0px;

    }
    ::-webkit-scrollbar-thumb{
        background-color:#f86168;
    }
    ::-webkit-scrollbar{
        border:1px solid #ffff55;
    }
    .api_ebay_box {
        background-color: #333 !important;
        border: 1px solid #333 !important;
        margin:0px !important;
        padding:0px !important;
        height:auto !important;
    }
    
    .api_ebay_box .product-price {
        font-size: 1.2em;
        font-weight: 700;
        line-height: 1.2em;
        padding-top:5px;
        padding-bottom:5px;
        padding-left:10px;
        padding-right:10px;
        text-align: left;
    }
    .api_ebay_box .product-cart-button .btn-group .btn-theme {
        width: calc(100% - 40px) !important;
    }
    .api_ebay_box .btn-group, .btn-group-vertical {
        display: block;
    }
    
    .api_ebay_box_mobile .fa-shopping-cart{
        padding-right:0px !important;
    }
    
    .api_ebay_box_mobile .input-group-addon {
        padding: 6px 7px !important;
    }
    .api_ebay_box_mobile .btn {
        padding: 6px 6px !important;
    }
    .api_ebay_box_mobile .product-cart-button .btn-group .btn-theme {
        width: calc(100% - 27px) !important;
    }
    .api_kata_tip_table_img{
        min-height:140px;
    }  
    .api_ebay_box_mobile .api_absolute_center{
        top:25%;
    }
    .api_ebay_title_mobile {
        line-height: 18px;
        height: 2.25rem;
        overflow:hidden;
        font-size:12px !important;        
        height: 2.25rem;
        white-space: normal;
        -webkit-line-clamp: 2;
        -webkit-box-orient: vertical;
        -moz-box-orient: vertical;
        -moz-line-clamp: 2;
        display: -webkit-box;
        white-space: normal;
        overflow: hidden;
        text-overflow: ellipsis;
        line-height: 18px;
        color: #333;
    }    
    .image_round_action{
        -webkit-animation:spin 4s linear infinite;
        -moz-animation:spin 4s linear infinite;
        animation:spin 4s linear infinite;
    }
    @-moz-keyframes spin { 100% { -moz-transform: rotate(360deg); } }
    @-webkit-keyframes spin { 100% { -webkit-transform: rotate(360deg); } }
    @keyframes spin { 100% { -webkit-transform: rotate(360deg); transform:rotate(360deg); } }
    </style>
';

?>




