
<?php

$temp_display = '';

$form_name = 'frm_set_information';
$attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => $form_name, 'name' => $form_name);
$temp_display .= form_open_multipart("main/api_facebook_register", $attrib);

$temp_display .= '
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="form-group">
                    '.lang("name", "name").'
                    '.form_input('name', '', 'class="form-control" required="required"').'
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <div class="form-group">
                    '.lang("phone", "phone").'
                    '.form_input('phone', '', 'class="form-control" required="required"').'
                </div>
            </div>
        </div>
';

if ($this->session->userdata('api_social_email') == '')
$temp_display .= '
        <div class="col-md-12">
            <div class="form-group">
                <div class="form-group">
                    '.lang("email", "email").'
                    '.form_input('email', '', 'class="form-control" required="required"').'
                </div>
            </div>
        </div>        
';
$temp_display .= '
                    <div class="col-md-12">
                        <div class="form-group">
                            '.lang("City", "City").' 
';
                               $config_data = array(
                                    'none_label' => lang("Select_a_city"),
                                    'table_name' => 'sma_city',
                                    'space' => ' &rarr; ',
                                    'strip_id' => '',        
                                    'field_name' => 'title_'.$this->api_shop_setting[0]['api_lang_key'],
                                    'condition' => 'order by title_'.$this->api_shop_setting[0]['api_lang_key'].' asc',
                                    'condition_parent' => ' and parent_id = 268',
                                    'translate' => 'yes',
                                    'no_space' => 1,
                                );                        
                                $this->site->api_get_option_category($config_data);
                                $temp_option = $_SESSION['api_temp'];
                                for ($i=0;$i<count($temp_option);$i++) {                        
                                    $temp = explode(':{api}:',$temp_option[$i]);
                                    $temp_10 = '';
                                    if ($temp[0] != '') {
                                        $config_data_2 = array(
                                            'id' => $temp[0],
                                            'table_name' => 'sma_city',
                                            'field_name' => 'title_'.$this->api_shop_setting[0]['api_lang_key'],
                                            'parent_id' => '268',
                                            'translate' => 'yes',
                                        );
                                        $_SESSION['api_temp'] = array();
                                        $this->site->api_get_category_arrow($config_data_2);          
                                        for ($i2 = count($_SESSION['api_temp']) - 1; ; $i2--) {
                                            if ($i2 == 0) {
                                                break;
                                            }
                                            $temp_arrow = '';
                                            if ($i2 > 1)
                                                $temp_arrow = ' &rarr; ';
                                            $temp_10 .= $_SESSION['api_temp'][$i2].$temp_arrow;
                                        }   
                                    }
                                    $tr_city[$temp[0]] = $temp_10.$temp[1];
                                }
                                $temp_display .= "".form_dropdown('city_id', $tr_city, $customer->city_id, 'class="form-control" required="required"')."";
$temp_display .= '      </div>
                    </div> 
';
$temp_display .= '
        <div class="col-md-12">
            <div class="form-group">
                '.lang("Delivery_Address", "address").'
                '.form_textarea('address', '', 'class="form-control" required="required" style="height: 100px;"').'
            </div>
        </div>                
    </div>
';

echo '
<button type="button" id="api_modal_set_information_trigger" class="api_display_none" data-backdrop="static" data-toggle="modal" data-target="#api_modal_set_information">Open Modal</button>
<!-- Modal -->
<div id="api_modal_set_information" class="modal fade" role="dialog" style="margin-top:100px;">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title api_modal_title">
            '.lang('information_for_delivery_service').'
        </h4>
      </div>
      <div class="modal-body">
        '.$temp_display.'
      </div>
      <div class="modal-footer">
        '.form_submit('submit', lang('Login'), 'class="btn btn-primary"').'
        <button type="button" class="btn btn-danger" data-dismiss="modal">
            '.lang('Cancel').'
        </button>        
      </div>
    </div>
  </div>
</div>
';

echo form_close();

if ($_GET['api_action'] == 'need_more_informations#_=_' || $_GET['api_action'] == 'need_more_informations')
    echo '
<script>
    document.frm_set_information.name.value = "'.$this->session->userdata('api_social_name').'";
    $("#api_modal_set_information_trigger").click();
</script>    
    ';
?>