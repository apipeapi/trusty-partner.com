<?php
echo '
<script>
function initMap() {     
';

for ($i=0;$i<count($google_data);$i++) {
echo '
  const myLatlng_'.$google_data[$i]['id'].' = { lat: '.$google_data[$i]['initial_lat'].', lng: '.$google_data[$i]['initial_lng'].'};
  const map_'.$google_data[$i]['id'].' = new google.maps.Map(document.getElementById("map_'.$google_data[$i]['id'].'"), {
    zoom: 18,
    center: myLatlng_'.$google_data[$i]['id'].',
  });
  // Create the initial InfoWindow.
  let infoWindow_'.$google_data[$i]['id'].' = new google.maps.InfoWindow({
    content: "'.$google_data[$i]['initial_content'].'",
    position: myLatlng_'.$google_data[$i]['id'].',
  });
  infoWindow_'.$google_data[$i]['id'].'.open(map_'.$google_data[$i]['id'].');
  // Configure the click listener.
  map_'.$google_data[$i]['id'].'.addListener("click", (mapsMouseEvent) => {
    // Close the current InfoWindow.
    infoWindow_'.$google_data[$i]['id'].'.close();
    // Create a new InfoWindow.
    infoWindow_'.$google_data[$i]['id'].' = new google.maps.InfoWindow({
      position: mapsMouseEvent.latLng,
    });
    var temp = JSON.stringify(mapsMouseEvent.latLng.toJSON(), null, 2);
    var obj = JSON.parse(temp);          

    //-Calculate-Distance-------------------------
    const geocoder = new google.maps.Geocoder();
    const service = new google.maps.DistanceMatrixService();
    const origin1 = { lat: '.$google_data[$i]['shop_lat'].', lng: '.$google_data[$i]['shop_lng'].'};
    const destinationA = { lat: obj["lat"], lng: obj["lng"] };
    service.getDistanceMatrix(
    {
      origins: [origin1],
      destinations: [destinationA],
      travelMode: google.maps.TravelMode.DRIVING,
      unitSystem: google.maps.UnitSystem.METRIC,
      avoidHighways: false,
      avoidTolls: false,
    },
    (response, status) => {
      if (status !== "OK") {
        alert("Error was: " + status);
      } else {
        $("#google_address_'.$google_data[$i]['id'].'").val(response.destinationAddresses[0]);
        //console.log(response.destinationAddresses[0]);    
        //console.log(response.rows[0].elements[0]["distance"]["text"]);
      }

      var temp_delevery_fee = calculate_delivery_fee(response.rows[0].elements[0]["distance"]["text"]);

      temp_delevery_fee = formatMoney(temp_delevery_fee);
      temp_delevery_fee = temp_delevery_fee.replace("$$","$");
      infoWindow_'.$google_data[$i]['id'].'.setContent(
        "<div><b>Address: </b>" + response.destinationAddresses[0] + "<div><b>Your Pin Location: </b>" + + obj["lat"] + " , " + obj["lng"] + "<div> <b>" + "Distance from our restaurant: </b> " + response.rows[0].elements[0]["distance"]["text"] + "</div> <div><b>Delivery Fee: </b>" + temp_delevery_fee + "</div>"
      );  
      $("#add_ons_google_location_'.$google_data[$i]['id'].'").val(obj["lat"] + ", " + obj["lng"]);
      $("#add_ons_distance_from_restaurant_'.$google_data[$i]['id'].'").val(response.rows[0].elements[0]["distance"]["text"]);
      $("#google_location_error_'.$google_data[$i]['id'].'").hide();
      infoWindow_'.$google_data[$i]['id'].'.open(map_'.$google_data[$i]['id'].');
    }
    );
    //-Calculate-Distance-------------------------
  });
';
}


echo '
}
</script>

<script
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBNuX93TO6QiReEGliaysdDUtiEULNlWGE&callback=initMap&libraries=&v=weekly"
async
></script>
';

                  
