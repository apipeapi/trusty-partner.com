<?php
//Promotion===============================================================================
if ($this->api_shop_setting[0]['front_promotion'] == 1) {
?>   
<section class="page-contents api_padding_bottom_0_im">
    <div class="container">
        <div class="row">

            <div class="col-xs-12">

                <div class="row">         
                    <?php
                    if (is_array($promotion_products)) {
                        if (count($promotion_products) > 0) {
                            for ($i_3=0;$i_3<count($promotion_products);$i_3++) {
                                $config_data = array(
                                    'id' => $promotion_products[$i_3]->id,
                                );
                                $temp_hide = $this->shop_model->api_get_product_display($config_data);
                                if ($temp_hide['result'] == 'hide') {
                                    unset($promotion_products[$i_3]);
                                }
                            }
                            $temp_arr = array_values($promotion_products); 
                            $promotion_products = $temp_arr;                            
                        }                        
                    ?>                           
                    <div class="col-xs-9">
                        <h3 class="margin-top-no text-size-lg">
                            <?= lang('Promotional_Products'); ?>
                        </h3>
                    </div>
                    <?php
                    }
                    ?>
                    <?php
                    if (is_array($promotion_products))
                    if (count($promotion_products) > $api_view_array['carousel_col']) {
                        ?>
                        <div class="col-xs-3">
                            <div class="controls pull-right ">
                                <a class="left fa fa-chevron-left btn btn-xs btn-default" id="carousel-promotion_prev" href="#carousel-promotion"
                                data-slide="prev"></a>
                                <a class="right fa fa-chevron-right btn btn-xs btn-default" id="carousel-promotion_next" href="#carousel-promotion"
                                data-slide="next"></a>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>

                <div id="carousel-promotion" class="carousel slide" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <?php
                        if ($promotion_products) { 
                            $r = 0;
                            foreach (array_chunk($promotion_products, $api_view_array['carousel_col_row']) as $fps) {

                                ?>
                                <div class="item row <?= empty($r) ? 'active' : ''; ?>" style="min-height: 315px !important; max-height: 315px !important;">
                                    <div class="">
                                        <?php
                                        $k = 1;
                                        foreach ($fps as $fp) {
if (($k%2) != 0) $class_record = $api_view_array['col_class_odd']; else $class_record = $api_view_array['col_class_even'];                                         
                                            
$temp_sold_out = array();
$temp_sold_out[3] = '<option value="1">1</option>';   
if ($this->api_shop_setting[0]['out_of_stock'] == 1) {
    if ($fp->quantity <= 0) {
        $temp_sold_out[0] = 'api_opacity_6';
        $temp_sold_out[1] = '
            <div class="api_absolute_center">
                <div class="api_sold_out_tag" style="visibility:hidden;">
                    Sold Out
                </div>
            </div>      
        ';
        $temp_sold_out[2] = 'disabled';
        $temp_sold_out[3] = '<option value="0">0</option>';
        $temp_sold_out[4] = 'api_display_none';
        $temp_sold_out[5] = $api_view_array['sorry'];
    }
}                                        
                                            ?>

<div class="page-products product-container <?= $api_view_array['col_class'].' '.$class_record; ?>">
    <div class="product api_margin_bottom_0 <?= $api_view_array['product_class']; ?>">        
        <div class="product-top"> 
            <div class="product-image">
                <a href="<?= site_url('product/'.$fp->slug); ?>">
<?php 
    if (is_file('assets/uploads/'.$fp->image))
        echo '
            <img class="img-responsive '.$temp_sold_out[0].'" src="'.base_url('assets/uploads/'.$fp->image).'" alt="">
        ';
    else
        echo '
            <img class="img-responsive '.$temp_sold_out[0].'" src="'.base_url('assets/uploads/no_image.png').'" alt="">
        ';
    echo $temp_sold_out[1];
?>
                </a>                
            </div>
            <div class="product-desc <?= $api_view_array['title_class']; ?>" style="<?php echo $api_view_array['title_style']; ?>">
                <div class="product_name" style="font-weight:700;">
                    <a href="<?= site_url('product/'.$fp->slug); ?>"><?= $fp->name; ?></a>
                </div>
                <p></p>          
            </div>            
        </div>
        <div class="clearfix"></div>
             
        <div class="product-bottom" style="padding-top: 0px; margin-top:0px;">

            <?php if (!$shop_settings->hide_price) { ?>
                <div class="product-price" style="<?= $api_view_array['price_style']; ?> width: 100% !important;">
                    <?php        
                        $config_data = array(
                            'id' => $fp->id,
                            'customer_id' => $this->session->userdata('company_id'),
                        );
                        $temp = $this->site->api_calculate_product_price($config_data);               

                        if ($this->sma->isPromo_v3($fp)) {
                            echo '
                                <del class="text-red">'.$this->sma->convertMoney($temp['temp_price']).'</del> 
                                '.$this->sma->convertMoney($temp['price']).'
                            ';
                        }
                        else
                            echo $this->sma->convertMoney($temp['price']);
                    ?>
                </div>
            <?php } ?>        
            
            <?= $api_view_array['price_class_break']; ?>
            <?= $temp_sold_out[5]; ?>
            <div class=" <?= $temp_sold_out[4]; ?>" style="<?= $api_view_array['select_qty_style']; ?>" style="width: 100% !important;">
                <div class="form-group" style="margin-bottom:5px;">
                    <div class="input-group">                
<span class="input-group-addon pointer btn-minus api_pause_carousel"><span class="fa fa-minus"></span></span>
<input type="tel" name="quantity" class="form-control text-center api_numberic_input quantity-input api_pause_carousel" value="1" required="required">
<span class="input-group-addon pointer btn-plus api_pause_carousel"><span class="fa fa-plus"></span></span>
                    </div>
                </div>        
            </div>        
            <div class="clearfix"></div>
<?php
    if ($fp->is_favorite == 1) $temp_is_favorite = 'fa-heart'; else $temp_is_favorite = 'fa-heart-o';
?>            
            <div class="product-cart-button <?= $temp_sold_out[4]; ?> <?= $api_view_array['btn_class']; ?>">
                <div class="btn-group" role="group" aria-label="...">
                    <button class="btn btn-info add-to-wishlist" data-id="<?= $fp->id; ?>"><i id="category_wishlist_heart_<?php echo $fp->id; ?>" class="fa <?php echo $temp_is_favorite; ?>"></i></button>
<?php
    $temp_display_1 = '';
    if ($fp->is_ordered != 1) { 
        if ($this->session->userdata('user_id')) {
            $temp_display_1 = 'api_display_none';
            echo '
                <button class="btn btn-theme " '.$temp_sold_out[2].' data-toggle="modal" data-target="#api_modal_add_cart" onclick="api_add_cart_confirm(\'product_featured_'.$fp->id.'\');"><i class="fa fa-shopping-cart padding-right-md"></i> '.$api_view_array['label_btn'].'</button>
            ';
        }
    }
?>
                    <button class="btn btn-theme add-to-cart <?php echo $temp_display_1; ?>" <?= $temp_sold_out[2]; ?> data-id="<?= $fp->id; ?>" id="product_featured_<?= $fp->id; ?>" ><i class="fa fa-shopping-cart padding-right-md"></i> <?= $api_view_array['label_btn']; ?></button>

                </div>
            </div>
            <?= $api_view_array['btn_break']; ?>            
            
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>                                                    

                                            <?php
                                            $k++;
                                        }
                                        ?>
                                    </div>
                                </div>
                                <?php
                                $r++;
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
<?php
}
//Promotion===============================================================================
?>                       



<?php
//Featured===============================================================================
?>   
<section class="page-contents api_padding_bottom_0_im">
    <div class="container">
        <div class="row">

            <div class="col-xs-12">

                <div class="row">         
                    <?php
                    if (is_array($featured_products)) {
                        if (count($featured_products) > 0) {
                            for ($i_3=0;$i_3<count($featured_products);$i_3++) {
                                $config_data = array(
                                    'id' => $featured_products[$i_3]->id,
                                );
                                $temp_hide = $this->shop_model->api_get_product_display($config_data);
                                if ($temp_hide['result'] == 'hide') {
                                    unset($featured_products[$i_3]);
                                }
                            }
                            $temp_arr = array_values($featured_products); 
                            $featured_products = $temp_arr;                            
                        }                        
                    ?>                           
                    <div class="col-xs-9">
                        <h3 class="margin-top-no text-size-lg">
                            <?= lang('featured_products'); ?>
                        </h3>
                    </div>
                    <?php
                    }
                    ?>
                    <?php
                    if (is_array($featured_products))
                    if (count($featured_products) > $api_view_array['carousel_col']) {
                        ?>
                        <div class="col-xs-3">
                            <div class="controls pull-right ">
                                <a class="left fa fa-chevron-left btn btn-xs btn-default" id="carousel-example_prev" href="#carousel-example"
                                data-slide="prev"></a>
                                <a class="right fa fa-chevron-right btn btn-xs btn-default" id="carousel-example_next" href="#carousel-example"
                                data-slide="next"></a>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>

                <div id="carousel-example" class="carousel slide" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <?php
                        if ($featured_products) { 
                            $r = 0;
                            foreach (array_chunk($featured_products, $api_view_array['carousel_col_row']) as $fps) {

                                ?>
                                <div class="item row <?= empty($r) ? 'active' : ''; ?>" style="min-height: 315px !important; max-height: 315px !important;">
                                    <div class="">
                                        <?php
                                        $k = 1;
                                        foreach ($fps as $fp) {
if (($k%2) != 0) $class_record = $api_view_array['col_class_odd']; else $class_record = $api_view_array['col_class_even'];                                         
                                            
$temp_sold_out = array();
$temp_sold_out[3] = '<option value="1">1</option>';   
if ($this->api_shop_setting[0]['out_of_stock'] == 1) {
    if ($fp->quantity <= 0) {
        $temp_sold_out[0] = 'api_opacity_6';
        $temp_sold_out[1] = '
            <div class="api_absolute_center">
                <div class="api_sold_out_tag" style="visibility:hidden;">
                    Sold Out
                </div>
            </div>      
        ';
        $temp_sold_out[2] = 'disabled';
        $temp_sold_out[3] = '<option value="0">0</option>';
        $temp_sold_out[4] = 'api_display_none';
        $temp_sold_out[5] = $api_view_array['sorry'];
    }
}                                        
                                            ?>

<div class="page-products product-container <?= $api_view_array['col_class'].' '.$class_record; ?>">
    <div class="product api_margin_bottom_0 <?= $api_view_array['product_class']; ?>">        
        <div class="product-top"> 
            <div class="product-image">
                <a href="<?= site_url('product/'.$fp->slug); ?>">
<?php 
    if (is_file('assets/uploads/'.$fp->image))
        echo '
            <img class="img-responsive '.$temp_sold_out[0].'" src="'.base_url('assets/uploads/'.$fp->image).'" alt="">
        ';
    else
        echo '
            <img class="img-responsive '.$temp_sold_out[0].'" src="'.base_url('assets/uploads/no_image.png').'" alt="">
        ';
    echo $temp_sold_out[1];
?>
                </a>                
            </div>
            <div class="product-desc <?= $api_view_array['title_class']; ?>" style="<?php echo $api_view_array['title_style']; ?>">
                <div class="product_name" style="font-weight:700;">
                    <a href="<?= site_url('product/'.$fp->slug); ?>"><?= $fp->name; ?></a>
                </div>
                <p></p>          
            </div>            
        </div>
        <div class="clearfix"></div>
             
        <div class="product-bottom" style="padding-top: 0px; margin-top:0px;">

            <?php if (!$shop_settings->hide_price) { ?>
                <div class="product-price" style="<?= $api_view_array['price_style']; ?> width: 100% !important;">
                    <?php        
                        $config_data = array(
                            'id' => $fp->id,
                            'customer_id' => $this->session->userdata('company_id'),
                        );
                        $temp = $this->site->api_calculate_product_price($config_data);               

                        if ($this->sma->isPromo_v3($fp)) {
                            echo '
                                <del class="text-red">'.$this->sma->convertMoney($temp['temp_price']).'</del> 
                                '.$this->sma->convertMoney($temp['price']).'
                            ';
                        }
                        else
                            echo $this->sma->convertMoney($temp['price']);
                    ?>
                </div>
            <?php } ?>        
            
            <?= $api_view_array['price_class_break']; ?>
            <?= $temp_sold_out[5]; ?>
            <div class=" <?= $temp_sold_out[4]; ?>" style="<?= $api_view_array['select_qty_style']; ?>" style="width: 100% !important;">
                <div class="form-group" style="margin-bottom:5px;">
                    <div class="input-group">                
<span class="input-group-addon pointer btn-minus api_pause_carousel"><span class="fa fa-minus"></span></span>
<input type="tel" name="quantity" class="form-control text-center api_numberic_input quantity-input api_pause_carousel" value="1" required="required">
<span class="input-group-addon pointer btn-plus api_pause_carousel"><span class="fa fa-plus"></span></span>
                    </div>
                </div>        
            </div>        
            <div class="clearfix"></div>
<?php
    if ($fp->is_favorite == 1) $temp_is_favorite = 'fa-heart'; else $temp_is_favorite = 'fa-heart-o';
?>            
            <div class="product-cart-button <?= $temp_sold_out[4]; ?> <?= $api_view_array['btn_class']; ?>">
                <div class="btn-group" role="group" aria-label="...">
                    <button class="btn btn-info add-to-wishlist" data-id="<?= $fp->id; ?>"><i id="category_wishlist_heart_<?php echo $fp->id; ?>" class="fa <?php echo $temp_is_favorite; ?>"></i></button>
<?php
    $temp_display_1 = '';
    if ($fp->is_ordered != 1) { 
        if ($this->session->userdata('user_id')) {
            $temp_display_1 = 'api_display_none';
            echo '
                <button class="btn btn-theme " '.$temp_sold_out[2].' data-toggle="modal" data-target="#api_modal_add_cart" onclick="api_add_cart_confirm(\'product_featured_'.$fp->id.'\');"><i class="fa fa-shopping-cart padding-right-md"></i> '.$api_view_array['label_btn'].'</button>
            ';
        }
    }
?>
                    <button class="btn btn-theme add-to-cart <?php echo $temp_display_1; ?>" <?= $temp_sold_out[2]; ?> data-id="<?= $fp->id; ?>" id="product_featured_<?= $fp->id; ?>" ><i class="fa fa-shopping-cart padding-right-md"></i> <?= $api_view_array['label_btn']; ?></button>

                </div>
            </div>
            <?= $api_view_array['btn_break']; ?>            
            
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>                                                    

                                            <?php
                                            $k++;
                                        }
                                        ?>
                                    </div>
                                </div>
                                <?php
                                $r++;
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
<?php
//Featured===============================================================================
?>                       

