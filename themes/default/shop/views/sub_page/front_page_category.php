<?php
$config_data_2 = array(
    'table_name' => 'sma_product_promotion',
    'select_table' => 'sma_product_promotion',
    'translate' => '',
    'select_condition' => "id > 0 and status = 'enabled' order by ordering asc",
);
$temp = $this->site->api_select_data_v2($config_data_2);
$temp_display = '';
for ($i=0;$i<count($temp);$i++) {
    $temp_display .= '
        <div class="col-md-12 api_temp_col_2">
            <a class="" href="'.base_url().'shop/products?search=&sort_by=promotion-desc&promotion_id='.$temp[$i]['id'].'">
                <img class="img-responsive api_link_box_none" src="'.base_url().'assets/uploads/'.$temp[$i]['image'].'"/>
            </a>
        ';
        if ($i < count($temp) - 1)
            $temp_display .= ' 
                <div class="api_height_15 api_clear_both"></div>
            ';
    $temp_display .= '
        </div>
    ';
}

for ($i=0;$i<count($list_categories);$i++) {
    $data_view = array (
        'wrapper_class' => 'hidden-sm hidden-xs',
        'file_path' => 'assets/uploads/thumbs/'.$list_categories[$i]['image'],
        'max_width' => 400,
        'max_height' => 400,
        'product_link' => 'size',
        'image_class' => '',
        'image_id' => '',
        'resize_type' => 'size',
    );
    $temp = $this->api_helper->api_get_image($data_view);
    $data_view = array (
        'wrapper_class' => 'visible-sm',
        'file_path' => 'assets/uploads/thumbs/'.$list_categories[$i]['image'],
        'max_width' => 300,
        'max_height' => 250,
        'product_link' => 'size',
        'image_class' => '',
        'image_id' => '',
        'resize_type' => 'size',
    );
    $temp_sm = $this->api_helper->api_get_image($data_view);    
    $data_view = array (
        'wrapper_class' => 'visible-xs',
        'file_path' => 'assets/uploads/thumbs/'.$list_categories[$i]['image'],
        'max_width' => 400,
        'max_height' => 400,
        'product_link' => 'size',
        'image_class' => 'img-responsive',
        'image_id' => '',
        'resize_type' => 'full',
    );
    $temp_mobile = $this->api_helper->api_get_image($data_view);

    $temp_category .= '
        <a class="api_link_box_none " href="category/'.$list_categories[$i]['slug'].'">
            <div class="col-md-4 col-sm-4 col-xs-6 api_padding_bottom_15 api_padding_top_15">
                <div class="col-md-12  api_padding_0">
                    <div class="bg_front">
                        '.$list_categories[$i]['title_'.$this->api_shop_setting[0]['api_lang_key']].'
                    </div>
                </div>
                '.$temp['image_table'].'
                '.$temp_sm['image_table'].'
                '.$temp_mobile['image_table'].'
            </div>
        </a>             
    ';
}
// echo $list_categories[1]['slug'];
    echo '
        <section class="page-contents api_padding_bottom_0_im">
            <div class="container api_padding_0">
                <div class="api_height_15 api_clear_both visible-xs"></div>
                '.$temp_display.' 
                '.$temp_category.' 
            </div> 
        </section>
    ';

?>

<style>
.bg_front{
    background-image: url(assets/images/opacity.png);
    background-repeat: repeat;
    padding: 10px; 
    position: absolute;
    top:0; 
    width: 100%; 
    font-size: 24px;
    text-align: center;
    color: #e7d139;
    text-transform: uppercase;
    font-weight: bold;
}
.api_link_box_none:hover{
    background-color: #979799;
}
@media screen and (min-width: 100px) and (max-width:767px) {
    .bg_front{
        font-size: 15px;
        padding:5px;
    }
    .col-xs-6{
        padding-right: 5px;
        padding-left: 5px;
    }
}
</style>