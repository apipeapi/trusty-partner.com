<style>
    @media screen and (min-width: 426px){
		 /* start category */
		#slider {
			position: relative;
			overflow: hidden;
			margin: 20px auto 0 auto;
			border-radius: 4px;
			/*overflow: auto;*/
			white-space: nowrap;
		}

		#slider ul {
			position: relative;
			margin: 0;
			padding: 0;
			list-style: none;
		}

		#slider ul li {
			position: relative;
			display: block;
			float: left;
			margin: 0;
			padding: 0;
			text-align: center;
		}
		#slider::-webkit-scrollbar{
			width: 0;
			height: 0;
			background-color: transparent;
			visibility: hidden;
		}

		a.control_prev_cat, a.control_next_cat {
			z-index: 999;
			display: block;
			text-decoration: none;
			font-weight: 600;
			font-size: 18px;
			background-color: #fe0001;
			height: 70px;
			width: 35px;
			border: 1px solid rgba(0,0,0,0.3);
			color: black;
			cursor: pointer;
			top: 30%;
			position: absolute;
			opacity: .9;
			padding-top:21px;
			padding-left: 12px;
			transition: background-color .1s ease-out,border .5s ease-out;
		}

		a.control_prev_cat:hover, a.control_next_cat:hover {
			opacity: 1;
			-webkit-transition: all 0.2s ease;
		}

		a.control_prev_cat {
			border-radius: 0 2px 2px 0;
			left: 0;
		}

		a.control_next_cat {
			right: -10px;
			border-radius: 2px 0 0 2px;
		}
		.scrollcategory::-webkit-scrollbar{
			width: 0;
			height: 0;
			background-color: transparent;
			visibility: hidden;
		}
		.category-element {
		    width: 33.3333%;
		}
		.scrollcategory ul{
			margin-left: 0px !important;
		}
		.scrollcategory li{
			display: inline-block;
			margin: 24px auto 0 5px;
			text-align: center;
			padding: 0x;
			text-decoration: none;
			max-width: calc(127px + 0.915%);
		}
		.scrollcategory li img{
			width: 127px;
			height: 127px;
			border-radius: 50%;
			object-fit: cover;
			object-position: 40% 0;
		}
		.category-image.rounded{
			width: 127px;
			border-radius: 50%;
			margin-left: 5px;
		}
		.category-element h5{
			font-weight: bold;
			font-size: 11px;
			width: auto;
			left: 0;
			position: relative;
			transform: translate(-5px, 0px);
		}
	}
	@media screen and (max-width: 425px){
		#slider{
			left: -10px;
		}
		a.control_prev_cat, a.control_next_cat{
			display: none;
			/* margin-left: 5%; */
		}
		.scrollcategory::-webkit-scrollbar {
			width: 0;
			height: 0;
			background-color: transparent;
		}
		.category-element {
		    width: 25%;
		}
		.scrollcategory{
			overflow: auto;
			white-space: nowrap;
		}
		.scrollcategory ul{
			margin-left: -68px !important;
		}
		.scrollcategory li{
			display: inline-block;
			margin: 0px auto 0 19px;
			text-align: center;
			padding: 0px;
			text-decoration: none;
			max-width: calc(100px + 0.915%);
		}
		.scrollcategory li img{
			width: 100px;
			height: 100px;
			border-radius: 50%;
		}
		.category-image.rounded{
			width: 100px;
			border-radius: 50%;
			margin-left: -15px;
		}
		.category-element h5{
			font-weight: bold;
			font-size: 11px;
			width: auto;
			left: 0;
			position: relative;
			transform: translate(-17px, 0px);
		}
		h3.margin-top-no.text-size-lg.mobile{
			margin-left: -15px;
		}
		section#api_slider_featured_wrapper.page-contents.mobile{
			padding-top:15px;
			margin-bottom:-27px !important;
		}
		
	}
	/* end category */
	.list_category{
		margin-left:10px !important;
	}
	.style_title_category{
		font-weight: bold;
		color: #db9803!important;
		font-size: 13px;
		white-space: normal;
		overflow: hidden;
		text-overflow: ellipsis;
		height: 40px;
		margin-top: 5px;
	}
</style>
<section id="api_slider_featured_wrapper" class="page-contents mobile">
	<div class="container">
		<div class="col-md-12">
			<!-- <img width="25" style="float:left; margin:-2px 10px 0 0" class="img-responsive image_round_action" src="'.base_url('assets/images/star_1.png').'"> -->
			<h3 class="margin-top-no text-size-lg mobile">
				<?=lang('categories')?>
			</h3>
		</div>  
		
		<div class="col-md-12 scrollcategory" >
			<a target-slide="slider" class="slick-prev slick-arrow control_prev_cat position-fixed"><span class="fa fa-chevron-left api_margin_top_5"></span></a>
			<a target-slide="slider" class="slick-next slick-arrow control_next_cat position-fixed"><span class="fa fa-chevron-right api_margin_top_5"></span></a>
			
			<div  id="slider">
				<ul class="group-list">
					<?php 
						foreach ($list_categories as $key => $value) {
							if ($value['image'] == '')
								$image = base_url('assets/uploads/no_image.png');
							else
								$image = base_url("assets/uploads/".$value["image"]);
							
							$data_view = array (
							'wrapper_class' => '',
							'file_path' => 'assets/uploads/'.$list_categories[$key]['image'],
							'max_width' => 150,
							'max_height' => 135,
							'product_link' => '',
							'image_class' => 'api_link_box_none',
							'image_id' => '',   
							'resize_type' => 'full',
							);
							$temp_image = $this->site->api_get_image($data_view);	
					?>

					<li class="list_category">
						<a class="category-link" href="<?=base_url('category/'.$value['slug'])?>">
							<div class="col-md-12 api_padding_0">
								<div class="main-content" role="main" tabindex="-1">
									<?php echo $temp_image['image_table'];?>
								</div>
								<div class="category-name style_title_category" ><?=$list_categories[$key]['title_'.$this->api_shop_setting[0]['api_lang_key']]?></div> 
							</div>
						</a>
					</li>
					<?php } ?>
				</ul>
				
		</div>
		
	</div>
</section>
<script>
	var slideCount = $('#slider ul li').length;
    var slideWidth = $('#slider ul li').width();
    var slideHeight = $('#slider ul li').height();
    var sliderUlWidth = slideCount * slideWidth+125;
	// $('.scrollcategory')
	if(slideCount<10){
		$('.scrollcategory a.control_prev_cat').remove();
		$('.scrollcategory a.control_next_cat').remove();
	}
	slideHeight = slideHeight>=159?slideHeight:159;
    $('#slider').css({ width: 'calc( 100% + 20px )', height: slideHeight });
    $('#slider ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });
    $('#slider ul li:last-child').prependTo('#slider ul');
    function moveLeft() {
        $('#slider ul').animate({
            left: + slideWidth
        }, 200, function () {
            $('#slider ul li:last-child').prependTo('#slider ul');
            $('#slider ul').css('left', '');
        });
    };
    function moveRight() {
        $('#slider ul').animate({
            left: - slideWidth
        }, 200, function () {
            $('#slider ul li:first-child').appendTo('#slider ul');
            $('#slider ul').css('left', '');
        });
    };
</script>
