<?php
$temp_display .= '
<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
<style>
    #map {
        '.$config_data['map_css'].'
    }
</style>
<script>
    function initMap() {
    const bounds = new google.maps.LatLngBounds();
    const markersArray = [];
';
$temp = explode(',',$config_data['origin_coordinate']);
$temp_2 = explode(',',$config_data['destination_coordinate']);
$temp_display .= '
    const origin1 = { lat: '.$temp[0].', lng: '.$temp[1].' };        
    const destinationA = { lat: '.$temp_2[0].', lng: '.$temp_2[1].' };
    const destinationIcon =
        "https://chart.googleapis.com/chart?" +
        "chst=d_map_pin_letter&chld=D|FF0000|000000";
    const originIcon =
        "https://chart.googleapis.com/chart?" +
        "chst=d_map_pin_letter&chld=O|FFFF00|000000";
    const map = new google.maps.Map(document.getElementById("map"), {
        center: { lat: 11.5408896, lng: 104.9133056 },
        zoom: 10,
    });
    const geocoder = new google.maps.Geocoder();
    const service = new google.maps.DistanceMatrixService();
    service.getDistanceMatrix(
        {
        origins: [origin1],
        destinations: [destinationA],
        travelMode: google.maps.TravelMode.DRIVING,
        unitSystem: google.maps.UnitSystem.METRIC,
        avoidHighways: false,
        avoidTolls: false,
        },
        (response, status) => {
        if (status !== "OK") {
            alert("Error was: " + status);
        } else {
            const originList = response.originAddresses;
            const destinationList = response.destinationAddresses;              
            deleteMarkers(markersArray);

            const showGeocodedAddressOnMap = function (asDestination) {
            const icon = asDestination ? destinationIcon : originIcon;

            return function (results, status) {
                if (status === "OK") {
                map.fitBounds(bounds.extend(results[0].geometry.location));
                markersArray.push(
                    new google.maps.Marker({
                    map,
                    position: results[0].geometry.location,
                    icon: icon,
                    })
                );
                } else {
                alert("Geocode was not successful due to: " + status);
                }
            };
            };

            for (let i = 0; i < originList.length; i++) {
            const results = response.rows[i].elements;
            geocoder.geocode(
                { address: originList[i] },
                showGeocodedAddressOnMap(false)
            );

            for (let j = 0; j < results.length; j++) {
                geocoder.geocode(
                { address: destinationList[j] },
                showGeocodedAddressOnMap(true)
                );
                $("#map_result").html(results[0]["distance"]["text"]);
                //console.log(results);
                
            }
            }
        }
        }
    );
    }

    function deleteMarkers(markersArray) {
    for (let i = 0; i < markersArray.length; i++) {
        markersArray[i].setMap(null);
    }
    markersArray = [];
    }
</script>
      

<div id="map"></div>
<div id="map_result"></div>


<script
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBNuX93TO6QiReEGliaysdDUtiEULNlWGE&callback=initMap&libraries=&v=weekly"
  async
></script>

';

?>