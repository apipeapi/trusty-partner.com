<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
<script>
      function initMap() {
          const myLatlng = { lat: 11.540472386283318, lng: 104.91391459014407 };
          const map = new google.maps.Map(document.getElementById("map"), {
            zoom: 16,
            center: myLatlng,
          });
          // Create the initial InfoWindow.
          let infoWindow = new google.maps.InfoWindow({
            content: "<i class='fa fa-map-marker'></i><span class='text-bold' style='padding-left: 5px;'>Our Shop <br> Please select your location</span>",
            position: myLatlng,
          });
          infoWindow.open(map);
          // Configure the click listener.
          map.addListener("click", (mapsMouseEvent) => {
            // Close the current InfoWindow.
            infoWindow.close();
            // Create a new InfoWindow.
            infoWindow = new google.maps.InfoWindow({
              position: mapsMouseEvent.latLng,
            });
            infoWindow.setContent(
              JSON.stringify(mapsMouseEvent.latLng.toJSON(), null, 2)
            );
            infoWindow.open(map);
            var temp = JSON.stringify(mapsMouseEvent.latLng.toJSON(), null, 2);
            var obj = JSON.parse(temp);
            $('#add_ons_google_location').val(obj['lat'] + ', ' + obj['lng']);
            $('#add_ons_google_location_hidden').val(obj['lat'] + ', ' + obj['lng']);
            // console.log(obj);
          //   console.log(obj['lat'] + ' = ' + obj['lng']);
          });
      }

      function handleLocationError(browserHasGeolocation, infoWindow, pos) {
          infoWindow.setPosition(pos);
          infoWindow.setContent(
              browserHasGeolocation
              ? "Error: The Geolocation service failed."
              : "Error: Your browser doesn't support geolocation."
      );
      infoWindow.open(map);
}
</script>
<style>
#map {
    width:100%;
    height:400px;
}
</style>
<script
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBNuX93TO6QiReEGliaysdDUtiEULNlWGE&callback=initMap&libraries=&v=weekly"
  async
></script>
<div id="map"></div>