<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript">if (parent.frames.length !== 0) { top.location = '<?= site_url(); ?>'; }</script>
    <title><?= $page_title; ?></title>
    <meta name="description" content="<?= $page_desc; ?>">
    <link rel="shortcut icon" href="<?= base_url().'assets/uploads/logos/'.$this->api_shop_setting[0]['icon']; ?>">
    <link rel="apple-touch-icon" href="<?= base_url().'assets/uploads/logos/'.$this->api_shop_setting[0]['apple_touch_icon']; ?>">    
    <meta property="og:url" content="<?= isset($product) && !empty($product) ? site_url('product/'.$product->slug) : site_url(); ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<?= $page_title; ?>" />
    <meta property="og:description" content="<?= $page_desc; ?>" />
    <meta property="og:image" content="<?= isset($product) && !empty($product) ? base_url('assets/uploads/'.$product->image) : base_url('assets/uploads/logos/'.$shop_settings->logo); ?>" />
    <meta property="fb:pages" content="131617807536765" />
<?php

    echo '<link href="'.$assets.'css/libs.min.css" rel="stylesheet">';

    if (base_url() == 'https://air.phsarjapan.com/') {
        echo '<link href="'.$assets.'css/styles.min_blood.css" rel="stylesheet">';
    } else {
        echo '<link href="'.$assets.'css/styles.min_'.$this->api_shop_setting[0]['shop_color'].'.css" rel="stylesheet">';
    }

    echo '<link href="'.base_url().'/assets/api/css/public.css" rel="stylesheet">';
    echo '<link href="'.base_url().'/assets/api/css/front_end.css" rel="stylesheet">';
    echo '<link href="'.base_url().'assets/api/plugin/menu_mobile/css/jquery.mmenu.all.css" type="text/css" rel="stylesheet">';
    echo '<link href="'.base_url().'assets/api/plugin/menu_mobile/css/style.css" type="text/css" rel="stylesheet">';
    // echo '<link href="'.$assets.'css/boostrap.min.css" type="text/css" rel="stylesheet">';


//if (!is_int(strpos($_SERVER['REQUEST_URI'],"cart/checkout")) )
    echo '
        <script src="'.$assets.'js/libs.min.js"></script>
    ';

?>
    <style>
        .btn-group-payment{
            padding: 13px 0;
            font-weight: 700;
            font-size: 18px;
        }
        .btn-b-fixed{
            position: fixed;
            bottom: 0;
            width: 100%;
            left: 0;
            z-index: 9999;
        }
        .btn-t-fixed{
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            z-index: 9999;
        }
        .btn-scrolltop{
            margin-bottom: 55px;
            padding: 4px;
            width: 35px;
            height: 35px;
        }
        .api_sold_out_tag{
            padding:0 9px;
        }
        a.btn > i.fa.fa-facebook,a.btn > i.fa.fa-google {
            border: 1px solid rgb(255, 255, 255);
            padding: 3px;
            border-radius: 20px;
            width: 23px;
            margin-left: -6px;
            margin-right: 10px;
        }
        span.count_item{
            font-size: 17px;
        }
        .product .product-bottom .product-cart-button .btn-group .btn-theme {
            width: calc(100% - 40px) !important;
        }
    </style>

</head>
<body>
<div id="loader-wrapper">
    <div id="loader"></div>
</div>
<?php
    $language_array = unserialize(multi_language);
    for ($i=0;$i<count($language_array);$i++) {    
        if (strtolower($language_array[$i][1]) == $Settings->user_language) {
            $this->api_shop_setting[0]['api_lang_key'] = $language_array[$i][0];
            break;
        }
    }
    include 'themes/default/shop/views/sub_page/api_modal.php';
    include 'themes/default/shop/views/sub_page/api_modal_2.php';
    include 'themes/default/shop/views/sub_page/api_cart_modal.php';    
?>

<?php
    $useragent = $_SERVER['HTTP_USER_AGENT'];
    if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iPad|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
        $this->session->set_userdata('api_mobile', 1);
    } else {
        $this->session->set_userdata('api_mobile', 0);
    }

    if ($this->session->userdata('company_id') > 0) {
        $config_data = array(
            'table_name' => 'sma_companies',
            'select_table' => 'sma_companies',
            'translate' => '',
            'select_condition' => "id = ".$this->session->userdata('company_id'),
        );
        $api_initial_user_company = $this->site->api_select_data_v2($config_data);
        $this->session->set_userdata('sale_consignment_auto_insert', $api_initial_user_company[0]['sale_consignment_auto_insert']);
    }
?>

<section id="wrapper" class="red ">
    <?php
        // include('themes/default/shop/views/sub_page/top_header.php');
        include('themes/default/shop/views/api_template/header/'.$this->api_shop_setting[0]['header'].'.php');
    ?>

    <div class="" id="api_header_sticky">        
    </div>
    <?php if (DEMO && ($m != 'main' || $v != 'index')) { ?>
    <div class="page-contents padding-bottom-no">
        <div class="container">
            <div class="alert alert-info margin-bottom-no">
            </div>
        </div>
    </div>
    <?php } ?>
<?php
if (!$loggedIn) {
        echo '        
<script>
    $("#api_cart_item_bar").hide();
</script>
    ';
    }
?>

</head>
<body>

<?php
echo $this->api_menu_mobile->view($l, $api_web, $api_template, $api_page, $config_data);
?>

<?php
if ($this->api_shop_setting[0]['facebook_login'] == 1 && $this->session->userdata('user_id') == '' && $v != 'login') {
    // <div class="fb-login-button api_margin_top_4 api_padding_top_10" data-width="" data-size="large" data-button-type="login_with" data-layout="rounded" data-auto-logout-link="false" data-use-continue-as="true" scope="public_profile,email" onlogin="checkLoginState();" ></div>

    $temp_display = '     
    <div class="container-fluid" style="background-color:#f8f8f8 !important;">
        <div class="container api_padding_0">
            <div class="col-md-12 api_text_align_right api_padding_left_0 api_padding_right_0 api_padding_top_20">
                <table width="100%" border="0">
                <tr>
                <td valign="middle" align="right">
                    <strong>'.lang('Sign With').'</strong>: &nbsp;
                </td>
    ';

    // if ($this->api_shop_setting[0]['require_login'] != 1) {
    //     $temp_display .= '
    //         <div class="api_float_right api_margin_top_3 api_margin_left_5">
    //             <a href="'.base_url().'login?register=1">
    //                 <button type="button" value="register" name="register" class="btn btn-lg btn-info api_padding_top_5 api_padding_left_15 api_padding_right_15" style="border-radius:10px; height:37px;">'.lang('Register').'</button>
    //             </a>
    //         </div>
    //     ';
    // }
                // $temp_display .= '
                //     <div class="api_margin_right_5 api_login_'.strtolower($key).' api_margin_top_5 api_link_box_none">
                //         <a href="'.site_url('social_auth/login/'.$key).'">
                //             <i aria-hidden="true" class="fa fa-'.strtolower($key).' api_color_white"></i>
                //             <span class="api_color_white">Login With '.$key.'</span>
                //         </a>
                //     </div>
                // ';    
    if (!$shop_settings->private) {
        $providers = config_item('providers');
        foreach ($providers as $key => $provider) {
            if ($provider['enabled']) {
                if (strtolower($key) == 'facebook') {
                    $temp_display .= '
                        <td valign="middle" align="right" width="42px">
                            <a href="'.site_url('social_auth/login/'.$key).'" title="Sign with '.$key.'">
                                <img class="api_link_box_none" src="'.base_url().'assets/api/image/api_login_'.strtolower($key).'.png" width="32px;" height="32px;" />
                            </a>
                        </td>
                    ';
                    break;
                }
            }
        }
        foreach ($providers as $key => $provider) {
            if ($provider['enabled']) {
                if (strtolower($key) == 'google') {
                    $temp_display .= '
                        <td valign="middle" align="right" width="42px">
                            <a href="'.site_url('social_auth/login/'.$key).'" title="Sign with '.$key.'">
                                <img class="api_link_box_none" src="'.base_url().'assets/api/image/api_login_'.strtolower($key).'.png" width="32px;" height="32px;" />
                            </a>
                        </td>
                    ';
                    break;
                }
            }
        }        
    }

    $temp_display .= '
                </tr>
                </table>
                <div class="api_height_15 api_clear_both"></div>
            </div>
        </div>
    </div>
    ';

    echo $temp_display;

}



if ($this->api_shop_setting[0]['holiday_message'] == 1 && trim($this->api_shop_setting[0]['shop_message']) != '') {
    echo '
        <div class="container-fluid" style="background-color:#f8f8f8 !important;">
            <div class="container api_padding_0">
                <br>
                <div class="col-md-12 text-red">
                    '.$this->api_shop_setting[0]['shop_message'].'
                </div>
                <div class="api_clear_both api_height_15"></div>
            </div>
        </div>
    ';
}




?>

