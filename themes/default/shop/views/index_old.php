<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php if(!empty($slider)) { ?>
<section class="slider-container">
    <div class="container-fluid">
        <div class="row">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators margin-bottom-sm">
                    <?php
                    $sr = 0;
                    foreach ($slider as $slide) {
                        if (!empty($slide->image)) {
                            echo '<li data-target="#carousel-example-generic" data-slide-to="'.$sr.'" class="'.($sr == 0 ? 'active' : '').'"></li> ';
                        }
                        $sr++;
                    }
                    ?>
                </ol>

                <div class="carousel-inner" role="listbox">
                    <?php
                    $sr = 0;
                    foreach ($slider as $slide) {
                        if (!empty($slide->image)) {
                            echo '<div class="item'.($sr == 0 ? ' active' : '').'">';
                            if (!empty($slide->link)) {
                                echo '<a href="'.$slide->link.'">';
                            }
                            echo '<img src="'.base_url('assets/uploads/'.$slide->image).'" alt="">';
                            if (!empty($slide->caption)) {
                                echo '<div class="carousel-caption">'.$slide->caption.'</div>';
                            }
                            if (!empty($slide->link)) {
                                echo '</a>';
                            }
                            echo '</div>';
                        }
                        $sr++;
                    }
                    ?>
                </div>

                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="fa fa-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only"><?= lang('prev'); ?></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="fa fa-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only"><?= lang('next'); ?></span>
                </a>
            </div>
        </div>
    </div>
</section>
<?php } ?>

<section class="page-contents">
    <div class="container">
        <div class="row">

            <div class="col-xs-12">

                <div class="row">
                    <div class="col-xs-9">
                        <h3 class="margin-top-no text-size-lg">
                            <?= lang('featured_products'); ?>
                        </h3>
                    </div>
                    <?php
                    if (count($featured_products) > 3) {
                        ?>
                        <div class="col-xs-3">
                            <div class="controls pull-right hidden-xs">
                                <a class="left fa fa-chevron-left btn btn-xs btn-default" href="#carousel-example"
                                data-slide="prev"></a>
                                <a class="right fa fa-chevron-right btn btn-xs btn-default" href="#carousel-example"
                                data-slide="next"></a>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>

                <div id="carousel-example" class="carousel slide" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <?php
                        if ($featured_products) { 
                            $r = 0;
                            foreach (array_chunk($featured_products, 3) as $fps) {
                                ?>
                                <div class="item row <?= empty($r) ? 'active' : ''; ?>">
                                    <div class="">
                                        <?php
                                        foreach ($fps as $fp) {
                                            ?>

<div class="page-products product-container col-sm-6 col-md-4">
    <div class="product">        
        <div class="product-top">
            <div class="product-image">
                <a href="<?= site_url('product/'.$fp->slug); ?>">
                    <img class="img-responsive" src="<?= base_url('assets/uploads/'.$fp->image); ?>" alt="">
                </a>
            </div>
            <div class="product-desc">
                    <span class="product_name" style="font-size: 15px; font-weight:700;">
                        <a href="<?= site_url('product/'.$fp->slug); ?>"><?= $fp->name; ?></a>
                    </span>
                    <p></p>          
            </div>            
        </div>
        <div class="clearfix"></div>
             
        <div class="product-bottom">

            <?php if (!$shop_settings->hide_price) { ?>
                <div class="product-price">
                    <?php
                    if ($fp->promotion) {
                        echo '<del class="text-red">'.$this->sma->convertMoney(isset($fp->special_price) && !empty(isset($fp->special_price)) ? $fp->special_price : $fp->price).'</del><br>';
                        echo $this->sma->convertMoney($fp->promo_price);
                    } else {
                        echo $this->sma->convertMoney(isset($fp->special_price) && !empty(isset($fp->special_price)) ? $fp->special_price : $fp->price);
                    }
                    ?>
                </div>
            <?php } ?>        

            <div class="product-rating">
                <div class="form-group" style="margin-bottom:0;">
                    <div class="" style="display:inline-block;margin:auto;">        
                        <div style="float:left;padding-right:10px;line-height:34px; font-size:14px;">Quantity</div>
                        <div style="float:left;">
                            <select name="quantity" class="form-control quantity-input">
                                <option value="1">1</option><option value="2">2</option>
                                <option value="3">3</option><option value="4">4</option>
                                <option value="5">5</option><option value="6">6</option>
                                <option value="7">7</option><option value="8">8</option>
                                <option value="9">9</option><option value="10">10</option>
                            </select>
                        </div>        
                    </div>        
                </div>        
            </div>        
            <div class="clearfix"></div>

            <div class="product-cart-button">
                <div class="btn-group" role="group" aria-label="...">
                    <button class="btn btn-info add-to-wishlist" data-id="<?= $fp->id; ?>"><i class="fa fa-heart-o"></i></button>
                    <button class="btn btn-theme add-to-cart" data-id="<?= $fp->id; ?>"><i class="fa fa-shopping-cart padding-right-md"></i> <?= lang('add_to_cart'); ?></button>
                </div>
            </div>                        

            <div class="clearfix"></div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>                                                    

                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <?php
                                $r++;
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>

                       
<?php
    for ($i=0;$i<count($main_categories);$i++) {
        if (${'products_'.$main_categories[$i]['id']}) {
?>
			<div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-9">
                        <h3 class="margin-top-no text-size-lg text-capitalize">
                            <?= $main_categories[$i]['name']; ?>
                        </h3>
                    </div>
                    <?php
                    if (count(${'products_'.$main_categories[$i]['id']}) > 3) {
                        ?>
                        <div class="col-xs-3">
                            <div class="controls pull-right hidden-xs">
                                <a class="left fa fa-chevron-left btn btn-xs btn-default" href="<?php echo '#carousel-'.$main_categories[$i]['id']; ?>"
                                data-slide="prev"></a>
                                <a class="right fa fa-chevron-right btn btn-xs btn-default" href="<?php echo '#carousel-'.$main_categories[$i]['id']; ?>"
                                data-slide="next"></a>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                
                <div id="<?php echo 'carousel-'.$main_categories[$i]['id']; ?>" class="carousel slide" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <?php
                        if (${'products_'.$main_categories[$i]['id']}) {
                            $r = 0;
                            foreach (array_chunk(${'products_'.$main_categories[$i]['id']}, 3) as $fps) {
                                ?>
                                <div class="item row <?= empty($r) ? 'active' : ''; ?>">
                                    <div class="featured-products">
                                        <?php
                                        foreach ($fps as $fp) {
                                            ?>
                                            <div class="col-sm-6 col-md-4">
                                                <div class="product" style="z-index: 1;">
                                                    <div class="details" style="transition: all 100ms ease-out 0s;">
                                                        <?php
                                                        if ($fp->promotion) {
                                                            ?>
                                                            <span class="badge badge-right theme"><?= lang('promo'); ?></span>
                                                            <?php
                                                        }
                                                        ?>
                                                        <img src="<?= base_url('assets/uploads/'.$fp->image); ?>" alt="">
                                                        <?php if (!$shop_settings->hide_price) { ?>
                                                        <div class="image_overlay"></div>
                                                        <div class="btn add-to-cart" data-id="<?= $fp->id; ?>"><i class="fa fa-shopping-cart"></i> <?= lang('add_to_cart'); ?></div>
                                                        <?php } ?>

                                                        <?php if (!$shop_settings->hide_price) { ?>
                                                            <span class="product_price">
                                                                <?php
                                                                if ($fp->promotion) {
                                                                    echo '<del class="text-red">'.$this->sma->convertMoney(isset($fp->special_price) && !empty(isset($fp->special_price)) ? $fp->special_price : $fp->price).'</del><br>';
                                                                    echo $this->sma->convertMoney($fp->promo_price);
                                                                } else {
                                                                    echo $this->sma->convertMoney(isset($fp->special_price) && !empty(isset($fp->special_price)) ? $fp->special_price : $fp->price);
                                                                }
                                                                ?>
                                                            </span>
                                                        <?php } ?>
                                                        <div class="stats-container">
                                                            
                                                            <span class="product_name">
                                                                <a href="<?= site_url('shop/product/'.$fp->slug); ?>"><?= $fp->name; ?></a>
                                                            </span>
                                                            <a href="<?= site_url('category/'.$fp->category_slug); ?>" class="link text-capitalize"><?= $fp->category_name; ?></a>
                                                            <?php
                                                            if ($fp->brand_name) {
                                                                ?>
                                                                <span class="link">-</span>
                                                                <a href="<?= site_url('brand/'.$fp->brand_slug); ?>" class="link text-capitalize"><?= $fp->brand_name; ?></a>
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <?php
                                $r++;
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
			
			
<?php
        }
    }
?>			
	   </div>
    </div>
</section>
