<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style>
    .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control{
        background-color: black;
        color: #dc9b01 !important;
        cursor: not-allowed;
    }
    input[type="text"]:focus,
    select:focus,
    textarea:focus {
        border-color: #dc9b01 !important;
    }
    .has-success .form-control{
        border-color: #dc9b01;
    }
    .has-success .form-control:focus{
        box-shadow:inset 0 1px 1px rgb(0 0 0 / 8%), 0 0 6px black !important;
    }
    input:-webkit-autofill,
    input:-webkit-autofill:hover, 
    input:-webkit-autofill:focus,
    select:-webkit-autofill,
    select:-webkit-autofill:focus  {
        border: 1px solid #dc9b01;
        -webkit-text-fill-color: #dc9b01;
        transition: background-color 5000s ease-in-out 0s;
    }
</style>
<section class="page-contents api_padding_0_mobile">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-sm-9 col-md-10 api_padding_right_0_pc">
                        
                        <div class="col-md-12 api_padding_0_mobile">
                            <h4 class="margin-top-md title text-bold">
                                <span><i class="fa fa-user margin-right-sm"></i> <?= lang('profile'); ?></span>            
                                <div id="api_customer_menu_panel_btn" class="api_float_right api_pointer api_screen_show_768" onclick="$('#api_customer_menu_panel').toggle('slideToggle?');" style="top: -10px; margin-top: -15px;">
                                    <button class="btn btn-danger navbar-btn"><div class="fa fa-bars fa-lg"></div></button>
                                </div>
                            </h4>
                        </div>

                        <div id="api_customer_menu_panel" class="col-md-2 api_padding_right_0_pc api_padding_0_mobile api_screen_hide_768">
                            <div class="api_height_10 api_screen_hide_768"></div>
                            <?php include('themes/default/shop/views/pages/customer_menu_panel.php'); ?>                            
                        </div>
                        <div class="col-md-10 api_padding_0_mobile">
                            <div class="api_height_13"></div>
                            
<?php
    if ($this->session->userdata('user_id') > 0) {
        $temp = $this->site->api_select_some_fields_with_where("
            activation_code, active
            "
            ,"sma_users"
            ,"id = ".$this->session->userdata('user_id')
            ,"arr"
        );        
    }

    if ($this->api_shop_setting[0]['require_login'] != 1) {
        $temp_display_1 = 'api_display_none';
        $temp_display_2 = '<label class="label label-danger api_float_right">'.lang('We_can_delivery_in_Phnom_Penh_Only').'</label>';
    }

?>                            
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#user" aria-controls="user" role="tab" data-toggle="tab" ><?= lang('details'); ?></a></li>
        <li role="presentation"><a href="#password" aria-controls="password" role="tab" data-toggle="tab" ><?= lang('change_password'); ?></a></li>
        <?php 
            // if ($temp[0]['active'] != 1 && $this->api_shop_setting[0]['require_login'] != 1) {
            if ($temp[0]['active'] == 0) { 
        ?>
        <li role="presentation"><a href="#verify_email" aria-controls="verify_email" role="tab" data-toggle="tab" id="verify_email_address"><?= lang('Verify_Email_Address'); ?></a></li>
        <?php } ?>
    </ul>
    <div class="tab-content padding-lg white bordered-light" style="margin-top:-1px;">
        <div role="tabpanel" class="tab-pane fade in active" id="user">
            <p><?= lang('fill_form'); ?></p>
            <?= form_open("profile/user", 'class="validate"'); ?>
            <div class="row">
                <div class="col-md-6" id="first_name_required">
                    <div class="form-group">
                        <?= lang('first_name', 'first_name'); ?>
                        <?= form_input('first_name', set_value('first_name', $user->first_name), 'class="form-control tip" id="first_name" required="required"'); ?>
                    </div>
                </div>
                <div class="col-md-6" id="last_name_required">
                    <div class="form-group">
                        <?= lang('last_name', 'last_name'); ?>
                        <?= form_input('last_name', set_value('last_name', $user->last_name), 'class="form-control tip" id="last_name" required="required"'); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6" id="phone_required">
                    <div class="form-group">
                        <?= lang('phone', 'phone'); ?>
                        <?= form_input('phone', set_value('phone', $customer->phone), 'class="form-control tip" id="phone" required="required"'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang('email', 'email'); ?>
                        <?= form_input('email', set_value('email', $this->session->userdata('email')), 'class="form-control tip"  id="email" readonly="readonly"'); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group <?= $temp_display_1; ?>">
                        <?= lang('company', 'company'); ?>
                        <?= form_input('company', set_value('company', $customer->company), 'class="form-control tip" id="company"'); ?>
                    </div>                    
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 api_display_none">
                    <div class="form-group">
                        <?= lang("city", "city"); ?>
                        <?= form_input('city', set_value('city', $customer->city), 'class="form-control tip" id="city"'); ?>
                    </div>
                </div>
                <div class="col-md-12 color_red_change api_display_none">
                    <div class="form-group">
                        <?= lang("city", "city"); ?>
                        <?php
                            $config_data = array(
                                'none_label' => lang("Select_a_city"),
                                'table_name' => 'sma_city',
                                'space' => ' &rarr; ',
                                'strip_id' => '',        
                                'field_name' => 'title_'.$this->api_shop_setting[0]['api_lang_key'],
                                'condition' => 'order by title_'.$this->api_shop_setting[0]['api_lang_key'].' asc',
                                'condition_parent' => ' and parent_id = 268',
                                'translate' => 'yes',
                                'no_space' => 1,
                            );                        
                            $this->site->api_get_option_category($config_data);
                            $temp_option = $_SESSION['api_temp'];
                            for ($i=0;$i<count($temp_option);$i++) {                        
                                $temp = explode(':{api}:',$temp_option[$i]);
                                $temp_10 = '';
                                if ($temp[0] != '') {
                                    $config_data_2 = array(
                                        'id' => $temp[0],
                                        'table_name' => 'sma_city',
                                        'field_name' => 'title_'.$this->api_shop_setting[0]['api_lang_key'],
                                        'parent_id' => '268',
                                        'translate' => 'yes',
                                    );
                                    $_SESSION['api_temp'] = array();
                                    $this->site->api_get_category_arrow($config_data_2);          
                                    for ($i2 = count($_SESSION['api_temp']) - 1; ; $i2--) {
                                        if ($i2 == 0) {
                                            break;
                                        }
                                        $temp_arrow = '';
                                        if ($i2 > 1)
                                            $temp_arrow = ' &rarr; ';
                                        $temp_10 .= $_SESSION['api_temp'][$i2].$temp_arrow;
                                    }   
                                }
                                $tr_city[$temp[0]] = $temp_10.$temp[1];
                            }
                            echo form_dropdown('city_id', $tr_city, $customer->city_id, 'class="form-control"');
                        ?>
                    </div>
                </div>
                <div class="col-md-6 api_display_none ">
                    <div class="form-group">
                        <?= lang("state", "state"); ?>
                        <?php
                        if ($Settings->indian_gst) {
                            $states = $this->gst->getIndianStates(true);
                            echo form_dropdown('state', $states, set_value('state', $customer->state), 'class="form-control selectpicker mobile-device" id="state" ');
                        } else {
                            echo form_input('state', set_value('state', $customer->state), 'class="form-control" id="state"');
                        }
                        ?>
                    </div>
                </div>                
            </div>

            <div class="row">
                <div class="col-md-6 api_display_none">
                    <div class="form-group">
                        <?= lang('country', 'country'); ?>
                        <?= form_input('country', set_value('country', $customer->country), 'class="form-control tip" id="country" '); ?>
                    </div>
                </div>
                
                
            </div>
            <div class="row">
<?php
    if ($this->api_shop_setting[0]['google_map_api'] == 1)
        $temp = 'required="required" readonly="readonly"';
    else
        $temp = 'required="required"';
?>
                <div class="col-md-6" id="delivery_address_required">
                    <div class="form-group color_red_change">
                        <?= lang('Delivery_Address', 'address'); ?>
                        <?= form_input('address', $customer->address, 'class="form-control  tip" id="google_address_1" '.$temp); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang('Note_to_Driver', 'Note_to_Driver'); ?>
                        <?= form_input('add_ons_note_to_driver', $customer_v2[0]['note_to_driver'], 'class="form-control tip" '); ?>
                    </div>
                </div>                

                <?php
//-google-map-----------------
if ($this->api_shop_setting[0]['google_map_api'] == 1) {
    echo '
        <div class="col-md-12" id="google_map_pin_required">
            '.lang('google_map_pin', 'google_map_pin').'
    ';
    $temp_google_function_id = '1';
    $temp_distance_from_restaurant = $customer_v2[0]['distance_from_restaurant'];
    $temp_google_location = $customer_v2[0]['google_location'];
    $temp = explode(',',$this->api_shop_setting[0]['google_location']);
    $temp_shop_lat = $temp[0];
    $temp_shop_lng = $temp[1];
    if ($temp_google_location == '') {
        $temp_initial_content = '<b>SUSHI DONMARU Restaurant</b><div><b>Pin Location: </b>'.$this->api_shop_setting[0]['google_location'].'</div>';
        $temp_initial_lat = $temp_shop_lat;
        $temp_initial_lng = $temp_shop_lng;
    }
    else {
        $temp_fee = $this->api_helper->calculate_delivery_fee($temp_distance_from_restaurant);
        $temp_initial_content = '<div><b>Delivery Address: </b>'.$customer_v2[0]['address'].'<div><b>Your Pin Location: </b>'.$temp_google_location.'<div> <b>Distance from our restaurant: </b>'.$temp_distance_from_restaurant.'</div> <div><b>Delivery Fee: </b>'.$this->sma->formatMoney($temp_fee).'</div>';

        $temp = explode(',',$temp_google_location);
        $temp_initial_lat = $temp[0];
        $temp_initial_lng = $temp[1];    
    }
    $google_data[0] = array(
        'id' => $temp_google_function_id,
        'initial_content' => $temp_initial_content,
        'initial_lat' => $temp_initial_lat,
        'initial_lng' => $temp_initial_lng,
        'shop_lat' => $temp_shop_lat,
        'shop_lng' => $temp_shop_lng,    
    );

    echo '
    <div class="col-md-6 api_display_none">
    <input type="text" name="add_ons_google_location" id="add_ons_google_location_'.$temp_google_function_id.'" value="'.$temp_google_location.'" style="width:100%;">
    </div>
    <div class="col-md-6 api_display_none">
    <input type="text" name="add_ons_distance_from_restaurant" id="add_ons_distance_from_restaurant_'.$temp_google_function_id.'" value="'.$temp_distance_from_restaurant.'" style="width:100%;">
    </div>
    <div class="col-md-12 api_padding_0 api_display_none" id="google_location_error_'.$temp_google_function_id.'" style="color:red;">
        '.lang('Please_pin_your_location_on_the_map').'
        <div class="api_height_10"></div>
    </div>
    <div id="map_1" style="width:100%; height:400px;"></div>
    ';
    include 'themes/default/shop/views/sub_page/api_google_map_distance.php';                
    echo '
            <div class="api_height_15 api_clear_both"></div>
        </div>
    ';
}    
//-google-map-----------------
?>

                <?php
                    $config_data = array(
                        'table_name' => 'sma_companies',
                        'select_table' => 'sma_companies',
                        'translate' => '',
                        'select_condition' => "id = ".$customer->id,
                    );
                    $select_data = $this->site->api_select_data_v2($config_data);
                ?>
                
                    
            </div>
            <?= $temp_display_2; ?> 

            <?= form_submit('billing', lang('update'), 'class="btn btn-primary api_display_none" id="submit_update"'); ?>
            <?= form_button('billing', lang('update'), 'class="btn btn-primary" onclick="update_profile()"'); ?>
            <?php echo form_close(); ?>        
        </div>

        <div role="tabpanel" class="tab-pane fade" id="password">
            <p><?= lang('fill_form'); ?></p>
            <?= form_open("profile/password", 'class="validate"'); ?>
            <div class="row">
                <div class="col-md-6">
                    <?php $u = mt_rand().'_2';?>
                    <div class="form-group">
                        <?= lang('current_password', 'old_password'); ?>
                        <?= form_password('old_password', set_value('old_password'), 'class="form-control tip" id="old_password'.$u.'" required="required"'); ?>
                        <span toggle="#old_password<?= $u;?>" id="api_from_login_eye_1" class="fa fa-fw fa-eye field-icon toggle-password api_pointer" onclick="api_toggle_old_password();"></span>
                    </div>
                    <div class="form-group">
                        <?= lang('user_new_password', 'user_new_password'); ?>
                        <?= form_password('new_password', set_value('new_password'), 'class="form-control tip" id="new_password'.$u.'" required="required" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" data-fv-regexp-message="'.lang('pasword_hint').'"'); ?>
                        <span toggle="#new_password<?= $u;?>" id="api_from_login_eye_2" class="fa fa-fw fa-eye field-icon toggle-password api_pointer" onclick="api_toggle_password();"></span>
                    </div>

                    <div class="form-group">
                        <?= lang('confirm_password', 'new_password_confirm'); ?>
                        <?= form_password('new_password_confirm', set_value('new_password_confirm'), 'class="form-control tip" id="new_password_confirm'.$u.'" required="required" data-fv-identical="true" data-fv-identical-field="new_password" data-fv-identical-message="'.lang('pw_not_same').'"'); ?>
                        <span toggle="#new_password_confirm<?= $u;?>" id="api_from_login_eye_3" class="fa fa-fw fa-eye field-icon toggle-password api_pointer" onclick="api_toggle_confirm_password();"></span>
                    </div>

<?php
    echo '
        <button type="button" class="btn btn-default" onclick="
        var postData = {
            \'website_address\' : \''.base_url().'\',
            \'close\' : \''.lang('Close').'\',
            \'use_this_password\' : \''.lang('use_this_password').'\',
            \'generated_password\' : \''.lang('generated_password').'\',
        };        
        api_generate_password(postData);
        ">
            '.lang('Generate_Password').'
        </button>
        <div class="api_height_30"></div>
    ';
?>
                    <?= form_submit('change_password', lang('change_password'), 'class="btn btn-primary"'); ?>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>

        <?php 
            // if ($temp[0]['active'] != 1 && $this->api_shop_setting[0]['require_login'] != 1) { 
            if ($temp[0]['active'] != 0) {
        ?>
        <div role="tabpanel" class="tab-pane fade" id="verify_email">
            <div class="row">
                <div class="col-md-12">
                    <?php
                        echo '
                            <b>'.lang('Email_Activation_Link').'</b>
                            <br><br>
                            '.lang('text_activation') .' <b>'. $this->session->userdata('email').'</b> '.lang('text_activation_2').'
                            <br><br>
                            '.lang('text_activation_3').'
                            <br><br>
                            <a href="'.base_url().'shop/api_resend_email_activation_link">
                                <button class="btn btn-primary">
                                    '.lang('Resend_Email_Activation_Link').'
                                </button>
                            </a>
                        ';
                    ?>

                </div>
            </div>
        </div>
        <?php } ?>
        
    </div>


                        </div>
                    </div>

                    <div class="col-sm-3 col-md-2">
                        <?php include(b'themes/default/shop/views/pages/sidebar2.php'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<script>
if($('#distance_from_our_restaurant').val() == '')
   $('#distance_from_our_restaurant').text('?');
if($('#address_delivery_fee').val() == '')
   $('#address_delivery_fee').text('$0.00');
   
function api_use_this_password(){
    var id = '<?php echo $u;?>';
    document.getElementById('new_password'+ id).value = document.getElementById('api_modal_body').innerHTML;
    document.getElementById('new_password_confirm'+ id).value = document.getElementById('api_modal_body').innerHTML;
}
function update_profile() {
    var error = 0;
    if($('#first_name').val() == '') {
        $('#first_name_required').scrollView();
        error = 1;
    } else if($('#last_name').val() == '') {
        $('#last_name_required').scrollView();
        error = 1;
    } else if($('#phone').val() == '') {
        $('#phone_required').scrollView();
        error = 1;
    } else if($('#address').val() == '') {
        $('#delivery_address_required').scrollView();
        error = 1;
    } 

    if(error == 1)
        $('#submit_update').click();
    else {
        google_map();
    }
}
function google_map() {
    var google_id = '<?php echo $temp_google_function_id;?>';
    if($('#add_ons_google_location_'+ google_id).val() == '') {
        $('#google_map_pin_required').scrollView();
        $('#google_location_error_'+ google_id).removeClass('api_display_none');
    }  else {
        $('#submit_update').click();
    }
}

var id = '<?php echo $u;?>';
function api_toggle_old_password() {
    if ($("#api_from_login_eye_1").hasClass("fa-eye")) {
        $("#api_from_login_eye_1").removeClass("fa-eye");
        $("#api_from_login_eye_1").addClass("fa-eye-slash");
        $("#old_password"+ id).attr("type","text");
    }
    else {
        $("#api_from_login_eye_1").addClass("fa-eye");
        $("#api_from_login_eye_1").removeClass("fa-eye-slash");
        $("#old_password"+ id).attr("type","password"); 
    }
}
function api_toggle_password(){
    if ($("#api_from_login_eye_2").hasClass("fa-eye")) {
        $("#api_from_login_eye_2").removeClass("fa-eye");
        $("#api_from_login_eye_2").addClass("fa-eye-slash");
        $("#new_password"+ id).attr("type","text");
    }
    else {
        $("#api_from_login_eye_2").addClass("fa-eye");
        $("#api_from_login_eye_2").removeClass("fa-eye-slash");
        $("#new_password"+ id).attr("type","password");  
    }
}
function api_toggle_confirm_password() {
    if ($("#api_from_login_eye_3").hasClass("fa-eye")) {
        $("#api_from_login_eye_3").removeClass("fa-eye");
        $("#api_from_login_eye_3").addClass("fa-eye-slash");
        $("#new_password_confirm"+ id).attr("type","text");
    }
    else {
        $("#api_from_login_eye_3").addClass("fa-eye");
        $("#api_from_login_eye_3").removeClass("fa-eye-slash");
        $("#new_password_confirm"+ id).attr("type","password"); 
    }
}
</script>
<?php 
    if($_GET['verify_email_address'] == 1)  {
        echo '<script>
                document.getElementById("verify_email_address").click();
            </script>';
    }
?>