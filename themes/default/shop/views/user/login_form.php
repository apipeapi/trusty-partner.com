<style>
    input[type="text"]:focus,
    input[type="email"]:focus,
    input[type="password"]:focus,
    select:focus,
    textarea:focus {
        border-color: #dc9b01 !important;
    }
    .has-success .checkbox, .has-success .checkbox-inline, .has-success .control-label, .has-success .form-control-feedback, .has-success .help-block, .has-success .radio, .has-success .radio-inline, .has-success.checkbox label, .has-success.checkbox-inline label, .has-success.radio label, .has-success.radio-inline label{
        color: #dc9b01;
    }
    

</style>
<?php
    defined('BASEPATH') or exit('No direct script access allowed');

$temp_display_login_form = '';



$temp_display_login_form .= form_open('login', 'class="validate"');

$temp_display_login_form .= '
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
';
if ($this->api_shop_setting[0]['require_login'] != 1) {
    $temp_display_login_form .= '
        <a href="'.base_url().'login?register=1" class="pull-right">'.lang('registers').'</a>
    ';
}
$u = mt_rand();
    $temp_display_login_form .= '
            <label for="username'.$u.'" class="control-label">'.lang('Username').'</label>
            <input type="text" name="identity" id="username'.$u.'" class="form-control" value="" required placeholder="'.lang('Username').'">
        </div>
        <div class="form-group">
            <a href="#" tabindex="-1" class="forgot-password pull-right">'.lang('forgot?').'</a>
            <label for="password'.$u.'" class="control-label">'.lang('password').'</label>
            <input type="password" id="password'.$u.'" name="password" class="form-control" placeholder="'.lang('password').'" value="" required>
            <span toggle="#password'.$u.'" class="fa fa-fw fa-eye field-icon toggle-password api_pointer"></span>            
        </div>
    ';
        if ($Settings->captcha) {
            $temp_display_login_form .= '
            <div class="form-group">
            <div class="form-group text-center">
                    <span class="captcha-image">'.$image.'</span>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <a href="'.admin_url('auth/reload_captcha').'" class="reload-captcha text-blue">
                                <i class="fa fa-refresh"></i>
                            </a>
                        </span>
                        '.form_input($captcha).'
                    </div>
                </div>
            </div>
            ';
        }
        $temp_display_login_form .= '
        <div class="form-group">
            <div class="checkbox">
                <label>
                    <input type="checkbox" value="1" name="remember_me" checked="checked"><span> '.lang('remember_me_login').'</span>
                </label>
            </div>
        </div>
        <button type="submit" value="login" name="login" class="btn btn-block btn-success">'.lang('login').'</button>
    </div>
</div>
'.form_close();

if ($this->api_shop_setting[0]['require_login'] != 1) {
    $temp_display_login_form .= '
        <div class="api_height_5"></div>
        <a href="'.base_url().'login?register=1">
            <button type="button" value="register" name="register" class="btn btn-block btn-info" style="color: white;">'.lang('Register').'</button>
        </a>
    ';
}

if (!$shop_settings->private) {
    $providers = config_item('providers');
    foreach ($providers as $key => $provider) {
        if ($provider['enabled']) {
            $temp_display_login_form .= '
                <div class="api_text_align_center">
                    <div class="api_login_'.strtolower($key).' api_margin_top_5 api_link_box_none ">
                        <a href="'.site_url('social_auth/login/'.$key).'">
                            <i aria-hidden="true" class="fa fa-'.strtolower($key).' api_color_white"></i>
                            <span class="api_color_white">Login With '.$key.'</span>
                        </a>
                    </div>
                </div>
            ';
        }
    }
}
// <div style="margin-top:10px;"><a href="'.site_url('social_auth/login/'.$key).'" class="btn btn-sm mt btn-default btn-block" title="'.lang('login_with').' '.$key.'" style="background-color:#1877f2;">'.lang('login_with').' '.$key.'</a></div>';



// if ($this->api_shop_setting[0]['facebook_login'] == 1) {
//     $temp_display_login_form .= '
//         <div class="api_text_align_center api_padding_top_10 api_link_box_none">
//             <div class="fb-login-button" data-width="" data-size="large" data-button-type="login_with" data-layout="rounded" data-auto-logout-link="false" data-use-continue-as="true" scope="public_profile,email" onlogin="checkLoginState();" ></div>
//         </div>
//     ';
// }
