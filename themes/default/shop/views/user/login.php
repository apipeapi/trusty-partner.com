<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style>
    .has-success .form-control{
        border-color:#dc9b01 !important;
        
    }
    .has-success .form-control:focus{
        box-shadow:inset 0 1px 1px rgb(0 0 0 / 8%), 0 0 6px black !important;
    }
    .has-success .checkbox, .has-success .checkbox-inline, .has-success .control-label, .has-success .form-control-feedback, .has-success .help-block, .has-success .radio, .has-success .radio-inline, .has-success.checkbox label, .has-success.checkbox-inline label, .has-success.radio label, .has-success.radio-inline label{
        color:#737373;
    }
    input:-webkit-autofill,
    input:-webkit-autofill:hover, 
    input:-webkit-autofill:focus,
    select:-webkit-autofill,
    select:-webkit-autofill:focus  {
        border: 1px solid #dc9b01;
        -webkit-text-fill-color: #dc9b01;
        transition: background-color 5000s ease-in-out 0s;
    }
    .btn-default:hover{
        background-color:#9D0001;
        border: 1px solid #9D0001;
    }
</style>
<section class="page-contents">
    <div class="container">
        <div class="row">
            <div class="col-xs-12" >
<?php
    if ($this->session->userdata('user_id')) {
        $temp_display_1 = "col-sm-9 col-md-10"; 
        $temp_display_2 = "col-sm-3 col-md-2 col-xs-12";
    }
    else {
        $temp_display_1 = "col-md-12"; 
        $temp_display_2 = "api_display_none";
    }
    if ($this->api_shop_setting[0]['require_login'] == 1) {
        $temp_display_11 = 'api_display_none';
    }
    else {
        $temp_display_11 = '';
    }

?>

<?php
if ($_GET['register'] != 1) {
    echo '
        <div class="row">
    ';
   
        echo '
            <div class="col-md-12">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="'.base_url().'login" aria-controls="login" >
                        '.lang('login').'
                        </a>
                    </li>
                    
                    <li role="presentation" class="'.$temp_display_11.'">
                        <a id="api_register"  href="'.base_url().'login?register=1" aria-controls="register">
                            '.lang('registers').'
                        </a>
                    </li>
                </ul>
        ';
        


        echo '         
        <div class="tab-content padding-lg white bordered-light" style="margin-top:-1px;">
            <div role="tabpanel" class="tab-pane fade in active" id="login">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="well margin-bottom-no">
        ';
            include('login_form_2.php');
            echo $temp_display_login_form;
            
        echo '
                        </div>
                    </div>
        ';
      
                echo '
                    <div class="col-sm-6 '.$temp_display_11.'">
                        <div class="api_height_15 visible-sm visible-xs"></div>
                        <h4 class="title">
                            <span>'.lang('register_new_account').'</span>
                        </h4>
                        <p>
                            '.lang('register_account_info').'
                        </p>
                    </div>
                ';
        echo '
                    <div class="api_clear_both"></div>
                </div>
            </div>
        </div>
        ';

    echo '
        </div>
    ';
}
else {

    // if ($this->sma->send_email('apipechinese@gmail.com', 'test', 'test', 'apipeapi@gmail.com', null, '', '', '')) {
    //     echo '1111';
    // }
    // else
    //     echo '000';

    echo '
        <div class="row">
    ';
        echo '
            <div class="col-md-12">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation">
                        <a href="'.base_url().'login" aria-controls="login" >
                        '.lang('login').'
                        </a>
                    </li>
                    <li role="presentation" class="active">
                        <a id="api_register"  href="'.base_url().'login?register=1" aria-controls="register">
                            '.lang('registers').'
                        </a>
                    </li>
                </ul>
        ';

        echo '         
        <div class="tab-content padding-lg white bordered-light" style="margin-top:-1px;">
            <div role="tabpanel" class="tab-pane fade in active" id="login">
                <div class="row">
                    <div class="col-sm-12">
        ';
?>
    <?php $attrib = array('class' => 'validate', 'role' => 'form', 'name' => 'api_form_regisiter');
    echo form_open("register", $attrib); ?>
    <div class="row">
        <div class="col-sm-4" id="first_name_required">
            <div class="form-group">
                <?= lang('first_name', 'first_name'); ?>
                <div class="controls">
                    <?= form_input('first_name', '', 'class="form-control" id="first_name" required="required" pattern=".{3,10}"'); ?>
                </div>
            </div>
        </div>
        <div class="col-sm-4" id="last_name_required">
            <div class="form-group">
                <?= lang('last_name', 'last_name'); ?>
                <div class="controls">
                    <?= form_input('last_name', '', 'class="form-control" id="last_name" required="required"'); ?>
                </div>
            </div>
        </div>
        <div class="col-sm-4" id="phone_required">
            <div class="form-group">
                <?= lang('phone', 'phone'); ?>
                <div class="controls">
                    <?= form_input('phone', '', 'class="form-control" id="phone" required="required"'); ?>
                </div>
            </div>
        </div>        
    </div>
    <div class="row">
        <div class="col-sm-6 api_display_none">
            <div class="form-group <?= $temp_display_1a; ?>">
                <?= lang('company', 'company'); ?>
                <?php 
                    $config_data = array(
                        'table_name' => 'sma_companies',
                        'select_table' => 'sma_companies',
                        'translate' => '',
                        'select_condition' => "id = 1 ",
                    );
                    $select_data = $this->site->api_select_data_v2($config_data);
                ?>
                <!-- Original 17-03-2021 -->
                <?php //form_input('company', set_value('company'), 'class="form-control tip" id="company" '); ?>
                <?= form_input('company', $select_data[0]['name'], 'class="form-control tip" id="company" '); ?>

            </div>
            <?= $temp_display_2a; ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6" id="username_required">
            <div class="form-group">
                <?= lang('username', 'username'); ?>
                <?= form_input('username', set_value('username'), 'class="form-control tip" id="username" required="required"'); ?>
                <span id="username_error" style="color:#a94442"> </span>
            </div>
        </div>
        <div class="col-sm-6" id="email_required">
            <div class="form-group">
                <?= lang('email', 'email'); ?>
                <div class="controls">
                    
                    <input type="email" id="email" name="email" class="form-control" required="required" />
                    <span id="email_error" style="color:#a94442;"> </span>
                    
                </div>
            </div>
        </div>        
    </div>
    <div class="row">
        <div class="col-md-6 api_display_none">
            <div class="form-group">
                <?= lang('country', 'country'); ?>
                <?= form_input('country', 'Cambodia', 'class="form-control tip" id="country"'); ?>
            </div>
        </div>                                                                       
        <?php $u = mt_rand().'_2';?>
        <div class="col-sm-6" id="password_required">
            <div class="form-group">
                <?= lang('password', 'passwordr'); ?>
                <div class="controls">
                <?php //echo  form_password('password', '', 'class="form-control tip" id="passwordr" required="required" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"'); ?>
                    <?php echo  form_password('password', '', 'class="form-control tip" id="passwordr'.$u.'" required="required" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"'); ?>
                    <span toggle="#passwordr<?= $u;?>" id="api_from_login_eye_2" class="fa fa-fw fa-eye field-icon toggle-password api_pointer" onclick="api_toggle_password();"></span>
                    <span class="help-block"><?= lang('password_hint'); ?></span>
                </div>
            </div>
        </div>
        <div class="col-sm-6" id="password_confirm_wrapper" >
            <div class="form-group">
                <?= lang('confirm_password', 'password_confirm'); ?>
                <div class="controls">
                <?php //echo form_password('password_confirm', '', 'class="form-control" id="password_confirm" required="required" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" data-bv-identical="true" data-bv-identical-field="password" data-bv-identical-message="' . lang('pw_not_same') . '"'); ?>
                    <?= form_password('password_confirm', '', 'class="form-control" id="password_confirm'.$u.'" '); ?>
                    <span toggle="#password_confirm<?= $u;?>" id="api_from_login_eye_3" class="fa fa-fw fa-eye field-icon toggle-password api_pointer" onclick="api_toggle_confirm_password();"></span>
                    <span id="error_password_confirm" style="color:#a94442; display:none;"><?php echo lang('pw_not_same'); ?></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="clearfix"></div>
    </div>
<?php
    echo '
        <button type="button" style="color:white" class="btn btn-default" onclick="
        var postData = {
            \'website_address\' : \''.base_url().'\',
            \'close\' : \''.lang('Close').'\',
            \'use_this_password\' : \''.lang('use_this_password').'\',
            \'generated_password\' : \''.lang('generated_password').'\',
        };        
        api_generate_password(postData);
        ">
            '.lang('Generate_Password').'
        </button>
        <div class="api_height_30"></div>
    ';
//-google-map-----------------
    $temp_google_function_id = '1';
    $temp_distance_from_restaurant = '';
    $temp_google_location = '';
    $temp = explode(',',$this->api_shop_setting[0]['google_location']);
    $temp_shop_lat = $temp[0];
    $temp_shop_lng = $temp[1];
    if ($temp_google_location == '') {
        $temp_initial_content = '<b>SUSHI DONMARU Restaurant</b><div><b>Pin Location: </b>'.$this->api_shop_setting[0]['google_location'].'</div>';
        $temp_initial_lat = $temp_shop_lat;
        $temp_initial_lng = $temp_shop_lng;
    }
    else {
        $temp_fee = $this->api_helper->calculate_delivery_fee($temp_distance_from_restaurant);
        $temp_initial_content = '<div><b>Your Pin Location: </b>'.$temp_google_location.'<div> <b>Distance from our restaurant: </b>'.$temp_distance_from_restaurant.'</div> <div><b>Delivery Fee: </b>'.$this->sma->formatMoney($temp_fee).'</div>';

        $temp = explode(',',$temp_google_location);
        $temp_initial_lat = $temp[0];
        $temp_initial_lng = $temp[1];    
    }
    $google_data[0] = array(
        'id' => $temp_google_function_id,
        'initial_content' => $temp_initial_content,
        'initial_lat' => $temp_initial_lat,
        'initial_lng' => $temp_initial_lng,
        'shop_lat' => $temp_shop_lat,
        'shop_lng' => $temp_shop_lng,    
    );
    if ($this->api_shop_setting[0]['google_map_api'] == 1)
        $temp = 'readonly="readonly"';
    else
        $temp = 'required="required"';
    echo '
    <div class="row">
        <div class="col-md-6" id="delivery_address_required">
            <div class="form-group has-success">
                '.lang('Delivery_Address', 'address').'
                '.form_input('address', '', 'class="form-control tip" '.$temp.' id="google_address_'.$temp_google_function_id.'"').'
                <span id="address_error" style="color:#a94442; display:none;">'.lang('validation_form').'</span>
            </div>
        </div>  
        <div class="col-md-6">
            <div class="form-group has-success">
                '.lang('Note_to_Driver', 'Note_to_Driver').'
                '.form_input('add_ons_note_to_driver', '', 'class="form-control tip" ').'
            </div>
        </div>  
    </div> 
    <div class="col-md-6 api_display_none">
    <input type="text" name="add_ons_google_location" id="add_ons_google_location_'.$temp_google_function_id.'" value="'.$temp_google_location.'" style="width:100%;">
    </div>
    <div class="col-md-6 api_display_none">
    <input type="text" name="add_ons_distance_from_restaurant" id="add_ons_distance_from_restaurant_'.$temp_google_function_id.'" value="'.$temp_distance_from_restaurant.'" style="width:100%;">
    </div>
    ';
if ($this->api_shop_setting[0]['google_map_api'] == 1) {    
    echo '
    <div class="col-md-12 api_padding_0 api_display_none" id="google_location_error_'.$temp_google_function_id.'" style="color:red;">
        '.lang('Please_pin_your_location_on_the_map').'
        <div class="api_height_10"></div>
    </div>    
    <div id="map_'.$temp_google_function_id.'" style="width:100%; height:400px;"></div>
    ';
    include 'themes/default/shop/views/sub_page/api_google_map_distance.php';
}
//-google-map-----------------



    echo '               
        <div class="col-sm-6 api_padding_0">
            <div class="api_height_15"></div>
            '.form_submit('register', lang('registers'), 'class="btn btn-primary api_display_none" id="submit_register"').'
            '.form_button('register', lang('registers'), 'class="btn btn-primary" onclick="api_submit_register();"').'
        </div>  
    ';

    if ($this->api_shop_setting[0]['facebook_login'] == 1) {
        echo '
        <div class="col-sm-6 api_padding_top_15">
            <div class="api_text_align_center api_link_box_none">
                <div class="fb-login-button" data-width="" data-size="large" data-button-type="login_with" data-layout="rounded" data-auto-logout-link="false" data-use-continue-as="true" scope="public_profile,email" onlogin="checkLoginState();" ></div>
            </div>
        </div>
        ';
    }

    echo form_close(); 

?>

<?php


        echo '
                    </div>
                </div>
            </div>
        </div>
        ';

    echo '
        </div>
    ';
}


?>



<div class="<?= $temp_display_2 ?>">
    <?php
    if ($this->session->userdata('user_id') > 0)
        include('themes/default/shop/views/pages/sidebar2.php'); 
    ?>
</div>

            </div>
        </div>
    </div>
</section>




<script>
var id = '<?php echo $u; ?>';
function api_use_this_password(){
    document.getElementById('passwordr'+ id).value = document.getElementById('api_modal_body').innerHTML;
    document.getElementById('password_confirm'+ id).value = document.getElementById('api_modal_body').innerHTML;
}

$(document).ready(function(){
    function alignModal(){
        var modalDialog = $(this).find("#api_modal");
        /* Applying the top margin on modal dialog to align it vertically center */
        modalDialog.css("margin-top", Math.max(0, ($(window).height() - modalDialog.height()) / 2));
    }
    // Align modal when it is displayed
    $("#api_modal").on("shown.bs.modal", alignModal);
    
    // Align modal when user resize the window
    $(window).on("resize", function(){
        $("#api_modal:visible").each(alignModal);
    });   
    alignModal();

});

function api_submit_register() {
    var error = 0;

    if($('#first_name').val() == '') {
        $('#first_name_required').scrollView();
        error = 1;
    } else if($('#last_name').val() == '') {
        $('#last_name_required').scrollView();
        error = 1;
    } else if($('#phone').val() == '') {
        $('#phone_required').scrollView();
        error = 1;
    } else if(api_validate_email($('#email').val()) == false) {
        $('#email_required').scrollView();
        error = 1;
    } else if($('#username').val() == '') {
        $('#username_required').scrollView();
        error = 1;
    } else if($('#passwordr'+ id).val() == '') {
        $('#password_required').scrollView();
        error = 1;
    } else if($('#password_confirm'+ id).val() == '' || ($('#password_confirm'+ id).val() != $('#passwordr'+ id).val())) {
        $('#error_password_confirm').show();
        $('#password_confirm_wrapper').scrollView();
        error = 1;
    }
    else if($('#google_address_1').val() == '') {
        $('#delivery_address_required').scrollView();
        error = 1;
    }
    if ($('#google_address_1').val() == '')
        $('#address_error').show();
    else
        $('#address_error').hide();

    if ($('#password_confirm'+ id).val() != $('#passwordr'+ id).val())
        $('#error_password_confirm').show();
    else  
        $('#error_password_confirm').hide();

    if(error == 1) {
        if ($('#password_confirm'+ id).val() == $('#passwordr'+ id).val()) {
            if ($('#google_address_1').val() != '')
                $('#submit_register').click(); 
        }        
    } else {
        var postData = {
                'username' : $('#username').val(),
                'email' : $('#email').val(),
        };
        ajax_check_username_email(postData);
    }
}
function ajax_check_username_email(postData) {
    var result = $.ajax
    (
     {
      url: '<?php echo base_url(); ?>main/ajax_check_username_email',
      type: 'GET',
      secureuri:false,
      dataType: 'html',
      data:postData,
      async: false,
      error: function (response, status, e)
      {
       alert(e);
      }
     }
    ).responseText;
    // var myWindow = window.open("", "MsgWindow", "width=700, height=400");
    // myWindow.document.write(result);     
    var array_data = String(result).split("api-ajax-request-multiple-result-split");
    document.getElementById('email_error').innerHTML = '';
    document.getElementById('username_error').innerHTML = '';
    var google_id = '<?php echo $temp_google_function_id;?>';
    if(array_data[2] != '') {
        document.getElementById('username_error').innerHTML = array_data[2];
        $('#username_required').scrollView();
    } 
    else if(array_data[1] != '') {
        document.getElementById('email_error').innerHTML = array_data[1];
        $('#email_required').scrollView();
    } 
    else {
<?php
if ($this->api_shop_setting[0]['google_map_api'] == 1)
    echo "
        if ($('#add_ons_google_location_'+ google_id).val() == '') {
            $('#google_location_error_'+ google_id).removeClass('api_display_none');
            $('#google_location_error_'+ google_id).scrollView();
        } 
        else {
            document.api_form_regisiter.submit();
        }
    ";
else
    echo "
        document.api_form_regisiter.submit();
    ";
?>
    }
}

function api_toggle_password(){
    if ($("#api_from_login_eye_2").hasClass("fa-eye")) {
        $("#api_from_login_eye_2").removeClass("fa-eye");
        $("#api_from_login_eye_2").addClass("fa-eye-slash");
        $("#passwordr"+ id).attr("type","text");
    }
    else {
        $("#api_from_login_eye_2").addClass("fa-eye");
        $("#api_from_login_eye_2").removeClass("fa-eye-slash");
        $("#passwordr"+ id).attr("type","password");  
    }
}
function api_toggle_confirm_password() {
    if ($("#api_from_login_eye_3").hasClass("fa-eye")) {
        $("#api_from_login_eye_3").removeClass("fa-eye");
        $("#api_from_login_eye_3").addClass("fa-eye-slash");
        $("#password_confirm"+ id).attr("type","text");
    }
    else {
        $("#api_from_login_eye_3").addClass("fa-eye");
        $("#api_from_login_eye_3").removeClass("fa-eye-slash");
        $("#password_confirm"+ id).attr("type","password"); 
    }
}
</script>