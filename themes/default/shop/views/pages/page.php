<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section class="page-contents">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">

                <div class="row">
                    <div class="col-sm-9 col-md-10">
                        <div class="panel panel-default margin-top-lg">
                            <div class="panel-heading text-bold">
                                <?php 
                                    //echo lang('contact_us');                            
                                    $temp = str_replace(' ', '_', strtolower($page->title));
                                    echo lang($temp);   
                                ?>
                            </div>
                            <div class="panel-body">
                                <?php
    $config_data = array(
        'table_name' => 'sma_pages',
        'select_table' => 'sma_pages',
        'translate' => 'yes',
        'select_condition' => "id = ".$page->id,
    );
    $select_data = $this->site->api_select_data_v2($config_data);                                
                                
    echo $this->sma->decode_html($select_data[0]['title_'.$this->api_shop_setting[0]['api_lang_key']]); 
    if ($page->slug == 'Contact-Us')
    echo '
        <table width="100%" border="0">
            <tr>
                <td align="center" valign="middle" style="width:40px !important; height:50px;">
                    <i class="fa fa-home fa-2x" aria-hidden="true"></i>
                </td>
                <td valign="middle" class="api_padding_top_5">
                    <label>'.lang('company_name').':</label>
                    Trusty Partner Co.,Ltd.
                </td>
            </tr>
            <tr>
                <td align="center" valign="middle" style="width:40px !important; height:50px;">
                    <i class="fa  fa-cutlery fa-2x" aria-hidden="true"></i>
                </td>
                <td valign="middle" class="api_padding_top_5">
                    <label>'.lang('restaurant_name').':</label>
                    SUSHI DONMARU
                </td>
            </tr>
            <tr>
                <td align="center" valign="middle" style="width:40px !important; height:50px;">
                    <i class="fa fa-location-arrow fa-2x" aria-hidden="true"></i>
                </td>
                <td valign="middle" class="api_padding_top_5">
                    <label>'.lang('Address').':</label>
                    #179, St63, Sangkat Boeung Keng Kang 1, Khan Boeung Keng Kang 1 , Phnom Penh, Kingdom of Cambodia,  120102
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <img src="'.base_url().'assets/images/shop_sushiya.jpg" width="300px"/>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><span style="color:#db9803">'.lang('our_store_front').'</span></td>
            <tr>
            <tr>
                <td align="center" valign="middle" style="width:40px !important; height:50px;">
                    <i class="fa fa-envelope fa-2x" aria-hidden="true"></i>
                </td>
                <td valign="middle" class="api_padding_top_5">
                    <label >'.lang('email').':</label>
                    sushidonmaru@gmail.com
                </td>
            </tr>
            <tr>
                <td align="center" valign="middle" style="width:40px !important; height:50px;">
                    <i class="fa fa-phone fa-2x" aria-hidden="true"></i>
                </td>
                <td valign="middle" class="api_padding_top_5">
                    <label >'.lang('phone_number').':</label>
                    096 990 0055
                </td>
            </tr>
            <tr>
                <td align="center" valign="middle" style="width:40px !important; height:50px;">
                    <i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i>
                </td>
                <td  valign="middle" class="api_padding_top_5">
                    <label >Facebook :</label>
                    <a style="color:#db9803" target="_blank" href="https://www.facebook.com/sushidonmaru">Sushi Donmaru Japanese Restaurant</a>
                </td>
            </tr>
            <tr>
                <td align="center"valign="middle" style="width:40px !important; height:50px;">
                    <i class="fa fa-map-marker fa-2x" aria-hidden="true"></i>
                </td>
                <td valign="middle" class="api_padding_top_5">
                    <label>'.lang('access').':</label>
                    <a style="color:#db9803" target="_blank" href="https://maps.app.goo.gl/ZNGjjMzZbcjXAtyQ6"> https://maps.app.goo.gl/ZNGjjMzZbcjXAtyQ6</a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3908.9794386275757!2d104.9211688!3d11.5533316!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x310951185ac7dd5b%3A0xb212d485d47ab770!2zU3VzaGkgRG9uIE1hcnUg4Z6f4Z-K4Z684Z6f4Z674Z644Z6K4Z674Z6T4Z6Y4Z-J4Z624Z6a4Z68IOmuqOS4vOS4uA!5e0!3m2!1sen!2skh!4v1620621418936!5m2!1sen!2skh" style="border:0;  width:95%;  height: 400px  " allowfullscreen="" loading="lazy"></iframe>
                </td>
            </tr>
        </table><br>
    ';
                                ?>
                                <?php
                                if ($page->slug == $shop_settings->contact_link) {
                                    echo '<p><button type="button" class="btn btn-primary email-modal">'.lang('send_email_title').'</button></p>';
                                }
                                ?>
                            </div>
                            <div id="cart-helper" class="panel panel-footer margin-bottom-no api_display_none">
                                <?= lang('updated_at').': '.$this->sma->hrld($page->updated_at); ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3 col-md-2">
                        <?php include('sidebar2.php'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>