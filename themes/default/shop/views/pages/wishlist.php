<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');
    if ($this->session->userdata('api_mobile') != 1)
        include 'themes/default/shop/views/sub_page/api_carousel/api_carousel_kata_tip/promotion_featured.php';
        //include 'themes/default/shop/views/sub_page/api_carousel_ebay/featured_bk.php';        
    else
        include 'themes/default/shop/views/sub_page/api_carousel_ebay/featured.php';
?>




<section class="page-contents api_padding_0_mobile">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-sm-9 col-md-10 api_padding_right_0_pc">
                        
                        <div class="col-md-12 api_padding_0_mobile">
                            <h4 class="margin-top-md title text-bold">
                                <span><i class="fa fa-heart-o margin-right-sm"></i> <?= lang('wishlist'); ?></span>            
                                <div id="api_customer_menu_panel_btn" class="api_float_right api_pointer api_screen_show_768" onclick="$('#api_customer_menu_panel').toggle('slideToggle?');" style="top: -10px; margin-top: -15px;">
                                    <button class="btn btn-danger navbar-btn"><div class="fa fa-bars fa-lg"></div></button>
                                </div>
                            </h4>
                        </div>

                        <div id="api_customer_menu_panel" class="col-md-2 api_padding_right_0_pc api_padding_0_mobile api_screen_hide_768">
                            <div class="api_height_10 api_screen_hide_768"></div>
                            <?php include('themes/default/shop/views/pages/customer_menu_panel.php'); ?>                            
                        </div>
                        <div class="col-md-10 api_padding_0_mobile">
                            <div class="api_height_5"></div>

<?php
    echo '
        <div class="col-md-6 api_padding_left_0_pc api_padding_0_mobile">
            
            <label>'.lang('Search').'</label>
            <div class="input-group">
                <input id="api_wishlist_search" type="text" class="form-control" id="product-search" aria-label="'.lang('Search').'..." placeholder="'.lang('Search').'..." value="'.$_GET['search'].'">
                <div class="input-group-btn">
                    <button type="submit" id="api_wishlist_search_btn" class="btn btn-default btn-search" onclick="window.location=\''.base_url().'shop/wishlist?search=\' + $(\'#api_wishlist_search\').val();">
                    <i class="fa fa-search"></i>
                    </button>
                </div>
            </div>
        </div>
    ';

echo '
    <div class="col-md-6 api_padding_right_0_pc api_padding_0_mobile">
        <div class="api_height_10 api_clear_both api_screen_show_768"></div>
        <label>
            '.lang("category").'
        </label>
        <div class="controls"> 
';
            $tr[''] = lang("All_Categories");
            if (is_array($select_category)) if (count($select_category) > 0)
            for ($i=0;$i<count($select_category);$i++) {
                $tr[$select_category[$i]['id']] = $select_category[$i]['name'];
            }                            

            echo form_dropdown('api_wishlist_category', $tr, $_GET['category'], 'data-placeholder="'.lang("All_Categories").'" id="api_wishlist_category" onchange="window.location=\''.base_url().'shop/wishlist?category=\' + $(\'#api_wishlist_category\').val();" class="form-control"');         
echo '
        </div>
    </div>
    <div class="api_height_15 api_clear_both"></div>
';    
?>

    <?php
    if (!empty($items)) {

$useragent=$_SERVER['HTTP_USER_AGENT'];
$api_view_array = array();

if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))) {

//=Mobile=====================================================================================    
$temp_display .= '   
<div class="col-md-12 api_padding_0">
<ul id="" style="list-style-type:none; margin:0px; padding:0px;">
';

    if (is_array($items))
    foreach ($items as $item) {
        if ($item->id != '') {

            $config_data_2 = array(
                'table_name' => 'sma_products',
                'select_table' => 'sma_products',
                'translate' => 'yes',
                'select_condition' => "id = ".$item->id,
            );
            $temp_product = $this->site->api_select_data_v2($config_data_2);
            $item->name = $temp_product[0]['title_'.$this->api_shop_setting[0]['api_lang_key']];

            if ($this->api_shop_setting[0]['out_of_stock'] == 1 || $temp_product[0]['product_status'] == 'unavailable_today') {
                if ($item->quantity > 0)
                    $temp_stock = '<span class="api_color_green">'.lang('in_stock').'</span>';
                else
                    $temp_stock = '<span class="api_color_red">'.lang('out_of_stock').'</span>';
            }
            else {
                if ($item->quantity > 0)
                    $temp_stock = '<span class="api_color_green">'.lang('in_stock').'</span>';
                else
                    $temp_stock = '<span class="api_color_green">'.lang('in_stock').'</span>';                
            }
$temp_display .= '
<li class="">
      <div class="product product_mobile api_margin_bottom_0 " style="border-bottom:1px solid #eee;">
    <table id="'.$item->id.'" class="" width="100%" border="0">
        <tr>
        <td class="api_padding_right_10" valign="center" align="center" width="120">
';
    if (is_file('assets/uploads/thumbs/'.$item->image))
        $temp = 'assets/uploads/thumbs/'.$item->image;
    else
        $temp = 'assets/uploads/thumbs/no_image.jpg';
    $data_view = array (
        'wrapper_class' => '',
        'file_path' => $temp,
        'max_width' => 120,
        'max_height' => 150,
        'product_link' => 'true',
        'image_class' => 'img-responsive',
        'image_id' => '',   
        'resize_type' => 'full',
    );
    $temp = $this->site->api_get_image($data_view);
    $temp_display .= $temp['image'];

$temp_display .= '
        </td>
        <td valign="top" style="vertical-align: top!important; padding-top:10px;">
            '.$item->name.'
            <div class="api_height_5"></div>
            <div>
';

        $config_data = array(
            'id' => $item->id,
            'customer_id' => $this->session->userdata('company_id'),
        );
        $temp = $this->site->api_calculate_product_price($config_data);

        if ($item->promotion) {
            $temp_display .= '<del class="text-red">'.$this->sma->convertMoney($temp['temp_price']).'</del><br>';
            $temp_display .= $this->sma->convertMoney($temp['price']);
        } else {
            $temp_display .= $this->sma->convertMoney($temp['price']);
        }

$temp_display .= '
            </div>
            '.$temp_stock.'            
            
            <div class="api_height_5 api_clear_both"></div>
            <div class="input-group ">
';
/*
            for ($i=1;$i<=10;$i++) {
                $tr2[$i] = $i;
            }
            $temp_display .= form_dropdown('api_list_qty_'.$item->id, $tr2, 1, 'id="api_list_qty_'.$item->id.'" class="form-control"');
*/

            $temp_display .= '
                <span class="input-group-addon pointer btn-minus api_drag_button"><span class="fa fa-minus"></span></span>
                <input type="tel" id="api_list_qty_'.$item->id.'" name="quantity" class="form-control text-center api_numberic_input quantity-input api_drag_input" value="1" >
                <span class="input-group-addon pointer btn-plus api_drag_button"><span class="fa fa-plus"></span></span>
            ';

$temp_display .= '
            </div>                     
            <div class="api_height_5 api_clear_both"></div>

            <div class="input-group " style="width:100%;">
';
    if ($item->is_hided == 'show') { 
        $temp_display .= '
            <div class="btn-group" role="group" style="width:33%;">
            <button type="button" class=" btn btn-sm btn-danger add-to-cart-2" style="width:100%;" data-id="'.$item->id.'" title="'.lang('add_to_cart').'"><i class="fa fa-shopping-cart"></i></button>
            </div>
            <div class="btn-group" role="group" style="width:33%;">
                <a href="'.base_url().'product/'.$item->slug.'">
                    <button type="button" class=" btn btn-sm btn-info" style="width:100%;" title="'.lang('view').'"><i class="fa fa-eye"></i></button>
                </a>
            </div>
        ';
    }
    else {
        $temp_display .= '
            <div class="btn-group" role="group" style="width:33%;">
            <a href="javascript:void(0);" onclick="alert(\''.lang('Sorry, this_product_is_disabled.').'\');">
                <button type="button" class=" btn btn-sm btn-danger" style="width:100%;" disabled="disabled" title="'.lang('add_to_cart').'"><i class="fa fa-shopping-cart"></i></button>
            </a>
            </div>
            <div class="btn-group" role="group" style="width:33%;">
            <a href="javascript:void(0);" onclick="alert(\''.lang('Sorry, this_product_is_disabled.').'\');">
                <button type="button" class=" btn btn-sm btn-info" style="width:100%;" disabled="disabled" title="'.lang('view').'"><i class="fa fa-eye"></i></button>
            </a>
            </div>
        ';        
    }

    $temp_display .= '
            <div class="btn-group" role="group" style="width:34%;">
                <button type="button" class="api_display_none btn btn-sm btn-success remove-wishlist api_drag_button" style="width:100%;" id="favorite_m_'.$item->id.'" data-id="'.$item->id.'" ><i class="fa fa-trash-o"></i></button>
                <button type="button" onclick="api_saa_alert_favorite(\'favorite_m_'.$item->id.'\',\'\',\'\',\'\');" class="btn btn-sm btn-success api_drag_button" style="width:100%;" ><i class="fa fa-trash-o"></i></button>
            </div>    
    ';

$temp_display .= '
            </div>  
        </td>
        </tr>
    </table>
      </div>
</li>
';
        }
    }

$temp_display .= '
</ul>                            
</div>
';
echo $temp_display;
//=Mobile=====================================================================================    

}
else {
//=PC=====================================================================================    
        echo '

        <div class="table-responsive">
        <table width="100%" class="table table-striped table-hover table-va-middle api_margin_bottom_0" style="border-bottom:0px;">
            <thead>
                <tr>
                    <th class="col-xs-1 api_display_none" style="text-align:center;">'.lang('Order').'</th>
                    <th class="col-xs-1">'.lang('photo').'</th>
                    <th class="col-xs-4">'.lang('description').'</th>
                    <th class="col-xs-2" style="text-align:center;">'.lang('qty').'</th>
                    <th class="col-xs-2" style="text-align:right;">'.lang('price').'</th>
                    <th class="col-xs-1" style="text-align:center;width:1%; white-space:nowrap;">'.lang('in_stock').'</th>
                    <th class="col-xs-2" style="text-align:center !important;">'.lang('actions').'</th>
                </tr>
            </thead>
        </table>
        <ul id="sortable">
        ';
        $r = 1;
        foreach ($items as $item) {
            if ($item->id > 0) {

                $config_data_2 = array(
                    'table_name' => 'sma_products',
                    'select_table' => 'sma_products',
                    'translate' => 'yes',
                    'select_condition' => "id = ".$item->id,
                );
                $temp_product = $this->site->api_select_data_v2($config_data_2);
                $item->name = $temp_product[0]['title_'.$this->api_shop_setting[0]['api_lang_key']];
                                
            ?>
            <li class="sortable">
            <table width="100%" class="table table-striped table-hover table-va-middle api_margin_bottom_0">
            <tr class="product" id="<?= $item->id ?>" style="background-color: #fff;" >
                <td class="col-xs-1 api_display_none">
                    <input type="tel" id="api_list_order_<?php echo $item->id; ?>" class="form-control api_numberic_input text-center" value="<?php echo $item->order_number; ?>" onchange="api_change_favorite_order('<?php echo $item->id; ?>',this.value);">
                </td>
                <td class="col-xs-1">
<?php
    if (is_file('assets/uploads/thumbs/'.$item->image))
        $temp = 'assets/uploads/thumbs/'.$item->image;
    else
        $temp = 'assets/uploads/thumbs/no_image.jpg';
    $data_view = array (
        'wrapper_class' => '',
        'file_path' => $temp,
        'max_width' => 50,
        'max_height' => 50,
        'product_link' => 'true',
        'image_class' => 'img-responsive',
        'image_id' => '',   
        'resize_type' => 'full',
    );
    $temp = $this->site->api_get_image($data_view);
    echo $temp['image_table'];
?>                    
                </td>
                <td class="col-xs-4">
                    <?= $item->name.'<br>'.$item->details; ?>
                </td>
                <td class="col-xs-2" align="center">
                    <div class="product-bottom" style="padding-top: 0px; margin-top:0px;">
                        <div class="input-group">
<?php

    for ($i=1;$i<=10;$i++) {
        $tr2[$i] = $i;
    }
    echo form_dropdown('api_list_qty_'.$item->id, $tr2, 1, 'id="api_list_qty_'.$item->id.'" class="form-control"');
    /*
    echo '
        <span class="input-group-addon pointer btn-minus"><span class="fa fa-minus"></span></span>
        <input type="tel" id="api_list_qty_'.$item->id.'" name="quantity" class="form-control text-center api_numberic_input quantity-input" value="1" style="min-width: 50px">
        <span class="input-group-addon pointer btn-plus"><span class="fa fa-plus"></span></span>
    ';
    */
?>
                        </div>                     
                    </div>
                </td>
                <td class="col-xs-2" align="right">
                <?php
                
                $config_data = array(
                    'id' => $item->id,
                    'customer_id' => $this->session->userdata('company_id'),
                );
                $temp = $this->site->api_calculate_product_price($config_data);

                if ($item->promotion) {
                    echo '<del class="text-red">'.$this->sma->convertMoney($temp['temp_price']).'</del><br>';
                    echo $this->sma->convertMoney($temp['price']);
                } else {
                    echo $this->sma->convertMoney($temp['price']);
                }
                ?>
                </td>
                <td align="center" class="col-xs-1">
                <?php

                    if ($this->api_shop_setting[0]['out_of_stock'] == 1) {
                        if ($item->quantity > 0)
                            $temp = '<span class="api_color_green" style="color:#dc9b01 !important">'.lang('yes').'</span>';
                        else
                            $temp = '<span class="api_color_red" style="color:#dc9b01 !important">'.lang('no').'</span>';
                    }
                    else {
                        if ($item->quantity > 0)
                            $temp = '<span class="api_color_green" style="color:#dc9b01 !important">'.lang('yes').'</span>';
                        else
                            $temp = '<span class="api_color_green" style="color:#dc9b01 !important">'.lang('yes').'</span>';                        
                    }

                    if ($temp_product[0]['product_status'] == 'unavailable_today')
                        $temp = '
                            <span class="api_color_red" style="color:#dc9b01 !important">'.lang('no').'</span>
                        ';
                    echo $temp;
                ?>
                </td>
                <td class="col-xs-2">
                    <div class="btn-group btn-group-justified" role="group" aria-label="...">
<?php 
    if ($item->is_hided == 'show') { 
        $temp = '
            <div class="btn-group" role="group">
            <button type="button" class=" btn btn-sm btn-danger add-to-cart-2" data-id="'.$item->id.'" title="'.lang('add_to_cart').'"><i class="fa fa-shopping-cart"></i></button>
            </div>
            <div class="btn-group" role="group">
                <a href="'.base_url().'product/'.$item->slug.'">
                    <button type="button" class=" btn btn-sm btn-info" title="'.lang('view').'"><i class="fa fa-eye"></i></button>
                </a>
            </div>
        ';
    }
    else {
        $temp = '
            <div class="btn-group" role="group">
            <a href="javascript:void(0);" onclick="alert(\''.lang('Sorry, this_product_is_disabled.').'\');">
            <button type="button" class=" btn btn-sm btn-danger" disabled="disabled" title="'.lang('add_to_cart').'" ><i class="fa fa-shopping-cart"></i></button>
            </a>
            </div>
            <div class="btn-group" role="group">
            <a href="javascript:void(0);" onclick="alert(\''.lang('Sorry, this_product_is_disabled.').'\');">
                <button type="button" class=" btn btn-sm btn-info" disabled="disabled" title="'.lang('view').'"><i class="fa fa-eye"></i></button>
            </a>
            </div>
        ';        
    }

    if ($temp_product[0]['product_status'] == 'unavailable_today') {
        $temp = '
            <div class="btn-group" role="group">
            <a href="javascript:void(0);" onclick="alert(\''.lang('Sorry, this_product_is_disabled.').'\');">
            <button type="button" class=" btn btn-sm btn-danger" disabled="disabled" title="'.lang('add_to_cart').'" ><i class="fa fa-shopping-cart"></i></button>
            </a>
            </div>
            <div class="btn-group" role="group">
                <a href="'.base_url().'product/'.$item->slug.'">
                    <button type="button" class=" btn btn-sm btn-info" title="'.lang('view').'"><i class="fa fa-eye"></i></button>
                </a>
            </div>
        ';        
    }
    echo $temp;

    echo '
        <div class="btn-group" role="group">
            <button type="button" class="api_display_none btn btn-sm btn-success remove-wishlist" id="favorite_'.$item->id.'" data-id="'.$item->id.'" title="'.lang('delete').'"><i class="fa fa-trash-o"></i></button>
            <button type="button" class=" btn btn-sm btn-success" onclick="api_saa_alert_favorite(\'favorite_'.$item->id.'\',\'\',\'\',\'\');" title="'.lang('delete').'"><i class="fa fa-trash-o"></i></button>
        </div>
    ';

?>
                </div>
                </td>
            </tr>
            </table>
            </li>
            <?php
            $r++;
        }
        }
    echo '
        </ul>
        </div>
    ';        
//=PC=====================================================================================    
}


} else {
    echo '<div class="api_height_10"></div><strong>'.lang('wishlist_empty').'</strong>';
}
?>
    

<?php
if (!empty($items)) {
?>
<div class="row" style="margin-top:15px;">
    <div class="col-md-6">
        <span class="page-info line-height-xl hidden-xs hidden-sm">
            <?= str_replace(['_page_', '_total_'], [$page_info['page'], $page_info['total']], lang('page_info')); ?>
        </span>
    </div>
    <div class="col-md-6">
    <div id="pagination" class="pagination-right"><?php echo $this->pagination->create_links(); ?></div>
    </div>
</div>       
<?php
}
?>
    

                        </div>
                    </div>


                    <div class="col-sm-3 col-md-2">
                        <?php include('themes/default/shop/views/pages/sidebar2.php'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </div>
    
</section>
                            
<script type="text/javascript">

var addresses = <?= !empty($addresses) ? json_encode($addresses) : 'false'; ?>;

function api_change_favorite_order(id,value){
    var postData = {
        "id" : id,
        "value" : value,
    };
    
    var result = $.ajax
    (
    	{
    		url:site.base_url + "shop/api_change_favorite_order",
    		type: 'GET',
    		secureuri:false,
    		dataType: 'html',
    		data:postData,
    		async: false,
    		error: function (response, status, e)
    		{
    			alert(e);
    		}
    	}
    ).responseText;
    
    //var myWindow = window.open("", "MsgWindow", "width=700, height=400");
    //myWindow.document.write(result); 

	var array_data = String(result).split("api-ajax-request-multiple-result-split");
    if (array_data[1] == 'error')
        window.location = "<?php echo base_url(); ?>login";
    else
        window.location = "<?php echo base_url(); ?>shop/wishlist";
}

function api_drop_favorite_order(id){
    var postData = {
        "id" : id,
    };
    
    var result = $.ajax
    (
    	{
    		url:site.base_url + "shop/api_drop_favorite_order",
    		type: 'GET',
    		secureuri:false,
    		dataType: 'html',
    		data:postData,
    		async: false,
    		error: function (response, status, e)
    		{
    			alert(e);
    		}
    	}
    ).responseText;
    
    //var myWindow = window.open("", "MsgWindow", "width=700, height=400");
    //myWindow.document.write(result); 

	var array_data = String(result).split("api-ajax-request-multiple-result-split");
    if (array_data[1] == 'error')
        window.location = "<?php echo base_url(); ?>login";
    else {
        
    }
}

</script>

 
<script>


function api_favorite_order_drop(postData){   
    var result = $.ajax
    (
    	{
    		url:'<?php echo base_url(); ?>shop/api_favorite_order_drop',
    		type: 'GET',
    		secureuri:false,
    		dataType: 'html',
    		data:postData,
    		async: false,
    		error: function (response, status, e)
    		{
    			alert(e);
    		}
    	}
    ).responseText;

	var array_data = String(result).split("api-ajax-request-multiple-result-split");
	var b = array_data[0];
	var result_text = array_data[1];
    if (result_text != 'Success') {
        window.location = '<?php echo base_url(); ?>login';
    }
}
/*
    $('.api_drag_input').on('click touchstart', function() {
        var val = this.value; //store the value of the element
        this.value = ''; //clear the value of the element
        this.value = val;
        
        this.focus();
    });
    $('.api_drag_button').on('click touchend', function() {
        this.click();
    });
*/
document.getElementById('api_wishlist_search').onkeypress = function(e){
    if (!e) e = window.event;
    var keyCode = e.keyCode || e.which;
    if (keyCode == '13'){
      $('#api_wishlist_search_btn').click();
      return false;
    }
}
</script>        






