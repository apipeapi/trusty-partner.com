<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<section class="api_page_cart page-contents api_padding_bottom_0_im api_padding_top_0">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">

                    <div class="col-sm-8">
                        <div class="panel panel-default api_pannel_left">
                            <div class="panel-heading text-bold">
                                <i class="fa fa-shopping-cart margin-right-sm"></i> <?= lang('shopping_cart'); ?>
                                [ <?= lang('items'); ?>: <span id="total-items"></span> ]
                                <a href="<?= shop_url('products'); ?>" class="pull-right hidden-xs" style="color:black">
                                    <i class="fa fa-share"></i>
                                    <?= lang('continue_shopping'); ?>
                                </a>
                            </div>
                            <div class="panel-body" style="padding:0;">
                                <div class="cart-empty-msg <?=($this->cart->total_items() > 1) ? 'hide' :'';?>">
                                    <?= '<h4 class="text-bold">'.lang('empty_cart').'</h4>'; ?>
                                </div>
                                <form name="api_cart" action="" method="post">
<?php

if ($this->session->userdata('api_mobile') == 1) {
    $temp_display .= '                                
    <table id="api_cart_table_mobile" width="100%" class="table table-condensed table-striped table-cart margin-bottom-no" border="0">
    ';

    for ($i=0;$i<count($select_data);$i++) {
        if ($select_data[$i]['product_id'] != '') {
            if ($select_data[$i]['quantity'] >= $select_data[$i]['qty']) {
                $temp_stock = '<span class="api_color_green">'.lang('in_stock').'</span>';
            } else {
                $temp_stock = '<span class="api_color_red api_display_none">'.lang('out_of_stock').'</span>';
            }
            $temp_display .= '                                
<tr>
<td valign="top" style="vertical-align: top!important;">
    <table width="100%" border="0">
        <tr>
        <td valign="middle" align="center" width="150px;" >
                <a href="'.base_url().'product/'.$select_data[$i]['slug'].'">
                    <span class="cart-item-image pull-center api_height_80">
                    <img class="img-responsive" src="'.base_url().'assets/uploads/thumbs/'.$select_data[$i]['image'].'" alt=""></span>
                </a>
        </td>
        <td valign="top" style="vertical-align: top!important; padding-top:10px;">
            <a href="'.base_url().'product/'.$select_data[$i]['slug'].'">
                '.$select_data[$i]['name'].'
            </a>
            <div class="api_height_5"></div>
            <div>
                '.$this->sma->formatMoney($select_data[$i]['price']).'
            </div>
            '.$temp_stock.'            
        </td>
        </tr>
        <tr>
        <td valign="middle" align="center" class="api_padding_top_10">
            <input type="tel" id="api_cart_qty_'.$select_data[$i]['rowid'].'" name="api_qty" onchange="api_update_cart(\''.$select_data[$i]['rowid'].'\',\''.base_url().'cart/api_update_cart\',\''.base_url().'cart\');" class="form-control text-center api_numberic_input" value="'.$select_data[$i]['qty'].'" style="width:50px;">
        </td>    
        <td valign="middle" class="api_padding_top_10">
            <div class="api_float_left">
                <a href="javascript:void(0);" onclick="api_remove_cart(\'api_cart\',\''.$select_data[$i]['rowid'].'\',\''.base_url().'cart/api_remove_cart\',\''.base_url().'cart\');" id="" class="btn btn-danger btn-sm  ">
                    '.lang('Remove').'
                </a>                                        
            </div>
            <div class="api_float_left api_padding_top_7 api_padding_left_10">
                <strong>'.lang('subtotal').'</strong>: '.$this->sma->formatMoney($select_data[$i]['subtotal']).'
            </div>
            <div class="api_clear_both"></div>
        </td>    
        </tr>
    </table>
</td>    
</tr>
';
        }
    }

    $temp_display .= '                                
</table>
';
    echo $temp_display;

    $temp_remove_all = '
        <a href="'.site_url('cart/destroy').'" id="empty-cart" class="btn btn-danger btn-sm">
            <i class="fa fa-trash mr-1"></i>
            '.lang('empty_cart').'
        </a>        
    ';
} else {
    $temp_remove_all = '
        <a href="javascript:void(0);" onclick="api_select_check_remove_cart(\'api_cart\',\'api_check[]\',\''.base_url().'cart/api_remove_cart\',\''.base_url().'cart\');" id="" class="btn btn-danger btn-sm api_link_box_none"><i class="fa fa-trash mr-1"></i>
            '.lang('Remove').'
        </a>
    '; ?>                         
<div class="cart-contents">
    <div class="table-responsive">
        <table id="" class="table table-condensed table-striped table-cart margin-bottom-no">
            <thead>
                <tr>
                    <th>
                        <input class="api_pointer" type="checkbox" name="api_check_all" onclick="api_select_check_all(this,'api_check[]');" style="display: block; width: 30px; margin-top: 0px;" />
                    </th>
                    <th>#</th>
                    <th class="col-xs-4" colspan="2"><?= lang('product'); ?></th>
                    <th class="col-xs-3"><?= lang('option'); ?></th>
                    <th class="col-xs-1"><?= lang('qty'); ?></th>
                    <th class="col-xs-2" style="text-align: right !important;"><?= lang('price'); ?></th>
                    <th class="col-xs-2" style="text-align: right !important;"><?= lang('subtotal'); ?></th>

                </tr>
            </thead>

<?php
    $k = 1;
    for ($i=0;$i<count($select_data);$i++) {
        if ($select_data[$i]['product_id'] != '') {

            $config_data_2 = array(
                'table_name' => 'sma_products',
                'select_table' => 'sma_products',
                'translate' => 'yes',
                'select_condition' => "id = ".$select_data[$i]['product_id'],
            );
            $temp = $this->site->api_select_data_v2($config_data_2);
            $select_data[$i]['name'] = $temp[0]['title_'.$this->api_shop_setting[0]['api_lang_key']];
                        
            echo '
            <tr class="shopping_cart_bg api_link_box_none">
            <td>
                <input class="api_pointer" type="checkbox" name="api_check[]" value="'.$select_data[$i]['rowid'].'" style="width: 30px; display: block; margin-top: 0px;">
            </td>
            <td>'.$k.'</td>
            <td align="center">
                <a  href="'.base_url().'product/'.$select_data[$i]['slug'].'">
                    <span class="cart-item-image">
                    <img class="" src="'.base_url().'assets/uploads/thumbs/'.$select_data[$i]['image'].'" alt="">
                    </span>
                </a>
            </td>
            <td>
                <a class="product_title"  href="'.base_url().'product/'.$select_data[$i]['slug'].'">
                    '.$select_data[$i]['name'].'
                </a>
            </td>
            <td></td>
            <td>
                <input type="tel" id="api_cart_qty_'.$select_data[$i]['rowid'].'" name="api_qty" onchange="api_update_cart(\''.$select_data[$i]['rowid'].'\',\''.base_url().'cart/api_update_cart\',\''.base_url().'cart\');" class="form-control text-center api_numberic_input" value="'.$select_data[$i]['qty'].'" style="width:50px;">
            </td>
            <td class="text-right">'.$this->sma->formatMoney($select_data[$i]['price']).'</td>
            <td class="text-right">'.$this->sma->formatMoney($select_data[$i]['subtotal']).'</td>
            </tr>        
            ';
            $k++;
        }
    } ?>


                                        </table>
                                    </div>
                                </div>
<?php
}
?>
                                </form>
                            </div>
                            <div class="cart-contents">
                                <div id="cart-helper" class="panel panel-footer margin-bottom-no">
                                
                                    <?= $temp_remove_all ?>
                                    
                                    <a href="<?= shop_url('products'); ?>" class="btn btn-primary btn-sm pull-right api_link_box_none">
                                        <i class="fa fa-share"></i>
                                        <?= lang('continue_shopping'); ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="cart-contents">
                        <div class="col-sm-4">
                            <div id="sticky-con" class="margin-top-lg">                             
                                <div class="panel panel-default">
                                    <div class="panel-heading text-bold">
                                        <i class="fa fa-calculator margin-right-sm"></i> <?= lang('cart_totals'); ?>
                                    </div>
                                    <div class="panel-body">
                                        <table id="cart-totals" class="table table-borderless table-striped cart-totals"></table>
                                        <a  href="<?= site_url('cart/checkout'); ?>" class="btn btn-primary btn-lg btn-block btn-checkout btn-group-payment api_link_box_none" style="font-family: inherit;font-weight:700">
                                            <i class="fa fa-credit-card" style="margin-right:2rem"></i><?= lang('checkout'); ?>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>


                </div>
                <?php /*<code class="text-muted">* <?= lang('shipping_rate_info'); ?></code>*/?>
            </div>
        </div>
    </div>
</section>


<script>
function api_update_cart(rowid,url,url_redirect){
    var postData = [];

    postData['rowid'] = rowid;
    var result = $.ajax
    (
    	{
    		url:url + '?rowid=' + rowid + '&qty=' + document.getElementById('api_cart_qty_' + rowid).value,
    		type: 'GET',
    		secureuri:false,
    		dataType: 'html',
    		data:postData,
    		async: false,
    		error: function (response, status, e)
    		{
    			alert(e);
    		}
    	}
    ).responseText;    
	var array_data = String(result).split("api-ajax-request-multiple-result-split");
// var myWindow = window.open("", "MsgWindow", "width=700, height=400");
// myWindow.document.write(result);   
  
    if (array_data[1] == 'success') {
        window.location = url_redirect;
    }
    else if (array_data[2] == 'out_of_stock') {
        $('#api_swal3-question').hide();
        $('#api_swal3-warning').show();
        $('#api_swal3-info').hide();
        $('#api_swal3-success').hide();
        $('#api_modal_2_title').html('Out of stock');
        $('#api_modal_2_body').html(array_data[2]);

        $('#api_modal_2_btn_ok').hide();
        $('#api_modal_2_btn_close').show();

        $('#api_modal_2_btn_close').html('<button id="" type="button" role="button" tabindex="0" class="api_swal3-cancel api_swal3-styled" data-dismiss="modal" onclick="window.location = \'' + url_redirect + '\'" style="background-color: rgb(170, 170, 170);">Ok</button>');    

        $('#api_modal_2_trigger').click();
    }    
    else if (array_data[2] == 'minimum_quantity') {
        $('#api_swal3-question').hide();
        $('#api_swal3-warning').show();
        $('#api_swal3-info').hide();
        $('#api_swal3-success').hide();
        $('#api_modal_2_title').html('Minimum Quantity is ' + array_data[3]);
        $('#api_modal_2_body').html('For this product');

        $('#api_modal_2_btn_ok').hide();
        $('#api_modal_2_btn_close').show();

        $('#api_modal_2_btn_close').html('<button id="" type="button" role="button" tabindex="0" class="api_swal3-cancel api_swal3-styled" data-dismiss="modal" onclick="window.location = \'' + url_redirect + '\'" style="background-color: rgb(170, 170, 170);">Ok</button>');    

        $('#api_modal_2_trigger').click();
    }
}
</script>
