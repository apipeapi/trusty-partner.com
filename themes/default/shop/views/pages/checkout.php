<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php
$shipping = $this->sma->convertMoney($this->cart->shipping(), false, false);
$order_tax = $this->sma->convertMoney($this->cart->order_tax(), false, false);

$total = $this->cart->total();
$total = $this->sma->convertMoney($total, false, false);

if ($customer->vat_no) {
    if ($customer->vat_invoice_type != 'do') {
        $order_tax = ($total * 10) / 100;
    } else {
        $order_tax = 0;
    }
    $order_tax = $this->sma->convertMoney($order_tax, false, false);
}

$temp = $this->sma->formatDecimal($total) + $this->sma->formatDecimal($order_tax) + $this->sma->formatDecimal($shipping);
$temp = $this->api_helper->round_up_second_decimal($temp);
$grand_total_label = $this->sma->formatMoney($temp, $selected_currency->symbol);

if ($this->session->userdata('sale_consignment_auto_insert') != 'yes') {
    echo shop_form_open('order', 'class="validate" name="frm_order" id="frm_order"');
} else {
    echo shop_form_open('order_consignment', 'class="validate" name="frm_order" id="frm_order"');
}
?>

<section class="page-contents api_padding_bottom_0_im api_padding_top_0_im">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">

                    <div class="col-sm-8">
                        <div class="panel panel-default margin-top-lg"> 
                            <div class="panel-heading text-bold">
                                <i class="fa fa-shopping-cart margin-right-sm" style="margin-left:-8px"></i> <?= lang('checkout'); ?>
                                <a href="<?= site_url('cart'); ?>" class="pull-right"style="margin-left:3px;color:black">
                                    <i class="fa fa-share"></i>
                                    <?= lang('back_to_cart'); ?>
                                </a>
                            </div>
                            <div class="panel-body">

                                <div>
                                <?php
                                if (!$this->loggedIn) {
                                    ?>
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#user" aria-controls="user" role="tab" data-toggle="tab"><?= lang('returning_user'); ?></a></li>
                                        <li role="presentation api_display_none"><a href="#guest" aria-controls="guest" role="tab" data-toggle="tab"><?= lang('guest_checkout'); ?></a></li>
                                    </ul>
                                    <?php
                                }
                                ?>

                                    <div class="tab-content padding-lg api_padding_0_im">
                                        <div role="tabpanel" class="tab-pane fade in active" id="user">
                                            <?php
                                            if ($this->loggedIn) {

if ($this->api_shop_setting[0]['product_import_date'] == 1 && base_url() == 'https://air.phsarjapan.com/') {
    if ($this->session->userdata('api_selected_import_date') != '') {
        $temp2 = explode('<api>',$this->session->userdata('api_selected_import_date'));

        $date1 = date_create($temp2[0]);
        $date2 = date_create($temp2[1]);    
        echo '
            <input type="hidden" name="add_ons_product_import_date" id="add_ons_product_import_date" value="'.date_format($date1, 'jS, F Y').' - '.date_format($date2, 'jS, F Y').'" />
            <label class="control-label" for="delivery_date">
                <strong style="color: #f44336 !important;">'.lang('Import_Date').'</strong>
            </label>
            <div>
            '.date_format($date1, 'jS, F Y').' - '.date_format($date2, 'jS, F Y').'
            </div>
            <br>
        ';
    }
}
else
    echo '
        <input type="hidden" name="add_ons_product_import_date" id="add_ons_product_import_date" value="" />
    ';

                                                $temp_address = $customer->address;
                                                if ($customer->state != '') {
                                                    $temp_address .= ', '.$customer->state;
                                                }
                                                if ($customer->country != '') {
                                                    $temp_address .= ', '.$customer->country;
                                                }
                                                if ($customer->postal_code != '') {
                                                    $temp_address .= '<br>'.lang('postal_code').': '.$customer->postal_code;
                                                }
                                                $temp_script = '
    var temp_array = [0];
';
                                                if ($this->api_shop_setting[0]['display_company'] != 1) {
                                                    for ($i=0;$i<count($select_company);$i++) {
                                                        $temp_name = $select_company[$i]['company'];
                                                        if ($select_company[$i]['company'] == '' || $select_company[$i]['company'] == '-') {
                                                            $temp_name = $select_company[$i]['name'];
                                                        }
                                                        break;
                                                    }
                                                    echo '
        <label class="control-label" for="delivery_date">
            <strong style="color: #f44336 !important;">'.lang('name').'</strong>
        </label>
        <div>
            '.$temp_name.'
        </div>
        <br>    
    ';
                                                    $temp_display_2 = 'api_display_none';
                                                } else {
                                                    $temp_display_2 = '';
                                                }
                                                echo '
        <label class="control-label '.$temp_display_2.'" for="delivery_date">
            <strong style="color: #f44336 !important;">'.lang('Company').'</strong>
        </label>
        <div class="form-group '.$temp_display_2.'">
            <div class="controls"> 
    ';
                                                $temp_branch_name = '';
                                                $temp_address = $select_company[0]['address'];
                                                for ($i=0;$i<count($select_company);$i++) {
                                                    if ($select_company[$i]['company'] == '' || $select_company[$i]['company'] == '-') {
                                                        $select_company[$i]['company'] = $select_company[$i]['name'];
                                                    }
                                                    $tr2[$select_company[$i]['id']] = $temp.' '.$select_company[$i]['company'];

                                                    $select_company[$i]['address'] = nl2br($select_company[$i]['address']);
                                                    $select_company[$i]['address'] = preg_replace("/\r|\n/", "", $select_company[$i]['address']);

                                                    $select_company[$i]['address'] = str_replace('<br>', ' ', $select_company[$i]['address']);
                                                    $select_company[$i]['address'] = str_replace('<br/>', ' ', $select_company[$i]['address']);
                                                    $select_company[$i]['address'] = str_replace('<br />', ' ', $select_company[$i]['address']);

                                                    $temp_script .= 'temp_array['.$select_company[$i]['id'].'] = "'.$select_company[$i]['address'].'";';
                                                    if ($select_company[$i]['id'] == $user[0]['company_id']) {
                                                        $temp_branch_name = $select_company[$i]['company'];
                                                        $temp_address = $select_company[$i]['address'];
                                                    }
                                                }
                                                echo form_dropdown('customer_id', $tr2, $user[0]['company_id'], 'data-placeholder="'.lang("Please_select_a_customer").'" id="customer_id" class="form-control" onchange="api_set_checkout_address(this.value);" style="width:280px;"');
                                                echo '
            </div>
        </div>    
    ';


                                                if (is_array($select_company_branch)) {
                                                    if (count($select_company_branch) > 0) {
                                                        echo '
            <label class="control-label" for="delivery_date">
                <strong style="color: #f44336 !important;">'.lang('Branch').'</strong>
            </label>
            <div class="form-group">
                <div class="controls"> 
        ';
                                                        $temp_branch_name = '';
                                                        $temp_address = $select_company_branch[0]['address'];
                                                        for ($i=0;$i<count($select_company_branch);$i++) {
                                                            if ($select_company_branch[$i]['id'] != '') {
                                                                $tr2[$select_company_branch[$i]['id']] = $select_company_branch[$i]['title'];
                                                                $temp_script .= 'temp_array['.$select_company_branch[$i]['id'].'] = "'.$select_company_branch[$i]['address'].'";';
                                                                if ($select_company_branch[$i]['id'] == $user[0]['company_branch']) {
                                                                    $temp_branch_name = $select_company_branch[$i]['title'];
                                                                    $temp_address = $select_company_branch[$i]['address'];
                                                                }
                                                            }
                                                        }

                                                        if ($user[0]['company_branch_type'] != 'purchaser' || $user[0]['company_branch'] == '') {
                                                            echo form_dropdown('company_branch', $tr2, $user[0]['company_branch'], 'data-placeholder="'.lang("Please_select_a_customer").'" id="company_branch" class="form-control" onchange="api_set_checkout_address(this.value);" style="width:280px;"');
                                                        } else {
                                                            echo $temp_branch_name;
                                                            echo '<input type="hidden" name="company_branch" value="'.$user[0]['company_branch'].'">';
                                                        }
                                                        echo '
                </div>
            </div>    
        ';
                                                    }
                                                }


echo '
<div class="api_height_15"></div>
'; 


?>

<?php
if ($this->loggedIn) {  
    echo '
        <div class="col-sm-12 text-bold api_padding_0" style="color: #f44336 !important;">
            '.lang('select_address').'
        </div>
    ';

    $temp_default_address = '
        <div class="col-md-6">
            <div class="checkbox bg">
                <label>
                    <input type="radio" name="address" value="" checked>
                    <span class="api_border_red" id="default_delivery_address" onclick="var postData = {
                            \'field_id\' : \''.$customer->id.'\',
                            \'delivery_fee\' : \''.$customer->delivery_fee.'\',
                            };
                            ajax_getDeliveryAddress(postData);">
                        <span class="api_font_size_18 api_color_red">
                        <b>'.$customer->name.'</b>
                        </span><br>
                        <b>'.lang('Delivery_Address').'</b>: '.$customer->address.'<br>
                        <b>'.lang('Note_to_Driver').'</b>: '.$customer->note_to_driver.'<br>
                        <b>'.lang('phone').'</b> : '.$customer->phone.'<br>
                        ';
                        if ($customer->distance_from_restaurant != '')
                        $temp_default_address .= '
                        <b>'.lang('distance_from_our_restaurant').'</b>: '.$customer->distance_from_restaurant.'<br>
                        ';
                        $temp_default_address .= '
                        <b>'.lang('delivery_fee').'</b>: '.$this->sma->formatMoney($this->api_helper->calculate_delivery_fee($customer->distance_from_restaurant)).'<br>                        
                        <span class="api_display_none">'.lang('discount').' : '.$customer->address_discount.'%</span>
                    </span>
                </label>
            </div>
        </div>  
    ';
    echo '<div class="row">';
    echo $temp_default_address;
    if (!empty($addresses)) {
        echo shop_form_open('order', 'class="validate" name="frm_order" id="frm_order"');
        $r = 1;
        for ($i=0;$i<count($addresses_v2);$i++) {
            ?>
            <div class="col-md-6">
                <div class="checkbox bg">
                    <label>
                        <input type="radio" name="address" value="<?= $addresses_v2[$i]['id']; ?>">
                        <span class="api_border_red" onclick="var postData = {
                                    'address_id' : '<?= $addresses_v2[$i]['id'];?>',
                                    };
                                    ajax_getDeliveryAddress(postData);">
                            <?php
echo '
    <span class="api_font_size_18 api_color_red"><b>'.$addresses_v2[$i]['first_name'].' '.$addresses_v2[$i]['last_name'].'</b></span><br>
    <b>'.lang('Delivery_Address').'</b> : '.$addresses_v2[$i]['delivery_address'].'<br>
    <b>'.lang('Note_to_Driver').'</b> : '.$addresses_v2[$i]['note_to_driver'].'<br>
    <b>'.lang('phone').'</b> : '.$addresses_v2[$i]['phone'].'<br>
';
if ($addresses_v2[$i]['distance_from_restaurant'] != '')
echo '
    <b>'.lang('distance_from_our_restaurant').'</b> : '.$addresses_v2[$i]['distance_from_restaurant'].'<br>
';
echo '
    <b>'.lang('delivery_fee').'</b> : '.$this->sma->formatMoney($this->api_helper->calculate_delivery_fee($addresses_v2[$i]['distance_from_restaurant'])).'<br>                                      
';

                            ?>

                        </span>
                    </label>
                </div>
            </div>
            <?php
            $r++;
        }
    }
    echo '</div>';
    if (count($addresses) < 6 && !$this->Staff) {
        echo '<div class="row margin-bottom-lg">';
        echo '<div class="col-sm-12">
        <a class="api_link_box_none" href="javascript:void(0);" onclick="$(\'#api_modal_trigger_address_0\').click();" style="color:black">
            <button class="btn btn-primary btn-sm">
                '.lang('add_new_delivery_address').'
            </button>
        </a>
        
        </div>';
        echo '</div>';
    }
    if (isset($istates))
        echo '
            <script>
                var istates = '.json_encode($istates).'
            </script>
        ';
    else
        echo '
            <script>
                var istates = false; 
            </script>
        ';
}

echo '
    <div class="api_height_15"></div>
';
?>

                    <div class="form-group api_display_none">
                        <label class="control-label" for="delivery_date">
            			<strong style="color: #f44336 !important;"><?php echo lang("delivery_date"); ?></strong>
                        </label>
                        <div class="controls"> 
            				<?php
                                

                                if (date('w') != 0) {
                                    if (date('w') == 6) {
                                        $temp = lang('Next_Monday');
                                    } else {
                                        $temp = lang('Tomorrow');
                                    }
                                    
                                    if (date('H') < 18) {
                                        $tr['1'] = lang('Today').' 12:00 PM to 7:00 PM';
                                        $tr['2'] = $temp.' 9:00 AM to 12:00 PM';
                                        $tr['3'] = $temp.' 12:00 PM to 7:00 PM';
                                        $temp_selected = 1;
                                    } else {
                                        $tr['2'] = $temp.' 9:00 AM to 12:00 PM';
                                        $tr['3'] = $temp.' 12:00 PM to 7:00 PM';
                                        $temp_selected = 2;
                                    }
                                } else {
                                    $temp = lang('Tomorrow');
                                    $tr['2'] = $temp.' 9:00 AM to 12:00 PM';
                                    $tr['3'] = $temp.' 12:00 PM to 7:00 PM';
                                    $temp_selected = 2;
                                }
                                    
                                                echo form_dropdown('delivery_date', $tr, $temp_selected, 'data-placeholder="'.lang("Available_delivery_date").'" id="delivery_date" class="form-control" style="width:280px;"'); ?>
                        </div>
                    </div>
                    
                                                <?php

$temp_padding = 'api_padding_left_10 api_padding_right_10';

 

?>                                                

                                                <div><strong><?= lang('payment_method'); ?></strong></div>
                                                <input type="hidden" name="payment_method" id="payment_method" value="" id="" required="required">
                                                
                                                <?php

echo '
<div class="api_height_10"></div>
';

echo '
<table width="100%">
<tr>
<td align="center">   
';

if ($customer->payment_category != '' && $customer->payment_category != 'cash on delivery' && $this->api_shop_setting[0]['air_base_url'] != base_url())
    $temp = '';
else
    $temp = 'api_display_none';
echo '
<div class="api_payment_box '.$temp.'" title="Account Payable">
<table class="api_payment_box_table" id="api_payment_box_table_account_receivable" onclick="api_select_payment(\'account_receivable\');" width="100" height="88" border="0">
<tr>
<td valign="middle" align="center">
    <div class="api_padding_10">
        <strong style="color:#555555">Account<br />Payable<br /><span class="api_text_transform_capitalize">'.$customer->payment_category.'</span></strong>
    </div>
</td>
</tr>
</table>
</div>
';

if ($this->api_shop_setting[0]['air_base_url'] != base_url())
    $temp = '';
else
    $temp = 'api_display_none';
echo '
<div class="api_payment_box '.$temp.'" title="Cash on delivery">
<table class="api_payment_box_table" id="api_payment_box_table_cod" onclick="api_bank_in_form_action(\'hide\'); api_select_payment(\'cod\');" width="100" height="88" border="0">
<tr>
<td valign="middle" align="center">
    <img class="img-responsive" src="'.base_url().'assets/api/image/cod.png"/>
    </div>
</td>
</tr>
</table>
</div>
';

echo '
<div class="api_payment_box " title="ABA Bank Transfer">
<table class="api_payment_box_table" id="api_payment_box_table_aba" onclick="api_bank_in_form_action(\'show\');  api_select_payment(\'aba\');" width="100" height="88" border="0">
<tr>
<td valign="middle" align="center">
    <div class="api_padding_10">
    <img  src="'.base_url().'assets/api/image/aba.jpg" height="66"/>
    </div>
</td>
</tr>
</table>
</div>
';

if ((is_int(strpos($_SERVER["HTTP_HOST"],"localhost")) || base_url() == "https://phsarjapan.com/" || base_url() == "https://air.phsarjapan.com/") && $customer->sale_consignment_auto_insert != 'yes')
    $temp = '';
else
    $temp = 'api_display_none';
echo '
<div class="api_payment_box '.$temp.'" title="Pipay">
<table class="api_payment_box_table" id="api_payment_box_table_pipay" onclick="api_select_payment(\'pipay\');" width="100" height="88" border="0">
<tr>
<td valign="middle" align="center">
    <div class="api_padding_10">
    <img class="img-responsive" src="'.base_url().'assets/api/image/pipay_gateway_logo.png"/>
    </div>
</td>
</tr>
</table>
</div>
';

if ((is_int(strpos($_SERVER["HTTP_HOST"],"localhost")) || base_url() == "https://phsarjapan.com/" || base_url() == "https://air.phsarjapan.com/"))
    $temp = '';
else
    $temp = 'api_display_none';    
echo '
<div class="api_payment_box '.$temp.'" title="Wing">
<table class="api_payment_box_table" id="api_payment_box_table_wing" onclick="api_select_payment(\'wing\');" width="100" height="88" border="0">
<tr>
<td valign="middle" align="center">
    <div class="api_padding_10">
    <img class="img-responsive" src="'.base_url().'assets/api/image/wing_gateway_logo.png"/>
    </div>
</td>
</tr>
</table>
</div>
';

echo '
    <div class="api_clear_both"></div>
';


if ($customer->sale_consignment_auto_insert != 'yes')
    $temp = '';
else
    $temp = 'api_display_none';
    
if ($this->api_shop_setting[0]['air_base_url'] == base_url()){
    //if ($this->session->userdata('user_id') == 37 && $customer->sale_consignment_auto_insert != 'yes')
    if ($customer->sale_consignment_auto_insert != 'yes')
        $temp = '';
    else
        $temp = 'api_display_none';
}

$temp = 'api_display_none';
echo '
<div class="api_payment_box '.$temp.'" title="Credit/Debit Card" style="width:auto;">
<table class="api_payment_box_table" id="api_payment_box_table_payway_credit" onclick="api_select_payment(\'payway_credit\');"  height="88" width="280" border="0">
<tr>
<td class="api_padding_10" valign="middle" align="center">
    <img class="credit_card_image_1" src="'.base_url().'assets/images/payway_generic.png"  />
</td>
<td class="api_padding_right_10" valign="middle" align="left">
    <div class="payment_credit_card">Credit/Debit Card</div>
    <div class="payment_credit_card_2">VISA, Mastercard, UnionPay</div>
</td>
</tr>
</table>
</div>
';
echo '
<div class="api_payment_box '.$temp.'" title="ABA PAY" style="width:auto;">
<table class="api_payment_box_table" id="api_payment_box_table_payway_aba_pay" onclick="api_select_payment(\'payway_aba_pay\');" width="290" height="88" border="0">
<tr>
<td class="api_padding_10" valign="middle" align="center">
    <img class="credit_card_image_1" src="'.base_url().'assets/images/payway_pay.png"  />
</td>
<td class="api_padding_right_10" valign="middle" align="left">
    <div class="payment_credit_card">ABA PAY</div>
    <div class="payment_credit_card_2">Scan to pay with ABA Mobile</div>
</td>
</tr>
</table>
</div>
';




echo '
<div class="api_height_5 api_clear_both"></div>
</td>
</tr>
</table>
';

?>
<?php 
$api_bank_in_form = '
<div id="api_bank_in_form">
';

$api_bank_in_form .= '
    <div class="api_height_20"></div>
    <fieldset class="scheduler-border">
        <legend class="scheduler-border">'.lang('ABA_Payment').'</legend>
        <div class="col-md-12 '.$temp_padding.'">
            <div class="api_height_10"></div>
'.lang('Only_ABA_bank_is_available').'. '.lang('first_you_have_to_transfer').' <span id="grand_total_label" style="font-weight: bold;"><b>'.$grand_total_label.'</b></span>
'.lang('to_our_bank_account').'.
<div class="api_height_10"></div> 
    <div class="col-md-6 api_display_none">
        <ul>
            <li>                
                ABA account number : <b>000924240</b>
            </li>                
            <li>                
                ABA account name : <b>DONMARU by S.SENG</b><br>
            </li>  
        </ul>
    </div>
    <div class="col-md-12" align="center"><img src="'.base_url().'assets/api/image/aba_qr_code.png" style="width: 250px;"></div><br>
    <div class="col-md-12" style="margin-bottom: 10px;padding-top: 10px;">
        '.lang('then_fill_form').'
    </div>
            <div class="api_height_30"></div>
</div>
';


$api_bank_in_form .= '        
    <div class="col-md-6 api_display_none '.$temp_padding.'">                                 
    <div class="form-group">
        '.lang("your_bank", "your_bank").'
';
        $tr22['aba'] = 'ABA Bank';
        $api_bank_in_form .= form_dropdown('your_bank', $tr22, '', 'class="form-control select" id="your_bank" onchange="api_your_bank_change(this.value);" style="width:100%;"');                
$api_bank_in_form .= '
    </div>
    </div>
';

$api_bank_in_form .= '   
    <div class="col-md-6 api_display_none '.$temp_padding.'">                                 
    <div class="form-group">
        '.lang('transfer_amount', 'transfer_amount').'
        '.form_input('transfer_amount', $grand_total_label, 'class="form-control" id="transfer_amount" readonly="readonly"').'
    </div>
    </div>
';

$api_bank_in_form .= '   
    <div class="col-md-6 '.$temp_padding.'">                                 
    <div class="form-group">
        '.lang('Your_ABA_bank_account_number', 'Your_ABA_bank_account_number').'
        '.form_input('your_bank_account_number', '', 'class="form-control" id="your_bank_account_number"').'
    </div>
    </div>
';

$api_bank_in_form .= '   
    <div class="col-md-6 api_display_none '.$temp_padding.'">                                 
    <div class="form-group">
        '.lang('our_company_bank_account_number', 'our_company_bank_account_number').'
        '.form_input('our_company_bank_account_number', '000358355', 'class="form-control" id="our_company_bank_account_number" readonly="readonly"').'
    </div>
    </div>
';

$api_bank_in_form .= '                                    
    <div class="col-md-6 '.$temp_padding.'">                                 
    <div class="form-group">
        '.lang('ABA_Transfer_Reference_#', 'ABA_Transfer_Reference_#').'
        '.form_input('transfer_reference_number', '', 'class="form-control" id="transfer_reference_number"').'
    </div>
    </div>
';

$api_bank_in_form .= '
    <div class="api_height_15 api_clear_both"></div>
</div>
';

echo $api_bank_in_form;
?>
                                                <div class="form-group">
                                                    <?= lang('comment_any', 'comment'); ?>
                                                    <?= form_textarea('comment', set_value('comment'), 'class="form-control" id="comment" style="height:100px;"'); ?>
                                                </div>
                                                <?php
if (!$this->Staff) {
    echo '
        <div class="col-md-12 api_padding_0">
            <label class=" label-warning  api_padding_5 api_color_white">'.lang('Please click Submit Order button to complete your order').'</label>
        </div>
        <div class="api_height_30 api_clear_both"></div>
    ';
    echo '
        <div class="btn btn-primary btn-group-payment btn-group-payment-o btn-checkout" onclick="api_checkout();" style="width:100%">
            <i class="fa fa-money" aria-hidden="true" style="margin-right:2rem; font-weight: 700;" ></i>'.lang('submit_order').'
        </div>
    ';
    //echo form_submit('add_order',lang('submit_order'),'class="btn btn-theme api_display_none" id="frm_order_submit"');                                          
} elseif ($this->Staff) {
    echo '<div class="alert alert-warning margin-bottom-no">'.lang('staff_not_allowed').'</div>';
}
echo form_close();
                                            }
                                            ?>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>

                    <!--cart_total-->
                    <div class="col-sm-4">
                        <div id="sticky-con" class="margin-top-lg">
                            <div class="panel panel-default">
                                <div class="panel-heading text-bold">
                                    <i class="fa fa-shopping-cart margin-right-sm"></i> <?= lang('totals'); ?>
                                </div>
                                <div class="panel-body">                     
                                    <table class="table table-striped table-borderless cart-totals margin-bottom-no">
                                    <?php 
$sum_original_price = 0;
$sum_discount = 0;
$percent = 0;
$qty = 0;
$pp = 0;
foreach ($this->cart->contents() as $key => $value) {
    $config_data_2 = array(
        'id' => $value['product_id'],
        'customer_id' => $this->session->userdata('user_id'),
    );
    $temp_product = $this->site->api_calculate_product_price($config_data_2);
    $pdiscount = 0;
    if ($temp_product['promotion']['price'] > 0) {
        $sum_original_price += $temp_product['temp_price'] * $value['qty'];
        $pdiscount = $temp_product['temp_price'] - $temp_product['price'];
    }
    else
        $sum_original_price += $temp_product['price'] * $value['qty'];
    
    if ($pdiscount != 0)
        $percent = '
            <small>
                ('.number_format(($pdiscount * 100) / $sum_original_price,2).'%) 
            </small>
        ';
    else 
        $percent = ''; 
}
                                    ?>
<tr>
    <td><?= lang('original_price');?></td>
    <td class="text-right" id="pop"><?= $this->sma->formatMoney($sum_original_price, $selected_currency->symbol); ?></td>
    <td class="api_display_none" id="poriginal_price"><?= $sum_original_price;?></td>
</tr>
<tr class="">
    <td><?= lang('discount'); ?></td>
    <td class="text-right" id="discount"><?php echo $percent; echo $this->sma->formatMoney($pdiscount, $selected_currency->symbol); ?></td>
</tr>
<tr class="">
    <td><?= lang('total_after_discount'); ?></td>
    <td class="text-right" id="tts"><?= $this->sma->formatMoney($total, $selected_currency->symbol); ?></td>
</tr>
<tr class="api_display_none">
    <td><?= lang('service_charge'); ?></td>
    <td class="text-right"><small>(5%) </small><span id="sc"></span></td>
    <td class="text-right api_display_none" id="service_charge">5</td>
</tr>
                                        <?php if ($Settings->tax2 !== false && $order_tax > 0) {
                                                echo '<tr><td>'.lang('order_tax').'</td><td class="text-right">'.$this->sma->formatMoney($order_tax, $selected_currency->symbol).'</td></tr>';
                                            } ?>
                                        <tr>
                                            <td><?= lang('shipping'); ?> *</td>
                                            <td class="text-right" id="shipping_fee"><?= $this->sma->formatMoney($shipping, $selected_currency->symbol); ?></td>
                                        </tr>
                                        <tr class="api_display_none">
                                            <td><?= lang('vat'); ?></td>
                                            <td class="text-right"><small>(10%)</small> <span id="vt"></span></td>
                                            <td class="text-right api_display_none" id="vat">10</td>
                                        </tr>
                                        <tr class="active text-bold">
                                            <td><?= lang('grand_total').' <span style="color:#dc9b01;">('.lang('include_vat').')</span>'; ?></td>
                                            <td class="text-right api_display_none"><?= $grand_total_label; ?></td>
                                            <td class="text-right" id="grand_total"><?= $grand_total_label; ?></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--cart_total-->

                </div>
                <?php /* <code class="text-muted">* <?= lang('shipping_rate_info'); ?></code>*/?>
            </div>
        </div>
    </div>
</section>

<?php
    include 'themes/default/shop/views/sub_page/api_gateway_payway.php';
?>

<script type="text/javascript">

function api_select_payment(id){
    var postData = [];
    
    postData[0] = 'cod';
    postData[1] = 'pipay';
    postData[2] = 'wing';
    postData[3] = 'account_receivable';
    postData[4] = 'payway_credit';    
    postData[5] = 'payway_aba_pay';    
    postData[6] = 'aba';    

    for (var i=0; i<=5; i++) {
        document.getElementById("api_payment_box_table_" + postData[i]).classList.remove("api_payment_box_table_selected");
        if (id == postData[i])
            $("#payment_method").val(id);    
    }
    $("#payment_method").val(id);  
    document.getElementById("api_payment_box_table_" + id).classList.add("api_payment_box_table_selected");    
}

<?php
if ($customer->payment_category != '' && $customer->payment_category != 'cash on delivery') {
                                                echo '
document.getElementById("payment_method").value = "account_receivable";
api_select_payment("account_receivable");

    ';
                                            } else {
                                                echo '
document.getElementById("payment_method").value = "cod";    
api_select_payment("cod");
    ';
                                            }

?>

function api_set_checkout_address(id){
    <?php
    //echo $temp_script;
    ?>
    // if (id == '') id = 0;
    // document.getElementById('api_address').innerHTML = temp_array[id];
}
function api_checkout(){
    if (document.getElementById('payment_method').value == 'payway_credit' || document.getElementById('payment_method').value == 'payway_aba_pay') {
        
        var postData = {
            'payment_method' : document.getElementById('payment_method').value,
            'comment' : document.getElementById('comment').value,
            'customer_id' : document.getElementById('customer_id').value,
            'payment_method' : document.getElementById('payment_method').value,
            'delivery_date' : document.getElementById('delivery_date').value,
            'add_ons_product_import_date' : document.getElementById('add_ons_product_import_date').value,
        };

        var result = $.ajax
        (
            {
                url: '<?php echo base_url(); ?>shop/api_ajax_order_insert',
                type: 'GET',
                secureuri:false,
                dataType: 'html',
                data:postData, 
                async: false,
                error: function (response, status, e)
                {
                    alert(e);
                }
            }
        ).responseText;

        var array_data = String(result).split("api-ajax-request-multiple-result-split");
        var result_text = array_data[1];
// var myWindow = window.open("", "MsgWindow", "width=700, height=400");
// myWindow.document.write(result);           
        if (result_text == 'product_not_found') {
            alert('A product is not found.');
            window.location = '<?php echo base_url().'cart'; ?>';
        }
        else {
            document.api_form_payway.hash.value = array_data[2];
            document.api_form_payway.tran_id.value = array_data[3];
            document.api_form_payway.amount.value = array_data[4];
            document.api_form_payway.payment_option.value = array_data[5];
            AbaPayway.checkout();
        }
    }
    else
        document.frm_order.submit();
}
function api_bank_in_form_action(action){        
    if (action == 'show') {
        $('#api_bank_in_form').show('toggle');
    }
    if (action == 'hide') {
        $('#api_bank_in_form').hide('toggle');
    }
}
api_bank_in_form_action('hide');



</script>

<style>
.api_payment_box{
    margin-left:5px;
    margin-right:5px;
    margin-bottom:15px;
    width:105px;
    height:88px;
    border:0px solid red;
    display:inline-block;
}
.api_payment_box_table{
    border:1px solid #c4c4c4;    
    cursor:pointer;
}
.api_payment_box_table_selected{
    border:2px solid #f30000;    
    cursor:pointer;
}
.api_payment_box_table:hover{
    border:1px solid #f30000 !important;    
}
.api_button{
    padding: 16px 42px;
    width:100%;
    box-shadow: 0px 0px 6px -2px rgba(0,0,0,0.5);
    line-height: 1.25;
    background: #f8acaf;
    text-decoration: none;
    color: white;
    font-size: 16px;
    text-transform: uppercase;
    position: relative;
    overflow: hidden;
    border-radius:10px;
    text-align:center;
    border-color:#f8acaf !important;
    cursor:pointer;
}
.api_button:hover{
    background: #ef238b;
}
.api_button_air{
    padding: 16px 42px;
    width:100%;
    box-shadow: 0px 0px 6px -2px rgba(0,0,0,0.5);
    line-height: 1.25;
    background: #992824;
    text-decoration: none;
    color: white;
    font-size: 16px;
    text-transform: uppercase;
    position: relative;
    overflow: hidden;
    border-radius:10px;
    text-align:center;
    border-color:#f8acaf !important;
    cursor:pointer;
}
.api_button_air:hover{
    background: #ee7571;
}
</style>

<script>
function ajax_getDeliveryAddress(postData) {
    var result = $.ajax({
        url: '<?php echo base_url();?>shop/ajax_getDeliveryAddress',
        type: 'GET',
        secureuri:false,
        dataType: 'html',
        data:postData,
        async: false,
        error: function (response, status, e)
        {
        alert(e);
        }
    }).responseText;
    // var myWindow = window.open("", "MsgWindow", "width=700, height=400");
    // myWindow.document.write(result);     
    var array_data = String(result).split("api-ajax-request-multiple-result-split");
    var result_text = array_data[1];
    
    var shipping = '';
    if (array_data[1] != '') {
        shipping = array_data[1];
        $("#shipping_fee").text('$' + parseFloat(shipping).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
    } else {
        shipping = '0.00'
        $("#shipping_fee").text('$'+ shipping);
    }

    var total = document.getElementById('tts').innerHTML;
    var sc = document.getElementById('service_charge').innerHTML;
    var vt = document.getElementById('vat').innerHTML;
    var poriginal_price = document.getElementById('poriginal_price').innerHTML;
    total = total.replace('$',"");
    total = total.replace(",",""); // replace , to 1111.00
   
    var service_charge = (poriginal_price * 5) / 100;
    service_charge = 0;
    var grand_total = parseFloat(poriginal_price) + parseFloat(shipping) + parseFloat(service_charge);
    var vat = (grand_total * 10) / 100; // find vat 10% of (total + shipping + service_charge)
    vat = 0;
    var final_grand_total = parseFloat(vat) + parseFloat(total) + parseFloat(service_charge) + parseFloat(shipping);
    
    final_grand_total = round_up_second_decimal(final_grand_total);

    final_grand_total = api_formatMoney(final_grand_total);
    final_grand_total = final_grand_total.replace('$','');
    service_charge = api_formatMoney(service_charge);
    service_charge = service_charge.replace('$','');
    vat = Math.trunc(vat * 100) / 100;

    $('#grand_total').text(final_grand_total);
    $('#grand_total_label').text(final_grand_total);
    $('#sc').text(service_charge);
    $('#vt').text('$' + parseFloat(vat).toFixed(2));
   
}

$( document ).ready(function() {
   $( "#default_delivery_address" ).trigger( "click" );
});
</script>

<?php
//-google-map-for-add-----------------
$temp_google_function_id = 0;
$temp_distance_from_restaurant = '';
$temp_google_location = '';
$temp = explode(',',$this->api_shop_setting[0]['google_location']);
$temp_shop_lat = $temp[0];
$temp_shop_lng = $temp[1];
if ($temp_google_location == '') {
    $temp_initial_content = '<b>SUSHI DONMARU Restaurant</b><div><b>Pin Location: </b>'.$this->api_shop_setting[0]['google_location'].'</div>';
    $temp_initial_lat = $temp_shop_lat;
    $temp_initial_lng = $temp_shop_lng;
}
else {
    $temp_fee = $this->api_helper->calculate_delivery_fee($temp_distance_from_restaurant);
    $temp_initial_content = '<div><b>Your Pin Location: </b>'.$temp_google_location.'<div> <b>Distance from our restaurant: </b>'.$temp_distance_from_restaurant.'</div> <div><b>Delivery Fee: </b>'.$this->sma->formatMoney($temp_fee).'</div>';

    $temp = explode(',',$temp_google_location);
    $temp_initial_lat = $temp[0];
    $temp_initial_lng = $temp[1];    
}
$google_data[0] = array(
    'id' => $temp_google_function_id,
    'initial_content' => $temp_initial_content,
    'initial_lat' => $temp_initial_lat,
    'initial_lng' => $temp_initial_lng,
    'shop_lat' => $temp_shop_lat,
    'shop_lng' => $temp_shop_lng,    
);
//-google-map-----------------
$i = 0;
$addresses_v2[0] = array();
$addresses_v2[0]['id'] = 0;

echo '
<button type="button" id="api_modal_trigger_address_'.$addresses_v2[$i]['id'].'" class="api_display_none" data-toggle="modal" data-target="#api_modal_'.$addresses_v2[$i]['id'].'">Open Modal</button>

<div id="api_modal_'.$addresses_v2[$i]['id'].'" class="modal fade" role="dialog" >
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content" style="background-color:#333333;">
      <div class="modal-header" style="border-color:#000">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="api_modal_title">
            '.lang('add_new_address').'
        </h4>
      </div>
      <div class="modal-body" id="api_modal_body">
      <form id="api_form_address_'.$addresses_v2[$i]['id'].'" action="" class="validate">
        <div class="col-md-6">
            <div class="form-group">
                '.lang('Address_Name', 'Address_Name').'
                '.form_input('add_ons_first_name', $addresses_v2[$i]['first_name'], 'class="form-control tip" id="add_ons_first_name_'.$addresses_v2[$i]['id'].'"').'
            </div>
        </div>    
        <div class="col-md-6" id="phone_requried">
            <div class="form-group">
                '.lang('phone', 'phone').'
                '.form_input('api_field_phone', $addresses_v2[$i]['phone'], 'class="form-control tip" id="api_field_phone_'.$addresses_v2[$i]['id'].'" required="required"').'
            </div>
        </div>     
        <div class="col-md-6">
            <div class="form-group color_red_change">
                '.lang('Delivery_Address', 'Delivery_Address').'
                '.form_input('add_ons_delivery_address', $addresses_v2[$i]['delivery_address'], 'class="form-control  tip" id="google_address_'.$addresses_v2[$i]['id'].'" required="required" readonly="readonly"').'
            </div>
        </div> 
        <div class="col-md-6">
            <div class="form-group color_red_change">
                '.lang('Note_to_Driver', 'Note_to_Driver').'
                '.form_input('add_ons_note_to_driver', $addresses_v2[$i]['note_to_driver'], 'class="form-control  tip" id="add_ons_note_to_driver_'.$addresses_v2[$i]['id'].'" ').'
            </div>
        </div>          
        <div class="col-md-12">
            '.lang('google_map_pin', 'google_map_pin').'
            <div class="col-md-6 api_display_none">
                <input type="text" name="add_ons_google_location" id="add_ons_google_location_'.$addresses_v2[$i]['id'].'" value="'.$addresses_v2[$i]['google_location'].'" style="width:100%;">
            </div>
            <div class="col-md-6 api_display_none">
                <input type="text" name="add_ons_distance_from_restaurant" id="add_ons_distance_from_restaurant_'.$addresses_v2[$i]['id'].'" value="'.$addresses_v2[$i]['distance_from_restaurant'].'" style="width:100%;">
            </div>
            <div class="col-md-12 api_padding_0" id="google_location_error_'.$addresses_v2[$i]['id'].'" style="display:none; color:red;">
                '.lang('Please_pin_your_location_on_the_map').'
                <div class="api_height_10"></div>
            </div>

            <div id="map_'.$addresses_v2[$i]['id'].'" style="width:100%; height:400px;">
            </div>
        </div>
        <div class="api_height_15 api_clear_both"></div>
      </div>
      <div class="modal-footer" id="api_modal_footer" style="border:0px;">
        <button type="button" id="api_btn_update_address_'.$addresses_v2[$i]['id'].'" class="api_link_box_none btn btn-info" onclick="
        var postData = {
            \'field_id\' : \''.$addresses_v2[$i]['id'].'\',
        };
        api_ajax_update_address(postData);        
        ">
            '.lang('Add').'
        </button>
        <button type="button" id="api_modal_close_'.$addresses_v2[$i]['id'].'" class="api_link_box_none btn btn-info" data-dismiss="modal" >
            '.lang('Cancel').'
        </button>
        </form>
      </div>
    </div>
  </div>
</div>
';

include 'themes/default/shop/views/sub_page/api_google_map_distance.php';  
      
?>

<script>
function api_ajax_update_address(postData){
    var error = 0;
    if ($('#api_field_phone_' + postData['field_id']).val() == '' || $('#google_address_' + postData['field_id']).val() == '') {
        $('#api_form_address_' + postData['field_id']).submit(); 
        error = 1;        
    }
    else if ($('#add_ons_google_location_' + postData['field_id']).val() == '') {
        $('#google_location_error_' + postData['field_id']).show();
        $('#api_form_address_' + postData['field_id']).submit(); 
        error = 1;
    }
    else 
        $('#google_location_error_' + postData['field_id']).hide();

    if (error == 0) {
        postData['add_ons_first_name'] = $('#add_ons_first_name_' + postData['field_id']).val();
        postData['api_field_phone'] = $('#api_field_phone_' + postData['field_id']).val();
        postData['add_ons_delivery_address'] = $('#google_address_' + postData['field_id']).val();
        postData['add_ons_distance_from_restaurant'] = $('#add_ons_distance_from_restaurant_' + postData['field_id']).val();
        postData['add_ons_google_location'] = $('#add_ons_google_location_' + postData['field_id']).val();
        postData['add_ons_note_to_driver'] = $('#add_ons_note_to_driver_' + postData['field_id']).val();

        if (postData['api_field_phone'] == '' || postData['add_ons_delivery_address'] == '') {
            $('#api_form_address_' + postData['field_id']).submit();
        }
        else {

            var result = $.ajax
            (
                {
                    url: '<?php echo base_url(); ?>shop/api_ajax_update_address',
                    type: 'GET',
                    secureuri:false,
                    dataType: 'html',
                    data:postData,
                    async: false,
                    error: function (response, status, e)
                    {
                        alert(e);
                    }
                }
            ).responseText;
        // var myWindow = window.open("", "MsgWindow", "width=700, height=400");
        // myWindow.document.write(result);     
            var array_data = String(result).split("api-ajax-request-multiple-result-split");
            var result_text = array_data[1];
            
            if (parseInt(postData['field_id']) > 0) {
            }
            else {                
                location.href = '<?php echo base_url(); ?>cart/checkout';
            }
        }
    }
}

</script>