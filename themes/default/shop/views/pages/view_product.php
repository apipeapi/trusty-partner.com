<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<section class="page-contents api_padding_0_mobile">
    <div class="container product product_view_body" style="border: 0px; padding: 15px; padding-top: 0px; margin-bottom:0px; min-height: 0px;">
        <div class="row">
            <div class="col-xs-12">

                <div class="row">
                    <div class="col-sm-9 col-md-10">

                        <div class="panel panel-default margin-top-lg" style="margin-top:5px !important;">
                            <div class="panel-heading text-bold">

<?php
    $config_data_2 = array(
        'table_name' => 'sma_products',
        'select_table' => 'sma_products',
        'translate' => 'yes',
        'select_condition' => "id = ".$product->id,
    );
    $temp_product = $this->site->api_select_data_v2($config_data_2);
    $product->name = $temp_product[0]['title_'.$this->api_shop_setting[0]['api_lang_key']];
?>

                                <div class="api_float_left">
                                    <i class="fa fa-list-alt margin-right-sm"></i> <?= $product->name.' ('.$product->code.')'; ?>
                                </div>

                                <div class="api_float_right">
                                    <a href="<?= shop_url('products'); ?>" style="color:#000 !important"><i class="fa fa-share"></i> <?= lang('products'); ?></a>
                                </div>
                                <div class="api_clear_both"></div>
                            </div>
                            <div class="panel-body mprint">

                                <div class="row">
                                    <div class="col-sm-5">

                                        <div class="photo-slider">
                                            <div class="carousel slide article-slide" id="photo-carousel">

                                                <div class="carousel-inner cont-slider">
                                                    <div class="item active">
                                                        <a href="#" data-toggle="modal" data-target="#lightbox">
<?php

$temp_sold_out = array();
$temp_sold_out[2] = 'add-to-cart';

if ($this->api_shop_setting[0]['out_of_stock'] == 1 || $temp_product[0]['product_status'] == 'unavailable_today') {
    if ($product->quantity <= 0) {
        $temp_sold_out[0] = 'api_opacity_6';
        $temp_sold_out[1] = '
            <div class="api_absolute_center">
                <div class="api_sold_out_tag" style="visibility:hidden;font-size:20px;">
                    Sold Out
                </div>
            </div>      
        ';
        $temp_sold_out[2] = 'disabled';
        $temp_sold_out[3] = '<option value="0">0</option>';
        $temp_sold_out[4] = 'api_display_none';
        $temp_sold_out[5] = $api_view_array['sorry'];
        $temp_sold_out[6] = 'background-color:#808080; border-color:#808080;';
    }
    
}


$data_view = array(
    'wrapper_class' => '',
    'file_path' => 'assets/uploads/'.$product->image,
    'max_width' => 400,
    'max_height' => 400,
    'product_link' => 'true',
    'image_class' => 'img-responsive img-thumbnail '.$temp_sold_out[0],
    'image_id' => '',
    'resize_type' => 'full',
);
$temp = $this->site->api_get_image($data_view);
$temp_img = '
    '.$temp['image_table'].'
    '.$temp_sold_out[1].'
';


    echo $temp_img;
    echo $temp_sold_out[1];

?>
                                                        </a>
                                                    </div>
                                                    <?php
                                                    if (!empty($images)) {
                                                        foreach ($images as $ph) {
                                                            echo '<div class="item '.$temp_sold_out[0].'"><a href="#" data-toggle="modal" data-target="#lightbox"><img class="img-responsive img-thumbnail" src="' . base_url('assets/uploads/' . $ph->photo) . '" alt="' . $ph->photo . '" /></a>
                                                                '.$temp_sold_out[1].'
                                                            </div>';
                                                        }
                                                    }
                                                    ?>
                                                </div>

                                                <ol class="carousel-indicators">
                                                    <?php
                                                    if (!empty($images)) {
                                                        echo '
                                                            <li class="active" data-slide-to="0" data-target="#photo-carousel">
                                                                <img class="img-thumbnail" alt="" src="'.base_url().'assets/uploads/thumbs/'.$product->image.'">
                                                            </li>
                                                        ';
                                                    }

                                                    ?>
                                                    <?php
                                                    $r = 1;
                                                    if (!empty($images)) {
                                                        foreach ($images as $ph) {
                                                            echo '<li class="" data-slide-to="'.$r.'" data-target="#photo-carousel"><img class="img-thumbnail" alt="" src="'.base_url('assets/uploads/thumbs/' . $ph->photo).'"></li>';
                                                            $r++;
                                                        }
                                                    }
                                                    ?>

                                                </ol>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>

                                        <?php if (!$shop_settings->hide_price) { ?>
                                        <?php if (($product->type != 'standard' || $warehouse->quantity > 0) || $Settings->overselling) { ?>

<?php
    if ($product->is_favorite == 1) {
        $temp_is_favorite = 'fa-heart';
    } else {
        $temp_is_favorite = 'fa-heart-o';
    }
?>
                                        <div class="form-group">
                                            <div class="btn-group" role="group" aria-label="...">


<?php
if ($this->api_shop_setting[0]['out_of_stock'] == 1) {
    if ($product->quantity > 0) {
        $temp_display_1 = '';
        $temp_display_2 = '';
        if ($product->is_ordered != 1) {
            if ($this->session->userdata('user_id')) {
                $temp_display_1 = 'api_display_none';
                $temp = str_replace('add-to-cart', '', $temp_sold_out[2]);
                $temp_display_2 = '
                        <button class="btn btn-theme '.$temp.'" data-toggle="modal" data-target="#api_modal_add_cart" onclick="api_add_cart_confirm(\'product_featured_'.$product->id.'\');" style="width: calc(100% - 39px);"><i class="fa fa-shopping-cart padding-right-md"></i> 
                            '.lang('add_to_cart').'
                        </button>                  
                    ';
            }
        }
    }
}
?>
                                            </div>
                                        </div>
<?php
    $config_data = array(
        'id' => $product->id,
        'customer_id' => $this->session->userdata('company_id'),
    );
    $temp_promotion = $this->site->api_calculate_product_price($config_data);  
    if ($temp_promotion['promotion']['price'] > 0) {
        $temp_qty = $temp_promotion['promotion']['min_qty'];
    }
    else
        $temp_qty = 1;
echo '
<div class="api_margin_bottom_10 ">        

        <div class="clearfix"></div>
             
        <div class="product-bottom" style="padding-top: 0px; margin-top:0px;">
            <div class="" style="">
                <div class="form-group" style="margin-bottom:5px;">
                    <div class="input-group">                
                        <span class="input-group-addon pointer btn-minus api_pause_carousel"><span class="fa fa-minus"></span></span>
                        <input type="tel" name="quantity" id="api_page_view_product_quantity" min_qty="'.$temp_qty.'" onmouseout="sma_input_qty_mouse_out(\'api_page_view_product_quantity\','.$temp_qty.');" class="form-control text-center api_numberic_input quantity-input api_pause_carousel" value="'.$temp_qty.'" required="required">
                        <span class="input-group-addon pointer btn-plus api_pause_carousel"><span class="fa fa-plus"></span></span>
                    </div>
                </div>        
            </div>        
            <div class="clearfix"></div>
            
            <div class="product-cart-button">
                <div class="btn-group" role="group" aria-label="...">
                    <button class="btn btn-info add-to-wishlist" data-id="'.$product->id.'"><i style="color:#dc9b01"id="category_wishlist_heart_'.$product->id.'" class="fa '.$temp_is_favorite.'"></i></button>
            
                    <button class="btn btn-theme '.$temp_display_1.' '.$temp_sold_out[2].'" data-id="'.$product->id.'" id="product_featured_'.$product->id.'" style="width: calc(100% - 39px); '.$temp_sold_out[6].'"><i class="fa fa-shopping-cart padding-right-md"></i> 
                        '.lang('add_to_cart').'
                    </button>
                    '.$temp_display_2.'
                </div>
            </div>
            
            <div class="clearfix"></div>
        </div>
    </div>
';
?>
                                        <?php } else {
    echo '<div class="well well-sm"><strong>'.lang('item_out_of_stock').'</strong></div>';
} ?>
                                        <?php } ?>
                                    </div>

                                    <div class="col-sm-7">
                                        <div class="clearfix"></div>
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped dfTable table-right-left">
                                                <tbody>
                                                    <tr>
                                                        <td width="50%"><?= lang("name"); ?></td>
                                                        <td width="50%"><?= $product->name; ?></td>
                                                    </tr>
                                                    <?php if (!empty($product->second_name)) { ?>
                                                    <tr>
                                                        <td width="50%"><?= lang("secondary_name"); ?></td>
                                                        <td width="50%"><?= $product->second_name; ?></td>
                                                    </tr>
                                                    <?php } ?>
                                                    <tr>
                                                        <td><?= lang("code"); ?></td>
                                                        <td><?= $product->code; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td><?= lang("type"); ?></td>
                                                        <td><?= lang($product->type); ?></td>
                                                    </tr>
                                                    <?php
                                                    if ($brand->name!="" && $brand) {
                                                        echo'<tr>
                                                            <td>'.lang("brand").'</td>
                                                            <td><a href="'.site_url('brand/'.$brand->slug).'" class="line-height-lg">'.$brand->name.'</a></td>
                                                        </tr>';
                                                    }
                                                    ?>
                                                    <tr>
                                                        <td><?= lang("category"); ?></td>
                                                        <td><?= '<a href="'.site_url('category/'.$category->slug).'" class="line-height-lg">'.$category->name.'</a>'; ?></td>
                                                    </tr>
                                                    <?php if ($product->subcategory_id) { ?>
                                                    <tr>
                                                        <td><?= lang("subcategory"); ?></td>
                                                        <td><?= '<a href="'.site_url('category/'.$category->slug.'/'.$subcategory->slug).'" class="line-height-lg">'.$subcategory->name.'</a>'; ?></td>
                                                    </tr>
                                                    <?php } ?>

                                                    <?php if (!$shop_settings->hide_price) { ?>
                                                    <tr>
                                                        <td><?= lang("price"); ?></td>
                                                        <td>
<?php

    $config_data = array(
        'id' => $product->id,
        'customer_id' => $this->session->userdata('company_id'),
    );
    $temp = $this->site->api_calculate_product_price($config_data);   
    if ($temp['promotion']['price'] > 0) {
        if ($product->quantity < $temp_promotion['promotion']['min_qty']) {
            $temp_stock = lang('Out_of_Stock');
            $temp_class = 'danger';
        }
        else {
            $temp_stock = lang('In_Stock').' '.number_format($product->quantity,0);
            $temp_class = 'success';
        }                 
        if ($temp['promotion']['rate'] > 0)
            echo '
                <del class="text-red">'.$this->sma->convertMoney($temp['temp_price']).'</del> 
                <span style="color:#dc9b01">'.$this->sma->convertMoney($temp['price']).'</span>
                <div class="label label-'.$temp_class.' api_border_r10 api_margin_left_10 api_display_none" >
                    '.$temp_stock.'
                </div>              
            ';
        else
            echo '
                <span style="color:#dc9b01">'.$this->sma->convertMoney($temp['price']).'</span>
                <div class="label label-'.$temp_class.' api_border_r10 api_margin_left_10 api_display_none" >
                    '.$temp_stock.'
                </div>              
            ';        
    }
    else
        echo $this->sma->convertMoney($temp['price']);
?>
                                                                
                                                        </td>
                                                    </tr>
                                                    <?php } ?>

<?php
    if ($temp['promotion']['price'] > 0) {
        echo '
            <tr>
                <td>
                    ' . lang("Promotion_End_Date") . '
                </td>
                <td>
                    <div>
                    '.$temp['promotion']['duration_end_date'].' 
                    </div>
                    <div class="label label-info api_border_r10">
                        '.$temp['promotion']['duration_left'].'
                    </div>                     
                </td>
            </tr>
        ';
    }
?>

                                                    <?php if ($product->tax_rate) { ?>
                                                    <tr>
                                                        <td><?= lang("tax_rate"); ?></td>
                                                        <td><?= lang("include_vat"); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td><?= lang("tax_method"); ?></td>
                                                        <td><?= $product->tax_method == 0 ? lang('inclusive') : lang('exclusive'); ?></td>
                                                    </tr>
                                                    <?php } ?>

                                                    <tr>
                                                        <td><?= lang("unit"); ?></td>
                                                        <td><?= $unit ? $unit->name.' ('.$unit->code.')' : ''; ?></td>
                                                    </tr>


                                                    <?php if ($variants) { ?>
                                                    <tr>
                                                        <td><?= lang("product_variants"); ?></td>
                                                        <td>
<?php
    foreach ($variants as $variant) {
        echo '<span class="label label-primary">'.$variant->name.'</span> ';
    }
?>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>

                                                    <?php
if (!empty($options)) {
    foreach ($options as $option) {
        if ($option->wh_qty != 0) {
            echo '<tr><td colspan="2" class="bg-primary">' . $option->name . '</td></tr>';

            $config_data = array(
                'id' => $product->id,
                'customer_id' => $this->session->userdata('company_id'),
                'option_id' => $option->id,
            );
            $temp = $this->site->api_calculate_product_price($config_data);

            if ($this->sma->isPromo_v3($product)) {
                $temp_price = '
                    <del class="text-red">'.$this->sma->convertMoney($temp['temp_price']).'</del> 
                    '.$this->sma->convertMoney($temp['price']).'
                ';
            } else {
                $temp_price = $this->sma->convertMoney($temp['price']);
            }

            echo '
                <td>
                    ' .lang("price").': '.$temp_price.'
                </td>
            ';
            echo '</tr>';
        }
    }
}
                                                    ?>

                                                    <?php if ($product->cf1 || $product->cf2 || $product->cf3 || $product->cf4 || $product->cf5 || $product->cf6) {
                                                        if ($product->cf1) {
                                                            echo '<tr><td>' . lang("pcf1") . '</td><td>' . $product->cf1 . '</td></tr>';
                                                        }
                                                        if ($product->cf2) {
                                                            echo '<tr><td>' . lang("pcf2") . '</td><td>' . $product->cf2 . '</td></tr>';
                                                        }
                                                        if ($product->cf3) {
                                                            echo '<tr><td>' . lang("pcf3") . '</td><td>' . $product->cf3 . '</td></tr>';
                                                        }
                                                        if ($product->cf4) {
                                                            echo '<tr><td>' . lang("pcf4") . '</td><td>' . $product->cf4 . '</td></tr>';
                                                        }
                                                        if ($product->cf5) {
                                                            echo '<tr><td>' . lang("pcf5") . '</td><td>' . $product->cf5 . '</td></tr>';
                                                        }
                                                        if ($product->cf6) {
                                                            echo '<tr><td>' . lang("pcf6") . '</td><td>' . $product->cf6 . '</td></tr>';
                                                        }
                                                    } ?>


                                                </tbody>
                                            </table>
                                            <?php if ($product->type == 'combo') { ?>
                                            <strong><?= lang('combo_items') ?></strong>
                                            <div class="table-responsive">
                                                <table
                                                class="table table-bordered table-striped table-condensed dfTable two-columns">
                                                <thead>
                                                    <tr>
                                                        <th><?= lang('product_name') ?></th>
                                                        <th><?= lang('quantity') ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($combo_items as $combo_item) {
                                                        echo '<tr><td>' . $combo_item->name . ' (' . $combo_item->code . ') </td><td>' . $this->sma->formatQuantity($combo_item->qty) . '</td></tr>';
                                                    } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="col-xs-12">

                                    <?= $product->details ? '<div class="panel panel-info"><div class="panel-heading">' . lang('product_details_for_invoice') . '</div><div class="panel-body">' . $product->details . '</div></div>' : ''; ?>
                                    <?= $product->product_details ? '<div class="panel panel-default"><div class="panel-heading">' . lang('product_details') . '</div><div class="panel-body">' . $product->product_details . '</div></div>' : ''; ?>

                                </div>
                            </div>

                            <?php //include('share.php');?>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3 col-md-2">
                    <?php include('sidebar2.php'); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="api_height_20 api_clear_both"></div>

<div class="container">
    <div class="featured-products api_padding_top_15_mobile">
    <div class="row">
        <?php
        if (!empty($other_products)) {
            ?>
            <div class="col-xs-12">
                <h3 class="margin-top-no text-size-lg">
                    <?= lang('other_products'); ?>
                </h3>
            </div>
            <div class="row">
            <div class="col-xs-12">
                <?php
                foreach ($other_products as $fp) {
                        $config_data_2 = array(
                            'table_name' => 'sma_products',
                            'select_table' => 'sma_products',
                            'translate' => 'yes',
                            'select_condition' => "id = ".$fp->id,
                        );
                        $temp = $this->site->api_select_data_v2($config_data_2);
                    if ($temp[0]['product_status'] != 'unavailable_today') {
                        $fp->name = $temp[0]['title_'.$this->api_shop_setting[0]['api_lang_key']];                       
                    ?>
                    <div class="col-md-3 col-sm-6 col-xs-6 api_padding_bottom_10">
                        <div class="product api_margin_0" style="z-index: 1;">
                            <div class="details" style="transition: all 100ms ease-out 0s;">
                                <?php
                                if ($fp->promotion) {
                                    ?>
                                    <span class="badge badge-right theme" style="background-color: #fe0001 !important;"><?= lang('promo'); ?></span>
                                    <?php
                                } ?>

                                <?php
                                    if (is_file('assets/uploads/'.$fp->image)) {
                                        echo '
                                            <img src="'.base_url('assets/uploads/'.$fp->image).'" alt="'.$fp->image.'" class="img-responsive" style="max-height: 90px !important;">
                                        ';
                                    } else {
                                        echo '
                                            <img src="'.base_url('assets/uploads/no_image.jpg').'" alt="'.$fp->image.'" class="img-responsive" style="max-height: 90px !important;">
                                        ';
                                    } 
                                ?>

                                <?php if (!$shop_settings->hide_price) { ?>
                                <div class="image_overlay"></div>
                                <div class="btn add-to-cart" data-id="<?= $fp->id; ?>"><i class="fa fa-shopping-cart"></i> <?= lang('add_to_cart'); ?></div>
                                <?php } ?>
                                <div class="stats-container" style="height:auto !important;">
                                    
                                    <div class="product_name" style="overflow:hidden;">
                                        <a style="color: #dc9b01;"href="<?= site_url('product/'.$fp->slug); ?>"><?= $fp->name; ?></a>
                                    </div>
                                    
                                    
                                    <?php if (!$shop_settings->hide_price) { ?>
                                    <div class="api_height_10"></div>
                                    <div class="product_price" style="loat: none !important; position:relative; padding:0px !important; top:0px !important; left:0px !important; background-color:#333 !important;color:#dc9b01 !important">
                                        <?php
                                $config_data = array(
                                    'id' => $fp->id,
                                    'customer_id' => $this->session->userdata('company_id'),
                                );
                                $temp_promotion = $this->site->api_calculate_product_price($config_data);                                          
                                if ($temp_promotion['promotion']['price'] > 0) {
                                    if ($fp->quantity < $temp_promotion['promotion']['min_qty']) {
                                        $temp_stock = lang('Out_of_Stock');
                                        $temp_class = 'danger';
                                    }
                                    else {
                                        $temp_stock = lang('In_Stock').' '.number_format($fp->quantity,0);
                                        $temp_class = 'success';
                                    }   
                                    if ($temp_promotion['promotion']['rate'] > 0)                                    
                                        echo '
                                            <div class="api_float_left">
                                                <del class="text-red">'.$this->sma->convertMoney($fp->price).'</del>
                                            </div>
                                            <div class="api_float_right api_font_size_14 api_font_weight_normal">                            
                                                <div class="label label-'.$temp_class.' api_border_r10 api_display_none" >
                                                    '.$temp_stock.'
                                                </div>
                                            </div>                  
                                            <div class="api_clear_both"></div>
                                        ';
                                    else
                                        echo '
                                            <div class="api_float_right api_font_size_14 api_font_weight_normal">                            
                                                <div class="label label-'.$temp_class.' api_border_r10 api_display_none" >
                                                    '.$temp_stock.'
                                                </div>
                                            </div>
                                            <div class="api_clear_both"></div>
                                        ';

                                    echo '
                                        <div class="api_float_left api_margin_top_5" style="color:#dc9b01">
                                            '.$this->sma->convertMoney($temp_promotion['price']).'
                                        </div>
                                    ';
                                    if ($temp_promotion['promotion']['rate'] <= 0)
                                        echo '<div class="api_clear_both"></div>';


                                    echo '                            
                                        <div class="api_float_right api_margin_top_5 api_font_size_14 api_font_weight_normal">                            
                                            <div class="label label-info api_border_r10" >
                                                '.$temp_promotion['promotion']['duration_left'].'
                                            </div>
                                        </div>  
                                        <div class="api_clear_both"></div>

                                    ';  
                                } else {
                                    echo $this->sma->convertMoney(isset($fp->special_price) && !empty($fp->special_price) ? $fp->special_price : $fp->price);
                                }
                                        ?>
                                    </div>
                                    <div class="api_height_10"></div>                                    
                                <?php 
                                    }                                
                                ?>
                                    
                                    <div>
                                    <a href="<?= site_url('category/'.$fp->category_slug); ?>" class="link" style="color:#dc9b01"><?= $fp->category_name; ?></a>
                                    <?php
                                    if ($fp->brand_name) {
                                        ?>
                                        <span class="link">-</span>
                                        <a href="<?= site_url('brand/'.$fp->brand_slug); ?>" class="link"><?= $fp->brand_name; ?></a>
                                        <?php
                                    } ?>
                                    </div>
                                    <div class="api_height_10"></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="api_height_10"></div>
                            </div>
                        </div>
                    </div>
                    <?php
                    }
                } 
                ?>
            </div>
        </div>
            <?php
        }
        ?>
        <div class="api_clear_both"></div>
    </div>
    </div>
</div>
</section>

<div id="lightbox" class="modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-middle">
        <div class="modal-content">
            <button type="button" class="close hidden" data-dismiss="modal" aria-hidden="true">×</button>
            <div class="modal-body">
                <img src="" alt="" />
            </div>
        </div>
    </div>
</div>
<style type="text/css">
.product:hover{-webkit-box-shadow:0 0px 0px rgba(0,0,0,0);box-shadow:0 0px 0px rgba(0,0,0,0)}
.stats-container{
    background-color: #333 !important;
}
</style>