<?php

if ($this->session->userdata('user_id') != '') {
        $config_data = array(
            'featured' => 1,
            'user_id' => $this->session->userdata('user_id'),
            'limit' => 4,
        );
        $featured_products = $this->shop_model->getFeaturedProducts_v2($config_data);

        if (is_array($featured_products)) if (count($featured_products) > 0) {
            for ($i_3=0;$i_3<count($featured_products);$i_3++) {
                $config_data = array(
                    'id' => $featured_products[$i_3]->id,
                );
                $temp_hide = $this->shop_model->api_get_product_display($config_data);
                if ($temp_hide['result'] == 'hide') {
                    unset($featured_products[$i_3]);
                }
            }
            $temp_arr = array_values($featured_products); 
            $featured_products = $temp_arr;                            
        }      
        $side_featured = $featured_products;
}
echo '
<div id="sticky-con">
';
    if ($side_featured) {
        
        echo '
        <h4 class="margin-top-md title text-bold">
            <span>'.lang('featured').'</span>
            <div class="pull-right api_display_none">
                <div class="controls pull-right ">
                    <a class="left fa fa-chevron-left btn btn-xs btn-default" href="#carousel-example"
                    data-slide="prev"></a>
                    <a class="right fa fa-chevron-right btn btn-xs btn-default" href="#carousel-example"
                    data-slide="next"></a>
                </div>
            </div>
        </h4>
        ';

        echo '
        <div id="carousel-example" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
        ';
                $useragent=$_SERVER['HTTP_USER_AGENT'];
                $api_view_array = array();                
                if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))) {
                    $api_view_array['col_class'] = 'col-md-4 col-sm-6 col-xs-6';
                }
                else {
                    $api_view_array['col_class'] = 'col-md-12 col-sm-12';
                }

                foreach (array_chunk($side_featured, 1) as $fps) {
                    echo '
                    <div class="item active">
                    <div class="api_text_align_center">
                    ';
                        foreach ($fps as $fp) { 

                            $config_data_2 = array(
                                'table_name' => 'sma_products',
                                'select_table' => 'sma_products',
                                'translate' => 'yes',
                                'select_condition' => "id = ".$fp->id,
                            );
                            $temp = $this->site->api_select_data_v2($config_data_2);
                            $fp->name = $temp[0]['title_'.$this->api_shop_setting[0]['api_lang_key']];    
                            
                            
                            $config_data = array(
                                'id' => $fp->id,
                            );
                            $temp_hide = $this->shop_model->api_get_product_display($config_data);
                            if ($temp_hide['result'] != 'hide') {

$temp_sold_out = array();
$temp_2 = 'api_link_box_none';
if ($this->api_shop_setting[0]['out_of_stock'] == 1) {
    if ($fp->quantity <= 0) {
        $temp_sold_out[0] = 'api_opacity_6';
        $temp_sold_out[1] = '
            <div class="api_absolute_center" style="top:35% !important;">
                <div class="api_sold_out_tag api_pointer" style="visibility:hidden;">
                    Sold Out
                </div>
            </div>      
        ';
        $temp_sold_out[2] = 'disabled';
        $temp_sold_out[3] = '<option value="0">0</option>';
        $temp_sold_out[4] = 'api_display_none';
        $temp_sold_out[5] = $api_view_array['sorry'];
        $temp_2 = 'api_pointer';
    }
}
                                $data_view = array (
                                    'wrapper_class' => '',
                                    'file_path' => 'assets/uploads/'.$fp->image,
                                    'max_width' => 150,
                                    'max_height' => 135,
                                    'product_link' => 'true',
                                    'image_class' => $temp_2.' '.$temp_sold_out[0],
                                    'image_id' => '',   
                                    'resize_type' => 'full',
                                );
                                $temp = $this->site->api_get_image($data_view);
                                echo '
                                    <a href="'.site_url('product/'.$fp->slug).'">
                                        '.$temp['image_table'].'
                                        '.$temp_sold_out[1].'
                                    </a>
                                ';

                                echo '
                                <div class="product_name" style="font-weight:bold;max-height:50px !important; overflow:hidden;">
                                    <a href="'.site_url('product/'.$fp->slug).'" style="color:#dc9b01">'.$fp->name.'</a>
                                </div>
                                ';

                                echo '    
                                <a href="'.site_url('category/'.$fp->category_slug).'" class="link" <div style="color: #999 !important;" >'.$fp->category_name.'
                                </a>
                                ';
                                if ($fp->brand_name) {
                                    echo '
                                        <span class="link" <div style="color: #999 !important;" >-</span>
                                        <a href="'.site_url('brand/'.$fp->brand_slug).'" class="link" <div style="color: #999 !important;" >
                                            '.$fp->brand_name.'
                                        </a>
                                    ';
                                }
                                                                
                            }
                            

                        }
                    echo '
                    </div>
                    </div>
                    ';
                }


        echo '            
            </div>
        </div>
        ';        
    }
echo '            
</div>
';
?>
<style type="text/css">

</style>