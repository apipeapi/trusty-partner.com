<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<?php
 
$useragent=$_SERVER['HTTP_USER_AGENT'];
$api_view_array = array();
if ($this->session->userdata('api_mobile') == 1) {
    $api_view_array['col_class'] = 'col-md-4 col-sm-6 col-xs-6 api_padding_bottom_15';
    $api_view_array['col_class_odd'] = 'api_padding_right_7';
    $api_view_array['col_class_even'] = 'api_padding_left_7';
    $api_view_array['product_class'] = 'api_padding_0';
    $api_view_array['title_class'] = 'api_padding_5';
    $api_view_array['title_style'] = 'min-height:50px; max-height: 50px !important; overflow:hidden;';
    $api_view_array['quantity_class'] = 'api_display_none';
    $api_view_array['price_class_break'] = '<div class="api_clear_both"></div>';
    $api_view_array['price_style'] = 'float: left !important; padding-bottom:0px; padding-left:5px !important; padding-right:5px !important; text-align: left !important; width: 100% !important;';
    $api_view_array['select_qty_style'] = 'width: 100% !important; padding-bottom:0px; padding-left:5px !important; padding-right:5px !important;';
    $api_view_array['label_qty_class'] = 'api_display_none';
    $api_view_array['select_style'] = 'width:100% !important;';
    $api_view_array['btn_class'] = 'api_padding_right_5 api_padding_left_5';
    $api_view_array['btn_break'] = '<div class="api_clear_both api_height_5"></div>';
    $api_view_array['label_btn'] = lang('Add');
    $api_view_array['carousel_col'] = 2;
    $api_view_array['carousel_col_row'] = 2;
    $api_view_array['carousel_col_row_2'] = 2;
    $api_view_array['select_option'] = '';
    
    $api_view_array['sorry'] = '
        <div class="col-md-12 api_padding_bottom_5 api_padding_0" style="max-height:75px; min-height:75px; overflow:hidden;">
            <div class="" style="color: #f44336 !important; font-weight: 700; text-align:center; visibility:hidden">
                Sorry for inconvenient
            </div>
            <div class="" style="color: #f44336 !important; font-weight: 700; text-align:center;">
                Sorry for inconvenient
            </div>
        </div>   
    ';
    $api_view_array['sort_class'] = 'api_padding_right_15';
} else {
    $api_view_array['title_style'] = 'min-height:40px; max-height: 40px !important; overflow:hidden;';
    $api_view_array['col_class'] = 'col-md-4 col-sm-6 api_padding_bottom_15 api_padding_top_15';
    $api_view_array['label_btn'] = lang('add_to_cart');
    $api_view_array['carousel_col'] = 3;
    $api_view_array['carousel_col_row'] = 3;
    $api_view_array['carousel_col_row_2'] = 3;
    $api_view_array['carousel_script'] = '';
    $api_view_array['select_option'] = '<option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option>';
    $api_view_array['sorry'] = '
        <div class="col-md-12 api_padding_bottom_5" style="max-height:75px; overflow:hidden;">
            <div class="" style="color: #f44336 !important; height:57px; font-weight: 700; text-align:center; visibility:hidden">
                Sorry for inconvenient
            </div>         
            <div class="" style="color: #f44336 !important; font-weight: 700; text-align:center;">
                Sorry for inconvenient
            </div>
        </div>    
    ';
    $api_view_array['sort_class'] = '';
}
?>

<section class="page-contents page-products api_padding_top_5_mobile">
    <div class="container api_padding_0">
        <div class="<?php echo $api_view_array['sort_class']; ?> api_padding_top_10" id="grid-selector">
            <?php 
          
            if ($_GET['promotion_id'] > 0) {
                $config_data_2 = array(
                    'table_name' => 'sma_product_promotion',
                    'select_table' => 'sma_product_promotion',
                    'select_condition' => "id = ".$_GET['promotion_id'],
                );
                $temp_promotion_display = $this->site->api_select_data_v2($config_data_2);

                $temp_promotion_name = '
                    <img width="25" style="float:left; margin:-10px 10px 0px 0px" class="img-responsive" src="'.base_url('assets/images/hot-item.png').'">
                    <h3 class="margin-top-no text-size-lg" style="float:left">
                        <span>'.$temp_promotion_display[0]['name'].'</span>
                    </h3>
                    <div class="api_clear_both"></div>
                ';
            }
                
                // echo $_GET['promotion_id'];
            ?>
            <?php 
                //display title
                $config_data_2 = array(
                    'table_name' => 'sma_categories',
                    'select_table' => 'sma_categories',
                    'translate' => 'yes',
                    'select_condition' => "id > 0",
                );
                $temp_api_2 = $this->site->api_select_data_v2($config_data_2);
                for ($k=0;$k<count($temp_api_2);$k++) {
                    if($this->uri->segment(2) == $temp_api_2[$k]['slug'] ){    
                        $temp_category = '
                            <div class="text-size-lg api_padding_top_10">
                                '.$temp_api_2[$k]['title_'.$this->api_shop_setting[0]['api_lang_key']].'
                            </div>
                        ';
                    }
                }

                if ($_GET['search'] != '') {
                    $temp_result = '
                        '.lang('About').' '.count($select_data).' '.lang('result_for').' <b>'.$_GET['search'].'</b>
                    ';                   
                }
                else if (count($select_data) <= 0)
                    $temp_result = lang('No_result_found');
            ?>
            <div class="col-md-6">
                <?php 
                    echo $temp_promotion_name;               
                    echo $temp_category;
                    echo $temp_result;
                ?>
            </div>
            <div class="col-md-6">
                <div id="grid-sort" style="margin-top:13px; margin-right:0px;">
                    <div class="api_float_left" style="margin-left:-25px">
                        <?= lang('sort'); ?>:
                    </div>
                    <div class="sort" style="margin-left:-30px; display: inline-block;">
                        <?php
                        if (is_int(strpos($_SERVER["REQUEST_URI"],"?search"))) 
                            $temp = "window.location = '".$_SERVER['REQUEST_URI']."&sort_by=' + this.value;";
                        else
                            $temp = "window.location = '".$_SERVER['REQUEST_URI']."?search=' + $('#product-search').val() + '&sort_by=' + this.value;";

                        echo '<select name="sorting2" id="sorting2" class="selectpicker" data-style="btn-sm" data-width="150px" onchange="'.$temp.'">';

                        if ($_GET['sort_by'] == '') 
                            $temp_selected = 'selected'; 
                        else 
                            $temp_selected = '';
                        echo '
                            <option value="" '.$temp_selected.'>'.lang('ordering_number').'</option>
                        ';

                        if ($_GET['sort_by'] == 'name-asc') 
                            $temp_selected = 'selected'; 
                        else 
                            $temp_selected = '';
                        echo '
                            <option value="name-asc" '.$temp_selected.'>'.lang('name_asc').'</option>
                        ';

                        if ($_GET['sort_by'] == 'name-desc') 
                            $temp_selected = 'selected'; 
                        else 
                            $temp_selected = '';
                        echo '    
                            <option value="name-desc" '.$temp_selected.'>'.lang('name_desc').'</option>
                        ';

                        if ($_GET['sort_by'] == 'price-asc') 
                            $temp_selected = 'selected'; 
                        else 
                            $temp_selected = '';
                        echo '
                            <option value="price-asc" '.$temp_selected.'>'.lang('price_asc').'</option>
                        ';

                        if ($_GET['sort_by'] == 'price-desc') 
                            $temp_selected = 'selected'; 
                        else 
                            $temp_selected = '';
                        echo '
                            <option value="price-desc" '.$temp_selected.'>'.lang('price_desc').'</option>
                        ';

                        if ($_GET['sort_by'] == 'id-desc') 
                            $temp_selected = 'selected'; 
                        else 
                            $temp_selected = '';
                        echo '
                            <option value="id-desc" '.$temp_selected.'>'.lang('id_desc').'</option>
                        ';

                        if ($_GET['sort_by'] == 'id-asc') 
                            $temp_selected = 'selected'; 
                        else 
                            $temp_selected = '';
                        echo '
                            <option value="id-asc" '.$temp_selected.'>'.lang('id_asc').'</option>
                        ';

                        if ($_GET['sort_by'] == 'views-desc') 
                            $temp_selected = 'selected'; 
                        else 
                            $temp_selected = '';
                        echo '
                            <option value="views-desc" '.$temp_selected.'>'.lang('views_desc').'</option>
                        ';

                        if ($_GET['sort_by'] == 'views-asc') 
                            $temp_selected = 'selected'; 
                        else 
                            $temp_selected = '';
                        echo '
                            <option value="views-asc" '.$temp_selected.'>'.lang('views_asc').'</option>
                        ';

                        // if ($_GET['sort_by'] == 'promotion-asc') 
                        //     $temp_selected = 'selected'; 
                        // else 
                        //     $temp_selected = '';
                        // echo '
                        //     <option value="promotion-asc" '.$temp_selected.'>'.lang('Ascending_by_promotion_price').'</option>
                        // ';

                        if ($_GET['sort_by'] == 'promotion-desc') 
                            $temp_selected = 'selected'; 
                        else 
                            $temp_selected = '';
                        echo '
                            <option value="promotion-desc" '.$temp_selected.'>'.lang('promotion').'</option>
                        ';

                        if ($_GET['sort_by'] == 'featured') 
                            $temp_selected = 'selected'; 
                        else 
                            $temp_selected = '';
                        echo '
                            <option value="featured" '.$temp_selected.'>'.lang('Popular_And_New').'</option>
                        ';
                    
                        ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <?php

for ($i=0;$i<count($select_data);$i++) {
if ($select_data[$i]['id'] > 0) {    
    if (($i%2) == 0) {
        $class_record = $api_view_array['col_class_odd'];
    } else {
        $class_record = $api_view_array['col_class_even'];
    }

    if (!is_file('assets/uploads/'.$select_data[$i]['image'])) {
        $temp_image = 'no_image.jpg';
    } else {
        $temp_image = $select_data[$i]['image'];
    }

    $temp_sold_out = array();
    $temp_sold_out[3] = '<option value="1">1</option>';

    if ($this->api_shop_setting[0]['out_of_stock'] == 1 || $select_data[$i]['product_status'] == 'unavailable_today') {
        if ($select_data[$i]['quantity'] <= 0) {
            $temp_sold_out[0] = 'api_opacity_6';
            $temp_sold_out[1] = '
            <div class="api_absolute_center">
                <div class="api_sold_out_tag" style="visibility:hidden;">
                    Sold Out
                </div>
            </div>      
        ';
            $temp_sold_out[2] = 'disabled';
            $temp_sold_out[3] = '<option value="0">0</option>';
            $temp_sold_out[4] = 'api_display_none';
            $temp_sold_out[5] = $api_view_array['sorry'];
        }
    }
    $config_data_2 = array(
        'table_name' => 'sma_products',
        'select_table' => 'sma_products',
        'translate' => 'yes',
        'select_condition' => "id = ".$select_data[$i]['id'],
    );
    $temp = $this->site->api_select_data_v2($config_data_2);
    $select_data[$i]['name'] = $temp[0]['title_'.$this->api_shop_setting[0]['api_lang_key']];
    echo '<div class="page-products product-container '.$api_view_array['col_class'].'">
    <div class="product mobile api_margin_bottom_0">        
        <div class="product-top row"> 
            <div class="product-image col-5 col-md-5 col-sm-6">
                <a href="'.site_url('product/'.$select_data[$i]['slug']).'">
                    <img class="img-responsive '.$temp_sold_out[0].'" src="'.base_url('assets/uploads/thumbs/'.$temp_image).'">
                    '.$temp_sold_out[1].'
                </a>                
            </div>
            <div class="'.$api_view_array['title_class'].' col-7 col-md-7 col-sm-6" style="position: initial;">
                <div class="api_break_word" style="font-weight:700;padding:0px 10px;">
                    <a href="'.site_url('product/'.$select_data[$i]['slug']).'" style="color:#dc9b01">
                        '.$select_data[$i]['name'].'
                    </a>
                </div>
                <div class="product-price" style="position:absolute;bottom:94px;padding:0px 10px;">';
                    $config_data = array(
                        'id' => $select_data[$i]['id'],
                        'customer_id' => $this->session->userdata('company_id'),
                    );
                    $temp = $this->site->api_calculate_product_price($config_data);
                    $product[0] = $select_data[$i];
                    if ($temp['promotion']['price'] > 0) {
                        if ($select_data[$i]['quantity'] < $temp['promotion']['min_qty']) {
                            $temp_stock = lang('Out_of_Stock');
                            $temp_class = 'danger';
                        }
                        else {
                            $temp_stock = lang('In_Stock').' '.number_format($select_data[$i]['quantity'],0);
                            $temp_class = 'success';
                        }              
                        if ($temp['promotion']['rate'] > 0)
                            echo '
                                <del class="text-red">'.$this->sma->convertMoney($temp['temp_price']).'</del> 
                                '.$this->sma->convertMoney($temp['price']).'
                                <div class="api_float_left api_font_size_14 api_margin_right_10  api_font_weight_normal api_display_none">                            
                                    <div class="label label-'.$temp_class.' api_border_r10" >
                                        '.$temp_stock.'
                                    </div>
                                </div>                              
                                <div class="api_float_left api_font_size_14 api_font_weight_normal">                            
                                    <div class="label label-info api_border_r10" >
                                        '.$temp['promotion']['duration_left'].'
                                    </div>
                                </div>                                               
                            ';
                        else 
                            echo '                          
                                '.$this->sma->convertMoney($temp['price']).'
                                <div class="api_clear_both"></div>
                                <div class="api_float_left api_font_size_14 api_margin_right_10  api_font_weight_normal api_display_none">                            
                                    <div class="label label-'.$temp_class.' api_border_r10" >
                                        '.$temp_stock.'
                                    </div>
                                </div>        
                                <div class="api_float_left api_font_size_14 api_font_weight_normal">                            
                                    <div class="label label-info api_border_r10" >
                                        '.$temp['promotion']['duration_left'].'
                                    </div>
                                </div>                                                              
                                                
                            ';                        
                    } else {
                        echo $this->sma->convertMoney($temp['price']);
                    }
                echo '
                </div>
            </div>
            <div class="product-bottom mobile col-12 col-md-12" style="bottom:15px;position:absolute;">';
                if ($temp['promotion']['price'] > 0) {
                    $temp_qty = $temp['promotion']['min_qty'];
                }
                else
                    $temp_qty = 1;               
                echo $api_view_array['price_class_break'];
                echo $temp_sold_out[5];
                echo '
                <div class="'.$temp_sold_out[4].'" style="'.$api_view_array['select_qty_style'].'" style="width: 100% !important;">
                    <div class="form-group" style="margin-bottom:5px;">
                        <div class="input-group">                
                            <span class="input-group-addon pointer btn-minus api_pause_carousel"><span class="fa fa-minus"></span></span>
                            <input type="tel" name="quantity" id="api_product_list_quantity_'.$select_data[$i]['id'].'" onmouseout="sma_input_qty_mouse_out(\'api_product_list_quantity_'.$select_data[$i]['id'].'\','.$temp_qty.');" class="form-control text-center api_numberic_input quantity-input api_pause_carousel" value="'.$temp_qty.'" min_qty="'.$temp_qty.'" required="required">
                            <span class="input-group-addon pointer btn-plus api_pause_carousel"><span class="fa fa-plus"></span></span>
                        </div>
                    </div>        
                </div>        
                <div class="clearfix"></div>
                ';
                if ($select_data[$i]['is_favorite'] == 1) {
                    $temp_is_favorite = 'fa-heart';
                } else {
                    $temp_is_favorite = 'fa-heart-o';
                }
                echo '
                <div class="product-cart-button '.$temp_sold_out[4].' '.$api_view_array['btn_class'].'">
                    <div class="btn-group" role="group" aria-label="...">
                    <table width="100%" border="0">
                    <tr>
                ';
                    echo '
                    <td class="api_td_width_auto" valign="top"> 
                        <button class="btn btn-info add-to-wishlist" data-id="'.$select_data[$i]['id'].'"><i id="category_wishlist_heart_'.$select_data[$i]['id'].'" class="fa '.$temp_is_favorite.'"></i>
                        </button>
                    </td>
                    ';
                $temp_display_1 = '';
                if ($select_data[$i]['is_ordered'] != 1) {
                    if ($this->session->userdata('user_id')) {
                        $temp_display_1 = 'api_display_none';
                        echo '
                        <td valign="top"> 
                            <button class="btn btn-theme " '.$temp_sold_out[2].' data-toggle="modal" data-target="#api_modal_add_cart"  onclick="api_add_cart_confirm(\'product_featured_'.$select_data[$i]['id'].'\');" style="width:100% !important"><i class="fa fa-shopping-cart padding-right-md"></i> '.$api_view_array['label_btn'].'
                            </button>          
                        </td>              
                        ';
                    }
                }
                echo '
                    <td valign="top"> 
                        <button class="btn btn-theme add-to-cart '.$temp_display_1.'" '.$temp_sold_out[2].' data-id="'.$select_data[$i]['id'].'" id="product_featured_'.$select_data[$i]['id'].'" style="width:100% !important"><i class="fa fa-shopping-cart padding-right-md"></i> '.$api_view_array['label_btn'].'
                        </button>
                    </td>
                ';

                echo '
                    </tr>
                    </table>
                    <div class="api_height_10 api_screen_hide_768"></div>
                    </div>
                </div>
                ';
    echo $api_view_array['btn_break'];
    echo '</div>
        </div>      
    </div>
</div>';
}
}

?>

        <?php
if (count($select_data) > 0) {
    echo '<div class="clearfix"></div>
    <div class="col-md-12 api_padding_0">
        <div class="col-md-6">
            <span class="page-info2 line-height-xl hidden-xs hidden-sm">
                '.str_replace(['_page_', '_total_'], [$page_info['page'], $page_info['total']], lang('page_info')).'
            </span>        
        </div>
        <div class="col-md-6">
            <div id="pagination2" class="pagination-right">
                '.$this->pagination->create_links().'
            </div>
        </div>
    </div>
    <div class="clearfix"></div>';
}
?>

    </div>
</section>

<style>
.product .product-bottom .product-cart-button {
    margin-bottom: -13px;
}

.product .product-bottom .product-cart-button .btn-group .btn-theme {
    width: calc(100% - 40px) !important;

}

.bootstrap-select .open {
    margin-left: -23px;
}

.page-products .product .product-top {
    position: initial;
}

.product .product-bottom {
    border: none;
    left: 0;
}

@media only screen and (min-width:420px) {
    .product {
        height: 289px;
    }

    .product .product-image {
        width: 45.33333%;
        left: 20px;
    }

    .product .col-md-7 {
        width: 54.66666%
    }

    .img-responsive {
        max-height: 180px !important;
        max-width: 120%;
    }

    .product .product-image img {
        transform: translate(-14px, 2px);
    }

    .product .product-price {
        font-size: 2.5rem;
    }
}

@media only screen and (max-width:419px) {
    .product.mobile {
        height: 333px !important;
    }

    .product .product-bottom {
        padding: 0;
    }

    .col-md-6,
    .col-sm-6,
    .col-xs-6 {
        padding: 5px 5px;
        /* padding-bottom:13px */
        /* padding-bottom: 15px; */
    }

    .img-responsive,
    .img-thumbnail,
    .table,
    label {
        max-width: 125px;
    }

    .product-bottom.mobile {
        bottom: 0;
    }

    .product .product-top {
        width: 120%;
        padding: 0 0 0 5px;
    }
    .api_break_word{
        display: -webkit-box;
        line-height: 1.7;
        -webkit-line-clamp: 2;
        -webkit-box-orient: vertical;
        overflow: hidden;
    }
}
</style>