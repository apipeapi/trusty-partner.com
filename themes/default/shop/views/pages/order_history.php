<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section class="page-contents">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-sm-9 col-md-10 api_padding_right_0_pc">
                        
                        <div class="col-md-12 api_padding_0_mobile">
                            <h4 class="margin-top-md title text-bold">
                                <span><i class="fa fa-list-alt margin-right-sm"></i> <?= lang('Ordr_History'); ?></span>            
                                <div id="api_customer_menu_panel_btn" class="api_float_right api_pointer api_screen_show_768" onclick="$('#api_customer_menu_panel').toggle('slideToggle?');" style="top: -10px; margin-top: -10px;-10px">
                                    <img src="<?= base_url().'assets/api/image/af_header_menu_yamaha_toggle_icon.png' ?>" />
                                </div>
                            </h4>
                        </div>

                        <div id="api_customer_menu_panel" class="col-md-2 api_padding_right_0_pc api_padding_0_mobile api_screen_hide_768">
                            <div class="api_height_10 api_screen_hide_768"></div>
                            <?php include('themes/default/shop/views/pages/customer_menu_panel.php'); ?>                            
                        </div>
                        <div class="col-md-10 api_padding_0_mobile">
                            <div class="api_height_5"></div>

    <?php
    if (!empty($items)) {
        echo '<div class="table-responsive"><table width="100%" class="table table-striped table-hover table-va-middle">';
        echo '<thead><tr><th>'.lang('photo').'</th><th>'.lang('description').'</th><th>'.lang('price').'</th>
        <th style="text-align:center;width:1%; white-space:nowrap;">'.lang('in_stock').'</th><th style="text-align:center !important;">'.lang('actions').'</th></tr></thead>';
        $r = 1;
        foreach ($items as $item) {
            ?>
            <tr class="product">
                <td class="col-xs-1">
                <a href="#<?= $item->id; ?>">
                <img src="<?= base_url('assets/uploads/thumbs/'.$item->image); ?>" alt="" class="img-responsive">
                </a>
                </td>
                <td class="col-xs-7"><?= '<a href="#">'.$item->name.'</a><br>'.$item->details; ?></td>
                <td class="col-xs-1">
                <?php
                if ($item->promotion) {
                    echo '<del class="text-red">'.$this->sma->convertMoney($item->price).'</del><br>';
                    echo $this->sma->convertMoney($item->promo_price);
                } else {
                    echo $this->sma->convertMoney($item->price);
                }
                ?>
                </td>
                <td align="center" class="col-xs-1"><?= $item->quantity > 0 ? lang('yes') : lang('no'); ?></td>
                <td class="col-xs-2">
                    <div class="btn-group btn-group-justified" role="group" aria-label="...">
                      <div class="btn-group" role="group">
                        <button type="button" class="tip btn btn-sm btn-theme add-to-cart" data-id="<?= $item->id; ?>" title="<?= lang('add_to_cart'); ?>"><i class="fa fa-shopping-cart"></i></button>
                    </div>
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-sm btn-danger remove-wishlist" data-id="<?= $item->id; ?>"><i class="fa fa-trash-o"></i></button>
                    </div>
                </div>
                </td>
            </tr>
            <?php
            $r++;
        }
        echo '</table></div>';
    } else {
        echo '<div class="api_height_10"></div><strong>'.lang('Order_history_is_emply').'</strong>';
    }
    ?>
    

<?php
if (!empty($items)) {
?>
<div class="row" style="margin-top:15px;">
    <div class="col-md-6">
        <span class="page-info line-height-xl hidden-xs hidden-sm">
            <?= str_replace(['_page_', '_total_'], [$page_info['page'], $page_info['total']], lang('page_info')); ?>
        </span>
    </div>
    <div class="col-md-6">
    <div id="pagination" class="pagination-right"><?= $pagination; ?></div>
    </div>
</div>       
<?php
}
?>
    

                        </div>
                    </div>


                    <div class="col-sm-3 col-md-2">
                        <?php include(b'themes/default/shop/views/pages/sidebar2.php'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
                            
<script type="text/javascript">
var addresses = <?= !empty($addresses) ? json_encode($addresses) : 'false'; ?>;
</script>
