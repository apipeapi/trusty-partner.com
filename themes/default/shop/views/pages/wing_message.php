<?php

if ($this->session->userdata('api_payment_response')['transaction_id'] != '' && $this->session->userdata('api_payment_response')['api_payment_type'] == 'wing') {
    $temp_display = '
        <div class="row wing_card_box_padding">
            <div class="col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0">
                <div class="col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 ">
                    <div class="form-group">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                            <img src="https://stageonline.wingmoney.com/wingonlinesdk/public/imgs/success.png" class="image_success" alt="Success logo">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="lbl_thank text-center lbl_thank_font_size">Thanks For Your Payment!</div>
                    </div>
                    <div class="form-group">
                        <hr>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7 control-label lbl_info_padding_right">
                            <p class="form-control-static text-left merchant_total_lable lbl_success_info">Account No:</p>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-5 control-label lbl_info_padding_left">
                            <p class="form-control-static text-right lbl_success_info">'.$this->session->userdata('api_payment_response')['wing_account'].'</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7 control-label lbl_info_padding_right">
                            <p class="form-control-static text-left merchant_total_lable lbl_success_info">Wing Account Name:</p>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-5 control-label lbl_info_padding_left">
                            <p class="form-control-static text-right lbl_success_info">****USD</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7 control-label lbl_info_padding_right">
                            <p class="form-control-static text-left merchant_total_lable lbl_success_info">Wing Transaction ID:</p>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-5 control-label lbl_info_padding_left">
                            <p class="form-control-static text-right lbl_success_info">'.$_SESSION['api_payment_response']['transaction_id'].'</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7 control-label lbl_info_padding_right">
                            <p class="form-control-static text-left merchant_total_lable lbl_success_info">'.$_SESSION['api_payment_response']['biller_name'].':</p>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-5 control-label lbl_info_padding_left">
                            <p class="form-control-static text-right lbl_success_info">'.$_SESSION['api_payment_response']['currency'].' '.$_SESSION['api_payment_response']['total'].'</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="api_height_40 api_clear_both"></div>
        <button class="form-control btn text_box button_next succ_done_button api_font_size_16" onclick="$(\'#api_modal_2\').modal(\'hide\');">
            DONE
        </button>
    ';

    echo '
        <button type="button" id="btn_pop_up_modal_2" class="api_display_none" data-toggle="modal" data-target="#api_modal_2">Open Modal</button>
        <!-- Modal -->
        <div id="api_modal_2" class="modal fade" role="dialog" >
          <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content" style="border-radius:20px !important; background-color:#f4f4f4">
              <div class="modal-header api_display_none">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title api_modal_title">Input your information</h4>
              </div>
              <div class="modal-body api_padding_0">
                '.$temp_display.'
              </div>
              <div class="modal-footer api_display_none">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
        <style>
            .succ_done_button {
                margin: 0px;
                border-top-left-radius: 0px;
                border-top-right-radius: 0px;
                height: 50px;
                border-bottom-left-radius: 18px;
                border-bottom-right-radius: 18px;
            }
            .succ_done_button:hover{
                color:#fff !important;
            }
            .button_next {
                color: #ffffff;
                background: #0099dc;
            }
            @media (min-width: 768px)
            .form-horizontal .control-label {
                padding-top: 7px;
                margin-bottom: 0;
                text-align: right;
            }
            .lbl_success_info {
                font-size: 18px !important;
            }
            .form-control-static {
                min-height: 54px !important;
                padding-top: 7px !important;
                padding-bottom: 7px !important;
                margin-bottom: 0 !important;
            }
            .merchant_total_lable {
                color: #989898;
            }
            .lbl_info_padding_right {
                padding-right: 0px;
            }
            .image_success {
                width: 120px;
                margin-bottom: 25px;
                margin-top: 25px;
            }
            .lbl_thank_font_size {
                font-size: 25px !important;
            }
            .lbl_thank {
                font-weight: bold;
                color: #bcc102;
            }
            hr {
                border-top: 1px solid #ccc!important;
            }
        </style>
        <script>
            $("#btn_pop_up_modal_2").click();
        </script>        
    ';

    $data_session = array();
    $this->session->set_userdata('api_payment_response', $data_session);  
}

?>