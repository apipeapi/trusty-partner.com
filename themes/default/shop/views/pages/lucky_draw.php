<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<link rel="stylesheet" href="<?php echo base_url()?>themes/default/shop/assets/css/lucky_draw.css" type="text/css">

<?php 
  if($lucky_draw_code[0]['lucky_draw_status'] == 'win') {
    //display img with name product
    $pro_id = explode("-", $lucky_draw_code[0]['lucky_draw_win_product']); 
    $config_data = array(
      'table_name' => 'sma_products',
      'select_table' => 'sma_products',
      'translate' => 'yes',
      'select_condition' => "id = ".$pro_id[0]." order by id desc ",
    );
    $temp = $this->site->api_select_data_v2($config_data);
    for($i=0; $i<count($temp); $i++) {
      $data_view = array (
        'wrapper_class' => '',
        'file_path' => 'assets/uploads/'.$temp[$i]['image'],
        'max_width' => 200,
        'max_height' => 90,
        'product_link' => 'true',
        'image_class' => 'img-responsive',
        'image_id' => '',   
        'resize_type' => 'full',
      );
      $temp2 = $this->site->api_get_image($data_view);
      // $display = $temp[$i]['title_en'].$temp2['image_table'];
      $display = '
                  <div class="myDiv">Congratulation you win a product</div>
                  <div style="float: left;padding-top: 10px;">'.$temp2['image_table'].'</div>
                  <div style="padding-top: 40px;">'.$temp[$i]['title_en'].'</div>
                 ';
    }
  }
  if($lucky_draw_code[0]['lucky_draw_status'] == 'lost'){
    $display ='<img src="'.base_url().'themes/default/shop/assets/images/lost.jpg" style="width: 100%; padding-top: 6px;"/>';
  }
  if(count($lucky_draw_code) == 0) {
    if($this->uri->segment(3) != '' ) {
        $display = '<div style="padding-top:50px; font-size: 22px;">'.$this->uri->segment(3).' is not registered yet</div>';
    }    
  }

  if($this->uri->segment(3) == '' ) {
    $display = '<div style="padding-top:50px;  font-size: 22px;">Invalid URL</div>';
    $invalid_url = '<div>Invalid</div>';    
  }
  
  if($lucky_draw_code[0]['lucky_draw_status'] == 'playable') {
    $display = '<img src="'.base_url().'themes/default/shop/assets/images/welcome.jpg" style="width: 100%; padding-top: 6px;"/>';
  } 

  $temp = '';
  $temp_class = 'event_btn_stop_play';
  if($lucky_draw_code[0]['lucky_draw_status'] != 'playable') {
    $temp = 'disabled';
    $temp_class = 'event_btn_stop_play_disabled';
  }
  $display_button = "
    <button id='lucky_draw_btn_play' ".$temp." class='".$temp_class."' style='display:block;' onclick='myFunction(this.value);' value='PLAY'>
          PLAY
    </button>
    <button type='button' id='lucky_draw_btn_stop' ".$temp." class='".$temp_class." ' style='display:none;' onclick='
            var postData = {                 
                \"element_id\": \"item-list\",
                \"lucky_draw_code\": \"".$this->uri->segment(3)."\",
            };
            ajax_get_product_display(postData);'>
            STOP
    </button>

  ";

?>
    <div style="width: 100%">
        <div style="width: 800px; margin: auto; margin-top: 50px; text-align: center">
            <img src="<?php echo base_url();?>themes/default/shop/assets/images/game1.jpg" width="100%" />
            <div class="box">
                <span id="message_lucky_draw" class="style_message">
                  <?php 
                      if (count($lucky_draw_code) != '') {
                        for($i=0; $i<count($lucky_draw_code); $i++) {
                          echo $lucky_draw_code[$i]['lucky_draw_code'];
                        } 
                      } else 
                        echo 'Invalid';
                        // echo $invalid_url;
                  ?>
                </span>
            </div>
            <div class="box_1" id="item-list">
                <?php echo $display_button; ?>
            </div>
            <!-- message_box -->
            <div class="box_2" id="box_2">
                  <div class="m1 style_message_2">
                      <!-- alert_message -->
                      <?php
                          echo '<span>'.$display.'</span>';
                          $data_view = array (
                            'wrapper_class' => '',
                            'file_path' => 'themes/default/shop/assets/images/SMALL.gif',
                            'max_width' => 200,
                            'max_height' => 100,
                            'product_link' => 'true',
                            'image_class' => 'img-responsive',
                            'image_id' => '',   
                            'resize_type' => 'full',
                          );
                          $temp2 = $this->site->api_get_image($data_view);
                          echo '
                            <span class="api_display_none" id="playing_lucky_draw">
                                  '.$temp2['image_table'].'
                            </span>
                          ';
                      ?>
                  </div>
            </div>
        </div>
        
        <div id="display_all_product" style="display: none">
            <?php
              //explode , and -(cut comma and minus)
              $exp = explode("," , $lucky_draw_code[0]['lucky_draw_win_code']);
              $temp_display = '';
              if($lucky_draw_code[0]['lucky_draw_win_code'] != '')
              $temp_display .='<div class="product_title">Win History</div>';
              if($lucky_draw_code[0]['lucky_draw_win_code'] != '')
              $temp_display .= '<table class="all_product">
                                    <tr>
                                        <th>#</th>
                                        <th>Image</th>
                                        <th>Product Name</th>
                                        <th>Status</th>
                                    </tr>';
                $counter = 1;
                for($k=count($exp)-1;$k>=0;$k--) {
                  $exp_2 = explode("-" , $exp[$k]); //take the value that have exploded before for use
                  if ($exp_2[0] != '') {
                      $config_data = array(
                        'table_name' => 'sma_products',
                        'select_table' => 'sma_products',
                        'translate' => 'yes',
                        'select_condition' => "id = ".$exp_2[0]." order by id desc ",
                      );
                      $temp = $this->site->api_select_data_v2($config_data);
                      $data_view = array (
                        'wrapper_class' => '',
                        'file_path' => 'assets/uploads/'.$temp[0]['image'],
                        'max_width' => 200,
                        'max_height' => 100,
                        'product_link' => 'true',
                        'image_class' => 'img-responsive',
                        'image_id' => '',   
                        'resize_type' => 'full',
                      );
                      $temp2 = $this->site->api_get_image($data_view);
                      
                      if($exp_2[1] == 1){
                          $message = "Received";
                      } if($exp_2[1] == 0){
                          $message = "Not yet receive";
                      }                    
                      $temp_display .= '<tr>
                        <td>
                          '.$counter++.'
                        </td>
                        <td>
                          '.$temp2['image_table'].'
                        </td>
                        <td>
                          '.$temp[0]['title_en'].'
                        </td>
                        <td>
                          '.$message.'
                        </td>
                      </tr>';
                }
              }
              $temp_display .= '</table>';
              echo $temp_display;

            
            ?>
        </div>  
    </div>

    <script>
        function myFunction(value) {
          if(value == 'PLAY') {
            $("#box_2").html($("#playing_lucky_draw").html());
            $("#lucky_draw_btn_play").hide();
            $("#lucky_draw_btn_stop").show();
            
          } else {
            $("#lucky_draw_btn_play").show();
            $("#lucky_draw_btn_stop").hide();
          }
        }
        function ajax_get_product_display(postData){
          var result = $.ajax
          (
          {
            url: '<?php echo base_url(); ?>shop/ajax_get_product_display',
            type: 'GET',
            secureuri:false,
            dataType: 'html',
            data:postData,
            async: false,
            error: function (response, status, e)
            {
            alert(e);
            }
          }
          ).responseText;
          // var myWindow = window.open("", "MsgWindow", "width=700, height=400");
          // myWindow.document.write(result);     
          var array_data = String(result).split("api-ajax-request-multiple-result-split");
          var result_text = array_data[1];
          console.log(result_text);
          $("#lucky_draw_btn_play").show();
          $("#lucky_draw_btn_stop").hide();
          $("#box_2").html($("#playing_lucky_draw").hide());
          $("#box_2").html(result_text);
          if(result_text) {
            $("#lucky_draw_btn_play").addClass("event_btn_stop_play_disabled");
            $("#lucky_draw_btn_play").removeClass("event_btn_stop_play");
            $("#lucky_draw_btn_play").attr("disabled",true);  
            $("#display_all_product").html(array_data[2]);
          }
        }
    </script>

<!-- <script src="<?php //echo base_url();?>themes/default/shop/assets/js/lucky_draw.js"></script> -->
