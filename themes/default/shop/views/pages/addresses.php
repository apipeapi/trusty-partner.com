<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div id="myModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-confirm">
        <div class="modal-content" style="border-radius: 5px;">
            <div class="modal-header" style="border-bottom: none;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="icon-box">
                    <div class="swal3-icon swal3-question" id="swal3-question" style="">?</div>
                </div>              
                <h2 class="are_you_sure" id="conf_title" style="text-align:center"><?= lang('are_you_sure');?></h2>
                <p class="delivery_adderss" id="delivery_address"><?= lang('delete_delivery_address'); ?></p>
            </div><br>
            <div class="modal-footer" id="delete_data" style="border-top:none;text-align: center;">
                <button type="button" role="button"  tabindex="0" class="submit_delete btn_cancel_ok" style="background-color: rgb(48, 133, 214);"><?= lang('Okay'); ?></button>
                <button id="" type="button" role="button" tabindex="0" class="cancel_delete btn_cancel_ok" data-dismiss="modal" style="background-color: rgb(170, 170, 170);"><?= lang('Cancel'); ?></button>
            </div>
        </div>
    </div>
</div>

<section class="page-contents">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-sm-9 col-md-10 api_padding_right_0_pc">
                        
                        <div class="col-md-12 api_padding_0_mobile">
                            <h4 class="margin-top-md title text-bold">
                                <span><i class="fa fa-building margin-right-sm"></i> <?= lang('my_addresses'); ?></span>            
                                <div id="api_customer_menu_panel_btn" class="api_float_right api_pointer api_screen_show_768" onclick="$('#api_customer_menu_panel').toggle('slideToggle?');" style="top: -10px; margin-top: -15px;">
                                    <button class="btn btn-danger navbar-btn"><div class="fa fa-bars fa-lg"></div></button>
                                </div>
                            </h4>
                        </div>

                        <div id="api_customer_menu_panel" class="col-md-2 api_padding_right_0_pc api_padding_0_mobile api_screen_hide_768">
                            <div class="api_height_10 api_screen_hide_768"></div>
                            <?php include('themes/default/shop/views/pages/customer_menu_panel.php'); ?>                            
                        </div>
                        <div class="col-md-10 api_padding_0_mobile">
                            <div class="api_height_13"></div>

                                <?php
                                if ($this->Settings->indian_gst) { $istates = $this->gst->getIndianStates(); }
                                    $user_info = $this->site->getUser();
                                    $config_data = array(
                                        'table_name' => 'sma_companies',
                                        'select_table' => 'sma_companies',
                                        'translate' => '',
                                        'select_condition' => "id = ".$user_info->company_id,
                                    );
                                    $select_data = $this->site->api_select_data_v2($config_data);
                                    
$r = 1;
                                    
for($i=0;$i<count($addresses_v2);$i++) {
    if ($i != 0)
        $and = ' or ';
    $eid .= $and. ' id = '.$addresses_v2[$i]['id'].'';


    $temp_distance_from_restaurant = $addresses_v2[$i]['distance_from_restaurant'];
    $temp_google_location = $addresses_v2[$i]['google_location'];
    $temp_fee = $this->api_helper->calculate_delivery_fee($temp_distance_from_restaurant);
    $temp_initial_content = '<div><b>Delivery Address: </b>'.$addresses_v2[0]['delivery_address'].'<div><b>Your Pin Location: </b>'.$temp_google_location.'<div> <b>Distance from our restaurant: </b>'.$temp_distance_from_restaurant.'</div> <div><b>Delivery Fee: </b>'.$this->sma->formatMoney($temp_fee).'</div>';
    $temp = explode(',',$temp_google_location);
    $temp_initial_lat = $temp[0];
    $temp_initial_lng = $temp[1];  
    $temp = explode(',',$this->api_shop_setting[0]['google_location']);
    $temp_shop_lat = $temp[0];
    $temp_shop_lng = $temp[1];
    if ($temp_initial_lat == '')
        $temp_initial_lat = $temp_shop_lat;
    if ($temp_initial_lng == '')
        $temp_initial_lng = $temp_shop_lng;

    $google_data[$r] = array(
        'id' => $addresses_v2[$i]['id'],
        'initial_content' => $temp_initial_content,
        'initial_lat' => $temp_initial_lat,
        'initial_lng' => $temp_initial_lng,
        'shop_lat' => $temp_shop_lat,
        'shop_lng' => $temp_shop_lng,    
    );            
echo '
<div class="row">
<div class="col-md-12" id="element_id_address_'.$addresses_v2[$i]['id'].'">
    <div class="col-md-12 api_padding_0 link-address api_address_detail">
        <div class="api_height_10"></div>        
        <div class="col-md-12 api_font_size_20">
            <span id="api_span_first_name_'.$addresses_v2[$i]['id'].'">'.$addresses_v2[$i]['first_name'].'</span>
        </div>
        <div class="col-md-12">
            <b>'.lang('Delivery_Address').'</b> : <span id="api_span_delivery_address_'.$addresses_v2[$i]['id'].'">'.$addresses_v2[$i]['delivery_address'].' '.$addresses_v2[$i]['postal_code'].'</span>
        </div>
        <div class="col-md-6">
            <b>'.lang('Note_to_Driver').'</b> : <span id="api_span_note_to_driver_'.$addresses_v2[$i]['id'].'">'.$addresses_v2[$i]['note_to_driver'].'</span>
        </div>
        <div class="col-md-6">
            <b>'.lang('phone_number').'</b> : <span id="api_span_phone_'.$addresses_v2[$i]['id'].'">'.$addresses_v2[$i]['phone'].'</span>
        </div>     
        ';
        if ($addresses_v2[$i]['distance_from_restaurant'] != '')
            echo '
            <div class="col-md-6">
                <b>'.lang('distance_from_our_restaurant').'</b> : <span id="api_span_distance_from_our_restaurant_'.$addresses_v2[$i]['id'].'">'.$addresses_v2[$i]['distance_from_restaurant'].'</span>
            </div>
            ';      
        echo '  
        <div class="col-md-6">
            <b>'.lang('delivery_fee').'</b> : 
            <span id="api_span_delivery_fee_'.$addresses_v2[$i]['id'].'">'.$this->sma->formatMoney($this->api_helper->calculate_delivery_fee($addresses_v2[$i]['distance_from_restaurant'])).'</span>
        </div>

     
        <div class="api_height_10 api_clear_both"></div>
        <div class="api_link_box_none" style="position: absolute; right: 5px; bottom: 4px;">
            <i onclick="update_delivery_address('.$addresses_v2[$i]['id'].');" title="Edit" class="fa fa-edit fa-2x api_display_none" style="color:red; width:35px !important; height:33px;"></i>
            <a href="javascript:void(0);" onclick="
                var postData = {
                    \'title\' : \''.lang('update_address').'\',
                    \'address_id\' : '.$addresses_v2[$i]['id'].',
                    \'element_id\' : \'element_id_address_'.$addresses_v2[$i]['id'].'\'
                };               
                api_temp_update_address(postData);            
            "><i title="Edit" class="fa fa-edit fa-2x" style="color:red; width:35px !important; height:33px;"></i></a>
        </div>
        <div class="api_link_box_none " style="position: absolute; right: 50px; bottom: 8px; cursor: pointer;">
            <div class="delete" onclick="
                        var postData = {
                            \'address_id\' : '.$addresses_v2[$i]['id'].',
                            \'element_id\' : \'element_id_address_'.$addresses_v2[$i]['id'].'\'
                        };
                        delete_address(postData);">
                <i title="Delete" class="fa fa-trash fa-2x" style="color:red; width:25px !important; height:30px;"></i>
            </div>
        </div>
    </div>
</div>
</div>
';

echo '
<button type="button" id="api_modal_trigger_address_'.$addresses_v2[$i]['id'].'" class="api_display_none" data-toggle="modal" data-target="#api_modal_'.$addresses_v2[$i]['id'].'">Open Modal</button>

<div id="api_modal_'.$addresses_v2[$i]['id'].'" class="modal fade" role="dialog" >
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content" style="background-color:#333333;">
      <div class="modal-header" style="border-color:#000">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="api_modal_title">
            '.lang('update_address').'
        </h4>
      </div>
      <div class="modal-body" id="api_modal_body">
      <form id="api_form_address_'.$addresses_v2[$i]['id'].'" action="" class="validate">
        <div class="col-md-6">
            <div class="form-group">
                '.lang('Address_Name', 'Address_Name').'
                '.form_input('add_ons_first_name', $addresses_v2[$i]['first_name'], 'class="form-control tip" id="add_ons_first_name_'.$addresses_v2[$i]['id'].'"').'
            </div>
        </div>     
        <div class="col-md-6" id="phone_requried">
            <div class="form-group">
                '.lang('phone', 'phone').'
                '.form_input('api_field_phone', $addresses_v2[$i]['phone'], 'class="form-control tip" id="api_field_phone_'.$addresses_v2[$i]['id'].'" required="required"').'
            </div>
        </div>     
        ';
        if ($this->api_shop_setting[0]['google_map_api'] == 1)
            $temp = 'required="required" readonly="readonly"';
        else
            $temp = 'required="required"';                        
        echo '
        <div class="col-md-6">
            <div class="form-group color_red_change">
                '.lang('Delivery_Address', 'Delivery_Address').'
                '.form_input('add_ons_delivery_address', $addresses_v2[$i]['delivery_address'], 'class="form-control  tip" id="google_address_'.$addresses_v2[$i]['id'].'" '.$temp).'
            </div>
        </div>   
        <div class="col-md-6">
            <div class="form-group color_red_change">
                '.lang('Note_to_Driver', 'Note_to_Driver').'
                '.form_input('add_ons_note_to_driver', $addresses_v2[$i]['note_to_driver'], 'class="form-control  tip" id="add_ons_note_to_driver_'.$addresses_v2[$i]['id'].'" ').'
            </div>
        </div>   
        ';
        
        if ($this->api_shop_setting[0]['google_map_api'] == 1)
            $temp = '';
        else
            $temp = 'api_display_none';
        echo '
        <div class="col-md-12 '.$temp.'">
            '.lang('google_map_pin', 'google_map_pin').'
            <div class="col-md-6 api_display_none">
                <input type="text" name="add_ons_google_location" id="add_ons_google_location_'.$addresses_v2[$i]['id'].'" value="'.$addresses_v2[$i]['google_location'].'" style="width:100%;">
            </div>
            <div class="col-md-6 api_display_none">
                <input type="text" name="add_ons_distance_from_restaurant" id="add_ons_distance_from_restaurant_'.$addresses_v2[$i]['id'].'" value="'.$addresses_v2[$i]['distance_from_restaurant'].'" style="width:100%;">
            </div>
            <div class="col-md-12 api_padding_0" id="google_location_error_'.$addresses_v2[$i]['id'].'" style="display:none; color:red;">
                '.lang('Please_pin_your_location_on_the_map').'
                <div class="api_height_15"></div>
            </div>

            <div id="map_'.$addresses_v2[$i]['id'].'" style="width:100%; height:400px;">
            </div>
        </div>
        ';
        echo '
        <div class="api_height_15 api_clear_both"></div>
      </div>
      <div class="modal-footer" id="api_modal_footer" style="border:0px;">
        <button type="button" id="api_btn_update_address_'.$addresses_v2[$i]['id'].'" class="api_link_box_none btn btn-info" onclick="
        var postData = {
            \'field_id\' : \''.$addresses_v2[$i]['id'].'\',
        };
        api_ajax_update_address(postData);        
        ">
            '.lang('update').'
        </button>
        <button type="button" id="api_modal_close_'.$addresses_v2[$i]['id'].'" class="api_link_box_none btn btn-info" data-dismiss="modal" >
            '.lang('Cancel').'
        </button>
        </form>
      </div>
    </div>
  </div>
</div>
';


    $r++;
}

if (count($addresses) < 6) {
    echo '
    <div class="row margin-top-lg">
        <div class="col-sm-12">
            <a href="javascript:void(0);" onclick="$(\'#api_modal_trigger_address_0\').click();">
                <button class="btn btn-primary btn-sm">
                    '.lang('add_address').'
                </button>
            </a>
        </div>
    </div>
    ';

}
                                if ($this->Settings->indian_gst) {
                                ?>
                                <script>
                                    var istates = <?= json_encode($istates); ?>
                                </script>
                                <?php
                                } else {
                                    echo '<script>var istates = false; </script>';
                                }
                                ?>
                        </div>
                    </div>
                    
                   

                    <div class="col-sm-3 col-md-2">
                        <?php include('themes/default/shop/views/pages/sidebar2.php'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
                           

<script type="text/javascript">
var addresses = <?= !empty($addresses_v2) ? json_encode($addresses_v2) : 'false'; ?>;

function delete_address(postData){
    $('#myModal').modal('show');    $(".submit_delete").show();
    $('#delete_data').on('click','.submit_delete',function(){
        var result = $.ajax
        (
        {
        url: '<?php echo base_url(); ?>shop/delete_address',
        type: 'GET',
        secureuri:false,
        dataType: 'html',
        data:postData,
        async: false,
        success: function(data)
        {
            if('#'+result_text)
            {
                $('#myModal').modal('hide');
                location.reload();
            }
            else
            {
                alert('Fail');
            }

        },
        error: function (response, status, e)
        {
        alert(e);
        }
        }
        ).responseText;
        var array_data = String(result).split("api-ajax-request-multiple-result-split");
        var result_text = array_data[1];
    });
}





function api_temp_update_address(postData) {  

    $('#api_modal_trigger_address_' + postData['address_id']).click();

}
function api_ajax_update_address(postData){
    var error = 0;
<?php
if ($this->api_shop_setting[0]['google_map_api'] == 1)
    echo "    
        if ($('#api_field_phone_' + postData['field_id']).val() == '' || $('#google_address_' + postData['field_id']).val() == '') {
            $('#api_form_address_' + postData['field_id']).submit(); 
            error = 1;        
        }
        else if ($('#add_ons_google_location_' + postData['field_id']).val() == '') {
            $('#google_location_error_' + postData['field_id']).show();
            $('#api_form_address_' + postData['field_id']).submit(); 
            error = 1;
        }
        else 
            $('#google_location_error_' + postData['field_id']).hide();
    ";
else
    echo "    
        if ($('#api_field_phone_' + postData['field_id']).val() == '') {
            $('#api_form_address_' + postData['field_id']).submit(); 
            error = 1;        
        }
        $('#google_location_error_' + postData['field_id']).hide();
    ";

?>
    if (error == 0) {
        postData['add_ons_first_name'] = $('#add_ons_first_name_' + postData['field_id']).val();
        postData['api_field_phone'] = $('#api_field_phone_' + postData['field_id']).val();
        postData['add_ons_delivery_address'] = $('#google_address_' + postData['field_id']).val();
        postData['add_ons_note_to_driver'] = $('#add_ons_note_to_driver_' + postData['field_id']).val();        
        postData['add_ons_distance_from_restaurant'] = $('#add_ons_distance_from_restaurant_' + postData['field_id']).val();
        postData['add_ons_google_location'] = $('#add_ons_google_location_' + postData['field_id']).val();

        if (postData['api_field_phone'] == '' || postData['add_ons_delivery_address'] == '') {
            $('#api_form_address_' + postData['field_id']).submit();
        }
        else {

            var result = $.ajax
            (
                {
                    url: '<?php echo base_url(); ?>shop/api_ajax_update_address',
                    type: 'GET',
                    secureuri:false,
                    dataType: 'html',
                    data:postData,
                    async: false,
                    error: function (response, status, e)
                    {
                        alert(e);
                    }
                }
            ).responseText;
        // var myWindow = window.open("", "MsgWindow", "width=700, height=400");
        // myWindow.document.write(result);     
            var array_data = String(result).split("api-ajax-request-multiple-result-split");
            var result_text = array_data[1];
            
            if (parseInt(postData['field_id']) > 0) {
                $('#add_ons_first_name_' + postData['field_id']).val(postData['add_ons_first_name']);
                console
                $('#api_field_phone_' + postData['field_id']).val(postData['api_field_phone']);
                $('#google_address_' + postData['field_id']).val(postData['add_ons_delivery_address']);
                $('#add_ons_note_to_driver_' + postData['field_id']).val(postData['add_ons_note_to_driver']);

                $('#api_span_first_name_' + postData['field_id']).html(postData['add_ons_first_name']);                
                $('#api_span_delivery_address_' + postData['field_id']).html(postData['add_ons_delivery_address']);
                $('#api_span_phone_' + postData['field_id']).html(postData['api_field_phone']);     
                $('#api_span_distance_from_our_restaurant_' + postData['field_id']).html(postData['add_ons_distance_from_restaurant']);
                $('#api_span_note_to_driver_' + postData['field_id']).html(postData['add_ons_note_to_driver']);

                var temp = calculate_delivery_fee(postData['add_ons_distance_from_restaurant']);
                temp = formatMoney(temp);
                temp = temp.replace('$$','$');
                $('#api_span_delivery_fee_' + postData['field_id']).html(temp);

                $('#api_modal_close_' + postData['field_id']).click();
            }
            else {                
                location.href = '<?php echo base_url(); ?>shop/addresses';
            }
        }
    }
}

</script>

<?php
$addresses_v2[0] = array();
$addresses_v2[0]['id'] = 0;

//-google-map-for-add-----------------
$temp_google_function_id = 0;
$temp_distance_from_restaurant = '';
$temp_google_location = '';
$temp = explode(',',$this->api_shop_setting[0]['google_location']);
$temp_shop_lat = $temp[0];
$temp_shop_lng = $temp[1];
if ($temp_google_location == '') {
    $temp_initial_content = '<b>SUSHI DONMARU Restaurant</b><div><b>Pin Location: </b>'.$this->api_shop_setting[0]['google_location'].'</div>';
    $temp_initial_lat = $temp_shop_lat;
    $temp_initial_lng = $temp_shop_lng;
}
else {
    $temp_fee = $this->api_helper->calculate_delivery_fee($temp_distance_from_restaurant);
    $temp_initial_content = '<div><b>Delivery Address: </b>'.$addresses_v2[0]['delivery_address'].'<div><b>Your Pin Location: </b>'.$temp_google_location.'<div> <b>Distance from our restaurant: </b>'.$temp_distance_from_restaurant.'</div> <div><b>Delivery Fee: </b>'.$this->sma->formatMoney($temp_fee).'</div>';

    $temp = explode(',',$temp_google_location);
    $temp_initial_lat = $temp[0];
    $temp_initial_lng = $temp[1];    
}
$google_data[0] = array(
    'id' => $temp_google_function_id,
    'initial_content' => $temp_initial_content,
    'initial_lat' => 11.558231692550244,
    'initial_lng' => 104.90370114755967,
    'shop_lat' => $temp_shop_lat,
    'shop_lng' => $temp_shop_lng,    
);
//-google-map-----------------
$i = 0;


echo '
<button type="button" id="api_modal_trigger_address_'.$addresses_v2[$i]['id'].'" class="api_display_none" data-toggle="modal" data-target="#api_modal_'.$addresses_v2[$i]['id'].'">Open Modal</button>

<div id="api_modal_'.$addresses_v2[$i]['id'].'" class="modal fade" role="dialog" >
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content" style="background-color:#333333;">
      <div class="modal-header" style="border-color:#000">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="api_modal_title">
            '.lang('add_new_address').'
        </h4>
      </div>
      <div class="modal-body" id="api_modal_body">
      <form id="api_form_address_'.$addresses_v2[$i]['id'].'" action="" class="validate">
        <div class="col-md-6">
            <div class="form-group">
                '.lang('Address_Name', 'Address_Name').'
                '.form_input('add_ons_first_name', $addresses_v2[$i]['first_name'], 'class="form-control tip" id="add_ons_first_name_'.$addresses_v2[$i]['id'].'"').'
            </div>
        </div>  
        <div class="col-md-6" id="phone_requried">
            <div class="form-group">
                '.lang('phone', 'phone').'
                '.form_input('api_field_phone', $addresses_v2[$i]['phone'], 'class="form-control tip" id="api_field_phone_'.$addresses_v2[$i]['id'].'" required="required"').'
            </div>
        </div>    
        ';
        if ($this->api_shop_setting[0]['google_map_api'] == 1)
            $temp = 'required="required" readonly="readonly"';
        else
            $temp = 'required="required"';
        echo '
        <div class="col-md-6">
            <div class="form-group color_red_change">
                '.lang('Delivery_Address', 'Delivery_Address').'
                '.form_input('add_ons_delivery_address', $addresses_v2[$i]['delivery_address'], 'class="form-control  tip" id="google_address_'.$addresses_v2[$i]['id'].'" '.$temp).'
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group color_red_change">
                '.lang('Note_to_Driver', 'Note_to_Driver').'
                '.form_input('add_ons_note_to_driver', $addresses_v2[$i]['note_to_driver'], 'class="form-control  tip" id="add_ons_note_to_driver_'.$addresses_v2[$i]['id'].'" ').'
            </div>
        </div>      
        <div class="col-md-3 api_display_none">
            <div class="form-group">
                '.lang('postal_code', 'postal_code').'
                '.form_input('api_field_postal_code', set_value('postal_code', $addresses_v2[$i]['postal_code']), 'class="form-control tip" id="api_field_postal_code_'.$addresses_v2[$i]['id'].'" ').'
            </div>
        </div>      
        ';
        if ($this->api_shop_setting[0]['google_map_api'] == 1)
            $temp = '';
        else
            $temp = 'api_display_none';
        echo '
        <div class="col-md-12 '.$temp.'">
            '.lang('google_map_pin', 'google_map_pin').'
            <div class="col-md-6 api_display_none">
                <input type="text" name="add_ons_google_location" id="add_ons_google_location_'.$addresses_v2[$i]['id'].'" value="'.$addresses_v2[$i]['google_location'].'" style="width:100%;">
            </div>
            <div class="col-md-6 api_display_none">
                <input type="text" name="add_ons_distance_from_restaurant" id="add_ons_distance_from_restaurant_'.$addresses_v2[$i]['id'].'" value="'.$addresses_v2[$i]['distance_from_restaurant'].'" style="width:100%;">
            </div>
            <div class="col-md-12 api_padding_0" id="google_location_error_'.$addresses_v2[$i]['id'].'" style="display:none; color:red;">
                '.lang('Please_pin_your_location_on_the_map').'
                <div class="api_height_10"></div>
            </div>

            <div id="map_'.$addresses_v2[$i]['id'].'" style="width:100%; height:400px;">
            </div>
        </div>
        <div class="api_height_15 api_clear_both"></div>
      </div>
      <div class="modal-footer" id="api_modal_footer" style="border:0px;">
        <button type="button" id="api_btn_update_address_'.$addresses_v2[$i]['id'].'" class="api_link_box_none btn btn-info" onclick="
        var postData = {
            \'field_id\' : \''.$addresses_v2[$i]['id'].'\',
        };
        api_ajax_update_address(postData);        
        ">
            '.lang('Add').'
        </button>
        <button type="button" id="api_modal_close_'.$addresses_v2[$i]['id'].'" class="api_link_box_none btn btn-info" data-dismiss="modal" >
            '.lang('Cancel').'
        </button>
        </form>
      </div>
    </div>
  </div>
</div>
';

include 'themes/default/shop/views/sub_page/api_google_map_distance.php';  
        
?>

<style>
    .api_address_detail{
        color:#dc9b01;
        background-color: #333;
    }
    .are_you_sure{
        color: #595959;
        font-size: 30px;
        text-align: center;
        font-weight: 600;
        text-transform: none;
        position: relative;
        margin: 0 0 .4em;
        padding: 0;
        display: block;
    }
    .delivery_adderss{
        font-size: 18px;
        text-align: center;
        font-weight: 300;
        position: relative;
        float: none;
        margin: 0;
        padding: 0;
        line-height: normal;
        color: #545454;
        word-wrap: break-word;
    }
    .btn_cancel_ok{
        border: 0;
        border-radius: 3px;
        box-shadow: none;
        color: #fff;
        cursor: pointer;
        font-size: 17px;
        font-weight: 500;
        margin: 0 5px;
        padding: 10px 32px;
    }   
</style>

