<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section class="page-contents api_padding_0_mobile">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-sm-9 col-md-10 api_padding_right_0_pc">
                        
                        <div class="col-md-12 api_padding_0_mobile">
                            <h4 class="margin-top-md title text-bold">
                                <span><i class="fa fa-list-alt margin-right-sm"></i> <?= lang('orders'); ?></span>            
                                <div id="api_customer_menu_panel_btn" class="api_float_right api_pointer api_screen_show_768" onclick="$('#api_customer_menu_panel').toggle('slideToggle?');" style="top: -10px; margin-top: -15px;">
                                    <button class="btn btn-danger navbar-btn"><div class="fa fa-bars fa-lg"></div></button>
                                </div>
                            </h4>
                        </div>

                        <div id="api_customer_menu_panel" class="col-md-2 api_padding_right_0_pc api_padding_0_mobile api_screen_hide_768">
                            <div class="api_height_10 api_screen_hide_768"></div>
                            <?php include('themes/default/shop/views/pages/customer_menu_panel.php'); ?>                            
                        </div>
                        <div class="col-md-10 api_padding_0_mobile">
                            <div class="api_height_5"></div>

<?php        
if ($orders[0]['id'] > 0) {
    if ($this->api_shop_setting[0]['display_company'] == 1)
        $temp = lang('Company');
    else
        $temp = lang('name');

    echo '
    <div class="table-responsive">
        <table width="100%" class="table table-striped table-hover table-va-middle" border="0">
        <thead>
        <tr>
        <th style="text-align:center !important;">'.lang('Order').'</th>
        <th>'.lang('Date').'</th>
        <th>'.$temp.'</th>
        <th style="text-align:right !important;">'.lang('Total').'</th>
        <th style="text-align:center !important; width:130px;">'.lang('Status').'</th>
        <th style="text-align:center;width:1%; white-space:nowrap;">'.lang('Action').'</th>
        </tr>
        </thead>
    ';
        $k = 1;
        for ($i=0;$i<count($orders);$i++) {
            if (($k%2) != 0) $class_record = "api_record_odd"; else $class_record = "api_record_even";
            $date = date_create($orders[$i]['date']);

            /*
            $temp_branch = '';
            if ($orders[$i]['company_branch'] > 0) {
                $temp = $this->site->api_select_some_fields_with_where("
                    getTranslate(translate,'en','".f_separate."','".v_separate."') as title
                    "
                    ,"sma_company_branch"
                    ,"id = ".$orders[$i]['company_branch']
                    ,"arr"
                );                
                $temp_branch = '<br>'.lang('Branch').': '.$temp[0]['title'];
            }
            */

            echo '
            <tr class="">
                <td style="text-align:center;vertical-align:middle;width:1%; white-space:nowrap;">
                    #'.$orders[$i]['id'].'
                </td>
                <td>
                    '.date_format($date, 'd M Y').'
                </td>
                <td>
                    '.$orders[$i]['customer'].'
                </td>
            ';
            echo '
                <td align="right">
            ';

                echo '
                    <div>
                        '.$this->sma->formatMoney($orders[$i]['grand_total']).'
                        <div class="api_height_5"></div>
                    </div>
                ';

                if ($orders[$i]['payment_status'] == 'completed' || $orders[$i]['payment_status'] == 'paid') {
                    $temp2 = '';

                    $config_data = array(
                        'table_name' => 'sma_payments',
                        'select_table' => 'sma_payments',
                        'translate' => '',
                        'select_condition' => "sale_id = ".$orders[$i]['id']." and (paid_by = 'wing' or paid_by = 'pipay' or paid_by = 'payway') limit 1",
                    );
                    $temp = $this->site->api_select_data_v2($config_data);

                    if ($temp[0]['paid_by'] == 'wing')
                        $temp2 = '
                            <div class="api_padding_top_5">
                                '.lang('Paid_by_Wing').'<br>
                                '.lang('Transaction_ID').': '.$temp[0]['wing_transaction_id'].'
                            </div>';
                    elseif ($temp[0]['paid_by'] == 'pipay')
                        $temp2 = '
                            <div class="api_padding_top_5">
                                '.lang('Paid_by_Pipay').'<br>
                                '.lang('Transaction_ID').': '.$temp[0]['pipay_transaction_id'].'
                            </div>';
                    elseif ($temp[0]['paid_by'] == 'payway')
                        $temp2 = '
                            <div class="api_padding_top_5">
                                '.lang('Paid_by_Payway').'<br>
                                '.lang('Transaction_ID').': '.$temp[0]['payway_transaction_id'].'
                            </div>';

                    echo '<div class="label label-success" >'.lang('paid').'</div>';
                    echo $temp2;
                }
                else
                    echo '<div class="label label-warning" >'.lang('Not_paid_yet').'</div>';

            echo '
                </td>
            ';
            
            echo '<td align="center" style="width:1%; white-space:nowrap;">';

                if ($orders[$i]['sale_status'] == 'completed')
                    echo '<div class="label label-success">'.lang('Completed').'</div>';
                else
                    echo '<div class="label label-warning">'.lang('Pending').'</div>';

            echo '</td>';
                                                                                    
            echo '
                <td align="center" style="width:1%; white-space:nowrap;">
                    <div class="btn-group" role="group">
                        <a class="api_table_view" href="'.base_url().'shop/orders/'.$orders[$i]['id'].'" title="'.lang('View').'">
                            <button type="button" class="btn btn-sm btn-danger" ><i class="fa fa-eye"></i></button>
                        </a>
                    </div>
                </td>
            </tr>
            ';
            $k++;
        }
    echo '
        </table>
    </div>
    ';    
}
else {
    echo '
        <div class="api_height_13"></div>
        <strong>'.lang('no_data_to_display').'</strong>
    ';
}        
?>

<?php
if (!empty($orders)) {
?>
<div class="row" style="margin-top:15px;">
    <div class="col-md-6">
        <span class="page-info line-height-xl hidden-xs hidden-sm">
            <?= str_replace(['_page_', '_total_'], [$page_info['page'], $page_info['total']], lang('page_info')); ?>
        </span>
    </div>
    <div class="col-md-6">
    <div id="pagination" class="pagination-right"><?= $pagination; ?></div>
    </div>
</div>       
<?php
}
?>
                        </div>
                    </div>


                    <div class="col-sm-3 col-md-2 col-xs-12">
                        <?php include(b'themes/default/shop/views/pages/sidebar2.php'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
                            

