<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section class="page-contents api_padding_0_mobile">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-sm-9 col-md-10 api_padding_right_0_pc">
                        
                        <div class="col-md-12 api_padding_0_mobile">
                            <h4 class="margin-top-md title text-bold">
                                <span><i class="fa fa-user margin-right-sm"></i> <?= lang('my_addresses'); ?></span>            
                                <div id="api_customer_menu_panel_btn" class="api_float_right api_pointer api_screen_show_768" onclick="$('#api_customer_menu_panel').toggle('slideToggle?');" style="top: -10px; margin-top: -15px;">
                                    <button class="btn btn-danger navbar-btn"><div class="fa fa-bars fa-lg"></div></button>
                                </div>
                            </h4>
                        </div>

                        <div id="api_customer_menu_panel" class="col-md-2 api_padding_right_0_pc api_padding_0_mobile api_screen_hide_768">
                            <div class="api_height_10 api_screen_hide_768"></div>
                            <?php include('themes/default/shop/views/pages/customer_menu_panel.php'); ?>                            
                        </div>
                        <div class="col-md-10 api_padding_0_mobile">
                            <div class="api_height_13"></div>     
                           
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#user" aria-controls="user" role="tab" data-toggle="tab" ><?= lang('update_address'); ?></a></li>
    </ul>
    <div class="tab-content padding-lg white bordered-light" style="margin-top:-1px;">
        <div role="tabpanel" class="tab-pane fade in active" id="user">
            <p><?= lang('fill_form'); ?></p>
            <?php 
                echo form_open("shop/edit_addresses/".$address_id."", 'class="validate"'); 
            ?>
            <?php 
                $config_data = array(
                    'table_name' => 'sma_addresses',
                    'select_table' => 'sma_addresses',
                    'translate' => '',
                    'select_condition' => "id = ".$address_id,
                );
                $select_data = $this->site->api_select_data_v2($config_data);
            ?>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang('first_name', 'first_name'); ?>
                        <?= form_input('add_ons_first_name', $select_data[0]['first_name'], 'class="form-control tip" id="first_name"'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang('last_name', 'last_name'); ?>
                        <?= form_input('add_ons_last_name', $select_data[0]['last_name'], 'class="form-control tip" id="last_name"'); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6" id="phone_requried">
                    <div class="form-group">
                        <?= lang('phone', 'phone'); ?>
                        <?= form_input('phone', $select_data[0]['phone'], 'class="form-control tip" id="phone" required="required"'); ?>
                    </div>
                </div>
                <div class="col-md-6 api_display_none">
                    <div class="form-group">
                        <?= lang('email', 'email'); ?>
                        <?= form_input('add_ons_email', $select_data[0]['email'], 'class="form-control tip"  id="email"'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang('postal_code', 'postal_code'); ?>
                        <?= form_input('postal_code', $select_data[0]['postal_code'], 'class="form-control tip" id="postal_code" '); ?>
                    </div>
                </div> 
            </div>
          
            <div class="row">
                <div class="col-md-12 color_red_change" id="city_requried">
                    <div class="form-group">
                        <?= lang("city", "city"); ?>
                        <?php
                            $config_data = array(
                                'none_label' => lang("Select_a_city"),
                                'table_name' => 'sma_city',
                                'space' => ' &rarr; ',
                                'strip_id' => '',        
                                'field_name' => 'title_'.$this->api_shop_setting[0]['api_lang_key'],
                                'condition' => 'order by title_'.$this->api_shop_setting[0]['api_lang_key'].' asc',
                                'condition_parent' => ' and parent_id = 268',
                                'translate' => 'yes',
                                'no_space' => 1,
                            );                        
                            $this->site->api_get_option_category($config_data);
                            $temp_option = $_SESSION['api_temp'];
                            for ($i=0;$i<count($temp_option);$i++) {                        
                                $temp = explode(':{api}:',$temp_option[$i]);
                                $temp_10 = '';
                                if ($temp[0] != '') {
                                    $config_data_2 = array(
                                        'id' => $temp[0],
                                        'table_name' => 'sma_city',
                                        'field_name' => 'title_'.$this->api_shop_setting[0]['api_lang_key'],
                                        'parent_id' => '268',
                                        'translate' => 'yes',
                                    );
                                    $_SESSION['api_temp'] = array();
                                    $this->site->api_get_category_arrow($config_data_2);          
                                    for ($i2 = count($_SESSION['api_temp']) - 1; ; $i2--) {
                                        if ($i2 == 0) {
                                            break;
                                        }
                                        $temp_arrow = '';
                                        if ($i2 > 1)
                                            $temp_arrow = ' &rarr; ';
                                        $temp_10 .= $_SESSION['api_temp'][$i2].$temp_arrow;
                                    }   
                                }
                                $tr_city[$temp[0]] = $temp_10.$temp[1];
                            }
                            echo form_dropdown('city_id', $tr_city, $select_data[0]['city_id'], 'class="form-control" required="required" id="city_id"');
                        ?>
                    </div>
                </div>          
            </div>
            <div class="row">
                <div class="col-md-12" id="addrerss_requrried">
                    <div class="form-group color_red_change">
                        <?= lang('Delivery_Address', 'address'); ?>
                        <?= form_textarea('add_ons_delivery_address', $select_data[0]['delivery_address'], 'class="form-control  tip" id="address" required="required" style="height:100px;"'); ?>
                    </div>
                </div>
                       
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang('landmark', 'landmark'); ?>
                        <?= form_input('add_ons_landmark', $select_data[0]['landmark'], 'class="form-control tip" id="landmark"'); ?>
                    </div>
                </div>
                <div class="col-md-6" id="destrination_requried">
                    <div class="form-group">
                        <?= lang('google_map_ping_location', 'google_map_ping_location'); ?>
                        <input id="add_ons_google_location" class="form-control" value="<?php echo $select_data[0]['map_coordinate']; ?>" disabled />
                        <input type="hidden" name="add_ons_map_coordinate" value="<?php echo $select_data[0]['map_coordinate']; ?>" id="add_ons_google_location_hidden">
                        <p class="" id="destrination_error" style="color:#a94442"></p>
                    </div>
                </div>   
                <?php 
                    if($_GET['update_address'] == 1) {
                        $pin = explode(',',$select_data[0]['map_coordinate']);
                        
                        echo '
                            <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
                            <style>
                            #map {
                                height: 300px;
                                width: 100%;
                            }
                            </style>
                            <script>
                            function initMap() {
                                const bounds = new google.maps.LatLngBounds();
                                const markersArray = [];
                                const origin1 = { lat: 11.540197914706646, lng: 104.91241091565547 };
                                const destinationA = { lat: '.$pin[0].', lng: '.$pin[1].' };
                                
                                const destinationIcon =
                                "https://chart.googleapis.com/chart?" +
                                "chst=d_map_pin_letter&chld=D|FF0000|000000";
                                const originIcon =
                                "https://chart.googleapis.com/chart?" +
                                "chst=d_map_pin_letter&chld=O|FFFF00|000000";
                                const map = new google.maps.Map(document.getElementById("map"), {
                                center: { lat: 11.540509596258902, lng: 104.91402933505334 },
                                zoom: 14,
                                });
                                const geocoder = new google.maps.Geocoder();
                                const service = new google.maps.DistanceMatrixService();
                                service.getDistanceMatrix(
                                {
                                    origins: [origin1],
                                    destinations: [destinationA],
                                    travelMode: google.maps.TravelMode.DRIVING,
                                    unitSystem: google.maps.UnitSystem.METRIC,
                                    avoidHighways: false,
                                    avoidTolls: false,
                                },
                                (response, status) => {
                                    if (status !== "OK") {
                                    alert("Error was: " + status);
                                    } else {
                                    const originList = response.originAddresses;
                                    const destinationList = response.destinationAddresses;

                                    deleteMarkers(markersArray);

                                    const showGeocodedAddressOnMap = function (asDestination) {
                                        const icon = asDestination ? destinationIcon : originIcon;

                                        return function (results, status) {
                                        if (status === "OK") {
                                            map.fitBounds(bounds.extend(results[0].geometry.location));
                                            markersArray.push(
                                            new google.maps.Marker({
                                                map,
                                                position: results[0].geometry.location,
                                                icon: icon,
                                            })
                                            );
                                        } else {
                                            alert("Geocode was not successful due to: " + status);
                                        }
                                        };
                                    };

                                    for (let i = 0; i < originList.length; i++) {
                                        const results = response.rows[i].elements;
                                        geocoder.geocode(
                                        { address: originList[i] },
                                        showGeocodedAddressOnMap(false)
                                        );

                                        for (let j = 0; j < results.length; j++) {
                                        geocoder.geocode(
                                            { address: destinationList[j] },
                                            showGeocodedAddressOnMap(true)
                                        );
                                            // console.log(results);
                                            var distance = results[0].distance.text.split("km");
                                            var sum = 0;
                                            var discount = 0;
                                            if (distance[0] <= 1 ) {
                                                sum = 1;
                                            } else  if (distance[0] > 1 && distance[0] <= 1.5) {
                                                sum = 1.5;
                                            } else  if (distance[0] > 1.5 && distance[0] <= 2) {
                                                sum = 2;
                                            } else if (distance[0] > 2) {
                                                sum = 2 + parseFloat((distance[0] - 2) / 2);
                                            }
                                            var postData = {
                                                \'field_id\' : '.$select_data[0]['id'].',
                                                \'distance_from_restaurant\' : results[0].distance.text,
                                                \'delivery_fee\' : sum,
                                            }; ajax_get_update_address(postData);
                                            document.getElementById(\'distance_from_our_restaurant\').innerHTML = results[0].distance.text;
                                            document.getElementById(\'distance_from_restaurant_hidden\').value = results[0].distance.text;
                                            document.getElementById(\'address_delivery_fee\').innerHTML = "$" + parseFloat(sum).toFixed(2);
                                            document.getElementById(\'delivery_fee_hidden\').value = sum;
                                            document.getElementById(\'discount\').value = discount;
                                        }
                                    }
                                    }
                                }
                                );
                            }

                            function deleteMarkers(markersArray) {
                                for (let i = 0; i < markersArray.length; i++) {
                                markersArray[i].setMap(null);
                                }
                                markersArray = [];
                            }
                            </script>
                            <div id="map"></div>

                            <script
                            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBNuX93TO6QiReEGliaysdDUtiEULNlWGE&callback=initMap&libraries=&v=weekly"
                            async
                            ></script>
                        ';
                    } else {
                        $config_data = array(
                            'map_css' => 'width:100%; height:400px;',
                            'origin_coordinate' => $this->api_shop_setting[0]['map_coordinate'],
                            'destination_coordinate' => $customer_v2[0]['map_coordinate'],
                        );
                        $temp_display2 = '';
                        include 'themes/default/shop/views/sub_page/api_google_map.php';
                        echo '
                            <div class="col-sm-12"> 
                                '.$temp_display2.'
                            </div>
                        ';
                    }
                ?>

                <?php
                echo '
                    <div class="form-group col-sm-6">
                        <div class="form-group">
                            <div class="api_padding_top_15">
                                '.lang('distance_from_our_restaurant').' : 
                                <span class="text-bold" id="distance_from_our_restaurant">
                                    '.$select_data[0]['distance_from_restaurant'].'
                                </span>
                                <input type="hidden" name="add_ons_distance_from_restaurant" value="'.$select_data[0]['distance_from_restaurant'].'" id="distance_from_restaurant_hidden">
                            </div>
                        </div>
                    </div>
                ';                
                echo '
                    <div class="form-group col-sm-6">
                        <div class="form-group">
                            <div class="api_padding_top_15">
                                '.lang('delivery_fee').' : 
                                <span class="text-bold" id="address_delivery_fee">  
                                    '.$this->sma->formatMoney($select_data[0]['delivery_fee']).'
                                </span>
                                <input type="hidden" name="add_ons_delivery_fee" value="'.$select_data[0]['delivery_fee'].'" id="delivery_fee_hidden">
                            </div>
                        </div>
                    </div>
                ';
                echo '
                    <div class="form-group col-sm-3 api_display_none">
                        <div class="form-group">
                            <div class="api_padding_top_15">
                                '.lang('discount').' : 
                                <span class="text-bold" id="address_discount">
                                    '.$select_data[0]['address_discount'].'
                                </span>
                                <input type="hidden" name="add_ons_address_discount" value="'.$select_data[0]['address_discount'].'" id="discount">
                            </div>
                        </div>
                    </div>
                ';
                ?>
                    
            </div>

            <?= form_submit('billing', lang('update'), 'class="btn btn-primary api_display_none" id="add_address"'); ?>
            
            <?php 
                echo form_button('billing', lang('update'), 'class="btn btn-primary" onclick="submit_address();"'); 
            ?>
            
            <?= form_button('billing', lang('Cancel'), 'class="btn btn-danger" onclick="cancel_update();"'); ?>
            <?php echo form_close(); ?>        
        </div>
        
    </div>


                        </div>
                    </div>

                    <div class="col-sm-3 col-md-2">
                        <?php include(b'themes/default/shop/views/pages/sidebar2.php'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<script>
    function submit_address() {
        $('#add_address').click();
    }

    function cancel_update() {
        window.location.href = "<?= base_url();?>shop/addresses";
    }

    function ajax_get_update_address(postData) {
        var result = $.ajax({
            url: '<?= base_url();?>shop/ajax_get_update_address',
            type: 'GET',
            secureuri:false,
            dataType: 'html',
            data:postData,
            async: false,
            error: function (response, status, e)
            {
            alert(e);
            }
        }).responseText;   
        var array_data = String(result).split("api-ajax-request-multiple-result-split");
        var result_text = array_data[1];
        window.location.href = "<?= base_url();?>shop/addresses";
    }
    
</script>