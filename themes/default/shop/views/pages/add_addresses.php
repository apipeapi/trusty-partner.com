<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section class="page-contents api_padding_0_mobile">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-sm-9 col-md-10 api_padding_right_0_pc">
                        
                        <div class="col-md-12 api_padding_0_mobile">
                            <h4 class="margin-top-md title text-bold">
                                <span><i class="fa fa-user margin-right-sm"></i> <?= lang('my_addresses'); ?></span>            
                                <div id="api_customer_menu_panel_btn" class="api_float_right api_pointer api_screen_show_768" onclick="$('#api_customer_menu_panel').toggle('slideToggle?');" style="top: -10px; margin-top: -15px;">
                                    <button class="btn btn-danger navbar-btn"><div class="fa fa-bars fa-lg"></div></button>
                                </div>
                            </h4>
                        </div>

                        <div id="api_customer_menu_panel" class="col-md-2 api_padding_right_0_pc api_padding_0_mobile api_screen_hide_768">
                            <div class="api_height_10 api_screen_hide_768"></div>
                            <?php include('themes/default/shop/views/pages/customer_menu_panel.php'); ?>                            
                        </div>
                        <div class="col-md-10 api_padding_0_mobile">
                            <div class="api_height_13"></div>
                            
                        
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#user" aria-controls="user" role="tab" data-toggle="tab" ><?= lang('add_address'); ?></a></li>
    </ul>
    <div class="tab-content padding-lg white bordered-light" style="margin-top:-1px;">
        <div role="tabpanel" class="tab-pane fade in active" id="user">
            <p><?= lang('fill_form'); ?></p>

            <?php 
                echo form_open("shop/add_addresses", 'class="validate"'); 
            ?>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang('first_name', 'first_name'); ?>
                        <?= form_input('add_ons_first_name', '', 'class="form-control tip" id="first_name"'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang('last_name', 'last_name'); ?>
                        <?= form_input('add_ons_last_name', '', 'class="form-control tip" id="last_name"'); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang('phone', 'phone'); ?>
                        <?= form_input('phone', '', 'class="form-control tip" id="phone" required="required"'); ?>
                    </div>
                </div>
                <div class="col-md-6 api_display_none">
                    <div class="form-group">
                        <?= lang('email', 'email'); ?>
                        <?= form_input('add_ons_email', '', 'class="form-control tip"  id="email"'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang('postal_code', 'postal_code'); ?>
                        <?= form_input('postal_code', '', 'class="form-control tip" id="postal_code" '); ?>
                    </div>
                </div>   
            </div>
          
            <div class="row">
                <div class="col-md-12 color_red_change">
                    <div class="form-group">
                        <?= lang("city", "city"); ?>
                        <?php
                            $config_data = array(
                                'none_label' => lang("Select_a_city"),
                                'table_name' => 'sma_city',
                                'space' => ' &rarr; ',
                                'strip_id' => '',        
                                'field_name' => 'title_'.$this->api_shop_setting[0]['api_lang_key'],
                                'condition' => 'order by title_'.$this->api_shop_setting[0]['api_lang_key'].' asc',
                                'condition_parent' => ' and parent_id = 268',
                                'translate' => 'yes',
                                'no_space' => 1,
                            );                        
                            $this->site->api_get_option_category($config_data);
                            $temp_option = $_SESSION['api_temp'];
                            for ($i=0;$i<count($temp_option);$i++) {                        
                                $temp = explode(':{api}:',$temp_option[$i]);
                                $temp_10 = '';
                                if ($temp[0] != '') {
                                    $config_data_2 = array(
                                        'id' => $temp[0],
                                        'table_name' => 'sma_city',
                                        'field_name' => 'title_'.$this->api_shop_setting[0]['api_lang_key'],
                                        'parent_id' => '268',
                                        'translate' => 'yes',
                                    );
                                    $_SESSION['api_temp'] = array();
                                    $this->site->api_get_category_arrow($config_data_2);          
                                    for ($i2 = count($_SESSION['api_temp']) - 1; ; $i2--) {
                                        if ($i2 == 0) {
                                            break;
                                        }
                                        $temp_arrow = '';
                                        if ($i2 > 1)
                                            $temp_arrow = ' &rarr; ';
                                        $temp_10 .= $_SESSION['api_temp'][$i2].$temp_arrow;
                                    }   
                                }
                                $tr_city[$temp[0]] = $temp_10.$temp[1];
                            }
                            echo form_dropdown('city_id', $tr_city, '', 'class="form-control" required="required" id="city_id"');
                        ?>
                    </div>
                </div>          
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group color_red_change">
                        <?= lang('Delivery_Address', 'address'); ?>
                        <?= form_textarea('add_ons_delivery_address', '', 'class="form-control  tip" id="address" required="required" style="height:100px;"'); ?>
                    </div>
                </div>
                    
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang('landmark', 'landmark'); ?>
                        <?= form_input('add_ons_landmark', '', 'class="form-control tip" id="landmark"'); ?>
                    </div>
                </div>
                <div class="col-md-6" id="destrination_requried">
                    <div class="form-group">
                        <?= lang('google_map_ping_location', 'google_map_ping_location'); ?>
                        <input id="add_ons_google_location" class="form-control" value="" disabled />
                        <input type="hidden" name="add_ons_map_coordinate" id="add_ons_google_location_hidden">
                        <p class="" id="destrination_error" style="color:#a94442"></p>
                    </div>
                </div>   
                <?php 
                echo $_GET['address'];
                    $config_data = array(
                        'map_css' => 'width:100%; height:400px;',
                        'origin_coordinate' => $this->api_shop_setting[0]['map_coordinate'],
                        'destination_coordinate' => $customer_v2[0]['map_coordinate'],
                    );
                    $temp_display = '';
                    include 'themes/default/shop/views/sub_page/api_google_map.php';
                    echo '
                        <div class="col-sm-12"> 
                            '.$temp_display.'
                        </div>
                    ';
                ?>

                <?php
                echo '
                    <div class="form-group col-sm-6">
                        <div class="form-group">
                            <div class="api_padding_top_15">
                                '.lang('distance_from_our_restaurant').' : 
                                <span class="text-bold" id="distance_from_our_restaurant">
                                </span>
                                <input type="hidden" name="add_ons_distance_from_restaurant" value="" id="distance_from_restaurant">
                            </div>
                        </div>
                    </div>
                ';                
                echo '
                    <div class="form-group col-sm-6">
                        <div class="form-group">
                            <div class="api_padding_top_15">
                                '.lang('delivery_fee').' : 
                                <span class="text-bold" id="address_delivery_fee">
                                </span>
                                <input type="hidden" name="add_ons_delivery_fee" value="" id="delivery_fee">
                            </div>
                        </div>
                    </div>
                ';
                echo '
                    <div class="form-group col-sm-3 api_display_none">
                        <div class="form-group">
                            <div class="api_padding_top_15">
                                '.lang('discount').' : 
                                <span id="address_discount">
                                </span>
                                <input type="hidden" name="add_ons_address_discount" value="" id="discount">
                            </div>
                        </div>
                    </div>
                ';
                ?>
                    
            </div>

            <?= form_submit('billing', lang('Add'), 'class="btn btn-primary api_display_none" id="add_address"'); ?>
            <?php  
                if($_GET['add-address'] == 1) { 
                    echo form_button('billing', lang('Add'), 'class="btn btn-primary" onclick="submit_address_v2();"');
                    echo form_button('billing', lang('Cancel'), 'class="btn btn-danger" onclick="cancel_add();"');
                } else {
                    echo form_button('billing', lang('Add'), 'class="btn btn-primary" onclick="submit_address();"');
                    echo form_button('billing', lang('Cancel'), 'class="btn btn-danger" onclick="cancel_update();"');
                }
            ?>
            
            <?php //echo form_button('billing', lang('Add'), 'class="btn btn-primary" onclick="submit_address();"'); ?>
            <?php //echo form_button('billing', lang('Cancel'), 'class="btn btn-danger" onclick="cancel_update();"'); ?>
            <?php echo form_close(); ?>        
        </div>
        
    </div>


                        </div>
                    </div>

                    <div class="col-sm-3 col-md-2">
                        <?php include(b'themes/default/shop/views/pages/sidebar2.php'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<script>
    if($('#distance_from_our_restaurant').val() == '')
        $('#distance_from_our_restaurant').text('?');
    if($('#address_delivery_fee').val() == '')
        $('#address_delivery_fee').text('$0.00');
    function submit_address(postData) {
        //window.location.href = "<?= base_url();?>shop/Add-Addresses?address=1";
        var error = 0;
        if($('#add_ons_google_location').val() == '') {
            $('#destrination_requried').scrollView();
            error = 1;
         
        }
        if(error == 0) {
            $('#add_address').click();
            $('#destrination_error').addClass('api_display_none');
        } else {
            $('#destrination_error').text('<?= lang('please_select_your_location');?>');
        }
        
    }
    function ajax_get_address(postData) {
        var result = $.ajax({
            url: '<?= base_url();?>shop/ajax_get_address',
            type: 'GET',
            secureuri:false,
            dataType: 'html',
            data:postData,
            async: false,
            error: function (response, status, e)
            {
            alert(e);
            }
        }).responseText;   
        // var myWindow = window.open("", "MsgWindow", "width=700, height=400");
        // myWindow.document.write(result); 
        var array_data = String(result).split("api-ajax-request-multiple-result-split");
        var result_text = array_data[1];
        
    }
    
    function cancel_update() {
        window.location.href = "<?= base_url();?>shop/addresses";
    }

    
</script>

