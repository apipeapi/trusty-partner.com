<style>
    .header_register a {
        color: #fff !important;
    }
   
</style>
<?php
$temp_display = '';

if ($this->session->userdata('api_mobile') != 1) {
    $temp_display .= '
    <!-- Main Header -->
    <section class="main-header red" id="main-header">
        <div id="api_search_header" class="container padding-y-md" >
            <table width="100%" border="0" >
            <tr>
            <td valign="bottom">           
';

    //-Logo-========================================================
    

    $temp_display .= '            
                <div class="api_float_left api_padding_right_40">
                    <a href="'.site_url().'">
                        <img alt="'.$shop_settings->shop_name.'" src="'.base_url('assets/uploads/logos/'.$shop_settings->logo).'" class="api_header_logo" height="60"/>
                    </a>
                </div>
            ';
            
    //-Logo-========================================================

    //-Search-========================================================
    $temp_display .= ' 
                '.shop_form_open('products', 'id="product-search-form" name="product-search-form" method="get"').'
                <div class="api_height_3"></div>
                <div class="api_float_left api_header_search_wrapper ">                  
            ';
    if (!$shop_settings->hide_price) {
        $temp = 'col-sm-8 col-md-6 col-md-offset-3';
    } else {
        $temp = 'col-md-6 col-md-offset-6';
    }
    $temp = '';
    $temp_display .= '
                  
                        <div class="search-box">
                            <div class="input-group">
                                <input name="search" type="text" class="form-control api_header_search" id="product-search" value="'.$_GET['search'].'" aria-label="Search..." onkeydown="if (event.keyCode == 13) document.getElementById(\'product-search-form\').submit()" placeholder="'.lang('Search').'" >
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default btn-search api_link_box_none" onclick="window.location = \''.base_url().'shop/products?search='.'\' + $(\'#product-search\').val();">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                      
                </div>
                '.form_close().'
            ';
    //-Search-========================================================

    //-Cart-Button-========================================================
    if (!$shop_settings->hide_price) {
        $temp_display .= '
                <div class="api_float_left api_header_cart_wrapper api_display_none">
                    <div class="col-md-12 cart-btn hidden-xs">
                        <button type="button" class="btn btn-theme btn-block dropdown-toggle shopping-cart api_link_box_none" id="dropdown-cart" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <i class="fa fa-shopping-cart margin-right-md"></i>
                            <span class="cart-total-items"></span>
                            <!-- <i class="fa fa-caret-down margin-left-md"></i> -->
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-cart">
                            <div id="cart-contents" style="display: block;">
                                <table class="table table-condensed table-striped table-cart" id="cart-items"></table>
                                <div id="cart-links" class="text-center margin-bottom-md">
                                    <div class="btn-group btn-group-justified" role="group" aria-label="View Cart and Checkout Button">
                                        <div class="btn-group">
                                            <a class="btn btn-default btn-sm" href="'.site_url('cart').'"><i class="fa fa-shopping-cart"></i> '.lang('view_cart').'</a>
                                        </div>
                                        <div class="btn-group api_display_none">
                                            <a class="btn btn-default btn-sm" href="'.site_url('cart/checkout').'"><i class="fa fa-check"></i> '.lang('checkout').'</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="cart-empty">'.lang('please_add_item_to_cart').'</div>
                        </div>
                    </div>
                </div>
                ';
    }
    //-Cart-Button-========================================================

    //-Top-========================================================
    if ($loggedIn) {
        
        $temp_display .= '

                <div class="api_float_right api_header_login_wrapper dropdown">
                    <a href="#" class="dropdown-toggle api_header_normal" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-user fa-lg"></i> <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right">
                ';
        if ($this->session->userdata('group_id') == 1 || $this->session->userdata('group_id') == 2) {
            $temp_display .= '
                            <li class=""><a href="'.site_url('admin').'"><i class="mi fa fa-dashboard"></i> '.lang('dashboard').'</a></li>        
                        ';
        }
       
        $temp_display .= '                                 
                        <li class=""><a href="'.site_url('profile').'"><i class="mi fa fa-user"></i> '.lang('profile').'</a></li>
                        <li class=""><a href="'.shop_url('orders').'"><i class="mi fa fa-heart"></i> '.lang('orders').'</a></li>
                    ';
        if ($this->session->userdata('sale_consignment_auto_insert') == 'yes') {
            $temp_display .= '
                            <li class="">
                                <a href="'.shop_url('consignment').'">
                                    <i class="mi fa fa-truck"></i> '.lang('Consignment').'
                                </a>
                            </li>        
                        ';
        }
        
        $temp_display .= '
                        <li class=""><a href="'.shop_url('wishlist').'"><i class="mi fa fa-heart-o"></i> '.lang('wishlist').'</a></li>
                        <li class="divider"></li>
                        <li class=""><a href="'.site_url('logout').'" id="logout"><i class="mi fa fa-sign-out"></i> '.lang('logout').'</a></li>
                    </ul>
                </div>                
                ';
               
        $temp_display .= '
                <div class="api_float_right api_header_favorite_wrapper api_padding_right_15">
                    <a class="api_header_normal" href="'.shop_url('wishlist').'" title="'.lang('wishlist').'" ><i class="fa fa-heart fa-lg"></i>  <span class="hidden-xs"></span></a>
                </div>
                ';
    } else {
        
        $temp_display .= '
                <div class="api_float_right api_header_login_wrapper">
                    <div class="dropdown">
                        <button class="btn dropdown-toggle" type="button" id="dropdownLogin" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="background-color: transparent; border-radius: 0; color: #fff; text-shadow: 0 -1px 0 rgba(0,0,0,.2); margin-top:-10px">
                            <i class="fa fa-sign-in"></i> '.lang('login').' <span class="caret"></span>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-login" aria-labelledby="dropdownLogin" data-dropdown-in="zoomIn" data-dropdown-out="fadeOut" style="line-height: 1.4; min-width: 230px; padding: 10px">
                ';
        include 'themes/default/shop/views/user/login_form.php';
        $temp_display .= $temp_display_login_form;

        $temp_display .= '
                        </div>
                    </div>
                </div>
                ';
                
    }
    
    $temp_display .= '
                <div class="api_float_right api_header_cart_image_wrapper api_padding_right_15">
                    <a href="'.base_url().'cart" style="color: #fff; text-shadow: 0 -1px 0 rgba(0,0,0,.2);">
                        <div class="api_cart_count"></div>
                        <div id="api_cart_count_text" class="api_cart_count_text"></div>
                    </a>
                </div>
            ';
            

    if ($loggedIn && $Staff) {
        $temp_display .= '
                    <div class="api_float_right api_header_admin_wrapper api_padding_right_15">
                        <a class="api_header_normal" href="'.admin_url().'" title="'.lang('admin_area').'" ><i class="fa fa-dashboard fa-lg"></i> </a>
                    </div>
                ';
    }
    
    $temp_display .= '
                <div class="api_float_right api_padding_right_13 api_padding_top_12">  
                    <ul class="list-inline nav pull-right">
                        <li class="dropdown" style="margin-top:-1px">     
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <img src="'.base_url('assets/images/' . $Settings->user_language . '.png').'" alt="">
                                <span class="api_header_normal">&nbsp;&nbsp;'.ucwords($Settings->user_language).'</span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right"> ';                         
                                $language_array = unserialize(multi_language);
                                    for ($i=0;$i<count($language_array);$i++) {  
                                        if (strtolower($language_array[$i][1]) == 'japanese') {
                                            $temp = 'api_border';
                                            $temp2 = 'style="margin-left:0px; width:17px;"';
                                        }
                                        else {
                                            $temp = '';
                                            $temp2 = '';
                                        } 
                                    $temp_display .= '
                                        <li>
                                                <a href="'.site_url('main/language/'.strtolower($language_array[$i][1])).'">
                                                    <img class="'.$temp.'" src="'.base_url('assets/images/'.strtolower($language_array[$i][1]).'.png').'" class="language-img" '.$temp2.'>
                                                    &nbsp;&nbsp;'.ucwords($language_array[$i][1]).'
                                                </a>
                                        </li>    ';
                                }  $temp_display .= '  
                            </ul>
                           
                            </ul>
                        </li>
                    </ul>
                </div>
            ';
            
  
    //-Top-========================================================

    $temp_display .= '
                <div class="api_clear_both"></div>
            </td>
            </tr>
            </table>
        </div>
    </section>
    <!-- End Main Header -->
';

    $temp_display .= '  
    <!-- Nav Bar -->
    <nav class="navbar navbar-default" role="navigation">
        <div class="container navigation">
            <div id="api_cart_item_bar" class="navbar-header api_display_none">
                <button type="button" class="navbar-toggle collapsed hidden-sm hidden-xs" data-toggle="collapse" data-target="#navbar-ex1-collapse">
                    '.lang('navigation').'
                </button>
                <a href="'.site_url('cart').'" class="btn btn-default btn-cart-xs visible-xs pull-right shopping-cart api_margin_bottom_8">
                    <i class="fa fa-shopping-cart"></i> <span class="cart-total-items"></span>
                </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-ex1-collapse">
                <ul class="nav navbar-nav">
';
    if ($m == 'main' && $v == 'index') {
        $temp = 'active';
    } else {
        $temp = '';
    }
    if ($m == 'shop' && $v == 'products' && $this->input->get('promo') != 'yes') {
        $temp2 = 'active';
    } else {
        $temp2 = '';
    }

    $airphsarjapan = !phsarjapan()?"and getTranslate(add_ons,'air','".f_separate."','".v_separate."') = 'yes'":false;

    $temp_display .= '
                    <li class="'.$temp.'"><a href="'.base_url().'">'.lang('home').'</a></li>
                    <li class="'.$temp2.'"><a href="'.shop_url('products').'">'.lang('products').'</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            '.lang('categories').' <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                    ';

    $condition = '';
    if ($this->api_shop_setting[0]['air_base_url'] == base_url())
        $condition .= " and add_ons like '%:air_display:{yes}:%' ";
    $config_data = array(
        'table_name' => 'sma_categories',
        'select_table' => 'sma_categories',
        'translate' => 'yes',
        'select_condition' => "parent_id = 0 ".$condition." and add_ons LIKE '%:display:{yes}:%' order by name asc",
    );
    $temp = $this->site->api_select_data_v2($config_data);
    for ($i=0;$i<count($temp);$i++) {
        $config_data = array(
            'table_name' => 'sma_categories',
            'select_table' => 'sma_categories',
            'translate' => 'yes',
            'select_condition' => "parent_id = ".$temp[$i]['id']." ".$condition." and add_ons LIKE '%:display:{yes}:%' order by name asc",
        );
        $temp3 = $this->site->api_select_data_v2($config_data);
        if (is_array($temp3))
        if (count($temp3) > 0) {
            $temp_display .= '<li class="dropdown dropdown-submenu">';
                $temp_display .= '<a class="dropdown-toggle" data-toggle="dropdown" href="'.site_url('category/'.$temp[$i]['slug']).'">'.$temp[$i]['title_'.$this->api_shop_setting[0]['api_lang_key']].'</a>';
                    $temp_display .= '<ul class="dropdown-menu">';
                    for ($j=0;$j<count($temp3);$j++) {
                        $temp_display .= '<li><a href="'.site_url('category/'.$temp[$i]['slug'].'/'.$temp3[$j]['slug']).'">'.$temp3[$j]['title_'.$this->api_shop_setting[0]['api_lang_key']].'</a></li>';
                    }
                    $temp_display .= '<li class="divider"></li>';
                    $temp_display .= '<li><a href="'.site_url('category/'.$temp[$i]['slug']).'">'.lang('all_products').'</a></li>';
                    $temp_display .= '</ul>';
        }else {
            $temp_display .= '<li class="">';
            $temp_display .= '<a href="'.site_url('category/'.$temp[$i]['slug']).'">'.$temp[$i]['title_'.$this->api_shop_setting[0]['api_lang_key']].'</a>';
        }
            $temp_display .= '</li>';


            
    }



    $temp_display .= '
                        </ul>
                    </li>
                    ';
    if ($m == 'cart_ajax' && $v == 'index') {
        $temp = 'active';
    } else {
        $temp = '';
    }

    $temp_display .= '
                    <li class="'.$temp.'">
                        <a href="'.site_url('cart').'">
                            '.lang('shopping_cart').'
                        </a>
                    </li>
                    ';

    if (!$shop_settings->hide_price) {
        if ($m == 'cart_ajax' && $v == 'checkout') {
            $temp = 'active';
        } else {
            $temp = '';
        }
        $temp_display .= '
                            <li class="'.$temp.'">
                                <a href="'.site_url('cart/checkout').'">'.lang('checkout').'</a>
                            </li>
                        ';
    }
    $temp_display .= '
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Nav Bar -->
    ';


    echo '
    <header class="api_header" id="api_header">
        '.$temp_display.'
    </header>
';

    echo '
<style>

.api_header .btn-search, .api_header .shopping-cart{
    background-color:#a70303 !important;
}
.api_header .btn-search:hover, .api_header .shopping-cart:hover{
    background-color:#3e3636 !important;
}
.api_header_normal{
    color: #fff !important; 
    font-size:15px; 
    text-shadow: 0 -1px 0 rgba(0,0,0,.2);
}


.field-icon {
    float: right;
    margin-left: -25px;
    margin-right: 5px;
    margin-top: -25px;
    position: relative;
    z-index: 2;
    cursor: point;
}


.api_search_header{
    padding: 5px 25px 5px 25px !important;
}

@media screen and (min-width: 100px) and (max-width:1023px) {
.api_search_header{
    padding: 5px 0px 5px 0px !important;
}
}
@media screen and (min-width: 1024px){
.api_search_header{
    padding: 5px 25px 5px 25px !important;
}
}

@media screen and (min-width: 768px) and (max-width:1023px) {
.api_header_logo{
    
}
.api_header_search {
    width:150px !important;
}
.api_header_search_wrapper{
    padding-top:5px;
}
.api_header_admin_wrapper{
    padding-top:10px;
}
.api_header_cart_wrapper{
    padding-top:5px;
}
.api_header_login_wrapper{
    padding-top:10px;
}
.api_header_favorite_wrapper{
    padding-top:10px;
}
.api_header_cart_image_wrapper{
    padding-top:5px;
}
}
@media screen and (min-width: 1024px){
.api_header_search {
    width:300px !important;
}
.api_header_search_wrapper{
    padding-top:30px;
}
.api_header_admin_wrapper{
    padding-top:40px;
}
.api_header_cart_wrapper{
    padding-top:40px;
}
.api_header_login_wrapper{
    padding-top:40px;
}
.api_header_favorite_wrapper{
    padding-top:40px;
}
.api_header_cart_image_wrapper{
    padding-top:34px;
}
}


@media screen and (min-width: 100px) and (max-width:767px) {
.api_header_logo{
    
}
.api_header_search {
    width:150px !important;
}
.api_header_search_wrapper{
    padding-top:5px;
}
.api_header_admin_wrapper{
    padding-top:10px;
}
.api_header_cart_wrapper{
    padding-top:5px;
}
.api_header_login_wrapper{
    padding-top:10px;
}
.api_header_favorite_wrapper{
    padding-top:10px;
}
.api_header_cart_image_wrapper{
    padding-top:5px;
}
}
@media screen and (min-width: 768px) and (max-width:1023px) {
.api_header_logo{
    
}
.api_header_search {
    width:250px !important;
}
.api_header_search_wrapper{
    padding-top:10px;
}
.api_header_admin_wrapper{
    padding-top:20px;
}
.api_header_cart_wrapper{
    padding-top:10px;
}
.api_header_login_wrapper{
    padding-top:20px;
}
.api_header_favorite_wrapper{
    padding-top:20px;
}
.api_header_cart_image_wrapper{
    padding-top:14px;
}
.api_header_favorite_label{
    display:none;
}
.api_header_admin_label{
    display:none;
}
}
@media screen and (min-width: 1024px) and (max-width:1366px) {
.api_header_logo{
    
}
.api_header_search {
    width:400px !important;
    margin-left:50px;
}
.api_header_search_wrapper{
    padding-top:10px;
}
.api_header_admin_wrapper{
    padding-top:20px;
}
.api_header_cart_wrapper{
    padding-top:10px;
}
.api_header_login_wrapper{
    padding-top:20px;
}
.api_header_favorite_wrapper{
    padding-top:20px;
}
.api_header_cart_image_wrapper{
    padding-top:14px;
}
.api_header_favorite_label{
    display:none;
}
.api_header_admin_label{
    display:none;
}
}
@media screen and (min-width: 1367px) {
.api_header_logo{
    
}
.api_header_search {
    width:400px !important;
    margin-left:50px;
}
.api_header_search_wrapper{
    padding-top:10px;
}
.api_header_admin_wrapper{
    padding-top:20px;
}
.api_header_cart_wrapper{
    padding-top:10px;
}
.api_header_login_wrapper{
    padding-top:20px;
}
.api_header_favorite_wrapper{
    padding-top:20px;
}
.api_header_cart_image_wrapper{
    padding-top:15px;
}
}
@media only screen and (max-width:425px){
    .page-contents {
        padding-top:3px!important;
    }
    .api_cart_count_text {
        margin-top: -36px !important;
        margin-left: 10px !important;
    }
}
</style>
<script>
$(".toggle-password").click(function() {
  $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});
</script>
';
} else {
    include('themes/default/shop/views/api_template/header/top_header.php');
}
