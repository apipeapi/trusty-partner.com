<header class="api_header" id="api_header">
    <!-- Top Header -->
    <section class="top-header " id="top-header">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                <?php
                if (!empty($pages)) {
                    echo '<ul class="list-inline nav pull-left hidden-xs">';
                    foreach ($pages as $page) {
                        echo '<li><a href="'.site_url('page/'.$page->slug).'">'.$page->name.'</a></li>';
                    }
                    echo '</ul>';
                }

                if ($this->api_shop_setting[0]['operated_by_logo'] != '') {
                    echo '<ul class="list-inline nav pull-left hidden-xs">';
                    echo '
                        <li>
                            <a href="javascript:void(0)" class="api_cursor_normal">
                                '.lang('Operated By ').' &nbsp;
                                <img alt="'.$shop_settings->shop_name.'" src="'.base_url('assets/uploads/logos/'.$this->api_shop_setting[0]['operated_by_logo']).'" height="25px;" />
                            </a>
                        </li>
                    ';
                    echo '</ul>';
                }


                echo '
                    <ul class="list-inline nav pull-left visible-sm visible-xs">
                        <li style="height:35px">
                            <a id="api_menu_mobile_button" class="api_pointer" href="#">
                                <span class="fa fa-bars fa-lg"></span>
                            </a>
                        </li>                                
                        <li>                                
                        <a class="api_color_white api_margin_left_5" href="'.site_url().'">
                            <img alt="'.$shop_settings->shop_name.'" src="'.base_url('assets/uploads/logos/'.$this->api_shop_setting[0]['logo_mobile']).'" height="25px;" />
                        </a>
                        </li>
                ';

                if ($this->api_shop_setting[0]['operated_by_logo'] != '') {
                    echo '
                        <li>                                
                            <a href="javascript:void(0)">
                                By &nbsp;
                                <img alt="'.$shop_settings->shop_name.'" src="'.base_url('assets/uploads/logos/'.$this->api_shop_setting[0]['operated_by_logo']).'" height="15px;" />
                            </a>
                        </li>
                    ';
                }

                echo '
                    </ul>
                ';                        
                ?>

                    <ul class="list-inline nav pull-right">

                    <li class="dropdown">
                              
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <img src="<?php echo base_url('assets/images/' . $Settings->user_language . '.png'); ?>" alt="">
                        <span class="hidden-xs">&nbsp;&nbsp;<?php echo ucwords($Settings->user_language); ?></span>
                        </a>
                             
                        <ul class="dropdown-menu dropdown-menu-right">
                            <?php 
                                $temp_display = '';
                                $language_array = unserialize(multi_language);
                                for ($i=0;$i<count($language_array);$i++) {  
                                    if (strtolower($language_array[$i][1]) == 'japanese') {
                                        $temp = 'api_border';
                                        $temp2 = 'style="margin-left:0px; width:17px;"';
                                    }
                                    else {
                                        $temp = '';
                                        $temp2 = '';
                                    }
                                    $temp_display .= '
                                        <li>
                                            <a href="'.site_url('main/language/'.strtolower($language_array[$i][1])).'">
                                                <img class="'.$temp.'" src="'.base_url('assets/images/'.strtolower($language_array[$i][1]).'.png').'" class="language-img" '.$temp2.'>
                                                &nbsp;&nbsp;'.ucwords($language_array[$i][1]).'
                                            </a>
                                        </li>    
                                    ';                   
                                }
                                echo $temp_display;
                            ?>
                        </ul>
                    </li>
                        <?php
                        if (DEMO) {
                            echo '<li class="hidden-xs hidden-sm"><a href="https://codecanyon.net/item/shop-module/20046278?ref=Tecdiary" class="green" target="_blank"><i class="fa fa-shopping-cart"></i> Buy Now!</a></li>';
                        }
                        ?>
                        <?= $loggedIn && $Staff ? '<li class="hidden-xs"><a href="'.admin_url().'"><i class="fa fa-dashboard"></i> '.lang('admin_area').'</a></li>' : ''; ?>
                        <li class="dropdown">
<!--                                
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <img src="<?php //echo base_url('assets/images/' . $Settings->user_language . '.png'); ?>" alt="">
                            <span class="hidden-xs">&nbsp;&nbsp;<?php //echo ucwords($Settings->user_language); ?></span>
                            </a>
-->                                 
                            <ul class="dropdown-menu dropdown-menu-right">
                            <?php $scanned_lang_dir = array_map(function ($path) {
                                return basename($path);
                            }, glob(APPPATH . 'language/*', GLOB_ONLYDIR));
                            foreach ($scanned_lang_dir as $entry) {
                                if (file_exists(APPPATH.'language'.DIRECTORY_SEPARATOR.$entry.DIRECTORY_SEPARATOR.'shop'.DIRECTORY_SEPARATOR.'shop_lang.php')) {
                                ?>
                            <li>
                                <a href="<?= site_url('main/language/' . $entry); ?>">
                                    <img src="<?= base_url('assets/images/'.$entry.'.png'); ?>" class="language-img">
                                    &nbsp;&nbsp;<?= ucwords($entry); ?>
                                </a>
                            </li>
                            <?php }
                            } ?>
                        </ul>
                    </li>
                    <?php if (!$shop_settings->hide_price && !empty($currencies)) { ?>
                    <li class="dropdown api_display_none">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <?= $selected_currency->symbol.' '.$selected_currency->code; ?>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <?php
                            foreach ($currencies as $currency) {
                                echo '<li><a href="'.site_url('main/currency/' . $currency->code).'">'.$currency->symbol.' '.$currency->code.'</a></li>';
                            }
                            ?>
                        </ul>
                    </li>

                    
                    <?php } ?>
                    <li style="height: 35px;">
                        <a href="<?php echo base_url().'cart'; ?>">
                            <div class="api_cart_count"></div>
                            <div id="api_cart_count_text" class="api_cart_count_text"></div>
                        </a>
                    </li>                                

                        <?php
                        if ($loggedIn) {
                            ?>
                            
                            <li class="">
                            <a href="<?= shop_url('wishlist'); ?>"><i class="fa fa-heart"></i> <span class="hidden-xs"><?= lang('wishlist'); ?></span></a>
                            </li>
                            
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-user"></i> <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <?php
                                        if ($this->session->userdata('group_id') == 1 || $this->session->userdata('group_id') == 2)
                                            echo '
                                                <li class=""><a href="'.site_url('admin').'"><i class="mi fa fa-dashboard"></i> '.lang('dashboard').'</a></li>        
                                            ';
                                    ?>                                                                                        
                                    <li class=""><a href="<?= site_url('profile'); ?>"><i class="mi fa fa-user"></i> <?= lang('profile'); ?></a></li>
                                    <li class=""><a href="<?= shop_url('orders'); ?>"><i class="mi fa fa-heart"></i> <?= lang('orders'); ?></a></li>

<?php
if ($this->session->userdata('sale_consignment_auto_insert') == 'yes') {
echo '
    <li class="">
        <a href="'.shop_url('consignment').'">
            <i class="mi fa fa-truck"></i> '.lang('Consignment').'
        </a>
    </li>        
';        
}
?>

                                    <li class=""><a href="<?= shop_url('wishlist'); ?>"><i class="mi fa fa-heart-o"></i> <?= lang('wishlist'); ?></a></li>
                                    <li class=""><a href="<?= shop_url('addresses'); ?>"><i class="mi fa fa-building"></i> <?= lang('address'); ?></a></li>
                                    <li class="divider"></li>
                                    <li class=""><a href="<?= site_url('logout'); ?>"><i class="mi fa fa-sign-out"></i> <?= lang('logout'); ?></a></li>
                                </ul>
                            </li>
                            <?php
                        } else {
                            ?>
                            <li>
                                <div class="dropdown">
                                    <button class="btn dropdown-toggle" type="button" id="dropdownLogin" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        <i class="fa fa-sign-in"></i> <?= lang('login'); ?> <span class="caret"></span>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-login" aria-labelledby="dropdownLogin" data-dropdown-in="zoomIn" data-dropdown-out="fadeOut">
                                        <?php  
                                        include 'themes/default/shop/views/user/login_form.php';
                                        echo $temp_display_login_form;
                                        ?>
                                    </div>
                                </div>
                            </li>
                            <?php
                        }
                        ?>
                        
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- End Top Header -->

    <!-- Main Header -->
    <section class="main-header" id="main-header">
        <div id="api_search_header" class="container padding-y-md" style="padding: 5px 15px 5px 15px !important;">
            <div class="row">

                <div class="col-sm-4 col-md-3 logo hidden-sm hidden-xs">
                    <a href="<?= site_url(); ?>">
                        <img alt="<?= $shop_settings->shop_name; ?>" src="<?= base_url('assets/uploads/logos/'.$shop_settings->logo); ?>" class="" height="80"/>
                    </a>
                </div>

                <div class="col-sm-8 col-md-9">
                    <div class="api_height_20 hidden-sm hidden-xs"></div>                        
                    <div class="row">
                        <div class="<?= (!$shop_settings->hide_price) ? 'col-sm-8 col-md-6 col-md-offset-3' : 'col-md-6 col-md-offset-6'; ?> search-box">
<?php

    if ($this->uri->segment(1) == 'category') 
        $temp = 'category/'.$this->uri->segment(2);
    else
        $temp = 'shop/products';

    echo form_open($temp, 'id="product-search-form" name="product-search-form" method="get"'); 
?>
                            <div class="input-group">
    <input name="search" type="text" class="form-control search_style" id="product-search" value="<?php echo $_GET['search']; ?>" aria-label="Search..." onkeydown="if (event.keyCode == 13) document.getElementById('product-search-form').submit();" placeholder="<?= lang('search');?>">
<?php
    echo '
        <input type="hidden" name="sort_by" value="'.$_GET['sort_by'].'"/>
        <input type="hidden" name="promotion_id" value="'.$_GET['promotion_id'].'"/>
    ';
?>


                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default btn-search api_link_box_none" onclick="document.getElementById('product-search-form').submit();">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                            <?= form_close(); ?>
                        </div>

                        <?php if (!$shop_settings->hide_price) { ?>
                        <div class="col-sm-4 col-md-3 cart-btn hidden-xs">
                            <button type="button" class="btn btn-theme btn-block dropdown-toggle shopping-cart" id="dropdown-cart" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <i class="fa fa-shopping-cart margin-right-md"></i>
                                <span class="cart-total-items"></span>
                                <!-- <i class="fa fa-caret-down margin-left-md"></i> -->
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-cart">
                                <div id="cart-contents">
                                    <table class="table table-condensed table-striped table-cart" id="cart-items"></table>
                                    <div id="cart-links" class="text-center margin-bottom-md">
                                        <div class="btn-group btn-group-justified" role="group" aria-label="View Cart and Checkout Button">
                                            <div class="btn-group">
                                                <a class="btn btn-default btn-sm" href="<?= site_url('cart'); ?>"><i class="fa fa-shopping-cart style_cart_shopping"></i> <?= '<span class="style_cart_shopping">'.lang('view_cart').'</span>'; ?></a>
                                            </div>
                                            <div class="btn-group api_display_none">
                                                <a class="btn btn-default btn-sm" href="<?= site_url('cart/checkout'); ?>"><i class="fa fa-check"></i> <?= lang('checkout'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="cart-empty"><?= lang('please_add_item_to_cart'); ?></div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Main Header -->

    <!-- Nav Bar -->
    <nav class="navbar navbar-default" role="navigation">
        <div class="container">
            <div id="api_cart_item_bar" class="navbar-header api_display_none">
                <button type="button" class="navbar-toggle collapsed hidden-sm hidden-xs" data-toggle="collapse" data-target="#navbar-ex1-collapse">
                    <?= lang('navigation'); ?>
                </button>
                <a href="<?= site_url('cart'); ?>" class="btn btn-default btn-cart-xs visible-xs pull-right shopping-cart api_margin_bottom_8">
                    <i class="fa fa-shopping-cart"></i> <span class="cart-total-items"></span>
                </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <li class="<?= $m == 'main' && $v == 'index' ? 'active' : ''; ?>"><a href="<?= base_url(); ?>"><?= lang('home'); ?></a></li>

                    <li class="<?= $m == 'shop' && $v == 'products' && $this->input->get('promo') != 'yes' && $_GET['product_use'] == '' ? 'active' : ''; ?>">
                        <a href="<?= shop_url('products'); ?>"><?= lang('products'); ?></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <?= lang('categories'); ?> <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu">
                            <?php
                                $config_data = array(
                                    'table_name' => 'sma_categories',
                                    'select_table' => 'sma_categories',
                                    'translate' => 'yes',
                                    'select_condition' => "parent_id = 0 and getTranslate(add_ons,'display','".f_separate."','".v_separate."') != 'no' order by CAST(order_number AS UNSIGNED) asc",
                                );      
                                $temp = $this->site->api_select_data_v2($config_data);
                  
                                if (is_array($temp))
                                if (count($temp) > 0) {
                                for ($i=0;$i<count($temp);$i++) {

                                    $config_data = array(
                                        'table_name' => 'sma_categories',
                                        'select_table' => 'sma_categories',
                                        'translate' => 'yes',
                                        'select_condition' => "parent_id = ".$temp[$i]['id']." and getTranslate(add_ons,'display','".f_separate."','".v_separate."') != 'no' order by CAST(order_number AS UNSIGNED) asc",
                                    );
                                    $temp3 = $this->site->api_select_data_v2($config_data);

                                    if (is_array($temp3))
                                    if (count($temp3) > 0) {
                                        echo '<li class="dropdown dropdown-submenu">';
                                        echo '<a class="dropdown-toggle" data-toggle="dropdown" href="'.site_url('category/'.$temp[$i]['slug']).'">'.$temp[$i]['title_'.$this->api_shop_setting[0]['api_lang_key']].'</a>';

                                        echo '<ul class="dropdown-menu">';
                                        for ($j=0;$j<count($temp3);$j++) {
                                            echo '<li><a href="'.site_url('category/'.$temp[$i]['slug'].'/'.$temp3[$j]['slug']).'">'.$temp3[$j]['title_'.$this->api_shop_setting[0]['api_lang_key']].'</a></li>';
                                        }
                                        echo '<li class="divider"></li>';
                                        echo '<li><a href="'.site_url('category/'.$temp[$i]['slug']).'">'.lang('all_products').'</a></li>';
                                        echo '</ul>';                
                                    }
                                    else {
                                        echo '<li class="">';
                                        echo '<a href="'.site_url('category/'.$temp[$i]['slug']).'">'.$temp[$i]['title_'.$this->api_shop_setting[0]['api_lang_key']].'</a>';                
                                    }
                                    echo '</li>';
                                }
                                }                  
                            ?>
                        </ul>
                    </li>
                    <li class="<?= $m == 'cart_ajax' && $v == 'index' ? 'active' : ''; ?>">
                        <a href="<?= site_url('cart'); ?>">
                            <?= lang('shopping_cart'); ?>
                        </a>
                    </li>
                    <?php if (!$shop_settings->hide_price) { ?>
                    <li class="<?= $m == 'cart_ajax' && $v == 'checkout' ? 'active' : ''; ?>"><a href="<?= site_url('cart/checkout'); ?>"><?= lang('checkout'); ?></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Nav Bar -->
</header>

<style>
.field-icon {
    float: right;
    margin-left: -25px;
    margin-right: 5px;
    margin-top: -25px;
    position: relative;
    z-index: 2;
    cursor: point;
}
</style>
<script>
$(".toggle-password").click(function() {
  $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});
</script>