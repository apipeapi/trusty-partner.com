<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php if (DEMO && ($m == 'main' && $v == 'index')) { ?>
<div class="page-contents padding-top-no">
    <div class="container">
        <div class="alert alert-info margin-bottom-no">
            <p>
                <strong>Shop module is not complete item but add-on to Stock Manager Advance and is available separately.</strong><br>
                This is joint demo for main item (Stock Manager Advance) and add-ons (POS & Shop Module). Please check the item page on codecanyon.net for more info about what's not included in the item and you must read the page there before purchase. Thank you
            </p>
        </div>
    </div>
</div>
<?php } ?>


<section class="footer">
<div class="container padding-bottom-md">
        <div class="row">
            <div class="col-md-4">
                <div class="title-footer"><span><?= lang('contact_us'); ?></span></div>
                <p>
                
                    <i class="fa fa-phone"></i> <span class="margin-left-md"><?= $shop_settings->phone; ?></span>
                    <i class="fa fa-envelope margin-left-xl"></i> <span class="margin-left-md"><?= $shop_settings->email; ?></span>
                </p>
            </div>

            <div class="clearfix visible-sm-block"></div>
<?php

        echo '
            <div class="col-md-4">
                <div class="title-footer"><span>'.lang('payment_methods').'</span></div>
                <p>'.lang('fotter_we_accept').'</p>
                <img title="Cash on delivery and ABA Payment" src="'.base_url().'assets/images/payway_accept.png" >
            </div>
        ';

?>

            <div class="col-md-4">
                <div class="title-footer"><span><?= lang('follow_us'); ?></span></div>
                <p><?= lang('follow_text'); ?></p>
                <ul class="follow-us">
                    <?php if (!empty($shop_settings->facebook)) { ?>
                    <li><a target="_blank" href="<?= $shop_settings->facebook; ?>"><i class="fa fa-facebook"></i></a></li>
                    <?php } if (!empty($shop_settings->twitter)) { ?>
                    <li><a target="_blank" href="<?= $shop_settings->twitter; ?>"><i class="fa fa-twitter"></i></a></li>
                    <?php } if (!empty($shop_settings->google_plus)) { ?>
                    <li><a target="_blank" href="<?= $shop_settings->google_plus; ?>"><i class="fa fa-google-plus"></i></a></li>
                    <?php } if (!empty($shop_settings->instagram)) { ?>
                    <li><a target="_blank" href="<?= $shop_settings->instagram; ?>"><i class="fa fa-instagram"></i></a></li>
                    <?php } ?>
                </ul>
            </div>
<?php
echo '

    <div class="col-md-12" style="text-align:center;margin-top:20px">
        <ul class="list-inline api_screen_hide_768">
';
    if ($this->api_shop_setting[0]['hide_about_us'] != 1)
        echo '        
            <li><a href="https://daishintc.com/aboutus" target="_blank">'.lang('about_us').'</a></li>
        ';
    if ($this->api_shop_setting[0]['hide_contact_us'] != 1)    
        echo '
            <li><a href="https://daishintc.com/contact" target="_blank">'.lang('contact_us').'</a></li>
        ';
echo '
            <li><a href="'.base_url().'page/Terms_and_Conditions">'.lang('terms_and_conditions').'</a></li>
            <li><a href="'.base_url().'page/Privacy_Policy">'.lang('privacy_policy').'</a></li>
            <li><a href="'.base_url().'page/Return_and_Refund_Policy">'.lang('return_and_refund_policy').'</a></li>
            <li><a href="'.base_url().'page/Contact-Us">'.lang('contact_us').'</a></li>
        </ul>
    </div>
   
';
        //  mobile *****
        echo '
        <div class="col-md-12" style="text-align:center;margin-top:-30px;margin-bottom:15px;">
        <ul class="list-inline api_screen_show_768">
';
    if ($this->api_shop_setting[0]['hide_about_us'] != 1)
        echo '        
            <li><a href="https://daishintc.com/aboutus" target="_blank">'.lang('about_us').'</a></li>
        ';
    if ($this->api_shop_setting[0]['hide_contact_us'] != 1)    
        echo '
            <li><a href="https://daishintc.com/contact" target="_blank">'.lang('contact_us').'</a></li>
        ';
echo '
        
            <li><a href="'.base_url().'page/Terms_and_Conditions">'.lang('terms_and_conditions').'</a></li> <br>
            <li><a href="'.base_url().'page/Privacy_Policy">'.lang('privacy_policy').'</a></li> <br>
            <li><a href="'.base_url().'page/Return_and_Refund_Policy">'.lang('return_and_refund_policy').'</a></li>
        </ul>
    </div>
';
?>

        </div>
    </div>
    <div class="footer-bottom">
        <div class="container api_text_align_center">
            <div class="col-md-12">
<?php

echo '
    &copy; '.date('Y').' '.$shop_settings->shop_name.' '.lang('all_rights_reserved').'
';

?>
                
            </div>

            <ul class="list-inline pull-right line-height-md api_display_none">
                <li class="padding-x-no text-size-lg">
                    <a href="#" class="theme-color text-blue" data-color="blue"><i class="fa fa-square"></i></a>
                </li>
                <li class="padding-x-no text-size-lg">
                    <a href="#" class="theme-color text-blue-grey" data-color="blue-grey"><i class="fa fa-square"></i></a>
                </li>
                <li class="padding-x-no text-size-lg">
                    <a href="#" class="theme-color text-brown" data-color="brown"><i class="fa fa-square"></i></a>
                </li>
                <li class="padding-x-no text-size-lg">
                    <a href="#" class="theme-color text-cyan" data-color="cyan"><i class="fa fa-square"></i></a>
                </li>
                <li class="padding-x-no text-size-lg">
                    <a href="#" class="theme-color text-green" data-color="green"><i class="fa fa-square"></i></a>
                </li>
                <li class="padding-x-no text-size-lg">
                    <a href="#" class="theme-color text-grey" data-color="grey"><i class="fa fa-square"></i></a>
                </li>
                <li class="padding-x-no text-size-lg">
                    <a href="#" class="theme-color text-purple" data-color="purple"><i class="fa fa-square"></i></a>
                </li>
                <li class="padding-x-no text-size-lg">
                    <a href="#" class="theme-color text-orange" data-color="orange"><i class="fa fa-square"></i></a>
                </li>
                <li class="padding-x-no text-size-lg">
                    <a href="#" class="theme-color text-pink" data-color="pink"><i class="fa fa-square"></i></a>
                </li>
                <li class="padding-x-no text-size-lg">
                    <a href="#" class="theme-color text-red" data-color="red"><i class="fa fa-square"></i></a>
                </li>
                <li class="padding-x-no text-size-lg">
                    <a href="#" class="theme-color text-teal" data-color="teal"><i class="fa fa-square"></i></a>
                </li>
            </ul>

            <div class="clearfix"></div>
        </div>
    </div>
</section>

<a href="#" class="back-to-top text-center btn-scrolltop" onclick="$('body,html').animate({scrollTop:0},500); return false">
    <i class="fa fa-angle-double-up"></i>
</a>
</section>
<?php if (!get_cookie('shop_use_cookie') && get_cookie('shop_use_cookie') != 'accepted' && !empty($shop_settings->cookie_message)) { ?>
<div class="cookie-warning">
    <div class="bounceInLeft alert alert-info">
        <!-- <a href="<?= site_url('main/cookie/accepted'); ?>" class="close">&times;</a> -->
        <a href="<?= site_url('main/cookie/accepted'); ?>" class="btn btn-sm btn-primary" style="float: right;"><?= lang('i_accept'); ?></a>
        <p>
            <?= $shop_settings->cookie_message; ?>
            <?php if (!empty($shop_settings->cookie_link)) { ?>
            <a href="<?= site_url('page/'.$shop_settings->cookie_link); ?>"><?= lang('read_more'); ?></a>
            <?php } ?>
        </p>
    </div>
</div>

<?php } ?>
<script type="text/javascript">
<?php
    echo 'var api_vat = "'.$customer->vat_no.'";';
    if ($customer->vat_no) {
        if ($customer->vat_invoice_type != 'do') {
            echo '
                api_vat = "'.$customer->vat_no.'";
            ';
        } else {
            echo '
                api_vat = "";
            ';
        }
    }
?>    
    if (api_vat != '') api_vat = 10; else api_vat = 0;
    var m = '<?= $m; ?>', v = '<?= $v; ?>', products = {}, filters = <?= isset($filters) && !empty($filters) ? json_encode($filters) : '{}'; ?>, shop_color, shop_grid, sorting;
    var cart = <?= isset($cart) && !empty($cart) ? json_encode($cart) : '{}' ?>;
    // console.log(cart);
    var site = {base_url: '<?= base_url(); ?>', site_url: '<?= site_url('/'); ?>', shop_url: '<?= shop_url(); ?>', csrf_token: '<?= $this->security->get_csrf_token_name() ?>', csrf_token_value: '<?= $this->security->get_csrf_hash() ?>', settings: {display_symbol: '<?= $Settings->display_symbol; ?>', symbol: '<?= $Settings->symbol; ?>', decimals: <?= $Settings->decimals; ?>, thousands_sep: '<?= $Settings->thousands_sep; ?>', decimals_sep: '<?= $Settings->decimals_sep; ?>', order_tax_rate: false, products_page: <?= $shop_settings->products_page ? 1 : 0; ?>}, shop_settings: {private: <?= $shop_settings->private ? 1 : 0; ?>, hide_price: <?= $shop_settings->hide_price ? 1 : 0; ?>}}

    var lang = {};
    lang.page_info = '<?= lang('page_info'); ?>';
    lang.cart_empty = '<?= lang('empty_cart'); ?>';
    lang.item = '<?= lang('item'); ?>';
    lang.items = '<?= lang('items'); ?>';
    lang.unique = '<?= lang('unique'); ?>';
    lang.total_items = '<?= lang('total_items'); ?>';
    lang.total_unique_items = '<?= lang('total_unique_items'); ?>';
    lang.tax = '<?= lang('tax'); ?>';
    lang.shipping = '<?= lang('shipping'); ?>';
    lang.total_w_o_tax = '<?= lang('total_w_o_tax'); ?>';
    lang.product_tax = '<?= lang('product_tax'); ?>';
    lang.order_tax = '<?= lang('order_tax'); ?>';
    lang.total = '<?= lang('total'); ?>';
    lang.grand_total = '<?= lang('grand_total'); ?>';
    lang.grand_total_cart = '<?= lang('grand_total').' <span style="color:#dc9b01;">('.lang('include_vat').')</span>'; ?>';
    lang.reset_pw = '<?= lang('forgot_password?'); ?>';
    lang.type_email = '<?= lang('type_email_to_reset'); ?>';
    lang.submit = '<?= lang('submit'); ?>';
    lang.error = '<?= lang('error'); ?>';
    lang.add_address = '<?= lang('add_address'); ?>';
    lang.update_address = '<?= lang('update_address'); ?>';
    lang.fill_form = '<?= lang('fill_form'); ?>';
    lang.already_have_max_addresses = '<?= lang('already_have_max_addresses'); ?>';
    lang.send_email_title = '<?= lang('send_email_title'); ?>';
    lang.message_sent = '<?= lang('message_sent'); ?>';
    lang.add_to_cart = '<?= lang('add_to_cart'); ?>';
    lang.out_of_stock = '<?= lang('out_of_stock'); ?>';
    lang.x_product = '<?= lang('x_product'); ?>';
    lang.are_you_sure = '<?= lang('are_you_sure');?>';
    lang.you_will_order_item_first_time = '<?= lang('first_time_order');?>';
    lang.remove_from_favorite_list = '<?= lang('remove_from_favorite_list');?>';
    lang.validation_form = '<?= lang('validation_form'); ?>';
   // lang.reset_pw = '<?=lang('resert_pw');?>';
    lang.type_email = '<?= lang('type_email'); ?>';
    
</script>
<?php if ($m == 'shop' && $v == 'product') { ?>
<script type="text/javascript">
$(document).ready(function ($) {
  $('.rrssb-buttons').rrssb({
    title: '<?= $product->code.' - '.$product->name; ?>',
    url: '<?= site_url('product/'.$product->slug); ?>',
    image: '<?= base_url('assets/uploads/'.$product->image); ?>',
    description: '<?= $page_desc; ?>',
    // emailSubject: '',
    // emailBody: '',
  });
});
</script>
<?php } ?>
<script type="text/javascript">

<?php if ($message || $warning || $error || $reminder) { ?>
$(document).ready(function() {
    
    <?php if ($message) { ?>
        sa_alert('<?=lang('success');?>', '<?= trim(str_replace(array("\r","\n","\r\n"), '', addslashes($message))); ?>',0,1);
    <?php } if ($warning) { ?>
        sa_alert('<?=lang('warning');?>', '<?= trim(str_replace(array("\r","\n","\r\n"), '', addslashes($warning))); ?>', 'warning', 1);
    <?php } if ($error) { ?>
        sa_alert('<?=lang('error');?>', '<?= trim(str_replace(array("\r","\n","\r\n"), '', addslashes($error))); ?>', 'error', 1);
    <?php } if ($reminder) { ?>
        
        sa_alert('<?=lang('reminder');?>', '<?= trim(str_replace(array("\r","\n","\r\n"), '', addslashes($reminder))); ?>', 'info', 1);
        
    <?php } ?>
});
<?php } ?>
    let o_url = '<?=site_url();?>';
    function a_btn(href, class_name, name,style=""){
        return '<a href="'+href+'" class="'+class_name+'" style="'+style+'">'+name+'</a>';
    }
    function alert_auth(){
        swal({
            title: '<?php echo lang('login_first');?>',
            type:'warning',
            html: '<p style="font-size:15px"><?php echo lang('You can register with us or with socials'); ?></p> <br/><div style="margin-top:10px">'
            +a_btn(o_url+'social_auth/login/Facebook','btn','<i class="fa fa-facebook"></i> facebook','background-color:rgb(76, 103, 161);border-color:rgb(76, 103, 161);margin:4px 4px;border-radius:20px;color:white;width:120px')
            +a_btn(o_url+'social_auth/login/Google','btn btn-danger','<i class="fa fa-google"></i> Google','margin:4px 4px;background-color:rgb(223, 76, 57);border-color:rgb(223, 76, 57);border-radius:20px;color:white;width:120px')+'</div>'
            +'<div style="padding-top:50px">'+a_btn(o_url+'login','btn btn-danger','<?php echo lang('login');?>','margin:4px 4px;background-color:rgb(76, 175, 80);border-color:rgb(76, 175, 80);border-radius:4px;color:white;width:120px')
            +a_btn(o_url+'login?register=1','btn btn-danger','<?php echo lang('registers');?>','margin:4px 4px;background-color:rgb(0, 188, 212);border-color:rgb(0, 188, 212);border-radius:4px;color:white;width:120px')+'</div>',
            showCancelButton: false,
            showConfirmButton: false
        }).catch(swal.noop);
    }
    function alert_auth_no_social(){
        swal({
            title: '<?php echo lang('login_first');?>',
            type:'warning',
            html:'<div style="padding-top:50px">'+a_btn(o_url+'login','btn btn-danger','<?php echo lang('login');?>','margin:4px 4px;background-color:rgb(76, 175, 80);border-color:rgb(76, 175, 80);border-radius:4px;color:white;width:120px')
            +a_btn(o_url+'login?register=1','btn btn-danger','<?php echo lang('registers');?>','margin:4px 4px;background-color:rgb(0, 188, 212);border-color:rgb(0, 188, 212);border-radius:4px;color:white;width:120px')+'</div>',
            showCancelButton: false,
            showConfirmButton: false
        }).catch(swal.noop);
    }    
    var login_first = function () {
        var tmp = null;
        $.ajax({
            async: false,
            global: false,
            url: "<?=site_url('/is_login');?>",
            success: function (data) {
                tmp = data;
            }
        });
        return tmp == 1?true:false;
    }();
    $('button.add-to-cart').click(function(){
        if(login_first===false){
            alert_auth_no_social();
            return false;
        }else{
            return true;
        }
    })
</script>
<?php
    
if (!is_int(strpos($_SERVER['REQUEST_URI'], "cart")) && !is_int(strpos($_SERVER['REQUEST_URI'], "shop/orders")) && $this->session->userdata('user_id') != '') {
    echo '
<div class="api_checkout_button_fix blue visible-sm visible-xs">
    <a href="'.base_url().'cart">
        <button type="button" class="btn-lg btn-theme btn-block btn-group-payment-o">
            <i class="fa fa-cart-arrow-down margin-right-md"></i>
            <strong>'.lang('view_cart').'</strong>    
        </button>
    </a>
</div>
';
}

    include 'themes/default/shop/views/sub_page/api_style_mobile.php';

    if ($this->api_shop_setting[0]['facebook_login'] == 1) {
        include 'themes/default/shop/views/sub_page/api_set_information.php';
    }


    if ($this->api_shop_setting[0]['product_import_date'] == 1 && base_url() == 'https://air.phsarjapan.com/') {
        $temp = $this->shop_model->api_display_import_date($this->api_shop_setting[0]['product_import_date_list']);
        if ($loggedIn && $this->session->userdata('api_selected_import_date') == '' && $this->session->userdata('group_id') == 3) {
            echo $temp['script'];
        }
        echo $temp['script_2'];
    }

    if ($this->api_shop_setting[0]['require_login'] != 1 && !$loggedIn && $this->session->userdata('api_message_login_first') == '') {
        echo '<script>alert_auth_no_social();</script>';
        $this->session->set_userdata('api_message_login_first', 1);
    }
    echo '<script src="'.base_url().'assets/api/js/public.js?v=1"></script>';
    echo '<script src="'.$assets.'js/scripts.min.js?v='.$this->api_shop_setting[0]['version'].'"></script>';

?>
<script type="text/javascript">
    $(window).bind("load", function () {
        $('#loader-wrapper').fadeOut(1000);
    });
</script>
</body>
</html>





