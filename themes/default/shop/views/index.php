<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>



<?php 
    include 'themes/default/shop/views/sub_page/front_slide.php';



    include 'themes/default/shop/views/sub_page/front_page_category.php'; 
?>

<?php
    

    // // include 'themes/default/shop/views/sub_page/slide_category.php';
    // include 'themes/default/shop/views/sub_page/list_category.php';
    
    // if ($this->session->userdata('api_mobile') != 1) {
    //     include 'themes/default/shop/views/sub_page/api_carousel/api_carousel_kata_tip/promotion_featured.php';
    //     include 'themes/default/shop/views/sub_page/api_carousel/api_carousel_kata_tip/member_main_category.php';
    // } else {
    //     $api_view_array['show_main_category'] = 1;
    //     include 'themes/default/shop/views/sub_page/api_carousel_ebay/featured.php';
    // }

    echo '
        <div class="api_height_15" style="background-color:#1a1a1a;"></div>
    ';
?>
<script>
    function slide_gen(query){
        var id = query;
        var slide = '#'+query+' ul';
        select = $('#'+query+' ul li');
		var slideCount = select.length;
		var slideWidth = select.width();
		var slideHeight = select.height();
		var sliderUlWidth = slideCount * slideWidth;
		select.parent().parent().css({ width: 1130, height: slideHeight });
		select.parent().css({ width: 1130, marginLeft: 0 });
		select.parent().children('li:last-child').prependTo(slide);
        // $('#count_pro_'+id).replaceWith(slideCount);
    }
    $('[id^="slider"] ul').each(function(){
        var length = $(this).children('li').length
        if(length<6){
            $(this).parent().parent().children('a').remove();
        }
    })
    $('a.control_prev_cat').click(function () {
        var id = $(this).attr('target-slide')
        $('#'+id+' ul').animate({
            left: + slideWidth
        }, 200, function () {
            $('#'+id+' ul li:last-child').prependTo('#'+id+' ul');
            $('#'+id+' ul').css('left', '');
        });
    });
    $('a.control_next_cat').click(function () {
        var id = $(this).attr('target-slide')
		$('#'+id+' ul').animate({
            left: - slideWidth
        }, 200, function () {
            $('#'+id+' ul li:first-child').appendTo('#'+id+' ul');
            $('#'+id+' ul').css('left', '');
        });
    });
</script>